<?php

	include("../includes/incsitecommon.php");
	header("Content-Type: text/css");

	$conn = connect();

	$strdbsql = "SELECT * FROM site_colours WHERE recordID = 1";
	$strType = "single";
	$colours = query($conn, $strdbsql, $strType);

	$primary = $colours['colourPrimary'];
	$secondary = $colours['colourSecondary'];
	$tertiary = $colours['colourTertiary'];
//	$quaternary = $colours['colourQuaternary'];
	$quaternary = "#3F51B5";

	$base = $colours['colourBase'];

	$backgroundImage = $colours['imageBackground'];
	$backgroundColour = $colours['colourBackground'];

	$font = $colours['font'];

	ob_start();	
	
?>

/* --------------------------------------------------
   CUSTOM FONTS
-------------------------------------------------- */
@font-face {
	font-family: 'Circular';
	src: url('../fonts/circular/Circular-Book.eot'); /* IE9 Compat Modes */
	src: url('../fonts/circular/Circular-Book.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
		 url('../fonts/circular/Circular-Book.woff2') format('woff2'), /* Super Modern Browsers */
		 url('../fonts/circular/Circular-Book.woff') format('woff'), /* Pretty Modern Browsers */
		 url('../fonts/circular/Circular-Book.ttf')  format('truetype'), /* Safari, Android, iOS */
		 url('../fonts/circular/Circular-Book.svg#svgFontName') format('svg'); /* Legacy iOS */
	font-style: normal;
	font-weight: normal;
}

@font-face {
	font-family: 'Circular';
	src: url('../fonts/circular/Circular-Medium.eot'); /* IE9 Compat Modes */
	src: url('../fonts/circular/Circular-Medium.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
		 url('../fonts/circular/Circular-Medium.woff2') format('woff2'), /* Super Modern Browsers */
		 url('../fonts/circular/Circular-Medium.woff') format('woff'), /* Pretty Modern Browsers */
		 url('../fonts/circular/Circular-Medium.ttf')  format('truetype'), /* Safari, Android, iOS */
		 url('../fonts/circular/Circular-Medium.svg#svgFontName') format('svg'); /* Legacy iOS */
	font-style: normal;
	font-weight: 500;
}

@font-face {
	font-family: 'Circular';
	src: url('../fonts/circular/Circular-Bold.eot'); /* IE9 Compat Modes */
	src: url('../fonts/circular/Circular-Bold.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
		 url('../fonts/circular/Circular-Bold.woff2') format('woff2'), /* Super Modern Browsers */
		 url('../fonts/circular/Circular-Bold.woff') format('woff'), /* Pretty Modern Browsers */
		 url('../fonts/circular/Circular-Bold.ttf')  format('truetype'), /* Safari, Android, iOS */
		 url('../fonts/circular/Circular-Bold.svg#svgFontName') format('svg'); /* Legacy iOS */
	font-style: normal;
	font-weight: 900;
}

/* --------------------------------------------------
   COMMON ELEMENTS
-------------------------------------------------- */
html {
	font-family: "Circular", calibri, "trebuchet ms", arial,sans-serif;
	font-size: 15px;
}

body {
	background-color: <?php print($base); ?>;
	background-image: url(/images/elements/watermark.png);
	background-position: top right;
	background-size: 1000px auto;
/*	background-image: url(/images/elements/sunflower-watermark3.png);
	background-position: bottom center;
	background-attachment:fixed;
	background-size: 100% auto;
	background-repeat:repeat-x;*/
	color: <?php print($tertiary); ?>;
	display: flex;
    min-height: 100vh;
    flex-direction: column;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
}

main {
	flex: 1 0 auto;
	padding: 20px 0 45px;
}

main.dark {
    background-color: rgba(0, 0, 0, 0.05);
}

main.large {
	background-color: rgba(0, 0, 0, 0.05);
	padding: 85px 0 110px;
}

section {
	display: inline-block;
	vertical-align: top;
	width: 100%;
}

.container {
	max-width: 1235px;
	width: calc(100% - 60px);
}

.row {
	margin-bottom: 30px;
}

.container .row {
	margin-left: -15px;
	margin-right: -15px;
}

.container .row.thin {
	margin-left: -5px;
	margin-right: -5px;
}

.container .row.extra {
	margin-left: -30px;
	margin-right: -30px;
}

.row .col {
	padding-left: 15px;
	padding-right: 15px;
}

.row.thin .col {
	padding-left: 5px;
	padding-right: 5px;
}

.row.extra .col {
	padding-left: 30px;
	padding-right: 30px;
}

.sidebar {
	padding-right: 30px;
}

.sidebar .block {
	margin: 0 0 60px;
}

.right .sidebar {
	padding-left: 30px;
	padding-right: 0;
}

ol:not(.browser-default) {
	padding-left: 0;
	list-style: none;
}

ul {
	margin-bottom: 21px;
}

ul.buttons {
	display: inline-block;
	margin: 0 -10px;
}

ul.list {
	padding-left: 15px;
}

ul.list li {
	list-style-type: disc;
	margin: 0 0 10px;
}

ul.horizontal {
	display: inline-block;
	overflow: hidden;
	position: relative;
	vertical-align: top;
	white-space: nowrap;
	width: 100%;
}

ul.horizontal:after {
	background: -moz-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 100%);
	background: linear-gradient(to right, rgba(255,255,255,0) 0%,rgba(255,255,255,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=1 );
	bottom: 0;
	content: "";
	position: absolute;
	right: 0;
	top: 0;
	width: 90px;
}

ul.horizontal li {
	display: inline-block;
}

table {
	border: solid 1px #EFEFEF;
	margin: 30px 0;
	width: 100%;
}

table tr {
	border-color: #EFEFEF;
}

table th,
table td {
	padding: 15px;
	vertical-align: top;
}

table.keywords th:first-child,
table.keywords td:first-child {
	color: <?php print($primary); ?>;
}

.crop-both {
	margin-top: 0 !important;
	margin-bottom: 0 !important;
}

.crop-top {
	margin-top: 0 !important;
}

.crop-bottom {
	margin-bottom: 0 !important;
}

.crop-both {
	margin-bottom: 0 !important;
	margin-top: 0 !important;
}

.buttons li {
	float: left;
	padding: 0 10px;
}

a.small:not(.btn) {
	font-size: 13px;
}

a.simple {
	text-decoration: none;
}

p {
	font-size: 15px;
	margin: 0 0 15px;
}

strong {
	font-weight: 900;
}

hr {
	border: none;
	border-top: solid 1px rgba(153,153,153,0.3);
	border-bottom: solid 1px rgba(153,153,153,0.3);
}

span.date {
	color: #adadad;
    display: inline-block;
    margin: 0 0 10px;
    vertical-align: top;
}

address {
	font-size: 15px;
	margin: 0 0 20px;
}

.map {
	border: none;
	height: 577px;
	position: relative;
	width: 100%;
}

img.frame {
	border: solid 6px #FFFFFF;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    transform: rotate(-5deg);
    -webkit-transform: rotate(-5deg);
    width: 100%;
}

img.standard {
	float: left;
    margin: 0 20px 20px 0;
    width: 300px;
}

img.standard.right {
	margin: 0 20px 0 20px;
}

.contain {
	background-color: #f7f7f7;
    border: solid 2px #e5e5e5;
    padding: 10px;
}

.contain ul {
	margin: 0;
}

.contain ul li:first-child {
	font-weight: 900;
}

button {
	cursor: pointer;
}

/* --------------------------------------------------
   HEADINGS
-------------------------------------------------- */
h1,
.h1,
h2,
.h2,
h3,
.h3,
h5,
.h5 {
    font-style: normal;
}

.heading {
	display: inline-block;
	margin: 0 0 30px;
    position: relative;
	text-align: left;
	vertical-align: top;
	width: 100%;
	z-index: 10;
}

.heading.small {
	margin: 0 0 20px;
}

.heading.center-align {
	text-align: center;
}

.heading h1,
.heading .h1,
.heading h2,
.heading .h2,
.heading h3,
.heading .h3 {
	background-color: <?php print($base); ?>;
	display: inline-block;
	font-size: 28px;
	font-weight: 900;
	margin: 0;
	padding: 0 30px 0 0;
	vertical-align: middle;
}

.heading.small h1,
.heading.small .h1,
.heading.small h2,
.heading.small .h2,
.heading.small h3,
.heading.small .h3 {
	font-size: 22px;
}

.heading.center-align h1,
.heading.center-align .h1,
.heading.center-align h2,
.heading.center-align .h2,
.heading.center-align h3,
.heading.center-align .h3 {
	padding: 0 30px;
}

h1,
.h1 {
	font-size: 52px;
	font-weight: 900;
	margin: 0 0 15px;
}

h2,
.h2 {
	font-size: 30px;
	font-weight: 500;
	line-height: 34px;
	margin: 0 0 23px;
}

h3,
.h3 {
	font-size: 22px;
	font-weight: 900;
	line-height: 24px;
	margin: 0 0 15px;
}

h4,
.h4 {
	font-size: 18px;
	font-weight: 900;
	line-height: 24px;
	margin: 0 0 15px;
}

h5,
.h5 {
	font-size: 15px;
	font-weight: 900;
	margin: 0 0 5px;
}

h3.code,
h4.code,
h5.code {
	border: dashed 2px <?php print($base); ?>;
    display: inline-block;
    font-size: 18px;
    margin: 0 auto 5px;
    padding: 10px;
    text-transform: uppercase;
}

/* --------------------------------------------------
   BUTTONS
-------------------------------------------------- */
.btn,
.btn.large {
	background-color: #E6E100;
	border: solid 1px rgba(0, 0, 0, .05);
	border-radius: 4px;
	box-shadow: none;
	color: <?php print($base); ?>;
	font-size: 14px;
	font-weight: 500;
	height: 40px;
	letter-spacing: 0;
	line-height: 40px;
	padding: 0 20px;
	text-transform: none;
}

.btn.wide {
	width: 100%;
}

.btn.small {
	height: 35px;
	line-height: 35px;
	padding: 0 15px;
}

.btn.large {
	font-size: 16px;
	height: 50px;
	line-height: 50px;
	padding: 0 30px;
}

.btn.trim {
	border-top-right-radius: 40px;
}

.btn.trim:after {
	content: "";
}

.btn.isolate {
	height: 35px;
	line-height: 35px;
	padding: 0;
	width: 35px !important;
}

.btn.finished {
	pointer-events: none;
}

.btn.transparent {
	background-color: transparent;
	border-color: transparent;
	box-shadow: none;
	color: <?php print($tertiary); ?>;
}

.btn.text {
	border-color: transparent;
	color: <?php print($tertiary); ?>;
	padding: 0;
}

.btn.large.text {
	font-size: 16px;
}

.btn.text svg {
	margin: 0 15px 0 0;
}

.btn:hover,
.btn.large:hover {
	background-color: #D7D212;
	box-shadow: none;
}

.btn i,
.btn.large i {
	font-size: 16px;
	font-weight: 900;
	margin-right: 5px;
	vertical-align: middle;
}

.btn i.right {
	margin-left: 5px;
	margin-right: 0;
}

.btn.isolate i {
	margin: 0;
}

.btn svg {
	display: inline-block;
	margin: 0 20px 0 -10px;
	vertical-align: middle;
	width: 20px;
}

.btn.isolate svg {
	margin: 0 auto;
}

.btn.btn-primary {
	background-color: <?php print($primary); ?>;
	color: <?php print($base); ?>;
}

.btn.btn-primary:hover {
	background-color: #7DBB4C;
}

.btn.btn-secondary {
	background-color: <?php print($secondary); ?>;
	color: <?php print($tertiary); ?>;
}

.btn.btn-secondary:hover {
	background-color: #FFDC7d;
}

.btn.btn-tertiary {
	background-color: <?php print($tertiary); ?>;
	color: <?php print($base); ?>;
}

.btn.btn-tertiary:hover {
	background-color: #222222;
}

.btn.btn-base {
	background-color: <?php print($base); ?>;
	color: <?php print($tertiary); ?>;
}

.btn.btn-base:hover {
	background-color: <?php print($tertiary); ?>;
	color: <?php print($base); ?>;
}

.btn.btn-default {
	background-color: #999999;
}

.btn.btn-default:hover {
	background-color: #666666;
}

.btn.btn-dark {
	background-color: #B0B612;
}

.btn.btn-dark:hover {
	background-color: #84880E;
}

.btn.btn-contact {
	background-color: #5B6ABF;
}

.btn.btn-contact:hover {
	background-color: #4E5BA5;
}

.btn.btn-favourite {
	background-color: #F44336;
}

.btn.btn-icon {
	background-color: transparent;
	box-shadow: none;
	color: #222222;
	padding: 0;
	text-align: center;
	width: 30px;
}

.btn.btn-icon i {
	margin-right: 0;
}

.btn.btn-counter {
	background-color: transparent;
	border: solid 2px #222222;
	box-shadow: none;
	color: #222222;
	height: 25px;
	line-height: 21px;
	padding: 0;
	text-align: center;
	vertical-align: middle;
	width: 25px;
}

.btn.btn-counter[disabled] {
	background-color: transparent !important;
	border-color: #EBEBEB;
	color: #EBEBEB !important;
}

.btn.btn-counter i {
	margin: 0;
}

.btn .preloader-wrapper {
	height: 30px;
	margin: 5px 0 0;
	width: 30px;
}

.btn.small .preloader-wrapper {
	height: 20px;
	margin: 5px 0 0;
	width: 20px;
}

.btn:disabled .spinner-layer {
	border-color: #222222;
}

.btn.btn-blank {
	background-color: transparent;
	box-shadow: none;
	color: #55ACEE;
	font-size: 14px;
	font-weight: normal;
	height: auto;
	line-height: 18px;
	padding: 0;
}

#paypal-button {
	margin-top: 30px;
	position: relative;
	z-index: 10;
}

.buttons a,
.buttons button {
	margin: 0 15px;
}

/* --------------------------------------------------
   HEADER
-------------------------------------------------- */
header {
	background-color: #FFFFFF;
	border-bottom: 1px solid #EFEFEF;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05);
	padding: 20px 0 0;
	position: relative;
	transition: transform .3s, box-shadow .3s;
    -webkit-transition: transform .3s, box-shadow .3s;
	width: 100%;
	will-change: transform, top, box-shadow;
	z-index: 1000;
}

header .header-wrapper {
	display: inline-block;
	vertical-align: top;
	width: 100%;
}

header .header-wrapper .header-logo {
	float: left;
}

header .header-wrapper .buttons {
	float: left;
}

header .header-wrapper .buttons a,
header .header-wrapper .buttons button {
	font-size: 18px;
	line-height: 20px;
	margin: 0 15px;
	padding: 0 15px 0 20px;
	text-align: left;
}

header .header-wrapper .buttons a.btn.btn-secondary,
header .header-wrapper .buttons button.btn.btn-secondary {
	margin-right: 0;
	padding: 0 40px 0 35px;
}

header .header-wrapper .buttons a:last-child,
header .header-wrapper .buttons button:last-child {
	margin-left: 25px;
	margin-right: 0;
}

header .header-wrapper .buttons a svg,
header .header-wrapper .buttons button svg {
	margin: 10px 15px 0 -20px;
}

header .header-wrapper .buttons a.btn.btn-secondary svg,
header .header-wrapper .buttons button.btn.btn-secondary svg {
	margin-left: -20px;
}

header .header-wrapper .buttons a span:not(.badge),
header .header-wrapper .buttons button span:not(.badge) {
	display: block;
	font-size: 13px;
	font-weight: 400;
	line-height: 16px;
	white-space: nowrap;
}

header .header-wrapper .buttons a span:not(.badge).large,
header .header-wrapper .buttons button span:not(.badge).large {
	font-size: 16px;
	font-weight: 500;
	line-height: 20px;
	margin-top: 6px;
}

header .header-wrapper .buttons a span:not(.badge).large i,
header .header-wrapper .buttons button span:not(.badge).large i {
	margin-left: 5px;
	margin-right: -5px;
}

header .header-wrapper .buttons a span.badge,
header .header-wrapper .buttons button span.badge {
	background-color: #F44336;
    border: solid 1px rgba(0,0,0,.05);
    border-radius: 22px;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: 400;
    height: 22px;
    line-height: 22px;
    min-width: 22px;
    padding: 0 5px;
    position: absolute;
    right: -10px;
    top: -8px;
}

header .brand-logo {
    display: inline-block;
}

header .brand-logo img {
	max-height: 55px;
	max-width: 260px;
	vertical-align: middle;
}

header .button-collapse,
header .button-basket {
	margin-left: 15px;
}


header .button-basket {
	color: <?php print($tertiary); ?>;
	font-size: 16px;
	font-weight: 900;
	text-decoration: none;
}

header .button-collapse i,
header .button-basket i {
	font-size: 20px;
	vertical-align: middle;
}

header .button-basket i {
	font-size: 20px;
}

/* --------------------------------------------------
   NAVIGATION
-------------------------------------------------- */
nav {
	background-color: transparent;
	box-shadow: none;
	display: inline-block;
	margin-top: 15px;
	padding: 0;
	vertical-align: top;
	width: 100%;
}

nav,
nav .nav-wrapper i,
nav a.button-collapse,
nav a.button-collapse i {
	height: 45px;
	line-height: 45px;
}

.button-collapse {
	background-color: <?php print($tertiary); ?>;
    border: solid 1px #000000;
    border-radius: 4px;
    color: <?php print($base); ?>;
    display: none;
    height: 40px;
    line-height: 36px;
    text-align: center;
    width: 40px;
}

.button-collapse:hover {
	background-color: #000000;
}

nav ul {
	margin-left: -20px;
	margin-right: -20px;
	vertical-align: top;
}

ul.social {
	margin-left: -8px;
	margin-right: -8px;
}

nav ul li {
	float: left;
	padding: 0 20px;
	position: relative;
}

ul.social li {
	padding: 0 8px;
}

ul.offer li {
	background-color: #E91E63;
	padding: 0 16px;
	position: relative;
	z-index: 1010;
}

ul.offer li:after {
	background-color: #E91E63;
    bottom: -10px;
    content: "";
    height: 20px;
    left: 0;
    position: absolute;
    transform: rotate(2deg);
    width: 100%;
    width: calc(100% - 1px);
    z-index: -1;
}

nav ul li.active {
	background-color: transparent;
}

nav ul a {
	color: <?php print($tertiary); ?>;
	display: inline-block;
	font-size: 15px;
	font-weight: 500;
	letter-spacing: .03em;
	padding: 0;
	position: relative;
	text-decoration: none;
	width: 100%;
}

nav ul a.disabled {
	background-color: #B5DEFF;
    border: 1px solid rgba(0, 0, 0, 0.05);
	border-radius: 4px;
	color: <?php print($tertiary); ?>;
	font-size: 13px;
	height: 32px;
	line-height: 32px;
	padding: 0 10px;
	vertical-align: top;
}

nav ul a.disabled:hover {
	background-color: #B5DEFF;
	color: <?php print($tertiary); ?>;
	text-decoration: underline;
}

nav ul a.disabled i {
	font-size: 18px;
	height: 32px;
	line-height: 32px;
}

ul.social a {
	font-size: 15px;
	letter-spacing: 0;
}

ul.social a {
	font-size: 14px;
	letter-spacing: 0;	
}

nav ul a.basket {
	color: #F44336;
}

nav ul li.active a {
	color: <?php print($primary); ?>;
}

nav ul a:hover {
	background-color: transparent;
	color: <?php print($primary); ?>;
}

nav ul a.basket:hover {
	color: #B40720;
}

nav ul:not(.social) li:not(:first-child) a:before,
nav ul:not(.dropdown-content) li:not(:first-child) a:before {
	content: "";
    width: 1px;
    height: 24px;
    background-color: #e6e6e6;
    position: absolute;
    top: 50%;
    -webkit-transform: translate(0,-50%);
    -khtml-transform: translate(0,-50%);
    -moz-transform: translate(0,-50%);
    -ms-transform: translate(0,-50%);
    -o-transform: translate(0,-50%);
    transform: translate(0,-50%);
    left: -20px;
}

nav ul:not(.social) li:last-child a:after,
nav ul:not(.dropdown-content) li:last-child a:after {
	left: auto;
	right: -20px;
}

nav ul li.badge a:after {
	content: "Shop";
    display: block;
    text-transform: uppercase;
    font-size: 9px;
    position: relative;
    background: #3F51B5;
    padding: 0 5px;
    text-align: center;
    line-height: 18px;
    font-weight: 500;
    border-radius: 3px;
    color: #fff;
    text-transform: uppercase;
    display: inline-block;
    vertical-align: middle;
    margin-left: 7px;
}

nav ul li.badge.featured a:after {
	content: "Featured";
}

nav ul li a i {
	font-size: 22px;
}

nav ul li a i.left {
	margin-right: 5px;
}

ul.social i {
	background-color: <?php print($primary); ?>;
	border-radius: 50%;
	color: <?php print($base); ?>;
	display: inline-block;
    font-size: 12px;
    height: 25px;
    line-height: 25px;
	margin-right: 10px;
    text-align: center;
	vertical-align: middle;
    width: 25px;
}

ul.social .facebook:hover {
	color: #3B5998;
}

ul.social .facebook i {
	background-color: #3B5998;
	color: #FFFFFF;
}

ul.social .twitter:hover {
	color: #55ACEE;
}

ul.social .twitter i {
	background-color: #55ACEE;
	color: #FFFFFF;
}

ul.social .instagram:hover {
    color: #BC3199;
}

ul.social .instagram i {
	background: radial-gradient(circle at 33% 100%,#fed373 4%,#f15245 30%,#d92e7f 62%,#9b36b7 85%,#515ecf);
    color: #FFFFFF;
}

ul.social .google:hover {
	color: #DC4E41;
}

ul.social .google i {
	background-color: #DC4E41;
    color: #FFFFFF;
}

ul.social span {
	display: inline-block;
	font-size: 14px;
	vertical-align: middle;
}

nav .dropdown-trigger {
	background-color: transparent;
    box-shadow: none;
    color: <?php print($tertiary); ?>;
    font-weight: 500;
    height: 45px;
    line-height: 45px;
	outline: none;
    padding: 0;
    vertical-align: top;
}

nav .dropdown-trigger:hover,
nav .dropdown-trigger:focus,
nav .dropdown-trigger.active {
	background-color: transparent;
}

nav .dropdown-trigger i {
	font-size: 14px;
	margin-right: 5px;
}

nav .dropdown-trigger i.right {
	margin-left: 5px;
	margin-right: 0;
}

nav .dropdown-content {
	background-color: <?php print($base); ?>;
	border-radius: 5px;
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.09);
	margin-top: 0;
	padding: 10px;
	top: calc(100% - 1px) !important;
	width: auto !important;
}

nav .dropdown-content ul {
	margin-left: 0;
	margin-right: 0;
}

nav .dropdown-content li {
	margin-bottom: 8px;
	padding: 0;
}

nav .dropdown-content li:last-child {
	margin-bottom: 0;
}

nav .dropdown-content li a {
	border-radius: 4px;
	color: <?php print($tertiary); ?>;
    font-size: 16px;
	padding: 7px 12px;
	position: relative;
	white-space: nowrap;
}

nav .dropdown-content li a:hover {
	background-color: <?php print($primary); ?>;
	color: <?php print($base); ?>;
}

nav .dropdown-content li a:before {
	content: "-";
	float: left;
	margin-right: 7px;
	position: relative;
}

nav .dropdown-content li a:after {
    color: <?php print($base); ?>;
	content: "\f301";
    font-family: 'Material-Design-Iconic-Font';
	margin-left: 7px;
    position: relative;
    -webkit-transition: right 0.2s cubic-bezier(0.39, 0.575, 0.565, 1) 0s;
    -khtml-transition: right 0.2s cubic-bezier(0.39, 0.575, 0.565, 1) 0s;
    -moz-transition: right 0.2s cubic-bezier(0.39, 0.575, 0.565, 1) 0s;
    -ms-transition: right 0.2s cubic-bezier(0.39, 0.575, 0.565, 1) 0s;
    -o-transition: right 0.2s cubic-bezier(0.39, 0.575, 0.565, 1) 0s;
    transition: right 0.2s cubic-bezier(0.39, 0.575, 0.565, 1) 0s;
}

/* --------------------------------------------------
   SIDE NAVIGATION
-------------------------------------------------- */
.sidenav {
	background-color: <?php print($base); ?>;
	display: flex;
    flex-direction: column;
	height: 100%;
	padding: 20px !important;
	z-index: 1002;
}

.sidenav ul {
	flex: 1 0 auto;
	margin: 0;
}

.sidenav li {
	border-bottom: none;
	border-radius: 4px;
}

.sidenav li > a {
	padding: 0 20px;
	text-decoration: none;
}

.sidenav .copy {
	line-height: 20px;
	margin-bottom: 0;
}

.sidenav .copy a {
	padding: 0;
}

.sidenav-overlay {
	z-index: 1001
}

/* --------------------------------------------------
   TOP
-------------------------------------------------- */
.top {
	background-color: <?php print($tertiary); ?>;
	color: <?php print($base); ?>;
	height: 43px;
	line-height: 43px;
	position: relative;
}

.top ul {
	display: inline-block;
	margin: 0;
	vertical-align: top;
}

.top ul li {
	float: left;
	font-size: 13px;
}

.top ul:not(.social) li a {
	color: <?php print($base); ?>;
	display: inline-block;
	height: 43px;
	line-height: 43px;
	margin: 0 16px;
	padding: 0;
	position: relative;
	text-decoration: none;
	vertical-align: top;
}

.top ul li:last-child a {
	margin-right: 0;
}

.top ul li:not(:last-child) a:before {
	content: "";
    width: 1px;
    height: 13px;
    background-color: #e7e7e7;
	pointer-events: none;
    position: absolute;
    top: 50%;
    -webkit-transform: translate(0,-50%);
    -khtml-transform: translate(0,-50%);
    -moz-transform: translate(0,-50%);
    -ms-transform: translate(0,-50%);
    -o-transform: translate(0,-50%);
    transform: translate(0,-50%);
    right: -16px;
}

.top ul li a:hover {
	color: <?php print($primary); ?>;
}

.top ul:not(.social) li a.icon {
	border: none;
	border-radius: 50%;
	box-shadow: 0rem 0.15rem 0rem 0rem rgba(0,0,0,0.1);
	height: 25px;
	line-height: 25px;
	margin: 10px 0 0 15px;
	padding: 0;
	text-align: center;
	width: 25px;
}

.top ul:not(.social) li a i {
	font-size: 12px;
	height: 25px;
	line-height: 25px;
    vertical-align: top;
}

.top ul li a img {
	height: 22px;
	margin-right: 10px;
	vertical-align: middle;
	width: 22px;
}

/* --------------------------------------------------
   DETECTOR
-------------------------------------------------- */
.detector {
	background-color: transparent;
    height: 8px;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
	z-index: 1020;
}

/* --------------------------------------------------
   COLLECTION MENU
-------------------------------------------------- */
.collection-menu {
	margin: 0 0 10px;
}

.collection-menu h5 {
	background-color: <?php print($base); ?>;
    border: 1px solid #EFEFEF;
    border-radius: 4px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.05);
    color: <?php print($tertiary); ?>;
	margin-bottom: 10px;
	overflow: hidden;
}

.collection-menu h5 a {
	color: inherit;
}

.collection-menu h5 i {
	font-size: 22px;
	line-height: 46px;
	margin-right: 15px;
	vertical-align: middle;
}

.collection-menu h5 span {
	border-bottom: solid 5px <?php print($primary); ?>;
	display: inline-block;
	height: 46px;
	line-height: 46px;
	padding: 0 20px;
	vertical-align: top;
}

.collection-menu ul {
	background-color: <?php print($base); ?>;
    border: 1px solid #EFEFEF;
    border-radius: 4px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.05);
	height: 394px;
	margin: 0;
	padding: 10px;
}

.collection-menu ul li {
	line-height: 30px;
	margin-bottom: 8px;
}

.collection-menu ul li:last-child {
	margin-bottom: 0;
}

.collection-menu ul li a {
	border-radius: 4px;
	color: <?php print($tertiary); ?>;
	display: inline-block;
	font-size: 15px;
	font-weight: 500;
	height: 30px;
	line-height: 30px;
    padding: 0 12px;
	vertical-align: top;
	width: 100%;
}

.collection-menu ul li a.highlight {
	background-color: #F1F1F1;
}

.collection-menu ul li a:hover {
	background-color: <?php print($primary); ?>;
	color: <?php print($base); ?>;
}

.collection-menu ul li a i {
	font-size: 14px;
	line-height: 30px;
}

/* --------------------------------------------------
   SHARE BUTTONS
-------------------------------------------------- */
.sharethis-inline-share-buttons {
	margin-bottom: 40px;
}

/* --------------------------------------------------
   AUTOCOMPLETE
-------------------------------------------------- */
.autocomplete-content {
	margin-top: 0;
	margin-bottom: 0
}

.input-field .prefix ~ .autocomplete-content {
	margin-left: 3rem;
	width: 92%;
	width: calc(100% - 3rem);
}

.autocomplete-content {
	display: block;
	margin-top: -20px;
	margin-bottom: 20px;
	opacity: 1;
	position: static;
}

.autocomplete-content li .highlight {
	color: #444;
}

.autocomplete-content li img {
	height: 40px;
	margin: 5px 15px
	width: 40px;
}

/* --------------------------------------------------
   BREADCRUMB
-------------------------------------------------- */
.breadcrumb {
	height: 20px;
	margin: 0 0 20px;
}

.breadcrumb:before {
	display: none;
}

.breadcrumb ol,
.breadcrumb ul {
	display: inline-block;
	margin: 0;
}

.breadcrumb li {
	float: left;
	font-size: 15px;
	height: 20px;
	line-height: 20px;
    color: <?php print($tertiary); ?>;
}

.breadcrumb li:last-child {
	pointer-events: none;
}

.breadcrumb li:after {
    color: <?php print($tertiary); ?>;
	content: '\E5CC';
    display: inline-block;
    font-family: 'Material Icons';
    font-weight: normal;
    font-style: normal;
    font-size: 18px;
    margin: 0 5px 0;
    vertical-align: top;
    -webkit-font-smoothing: antialiased;
}

.breadcrumb li:last-child:after {
	display: none;
}

.breadcrumb li a {
	color: <?php print($tertiary); ?>;
	text-decoration: none;
}

.breadcrumb li a:hover {
	color: <?php print($primary); ?>;
}

.breadcrumb i,
.breadcrumb [class^="mdi-"],
.breadcrumb [class*="mdi-"],
.breadcrumb i.material-icons {
	font-size: 15px;
    height: 20px;
    line-height: 16px;
}

/* --------------------------------------------------
   PAGINATION
-------------------------------------------------- */
.pagination {
	display: inline-block;
	margin: 0 -5px;
}

.pagination li {
	background-color: transparent;
	height: 30px;
	margin: 0 5px;
}

.pagination li.active {
	background-color: transparent;
}

.pagination li a {
	border: solid 2px transparent;
	border-radius: 50%;
	font-size: 18px;
	height: 30px;
	line-height: 26px;
	opacity: .4;
	padding: 0;
	text-decoration: none;
	width: 30px;
}

.pagination li a:hover {
	opacity: 1;
}

.pagination li.active a {
    background-color: transparent;
	border-color: <?php print($primary); ?>;
	color: <?php print($primary); ?>;
    font-weight: 900;
	opacity: 1;
}

.pagination li.jplist-prev a,
.pagination li.jplist-next a {
	background-color: <?php print($primary); ?>;
	border: solid 1px rgba(0,0,0,.05);
    border-radius: 30px;
    color: <?php print($base); ?>;
    opacity: 1;
    padding: 0 10px;
    width: auto;
}

.pagination li.jplist-prev a:hover,
.pagination li.jplist-next a:hover {
	background-color: #7DBB4C;
    color: <?php print($base); ?>;
 
}

.pagination li.active a:hover {
	opacity: 1;
}

.pagination li i {
	font-size: 18px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	width: 30px;
}

/* --------------------------------------------------
   DROPDOWN
-------------------------------------------------- */
.dropdown-wrapper {
	display: inline-block;
	position: relative;
	vertical-align: middle;
}

.dropdown-trigger {
	background-color: rgba(153,153,153,0.2);
	border: none;
	box-shadow: 0rem 0.15rem 0rem 0rem rgba(0,0,0,0.1);
    color: #000000;
    display: inline-block;
    font-weight: 900;
    height: 28px;
    line-height: 28px;
    padding: 0 10px;
	text-decoration: none;
}

.dropdown-trigger:hover,
.dropdown-trigger:focus,
.dropdown-trigger.active {
	background-color: rgba(153,153,153,0.3);
}

.dropdown-trigger i {
	font-size: 18px;
	height: 28px;
	float: left;
	line-height: 28px;
	margin-left: 0;
	margin-right: 5px;
}

.dropdown-trigger i.right {
	float: right;
	margin-left: 5px;
	margin-right: 0;
}

.dropdown-content {
	background-color: #F6F6F6;
	margin-top: 5px;
}

.dropdown-content.basket {
	background-color: <?php print($base); ?>;
	border: 1px solid #E8E9ED;
	border-radius: 4px;
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.09);
	left: 50% !important;
	margin: 11px 0 0;
	padding: 30px;
	transform: translateX(-50%) !important;
	-webkit-transform: translateX(-50%) !important;
	width: 370px !important;
}

.dropdown-content.basket:before {
    background-image: url(/themeforest/raworganic/wp-content/themes/raworganic/assets/images/cart-arrow.png);
	content: '';
    height: 11px;
    left: 0;
    margin: auto;
    position: absolute;
    right: 0;
    top: -11px;
    width: 21px;
}

.dropdown-content li {
	min-height: 30px;
}

.dropdown-content li > a,
.dropdown-content li > span {
	color: #000000;
	font-size: 15px;
	font-weight: normal;
	padding: 5px 10px;
	text-decoration: none;
}

.dropdown-content li > a:hover,
.dropdown-content li > span:hover {
	background-color: #EEEEEE;
	color: #000000;
}

.nice-select {
	border: solid 1px #EFEFEF;
	border-radius: 4px;
	font-size: 16px;
	height: 50px;
	line-height: 50px;
    padding-left: 20px;
    padding-right: 20px;
	width:100%;
}

.nice-select:hover {
	border-color: #EFEFEF;
}

.nice-select:active,
.nice-select.open,
.nice-select:focus {
	border-color: #EFEFEF;
}

.nice-select:after {
	border: none;
    color: <?php print($tertiary); ?>;
    content: "\f312";
    font-family: "Material-Design-Iconic-Font";
    font-size: 16px;
    height: auto;
    margin: 0;
    opacity: .66;
    position: absolute;
    right: 24px;
    top: 50%;
    -webkit-transform: translate(0px,-50%);
    -khtml-transform: translate(0px,-50%);
    -moz-transform: translate(0px,-50%);
    -ms-transform: translate(0px,-50%);
    -o-transform: translate(0px,-50%);
    transform: translate(0px,-50%);
    width: auto;
}

.nice-select.open:after {
	-webkit-transform: translate(0px,-50%);
    -khtml-transform: translate(0px,-50%);
    -moz-transform: translate(0px,-50%);
    -ms-transform: translate(0px,-50%);
    -o-transform: translate(0px,-50%);
    transform: translate(0px,-50%);
}

.nice-select .list {
	max-height: 250px;
	min-width: 185px;
	overflow: auto;
	padding: 5px;
	z-index: 100;
}

.nice-select .option {
	border-radius: 4px;
	line-height: 35px;
	margin-bottom: 0;
	min-height: 35px;
	padding: 0 24px;
}

.nice-select .option:hover,
.nice-select .option.focus,
.nice-select .option.selected.focus {
	background-color: #F3F3F4;
}

/* --------------------------------------------------
   DECORATIONS
-------------------------------------------------- */
.decor-top {
	box-shadow: 0 3px 6px rgba(0, 0, 0, 0.6);
}

.decor-top:before,
.decor-bottom:after,
.decor-both:before,
.decor-both:after {
	background-repeat: no-repeat;
    content: "";
    height: 65px;
    left: 0;
    margin: 0 auto;
    position: absolute;
    right: 0;
    z-index: 500;
}

.decor-top:before,
.decor-both:before {
    background-image: url(/images/elements/decoration-top.png);
    background-position: top right;
    top: -65px;
}

.decor-top.decor-colour:before,
.decor-both.decor-colour:before {
	background-image: url(/images/elements/decoration-bottom-colour.png);
}

.decor-bottom:after,
.decor-both:after {
    background-image: url(/images/elements/decoration-top.png);
    background-position: top center;
    bottom: -65px;
}

.decor-bottom.decor-colour:after,
.decor-both.decor-colour:after {
	background-image: url(/images/elements/decoration-top-colour.png);
}

/* --------------------------------------------------
   NOTIFICATIONS
-------------------------------------------------- */
.notification {
	background-color: <?php print($primary); ?>;
	box-shadow: 0 -3px 6px rgba(0,0,0,0.05) inset;
	color: <?php print($base); ?>;
	padding: 15px 0;
	position: relative;
}

.notification.remove {
	display: none;
}

.notification .container {
	position: relative;
}

.notification p {
	font-size: 14px;
	margin: 0;
	padding-right: 45px;
}

.notification a {
	color: <?php print($base); ?>;
}

.notification .close {
	background-color: rgba(255,255,255,0.2);
    border: none;
    border-radius: 50%;
    box-shadow: 0 0 0 black;
    color: #FFFFFF;
    cursor: pointer;
    height: 30px;
    line-height: 8px;
    position: absolute;
    right: 0;
    text-align: center;
    top: -4px;
    width: 30px;
}

.notification .close i {
	font-size: 14px;
	font-weight: 900;
}

/* --------------------------------------------------
   FILTER
-------------------------------------------------- */
.filter {
	
}

.filter .collapsible {
	background-color: transparent;
	border: none;
	box-shadow: none;
	margin: 0;
}

.faqs .collapsible {
	background-color: <?php print($base); ?>;
    border: 1px solid #EFEFEF;
	border-radius: 4px;
	box-shadow: none;
	margin: 0 0 30px;
}

.filter .collapsible li {
	border-bottom: 1px solid #DDDDDD;
	margin: 0 0 20px;
	padding: 0 0 10px;
}

.filter .option {
    display: inline-block;
    font-size: 15px;
    line-height: 30px;
    margin: 0 0 5px;
    padding-left: 35px;
    position: relative;
    vertical-align: top;
    width: 100%;
}

.filter .option a {
	color: <?php print($tertiary); ?>;
	text-decoration: underline;
}

.filter .option img {
    left: 0;
    position: absolute;
    top: 0;
}

.collapsible-header {
	background-color: transparent;
	border-bottom: none;
	font-weight: 900;
	min-height: 30px;
	line-height: 30px;
	padding: 0;
	position: relative;
	text-transform: uppercase;
}

.faqs .collapsible-header {
	border-bottom: 1px solid #EFEFEF;
	font-size: 20px;
	font-weight: normal;
	line-height: 30px;
    padding: 15px 80px 15px 30px;
	text-transform: none;
}

.faqs .collapsible-header:after {
	content: "\F275";
	font-family: 'Material-Design-Iconic-Font';
	font-size: 24px;
	position: absolute;
	right: 30px;
	top: 50%;
	transform: translateY(-50%);
	-webkit-transform: translateY(-50%);
}

.faqs li.active .collapsible-header:after {
	content: "\F271";
}

.collapsible-header i {
	height: 30px;
    line-height: 30px;
    margin: 0;
	position: absolute;
	right: 0;
    width: auto;
}

.collapsible-body {
	border-bottom: none;
	margin: 0;
    padding: 10px 0;
}

.faqs .collapsible-body {
	border-bottom: 1px solid #EFEFEF;
    background-color: #F8F8F8;
    padding: 33px 30px;
}

.faqs .collapsible-body p:last-child {
	margin-bottom: 0;
}

.collapsible-body .input-field {
	margin: 0;
}

/* --------------------------------------------------
   SIDEBAR ELEMENTS
-------------------------------------------------- */
.help,
.logout,
.favourite {
	background-color: #5B6ABF;
	background-image: url(/images/elements/contact-us.jpg);
	background-position: center center;
	background-size: cover;
	border-radius: 5px;
	box-shadow: 0 3px 6px rgba(0,0,0,0.06);
	color: #FFFFFF;
	margin: 0 0 10px;
	padding: 30px;
	position: relative;
}

.logout {
	background-color: transparent;
	background-image: none;
	box-shadow: none;
	padding: 0;
}

.favourite {
	background-color: #F44336;
	background-image: url(/images/elements/favourites.jpg);
	color: #FFFFFF;
}

.help p,
.favourite p {
	font-size: 16px;
}

.help i,
.favourite i {
	color: #EBEBEB;
    font-size: 16px;
    position: absolute;
    right: 30px;
    top: 30px;
	transition: all .2s;
    -webkit-transition: all .2s;
    will-change: color,transform;
}

.help i:hover,
.favourite i:hover {
    transform: scale(1.15);
    -webkit-transform: scale(1.15);
}

.help i:hover {
	color: #5B6ABF;
}

.favourite i:hover {
	color: #F44336;
}

/* --------------------------------------------------
   MODALS
-------------------------------------------------- */
.modal {
	background-color: transparent;
	border-radius: 5px;
	box-shadow: none;
	top: 50% !important;
	transform: translateY(-50%) !important;
	-webkit-transform: translateY(-50%) !important;
	width: 45%;
}

.modal.small {
	max-width: 430px;
	width: 100%;
}

.modal.large {
	width: 55%;
}

.modal .modal-content {
	background-color: <?php print($base); ?>;
	box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05);
	padding: 33px;
}

.modal.image .modal-content {
	background-image: url(/images/elements/newsletter.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-size: cover;
	color: <?php print($base); ?>;
}

.modal.image.offer .modal-content {
	background-image: url(/images/elements/offer.jpg);
}

.modal .modal-content h3,
.modal .modal-content .h3 {
	margin-bottom: 34px;
	text-align: center;
}

.modal .modal-footer {
    background-color: #F8F8F8;
	border-radius: 0 0 5px 5px;
	border-top: solid 1px #EFEFEF;
	box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05);
    font-size: 15px;
    padding: 18px 30px;
	text-align: center;
}

.modal .modal-footer a {
	color: <?php print($primary); ?>;
}

.modal .modal-under {
	color: <?php print($base); ?>;
	font-size: 16px;
	padding: 25px 15px 0;
	text-align: center;
}

.modal .modal-under a {
	opacity: .8;
}

.modal .modal-under a:hover {
	opacity: 1;
}

/* --------------------------------------------------
   DATEPICKER
-------------------------------------------------- */
.datepicker-modal .datepicker-container {
	padding: 0;
}

.datepicker-date-display {
	background-image: url(/images/gallery/contact-us.jpg);
	background-position: center center;
	background-size: cover;
	position: relative;
}

.datepicker-date-display:before {
	background-color: <?php print($secondary); ?>;
	bottom: 0;
	content: "";
	left: 0;
	opacity: .8;
	position: absolute;
	right: 0;
	top: 0;
}

.datepicker-date-display span {
	position: relative;
}

.datepicker-day-button:focus {
	background-color: rgba(219, 7, 35, 0.15);
}

.datepicker-table td.is-selected {
	background-color: <?php print($tertiary); ?>;
}

.datepicker-table td.is-today {
	color: <?php print($tertiary); ?> !important;
}

.datepicker-controls {
	padding: 0 5px 10px;
}

.datepicker-calendar-container {
	padding: 20px;
}

.month-prev,
.month-next {
	background-color: rgba(153,153,153,0.3);
	border-radius: 50%;
	height: 35px;
	line-height: 35px;
	margin-top: 0;
	padding: 5px 0 0;
	width: 35px;
}

.datepicker-footer {
	display: none;
}

.datepicker-controls .select-month input.select-dropdown,
.datepicker-controls .select-year input.select-dropdown {
	background-color: transparent;
	border: none;
	font-size: 18px;
	height: 35px;
	line-height: 35px;
	padding: 0;
	pointer-events: none;
	width: 100px;
}

.datepicker-controls .select-year input.select-dropdown {
	width: 50px;
}

/* --------------------------------------------------
   BANNER
-------------------------------------------------- */
.banner {
	background-color: #B5DEFF;
    border: 1px solid rgba(0, 0, 0, 0.05);
    border-radius: 4px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.05);
    display: inline-block;
	margin: 0 0 10px;
    padding: 10px 0;
    vertical-align: top;
    width: 100%;
}

.banner ul {
	margin: 0;
}

.banner ul li {
    float: left;
	font-weight: 500;
	padding: 0 20px;
	position: relative;
    text-align: left;
	width: 33.33%;
}

.banner ul li:not(:first-child):before {
	content: "";
    width: 1px;
    height: 24px;
    background-color: rgba(0, 0, 0, 0.1);
    position: absolute;
    top: 50%;
    -webkit-transform: translate(0,-50%);
    -khtml-transform: translate(0,-50%);
    -moz-transform: translate(0,-50%);
    -ms-transform: translate(0,-50%);
    -o-transform: translate(0,-50%);
    transform: translate(0,-50%);
    left: 0;
}

.banner ul li:last-child:after {
	left: auto;
	right: 0;
}

.banner ul li svg {
	fill: <?php print($tertiary); ?>;
	float: left;
	height: 35px;
	margin: 5px 15px 5px 0;
	width: 35px;
}

.banner ul li h5,
.banner ul li span {
	display: block;
    font-size: 13px;
    font-weight: 400;
    line-height: 16px;
	margin: 0;
}

.banner ul li h5 {
	font-size: 16px;
	font-weight: 500;
	line-height: 20px;
	margin-top: 6px;
}

/* --------------------------------------------------
   BLOCK
-------------------------------------------------- */
.section {
	background-color: <?php print($base); ?>;
    border: 1px solid #EFEFEF;
    border-radius: 7px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.05);
    display: inline-block;
    margin: 0 0 20px;
    padding: 20px;
    vertical-align: top;
    width: 100%;
}

/* --------------------------------------------------
   PRODUCT SEARCH
-------------------------------------------------- */
.search {
    align-items: center;
	display: flex;
    flex-wrap: nowrap;
	float: right;
	margin: 0;
}

.search form {
    align-items: center;
	border-radius: 4px;
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
	display: flex;
    flex-wrap: nowrap;
	margin: 0 15px;
}

.search form .nice-select {
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
	font-size: 15px;
	height: 45px;
	line-height: 45px;
	min-width: 220px;
	padding-left: 15px;
	padding-right: 15px;
}

.search form .input-field {
	flex-grow: 1;
	margin: 0 0 0 -2px;
    position: relative;
}

.search form .input-field input {
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
	font-weight: 400;
	height: 45px;
	min-width: 283px;
}

.search input:not([type]):focus:not([readonly]),
.search input[type=text]:not(.browser-default):focus:not([readonly]),
.search input[type=password]:not(.browser-default):focus:not([readonly]),
.search input[type=email]:not(.browser-default):focus:not([readonly]),
.search input[type=url]:not(.browser-default):focus:not([readonly]),
.search input[type=time]:not(.browser-default):focus:not([readonly]),
.search input[type=date]:not(.browser-default):focus:not([readonly]),
.search input[type=datetime]:not(.browser-default):focus:not([readonly]),
.search input[type=datetime-local]:not(.browser-default):focus:not([readonly]),
.search input[type=tel]:not(.browser-default):focus:not([readonly]),
.search input[type=number]:not(.browser-default):focus:not([readonly]),
.search input[type=search]:not(.browser-default):focus:not([readonly]),
.search textarea.materialize-textarea:focus:not([readonly]) {
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}

.search form .btn.isolate svg {
	display: block;
}

/* --------------------------------------------------
   FORM ELEMENTS
-------------------------------------------------- */
form.apply,
form.contact,
form.account,
form.checkout {
	padding: 20px 0;
}

form.contact,
form.account {
	background-color: <?php print($base); ?>;
	border-radius: 5px;
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.12);
	padding: 30px;
}

form.apply h3,
form.apply .h3,
form.contact h3,
form.contact .h3,
form.account h3,
form.account .h3,
form.checkout h3,
form.checkout .h3 {
	margin-bottom: 10px;
}

form.apply .row,
form.contact .row,
form.account .row,
form.checkout .row {
	margin-left: -5px;
	margin-right: -5px;
}

form.apply .col,
form.contact .col,
form.account .col,
form.checkout .col {
	padding-left: 5px;
	padding-right: 5px;
}

form.apply .input-field span:not(.lever),
form.contact .input-field span:not(.lever),
form.account .input-field span:not(.lever),
form.checkout .input-field span:not(.lever) {
	height: auto;
    line-height: 20px;
    margin-top: 5px;
}

form.apply [type="checkbox"].filled-in:checked + span:not(.lever):before,
form.contact [type="checkbox"].filled-in:checked + span:not(.lever):before,
form.account [type="checkbox"].filled-in:checked + span:not(.lever):before,
form.checkout [type="checkbox"].filled-in:checked + span:not(.lever):before {
	top: 2px;
}

form.apply [type="checkbox"].filled-in:not(:checked) + span:not(.lever):after,
form.contact [type="checkbox"].filled-in:not(:checked) + span:not(.lever):after,
form.account [type="checkbox"].filled-in:not(:checked) + span:not(.lever):after,
form.checkout [type="checkbox"].filled-in:not(:checked) + span:not(.lever):after {
	top: 0;
}

form.apply [type="checkbox"].filled-in:checked + span:not(.lever):after,
form.contact [type="checkbox"].filled-in:checked + span:not(.lever):after,
form.account [type="checkbox"].filled-in:checked + span:not(.lever):after,
form.checkout [type="checkbox"].filled-in:checked + span:not(.lever):after {
	top: 0;
}


.input-field {
	margin-bottom: 0;
	margin-top: 15px;
}

.input-field label {
	color: #222222;
    display: inline-block;
    font-size: 15px;
    margin: 0 0 5px;
    position: relative;
    top: 0;
    transform: none;
    -webkit-transform: none;
    width: 100%;
}

.input-field label:not(.label-icon).active {
	font-size: 15px;
	transform: none;
	-webkit-transform: none;
}

input:not([type]):focus:not([readonly]),
input[type=text]:focus:not([readonly]),
input[type=password]:focus:not([readonly]),
input[type=email]:focus:not([readonly]),
input[type=url]:focus:not([readonly]),
input[type=time]:focus:not([readonly]),
input[type=date]:focus:not([readonly]),
input[type=datetime]:focus:not([readonly]),
input[type=datetime-local]:focus:not([readonly]),
input[type=tel]:focus:not([readonly]),
input[type=number]:focus:not([readonly]),
input[type=search]:focus:not([readonly]),
textarea.materialize-textarea:focus:not([readonly]) {
	box-shadow: none;
	border: solid 1px <?php print($tertiary); ?>;
}

input:not([type]):focus:not([readonly]),
input[type=text]:not(.browser-default):focus:not([readonly]),
input[type=password]:not(.browser-default):focus:not([readonly]),
input[type=email]:not(.browser-default):focus:not([readonly]),
input[type=url]:not(.browser-default):focus:not([readonly]),
input[type=time]:not(.browser-default):focus:not([readonly]),
input[type=date]:not(.browser-default):focus:not([readonly]),
input[type=datetime]:not(.browser-default):focus:not([readonly]),
input[type=datetime-local]:not(.browser-default):focus:not([readonly]),
input[type=tel]:not(.browser-default):focus:not([readonly]),
input[type=number]:not(.browser-default):focus:not([readonly]),
input[type=search]:not(.browser-default):focus:not([readonly]),
textarea.materialize-textarea:focus:not([readonly]) {
	box-shadow: none;
	border: solid 1px <?php print($tertiary); ?>;
}

.input-field input[type=text],
.input-field input[type=password],
.input-field input[type=email],
.input-field input[type=url],
.input-field input[type=time],
.input-field input[type=date],
.input-field input[type=datetime],
.input-field input[type=datetime-local],
.input-field input[type=tel],
.input-field input[type=number],
.input-field input[type=search],
.input-field select,
.input-field textarea {
	background-color: <?php print($base); ?>;
	border: solid 1px #EFEFEF;
    border-radius: 4px;
	box-sizing: border-box;
	color: rgba(0,0,0,0.66);
	font-size: 16px;
	font-weight: 500;
	height: 45px;
	margin-bottom: 0;
	outline: none;
	padding: 0 15px;
	position: relative;
	transition: border .3s;
	-webkit-transition: border .3s;
}

.input-field input[type=text]:disabled,
.input-field input[type=password]:disabled,
.input-field input[type=email]:disabled,
.input-field input[type=url]:disabled,
.input-field input[type=time]:disabled,
.input-field input[type=date]:disabled,
.input-field input[type=datetime]:disabled,
.input-field input[type=datetime-local]:disabled,
.input-field input[type=tel]:disabled,
.input-field input[type=number]:disabled,
.input-field input[type=search]:disabled,
.input-field select:disabled,
.input-field textarea:disabled {
	border: solid 1px #e0e0e0;
    color: rgba(0,0,0,0.42);
}

.input-field input[type=text].large,
.input-field input[type=password].large,
.input-field input[type=email].large,
.input-field input[type=url].large,
.input-field input[type=time].large,
.input-field input[type=date].large,
.input-field input[type=datetime].large,
.input-field input[type=datetime-local].large,
.input-field input[type=tel].large,
.input-field input[type=number].large,
.input-field input[type=search].large,
.input-field select.large,
.input-field textarea.large {
	height: 60px;
}

.input-field h4 input[type=text],
.input-field .h4 input[type=text],
.input-field h4 select,
.input-field .h4 select {
	font-size: 18px;
	margin: 0 5px;
	width: auto;
}

.input-field select {
	background-image: url(/images/elements/dropdown.png);
	background-position: right 10px center;
	background-repeat: no-repeat;
	background-size: 15px;
	height: 3rem;
    opacity: 1;
	pointer-events: auto;
    position: relative;
    width: 100%;
	-webkit-appearance:none;
}

.input-field textarea {
	height: 90px;
	max-width: 100%;
	max-height: 500px;
	min-height: 90px;
	min-width: 100%;
	outline: none;
	padding: 10px;
	transition: border .3s;
	-webkit-transition: border .3s;
	width: 100%;
}

.input-field select:focus,
.input-field textarea:focus {
	box-shadow: none;
	border: solid 1px #222222;
}

.input-field input::-webkit-input-placeholder,
.input-field textarea::-webkit-input-placeholder {
	color: rgba(0, 0, 0, .66);
	text-transform: none;
}

.input-field input::-moz-placeholder,
.input-field textarea::-moz-placeholder {
	color: rgba(0, 0, 0, .66);
	text-transform: none;
}

.input-field input:-ms-input-placeholder,
.input-field textarea:-ms-input-placeholder {
	color: rgba(0, 0, 0, .66);
	text-transform: none;
}

.input-field input:-moz-placeholder,
.input-field textarea:-moz-placeholder {
	color: rgba(0, 0, 0, .66);
	text-transform: none;
}

.input-field input.error,
.input-field select.error,
.input-field textarea.error {
	border-color: #F44336;
}

.input-field input.counter,
.input-field input.total {
	background-color: transparent;
	border-color: transparent;
	display: inline-block;
	height: 25px;
	font-size: 18px;
	font-weight: 500;
	line-height: 21px;
	margin: 0 10px;
	min-width: 25px;
	padding: 0;
	text-align: center;
	vertical-align: middle;
	width: 25px;
}

.input-field input.total {
	width: 80px;
}

.input-field input.datepicker {
	background-image: url(/images/elements/datepicker.png);
	background-position: center right 10px;
	background-repeat: no-repeat;
	background-size: 18px;
}

.input-field input.code {
	text-transform: uppercase;
}

.input-field input.autocomplete,
.input-field select.autocomplete,
.input-field textarea.autocomplete {
	background-color: #F1F4A2;
}

.input-field input.autocomplete.blank,
.input-field select.autocomplete.blank,
.input-field textarea.autocomplete.blank {
	background-color: #FFFFFF;
}

.input-field input.autocomplete:focus {
	border-color: #B0B612;
}

.input-field input.required,
.input-field textarea.required {
	background-image: url(/images/elements/required.png);
	background-position: right 10px top 13px;
	background-repeat: no-repeat;
}

.input-field input.success,
.input-field textarea.success {
	border-color: #8BC34A !important;
}

.input-field input.small {
	height: 30px;
	line-height: 30px;
	padding: 0 15px;
}

.input-field .prefix,
.input-field .postfix {
	position: absolute;
    transition: color .2s;
    width: 3rem;
	z-index: 10;
}

.input-field .prefix,
.input-field .postfix {
    left: 10px;
    top: 5px;
    width: auto;
}

.input-field .prefix {
	left: 5px;
	right: auto;
}

.input-field .postfix {
	left: auto;
	right: 5px;
}

.input-field i.prefix,
.input-field i.postfix {
	border-right: 1px solid #EFEFEF;
	color: <?php print($primary); ?>;
	font-size: 24px;
	line-height: 32px;
	text-align: center;
	top: 9px;
	width: 55px;
}

.input-field i.prefix.active,
.input-field i.postfix.active {
	color: <?php print($tertiary); ?>;
}

.input-field .prefix ~ input,
.input-field .prefix ~ textarea,
.input-field .prefix ~ label,
.input-field .prefix ~ .validate ~ label,
.input-field .prefix ~ .autocomplete-content {
	margin-left: 0;
	padding-left: 75px;
	width: 100%;
}

.input-field .postfix ~ input,
.input-field .postfix ~ textarea,
.input-field .postfix ~ label,
.input-field .postfix ~ .validate ~ label,
.input-field .postfix ~ .autocomplete-content {
	padding-right: 75px;
}

.select-wrapper input.select-dropdown {
	border: 2px solid rgba(153,153,153,0.3);
	margin-bottom: 0;
	padding: 0 10px;
}

.select-wrapper span.caret {
	right: 10px;
}

[type="checkbox"] + span:not(.lever) {
	display: inline-block;
	height: 30px;
	line-height: 30px;
	pointer-events: auto;
	top: 0;
}

[type="checkbox"].filled-in:not(:checked) + span:not(.lever):after {
	border: solid 1px #A6A6A6;
	border-radius: 50%;
	height: 20px;
	top: 5px;
	width: 20px;
}

[type="checkbox"].filled-in:checked + span:not(.lever):after {
	background-color: <?php print($primary); ?>;
	border-color: transparent;
	border-radius: 50%;
	height: 20px;
	top: 5px;
	width: 20px;
}

[type="checkbox"].filled-in + span:not(.lever):before,
[type="checkbox"].filled-in + span:not(.lever):after {
	transition: none;
	-webkit-transition: none;
}

[type="checkbox"].filled-in:checked + span:not(.lever):before {
	height: 12px;
    left: 2px;
    margin-top: 0;
    top: 8px;
    width: 6px;
}

[type="checkbox"].filled-in:not(:checked) + span:not(.lever) .sticker,
[type="checkbox"].filled-in:checked + span:not(.lever) .sticker,
.filter .option img {
	float: left;
	margin: 5px 10px 0 0;
	width: 20px;
}

[type="radio"] + label {
	display: inline-block;
	height: 30px;
	line-height: 30px;
	pointer-events: auto;
	top: 0;
}

[type="radio"].block:not(:checked) + label,
[type="radio"].block:checked + label {
	background-color: #F7F7F7;
    border: solid 2px #E5E5E5;
    height: 40px;
    line-height: 36px;
    margin: 0 -1px 0;
    padding: 0 10px;
	position: relative;
    text-align: center;
    width: 25%;
}

[type="radio"].block:checked + label {
	background-color: #E5E5E5;
    border-color: #D6D6D6;
}

[type="radio"].block:not(:checked)+label:before,
[type="radio"].block:checked + label:before {
	display: none;
}

[type="radio"].block:not(:checked) + label:after,
[type="radio"].block:checked + label:after {
	background-color: #d6d6d6;
    border: none;
    border-radius: 0;
    bottom: 0px;
    content: "";
    height: auto;
    left: auto;
    margin: 0;
    position: absolute;
    right: -2px;
    top: 0px;
    width: 2px;
	z-index: 10;
}

.label-note {
	color: #222222;
	margin: 5px 0 0;
}

.label-note a {
	color: #B0B612;
}

.label-note.overlap {
	box-shadow: 0rem 0.15rem 0rem 0rem rgba(0,0,0,0.1);
    font-size: 14px;
	left: 0;
    line-height: 35px;
    margin: 0px 0 0;
    padding: 0 10px;
	position: absolute;
	right: 0;
}

.label-note.error {
	color: #F44336;
}

.label-note.error a {
	color: #F44336;
	font-weight: 900;
}

.label-note.success {
	color: #8BC34A;
}

.label-note.success a {
	color: #8BC34A;
	font-weight: 900;
}

.label-note.overlap.error {
	background-color: #F44336;
	color: #FFFFFF;
}

.label-note.overlap.success {
	background-color: #8BC34A;
	color: #FFFFFF;
}

.alert {
	display: none;
    font-size: 14px;
    padding: 15px;
}

.alert.active {
	display: block;
}

.alert.edge {
	margin-bottom: 30px;
}

.alert.info {
	background-color: #F1F4A2;
    color: #222222;
}

.alert.warning {
	background-color: #FFE082;
    color: #FF6F00;
}

.alert.error {
	background-color: #FFCED4;
    color: #F44336;
}

.note {
	color: <?php print($secondary); ?>;
	font-size: 15px;
	margin: 20px 0 0;
	line-height: 24px;
}

.note i {
	font-size:22px;
	margin-right: 5px;
	vertical-align: middle;
}

/* --------------------------------------------------
   CONTACT
-------------------------------------------------- */
.contact-info img {
	margin-bottom: 18px;
}

/* --------------------------------------------------
   ADVERTS
-------------------------------------------------- */
.adverts {
	
}

.adverts .advert {
	border-radius: 5px;
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.12);
	padding: 30px;
}

.adverts .advert img {
	float: left;
	width: 38px;
}

.adverts .advert .advert-wrapper {
	padding-left: 68px;
}

.adverts .advert h4 {
	color: <?php print($primary); ?>;
	font-size: 28px;
	font-weight: 900;
}

.adverts .advert p {
	margin: 0;
}

/* --------------------------------------------------
   CARDS
-------------------------------------------------- */
.card {
	background-color: <?php print($primary); ?>;
	border-radius: 4px;
	box-shadow: 0 2px 5px rgba(0,0,0,0.12);
	height: 500px;
	margin: 0 0 10px;
	overflow: hidden;
}

.card.small {
	height: 221px;
}

.card.large {
	height: 452px;
}

.card.auto {
	height: auto;
}

.card.blank {
	background-color: <?php print($base); ?>;
	border: solid 2px #222222;
}

.card.auto-height {
	height: auto;
}

.card.profile {
	border: none;
}

.promo-slider .card {
	border-radius: 0;
	box-shadow: none;
}	

.event .card {
	margin-bottom: 30px;
}
	
.card.profile.blank:before {
	background-image: url(/images/elements/dot.png);
	bottom: 0;
    content: "";
    left: 70px;
    position: absolute;
    top: 0;
    width: 5px;
}

.card .card-image {
	bottom: 0;
	left: 0;
	position: absolute;
	right: 0;
	z-index: 0;
}

.card .profile {
	border: solid 4px <?php print($base); ?>;
    bottom: -20px;
    left: 30px;
    position: absolute;
    width: 90px;
}

.card .card-content {
	background-position: center center;
	background-repeat: no-repeat;
	background-size: cover;
	color: <?php print($base); ?>;
	display: table;
	height: 100%;
	padding: 0;
	width: 100%;
}

.card.main .card-content {
	padding: 60px;
}

.card.blank .card-content {
	color: #222222;
}

.card.profile .card-content {
	padding: 60px 20px 20px 150px;
}

.card.profile.blank .card-content {
	padding: 0 0 0 150px;
}

.card .card-content .wrapper {
	display: table-cell;
	vertical-align: bottom;
	width: 100%;
}

.card.main .card-content .wrapper {
	vertical-align: middle;
}

.card:not(.main) .card-content .wrapper {
	background: -moz-linear-gradient(top, rgba(0,0,0,0) 50%, rgba(0,0,0,0.65) 100%);
	background: -webkit-linear-gradient(top, rgba(0,0,0,0) 50%,rgba(0,0,0,0.65) 100%);
	background: linear-gradient(to bottom, rgba(0,0,0,0) 20%,rgba(0,0,0,0.5) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 );
	padding: 30px;
	transition: all .5s;
	-webkit-transition: all .5s;
}

.promo-slider:hover .card:not(.main) .card-content .wrapper,
.card:not(.main):hover .card-content .wrapper {
	background: -moz-linear-gradient(top, rgba(0,0,0,0) 50%, rgba(0,0,0,0.85) 100%);
	background: -webkit-linear-gradient(top, rgba(0,0,0,0) 50%,rgba(0,0,0,0.85) 100%);
	background: linear-gradient(to bottom, rgba(0,0,0,0) 50%,rgba(0,0,0,0.85) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 );
}

.card .card-content .col {
	padding: 0;
}

.card .card-content .base {
	background-color: #FFFFFF;
    height: 100%;
    opacity: .85;
	padding: 45px;
    position: relative;
    z-index: 10;
}

.card.cover .card-content:after,
.card.highlight .card-content:after {
	background-color: <?php print($secondary); ?>;
	bottom: 0;
	content: "";
	height: auto;
	left: 0;
	opacity: .6;
	pointer-events: none;
	position: absolute;
	right: 0;
	top: 0;
	transition: opacity .6s;
	-webkit-transition: opacity .6s;
	width: auto;
}

.card.cover.gradual-1 .card-content:after {
	opacity: .2;
}

.card.cover.gradual-2 .card-content:after {
	opacity: .4;
}

.card.cover.gradual-3 .card-content:after {
	opacity: .6;
}

.card.highlight .card-content:after {
	background-color: #5B6ABF;
}

.card.cover.link:hover .card-content:after {
	opacity: .8;
}

.card .card-content .sticker {
	height: 40px;
	pointer-events: none;
	position: absolute;
	right: 30px;
	top: 30px;
	width: 40px;
	z-index: 10;
}

.card.large .card-content .sticker {
	height: 80px;
	width: 80px;
}

.card .card-content .sticker img {
	height: auto;
	width: 100%;
}

.card .card-content span {
	color: <?php print($primary); ?>;
	display: inline-block;
	font-size: 12px;
	font-weight: 900;
	letter-spacing: 1px;
	margin: 0 0 5px;
	position: relative;
	text-transform: uppercase;
	z-index: 10;
}

.card.large .card-content span {
	background-color: <?php print($tertiary); ?>;
    box-shadow: 0rem 0.15rem 0rem 0rem rgba(0,0,0,0.1);
	color: <?php print($base); ?>;
    font-weight: normal;
    height: 20px;
    line-height: 20px;
    margin: 0 0 10px;
    padding: 0 5px;
    vertical-align: top;
}

.card.highlight .card-content span {
	color: #BEEFEB;
}

.card .card-content h1 {
	color: #FFFFFF;
	font-size: 58px;
	font-weight: 900;
	line-height: 75px;
	margin: 0;
	position: relative;
	text-shadow: 0px 4px 20px rgba(0, 0, 0, 0.9) !important;
	z-index: 10;
}

.card .card-content h2 {
	color: #FFFFFF;
	font-size: 48px;
	line-height: 64px;
	margin: 0 0 30px;
	position: relative;
	text-shadow: 0px 4px 20px rgba(0, 0, 0, 0.9) !important;
	z-index: 10;
}

.card .card-content h3 {
	font-size: 48px;
	line-height: 48px;
	margin: 0 0 5px;
}

.card.small .card-content h3 {
	font-size: 30px;
	line-height: 30px;
}

.event .card .card-content h3 {
	font-size: 34px;
	line-height: 38px;
	text-decoration: none;
}

.card .card-content h4 {
	font-size: 16px;
	margin: 0 0 2px;
}

.card.large .card-content h4 {
	font-size: 18px;
	margin: 0 0 10px;
}

.card .card-content p {
	font-size: 16px;
	line-height: 26px;
	margin: 0 auto;
	max-width: 660px;
	position: relative;
	z-index: 10;
	font-weight: 500;
}

.card.main .card-content p {
	margin-bottom: 60px;
}

.card.large .card-content p {
	color: #FFFFFF;
}

.card.blank .card-content p {
	color: #222222;
}

.card.large .card-content p a {
	color: <?php print($base); ?>;
	text-decoration: underline;
}

.card .card-content .btn {
	margin: 15px 0 0;
}

.card.large .card-content .btn {
	margin: 30px 0 0;
}

.card.main .card-content .btn {
	margin: 0 15px;
}

.card .card-action {
	background-color: transparent;
	border-top: none;
	padding: 30px;
	position: relative;
	text-align: right;
	z-index: 10;
}

.card .profile-image {
	display: inline-block;
	position: relative;
	vertical-align: top;
}

@keyframes example {
	from {
		transform: rotate(0);
		-webkit-transform: rotate(0);
	}
    to {
		transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
	}
}

.card .profile-image i {
	background-color: #FFFFFF;
	border-radius: 50%;
	bottom: 0;
	color: #222222;
	font-size: 20px;
	height: 30px;
	line-height: 30px;
	position: absolute;
	right: 0;
	text-align: center;
	width: 30px;
    animation-duration: 8s;
	animation-iteration-count: infinite;
	animation-name: example;
	animation-timing-function: linear;
}

/* --------------------------------------------------
   WIDGETS
-------------------------------------------------- */
.widget {
	position: relative;
	margin: 0 0 20px;
}

.widget:before {
	background-color: #222222;
	color: #FFFFFF;
	content: "Test";
	display: inline-block;
	height: 40px;
	line-height: 40px;
	margin: 0 0 10px;
	padding: 0 10px;
	position: relative;
	width: 100%;
}

.widget.widget-facebook:before {
	background-color: #3B5998;
	color: #FFFFFF;
	content: "Facebook";
}

.widget.widget-twitter:before {
	background-color: #55ACEE;
	color: #FFFFFF;
	content: "Twitter";
}

/* --------------------------------------------------
   ITEM
-------------------------------------------------- */
.blog {
    border: 1px solid #EFEFEF;
    border-radius: 20px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.06);
	overflow: hidden;
}

.item-image,
.blog-image {
	position: relative;
}

.blog-image {
	height: 400px;
	margin: 0 0 30px;
	overflow: hidden;
	position: relative;
	width: 100%;
}

.blog-image:after {
	background: -moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.66) 100%);
    background: -webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.66) 100%);
    background: linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.66) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#000000a8',GradientType=0 );
	bottom: 0;
	left: 0;
	content: "";
	position: absolute;
	right: 0;
	top: 0;
	z-index: 10;
}

.blog-image .main-image {
	height: 400px;
	object-fit: cover;
	-o-object-fit: cover;
	width: 100%;
}

.blog-image .slick-slider {
	left: 0;
	position: absolute;
	top: 50%;
	transform: translateY(-50%);
	-webkit-transform: translateY(-50%);
	vertical-align: top;
	width: 100%;
}

.item-image .sticker,
.blog-image .sticker {
	left: 15px;
	pointer-events: none;
    position: absolute;
    top: 15px;
    z-index: 10;
}

.item-image .sticker img,
.blog-image .sticker img {
	height: 60px;
    width: auto;
}

.item-image h3,
.item-image .h3,
.blog-image h3,
.blog-image .h3 {
	color: <?php print($base); ?>;
	position: relative;
	z-index: 20;
}

.blog-image h3,
.blog-image .h3 {
	bottom: 0;
	font-size: 40px;
	left: 0;
	margin: 0;
	line-height: 40px;
	padding: 30px;
	position: absolute;
	right: 0;
}

.blog-options {
	padding: 0 30px;
}

.blog-options .author {
	float: left;
	margin-right: 30px;
}

.blog-options .author img {
	display: inline-block;
	height: 35px;
	margin-right: 10px;
	vertical-align: middle;
	width: 35px;
}

.blog-options .author span {
	display: inline-block;
	vertical-align: middle;
}

.blog-options .date {
	line-height: 35px;
	margin: 0;
	vertical-align: middle;
}

.blog-content {
	padding: 30px 30px 15px;
}

.blog-content h3,
.blog-content .h3 {
	margin-top: 30px;
}

.blog-content img {
	border-radius: 5px;
	box-shadow: 0 3px 6px rgba(0,0,0,0.06);
	object-fit: cover;
	-o-object-fit: cover;
	margin: 0 0 15px;
	position: relative;
}

.blog-content img.left {
	margin: 0 15px 15px 0;
}

.blog-content img.right {
	margin: 0 0 15px 15px;
}

.blog-content ul,
.blog-content ol {
	list-style: none;
	margin: 15px 0 30px;
	overflow: hidden;
	position: relative;
}

.blog-content ol {
	border-radius: 4px;
	counter-reset: custom-counter;
	padding: 30px 30px 30px 20px;
}

.blog-content ol:before {
	background-color: <?php print($secondary); ?>;
	bottom: 0;
	content: "";
	left: 0;
	opacity: .3;
	position: absolute;
	right: 0;
	top: 0;
	z-index: -10;
}

.blog-content ul li,
.blog-content ol li {
	margin: 0 0 5px;
	padding-left: 30px;
	position: relative;
}

.blog-content ol li {
	padding-left: 45px;
}

.blog-content ol li {
	counter-increment: custom-counter;
}

.blog-content ul li:last-child,
.blog-content ol li:last-child {
	margin-bottom: 0;
}

.blog-content ul li:before,
.blog-content ol li:before {
	background-color: <?php print($primary); ?>;
	border-radius: 50%;
	content: "";
	height: 10px;
	left: 0;
	line-height: 10px;
	position: absolute;
	text-align: center;
	top: 6px;
	width: 10px;
}

.blog-content ol li:before {
	background-color: <?php print($base); ?>;
	border: solid 2px #EFEFEF;
	content: counter(custom-counter);
	height: 30px;
	line-height: 28px;
	top: -5px;
	width: 30px;
}

.blog-buttons {
	padding: 30px;
	position: relative;
}

.blog-buttons:before {
	background-color: #E9E9E9;
	left: 30px;
	content: "";
	height: 1px;
	position: absolute;
	right: 30px;
	top: 0;
}

.blog-buttons .sharethis-inline-share-buttons {
	margin: 0;
}

.item-details {
	padding-bottom: 20px;
}

.item-details h1,
.item-details .h1 {
	font-size: 32px;
}

.item-options,
.basket-options {
	padding: 0 0 20px;
}

.basket-options {
	border-top: none;
	border-bottom: none;
	margin-top: 10px;
	padding: 0;
}

.item-options .input-field,
.basket-options .input-field {
	margin-top: 0;
}

.item-options .input-field {
	margin-top: 5px;
}

.item-options .btn:not(.btn-counter) {
	margin-left: 15px;
}
.item-info {
	padding: 20px 0;
}

.item-info p {
	margin-bottom: 20px;
}

.item-info p:last-child {
	margin-bottom: 0;
}

.item-info table {
	margin: 20px 0;
}

.item-info th,
.item-info td {
	padding: 12px;
}

.method,
.ingredients {
	padding: 0 0 20px;
}

.ingredients p a {
	color: <?php print($tertiary); ?>;
	font-weight: 900;
}

.method ol {
	background-color: #F5F5F5;
    margin: 0 0 20px;
    padding: 20px;
}

.ingredients ul {
	background-color: #FFFFFF;
    border: 1px solid #E9E9E9;
	margin: 20px 0;
    padding: 40px 0;
	position: relative;
}

.ingredients ul:before,
.ingredients ul:after {
    background-color: #FFD4D4;
	content: "";
    height: 100%;
    position: absolute;
    top: 0;
    width: 1px;
}

.ingredients ul:before {
	left: 70px;
}

.ingredients ul:after {
	left: 74px;
}

.method ol li {
	counter-increment: inst;
	list-style: none;
	margin-bottom: 20px;
	padding-left: 40px;
	position: relative;
}

.method ol li:last-child {
	margin-bottom: 0;
}

.method ol li:before {
	background-color: #FFFFFF;
    border-radius: 50%;
    box-shadow: inset 0 0 1px #222222;
    content: counter(inst);
    font-size: 14px;
    font-weight: 900;
    height: 25px;
    left: 0;
    line-height: 25px;
    position: absolute;
    text-align: center;
    top: 0px;
    width: 25px;
}

.ingredients ul li {
	border-bottom: 1px solid #E9E9E9;
	padding-left: 120px;
}

.ingredients ul li:first-child {
	border-top: 1px solid #E9E9E9;
}

.ingredients .input-field {
	margin-top: 0;
}

.ingredients .input-field label {
	height: 40px;
	line-height: 40px;
	margin-bottom: 0;
}

.ingredients [type="checkbox"].filled-in:not(:checked) + span:not(.lever) {
	text-decoration: line-through;
}

.ingredients [type="checkbox"].filled-in:checked + span:not(.lever):before {
	border-right: 2px solid #222222;
    border-bottom: 2px solid #222222;
	top: 13px;
	width: 7px;
}

.ingredients [type="checkbox"].filled-in:disabled:checked + span:not(.lever):before {
	border-right: 2px solid #E0E0E0;
    border-bottom: 2px solid #E0E0E0;
}

.ingredients [type="checkbox"].filled-in:checked + span:not(.lever):after {
	background-color: transparent;
	border: solid 2px #222222;
	height: 20px;
	top: 10px;
	width: 20px;
}

.ingredients [type="checkbox"].filled-in:not(:checked) + span:not(.lever):after {
	border: solid 2px #E0E0E0;
	height: 20px;
	top: 10px;
	width: 20px;
}

.ingredients [type="checkbox"].filled-in:disabled:checked + span:not(.lever):after {
	border: solid 2px #E0E0E0;
}

.comments {
	padding: 0 0 20px;
}

#reviews .comments {
	padding: 0 0 30px;
}

.comments.blank {
	background-color: #F5F5F5;
    border-top: 0;
    margin-bottom: 20px;
    padding: 20px;
}

.comments .row {
	margin-left: -5px;
	margin-right: -5px;
}

.comments .col {
	padding-left: 5px;
	padding-right: 5px;
}

.comments.blank h4 {
	font-size: 15px;
	margin: 0;
}

.comments.blank p {
	font-size: 14px;
	margin: 0 0 5px;
}

.comments .comment {
	background-color: #F5F5F5;
	display: inline-block;
	margin-bottom: 20px;
	position: relative;
	vertical-align: top;
	width: 100%;
}

.comments .comment:last-child {
	margin-bottom: 0
}

.comments .comment.response {
	border-top: solid 1px #E6E6E6;
    margin-top: -20px;
}

.comments .comment .profile {
	background-color: #EBEBEB;
	color: #222222;
	height: 40px;
	left: 20px;
	position: absolute;
	text-align: center;
	top: 20px;
	width: 40px;
}

.comments .comment .message {
	display: inline-block;
	padding: 20px;
	vertical-align: top;
	width: 100%;
}

.comments .comment .message h4 {
	font-size: 15px;
	margin: 0;
	padding-left: 50px;
}

.comments .comment .message span {
	color: #ADADAD;
    display: inline-block;
    font-size: 14px;
    margin: 0 0 5px;
    vertical-align: top;
}

.comments .comment .message span.date {
	padding-left: 50px;
}

.comments .comment .message p {
	font-size: 14px;
	margin: 0 0 10px;
}

.comments .comment .message p:last-child {
	margin-bottom: 0;
}

.comments .comment .message a i {
	margin-right: 5px;
}

.comments .comment .message a span {
	color: inherit;
	text-decoration: underline;
}

.comments .comment .reviews {
	margin: 0 0 5px;
}

.comments .comment .reviews i {
	font-size: 20px;
}

.comments .comment .reviews span {
	color: #222222;
	font-size: 14px;
	margin: 0 0 0 5px;
}

.comments.blank .input-field input[type=text],
.comments.blank .input-field input[type=password],
.comments.blank .input-field input[type=email],
.comments.blank .input-field input[type=url],
.comments.blank .input-field input[type=time],
.comments.blank .input-field input[type=date],
.comments.blank .input-field input[type=datetime],
.comments.blank .input-field input[type=datetime-local],
.comments.blank .input-field input[type=tel],
.comments.blank .input-field input[type=number],
.comments.blank .input-field input[type=search],
.comments.blank .input-field select,
.comments.blank .input-field textarea {
	font-size: 14px;
	height: 40px;
}

.comments.blank .input-field input.required,
.comments.blank .input-field textarea.required {
	background-position: right 10px top 10px;
}

/* --------------------------------------------------
   RECENT
-------------------------------------------------- */
.recent.small ul {
	margin: 15px 0 0;
}

.recent li:not(:last-child) {
    border-bottom: solid 1px #E9E9E9;
	margin-bottom: 20px;
    padding-bottom: 20px;
}

.recent.small li:not(:last-child) {
	margin-bottom: 15px;
	padding-bottom: 15px;
}

.recent .recent-wrapper {
	display: inline-block;
	vertical-align: top;
	width: 100%;
}

.recent .recent-image {
	background-color: <?php print($primary); ?>;
	background-position: center center;
	background-size: cover;
	border-radius: 4px;
	float: left;
	height: 100px;
	width: 100px;
}

.recent.small .recent-image {
	height: 80px;
	width: 80px;
}

.recent .recent-content {
	float: right;
	padding-left: 15px;
	width: calc(100% - 100px);
}

.recent.small .recent-content {
	line-height: 20px;
	width: calc(100% - 80px);
}

.recent .recent-content h4 {
	font-size: 18px;
	margin: 0 0 5px;
}

.recent.small .recent-content h4 {
	font-size: 15px;
	line-height: 20px;
	margin: 0;
}

.recent .recent-content h4.truncate {
	display: -webkit-box;
    max-height: 48px;
    white-space: normal;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
}

.recent .recent-content h4:hover,
.recent .recent-content a:hover {
	color: <?php print($primary); ?>;
}

.recent .recent-content .date {
	font-size: 14px;
	margin: 0;
}

.recent.small .recent-content .date {
	font-size: 13px;
}

.recent .recent-content a {
	color: <?php print($tertiary); ?>;
	text-decoration: none;
}


.recent .recent-content a.link {
	color: <?php print($primary); ?>;
	font-size: 16px;
	font-weight: 900;
}

.recent.small .recent-content a.link {
	display: inline-block;
	font-size: 14px;
	vertical-align: top;
	width: 100%;
}

/* --------------------------------------------------
   INTERACTIVE REVIEWS
-------------------------------------------------- */
.br-theme-css-stars .br-widget {
	height: 28px;
	white-space: nowrap;
}

.br-theme-css-stars .br-widget a {
	float: left;
	font-size: 23px;
	height: 18px;
	margin-right: 5px;
	text-decoration: none;
	width: 18px;
}

.br-theme-css-stars .br-widget a:after {
	color: #999999;
	content: "\2606";
}

.br-theme-css-stars .br-widget a.br-active:after {
	color: #FFD54F;
	content: "\2605";
}

.br-theme-css-stars .br-widget a.br-selected:after {
	color: #FFD54F;
	content: "\2605";
}

.br-theme-css-stars .br-widget .br-current-rating {
	display: none;
}

.br-theme-css-stars .br-readonly a {
	cursor: default;
}

/* --------------------------------------------------
   TABLES
-------------------------------------------------- */
.table {
	border: solid 1px #DDDDDD;
	border-bottom: none;
	display: table;
	margin: 0 0 30px;
	position: relative;
	width: 100%;
}

.table .table-row {
	border-bottom: solid 1px #DDDDDD;
	display: table-row;
}

.table .table-row-head {
	display: table-row;
}

.table .table-row-group {
	display: table-row-group;
}

.table .table-cell {
	display: table-cell;
	height: 40px;
	line-height: 40px;
	padding: 0 10px;
	width: 20%;
}

.table .table-row .table-cell {
	border-bottom: solid 1px #DDDDDD;
}

.table .table-row-head .table-cell {
	background-color: #EBEBEB;
	border-bottom: solid 1px #DDDDDD;
	font-weight: 900;
}

.table .table-cell .badge {
	background-color: #222222;
	color: #FFFFFF;
	display: inline-block;
	font-size: 12px;
	font-weight: 900;
	height: 20px;
	line-height: 20px;
	padding: 0 5px;
	vertical-align: middle;
}

.table .table-cell .badge.default {
	background-color: #4DD0E1;
	color: #FFFFFF;
}

.table .table-cell .badge.success {
	background-color: #8BC34A;
	color: #FFFFFF;
}

.table .table-cell .badge.error {
	background-color: <?php print($tertiary); ?>;
	color: <?php print($base); ?>;
}

/* --------------------------------------------------
   PRODUCTS
-------------------------------------------------- */
.events,
.orders,
.products {
	display: inline-block;
	margin-left: -15px;
	margin-right: -15px;
	vertical-align: top;
	width: calc(100% + 30px);
}

.orders,
.reviews {
	margin-left: 0;
	margin-right: 0;
	width: 100%;
}

.orders {
	margin-left: -7.5px;
	margin-right: -7.5px;
	width: calc(100% + 15px);
}

.tab-content .products {
	margin-left: -7.5px;
	margin-right: -7.5px;
	width: calc(100% + 15px);
}

.event-bar,
.order-bar,
.review-bar,
.product-bar {
	display: inline-block;
	margin-bottom: 20px;
	vertical-align: top;
	width: 100%;
}

.event-bar.bottom,
.order-bar.bottom,
.review-bar.bottom,
.product-bar.bottom {
	margin-bottom: 0;
}

.event-bar .select-wrapper input.select-dropdown,
.order-bar .select-wrapper input.select-dropdown,
.review-bar .select-wrapper input.select-dropdown,
.product-bar .select-wrapper input.select-dropdown {
	background-color: rgba(153,153,153,0.2);
	border: none;
    box-shadow: 0rem 0.15rem 0rem 0rem rgba(0,0,0,0.1);
    color: #000000;
    display: inline-block;
    font-weight: 900;
    height: 28px;
    line-height: 28px;
    padding: 0 10px;
    text-decoration: none;
}

.events .event,
.orders .order,
.reviews .review,
.products .product {
	float: left;
	margin-bottom: 30px;
	padding: 0 15px;
	position: relative;
	width: 25%;
}

.products.grid-4 .product {
	width: 33.33%;
}

.products .slick-products .product {
	padding: 0 7.5px;
	width: 100%;
}

.events .event {
	margin-bottom: 0;
	width: 33.33%;
}

.orders .order,
.reviews .review {
	padding: 20px;
	width: 100%;
}

.orders .order {
	border: solid 1px #E0E0E0;
	margin: 0 7.5px 15px;
	width: calc(50% - 15px);
}

.reviews .review {
	background-color: #F5F5F5;
	border-color: #F5F5F5;
}

.reviews .review.response {
	background-color: #EEEEEE;
    border-color: #EEEEEE;
    float: right;
    margin-top: -15px;
    width: calc(100% - 200px);
}

.reviews .review.response:before {
	border-left: solid 2px #222222;
    border-bottom: solid 2px #222222;
    content: "";
    height: 50px;
    left: -100px;
    position: absolute;
    width: 85px;
    top: 0;
}

.reviews .review.response:after {
	border-bottom: 5px solid transparent;
	border-left: 8px solid #222222;
    border-top: 5px solid transparent;
    content: "";
    height: 0;
    left: -20px;
    position: absolute;
    top: 44px;
    width: 0;
}

.basket-row {
	border-bottom: solid 1px #E5E5E5;
	display: inline-block;
	margin-bottom: 20px;
	padding-bottom: 20px;
	vertical-align: top;
	width: 100%;
}

.events .event.small,
.products .product.small {
	width: 25%;
}

.events .event .event-wrapper,
.products .product .product-wrapper {
	overflow: hidden;
	position: relative;
}

.events .event .favourite-toggle,
.products .product .favourite-toggle {
	background-color: transparent;
    border: 0;
    color: #FFFFFF;
	cursor: pointer;
    height: 30px;
    font-size: 16px;
    line-height: 30px;
	opacity: .8;
    position: absolute;
    right: 15px;
    top: 15px;
    width: 30px;
	transition: all .2s;
	-webkit-transition: all .2s;
	will-change: color, opacity, transform;
    z-index: 20;
}

.events .event .favourite-toggle:hover,
.events .event .favourite-toggle.active,
.products .product .favourite-toggle:hover,
.products .product .favourite-toggle.active {
	color: #F44336;
	opacity: 1;
	transform: scale(1.15);
	-webkit-transform: scale(1.15);
}

.products .product .favourite-remove {
	position: absolute;
    right: 30px;
    top: 15px;
	z-index: 20;
}

.events .event .event-image,
.orders .order .order-image,
.products .product .product-image,
.basket-row .basket-image {
	background-color: #EEEEEE;
	background-image: url(https://image.freepik.com/free-photo/white-and-brown-wood-texture-background_1318-42.jpg);
	background-position: center center;
	background-size: cover;
	border-radius: 4px;
	overflow: hidden;
	position: relative;
}

.products.small .product .product-image {
	float: left;
	width: 80px;
}

.events .event .event-image {
	min-height: 220px;
}

.orders .order .order-image,
.basket-row .basket-image {
	float: left;
	margin-right: 20px;
	min-height: 115px;
	max-width: 150px;
}

.orders .order .order-image {
	float: none;
	min-height: 69px;
	margin-right: 0;
	max-width: 100%;
	width: 100%;
}

.events .event .event-image:before,
.orders .order .order-image:before,
.products .product .product-image:before {
	background-color: #FFFFFF;
	bottom: 0;
	content: "";
	left: 0;
	opacity: 0;
	pointer-events: none;
	position: absolute;
	right: 0;
	top: 0;
	z-index: 20;
}

.events .event .event-image:hover:before,
.orders .order .order-image:hover:before,
.products .product .product-image:hover:before {
	opacity: .1;
}

.events .product .favourite-toggle:hover + .event-image:before,
.products .product .favourite-toggle:hover + .product-image:before {
	opacity: .1;
}

.orders .order .order-image .counter {
	background-color: #222222;
    border-radius: 50%;
    bottom: 5px;
    color: #FFFFFF;
    font-size: 10px;
    font-weight: 900;
    height: 15px;
    left: 5px;
    line-height: 15px;
    position: absolute;
    text-align: center;
    width: 15px;
}

.events .event .event-image .sticker,
.products .product .product-image .sticker {
	left: 15px;
	pointer-events: none;
	position: absolute;
	top: 15px;
	z-index: 10;
}

.events .event .event-image .sticker,
.products .product .product-image .sticker .count {
	background-color: <?php print($tertiary); ?>;
	border-radius: 50%;
	color: <?php print($base); ?>;
	height: 46px;
	line-height: 46px;
	text-align: center;
	width: 46px;
}

.events .event .event-image .sticker img,
.products .product .product-image .sticker img {
	height: 35px;
	width: auto;
}

.events .event .product-image a,
.orders .order .order-image a,
.products .product .product-image a {
	display: inline-block;
	vertical-align: top;
	width: 100%;
}

.events .event .event-image img,
.orders .order .order-image img,
.products .product .product-image img,
.basket-row .basket-image img {
	vertical-align: top;
	width: 100%;
}

.events .event .event-image img,
.products .product .product-image img {
	height: 190px;
	object-fit: cover;
	-o-object-fit: cover;
}

.products.small .product .product-image img {
	height: 80px;
}

.events .event .event-image img {
	height: 220px;
}

.events .event .event-image span,
.products .product .product-image span {
	color: #000000;
	font-weight: 900;
    height: auto;
    margin: 0;
    padding: 100px 0 0;
    text-align: center;
}

.events .event .event-content,
.review .review .review-content,
.products .product .product-content,
.basket-row .basket-content {
    padding: 15px 0 0;
    position: relative;
    z-index: 10;
}

.products .product .product-content {
	display: inline-block;
	vertical-align: top;
	width: 100%;
}

.products.small .product .product-content {
	padding: 0 0 0 15px;
	width: calc(100% - 80px);
}

.basket-row .basket-content {
	background-color: transparent;
	display: inline-block;
	vertical-align: top;
	width: calc(100% - 170px);
}

.item .reviews,
.comment .reviews,
.events .event .event-content .info,
.reviews .review .review-content .reviews,
.products .product .product-content .reviews {
	display: inline-block;
	margin: 4px 0;
	vertical-align: top;
	width: auto;
}

.events .event .event-content .info {
	margin: 5px 0 10px;
}

.products .product .product-content .reviews {
	margin-bottom: 15px;
}

.item .reviews,
.comment .reviews,
.reviews .review {
	margin: 0;
}

.item .reviews {
	margin-bottom: 20px;
}

.item .reviews li,
.comment .reviews li,
.events .event .event-content .info li,
.reviews .review .review-content .reviews li,
.products .product .product-content .reviews li {
	float: left;
}


.item .reviews i,
.comment .reviews i,
.events .event .event-content .info i,
.reviews .review .review-content .reviews i,
.products .product .product-content .reviews i {
	color: #999999;
	font-size: 20px;
	vertical-align: top;
}

.item .reviews i,
.comment .reviews i,
.reviews .reviews i {
	font-size: 22px;
}

.item .reviews li.filled i,
.comment .reviews li.filled i,
.events .event .event-content .info li.filled i,
.reviews .review .review-content .reviews li.filled i,
.products .product .product-content .reviews li.filled i {
	color: #FFD54F;
}

.item .reviews span,
.comment .reviews span,
.events .event .event-content .info span,
.reviews .review .review-content .reviews span,
.products .product .product-content .reviews span {
	height: 20px;
	line-height: 20px;
	margin: 0 5px 0;
	text-decoration: underline;
	vertical-align: middle;
	width: auto;
}

.item .reviews span:last-child,
.comment .reviews span:last-child,
.events .event .event-content .info span:last-child,
.reviews .review .review-content .reviews span:last-child,
.products .product .product-content .reviews span:last-child {
	margin: 0;
}

.item .reviews span,
.comment .reviews span,
.reviews .reviews span {
	font-size: 16px;
}

.events .event span,
.reviews .review span,
.products .product span,
.basket-container .basket-row span {
	display: inline-block;
	height: 18px;
	line-height: 18px;
	overflow: hidden;
    margin: 0 0 5px;
	text-overflow: ellipsis;
	vertical-align: top;
	white-space: nowrap;
	width: 100%;
}

.events .event span.date,
.reviews .review span.date,
.products .product span.date,
.basket-container .basket-row span.date {
	color: #ADADAD;
	display: inline-block;
	height: auto;
	line-height: 18px;
    margin: 0 0 10px;
	vertical-align: top;
	width: 100%;
}

.events .event span.date {
	color: <?php print($base); ?>;
	font-size: 16px;
	font-weight: normal;
	left: 30px;
	letter-spacing: 0;
	position: absolute;
	top: 30px;
	text-transform: none;
}

.basket-container .basket-row span {
	height: 18px;
}

.events .event span.title,
.orders .order span.title,
.reviews .review span.title,
.products .product span.title,
.basket-container .basket-row span.title {
	color: <?php print($tertiary); ?>;
	font-size: 16px;
	font-weight: 900;
	height: 18px;
	margin: 0 0 5px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}

.events .event span.title {
	height: 44px;
	line-height: 22px;
	text-overflow: unset;
	white-space: normal;
}

.events .event span.title a,
.orders .order span.title a,
.reviews .review span.title a,
.products .product span.title a,
.basket-container .basket-row span.title a {
	color: <?php print($tertiary); ?>;
	text-decoration: none;
}

.orders .order span.title,
.reviews .review span.title {
	font-size: 20px;
	height: 22px;
	line-height: 22px;
}

.orders .order span.title {
	display: inline-block;
	line-height: 25px;
	margin-bottom: 10px;
    vertical-align: middle;
	width: calc(100% - 80px);
}

.orders .order span.title i.interactive {
	cursor: pointer;
}

.products .product span.description {
	font-size: 14px;
	height: 36px;
    white-space: normal;
}

.products.small .product span.description {
	display: none;
}

.orders .order span.label {
	background-color: #EBEBEB;
    display: inline-block;
    font-size: 14px;
    height: 25px;
    line-height: 25px;
	margin-right: 5px;
    padding: 0 6px;
    vertical-align: middle;
}

.orders .order span.label.saved {
	background-color: <?php print($tertiary); ?>;
	color: #FFFFFF;
}

.orders .order span.label i {
	display: inline-block;
    font-size: 16px;
    margin-right: 5px;
    vertical-align: middle;
}

.orders .order span.price {
	color: <?php print($tertiary); ?>;
    display: inline-block;
    float: right;
    font-size: 20px;
    font-weight: 900;
    height: 25px;
    line-height: 25px;
    margin: 0;
	text-align: right;
    vertical-align: middle;
    width: 80px;
}

.orders .order p {
	margin-bottom: 10px;
}

.events .event .brief,
.reviews .review .brief,
.products .product .brief,
.basket-container .basket-row .brief {
	display: inline-block;
    height: 72px;
    line-height: 18px;
    overflow: hidden;
    margin: 0 0 15px;
    vertical-align: top;
    width: 100%;
}

.reviews .review .brief {
	height: auto;
}

.products .product span.price {
	background-color: <?php print($tertiary); ?>;
	border-bottom-left-radius: 4px;
	border-bottom-right-radius: 4px;
	color: <?php print($base); ?>;
	display: inline-block;
    font-size: 15px;
    font-weight: 900;
    height: 30px;
	left: 0;
    line-height: 30px;
    margin: 0;
	overflow: visible;
	padding: 0 15px;
	pointer-events: none;
	position: absolute;
	text-align: center;
	top: 160px;
    width: 100%;
	z-index: 30;
}

.products .product span.price span {
	height: 30px;
    line-height: 30px;
    margin: 0;
    vertical-align: top;
	width: auto;
}

.item span.price {
	display: inline-block;
	margin: 0 0 15px;
	vertical-align: top;
	width: 100%;
}

.item span.priceper {
	color: #999999;
	display: inline-block;
	font-size: 15px;
	margin: -15px 0 15px;
	vertical-align: top;
	width: 100%;
}

.events .event span.unit,
.products .product span.unit {
	color: #999999;
    font-size: 13px;
    height: auto;
	margin: 0 0 10px;
}

.item ul.key,
.events .event ul.key,
.products .product ul.key {
	display: inline-block;
	margin: 0;
	vertical-align: top;
	width: 100%;
}

.item ul.key.large,
.events .event ul.key.large,
.products .product ul.key.large {
	margin-bottom: 10px;
}

.item ul.key li,
.events .event ul.key li,
.products .product ul.key li {
    float: left;
    margin-right: 5px;
}

.item ul.key.large li,
.events .event ul.key.large li,
.products .product ul.key.large li {
    margin-right: 20px;
}

.item ul.key li {
	margin-right: 10px;
}

.item ul.key li img,
.events .event ul.key li img,
.products .product ul.key li img {
	width: 15px;
}

.item ul.key.large li img,
.events .event ul.key.large li img,
.products .product ul.key.large li img {
	margin-right: 10px;
	vertical-align: middle;
	width: 30px;
}

.item ul.key li img {
	width: 20px;
}

.item ul.key.large li span,
.events .event ul.key.large li span,
.products .product ul.key.large li span {
	vertical-align: middle;
}

.item ul.key .favourite-toggle {
	background-color: transparent;
    border: 0;
    color: #EBEBEB;
    height: 20px;
    font-size: 20px;
    line-height: 20px;
	opacity: .8;
    width: auto;
	transition: all .2s;
	-webkit-transition: all .2s;
	will-change: color, opacity, transform;
}

.item ul.key .favourite-toggle:hover,
.item ul.key .favourite-toggle.active {
	color: #F44336;
	opacity: 1;
}

.item ul.key .favourite-toggle:hover {
	transform: scale(1.15);
	-webkit-transform: scale(1.15);
}

.orders .order .row {
	margin-left: -10px;
	margin-right: -10px;
}

.orders .order .col {
	padding: 0 10px;
}

.orders .order ul.horizontal {
	margin: 10px -2.5px;
}

.orders .order ul.horizontal li {
	padding: 0 2.5px;
	width: 25%;
}
	
.orders .order .feedback {
	border: solid 1px #E0E0E0;
    display: inline-block;
    font-size: 14px;
    height: 35px;
    line-height: 31px;
    margin: 0 0 10px;
    padding: 0 10px;
    text-decoration: none;
    vertical-align: top;
    width: 100%;
}

.orders .order .feedback a {
	color: orange;
	text-decoration: none;
}

.orders .order .feedback i {
	display: inline-block;
	line-height: 31px;
	margin-right: 5px;
	vertical-align: middle;
}

.orders .order .feedback i.right {
	margin-right: 0;
}

.orders .order ul.buttons {
	margin-left: -5px;
	margin-right: -5px;
	vertical-align: top;
	width: calc(100% + 10px);
}
	
.orders .order .buttons li {
	float: left;
	padding: 0 5px;
}

.orders .order .buttons li:not(:first-child) {
	width: calc(100% - 40px);
}

/* --------------------------------------------------
   IMAGE
-------------------------------------------------- */
.event-image,
.product-image {
	border-radius: 4px;
	width: 100%;
}	

/* --------------------------------------------------
   CAROUSEL
-------------------------------------------------- */
.carousel .carousel-item {
	padding: 0 1px;
	width: 33.33%;
}

/* --------------------------------------------------
   GRID
-------------------------------------------------- */
.grid {
	margin-left: -5px;
	margin-right: -5px;
}

.grid-item,
.grid-sizer {
	padding: 0 5px;
	width: 100%;
}

.grid-item.s12 {width: 100%;}
.grid-item.s6 {width: 50%;}
.grid-item.s3 {width: 25%;}
.grid-item.m12 {width: 100%;}
.grid-item.m6 {width: 50%;}
.grid-item.m3 {width: 25%;}
.grid-item.l12 {width: 100%;}
.grid-item.l6 {width: 50%;}
.grid-item.l5 {width: 41.66%;}
.grid-item.l4 {width: 33.33%;}
.grid-item.l3 {width: 25%;}
.grid-item.l1-5 {width: 12.5%;}

/* --------------------------------------------------
   SLIDER
-------------------------------------------------- */
.promo-slider {
	border-radius: 4px;
	box-shadow: 0 2px 5px rgba(0,0,0,0.12);
	height: auto !important;
}

.slick-slider.promo-slider {
	margin-bottom: 10px;
}

.promo-slider .slick-list {
	border-radius: 4px;
	overflow: hidden;
}

.banner-slider {
	height: 320px !important;
	margin: 0 0 20px;
}

.blog-slider {
	height: 260px !important;
	margin: 0 0 20px;
}

.promo-slider .container,
.banner-slider .container {
	position: relative;
}

.promo-slider .slides {
	background-color: <?php print($base); ?>;
	height: 522px !important;
}

.banner-slider .slides {
	background-color: <?php print($base); ?>;
	box-shadow: 0 3px 6px rgba(0, 0, 0, 0.06);
	height: 320px !important;
}

.blog-slider .slides {
	background-color: <?php print($base); ?>;
	height: 260px !important;
}

.banner-slider .slides li {
	border-radius: 5px;
}

.slick-slider .card {
	margin-bottom: 0;
}

.promo-slider .slides li .caption {
	color: #222222;
	left: 0;
	padding-top: 80px;
	top: 0;
	width: 100%;
}

.banner-slider .slides li .caption {
	bottom: 0;
	color: <?php print($base); ?>;
	display: table;
	height: 100%;
	left: 0;
	padding: 30px 60px;
	right: 25%;
	top: 0;
	width: 75%;
}

.banner-slider .slides li .caption.wide {
	width: 100%;
}

.banner-slider .slides li .caption .wrapper {
	display: table-cell;
	vertical-align: middle;
}

.banner-slider .slides li .caption h1 {
	font-size: 48px;
	text-shadow: 0px 4px 20px rgba(0,0,0,0.9);
}

.banner-slider .slides li .caption h2 {
	font-size: 16px;
	font-weight: 500;
	line-height: 26px;
	text-shadow: 0px 4px 20px rgba(0,0,0,0.9);
}

.banner-slider .slides li .caption .btn {
	margin-right: 10px;
}

.banner-slider .slides li .caption .breakpoint {
	bottom: 0;
	left: 0;
	position: absolute;
	right: 0;
}

.blog-slider .slides .sticker {
	left: 15px;
    pointer-events: none;
    position: absolute;
    top: 15px;
    z-index: 10;
}

.blog-slider .slides .sticker img {
	height: 66px;
    width: auto;
}

.banner-slider .indicators {
	bottom: 45px;
	height: 12px;
    left: calc(13% + 45px);
    text-align: left;
    width: calc(74% - 95px);
    z-index: 10;
}

.banner-slider .indicators .indicator-item {
	background-color: #FFFFFF;
    height: 12px;
    margin: 0 5px;
	vertical-align: top;
    width: 12px;
}

.banner-slider .indicators .indicator-item:first-child {
	margin-left: 0;
}

.banner-slider .indicators .indicator-item.active {
	background-color: #DCE317;
}

/* --------------------------------------------------
   BLOG
-------------------------------------------------- */
.blog {
	display: inline-block;
	margin: 0 0 30px;
	vertical-align: top;
	width: 100%;
}

.blog .image {
	display: inline-block;
	max-width: 286px;
	position: relative;
	vertical-align: top;
	width: 100%;
}

.blog .image img {
	display: inline-block;
	height: 286px;
	max-width: 286px;
	object-fit: cover;
	-o-object-fit: cover;
	vertical-align: top;
	width: 100%;
}

.blog .content {
	display: inline-block;
	vertical-align: top;
	width: 100%;
}

.blog.large .content {
	width: calc(100% - 286px);
}

/* --------------------------------------------------
   STAFF MEMBERS
-------------------------------------------------- */
.staff-member {
	
}

.staff-member .image {
	border-radius: 50%;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
    float: left;
    height: 80px;
    object-fit: cover;
	-o-object-fit: cover;
    width: 80px;
}

.staff-member .details {
	background-color: #F1F4A2;
    display: inline-block;
	height: 260px;
	margin-bottom: 18px;
    padding: 20px;
	position: relative;
    width: 100%;
}

.staff-member .details:after {
    border-left: 8px solid transparent;
    border-right: 8px solid transparent;
	border-top: 10px solid #F1F4A2;
    bottom: -10px;
    content: "";
    height: 0;
    left: 40px;
    opacity: 1;
    position: absolute;
    transform: translateX(-50%);
    -webkit-transform: translateX(-50%);
    width: 0;
    z-index: 10;	
}

.staff-member .details span.title {
	font-size: 18px;
	font-weight: 900;
}

.staff-member .details .text {
	display: inline-block;
	height: 100%;
	overflow-y: auto;
	vertical-align: top;
	width: 100%;
}

.staff-member .details .text p {
	font-size: 14px;
	font-style: italic;
	line-height: 18px;
}

.staff-member .details .text p:last-child {
	margin-bottom: 0;
}

/* --------------------------------------------------
   SLICK SLIDER
-------------------------------------------------- */
.slick-slider {
	margin-bottom: 15px;
}

.item-image .slick-slider {
	border-radius: 7px;
	overflow: hidden;
}

.slick-products {
	margin-bottom: 0;
}

.slick-carousel .slick-track {
	margin: 0 -1px;
}

.item-image .slick-nav {
	border-radius: 0;	
}

.slick-nav .slick-track {
	margin: 0 -7.5px;
	width: calc(100% + 15px) !important;
}

.slick-products .slick-track {
	margin: 0;
}

.slick-slider .slick-slide {
	margin: 0;
	outline: none;
}

.slick-carousel .slick-slide {
	margin: 0 1px;
	outline: none;
	position: relative;
}

.slick-nav .slick-slide {
	cursor: pointer;
	margin: 0 7.5px;
	outline: none;
	position: relative;
	/*width: calc(33.33% - 15px) !important;*/
}

.slick-nav .slick-slide:before {
	background-color: #FFFFFF;
	bottom: 0;
	content: "";
	left: 0;
	opacity: 0;
	pointer-events: none;
	position: absolute;
	right: 0;
	top: 0;
	z-index: 20;
}

.slick-nav .slick-slide:hover:before {
	opacity: .1;
}

.slick-nav .slick-slide:not(.slick-current):after {
	background-color: <?php print($tertiary); ?>;
	border-radius: 4px;
	bottom: 0;
	content: "";
	left: 0;
	opacity: .66;
	position: absolute;
	right: 0;
	top: 0;
	z-index: 30;
}

.slick-nav .slick-slide:not(.slick-current):hover:after {
	opacity: .5;
}

.slick-slide img {
	height: 450px;
	object-fit: cover;
	width: 100%;
}

.slick-nav .slick-slide img {
	height: 120px;
}

.item-image .slick-nav .slick-slide img {
	border-radius: 4px;
}

.slick-carousel .slick-slide .carousel-item {
	height: 100%;
	width: 100%;
}

.slick-carousel .slick-slide .caption {
	background-color: <?php print($secondary); ?>;
	bottom: 0;
	left: 0;
	position: absolute;
	right: 0;
}

.slick-carousel .slick-slide .caption span {
	color: #FFFFFF;
	display: inline-block;
	vertical-align: top;
	width: 100%;
}

.slick-arrow {
	background-color: #222222;
    border: none;
	color: #FFFFFF;
	height: 40px;
	left: 0;
	line-height: 40px;
	padding: 0;
    position: absolute;
	text-align: center;
    top: 50%;
    transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
	width: 40px;
    z-index: 10;
}

.slick-products .slick-arrow {
	background-color: <?php print($base); ?>;
	border: solid 1.5px;
    border-radius: 4px;
    color: <?php print($tertiary); ?>;
    height: 30px;
    left: auto;
	line-height: 26px;
    right: 5px;
    top: -35px;
    width: 30px;
}

.slick-arrow:hover,
.slick-arrow:focus {
	background-color: <?php print($tertiary); ?>;
	color: <?php print($base); ?>;
}

.slick-products .slick-arrow.slick-prev {
	right: 45px;
}

.slick-next {
	left: auto;
	right: 0;
}

.slick-arrow i {
	display: inline-block;
	vertical-align: middle;
}

.promo-slider .slick-dots {
	bottom: 30px;
	margin: 0;
	position: absolute;
	right: 30px;
}

.slick-dots li {
	float: left;
	margin-left: 5px;
}

.slick-dots li button {
	background-color: transparent;
	border: solid 2px #FFFFFF;
	border-radius: 14px;
	font-size: 0;
	height: 14px;
	padding: 0;
	transition: background-color .3s;
	-webkit-transition: background-color .3s;
	width: 14px;
	will-change: background-color;
}

.slick-dots li.slick-active button {
	background-color: #FFFFFF;
}

/* --------------------------------------------------
   PROMOTIONS
-------------------------------------------------- */
section.promotions a {
	text-decoration: none;
}

/* --------------------------------------------------
   BOX
-------------------------------------------------- */
.box {
	background-color: <?php print($base); ?>;
	border-radius: 4px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.12);
	margin-top: 30px;
    padding: 30px;
}

.box.transparent {
	background-color: transparent;
	border-color: transparent;
}

.box:first-child {
	margin-top: 0;
}

/* --------------------------------------------------
   SITEMAP
-------------------------------------------------- */
ul.sitemap {
	background-color: <?php print($base); ?>;
	border: solid 1px #EFEFEF;
	border-radius: 5px;
}

.sitemap li {
	border-bottom: 1px solid #EFEFEF;
}

.sitemap a {
	color: <?php print($tertiary); ?>;
	display: inline-block;
	font-size: 18px;
	line-height: 50px;
	padding: 0 50px 0 15px;
	position: relative;
	text-decoration: none;
	vertical-align: top;
	width: 100%;
}

.sitemap a:after {
	border: solid 2px <?php print($tertiary); ?>;
	border-radius: 50%;
	content: "\f2ee";
	float: right;
	font-family: 'Material-Design-Iconic-Font';
	font-size: 12px;
	font-weight: 900;
	height: 20px;
	line-height: 16px;
	position: absolute;
	right: 15px;
	text-align: center;
	top: 50%;
	transform: translateY(-50%);
	-webkit-transform: translateY(-50%);
	width: 20px;
}

/* --------------------------------------------------
   COLLECTIONS
-------------------------------------------------- */
.collection {
	border: none;
	border-radius: 0;
	margin: 0 0 30px;
	overflow: visible;
}

.collection .collection-item {
	font-size: 15px;
	height: 35px;
	line-height: 35px;
	padding: 0;
}

.collection.links .collection-item {
	background-color: transparent;
	border-bottom: none;
	border-radius: 4px;
	color: <?php print($tertiary); ?>;
	display: inline-block;
	font-weight: 900;
	height: 40px;
	line-height: 40px;
	margin: 0 0 5px;
	padding: 0 10px;
	text-decoration: none;
	vertical-align: top;
	width: 100%;
}

.collection.links a:not(.active):hover,
.collection.links a:not(.highlight):hover {
	background-color: #EBEBEB;
	color: <?php print($tertiary); ?>;
}

.collection.links .collection-item.active {
	background-color: #EBEBEB;
	color: <?php print($tertiary); ?>;
}

.collection.links .collection-item.active:hover {
	background-color: #EEEEEE;
}

.collection.links .collection-item.highlight {
	background-color: #5B6ABF;
	color: #FFFFFF;
}

.collection.links .collection-item.highlight:hover {
	background-color: #4E5BA5;
	color: #FFFFFF;
}

.collection.links .collection-item.exit {
	color: <?php print($tertiary); ?>;
}

.collection.links .collection-item span {
	vertical-align: middle;
}

.collection.links .collection-item i {
	background-color: <?php print($base); ?>;
	border: solid 2px #EFEFEF;
	border-radius: 50%;
    color: <?php print($tertiary); ?>;
    float: left;
    font-size: 12px;
	font-weight: 900;
    height: 30px;
    line-height: 28px;
    margin: 5px 10px 0 0;
    min-width: 30px;
    padding: 0;
	text-align: center;
}

.collection.links .collection-item:hover i,
.collection.links .collection-item.active i {
	border-color: #D9D9D9;
}

.collection.links .collection-item i.right {
	margin-right: 0;
}	
	
.collection.links .collection-item span.badge {
	background-color: #222222;
    box-shadow: 0rem 0.15rem 0rem 0rem rgba(0,0,0,0.1);
    color: #FFFFFF;
    float: left;
    font-size: 14px;
    height: 20px;
    line-height: 20px;
    margin: 10px 10px 0 0;
    min-width: 20px;
    padding: 0;
}

/* --------------------------------------------------
   BOOKMARK
-------------------------------------------------- */
.bookmark {
	background-color: <?php print($primary); ?>;
	border: solid 1px rgba(0,0,0,.05);
	border-radius: 50%;
	bottom: 20px;
	color: <?php print($base); ?>;
	cursor: pointer;
	font-size: 22px;
	height: 45px;
	line-height: 45px;
	opacity: 0;
	position: fixed;
	right: 20px;
	text-align: center;
	transform: translateY(60px);
	-webkit-transform: translateY(60px);
	transition: all .6s;
	-webkit-transition: all .6s;
	width: 45px;
	z-index: 600;
}

.bookmark:hover {
	background-color: #7DBB4C;
}

.bookmark:before {
	content: "\F303";
	font-family: 'Material-Design-Iconic-Font';
}

.fixed-head .bookmark {
	opacity: 1;
	transform: translateY(0);
	-webkit-transform: translateY(0);
}

/* --------------------------------------------------
   JPLIST
-------------------------------------------------- */
.jplist-hidden {
	display: none;
}

.jplist-first {
	display: none !important;
}

.jplist-last {
	display: none !important;
}

.jplist-next.disabled {
	display: none !important;
}

.jplist-prev.disabled {
	display: none !important;
}

.jplist-next span:not(.sr-only),
.jplist-prev span:not(.sr-only) {
	font-size: 0;
}

.jplist-next span:not(.sr-only):after,
.jplist-prev span:not(.sr-only):after {
	font-size: 15px;
	font-weight: normal;
}

.jplist-next span:not(.sr-only):after {
	content: "Next";
}

.jplist-prev span:not(.sr-only):after {
	content: "Previous";
}

.jplist-sort {
	display: inline-block;
	margin: 0 10px 0 0;
	min-width: 190px;
	vertical-align: middle;
}

.jplist-pagination-info {
	display: inline-block;
	line-height: 30px;
	padding: 0;
}

/* --------------------------------------------------
   TOAST
-------------------------------------------------- */
#toast-container {
	bottom: 0;
    left: 0;
    max-width: none;
	min-height: 48px;
    right: 0;
    top: auto;
}

.toast {
	border-radius: 0;
	float: none;
	left: 0;
	margin-top: 0;
	padding: 0;
	position: absolute;
	right: 0;
}

.toast .inner {
	background-image: none;
	line-height: 28px;
	margin: 0;
	padding: 10px 0;
	position: relative;
	vertical-align: middle;
	width: 100%;
}

.toast .inner.error {
	background-color: #F44336;
}

.toast .inner.favourite {
	background-color: #F44336;
}

.toast .inner.basket {
	background-color: #8BC34A;
}

.toast span {
	font-size: 15px;
}

.toast .inner span:before {
	float: left;
	font-family: "Material Icons";
	margin-right: 15px;
}

.toast .inner.favourite span:before {
	content: "\E87D";
}

.toast .inner.basket span:before {
	content: "\E8CB";
}

.toast .inner.error span:before {
	content: "\E000";
}

.toast .inner a {
	color: <?php print($base); ?>;
	font-size: 15px;
}

/* --------------------------------------------------
   TABS
-------------------------------------------------- */
.tabs-wrapper {
	background-color: <?php print($base); ?>;
	border: 1px solid #EFEFEF;
	border-radius: 4px;
	box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05);
}

.tabs {
	background-color: <?php print($base); ?>;
	border: 1px solid #EFEFEF;
	border-radius: 4px;
	box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05);
	height: 60px;
	margin: 0 0 30px;
    padding: 0 30px;
}

.tabs-wrapper .tabs {
	border: none;
	border-radius: 0;
	box-shadow: none;
}

.tabs.small {
	height: 50px;
	margin: 0 0 20px;
}

.checkout .tabs {
	margin: 30px 0 0;
}

.tabs .tab {
	height: 60px;
	line-height: 60px;
	text-transform: none;
}

.tabs.small .tab {
	height: 50px;
	line-height: 50px;
}

.tabs .tab a {
	background-color: transparent;
	color: <?php print($tertiary); ?>;
	font-size: 20px;
	padding: 0 30px;
	text-decoration: none;
}

.tabs.small .tab a {
	font-size: 16px;
	padding: 0 20px;
}

.checkout .tabs .tab img {
	display: inline-block;
	height: 25px;
	vertical-align: middle;
}

.tabs .tab a:hover,
.tabs .tab a.active {
	background-color: transparent;
	color: <?php print($tertiary); ?>;
}

.tabs .tab a:focus,
.tabs .tab a:focus.active {
	background-color: transparent;
}

.item .tabs .tab a:hover,
.item .tabs .tab a.active {
	background-color: #F5F5F5;
}

.tabs .tab a.active {
	background-color: transparent;
}

.tabs .indicator {
	background-color: <?php print($primary); ?>;
	height: 5px;
}

.tabs.small .indicator {
	height: 4px;
}

.tab-content {
	padding: 0 20px;
}

.checkout .tab-content {
	margin: 15px 0 0;
	padding: 0;
}

/* --------------------------------------------------
   BASKET
-------------------------------------------------- */
.basket-wrapper {
	display: inline-block;
	margin: 20px 0;
	vertical-align: top;
	width: 100%;
}

.basket-container {
	background-color: #F7F7F7;
    border: solid 1px #E5E5E5;
	margin: 0 0 30px;
    padding: 20px;
}

.basket-summary {
	margin-top: -10px;
}

.basket-summary .input-field {
	border-bottom: solid 1px #E5E5E5;
	margin-top: 0;
	padding: 10px 0;
}

.basket-summary .input-field:first-child {
	padding-top: 0;
}

.basket-summary .input-field:last-child {
	border-bottom: none;
	margin: 10px 0 0;
	padding: 0;
}

.basket-summary .input-field.discount {
	color: #5B6ABF;
}

.basket-summary .input-field.discount .btn {
	color: #5B6ABF;
	height: auto;
	line-height: 21px;
}

.basket-summary .input-field span {
	font-size: 16px;
	font-weight: 900;
}

.basket-summary .input-field span.badge {
	background-color: #8BC34A;
    color: #FFFFFF;
    font-size: 14px;
	height: 24px;
	line-height: 24px;
    padding: 0 5px;
}

.basket-summary .input-field:last-child span {
	font-size: 18px;
}

.basket-summary .collapsible {
	border: none;
	box-shadow: none;
	margin: -10px 0 0;
}

.basket-summary .collapsible-header {
	color: #5B6ABF;
	font-size: 16px;
	line-height: 1.5;
	min-height: 0;
	text-transform: none;
}

.collapsible-header i {
	height: 24px;
	line-height: 24px;
}

.basket-summary .collapsible-body {
	padding: 0;
}

.basket-summary .input-field input[type=text],
.basket-summary .input-field input[type=password], 
.basket-summary .input-field input[type=email],
.basket-summary .input-field input[type=url],
.basket-summary .input-field input[type=time],
.basket-summary .input-field input[type=date],
.basket-summary .input-field input[type=datetime],
.basket-summary .input-field input[type=datetime-local],
.basket-summary .input-field input[type=tel],
.basket-summary .input-field input[type=number],
.basket-summary .input-field input[type=search],
.basket-summary .input-field select,
.basket-summary .input-field textarea {
	border: solid 1px #e5e5e5;
}

.basket-summary .input-field input[type=text]:focus,
.basket-summary .input-field input[type=password]:focus,
.basket-summary .input-field input[type=email]:focus,
.basket-summary .input-field input[type=url]:focus,
.basket-summary .input-field input[type=time]:focus,
.basket-summary .input-field input[type=date]:focus,
.basket-summary .input-field input[type=datetime]:focus,
.basket-summary .input-field input[type=datetime-local]:focus,
.basket-summary .input-field input[type=tel]:focus,
.basket-summary .input-field input[type=number]:focus,
.basket-summary .input-field input[type=search]:focus,
.basket-summary .input-field select:focus,
.basket-summary .input-field textarea:focus {
	border: solid 1px #222222;
}

/* --------------------------------------------------
   FEATURES
-------------------------------------------------- */
.features {
	display: table;
	margin-bottom: 20px;
	vertical-align: top;
	width: 100%;
}

.features .feature-container {
	border-left: solid 2px rgba(153, 153, 153, 0.2);
	display: table-cell;
	padding: 0 30px;
	width: 25%;
}

.features .feature-container.last {
	border-right: solid 2px rgba(153, 153, 153, 0.2);
}

.features .feature-container i {
	color: #b0b612;
	float: right;
	font-size: 26px;
	line-height: 50px;
}

.features .feature-container h5 {
	font-size: 22px;
	font-weight: normal;
}

/* --------------------------------------------------
   STRIPE
-------------------------------------------------- */
.StripeElement {
	background-color: white;
	border: 1px solid rgba(153,153,153,0.3);
	border-radius: 4px;
	height: 45px;
	padding: 12px 12px;
	transition: border-color .3s;
	-webkit-transition: border-color .3s;
}

.StripeElement--focus {
	border-color: #222222;
}

.StripeElement--invalid {
	border-color: #F44336;
}

.StripeElement--webkit-autofill {
	background-color: <?php print($secondary); ?> !important;
}

/* --------------------------------------------------
   FOOTER
-------------------------------------------------- */
.page-footer {
	background-color: <?php print($base); ?>;
	color: <?php print($tertiary); ?>;
	padding-top: 45px;
	position: relative;
}

.page-footer h5 {
	font-size: 22px;
	line-height: 26px;
	margin: 0 0 27px;
}

.page-footer ul {
	color: #8e8e8e;
	font-size: 14px;
}

.page-footer ul li {
	line-height: 25px;
}

.page-footer a {
	color: <?php print($tertiary); ?>;
	text-decoration: none;
}

.page-footer a:hover {
	color: <?php print($primary); ?>;
}

.page-footer a.active {
	color: <?php print($tertiary); ?>;
	font-weight: 900;
}

.page-footer .logo {
	margin-bottom: 30px;
}

.page-footer .logo img {
	max-height: 114px;
}

.page-footer form {
	margin: 53px 0;
}

.page-footer form#newsletter {
	margin: 0 0 42px;
}

.page-footer .widget {
    background-color: #F1F7EC;
	border: 1px solid #E3EADE;
	border-radius: 5px;
	margin: 15px 0 38px;
    padding: 30px 30px 30px 40px;
}

.page-footer .widget:before {
	display: none;
}

.page-footer .widget ul {
	margin: 0;
}

.page-footer .widget ul li {
	color: <?php print($tertiary); ?>;
	font-weight: 500;
	margin-bottom: 13px;
	padding-left: 26px;
	position: relative;
}

.page-footer .widget ul li:last-child {
	margin-bottom: 0;
}

.page-footer .widget ul li:before {
	font-size: 16px;
    color: <?php print($primary); ?>;
    content: '\f1c7';
    font-family: 'Material-Design-Iconic-Font';
	font-weight: normal;
    position: absolute;
    top: 50%;
    left: 0;
    -webkit-transform: translate(0, -50%);
    -khtml-transform: translate(0, -50%);
    -moz-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    -o-transform: translate(0, -50%);
    transform: translate(0, -50%);
}

.page-footer .cards img {
	margin-right: 13px;
	width: 43px;
}

.page-footer .footer-copyright {
	background-color: transparent;
	border-top: solid 1px #EFEFEF;
	color: <?php print($tertiary); ?>;
	min-height: 62px;
	padding: 0;
}

.page-footer .footer-copyright p {
	font-size: 14px;
	margin-bottom: 0;
}

/* --------------------------------------------------
   RESPONSIVENESS
-------------------------------------------------- */
@media only screen and (max-width: 1235px) {
	main {
		padding: 30px 0 60px;
	}
	.container {
		padding: 0 30px;
		width: 100%;
	}
	header .brand-logo img {
		max-height: 50px;
		max-width: 230px;
	}
	header .header-wrapper .buttons a,
	header .header-wrapper .buttons button {
		margin-right: 0;
	}
	header .header-wrapper .buttons a.text,
	header .header-wrapper .buttons button.text {
		display: none !important;
	}
	.search {
		width: calc(100% - 260px);
	}
	.search form {
		margin-left: 0;
	}
	.search form .input-field input {
		min-width: 220px;
	}
	.search form .nice-select {
		min-width: 200px;
	}
	nav ul li {
		padding: 0 15px;
	}
	nav ul a {
		font-size: 15px;
		padding: 0 10px;
	}
	nav ul:not(.social) li:not(:first-child) a:before,
	nav ul:not(.dropdown-content) li:not(:first-child) a:before {
		left: -14px;
	}
	nav ul.social li {
		padding: 0;
	}
	nav ul.social span {
		display: none;
	}
	nav ul.social i {
		font-size: 14px;
		height: 30px;
		line-height: 30px;
		margin-right: 0;
		width: 30px;
	}
	.notification .close {
		right: 30px;
	}
	.modal.small,
	.modal.large {
		width: 45%;
	}
	.sidebar {
		padding-left: 0 !important;
		padding-right: 0;
	}
	.banner-slider {
		padding: 0 20px;
	}
	.banner-slider .slides li .caption {
		background-color: transparent;
		bottom: auto;
		left: 40px;
		padding: 0;
		right: 40px;
		top: 50%;
		transform: translateY(-50%) !important;
		-webkit-transform: translateY(-50%) !important;
	}
	.banner-slider .slides li .caption .decor-both:before,
	.banner-slider .slides li .caption .decor-both:after {
		display: none;
	}
	.banner-slider .slides li .caption h1 {
		font-size: 46px;
	}
	.card .profile {
		left: auto;
		right: 30px;
		width: 80px;
	}
	.card.profile .card-content {
		padding-left: 20px;
		padding-right: 120px;
	}
	.card.profile.blank .card-content {
		padding-left: 120px;
	}
	.card .card-content .base {
		padding: 40px;	
	}
	.card.large .card-content h3 {
		font-size: 28px;
	}
	.features .feature-container {
		padding: 0 20px;
	}
	.features .feature-container h5 {
		font-size: 18px;
		font-weight: 900;
	}
	.page-footer {
		padding-top: 30px;
	}
	.page-footer .row {
		margin-left: -30px;
		margin-right: -30px;
	}
	.page-footer .col {
		padding: 0 30px;
	}
}

@media only screen and (max-width: 992px) {
	html {
		font-size: 14px;
	}
	body {
		background-image: none;
	}
	p {
		font-size: 14px;
	}
	.btn.large {
		height: 45px;
		line-height: 45px;
		padding: 0 15px;
	}
	.btn.isolate {
		height: 30px;
		line-height: 30px;
		width: 30px;
	}
	.btn svg {
		margin: 0 15px 0 0;
		width: 16px;
	}
	.modal.small,
	.modal.large {
		width: 70%;
	}
	ul.social li {
		padding: 0;
	}
	ul.social i {
		height: 25px;
		line-height: 25px;
		width: 25px;
	}
	.top ul li a img {
		height: 25px;
		width: 25px;
	}
	header .header-wrapper .header-logo {
		float: none;
		margin-bottom: 20px;
		width: 100%;
	}
	header .brand-logo img {
		max-width: 260px;
	}
	header .header-wrapper .buttons a,
	header .header-wrapper .buttons button {
		margin: 0;
	}
	.search {
		float: none;
		width: 100%;
	}
	.search form .nice-select {
		min-width: 190px;
	}
	.search form .input-field {
		margin-left: 15px;
	}
	.search form .input-field input {
		min-width: 200px;
	}
	nav ul li {
		padding: 0 10px;
	}
	nav ul a {
		padding: 0 8px;
	}
	nav ul:not(.social) li:not(:first-child) a:before,
	nav ul:not(.dropdown-content) li:not(:first-child) a:before {
		left: -10px;
	}
	.sidebar {
		padding-right: 0;
	}
	.pagination {
		width: auto;
	}
	.nice-select {
		height: 42px;
		line-height: 42px;
		padding-left: 12px;
		padding-right: 12px;
	}
	.input-field input[type=text],
	.input-field input[type=password],
	.input-field input[type=email],
	.input-field input[type=url],
	.input-field input[type=time],
	.input-field input[type=date],
	.input-field input[type=datetime],
	.input-field input[type=datetime-local],
	.input-field input[type=tel],
	.input-field input[type=number],
	.input-field input[type=search],
	.input-field select, .input-field textarea {
		padding: 0 12px;
	}
	.search form .input-field input {
		height: 42px;
	}
	.event-bar,
	.review-bar,
	.product-bar {
		margin-bottom: 20px;
	}
	.events,
	.products {
		margin-left: -10px;
		margin-right: -10px;
		width: calc(100% + 20px);
	}
	.events .event,
	.products .product {
		padding: 0 10px;
	}
	.grid {
		margin-left: -7.5px;
		margin-right: -7.5px;
	}
	.grid-item,
	.grid-sizer {
		padding: 0 7.5px;
	}
	.card {
		height: 220px;
		margin: 0 0 15px;
	}
	.card.large {
		height: 460px;
	}
	.card.small {
		height: 220px;
	}
	.card.profile .card-content {
		padding: 40px 20px 20px;
	}
	.card.large .card-content {
		padding: 0;
	}
	.card.profile.blank .card-content {
		padding-left: 0;
	}
	.card .card-content .base {
		padding: 20px;
	}
	.card .card-content h1 {
		font-size: 40px;
		line-height: 55px;
	}
	.card .card-content h2 {
		font-size: 30px;
		line-height: 38px;
		margin: 0 0 15px;
	}
	.card.small .card-content h3 {
		font-size: 24px;
	}
	.card .card-content h4 {
		font-size: 16px;
		margin: 0 0 5px;
	}
	.card .card-content p {
		line-height: 20px;
		margin: 0 auto 30px;
	}
	.features {
		border-top: solid 2px rgba(153,153,153,0.2);
		display: inline-block;
		margin-bottom: 0;
	}
	.features .feature-container {
		border-bottom: solid 2px rgba(153,153,153,0.2);
		float: left;
		margin: 0;
		padding: 20px;
		width: 50%;
	}
	.features .feature-container:nth-child(2n) {
		border-right: solid 2px rgba(153,153,153,0.2);
	}
	.page-footer {
		padding-top: 15px;
	}
	.page-footer .widget {
		margin-top: 15px;
	}
}

@media only screen and (max-width: 768px) {
	.btn.large {
		font-size: 14px;
		height: 40px;
		line-height: 40px;
	}
	.card.small {
		height: 183px;
	}
	.card.large {
		height: 183px;
	}
	.card.main {
		height: auto;
	}
	.card .card-content .wrapper {
		vertical-align: middle;
	}
	.card:not(.main) .card-content .wrapper {
		padding: 15px 0;
	}
	.card.main .card-content .wrapper {
		background: -moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.65) 100%);
		background: -webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.65) 100%);
		background: linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.65) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#a6000000',GradientType=0 );
		transition: all .5s;
		-webkit-transition: all .5s;
	}
	.card.small .card-content .wrapper {
		padding: 15px 0;
	}
	.card .card-content .col {
		padding: 0 15px;
	}
	.card.main .card-content .col {
		padding: 80px 60px;
	}
	.card.small .card-content .col {
		padding: 0 15px;
	}
	.card .card-content h1 {
		font-size: 28px;
		line-height: 30px;
		margin: 0 0 5px;
	}
	.card .card-content h2 {
		font-size: 22px;
		line-height: 28px;
	}
	.card .card-content h3 {
		font-size: 22px;
		line-height: 26px;
	}
	.card.large .card-content h3 {
		font-size: 22px;
		line-height: 24px;
	}
	.card .card-content h4 {
		font-weight: normal;
		line-height: 18px;
	}
	.card .card-content .btn {
		margin: 15px 0 0;
	}
}

@media only screen and (max-width: 600px) {
	html {
		font-size: 13px;
	}
	body {
		background-color: #F8F8F8;
	}
	p {
		font-size: 13px;
	}
	.btn,
	.btn.large {
		font-size: 13px;
	}
	.btn.small {
		padding: 0 10px;
	}
	header {
		background-color: <?php print($base); ?>;
		border-bottom: 1px solid #E5E5E5;
		box-shadow: 0 3px 6px rgba(0,0,0,0.05);
	}
	header .header-wrapper .header-logo {
		margin-bottom: 15px;
	}
	header .brand-logo img {
		max-height: 45px;
	}
	.search {
		margin: 15px 0 0;
		position: relative;
		width: 100%;
	}
	.search form {
		margin: 0;
		width: 100%;
	}
	.search form .nice-select {
		min-width: 180px;
	}
	.nice-select:after {
		right: 14px;
	}
	.search form .input-field input {
		min-width: 190px;
	}
	header .header-wrapper .buttons {
		bottom: calc(100% + 15px);
		float: none;
		position: absolute;
		right: 0;
	}
	.button-collapse {
		display: block;
		margin-left: 15px !important;
	}
	.notification .close {
		right: 30px;
		top: 0;
		transform: none;
		-webkit-transform: none;
	}
	.modal.small,
	.modal.large {
		width: 90%;
	}
	.grid-item.s12 {
		width: 100%;
	}
	.banner-slider .slides li .caption {
		bottom: auto;
		left: 20px;
		padding: 20px;
		right: 20px;
		top: 50%;
		transform: translateY(-50%) !important;
		-webkit-transform: translateY(-50%) !important;
	}
	.banner-slider .slides li .caption h1 {
		font-size: 18px;
		margin: 0 0 5px;
	}
	.banner-slider .slides li .caption h2 {
		font-size: 13px;
		line-height: 18px;
		margin: 0 0 15px;
	}
	.events .event,
	.products .product {
		width: 50%;
	}
	.card.large {
		height: auto;
	}
	.card .card-content .base {
		padding: 30px;
	}
	.card .card-content span {
		margin: 0 0 5px;
	}
	.card.large .card-content span {
		margin: 0 0 10px;
	}
	.features .feature-container {
		border-right: solid 2px rgba(153,153,153,0.2);
		width: 100%;
	}
	.orders .order .order-image,
	.basket-row .basket-image {
		display: none;
	}
	.basket-content {
		width: 100%;
	}
	.events .event span.description,
	.orders .order span.description,
	.reviews .review span.description,
	.products .product span.description,
	.basket-container .basket-row span.description {
		display: none;
	}
	.input-field input.counter,
	.input-field input.total {
		font-size: 18px !important;
	}
	.page-footer .footer-copyright p.right {
		float: none !important;
	}
}

@media only screen and (max-width: 480px) {
	main {
		padding: 20px 0 40px;
	}
	.container {
		padding: 0 10px;
		width: 100%;
	}
	.page-footer {
		width: 100%;
	}
	.row {
		margin-left: -20px;
		margin-right: -20px;
	}
	.row .col {
		padding-left: 20px;
		padding-right: 20px;
	}
	header .brand-logo img {
    	max-width: 200px;
	}
	header .header-wrapper .buttons a,
	header .header-wrapper .buttons button {
    	font-size: 15px;
	}
	header .button-collapse i, header .button-basket i {
    	font-size: 18px;
    	vertical-align: middle;
	}
	.button-collapse {
    	display: block;
    	margin-left: 10px !important;
	}
	.nice-select {
    	font-size: 13px;
	}
	.btn.large {
    	padding: 0 10px;
	}
	.events,
	.products {
		margin-left: -5px;
		margin-right: -5px;
		width: calc(100% + 20px);
	}
	.search form .nice-select {
    	min-width: 150px;
	}
	.event .card .card-content h3 {
    	font-size: 15px;
    	line-height: 20px;
    	position: absolute;
    	top: 134px;
	}
	.events .event span.date {
    	color: #ffffff;
    	font-size: 12px;
    	font-weight: normal;
    	left: 6px;
    	letter-spacing: 0;
    	position: absolute;
    	top: 4px;
    	text-transform: none;
	}
	.center-btn {
    	top: 10px !important;
	}
	.banner-slider .slides li .caption .btn {
		margin-bottom: 10px;
	}
	.page-footer h5 {
    	font-size: 18px;
    	line-height: 26px;
    	margin: 0 0 27px;
	}
	.container .row {
    	margin-left: 0px;
    	margin-right: 0px;
	}
	.g-recaptcha {
    	transform:scale(0.71);
    	transform-origin:0 0;
	}
	.events .event,
	.orders .order,
	.reviews .review,
	.products .product {
		margin-bottom: 10px;
	}
	.events .event,
	.products .product {
		padding: 0 5px;
	}
	.input-field input[type=email]{
		white-space: nowrap;
  		overflow: hidden;
  		text-overflow: ellipsis;
  		max-width: 260px;
	}
	.events .event span.title,
	.orders .order span.title,
	.reviews .review span.title,
	.products .product span.title,
	.basket-container .basket-row span.title {
		font-size: 16px;
	}
	.events .event .btn,
	.orders .order .btn,
	.reviews .review .btn,
	.products .product .btn,
	.basket-container .basket-row .btn {
		width: 100%;
	}
	.item-options .btn:not(.btn-counter) {
    	margin-left: 15px;
    	margin-top: 15px;
	}
	.product-resp {
		width: 100% !important;
	}
	.item-options .input-field {
    	margin-top: 15px;
	}
	.item-details h1, .item-details .h1 {
    	font-size: 32px;
    	margin-top: 15px;
	}
	.top ul:not(.social) li a {
    	display: inline-block;
    	font-size: 11px;
    	height: 43px;
    	line-height: 43px;
    	margin: 0px 14px;
    	padding: 0;
    	position: relative;
    	text-decoration: none;
    	vertical-align: top;
	}
}

@media print {
	.container {
		padding: 0 20px;
		width: 100%;
	}
	.notification {
		display: none;
	}
	.top {
		display: none;
	}
	.search {
		display: none;
	}
	nav {
		display: none;
	}
	.sidebar {
		display: none;
	}
	.page-footer {
		display: none;
	}
}

<?php
	
	$styleCss = ob_get_clean();
	print(compressCss($styleCss));
	
?>>