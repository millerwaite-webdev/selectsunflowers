function isValid() {
	var valid = true;
	$("#paypal .required").each(function() {
		if($(this).val() == "" || $(this).val() == null) {
			valid = false;
		}
	});
	
	//if(valid) { $("#check").selected(true);} else {$("#check").selected(false);}
	return valid;
}

function ppIsValid() {
	var valid = true;
	$("#frm_ppcountry").each(function() {
		if($(this).val() == "" || $(this).val() == null) {
			valid = false;
		}
	});
	return valid;
}

function onChangeCheckbox(handler) {
	document.querySelector('#frm_ppcountry').addEventListener('change', handler);
}

function toggleValidationMessage() {
	document.querySelector('#msg').style.display = (ppIsValid() ? 'none' : 'block');
	$("#frm_ppcountry").removeClass("error");
	var address = $("#frm_ppcountry");
	if($(address).val() == "" || $(address).val() == null) {
		$(address).addClass("error");
		if($(address).data("label") !== undefined && !$(address).next(".label-note").length) $(address).after("<div class='label-note error'>Your "+$(address).data("label")+" is invalid.</div>");
		console.log("Test 4");
	}
	$("input.error, textarea.error, select.error").change(function(e){
		$(this).removeClass("error");
		if($(this).next(".label-note") != "") {
			$(this).next(".label-note").remove();
		}
	});
}

function toggleButton(actions) {
	return isValid() ? actions.enable() : actions.disable();
}

function getNote() {
	return $("textarea[name=frm_note]").val();
}

// Render the PayPal button
paypal.Button.render({
	// Set your environment
	env: paypalType, // sandbox | production

	// paypal default region
	locale: "en_GB",
		
	// Show the buyer a 'Pay Now' button in the checkout flow
	commit: true,
	
	// Style checkout button
	style: {
		color: 'gold',
		label: 'checkout',
		shape: 'rect',
		size: 'large',
		tagline: false
	},
	
	validate: function(actions) {
		toggleButton(actions);
		
		onChangeCheckbox(function() {
			toggleButton(actions);
		});
	},

	onClick: function() {
		toggleValidationMessage();
	},
	
	payment: function(data, actions) {
	
		// Set up a url on your server to create the payment
		var CREATE_URL = '/ajax/paypal-create.php';

		// Make a call to your server to set up the payment
		return paypal.request.post(CREATE_URL).then(function(res) {
			return res.id;
		});
	},

	// onAuthorize() is called when the buyer approves the payment
	onAuthorize: function(data, actions) {
	
		// Set up a url on your server to execute the payment
		var EXECUTE_URL = '/ajax/paypal-execute.php';
		
		// Set up the data you need to pass to your server
		var data = {
			paymentID: data.paymentID,
			payerID: data.payerID
		};

		// Make a call to your server to execute the payment
		return paypal.request.post(EXECUTE_URL, data)
			.then(function(res) {
				var destination = "/confirmation/";
				if(typeof res.st != "undefined") {
					destination += res.st+"/";
				}
				if(typeof res.id != "undefined") {
					destination += res.id;
				}
				window.location = destination;
			});
	},
	onCancel: function(data, actions) {
		$("#order-cancel").modal("open");
	},
	onError: function(err) {
		$("#order-error").modal("open");
	}
}, '#paypal-button');