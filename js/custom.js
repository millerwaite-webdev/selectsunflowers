// Functions
function basketAdd(product,quantity,method) {
	$.ajax({
		cache: false,
		url: "/ajax/basket.php",
		type: 'POST',
		dataType: 'json',
		data: {
			method: method,
			product: product,
			quantity: quantity
		},
		success: function(data) {
			$("#basket-total").html("&pound;"+numberFormat(data.baskettotal,2));
			$("#basket-final").html("&pound;"+numberFormat(data.baskettotal,2));
			$("#basket-mobile").html("&pound;"+numberFormat(data.baskettotal,2));
			if(method != "all") {
				var $toastContent = $("<div class='inner basket'><div class='container'>"+data.message+"<a href='/basket' class='right'>View Basket</a></div></div>");
				M.toast({html: $toastContent, displayLength: 10000});
			}
			if(data.basketdiscount > 0) $("nav ul a.basket").addClass("discount");
		},
		error: function(data) {
			var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error adding this item to your basket.</span></div></div>");
			M.toast({html: $toastContent, displayLength: 5000});
		}
	});
}

function pcLookup() {
	if($('#postcodeLookupToggle').html() == "(Enter Manually)") {
		$('#postcodeLookupToggle').html("(Use Look Up)");
	} else {
		$('#postcodeLookupToggle').html("(Enter Manually)");
	}
	if($('#pcButton').hasClass('hide')) {
		$('#pcButton').removeClass('hide');
	} else {
		$('#pcButton').addClass('hide');
	}
	if($('#address').hasClass('hide')) {
		$('#address').removeClass('hide');
	}
	//if($('select#frm_address').length == 1) {
		var add = $('#frm_address').val();
		
		$('#address1').html("<input type='text' class='required depend' id='frm_address' name='frm_address' tabindex='7' value='"+ add + "' />");
	//}
}



function orderEdit(order) {
	$.ajax({
		cache: false,
		url: "/ajax/order.php",
		type: 'POST',
		dataType: 'json',
		data: {
			method: "edit",
			order: order
		},
		success: function(data) {
			$("input[name=frm_title]").val(data.title);
			$("input[name=frm_description]").val(data.description);
			$('#order-modal').modal('open');
		},
		error: function(data) {
			
		}
	});
}

function orderSave(order,title,description) {
	$.ajax({
		cache: false,
		url: "/ajax/order.php",
		type: 'POST',
		dataType: 'json',
		data: {
			method: "save",
			order: order,
			title: title,
			description: description
		},
		success: function(data) {
			$('#order-modal').modal('close');
		},
		error: function(data) {
			alert("Hello World!");
		}
	});
}

function favouriteToggle(product) {
	$.ajax({
		cache: false,
		url: "/ajax/favourite.php",
		type: 'POST',
		dataType: 'json',
		data: {
			product: product
		},
		success: function(data) {
			if(data.method == "add") {
				$(".favourite-toggle[data-id="+data.id+"]").addClass("active").attr('aria-label','Add To Favorites');
				var $toastContent = $("<div class='inner favourite'><div class='container'>"+data.message+"</div></div>");
				M.toast({html: $toastContent, displayLength: 5000});
			} else if(data.method == "remove") {
				$(".favourite-toggle[data-id="+data.id+"]").removeClass("active").attr('aria-label','Remove From Favorites');
				var message = data.message;
				var $toastContent = $("<div class='inner favourite'><div class='container'>"+data.message+"</div></div>");
				M.toast({html: $toastContent, displayLength: 5000});
			} else if(data.method == "login") {
				$("#modal-login").modal("open");
			} else {
				var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error adding this item to your favourites.</span></div></div>");
				M.toast({html: $toastContent, displayLength: 5000});
			}
		},
		error: function(data) {
			var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error adding this item to your favourites.</span></div></div>");
			M.toast({html: $toastContent, displayLength: 5000});
		}
	});
}

function favouriteRemove(product) {
	$.ajax({
		cache: false,
		url: "/ajax/favourite.php",
		type: 'POST',
		dataType: 'json',
		data: {
			product: product
		},
		success: function(data) {
			$(".product[data-id="+data.id+"]").remove();
			var count = $(".favourite-count").text() - 1;
			$(".favourite-count").text(count);
			var message = data.message;
			var $toastContent = $("<div class='inner favourite'><div class='container'>"+data.message+"</div></div>");
			M.toast({html: $toastContent, displayLength: 5000});
		},
		error: function(data) {
			var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error adding this item to your favourites.</span></div></div>");
			M.toast({html: $toastContent, displayLength: 5000});
		}
	});
}

function formValidation() {
	var result = true;
	$(".required").removeClass("error");
	$(".required").each(function() {
		if($(this).val() == "" || $(this).val() == null) {
			$(this).addClass("error");
			if($(this).data("label") !== undefined && !$(this).next(".label-note").length) $(this).after("<div class='label-note error'>Your "+$(this).data("label")+" is invalid.</div>");
			result = false;
		}
	});
	$("input.error, textarea.error, select.error").change(function(e){
		$(this).removeClass("error");
		if($(this).next(".label-note") != "") {
			$(this).next(".label-note").remove();
		}
	});
	return result;
}


/**
 * numberFormat
 *
 * JS equivalent to php function number_format
 *
 * @param float|double	x			The number to be formatted
 * @param int			decPlaces	The number of decimal places to display
 * @param char			decPoint	The character to use for a decimal places (default '.')
 * @param char			thousandSep	The character to use for a thousand separator (default ',')
 *
 * @return struing					The formatted number
 */
function numberFormat(x,decPlaces,decPoint,thousandSep) {
	
	if(typeof decPoint == "undefined") {
		decPoint = "."
	}
	if(typeof thousandSep == "undefined") {
		thousandSep = ","
	}
	var parts = x.toFixed(decPlaces).split(decPoint);
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousandSep);
	return parts.join(decPoint);
}

function checkoutExit(e) {
	var target = $(e).data('trigger');
	$('#'+target).modal('open');
/*	$.ajax({
		cache: false,
		url: "/ajax/cookie.php",
		type: 'POST',
		dataType: 'text',
		data: {
			method: "exit"
		},
		success: function () {
			$('.detector[data-trigger=modal-exit]').remove();
		}
	});*/
}

$(document).ready(function(){
			
	// Init responsive sidebar
	$(".sidenav").sidenav();

	// Init slick sliders
	$('.slick-slider').slick({
		arrows: true,
	//	asNavFor: '.slick-nav',
		draggable: false,
		fade: true,
		nextArrow: "<button type='button' class='slick-next'><i class='material-icons'>chevron_right</i></button>",
		prevArrow: "<button type='button' class='slick-prev'><i class='material-icons'>chevron_left</i></button>",
		slidesToShow: 1,
		slidesToScroll: 1
	});
	
	$('.promo-slider').slick({
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		dots: true,
		draggable: true,
		fade: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	
	$('.slick-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		nextArrow: "<button type='button' class='slick-next'><i class='material-icons'>chevron_right</i></button>",
		prevArrow: "<button type='button' class='slick-prev'><i class='material-icons'>chevron_left</i></button>",
		asNavFor: '.slick-slider',
		dots: false,
		centerMode: false,
		focusOnSelect: true
	});
	
	$('.slick-carousel').slick({
		arrows: false,
		draggable: true,
		slidesToShow: 1,
		slidesToScroll: 1,
	});
	
	$('#slick-category1').slick({
		arrows: true,
		centerMode: false,
		draggable: false,
		nextArrow: "<button type='button' class='slick-next'><i class='material-icons'>chevron_right</i></button>",
		prevArrow: "<button type='button' class='slick-prev'><i class='material-icons'>chevron_left</i></button>",
		slidesToShow: 4,
		slidesToScroll: 1,
	});
	
/*	$('#slick-category2').slick({
		arrows: true,
		centerMode: false,
		draggable: false,
		nextArrow: "<button type='button' class='slick-next'><i class='material-icons'>chevron_right</i></button>",
		prevArrow: "<button type='button' class='slick-prev'><i class='material-icons'>chevron_left</i></button>",
		slidesToShow: 4,
		slidesToScroll: 1,
	});*/
	
	// Init slider
	$('.promo-slider').slider({
		height: 522,
		indicators: false,
		interval: 10000
	});
	
	// Init slider
	$('.banner-slider').slider({
		height: 350,
		indicators: false,
		interval: 10000
	});
	
	// Init masonry
	$('.grid').masonry({
		itemSelector: '.grid-item',
		columnWidth: '.grid-sizer',
		percentPosition: true
	});
	
	// Init modal
	$('.modal').modal({
		onCloseEnd: function(){
			$.cookie("newsletter", 1, {expires : 365, path: "/"});
		}
	});
	
	// Init tooltip
	$('.tooltipped').tooltip();
	
	// Init tabs
	$('ul.tabs').tabs({
		onShow: function(tabs) {
			$("#frm_method").val(tabs.id);
		}
	});
	
	// Get date/time info
	var today = new Date()
	var now = today.getTime();
	var later = today.setHours(16);
	
	if(now < later) {
		var advance = 24;
		$("#nextday").removeClass("active");
	} else {
		if(today.getDay() != "Friday") {
			var advance = 72;
		} else {
			var advance = 48;
		}
		$("#nextday").addClass("active");
	}
	
	// Init datepicker
	$('.datepicker').datepicker({
		selectMonths: false,
		autoClose: true,
		format: "dddd dd mmm, yyyy",
		defaultDate: new Date(),
		setDefaultDate: true,
		disableDayFn: function(date) {
			if(date.getDay() == 0) {
				return true;
			} else {
				return false;
			}
		},
		firstDay: 1,
		minDate: new Date((new Date()).valueOf() + 1000 * 3600 * advance),
		showDaysInNextAndPreviousMonths: true
	});
	
	// Init image zoom
	$('.materialboxed').materialbox();
	
	// Init select
	$('select:not(.nice-select)').formSelect();
	
	// Init dropdown
	$(".filter-button").dropdown({
		coverTrigger: false
	});
	
	$('.select-button').dropdown({
		coverTrigger: false
	});
	
	// Init Nice Select
	$('select.nice-select').niceSelect();
	
	$('.product-select').change(function(){
		$.ajax({
			cache: false,
			url: "/ajax/product.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "size",
				product: $(this).val()
			},
			success: function(data) {
				if(data.amount >= 1000) {
					$('#modal-restrict').modal('open');
				}
			},
			error: function(data) {
				// Failure
			}
		});
	});
	
	// Init carousel
	$('.carousel').carousel({
		dist: 0,
		noWrap: true
	});
	
	$(".account-button").dropdown({
		alignment: "right",
		belowOrigin: true,
		constrainWidth: false
	});
	
	$(".dropdown-trigger").dropdown({
		alignment: "left",
		constrainWidth: false,
		coverTrigger: false,
		hover: true,
		onOpenStart: function(){
			$(".dropdown-trigger").addClass("active");
		},
		onCloseStart: function(){
			$(".dropdown-trigger").removeClass("active");
		},
	});
	
	$(".basket-trigger").dropdown({
		closeOnClick: false,
		constrainWidth: false,
		coverTrigger: false,
	});
	
	// Toggle cookie notification
	$(".cookie-toggle").click(function(){
		$.ajax({
			cache: false,
			url: "/ajax/cookie.php",
			type: 'POST',
			dataType: 'text',
			data: {
				method: "cookies"
			},
			success: function () {
				$(".notification.cookie").addClass("remove");
			},
			error: function () {
				alert("Failure");
			}
		});
	});	
	
	// Basket Options
	$(".basket-add").click(function(e) {
		e.preventDefault(e);
		var product = $("[name=frm_product]").val();
		var quantity = $("input[name=frm_qty]").val();
		basketAdd(product,quantity,"add");
	});
	
	$(".basket-all").click(function(e) {
		e.preventDefault(e);
		$(".product").each(function(data) {
			var product = $(this).data("id");
			var quantity = 1;
			basketAdd(product,quantity,"all");
		});
		var $toastContent = $("<div class='inner basket'><div class='container'><span>All of your favourites have been added to the basket.</span><a href='/basket' class='right'>View Basket</a></div></div>");
		M.toast({html: $toastContent, displayLength: 10000});
	});
	$("#frm_country, #frm_postcode").change(function(e){
		$.ajax({
			cache: false,
			url: "/ajax/basket.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "changePostage",
				country: $('#frm_country').val(),
				postcode: $('#frm_postcode').val()
			},
			success: function(data) {
				$("#basket-total").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-final").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-mobile").html("&pound;"+numberFormat(data.baskettotal,2));

				$("#discount-value").text("- £"+numberFormat(data.basketdiscount,2));
				$("#vat-amount").text(" £"+numberFormat(data.vatamount,2));
				if(data.deliveryamount != null) {
					$("#delivery-value").text("£"+numberFormat(data.deliveryamount,2));
				} else {
					$("#delivery-value").text("TBC");
				}
			},
			error: function(data) {
				alert("Failure");
				var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error removing this item from your basket.</span></div></div>");
				M.toast({html: $toastContent, displayLength: 5000});
			}
		});
	});
	
	$("#frm_ppcountry, #frm_pppostcode").change(function(e){
		$.ajax({
			cache: false,
			url: "/ajax/basket.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "changePostage",
				country: $('#frm_ppcountry').val(),
				postcode: $('#frm_pppostcode').val()
			},
			success: function(data) {
				$("#basket-total").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-final").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-mobile").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#discount-value").text("- £"+numberFormat(data.basketdiscount,2));
				$("#vat-amount").text(" £"+numberFormat(data.vatamount,2));
				if(data.deliveryamount != null) {
					$("#delivery-value").text("£"+numberFormat(data.deliveryamount,2));
				} else {
					$("#delivery-value").text("TBC");
				}
				$("#paypal-box").removeClass("hide");
			},
			error: function(data) {
				var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error removing this item from your basket.</span></div></div>");
				M.toast({html: $toastContent, displayLength: 5000});
				$("#paypal-box").addClass("hide");
			}
		});
	});
	
	$(".basket-edit").click(function(e){
		//pass country to ajax
		e.preventDefault(e);
		var method = $('#frm_method').val();
		if(method == "paypal") var country = $('#frm_ppcountry').val();
		else var country = $('#frm_country').val();
		$.ajax({
			cache: false,
			url: "/ajax/basket.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "edit",
				product: $(this).data("id"),
				quantity: $(this).data("count"),
				country: country,
				postcode: $('#frm_postcode').val()
			},
			success: function(data) {
				$("#basket-total").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-final").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-mobile").html("&pound;"+numberFormat(data.baskettotal,2));
				$(".counter[data-id="+data.id+"]").val(numberFormat(data.quantity,0));
				$(".total[data-id="+data.id+"]").val("£"+numberFormat(data.price,2));
				
				if(data.quantity > 1) $(".negative[data-id="+data.id+"]").prop("disabled", false);
				else $(".negative[data-id="+data.id+"]").prop("disabled", true);
				
				if(data.basketdiscount > 0) {
					$("nav ul a.basket").addClass("discount");
					$("#discount-on").removeClass("hide");
				} else {
					$("nav ul a.basket").removeClass("discount");
					$("#discount-on").addClass("hide");
				}
				
				$("#discount-value").text("- £"+numberFormat(data.basketdiscount,2));
				$("#vat-amount").text(" £"+numberFormat(data.vatamount,2));
				if(data.deliveryamount != null) {
					$("#delivery-value").text("£"+numberFormat(data.deliveryamount,2));
				} else {
					$("#delivery-value").text("TBC");
				}
				
				if(data.baskettotal >= 15) {
					if($("#minOrder").hasClass("active")) {
						$("#minOrder").removeClass("active");
					}
					if($(".payment-options").hasClass("hide")) {
						$(".payment-options").removeClass("hide");
					}
				} else {
					if(!$("#minOrder").hasClass("active")) {
						$("#minOrder").addClass("active");
					}
					if(!$(".payment-options").hasClass("hide")) {
						$(".payment-options").addClass("hide");
					}
				}
				
				if(data.packSizeWarning || data.qtyWarning) {
					if(!$("#qtyWarning").hasClass("active")) {
						$("#qtyWarning").addClass("active");
					}
				} else {
					if($("#qtyWarning").hasClass("active")) {
						$("#qtyWarning").removeClass("active");
					}
				}
			},
			error: function(data) {
				alert("Failure");
				var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error removing this item from your basket.</span></div></div>");
				M.toast({html: $toastContent, displayLength: 5000});
			}
		});
	});
	
	$(".basket-remove").click(function(e){
		//pass country to ajax
		e.preventDefault(e);
		$.ajax({
			cache: false,
			url: "/ajax/basket.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "remove",
				product: $(this).data("id"),
				country: $('#frm_country').val(),
				postcode: $('#frm_postcode').val()
			},
			success: function(data) {
				$("#basket-total").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-final").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#basket-mobile").html("&pound;"+numberFormat(data.baskettotal,2));
				$("#row-"+data.id).remove();
				
				if(data.basketdiscount == 0) $("nav ul a.basket").removeClass("discount");
				if(data.baskettotal <= 0) location.reload();
				

				$("#discount-value").text("- £"+numberFormat(data.basketdiscount,2));
				$("#vat-amount").text("£"+numberFormat(data.vatamount,2));
				if(data.deliveryamount != null) {
					$("#delivery-value").text("£"+numberFormat(data.deliveryamount,2));
				} else {
					$("#delivery-value").text("TBC");
				}
			},
			error: function(data) {
				var $toastContent = $("<div class='inner error'><div class='container'><span>There was an error removing this item from your basket.</span></div></div>");
				M.toast({html: $toastContent, displayLength: 5000});
			}
		});
	});
	
	// Toggle product quantity
	$(".count-toggle").click(function(e){
		e.preventDefault(e);
		var change = $(this).data("count");
		var current = $("input[name=frm_qty]").val();
		var result = parseInt(change) + parseInt(current);
		var initial = $("select[name=frm_product]").children(":selected").data("price");
		var total = parseFloat(initial) * parseFloat(result);
		$("input[name=frm_qty]").val(result);
		$("input[name=frm_total]").val("£"+total.toFixed(2));
		if(result > 1) $("button.negative").prop("disabled", false);
		else $("button.negative").prop("disabled", true);
		if(result < 10) $("button.positive").prop("disabled", false);
		else $("button.positive").prop("disabled", true);
	});
	
	$(".product-select").change(function(e){
		var current = $("input[name=frm_qty]").val();
		var initial = $("select[name=frm_product]").children(":selected").data("price");
		var total = parseFloat(initial) * parseFloat(current);
		$("input[name=frm_total]").val("£"+total.toFixed(2));
		
	});
	
	// Toggle favourite
	$(".favourite-toggle").click(function(e){
		e.preventDefault(e);
		favouriteToggle($(this).data("id"));
	});
	
	// Remove favourite
	$(".favourite-remove").click(function(e){
		e.preventDefault(e);
		favouriteRemove($(this).data("id"));
	});
	
	// Postcode lookup/checker
	$(".postcode-search").click(function(e){
		e.preventDefault(e);
		$(this).prop("disabled", true);
		$("#frm_postcode").prop("disabled", true);
		$(this).html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
		$.ajax({
			cache: false,
			url: "/ajax/postcode.php",
			type: 'POST',
			dataType: 'text',
			data: {
				postcode: $("input[name=frm_postcode]").val()
			},
			success: function(data){
				$(".postcode-search").prop("disabled", false);
				$("#frm_postcode").prop("disabled", false);
				$(".postcode-search").html("Find Address");
				$("#address").html(data);
				$("#address").removeClass("hide");
			},
			error: function(data) {
				alert("Failure");
			}
		});
	});
	
	// Postcode input restrictions
	$("input.code").keypress(function(e){
		var regex = new RegExp("^[a-zA-Z0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if(!(regex.test(str))) {
			return false;
		}
	});
	
	// Phone input restrictions
	$("input[type=tel]").keypress(function(e){
		var regex = new RegExp("^[0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if(!(regex.test(str))) {
			return false;
		}
	});
	
	// Product search
	$("input.autocomplete").keyup(function(e){
		var keywords = $(this).val();
		if(keywords.length) {
			$.ajax({
				cache: false,
				url: "/ajax/search.php",
				type: 'POST',
				dataType: 'json',
				data: {
					keywords: keywords
				},
				success: function(data){
					var products = [];
					$.each(data, function(i, item) {
						if(item.link != "") {
							products.push("<li><a href='"+item.link+"'>"+item.name+"</a></li>");
						} else {
							products.push("<li><span>"+item.name+"</span></li>");
						}
					});
					$(".autocomplete-content").html(products);
					$(".autocomplete-content").removeClass("hide");
				},
				error: function(data) {
					alert("Failure");
				}
			});
		} else {
			$(".autocomplete-content").addClass("hide");
		}
	});
	
	// Hide product search
	$(document).click(function(e) { 
		if(!$(e.target).closest(".search .container").length) {
			if(!$(".autocomplete-content").hasClass("hide")) {
				$(".autocomplete-content").addClass("hide");
			}
		}
	});
	
	// Additional inputs
	$("select[data-extra]").change(function(e){
		var target = $(this).data("extra");
		if($(this).val() == "other") {
			$("#"+target).removeClass("hide");
		} else {
			$("#"+target).addClass("hide");
		}
	});
	
	// Bookmark (Top)
	$(".bookmark").click(function(){
		$("html, body").animate({
			scrollTop: 0
		}, 600);
		return false;
	});
	
	// Bookmark (Checkpoint)
	$(".btn-scroll").click(function(){
		$("html, body").animate({
			scrollTop: $(".banner-slider").position().top + $(".banner-slider").height()
		}, 600);
		return false;
	});
	
	// Profile Picture
	$(".profile").initial({
		charCount: 2,
		fontSize: 40
	});
	
	$(".profile-small").initial({
		charCount: 2,
		fontSize: 48,
		fontWeight: 700
	});
	
	// Review response
	$(".comment button").click(function(){
		var id = $(this).data("id");
		if($("#"+id).hasClass("hide")) {
			$("#"+id).removeClass("hide");
		} else {
			$("#"+id).addClass("hide");
		}
	});
	
	$('.collapsible:not(.accordion)').collapsible({
		accordion: false
	});
	
	$('.accordion').collapsible({
		accordion: true
	});
	
	if($(window).width() < 992) {
		$('.collapsible').collapsible('close');
	};
	
	// Logout
	$("#logout").click(function(e){
		e.preventDefault(e);
		$.ajax({
			cache: false,
			url: "/ajax/account.php",
			type: 'POST',
			dataType: 'text',
			data: {
				method: "logout",
			},
			success: function(data) {
				window.location.replace("/");
			},
			error: function(data) {
				location.reload();
			}
		});
	});
	
	// Newsletter Modal
	setTimeout( function(){
		if($('#modal-newsletter').length){
			$('#modal-newsletter').modal('open');
			$.ajax({
				cache: false,
				url: "/ajax/cookie.php",
				type: 'POST',
				dataType: 'text',
				data: {
					method: "newsletter"
				}
			});
		}
	}, 1000);
	
	// Fixed elements
	var topofTop = $(".top").offset().top;
	var heightofTop = $(".top").outerHeight();
	
	// Fixed navbar on page scroll
	$(window).scroll(function(){
		if($(window).scrollTop() > (topofTop + heightofTop)){
			$("body").addClass('fixed-top');
		} else {
			$("body").removeClass('fixed-top');
		}
	});
	
	// Fixed elements
	var topofHead = $("header").offset().top;
	var heightofHead = $("header").outerHeight();
	
	// Fixed navbar on page scroll
	$(window).scroll(function(){
		if($(window).scrollTop() > (topofHead + heightofHead)){
			$("body").addClass('fixed-head');
		} else {
			$("body").removeClass('fixed-head');
		}
	});
	
	// Fixed navbar on page load
	$(document).ready(function(){
		if($(window).scrollTop() > (topofHead + heightofHead)){
			$("body").addClass('fixed-head');
		} else {
			$("body").removeClass('fixed-head');
		}
	});
	
	// JP List
	$('#shop').jplist({
		itemsBox: '.products',
		itemPath: '.product',
		panelPath: '.product-options',
		deepLinking: false,
		storage: 'localstorage',
		storageName: 'shop-storage',
		dataSource: {
			type: 'server',
			server: {
				ajax:{
					url: '/server/server-html-shop.php',
					dataType: 'html',
					type: 'POST'
				},
				serverOkCallback: function(serverData, statuses, ajax, response){
					$(".basket-add").click(function(e) {
						e.preventDefault(e);
						basketAdd($(this).data("id"),1,"add");
					});
					$(".favourite-toggle").click(function(e){
						e.preventDefault(e);
						favouriteToggle($(this).data("id"));
					});
				}
			},
			render: null
		}
	});
	
	$('#favourites').jplist({
		itemsBox: '.products',
		itemPath: '.product',
		panelPath: '.product-options',
		storage: "localstorage",
		storageName: "favourites-storage",
		dataSource: {
			type: 'server',
			server: {
				ajax:{
					url: '/server/server-html-favourites.php',
					dataType: 'html',
					type: 'POST'
				},
				serverOkCallback: function(serverData, statuses, ajax, response){
					$(".basket-add").click(function(e) {
						e.preventDefault(e);
						basketAdd($(this).data("id"),1,"add");
					});
					$(".favourite-toggle").click(function(e){
						e.preventDefault(e);
						favouriteToggle($(this).data("id"));
					});
					$(".favourite-remove").click(function(e){
						e.preventDefault(e);
						favouriteRemove($(this).data("id"));
					});
				}
			},
			render: null
		}
	});
	
	$('#orders').jplist({
		itemsBox: '.orders',
		itemPath: '.order',
		panelPath: '.order-options',
		storage: "localstorage",
		storageName: "orders-storage",
		dataSource: {
			type: 'server',
			server: {
				ajax:{
					url: '/server/server-html-orders.php',
					dataType: 'html',
					type: 'POST'
				},
				serverOkCallback: function(serverData, statuses, ajax, response){
					$(".basket-add").click(function(e) {
						e.preventDefault(e);
						basketAdd($(this).data("id"),1,"add");
					});
					$(".favourite-toggle").click(function(e){
						e.preventDefault(e);
						favouriteToggle($(this).data("id"));
					});
				}
			},
			render: null
		}
	});
	
	$('#events').jplist({
		itemsBox: '.events',
		itemPath: '.event',
		panelPath: '.event-options',
		dataSource: {
			type: 'server',
			server: {
				ajax:{
					url: '/server/server-html-events.php',
					dataType: 'html',
					type: 'POST'
				}
			},
			render: null
		}
	});
	
	$('#reviews').jplist({
		itemsBox: '.comments',
		itemPath: '.comment',
		panelPath: '.comment-options',
		storage: "localstorage",
		storageName: "reviews-storage",
		dataSource: {
			type: 'server',
			server: {
				ajax:{
					url: '/server/server-html-reviews.php',
					dataType: 'html',
					type: 'POST'
				},
				serverOkCallback: function(serverData, statuses, ajax, response){
					$(".profile").initial({
						charCount: 2,
						fontSize: 40
					});
					$(".profile-small").initial({
						charCount: 2,
						fontSize: 48,
						fontWeight: 700
					});
					$(".comment button").click(function(){
						var id = $(this).data("id");
						if($("#"+id).hasClass("hide")) {
							$("#"+id).removeClass("hide");
						} else {
							$("#"+id).addClass("hide");
						}
					});
				}
			},
			render: null
		}
	});
	
	$('#saved').jplist({
		itemsBox: '.products',
		itemPath: '.product',
		panelPath: '.product-options',
		dataSource: {
			type: 'server',
			server: {
				ajax:{
					url: '/server/server-html-saved.php',
					dataType: 'html',
					type: 'POST'
				},
				serverOkCallback: function(serverData, statuses, ajax, response){
					$(".order-edit").click(function(e){
						e.preventDefault(e);
						var order = $(this).data("id");
						orderEdit(order);
					});
					$(".order-save").submit(function(e){
						e.preventDefault(e);
						var order = $("input[frm_order]").val();
						var title = $("input[frm_title]").val();
						var description = $("input[frm_description]").val();
						orderSave(order,title,description);
					});
					$(".order-add").click(function(e){
						e.preventDefault(e);
						var order = $(this).data("id");
						var title = $("#order-"+order+" .title").text();
						$("#order-"+order+" .order-image").each(function(data) {
							var product = $(this).data("id");
							var quantity = $(this).find(".counter").text();
							basketAdd(product,quantity,"all");
						});
						var $toastContent = $("<div class='inner basket'><div class='container'><span><strong>"+title+"</strong> has been added to your basket.</span><a href='/basket' class='right'>View Basket</a></div></div>");
						M.toast({html: $toastContent, displayLength: 10000});
					});
				}
			},
			render: null,
		}
	});
	
});

// Login Form
$("#login").submit(function(e) {
	e.preventDefault(e);
	
	// Display loading and disable
	$("#login button[type=submit]").prop("disabled", true);
	$("#login button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
	
	$.ajax({
		cache: false,
		url: '/ajax/account.php',
		type: 'POST',
		dataType: 'json',
		data: {
			method: "login",
			username: $("input[name=username]").val(),
			password: $("input[name=password]").val()
		},
		success: function(data) {
			if(data.method == "incorrect") {
				$("#login .alert").text(data.message);
				$("#login .alert").addClass("warning");
				$("#login .alert").addClass("active");
				$("button[type=submit]").prop("disabled", false);
				$("button[type=submit]").html("Login");
			} else {
				$("button[type=submit]").prop("disabled", false);
				$("button[type=submit]").html("Login");
				window.location.replace("/account");
			}
		},
		error: function(data) {
			$("#login .alert").text("There was a problem with your details. Please try again.");
			$("#login .alert").addClass("error");
			$("#login .alert").addClass("active");
			$("button[type=submit]").prop("disabled", false);
			$("button[type=submit]").html("Login");
		}
	});
});

// Register Form
$("#register-form").submit(function(e) {
	e.preventDefault(e);
	
	if(formValidation()) {
		// Display loading and disable
		$("#register-form button[type=submit]").prop("disabled", true);
		$("#register-form button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
		
		// Process marketing selection
		if($("#register-form select[name=frm_hear]").val() == "other") var hear = $("textarea[name=frm_hear_more]").val();
		else var hear = $("#register-form select[name=frm_hear]").val();
		
		// Process newsletter selection
		if($("#register-form input[name=frm_newsletter]").is(":checked")) var newsletter = 1;
		else var newsletter = 0;
		
		// Process payment
		$.ajax({
			cache: false,
			url: "/ajax/account.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "register",
				title: $("#register-form select[name=frm_title]").val(),
				firstname: $("#register-form input[name=frm_firstname]").val(),
				lastname: $("#register-form input[name=frm_lastname]").val(),
				email: $("#register-form input[name=frm_email]").val(),
				username: $("#register-form input[name=frm_username]").val(),
				password: $("#register-form input[name=frm_password]").val(),
				postcode: $("#register-form input[name=frm_postcode]").val(),
				address: $("#register-form select[name=frm_address]").val(),
				town: $("#register-form input[name=frm_town]").val(),
				county: $("#register-form input[name=frm_county]").val(),
				country: $("#register-form select[name=frm_country]").val(),
				note: $("#register-form textarea[name=frm_note]").val(),
				telephone: $("#register-form input[name=frm_telephone]").val(),
				hear: hear,
				newsletter: newsletter,
			},
			success: function(data) {
				$("button[type=submit]").prop("disabled", false);
				$("button[type=submit]").html("<i class='material-icons'>check</i>");
				window.location.replace("/account");
			},
			error: function(data) {
				$("button[type=submit]").prop("disabled", false);
				$("button[type=submit]").html("Register");
				
			}
		});
	}
});

// Account Form
$("#account-form").submit(function(e) {
	e.preventDefault(e);
	
	if(formValidation()) {
		// Display loading and disable
		$("#account-form button[type=submit]").prop("disabled", true);
		$("#account-form button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
		
		// Process payment
		$.ajax({
			cache: false,
			url: "/ajax/account.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "edit",
				title: $("#account-form select[name=frm_title]").val(),
				firstname: $("#account-form input[name=frm_firstname]").val(),
				lastname: $("#account-form input[name=frm_lastname]").val(),
				email: $("#account-form input[name=frm_email]").val(),
				postcode: $("#account-form input[name=frm_postcode]").val(),
				address: $("#account-form #frm_address").val(),
				address2: $("#account-form input[name=frm_address2]").val(),
				town: $("#account-form input[name=frm_town]").val(),
				county: $("#account-form input[name=frm_county]").val(),
				country: $("#account-form select[name=frm_country]").val(),
				notes: $("#account-form textarea[name=frm_note]").val(),
				telephone: $("#account-form input[name=frm_telephone]").val(),
			},
			success: function(data) {
				$("button[type=submit]").prop("disabled", false);
				$("button[type=submit]").html("<i class='material-icons'>check</i>");
				location.reload();
			},
			error: function(data) {
			}
		});
	}
});

// Contact Form
$("#contact").submit(function(e) {
	e.preventDefault(e);
	
	if(formValidation()) {
		
		if(grecaptcha.getResponse()) {
	
			$(".g-recaptcha").next(".label-note").remove();
	
			// Display loading and disable
			$("#contact button[type=submit]").prop("disabled", true);
			$("#contact button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
		
			// Process payment
			$.ajax({
				cache: false,
				url: "/ajax/contact.php",
				type: 'POST',
				dataType: 'json',
				data: {
					method: "enquiry",
					title: $("#contact select[name=frm_title]").val(),
					firstname: $("#contact input[name=frm_firstname]").val(),
					lastname: $("#contact input[name=frm_lastname]").val(),
					email: $("#contact input[name=frm_email]").val(),
					phone: $("#contact input[name=frm_phone]").val(),
					message: $("#contact textarea[name=frm_message]").val()
				},
				success: function(data) {
					if(data.method == 1) {
						$("#contact .label-note").remove();
						$("#contact select, #contact input, #contact textarea").prop("disabled", true),
						$(".g-recaptcha").remove();
						$("button[type=submit]").prop("disabled", false);
						$("button[type=submit]").addClass("finished wide");
						$("button[type=submit]").html("We have received your enquiry and will contact you as soon as possible.");
					} else {
						if(!$("#contact button[type=submit]").next(".label-note").length) $("#contact button[type=submit]").after("<div class='label-note error'>There was an error submitting your enquiry. Please try again or email <a href='mailto:"+data.email+"?subject=Website Enquiry'>"+data.email+"</a> directly if this issue continues.</div>");
						$("button[type=submit]").prop("disabled", false);
						$("button[type=submit]").html("<i class='material-icons right'>chevron_right</i>Submit");
					}
				},
				error: function(data) {
					if(!$("#contact button[type=submit]").next(".label-note").length) $("#contact button[type=submit]").after("<div class='label-note error'>There was an error submitting your enquiry. Please try again or email <a href='mailto:"+data.email+"?subject=Website Enquiry'>"+data.email+"</a> directly if this issue continues.</div>");
					$("button[type=submit]").prop("disabled", false);
					$("button[type=submit]").html("<i class='material-icons right'>chevron_right</i>Submit");
				}
			});
			
		} else {
			if(!$(".g-recaptcha").next(".label-note").length) $(".g-recaptcha").after("<div class='label-note error'>Please prove you are not a robot.</div>");
		}
	}
});

// Review Form
$("#review").submit(function(e) {
	e.preventDefault(e);
	
	if(formValidation()) {
		
		if(grecaptcha.getResponse()) {
	
			$(".g-recaptcha").next(".label-note").remove();
	
			// Display loading and disable
			$("#review button[type=submit]").prop("disabled", true);
			$("#review button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
	
			// Process submission
			$.ajax({
				cache: false,
				url: "/ajax/review.php",
				type: 'POST',
				dataType: 'json',
				data: {
					method: "add",
					stock: $("input[name=frm_stock]").val(),
					name: $("input[name=frm_name]").val(),
					email: $("input[name=frm_email]").val(),
					rating: $("select[name=frm_rating]").val(),
					title: $("input[name=frm_title]").val(),
					description: $("textarea[name=frm_description]").val()
				},
				success: function(data) {
					$("#review button[type=submit]").prop("disabled", false);
					$("#review button[type=submit]").html("Publish");
					$("#review button[type=submit]").append("<div class='label-note error'>"+data.response+"</div>");
				//	form.submit();
				},
				error: function(data) {
				}
			});
		} else {
			if(!$(".g-recaptcha").next(".label-note").length) $(".g-recaptcha").after("<div class='label-note error'>Please prove you are not a robot.</div>");
		}
	}
});

// Forgot Password Form
$("#forgot-password").submit(function(e) {
	e.preventDefault(e);
	
	if(formValidation()) {
	
		// Display loading and disable
		$("#forgot-password button[type=submit]").prop("disabled", true);
		$("#forgot-password button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
	
		$.ajax({
			cache: false,
			url: "/ajax/account.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "forgot-password",
				email: $("input[name=frm_email]").val(),
			},
			success: function(data) {
				$("#forgot-password button[type=submit]").html("Reset my Password");
				if(!$("input[name=frm_email]").next(".label-note").length) {
					$("input[name=frm_email]").after("<div class='label-note'>"+data.message+"</div>");
				} else {
					$(".label-note").html(data.message)
				}
				if(data.response == 1) {
					$("input[name=frm_email]").prop("disabled", true);
					$(".label-note").addClass("success")
				} else {
					$("#forgot-password button[type=submit]").prop("disabled", false);
					$(".label-note").addClass("error")
				}
			},
			error: function(data) {
				if(!$("input[name=frm_email]").next(".label-note").length) {
					$("input[name=frm_email]").after("<div class='label-note error'>We were unable to reset your password. Please try again!</div>");
				} else {
					$(".label-note").text("We were unable to reset your password. Please try again!")
				}
			}
		});
	}
});

// Reset Password Form
$("#reset-password").submit(function(e) {
	e.preventDefault(e);
	
	if(formValidation()) {
	
		// Display loading and disable
		$("#reset-password button[type=submit]").prop("disabled", true);
		$("#reset-password button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
	
		$.ajax({
			cache: false,
			url: "/ajax/account.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "reset-password",
				customer: $("input[name=frm_customer]").val(),
				password: $("input[name=frm_password]").val()
			},
			success: function(data) {
				$("#reset-password button[type=submit]").html("Reset my Password");
				if(!$("input[name=frm_password]").next(".label-note").length) {
					$("input[name=frm_password]").after("<div class='label-note'>"+data.message+"</div>");
				} else {
					$(".label-note").html(data.message)
				}
				if(data.response == 1) {
					$("input[name=frm_password]").prop("disabled", true);
					$(".label-note").addClass("success")
					window.location.replace("/account");
				} else {
					$("#reset-password button[type=submit]").prop("disabled", false);
					$(".label-note").addClass("error")
				}
			},
			error: function(data) {
				$("#reset-password button[type=submit]").html("Reset my Password");
				$("#reset-password button[type=submit]").prop("disabled", false);
				if(!$("input[name=frm_password]").next(".label-note").length) {
					$("input[name=frm_password]").after("<div class='label-note error'>We were unable to reset your password. Please try again!</div>");
				} else {
					$(".label-note").text("We were unable to reset your password. Please try again!")
				}
			}
		});
	}
});

// Discount Code Form
$("#discount").submit(function(e) {
	e.preventDefault(e);
	
	// Display loading and disable
	$("#discount button[type=submit]").prop("disabled", true);
	$("#discount button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
	
	$.ajax({
		cache: false,
		url: "/ajax/basket.php",
		type: 'POST',
		dataType: 'json',
		data: {
			method: "discount",
			code: $("input[name=frm_discount]").val()
		},
		success: function(data) {
			$("#discount button[type=submit]").prop("disabled", false);
			$("#discount button[type=submit]").html("Apply");
			
			$("#basket-total").html("&pound;"+numberFormat(data.baskettotal,2));
			$("#basket-final").html("&pound;"+numberFormat(data.baskettotal,2));
			$("#basket-mobile").html("&pound;"+numberFormat(data.baskettotal,2));
			
			if(data.discountvalue > 0) {
				$("#discount-on").removeClass("hide");
				if(data.discounttoggle > 0) $("#discount-remove").removeClass("hide");
				else $("#discount-remove").addClass("hide");
				$("#discount-label").text(data.discountlabel);
			} else {
				$("#discount-on").addClass("hide");
				if(!$("input[name=frm_discount]").next(".label-note").length) $("input[name=frm_discount]").after("<div class='label-note error'>"+data.message+"</div>");
			}
			
			$("#discount-value").text("- £"+data.discountvalue);
			
		},
		error: function(data) {
			// Failure
		}
	});
});

// Discount Code Form
$("#discount-remove").click(function(e) {
	e.preventDefault(e);
	$.ajax({
		cache: false,
		url: "/ajax/basket.php",
		type: 'POST',
		dataType: 'json',
		data: {
			method: "discountremove",
			code: $("input[name=frm_discount]").val()
		},
		success: function(data) {			
			$("#basket-total").html("&pound;"+numberFormat(data.baskettotal,2));
			$("#basket-final").html("&pound;"+numberFormat(data.baskettotal,2));
			$("#basket-mobile").html("&pound;"+numberFormat(data.baskettotal,2));
			$("#discount-on").addClass("hide");
			$("input[name=frm_discount]").val();			
		},
		error: function(data) {
			// Failure
		}
	});
});

	$("#frm_country").change(function(){
		if($(this).val() == "United Kingdom") {
			$('#postcodeLookupToggle').html("(Enter Manually)");
			if($('#pcButton').hasClass('hide')) {
				$('#pcButton').removeClass('hide');
			}
		} else {
			$('#postcodeLookupToggle').html("");
			
			if(!$('#pcButton').hasClass('hide')) {
				$('#pcButton').addClass('hide');
			}
			if($('#address').hasClass('hide')) {
				$('#address').removeClass('hide');
			}

			var add = $('#frm_address').val();
			$('#address1').html("<input type='text' class='required depend' id='frm_address' name='frm_address' tabindex='7' value='"+ add + "' />");
			
		}
		
		//pass country to ajax
	});

// Newsletter Subscribe Form
$("#newsletter").submit(function(e) {
	e.preventDefault(e);
	
	// Display loading and disable
	$("#newsletter button[type=submit]").prop("disabled", true);
	$("#newsletter button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");

	if($("input[name=frm_subscribe]").val() == "" || $("input[name=frm_subscribe]").val() == null) {
		$("input[name=frm_subscribe]").addClass("error");
		$("input[name=frm_subscribe]").prop("disabled", false);
		$("input[name=frm_subscribe]").after("<div class='label-note error overlap'>Please enter a valid email address!</div>");
		$("#newsletter button[type=submit]").prop("disabled", false);
		$("#newsletter button[type=submit]").html("Subscribe");
		result = false;
	} else {
		$.ajax({
			cache: false,
			url: "/ajax/contact.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "newsletter",
				email: $("input[name=frm_subscribe]").val(),
			},
			success: function(data) {
				if(data.method == 1) {
					$("input[name=frm_subscribe]").addClass("success");
					$("input[name=frm_subscribe]").prop("disabled", true);
					$("input[name=frm_subscribe]").after("<div class='label-note success overlap'>"+data.message+"</div>");
					
					$("#newsletter button[type=submit]").prop("disabled", true);
					$("#newsletter button[type=submit]").html("<i class='material-icons'>check</i>");
				} else {
					$("input[name=frm_subscribe]").addClass("error");
					$("input[name=frm_subscribe]").prop("disabled", false);
					$("input[name=frm_subscribe]").after("<div class='label-note error overlap'>"+data.message+"</div>");
					
					$("#newsletter button[type=submit]").prop("disabled", false);
					$("#newsletter button[type=submit]").html("Subscribe");
				}
			},
			error: function(data) {
				$("input[name=frm_subscribe]").addClass("error");
				if($("input[name=frm_subscribe]").data("label") !== undefined && !$("input[name=frm_subscribe]").next(".label-note").length) $("input[name=frm_subscribe]").after("<div class='label-note error'>Your "+$("input[name=frm_subscribe]").data("label")+" is invalid.</div>");
				$("#newsletter button[type=submit]").prop("disabled", false);
				$("#newsletter button[type=submit]").html("Subscribe");
			}
		});
	}
		
	$("input[name=frm_subscribe].error").change(function(e){
		$(this).removeClass("error");
		if($(this).next(".label-note") != "") {
			$(this).next(".label-note").remove();
		}
	});
	

});
$("#subscribe").submit(function(e) {
	e.preventDefault(e);
	
	// Display loading and disable
	$("#subscribe button[type=submit]").prop("disabled", true);
	$("#subscribe button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");

	if($("input[name=frm_newsletter]").val() == "" || $("input[name=frm_newsletter]").val() == null) {
		$("input[name=frm_newsletter]").addClass("error");
		$("input[name=frm_newsletter]").prop("disabled", false);
		$("input[name=frm_newsletter]").after("<div class='label-note error overlap'>Please enter a valid email address!</div>");
		$("#subscribe button[type=submit]").prop("disabled", false);
		$("#subscribe button[type=submit]").html("Subscribe");
		result = false;
	} else {
		$.ajax({
			cache: false,
			url: "/ajax/contact.php",
			type: 'POST',
			dataType: 'json',
			data: {
				method: "newsletter",
				email: $("input[name=frm_newsletter]").val(),
			},
			success: function(data) {
				if(data.method == 1) {
					$("input[name=frm_newsletter]").addClass("success");
					$("input[name=frm_newsletter]").prop("disabled", true);
					$("input[name=frm_newsletter]").after("<div class='label-note success overlap'>"+data.message+"</div>");
					
					$("#subscribe button[type=submit]").prop("disabled", true);
					$("#subscribe button[type=submit]").html("<i class='material-icons'>check</i>");
					$.cookie("newsletter", 1);
				} else {
					$("input[name=frm_newsletter]").addClass("error");
					$("input[name=frm_newsletter]").prop("disabled", false);
					$("input[name=frm_newsletter]").after("<div class='label-note error overlap'>"+data.message+"</div>");
					
					$("#subscribe button[type=submit]").prop("disabled", false);
					$("#subscribe button[type=submit]").html("Subscribe");
				}
			},
			error: function(data) {
				$("input[name=frm_newsletter]").addClass("error");
				if($("input[name=frm_newsletter]").data("label") !== undefined && !$("input[name=frm_newsletter]").next(".label-note").length) $("input[name=frm_subscribe]").after("<div class='label-note error'>Your "+$("input[name=frm_subscribe]").data("label")+" is invalid.</div>");
				$("#subscribe button[type=submit]").prop("disabled", false);
				$("#subscribe button[type=submit]").html("Subscribe");
			}
		});
	}
		
	$("input[name=frm_newsletter].error").change(function(e){
		$(this).removeClass("error");
		if($(this).next(".label-note") != "") {
			$(this).next(".label-note").remove();
		}
	});
	
});