// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
	base: {
		color: '#32325d',
		fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
		fontSmoothing: 'antialiased',
		fontSize: '15px',
		'::placeholder': {
			color: '#222222'
		}
	},
	invalid: {
		color: '#fa755a',
		iconColor: '#fa755a'
	}
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
	var displayError = document.getElementById('card-errors');
	if (event.error) {
		displayError.textContent = event.error.message;
	} else {
		displayError.textContent = '';
	}
});

function stripeTokenHandler(token) {
	// Insert the token ID into the form so it gets submitted to the server
	var form = document.getElementById('payment-form');
	var hiddenInput = document.createElement('input');
	hiddenInput.setAttribute('type', 'hidden');
	hiddenInput.setAttribute('name', 'stripeToken');
	hiddenInput.setAttribute('value', token.id);
	form.appendChild(hiddenInput);	
	
	// Display loading and disable
	$("#payment-form button[type=submit]").prop("disabled", true);
	$("#payment-form button[type=submit]").html("<div class='preloader-wrapper active'><div class='spinner-layer'><div class='circle-clipper left'><div class='circle'></div></div><div class='gap-patch'><div class='circle'></div></div><div class='circle-clipper right'><div class='circle'></div></div></div></div>");
	
	// Process newsletter selection
	if($("input[name=frm_newsletter]").is(":checked")) var newsletter = 1;
	else var newsletter = 0;
	
	// Process marketing selection
	if($("select[name=frm_hear]").val() == "other") var hear = $("textarea[name=frm_hear_more]").val();
	else var hear = $("select[name=frm_hear]").val();
	
	// Process payment
	$.ajax({
		cache: false,
		url: "/ajax/payment.php",
		type: 'POST',
		dataType: 'json',
		data: {
			title: $("select[name=frm_title]").val(),
			firstname: $("input[name=frm_firstname]").val(),
			lastname: $("input[name=frm_lastname]").val(),
			email: $("input[name=frm_email]").val(),
			postcode: $("input[name=frm_postcode]").val(),
			address: $("#frm_address").val(),
			town: $("input[name=frm_town]").val(),
			county: $("input[name=frm_county]").val(),
			country: $("select[name=frm_country]").val(),
			note: $("textarea[name=frm_note]").val(),
			telephone: $("input[name=frm_telephone]").val(),
			date: $("input[name=frm_date]").val(),
			time: $("select[name=frm_time]").val(),
			hear: hear,
			newsletter: newsletter,
			token: token.id
		},
		success: function(data) {
			$("button[type=submit]").prop("disabled", false);
			$("button[type=submit]").html("<i class='material-icons'>check</i>");
			window.location.replace("/confirmation/success/"+data.id);
		},
		error: function(data) {
			$('#order-error').modal('close');
		}
	});
}

function validatePhone(txtPhone) {
    var a = $(txtPhone).val();
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(a)) {
        return true;
    } else {
        return false;
    }
}

function stripeFormValidation() {
	var result = true;
	$(".required").removeClass("error");
	$(".required").each(function() {
		if($(this).val() == "" || $(this).val() == null) {
			$(this).addClass("error");
			if($(this).data("label") !== undefined && !$(this).next(".label-note").length) $(this).after("<div class='label-note error'>Your "+$(this).data("label")+" is invalid.</div>");
			result = false;
		}
	})
	if(($(".depend").val() == "" || $(".depend").val() == null) && ($(".leader").val() != "" & $(".leader").val() != null)) {
		$(".leader").addClass("error");
		$(".leader").after("<div class='label-note error'>We did not recognise this address. <a href='#'>Find address</a>.</div>");
		return false;
	}
	$("input.error, textarea.error, select.error").change(function(e){
		$(this).removeClass("error");
		if($(this).next(".label-note") != "") {
			$(this).next(".label-note").remove();
		}
	});
	return result;
}

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
	event.preventDefault();	
	if(stripeFormValidation()) {
		stripe.createToken(card).then(function(result) {
			if(result.error) {
				// Inform the user if there was an error.
				var errorElement = document.getElementById('card-errors');
				errorElement.textContent = result.error.message;
			} else {
				// Send the token to your server.
				stripeTokenHandler(result.token);
			}
		});
	}
});