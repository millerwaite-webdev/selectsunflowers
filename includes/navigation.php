<?php
	
	print("<div class='top'>");
		print("<div class='container'>");	
			if(count($socialLinks) > 0) {
				print("<ul class='social hide-on-small-only hide-on-large-only'>");
					foreach($socialLinks AS $socialLink) {
						print("<li>");
							print("<a class='icon ".strtolower($socialLink['socialName'])."' href='".$socialLink['socialLink']."' target='_blank'>");
								print($socialLink['siteIcon']);
							print("</a>");
						print("</li>");
					}
				print("</ul>");
			} else {				
			/*	print("<ul class='offer hide-on-small-only'>");
					print("<li>");
						print("Order Now, get it <u>".expectedDelivery(time(), "uk")."</u>");
					print("</li>");
				print("</ul>");*/
			}
			print("<ul class='right'>");
				print("<li><a href='/shipping-and-handling'>Shipping &amp; Handling</a></li>");
				print("<li><a href='/terms-and-conditions'>Terms &amp; Conditions</a></li>");
				print("<li><a href='/contact-us'>Contact Us</a></li>");
				if($accountid > 0) print("<li><a href='#' id='logout'>Logout</a></li>");
			//	print("<li class='hide'><a href='#modal-newsletter' class='modal-trigger'>Newsletter</a></li>");
			print("</ul>");
		print("</div>");
	print("</div>");
	
	// Main Menu
	print("<header>");
		print("<div class='container'>");
			print("<div class='header-wrapper'>");
				print("<div class='header-logo'>");
					print("<a href='".$strsiteurl."' class='brand-logo'>");
						print("<img src='/images/company/logo-horizontal.png' alt='".$compName."' />");
					print("</a>");
				print("</div>");
				
				// Product Search
				print("<div class='search' itemscope itemtype='https://schema.org/WebSite'>");
					print("<meta itemprop='url' content='".$strsiteurl."' />");
					print("<form id='search' method='post' action='/shop' itemprop='potentialAction' itemscope itemtype='https://schema.org/SearchAction'>");
					
						print("<meta itemprop='target' content='".$strsiteurl."shop#title-filter:value={search}' />");
					
						// Category
						$strdbsql = "SELECT category.contentName AS name FROM category 
						INNER JOIN category_relations ON category.recordID = category_relations.childID 
						WHERE category_relations.parentID = 1";
						$category = query($conn,$strdbsql,"single");
						
						// Subcategories
						$strdbsql = "SELECT category.contentName AS name, category.metaPageLink AS link FROM category 
						INNER JOIN category_relations ON category.recordID = category_relations.childID 
						WHERE category_relations.parentID = 1 AND category.recordID <> category_relations.parentID 
						ORDER BY menuOrder";
						$subcategories = query($conn,$strdbsql,"multi");
					
						// Dropdown
						if(count($subcategories) > 0) {
							print("<label class='hide' for='search_categories'>Search In Categories</label>");
							print("<select class='nice-select' id='search_categories' name='category'>");
								print("<option value='' selected>Browse ".$category['name']."</option>");
								foreach($subcategories AS $subcategory) {
									print("<option value='".$subcategory['link']."' ".($_REQUEST['category'] == $subcategory['link'] ? "selected" : "").">".$subcategory['name']."</option>");
								}
							print("</select>");
						}
						
						// Textbox
						print("<div class='input-field'>");
							print("<button type='submit' class='btn transparent isolate postfix center'>");
								print("<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'><defs><style>.cls-1{fill:none;stroke:#ababab;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs><title>search</title><g><g><circle class='cls-1' cx='9' cy='9' r='8'></circle><line class='cls-1' x1='19' y1='19' x2='14.65' y2='14.65'></line></g></g></svg>");
							print("</button>");
							print("<label class='hide' for='header_search'>Search Sunflowers</label>");
							print("<input type='text' name='search' id='header_search' value='".$_REQUEST['search']."' placeholder='Search Sunflowers' itemprop='query-input' />");
						print("</div>");
						
					print("</form>");
					
					print("<div class='buttons'>");
						if($accountid > 0) print("<a href='/account' class='btn large transparent text hide-on-med-and-down'>");
						else print("<a href='#modal-login' class='btn large transparent text modal-trigger hide-on-med-and-down'>");
							print("<svg class='left' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 18 20'><defs><style>.cls-3{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs><title>your-account</title><g><g><path class='cls-3' d='M17,19V17a4,4,0,0,0-4-4H5a4,4,0,0,0-4,4v2'></path><circle class='cls-3' cx='9' cy='5' r='4'></circle></g></g></svg>");
							print("<span class='large'>My Account</span>");
							print("<span>".($accountid > 0 ? "Hello, ".$customerfirstname : "Login/Register")."</span>");
						print("</a>");
						print("<div class='dropdown-wrapper'>");
							//print("<a href='#' class='btn btn-secondary large waves-effect waves-light basket-trigger' data-target='dropdown-basket'>");
							print("<a href='/basket' class='btn btn-secondary large'>");
								print("<span class='badge".(count($basketcount) > 0 ? "" : " hide")."'>".$basketcount."</span>");
								print("<svg class='left' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 22'><defs><style>.cls-2{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs><title>shopping-bag</title><g><g><path class='cls-2' d='M4,1,1,5V19a2,2,0,0,0,2,2H17a2,2,0,0,0,2-2V5L16,1Z'></path><line class='cls-2' x1='1' y1='5' x2='19' y2='5'></line><path class='cls-2' d='M14,9A4,4,0,0,1,6,9'></path></g></g></svg>");
								print("<span class='large'>Checkout<i class='material-icons'>chevron_right</i></span>");
								print("<span id='basket-mobile'>Total: &pound;".number_format($baskettotal,2)."</span>");
							print("</a>");
							print("<div id='dropdown-basket' class='dropdown-content basket'>");
								print("<span>Your Basket is Empty!</span>");
							print("</div>");
						print("</div>");
						print("<a href='#' data-target='mobile-demo' class='button-collapse sidenav-trigger right'><i class='material-icons'>menu</i></a>");
					print("</div>");
				print("</div>");
			//	print("<a href='/basket' class='button-basket right hide-on-large-only'><i class='fas fa-shopping-basket'></i> <span id='basket-mobile'>&pound;".$baskettotal."</span></a>");
				print("</div>");
			print("</div>");
			print("<nav class='hide-on-small-only'>");
				print("<div class='container'>");
					print("<div class='nav-wrapper'>");
						
						if(count($menuLinks) > 0) {
							print("<ul>");
								foreach($menuLinks AS $menuLink) {
									$strdbsql = "SELECT * FROM site_pages WHERE visible = 1 AND parentPageID = :parent ORDER BY pageOrder";
									$result = query($conn, $strdbsql, "multi", array("parent"=>$menuLink['recordID']));
									if(count($result) > 0) {
										print("<li".($strPage == $menuLink['pageName'] ? " class='active'" : "").">");
											print("<a class='".$menuLink['pageName']." dropdown-trigger' data-target='".$menuLink['pageName']."-dropdown' href='".$strsiteurl.$menuLink['pageName']."'>".$menuLink['pageTitle']."<i class='fa fa-angle-down right'></i></a>");
											print("<ul id='".$menuLink['pageName']."-dropdown' class='dropdown-content'>");
												foreach($result AS $row) {
													print("<li><a href='".$strsiteurl.$row['pageName']."'>".$row['pageTitle']."</a></li>");
												}
											print("</ul>");
										print("</li>");
									} else {
										print("<li".($strPage == $menuLink['pageName'] ? " class='active'" : "")."><a class='".$menuLink['pageName']."' href='".$strsiteurl.$menuLink['pageName']."'>".$menuLink['pageTitle']."</a></li>");
									}
								}
							print("</ul>");
						}
						
						print("<ul>");
						
							// Dropdown
						/*	print("<li>");
								print("<a href='".$strsiteurl."shop/new-products' class='dropdown-trigger' data-target='new-dropdown'>New Sunflowers<i class='fa fa-angle-down right'></i></a>");
								print("<div id='new-dropdown' class='dropdown-content'>");
									print("<ul>");
										print("<li><a href='".$strsiteurl."'>Link 1</a></li>");
										print("<li><a href='".$strsiteurl."'>Link 2</a></li>");
										print("<li><a href='".$strsiteurl."'>Link 3</a></li>");
									print("</ul>");
								print("</div>");
							print("</li>");*/
							print("<li".($_REQUEST['category'] == "new" ? " class='active'" : "")."><a href='".$strsiteurl."shop/new'>New Sunflowers</a></li>");
							print("<li".($_REQUEST['category'] == "popular" ? " class='active'" : "")."><a href='".$strsiteurl."shop/popular'>Popular Picks</a></li>");
						//	print("<li class='badge featured".($_REQUEST['category'] == "mothers-day" ? " active" : "")."'><a href='".$strsiteurl."shop/mothers-day'>Mother's Day Specials</a></li>");
							print("<li".($_REQUEST['category'] == "spring" ? " class='active'" : "")."><a href='".$strsiteurl."shop/spring'>Spring Bloomers</a></li>");
						print("</ul>");
						
						print("<ul class='right'>");
						//	if($accountid > 0) print("<li><a href='".$strsiteurl."account/favourites'><i class='material-icons left'>favorite_border</i>My Wishlist</a></li>");
							print("<li><a href='".$strsiteurl."news' class='disabled'><i class='material-icons left'>help_outline</i>Tips for Growing Perfect Flowers</a></li>");
						print("</ul>");
						
					print("</div>");
				print("</div>");
			print("</nav>");
		print("</div>");
	print("</header>");
	
	// Responsive Menu
	print("<div class='sidenav' id='mobile-demo'>");
		print("<ul>");
			foreach($menuLinks AS $menuLink) {
				print("<li".($strPage == $menuLink['pageName'] ? " class='active'" : "")."><a href='".$strsiteurl.$menuLink['pageName']."'>".$menuLink['pageTitle']."</a></li>");
			}
		print("</ul>");
		print("<p class='copy show-on-medium-and-down'>&copy; ".date("Y")." ".$compName.". All Rights Reserved. Powered by <a href='https://millerwaite.co.uk/' target='_blank'>Miller Waite</a></p>");
	print("</div>");

?>