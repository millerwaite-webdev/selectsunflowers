<?php

	$strdbsql = "SELECT * FROM site_promotions WHERE enabled = 1 ORDER BY displayOrder";
	$promotions = query($conn,$strdbsql,"multi");
	
	foreach($promotions AS $key => $value) {
		switch($value['type']) {
			case 1: $promotions[$key]["cols"] = "s12 m12 l6"; $promotions[$key]["size"] = "large"; $promotions[$key]["type"] = "slider"; $promotions[$key]["slider"] = true; break;
			case 2: $promotions[$key]["cols"] = "s6 m12 l12"; $promotions[$key]["size"] = "small"; $promotions[$key]["type"] = "link"; $promotions[$key]["slider"] = false; break;
			case 3: $promotions[$key]["cols"] = "s12 m6 l3"; $promotions[$key]["size"] = "small"; $promotions[$key]["type"] = "button"; $promotions[$key]["slider"] = false; break;
		}
	}
	
	$sliders = [];
	
	foreach($promotions AS $key => $value) {
		if($value['slider']) {
			$sliders[] = $promotions[$key];
			unset($promotions[$key]);
		}
	}
	
	$promotions = array_slice($promotions, 0, 2);
	
	if(count($promotions) > 0 || count($sliders > 0)) {
		print("<section class='promotions'>");
			print("<div class='row thin crop-bottom'>");
			
				$strdbsql = "SELECT * FROM category WHERE showMenu = 1 LIMIT 9";
				$categories = query($conn,$strdbsql,"multi");
				
				if(count($categories) > 0) {
					print("<div class='col s12 m12 l3'>");
						print("<div class='collection-menu'>");
							print("<h5><a href='".$strsiteurl."shop'><i class='material-icons right'>menu</i></a><span>Varieties</span></h5>");
							print("<ul>");
								foreach($categories AS $category) {
									print("<li><a href='".$strsiteurl."shop/".$category['metaPageLink']."'>".$category['contentName']."<i class='fa fa-angle-right right'></i></a></li>");
								}
								print("<li><a href='".$strsiteurl."shop' class='highlight'>View All Varieties<i class='fa fa-angle-right right'></i></a></li>");
							print("</ul>");
						print("</div>");
					print("</div>");
				}
		
				if(count($sliders) > 0) {
					print("<div class='col s12 m12 l6'>");
						print("<div class='promo-slider'>");
							foreach($sliders AS $key => $value) {
								print("<div class='slick-slide'>");
									print("<div class='card ".$value['size']." ".$value['type']."'>");
										print("<div class='card-content ".($value['type'] == "main" || $value['type'] == "button" ? "center" : "")."' ".(!empty($value['image']) ? "style='background-image:url(/images/promotions/".$value['image'].");'" : "").">");
											print("<div class='wrapper'>");
													
												if(!empty($value['sticker'])) print("<div class='sticker'><img src='/images/elements/".$value['sticker']."' alt='".$value['title']."' /></div>");
													
												// Titles
												if($value['type'] == "main") {
													if(!empty($value['title'])) print("<h1>".$value['title']."</h1>");
													if(!empty($value['subtitle'])) print("<h2>".$value['subtitle']."</h2>");
												} else {
													if(!empty($value['subtitle'])) print("<h4>".$value['subtitle']."</h4>");
													if(!empty($value['title'])) print("<h3>".$value['title']."</h3>");
												}
												
												// Description
												if(!empty($value['description'])) print("<p>".$value['description']."</p>");
												
												// Buttons
												if($value['type'] != "button") {
													if(!empty($value['label1']) && !empty($value['link1'])) print("<a class='btn btn-primary' href='/".$value['link1']."'>".$value['label1']."</a>");
													if(!empty($value['label2']) && !empty($value['link2'])) print("<a class='btn btn-secondary' href='/".$value['link2']."'>".$value['label2']."</a>");
												}
												
											print("</div>");
										print("</div>");
									print("</div>");
								print("</div>");
							}
						print("</div>");
					print("</div>");
				}
				
				if(count($promotions) > 0) {
					print("<div class='col s12 m12 l3'>");
						foreach($promotions AS $key => $value) {
							if($value['type'] == "button") print("<a href='".$value['link1']."'>");
								print("<div class='card ".$value['size']." ".$value['type']."'>");
									print("<div class='card-content ".($value['type'] == "main" || $value['type'] == "button" ? "center" : "")."' ".(!empty($value['image']) ? "style='background-image:url(/images/promotions/".$value['image'].");'" : "").">");
										print("<div class='wrapper'>");
										
											if(!empty($value['sticker'])) print("<div class='sticker'><img src='/images/elements/".$value['sticker']."' alt='".$value['title']."' /></div>");
												
											// Titles
											if($value['type'] == "main") {
												if(!empty($value['title'])) print("<h1>".$value['title']."</h1>");
												if(!empty($value['subtitle'])) print("<h2>".$value['subtitle']."</h2>");
											} else {
												if(!empty($value['subtitle'])) print("<h4>".$value['subtitle']."</h4>");
												if(!empty($value['title'])) print("<h3>".$value['title']."</h3>");
											}
											
											// Description
											if(!empty($value['description'])) print("<p>".$value['description']."</p>");
											
											// Buttons
											if($value['type'] != "button") {
												if(!empty($value['label1']) && !empty($value['link1'])) print("<a class='btn btn-primary small' href='/".$value['link1']."'>".$value['label1']."</a>");
												if(!empty($value['label2']) && !empty($value['link2'])) print("<a class='btn btn-secondary small' href='/".$value['link2']."'>".$value['label2']."</a>");
											}
										
										print("</div>");
									print("</div>");
								print("</div>");
							if($value['type'] == "button") print("</a>");
						}
					print("</div>");
				}
				
			print("</div>");
		print("</section>");
	}

?>