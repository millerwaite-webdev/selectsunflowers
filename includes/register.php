<?php
		
	if($accountid == 0) {
		
		print("<div class='heading crop-bottom'>");
			print("<h1>Register</h1>");
		print("</div>");

		print("<div class='row'>");
			print("<div class='col s12 m12 l6'>");						
				print("<form id='register-form' class='checkout' name='register-form' action='/account' method='post'>");
				
					// Customer Details
					print("<div class='box'>");
						print("<h2 class='h3'>Your Details</h2>");
						print("<div class='row crop-bottom'>");
						
							// Title
							$titles = array();
							
							$titles[0] = array("label"=>"Mr","value"=>"Mr");
							$titles[1] = array("label"=>"Miss","value"=>"Miss");
							$titles[2] = array("label"=>"Mrs","value"=>"Mrs");
							$titles[3] = array("label"=>"Ms","value"=>"Ms");
							$titles[4] = array("label"=>"Other","value"=>"");
							
							print("<div class='col s12 m12 l2'>");
								print("<div class='input-field'>");
									print("<select class='browser-default required' id='frm_title' name='frm_title' aria-label='Title' tabindex='1'>");
										print("<option value='' disabled".($accountid == 0 ? " selected" : "").">Title</option>");
										foreach($titles AS $title) {
											print("<option value='".$title['value']."'>".$title['label']."</option>");
										}
									print("</select>");
								print("</div>");
							print("</div>");
							print("<div class='col s12 m12 l5'>");
								print("<div class='input-field'>");
									print("<input type='text' class='required' id='frm_firstname' name='frm_firstname' aria-label='First Name' tabindex='2' placeholder='First Name' value='' />");
								print("</div>");
							print("</div>");
							print("<div class='col s12 m12 l5'>");
								print("<div class='input-field'>");
									print("<input type='text' class='required' id='frm_lastname' name='frm_lastname' tabindex='2' aria-label='Last Name' placeholder='Last Name' value='' />");
								print("</div>");
							print("</div>");
							print("<div class='col s12'>");
								print("<div class='input-field'>");
									print("<input type='email' class='required' id='frm_email' name='frm_email' tabindex='4' aria-label='Email Address' placeholder='Email Address' value='' />");
								print("</div>");
							print("</div>");
						print("</div>");
					print("</div>");
					
					// Account
					print("<div class='box'>");
						print("<h3>Login Information</h3>");
						print("<div class='row crop-bottom'>");
							print("<div class='col s12 m12 l6'>");
								print("<div class='input-field'>");
									print("<input type='text' class='required' id='frm_username' aria-label='Username' name='frm_username' tabindex='5' placeholder='Username' />");
								print("</div>");
							print("</div>");
							print("<div class='col s12 m12 l6'>");
								print("<div class='input-field'>");
									print("<input type='password' class='required' id='frm_password' aria-label='Password' name='frm_password' tabindex='6' placeholder='Password' />");
								print("</div>");
							print("</div>");
							print("<div id='modal-password' class='modal'>");
								print("<div class='modal-content'>");
									print("<h2 class='crop-bottom'><i class='material-icons right modal-close'>close</i>Why should I create an account?</h2>");
									print("<p>An account will be created for all cu</p>");
								print("</div>");
							print("</div>");
						print("</div>");
					print("</div>");
					
					
					// Delivery Address
					print("<div class='box'>");
						print("<h3>Delivery Address</h3>");
						print("<div class='row crop-bottom'>");

							print("<div class='col s12 m10 l8'>");
								print("<div class='input-field'>");
									print("<select class='required browser-default aria-label='country' id='frm_country' name='frm_country' data-label='country' tabindex='6' placeholder='Country' >");
									$q = "SELECT configCountries.countryName FROM configCountries INNER JOIN deliveryLocations ON configCountries.zone = deliveryLocations.zone WHERE deliveryLocations.allowDelivery = 1 ORDER BY deliveryLocations.recordID, configCountries.countryName";
									$r = query($conn,$q,"multi");
									foreach($r AS $c) {
										print("<option value='".$c['countryName']."' ".((!empty($customercountry) && $customercountry == $c['countryName'])  ? " selected" : "").">".$c['countryName']."</option>");
									}
									print("</select>");
								print("</div>");
							print("</div>");
							
							// Postcode
							print("<div class='col s12 m10 l8'>");
								print("<div class='input-field'>");
									print("<button id='pcButton' class='btn btn-primary small postfix postcode-search'>Find Address</button>");
									print("<input type='text' aria-label='Postal Code' class='required leader code' id='frm_postcode' name='frm_postcode' data-label='postcode' maxlength='7' tabindex='6' placeholder='Postcode' value='".$customerpostcode."' />");
								print("</div>");
							print("</div>");
							print("<div class='col s12 m12 l4'>");
								print("<div class='input-field'>");
									print("<a id='postcodeLookupToggle' href='javascript:pcLookup()' class='btn transparent'>(Enter Manually)</a>");
								print("</div>");
							print("</div>");
							
							print("<div id='address'".(empty($customeraddress1) && empty($customertown) && empty($customercounty) ? " class='hide'" : "").">");
								print("<div class='col s12'>");
									print("<div class='input-field' id='address1'>");
										print("<input type='text' class='required depend' id='frm_address' name='frm_address' aria-label='Street Address' tabindex='7' value='".$customeraddress1."' />");
									print("</div>");
								print("</div>");
								print("<div class='col s12 m12 l6'>");
									print("<div class='input-field'>");
										print("<input type='text' class='required depend' id='frm_town' name='frm_town' aria-label='Town' tabindex='8' value='".$customertown."' />");
									print("</div>");
								print("</div>");
								print("<div class='col s12 m12 l6'>");
									print("<div class='input-field'>");
										print("<input type='text' class='required depend' id='frm_county' aria-label='County' name='frm_county' tabindex='9' value='".$customercounty."' />");
									print("</div>");
								print("</div>");
							print("</div>");
							
						print("</div>");
					print("</div>");
					
					// Contact Information
					print("<div class='box'>");
						print("<h3>Contact Information</h3>");
						print("<div class='row'>");
						
							// Telephone
							print("<div class='col s12'>");
								print("<div class='input-field'>");
									print("<input type='tel' class='required".(!empty($customertelephone) ? " autocomplete" : "")."' aria-label='Telephone' name='frm_telephone' data-label='contact number' tabindex='8' maxlength='11' placeholder='Telephone/Mobile Number' value='".$customertelephone."' />");
								print("</div>");
							print("</div>");
							
							// Marketing
							print("<div class='col s12'>");
								print("<div class='input-field'>");
									print("<label for='frm_hear'>How did you hear about us?</label>");
									print("<select class='browser-default' id='frm_hear' name='frm_hear' data-extra='marketing' tabindex='9'>");
										print("<option value='' disabled selected>Please Select</option>");
										print("<option value='Recommended by a friend'>Recommended by a friend</option>");
										print("<option value='Social media'>Social media</option>");
										print("<option value='Internet'>Internet</option>");
										print("<option value='other'>Other</option>");
									print("</select>");
								print("</div>");
							print("</div>");
							
							// Additional Marketing
							print("<div id='marketing' class='col s12 hide'>");
								print("<div class='input-field'>");
									print("<textarea class='materialize-textarea' id='frm_hear_more' aria-label='Additional Information' name='frm_hear_more' tabindex='9' data-length='500' maxlength='500' placeholder='For example: I Discovered one of your leaflets at a local supermarket.'></textarea>");
								print("</div>");
							print("</div>");
						print("</div>");

						// Marketing
						print("<div class='row crop-bottom'>");
							print("<div class='col s12'>");
								print("<div class='input-field'>");
									print("<label for='frm_newsletter'>");
										print("<input type='checkbox' class='filled-in' id='frm_newsletter' name='frm_newsletter' tabindex='10' />");
										print("<span>Tick this one to hear about our ".$compName." events, exclusive offers, insider news and much more (no rubbish we promise).</span>");
									print("</label>");
								print("</div>");
							print("</div>");
						print("</div>");
					print("</div>");
					
					print("<div class='input-field'>");
						print("<button type='submit' tabindex='11' class='btn btn-primary wide'>Register</button>");
					print("</div>");
					
				print("</form>");
			print("</div>");
		print("</div>");
		
	} else {
		
		?>
		
		<script>
		window.location.replace("/account");
		</script>
		
		<?php
		
	}

?>