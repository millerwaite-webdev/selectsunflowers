<?php
	
	$items[0] = ["label"=>"Home","link"=>"index"];
	
	// Product
	if($_REQUEST['product']) {
		
		$product = $_REQUEST['product'];
		
		$strdbsql = "SELECT stock.*, stock_group_information.name AS stockGroup, stock_group_information.metaLink AS stockLink FROM stock 
		INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
		WHERE stock_group_information.metaLink = :product";
		$result = query($conn,$strdbsql,"single",array("product"=>$product));
		
		$items[1] = ["label"=>$pageName,"link"=>$pageLink];
		
		// Category
		$strdbsql = "SELECT contentName AS name, metaPageLink AS link FROM category WHERE metaPageLink = :category";
		$result2 = query($conn,$strdbsql,"single",array("category"=>$_REQUEST['category']));
		
		if($result2) $items[2] = ["label"=>$result2['name'],"link"=>$pageLink."/".$result2['link']];
		else $items[2] = ["label"=>"All Varieties","link"=>$pageLink."/all"];
		
		if($result) $items[3] = ["label"=>\ForceUTF8\Encoding::toUTF8($result['stockGroup']),"link"=>$pageLink."/".$result['stockLink']];
		else $items[3] = ["label"=>"Product not found","link"=>""];
		
	} else {
		
		// Basket
		if($strPage == "basket") {
			
			$items[1] = ["label"=>"Basket","link"=>"basket"];
		
		} else if($strPage == "confirmation") {
			
			// Confirmation
			$items[1] = ["label"=>"Checkout","link"=>""];
			$items[2] = ["label"=>"Confirmation","link"=>""];
			
		} else if($strPage == "account") {
			
			$items[1] = ["label"=>"Account","link"=>"account"];
			
			// Account
			if($_REQUEST['level'] == "personal-details") {
				$items[2] = ["label"=>"Personal Details","link"=>"account/personal-details"];
			} else if($_REQUEST['level'] == "processed-orders") {
				$items[2] = ["label"=>"Processed Orders","link"=>"account/processed-orders"];
			} else if($_REQUEST['level'] == "favourites") {
				$items[2] = ["label"=>"Favourites","link"=>"account/favourites"];
			} else if($_REQUEST['level'] == "my-reviews") {
				$items[2] = ["label"=>"My Reviews","link"=>"account/my-reviews"];
			}
			
		} else if($strPage == "register") {
			
			// Register
			$items[1] = ["label"=>"Register","link"=>"register"];
		
		} else if($strPage == "forgot-password") {
			
			// Forgot Password
			$items[1] = ["label"=>"Forgot Password","link"=>"forgot-password"];
		
		} else if($strPage == "news") {
			
			$items[1] = ["label"=>"Latest News","link"=>$pageLink];
			
			// Blog
			if($_REQUEST['article']) {
				
				$strdbsql = "SELECT * FROM site_news_events WHERE link = :article AND enabled = 1";
				$result = query($conn,$strdbsql,"single",["article"=>$_REQUEST['article']]);
				
				$items[2] = ["label"=>\ForceUTF8\Encoding::toUTF8($result['title']),"link"=>$pageLink."/".$result['link']];
				
			}
			
		} else if($strPage == "404") {
			
			// 404
			$items[1] = ["label"=>"Page not found","link"=>""];
			
		} else {
			
			// Database Pages
			if($pageParent > 0) {
				
				$strdbsql = "SELECT * FROM site_pages WHERE recordID = :parent";
				$result = query($conn,$strdbsql,"single",array("parent"=>$pageParent));
				
				$items[1] = ["label"=>\ForceUTF8\Encoding::toUTF8($result['pageTitle']),"link"=>$result['pageName']];
				$items[2] = ["label"=>\ForceUTF8\Encoding::toUTF8($pageName),"link"=>$pageLink];
				
			} else if(isset($_REQUEST['category'])) {

				// Category Pages
				$strdbsql = "SELECT contentName AS name, metaPageLink AS link FROM category WHERE metaPageLink = :category";
				$result = query($conn,$strdbsql,"single",array("category"=>$_REQUEST['category']));
				
				$items[1] = ["label"=>\ForceUTF8\Encoding::toUTF8($pageName),"link"=>$pageLink];
				$items[2] = ["label"=>\ForceUTF8\Encoding::toUTF8($result['name']),"link"=>$result['link']];

			} else {
				
				$items[1] = ["label"=>\ForceUTF8\Encoding::toUTF8($pageName),"link"=>$pageLink];
				
			}
		}
	}

	if(count($items) > 0) {
		print("<div class='breadcrumb'>");
			print("<div class='container'>");
				print("<ol itemscope='' itemtype='http://schema.org/BreadcrumbList'>");
					$i = 1;
					foreach($items AS $item) {
						print("<li itemprop='itemListElement' itemscope='' itemtype='http://schema.org/ListItem'>");
							if(!empty($item['link'])) print("<a itemscope='' itemtype='http://schema.org/Thing' itemprop='item' itemid='".$strsiteurl.($item['link'] == "index" ? "" : $item['link'])."' href='".$strsiteurl.($item['link'] == "index" ? "" : $item['link'])."'>");
								print("<span itemprop='name'>".$item['label']."</span>");
							if(!empty($item['link'])) print("</a>");
							print("<meta itemprop='position' content='".$i."'>");
						print("</li>");
						$i++;
					}
				print("</ol>");
			print("</div>");
		print("</div>");
	}

?>