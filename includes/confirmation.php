<?php

	// Function to return the JavaScript representation of a TransactionData object.
	function getTransactionJs($order, $company) {
		$data = "ga('ecommerce:addTransaction', {
			'id': '".$order['recordID']."',
			'affiliation': '".$company."',
			'revenue': '".$order['amountTotal']."',
			'shipping': '".$order['amountDelivery']."',
			'tax': '".$order['amountVat']."'
		});";
		return $data;
	}

	// Function to return the JavaScript representation of an ItemData object.
	function getItemJs($orderID, $item) {
		$data = "ga('ecommerce:addItem', {
			'id': '".$orderID."',
			'name': '".$item['groupName']."',
			'sku': '".$item['stockCode']."',
			'price': '".$item['itemPrice']."',
			'quantity': '".$item['itemQuantity']."'
		});";
		return $data;
	}

	switch($_REQUEST['level']) {
		default:
		
			break;
		case "success":
		
			$orderID = $_REQUEST['id'];
			
			$strdbsql = "SELECT * FROM order_header WHERE recordID = :order";
			$order = query($conn,$strdbsql,"single",["order"=>$orderID]);
			
			if($order) {
		
				print("<div class='row'>");
					print("<div class='col s12 m12 l8'>");
						
						print("<div class='comments'>");
							print("<div class='comment'>");
								print("<div class='message'>");
									print("<h1 class='h3'>Payment Complete</h1>");
									print("<p>Thank you for shopping with ".$compName.". Your order is currently being processed and you will receive an email confirming your order shortly.</p>");
									print("<p>Your order reference is:</p>");
									print("<h3># ".$order['recordID']."</h3>");
								
									print("<div class='row crop-bottom'>");
										print("<div class='col'>");
											print("<a href='/' class='btn btn-tertiary'>Return to Homepage</a>");
										print("</div>");
									print("</div>");
								
								print("</div>");
							print("</div>");
						print("</div>");
						
					print("</div>");
				print("</div>");
				
				?>
				
				<script>
				ga('require', 'ecommerce');

				<?php
				
				print(getTransactionJs($order, $compName));

				$strdbsql = "SELECT 
				(order_items.quantity * order_items.price) AS itemPrice, 
				order_items.quantity AS itemQuantity, 
				stock.stockCode, 
				stock_group_information.name AS groupName 
				FROM order_items 
				INNER JOIN stock ON order_items.stockID = stock.recordID 
				LEFT JOIN stock_units ON stock.unit = stock_units.recordID
				INNER JOIN site_vat ON stock.vatID = site_vat.recordID
				INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
				WHERE orderHeaderID = :order";
				$items = query($conn,$strdbsql,"multi",["order"=>$orderID]);
				
				foreach($items as $item) {
				  print(getItemJs($orderID, $item));
				}
				
				?>

				ga('ecommerce:send');
				</script>
				
				<?php
				
			} else {
				
				// No order
				
			}
		
			break;
		case "error":
		
			print("<h1>Something Went Wrong</h1>");
			print("<p>There was an issue processing your order. Not to worry, we still have your items saved. You can restart your order by returning to your basket.</p>");
			print("<p>For professional support, please contact our team on <a href='tel:".str_replace(" ", "", $compPhone)."'>".$compPhone."</a> or via our online contact service:</p>");
			print("<div class='panel'>");
				print("<a href='/basket' class='btn btn-tertiary'>Basket</a>");
				print("<a href='/contact-us' class='btn btn-contact'>Contact Us</a>");
			print("</div>");
		
			break;
	}
	
?>