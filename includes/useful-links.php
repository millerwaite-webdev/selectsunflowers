<?php
	
	// Opening hours
/*	$hours = array();
	
	$hours[0] = array("day"=>"Monday","open"=>"08:00","close"=>"17:00");
	$hours[1] = array("day"=>"Tuesday","open"=>"08:00","close"=>"17:00");
	$hours[2] = array("day"=>"Wednesday","open"=>"08:00","close"=>"15:00");
	$hours[3] = array("day"=>"Thursday","open"=>"08:00","close"=>"17:00");
	$hours[4] = array("day"=>"Friday","open"=>"08:00","close"=>"17:00");
	$hours[5] = array("day"=>"Saturday","open"=>"08:00","close"=>"17:00");
	$hours[6] = array("day"=>"Sunday","open"=>"","close"=>"");
	
	if(count($hours) > 0) {
		print("<h3>Opening Hours</h3>");	
		print("<ul class='links'>");
			foreach($hours AS $hour) {
				print("<li>".$hour['day'].": ".(!empty($hour['open']) && !empty($hour['close']) ? $hour['open']." - ".$hour['close'] : "Closed")."</li>");
			}
		print("</ul>");
	}*/
	
	// Related pages
	$strdbsql = "SELECT * FROM site_pages WHERE (recordID = :page OR parentPageID = :page OR recordID = :parent OR parentPageID = :parent) AND visible = 1 ORDER BY pageOrder";
	$commonLinks = query($conn,$strdbsql,"multi",array("page"=>$pageID,"parent"=>$pageParent));
	
	if(count($commonLinks) > 1) {
		print("<div class='collection links'>");
			foreach($commonLinks AS $commonLink) {
				print("<a href='".$strsiteurl.$commonLink['pageName']."' class='collection-item".($pageID == $commonLink['recordID'] ? " active" : "")."'><i class='zmdi zmdi-arrow-right right'></i>".$commonLink['pageTitle']."</a>");
			}
		print("</div>");
	}

?>