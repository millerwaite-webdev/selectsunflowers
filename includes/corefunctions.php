<?php	
	
	// Define DB Functions
	function connect($driver = DRIVER, $username = USER, $password = PASS, $host = HOST, $db = DB) {
		try {
			// Define new PHP Data Object connection
			$conn = new PDO('' . $driver . ':host=' . $host . ';dbname=' . $db, $username, $password);
			// Sets error reporting level
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		} catch(PDOException $e) {
			// If failed, display error
			echo "ERROR: " . $e->getMessage();
		}
	}
	
	// PDO query
	function query($conn, $querystring, $querytype, $values = null) {
		if($conn) {
			// Prepares query
			$query = $conn->prepare($querystring);
			
			// Binds values and executes query
			$query->execute($values);
			
			switch ($querytype) {
				case "single":
					// Returns associative array of single result
					$result = $query->fetch(PDO::FETCH_ASSOC);
				break;

				case "multi":
					// Returns multidimensional associative array of all results
					$result = $query->fetchAll(PDO::FETCH_ASSOC);
				break;

				case "count":
					// Returns number of selected rows
					$result = $query->rowCount();
				break;

				case "insert":
					// Insert new row and return is fld_counter (auto increment) of the inserted line
					$result = $conn->lastInsertID();
				break;

				case "update":
					// Return number of rows updated by query
					$result = $query->rowCount();
				break;

				case "delete":
					// Return number of rows deleted by query
					$result = $query->rowCount();
				break;

				default:
					// Invalid query type
					$result = "Invalid !";
				break;
			}
			return $result;
		} else {
			return "Failure !";
		}
	}

	function abbr($str, $maxLen) {
		if (strlen($str) > $maxLen && $maxLen > 1) {
			preg_match("#^.{1,".$maxLen."}\.#s", $str, $matches);
			return $matches[0];
		} else {
			return $str;
		}
	}
	
	function fnconvertunixtime($strtime, $strformat = "d/m/Y") {
		return date($strformat, $strtime);
	}
	
	function resendconfirmation($conn, $order) {
		
		confirmationemail($conn, $order, true);
		
		print("Successfully sent email!");
		
	}
	
	function confirmationemail($conn, $basket, $test) {
		
		global $strsiteurl, $strdomain, $compName, $compPhone, $compEmail, $compAddNumber, $compAddStreet, $compAddCity, $compAddCounty, $compAddCountry, $compAddPostcode, $devEmail, $debug, $strlogo;
		
		$strdbsql = "SELECT * FROM order_header WHERE basketHeaderID = :basket";
		$getOrder = query($conn,$strdbsql,"single",["basket"=>$basket]);
		
		if($debug == false && $test == false) $to = $getOrder['email'];
		else $to = $devEmail;
		
		$subject = "Order Confirmation";
		
		$headers = "From: ".$compEmail."\r\n";
		$headers .= "Reply-To: ".$compEmail."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$headers .= "Content-Transfer-Encoding: base64\r\n";
		
		$html = "<!DOCTYPE html>";
		$html .= "<html>";

		$html .= "<head>";
			$html .= "<meta charset='UTF-8'>";
			$html .= "<title>Thank you for your order (#".$getOrder['recordID'].")</title>";
			$html .= "<style>
				body{font-family:arial;}
				h1{font-size:16px;font-weight:normal;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h2{font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h3{border-bottom:solid 5px #FFFFFF;font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h4{font-size:14px;font-weight:bold;line-height:18px;mso-line-height-rule:exactly;margin:0;}
				h5{color:#b2b2b2;font-size:12px;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				p{font-size:14px;line-height:18px;mso-line-height-rule:exactly;margin:0;}
				p.price{font-size:14px;}
				ul{list-style:none;margin:0;padding:0;}
				ul li{font-size:14px;line-height:18px;mso-line-height-rule:exactly;}
				address{font-size:14px;line-height:18px;mso-line-height-rule:exactly;}
				.content{background-color:#F5F5F5;}
				.block{background-color:#FFFFFF;}
				.container{background-color:#FFFFFF;padding:0 25px;}
				.dark{background-color:#222222;padding:0 25px;}
				.dark h3{border:none;color:#FFFFFF;font-size:18px;}
				.dark li{color:#FFFFFF;}
				.light{background-color:#5B6ABF;padding:0 25px;}
				.light span{color:#FFFFFF;font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;}
				.horizontal-space{display:inline-block;height:10px;width:100%;}
			</style>";
		$html .= "</head>".PHP_EOL;
  
		$html .= "<body>";
    
		$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
			$html .= "<tbody>";
				$html .= "<tr>";
					$html .= "<td align='center' class='content'>";
						
						$html .= "<table align='left' border='0' cellpadding='0' cellspacing='2' width='600'>";
							$html .= "<tbody>";
								
								// Header
								$html .= "<tr>";
									$html .= "<td class='container' align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
											
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='10'></td>";
												$html .= "</tr>".PHP_EOL;
											
												// Logo
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<a href='".$strsiteurl."' target='_blank'>";
															$html .= "<img src='".$strsiteurl.trim($strlogo,"/")."' alt='".$compName."' width='160' />";
														$html .= "</a>";
													$html .= "</td>";
												$html .= "</tr>".PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='10'></td>";
												$html .= "</tr>".PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>".PHP_EOL;
								
								// Main Image
								$html .= "<tr>";
									$html .= "<td align='center' class='block'>";
										$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='center'>";
														$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='center'>";
																		$html .= "<img src='".$strsiteurl."images/emails/order-complete.jpg' alt='Your Order is Complete' width='100%' />";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>".PHP_EOL;
								
								// Introduction
								$html .= "<tr>";
									$html .= "<td align='left' class='container'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
															
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																// Heading
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<h1>Thank you for shopping with ".$compName.". We are currently processing your order and it will be with you shortly.</h1>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>".PHP_EOL;
																
																// Order Summary
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<h2>";
																			$html .= "Order: ".$getOrder['recordID'];
																			$html .= "<span class='pipe'>&nbsp;&nbsp;|&nbsp;&nbsp;</span>";
																			$html .= "Date ordered: ".date("d/m/y", $getOrder['timestampOrder']);
																		$html .= "</h2>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>".PHP_EOL;
																
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>".PHP_EOL;
								
								// Order
								$html .= "<tr>";
									$html .= "<td align='left' class='container'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' colspan='3' height='20'></td>";
												$html .= "</tr>".PHP_EOL;
												
												// Customer Details
												$html .= "<tr>";
													
													// Billing Address
													$html .= "<td align='left' width='49%'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<h3>Billing Address</h3>";
																		$html .= "<address>";
																			$html .= (!empty($getOrder['addDeliveryTitle']) ? $getOrder['addDeliveryTitle']." " : "").(!empty($getOrder['addDeliveryFirstname']) ? $getOrder['addDeliveryFirstname']." " : "").(!empty($getOrder['addDeliverySurname']) ? $getOrder['addDeliverySurname'] : "")."<br>";
																			if(!empty($getOrder['addDelivery1'])) $html .= $getOrder['addDelivery1'];
																			if(!empty($getOrder['addDelivery2'])) $html .= ", ".$getOrder['addDelivery2'];
																			if(!empty($getOrder['addDelivery3'])) $html .= ", ".$getOrder['addDelivery3']."<br>";
																			if(!empty($getOrder['addDelivery4'])) $html .= $getOrder['addDelivery4'];
																			if(!empty($getOrder['addDelivery5'])) $html .= ", ".$getOrder['addDelivery5'];
																			if(!empty($getOrder['addDelivery6'])) $html .= ", ".$getOrder['addDelivery6'];
																			if(!empty($getOrder['addDeliveryPostcode'])) $html .= "<br/>".$getOrder['addDeliveryPostcode'];
																		$html .= "</address>";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>".PHP_EOL;
													
													$html .= "<td align='left' width='1%'></td>";
													
													// Delivery Address
													$html .= "<td align='left' width='49%'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<h3>Delivery Address</h3>";
																		$html .= "<address>";
																			$html .= (!empty($getOrder['addBillingTitle']) ? $getOrder['addBillingTitle']." " : "").(!empty($getOrder['addBillingFirstname']) ? $getOrder['addBillingFirstname']." " : "").(!empty($getOrder['addBillingSurname']) ? $getOrder['addBillingSurname'] : "")."<br>";
																			if(!empty($getOrder['addBilling1'])) $html .= $getOrder['addBilling1'];
																			if(!empty($getOrder['addBilling2'])) $html .= ", ".$getOrder['addBilling2'];
																			if(!empty($getOrder['addBilling3'])) $html .= ", ".$getOrder['addBilling3'];
																			if(!empty($getOrder['addBilling4'])) $html .= "<br/>".$getOrder['addBilling4'];
																			if(!empty($getOrder['addBilling5'])) $html .= ", ".$getOrder['addBilling5'];
																			if(!empty($getOrder['addBilling6'])) $html .= ", ".$getOrder['addBilling6'];
																			if(!empty($getOrder['addBillingPostcode'])) $html .= "<br/>".$getOrder['addBillingPostcode'];
																		$html .= "</address>";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>".PHP_EOL;
													
												$html .= "</tr>".PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' colspan='3' height='20'></td>";
												$html .= "</tr>".PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
									
								// Products
								$html .= "<tr>";
									$html .= "<td align='left' class='container'>";		
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' colspan='3' height='20'></td>";
												$html .= "</tr>".PHP_EOL;
												
												// Items
												$strdbsql = "SELECT order_items.quantity, order_items.price, stock_group_information.name, stock_images.imageLink AS image, stock_images.description AS alt, stock_units.abbreviation AS unit FROM order_items 
												INNER JOIN stock ON order_items.stockID = stock.recordID 
												INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
												INNER JOIN stock_images ON stock_group_information.recordID = stock_images.stockID 
												INNER JOIN stock_units ON stock.unit = stock_units.recordID 
												WHERE order_items.orderHeaderID = :order 
												GROUP BY stock_images.stockID";
												$items = query($conn,$strdbsql,"multi",["order"=>$getOrder['recordID']]);
												
												if(count($items) > 0) {
													foreach($items AS $item) {
														$html .= "<tr>";
															$html .= "<td align='left' valign='middle' width='95'>";
																$html .= "<img style='vertical-align:middle;' width='85' src='".$strsiteurl."images/products/main/".$item['image']."' alt='".$item['alt']."' />";
															$html .= "</td>";
															$html .= "<td align='left'>";
																$html .= "<h4>".$item['name']."</h4>";
																$html .= "<p>".($item['unit'] == "kg" ? "1kg" : ucwords($item['unit']))."</p>";
																$html .= "<p>Qty: ".$item['quantity']."</p>";
															$html .= "</td>";
															$html .= "<td align='right' valign='middle' width='100'>";
																$html .= "<h4>&nbsp;</h4>";
																$html .= "<p>&nbsp;</p>";
																$html .= "<p class='price'>&pound;".$item['price']."</p>";
															//	$html .= "<div class='ships-by-message' style='font-family:Helvetica,sans-serif;font-size:13px;'>Preparing...</div>";
															$html .= "</td>";
														$html .= "</tr>".PHP_EOL;
														
														// Spacer
														$html .= "<tr>";
															$html .= "<td align='left' colspan='3' height='10'></td>";
														$html .= "</tr>".PHP_EOL;
													}
												}
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' colspan='3' height='10'></td>";
												$html .= "</tr>";
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>".PHP_EOL;
								
								// Totals
								$html .= "<tr>";
									$html .= "<td align='left' class='container'>";		
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";

												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' colspan='3' height='20'></td>";
												$html .= "</tr>".PHP_EOL;
												
												// Calculations
												$html .= "<tr>";
													$html .= "<td align='right' colspan='2'><p>Subtotal</p></td>";
													$html .= "<td align='right'><p>&pound;".$getOrder['amountStock']."</p></td>";
												$html .= "</tr>";
												
												if($getOrder['amountOffer'] > 0) {
													$html .= "<tr>";
														$html .= "<td align='right' colspan='2'><p>Discount</p></td>";
														$html .= "<td align='right'><p>- &pound;".$getOrder['amountOffer']."</p></td>";
													$html .= "</tr>";
												}
												
												$html .= "<tr>";
													$html .= "<td align='right' colspan='2'><p>Delivery Fee</p></td>";
													$html .= "<td align='right'><p>&pound;".$getOrder['amountDelivery']."</p></td>";
												$html .= "</tr>";
												$html .= "<tr>";
													$html .= "<td align='right' colspan='2'><p>VAT</p></td>";
													$html .= "<td align='right'><p>&pound;".$getOrder['amountVat']."</p></td>";
												$html .= "</tr>";
												$html .= "<tr>";
													$html .= "<td align='right' colspan='2'><p><strong>Total</strong></p></td>";
													$html .= "<td align='right'><p><strong>&pound;".$getOrder['amountTotal']."</strong></p></td>";
												$html .= "</tr>".PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td colspan='3' height='20'></td>";
												$html .= "</tr>".PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>".PHP_EOL;
								
								// Contact
								$html .= "<tr>";
									$html .= "<td class='dark'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
											
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='20'></td>";
												$html .= "</tr>".PHP_EOL;
												
												// Opening Hours
												$html .= "<tr>";
													$html .= "<td align='left'><h3>Questions? We're on call.</h3></td>";
												$html .= "</tr>";
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<ul>";
															$html .= "<li>Monday - 08:00 - 17:00</li>";
															$html .= "<li>Tuesday - 08:00 - 17:00</li>";
															$html .= "<li>Wednesday - 08:00 - 15:00</li>";
															$html .= "<li>Thursday - 08:00 - 17:00</li>";
															$html .= "<li>Friday - 08:00 - 17:00</li>";
															$html .= "<li>Saturday - 08:00 - 15:00</li>";
															$html .= "<li>Sunday - Closed</li>";
														$html .= "</ul>";
													$html .= "</td>";
												$html .= "</tr>".PHP_EOL;
												
												// Contact Links
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<a href='tel:".$compPhone."'>";
															$html .= "<img src='".$strsiteurl."images/emails/bubble.png' class='icon-bubble' alt='Phone' width='28' />";
															$html .= $compPhone;
														$html .= "</a>";
														$html .= "<span class='pipe'>&nbsp;&nbsp;|&nbsp;&nbsp;</span>";
														$html .= "<a href='mailto:".$compEmail."'>";
															$html .= "<img src='".$strsiteurl."images/emails/email.png' class='icon-email' alt='Email' width='28' />";
															$html .= $compEmail;
														$html .= "</a>";
													$html .= "</td>";
												$html .= "</tr>".PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='20'></td>";
												$html .= "</tr>".PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>".PHP_EOL;
								
							$html .= "</tbody>";
						$html .= "</table>";
						
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</tbody>";
		$html .= "</table>";
		
		// Footer
		$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
			$html .= "<tbody>";
			
				// Spacer
				$html .= "<tr>";
					$html .= "<td height='10'></td>";
				$html .= "</tr>".PHP_EOL;
			
				// Information
				$html .= "<tr>";
					$html .= "<td align='center'>";
						$html .= "<h5>";
							if(!empty($compAddNumber)) $html .= $compAddNumber." ";
							if(!empty($compAddStreet)) $html .= $compAddStreet.", ";
							if(!empty($compAddCity)) $html .= $compAddCity.", ";
							if(!empty($compAddCounty)) $html .= $compAddCounty.", ";
							if(!empty($compAddCountry)) $html .= $compAddCountry.", ";
							if(!empty($compAddPostcode)) $html .= $compAddPostcode;
							$html .= "<span class='pipe'>&nbsp;&nbsp;|&nbsp;&nbsp;</span>";
							$html .= "&copy; ".date("Y")." ".$compName;
						$html .= "</h5>";
					$html .= "</td>";
				$html .= "</tr>".PHP_EOL;
				
				// Spacer
				$html .= "<tr>";
					$html .= "<td height='10'></td>";
				$html .= "</tr>".PHP_EOL;
				
			$html .= "</tbody>";
		$html .= "</table>";
	
		$html .= "</body>";

		$html .= "</html>";
		
		// Send email
		if($test == false) {
			if(mail($to, $subject, chunk_split(base64_encode($html)), $headers)) {
			
				// Update confirmation email status on order
				$strdbsql = "UPDATE order_header SET sentEmailConfirmation = 1 WHERE basketHeaderID = :basket";
				$result = query($conn,$strdbsql,"update",["basket"=>$basket]);
				
				if($result) {
					
					if($debug == false && $test == false) mail($compEmail, $subject, chunk_split(base64_encode($html)), $headers);
					else mail($devEmail, $subject, chunk_split(base64_encode($html)), $headers);
				
				}
				
			}
		} else {
			mail($devEmail, $subject, chunk_split(base64_encode($html)), $headers);
		}
		
	}
?>