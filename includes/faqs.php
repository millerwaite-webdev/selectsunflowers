<?php

	// Categories
	$strdbsql = "SELECT site_faqs_categories.recordID, site_faqs_categories.groupName, COUNT(site_faqs.recordID) AS total FROM site_faqs_categories 
	INNER JOIN site_faqs ON site_faqs_categories.recordID = site_faqs.groupID 
	WHERE site_faqs_categories.enabled = 1 AND site_faqs.enabled = 1 ORDER BY site_faqs_categories.orderID";
	$parents = query($conn,$strdbsql,"multi");
	
	if(count($parents) > 0) {
		
		// Tabs
		print("<ul class='tabs'>");
			foreach($parents AS $parent) {
				if($parent['total'] > 0) {
					print("<li class='tab'><a class='active' href='#category".$parent['recordID']."'>".$parent['groupName']."</a></li>");
				}
			}
		print("</ul>");

		// Content
		foreach($parents AS $parent) {
			
			// Faqs
			$strdbsql = "SELECT * FROM site_faqs WHERE groupID = :parent AND enabled = 1 ORDER BY orderID";
			$children = query($conn,$strdbsql,"multi",["parent"=>$parent['recordID']]);
			
			if(count($children) > 0) {
				$i = 1;
				print("<div id='category".$parent['recordID']."' class='faqs'>");
					print("<ul class='collapsible accordion'>");
						foreach($children AS $child) {
							print("<li ".($i == 1 ? "class='active'" : "").">");
								print("<div class='collapsible-header'>".$child['question']."</div>");
								print("<div class='collapsible-body'>".$child['answer']."</div>");
							print("</li>");
							$i++;
						}
					print("</ul>");
				print("</div>");
			}
		}
		
	}
	
?>