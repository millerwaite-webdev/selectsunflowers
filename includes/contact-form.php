<?php

	print("<div class='row'>");
		print("<div class='col s12 m12 l6'>");
	
			print("<form id='contact' class='contact' method='POST' action='/contact-us'>");
	
				print("<h3>Get in Touch!</h3>");
				print("<p>Need help with your order? Contact us via <a href='mailto:".$compEmail."'>".$compEmail."</a> or by using the form below:</p>");
	
				print("<div class='row'>");
				
					// Title
					$titles = array();
								
					$titles[0] = array("label"=>"Mr","value"=>"Mr");
					$titles[1] = array("label"=>"Miss","value"=>"Miss");
					$titles[2] = array("label"=>"Mrs","value"=>"Mrs");
					$titles[3] = array("label"=>"Ms","value"=>"Ms");
					$titles[4] = array("label"=>"Other","value"=>"Other");
								
					print("<div class='col s12 m12 l2'>");
						print("<div class='input-field'>");
							print("<select class='browser-default required' name='frm_title' tabindex='1' aria-label='Title'>");
								print("<option value='' disabled selected>Title</option>");
								foreach($titles AS $title) {
									print("<option value='".$title['value']."'>".$title['label']."</option>");
								}
							print("</select>");
						print("</div>");
					print("</div>");
				
					// First Name
					print("<div class='col s12 m12 l5'>");
						print("<div class='input-field'>");
							print("<input type='text' name='frm_firstname' class='required' maxlength='50' aria-label='First Name' placeholder='First Name' tabindex='2' />");
						print("</div>");
					print("</div>");
					
					// Last Name
					print("<div class='col s12 m12 l5'>");
						print("<div class='input-field'>");
							print("<input type='text' name='frm_lastname' class='required' maxlength='50' aria-label='Last Name' placeholder='Last Name' tabindex='3' />");
						print("</div>");
					print("</div>");
					
					// Email Address
					print("<div class='col s12 m12 l6'>");
						print("<div class='input-field'>");
							print("<input type='email' name='frm_email' class='required' data-label='email address' maxlength='50' aria-label='Email Address' placeholder='Email Address' tabindex='4' />");
						print("</div>");
					print("</div>");
					
					// Phone Number
					print("<div class='col s12 m12 l6'>");
						print("<div class='input-field'>");
							print("<input type='tel' name='frm_phone' class='required' data-label='phone number' maxlength='11' aria-label='Phone Number' placeholder='Phone Number' tabindex='5' />");
						print("</div>");
					print("</div>");
					
					// Message
					print("<div class='col s12'>");
						print("<div class='input-field'>");
							print("<textarea name='frm_message' class='required' data-length='500' max-length='500' class='materialize-textarea' aria-label='Message' placeholder='Message' tabindex='6'></textarea>");
						print("</div>");
					print("</div>");
				print("</div>");
				
				// Google reCaptcha
				print("<div class='row'>");
					print("<div class='col s12'>");
						print("<div class='g-recaptcha captcha-mob' data-sitekey='".$captchaKey."'></div>");
					print("</div>");
				print("</div>");
				
				// Submit Button
				print("<div class='input-field'>");
					print("<button class='btn btn-primary wide large submit' type='submit' name='action' tabindex='7'>Submit Enquiry</button>");
				print("</div>");
				
			print("</form>");
			
		print("</div>");
		print("<div class='col s12 m12 l6'>");
		
			// Map
			print("<div class='row'>");
				print("<div class='col s12'>");
					
						
					print("<div id='map' class='map'></div>");
	//open maps
			/*		print("<link rel=\"stylesheet\" href=\"https://unpkg.com/leaflet@1.3.4/dist/leaflet.css\" integrity=\"sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==\" crossorigin=\"\"/>");
					print("<script src=\"https://unpkg.com/leaflet@1.3.4/dist/leaflet.js\" integrity=\"sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==\" crossorigin=\"\"></script>");

					print("<script>");
						print("var map = L.map('map').setView([54.6697351, -2.843788], 12);");

						print("L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {");
							print("attribution: '&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors'");
						print("}).addTo(map);");

						print("L.marker([54.6697351, -2.863788]).addTo(map);");
					print("</script>");
			*/

	//google maps
			/*		print("<input id='address' type='hidden' value='".$compAddPostcode."'>");
					print("<script>");
					print("var map;");
					print("function initMap() {");
						print("var geocoder = new google.maps.Geocoder();");
						print("var address = document.getElementById('address').value;");
			
						print("geocoder.geocode({'address': address}, function(results, status) {");
							print("if (status === 'OK') {");
								print("map =  new google.maps.Map(document.getElementById('map'), {");
									print("zoom: 10,");
									print("center: results[0].geometry.location,");
									print("disableDefaultUI: true,");
									print("styles: [{\"featureType\": \"water\",\"elementType\": \"geometry\",\"stylers\": [{\"color\": \"#e9e9e9\"},{\"lightness\": 17}]},{\"featureType\": \"landscape\",\"elementType\": \"geometry\",\"stylers\": [{\"color\": \"#f5f5f5\"},{\"lightness\": 20}]},{\"featureType\": \"road.highway\",\"elementType\": \"geometry.fill\",\"stylers\": [{\"color\": \"#ffffff\"},{\"lightness\": 17}]},{\"featureType\": \"road.highway\",\"elementType\": \"geometry.stroke\",\"stylers\": [{\"color\": \"#ffffff\"},{\"lightness\": 29},{\"weight\": 0.2}]},{\"featureType\": \"road.arterial\",\"elementType\": \"geometry\",\"stylers\": [{\"color\": \"#ffffff\"},{\"lightness\": 18}]},{\"featureType\": \"road.local\",\"elementType\": \"geometry\",\"stylers\": [{\"color\": \"#ffffff\"},{\"lightness\": 16}]},{\"featureType\": \"poi\",\"elementType\": \"geometry\",\"stylers\": [{\"color\": \"#f5f5f5\"},{\"lightness\": 21}]},{\"featureType\": \"poi.park\",\"elementType\": \"geometry\",\"stylers\": [{\"color\": \"#dedede\"},{\"lightness\": 21}]},{\"elementType\": \"labels.text.stroke\",\"stylers\": [{\"visibility\": \"on\"},{\"color\": \"#ffffff\"},{\"lightness\": 16}]},{\"elementType\": \"labels.text.fill\",\"stylers\": [{\"saturation\": 36},{\"color\": \"#333333\"},{\"lightness\": 40}]},{\"elementType\": \"labels.icon\",\"stylers\": [{\"visibility\": \"off\"}]},{\"featureType\": \"transit\",\"elementType\": \"geometry\",\"stylers\": [{\"color\": \"#f2f2f2\"},{\"lightness\": 19}]},{\"featureType\": \"administrative\",\"elementType\": \"geometry.fill\",\"stylers\": [{\"color\": \"#fefefe\"},{\"lightness\": 20}]},{\"featureType\": \"administrative\",\"elementType\": \"geometry.stroke\",\"stylers\": [{\"color\": \"#fefefe\"},{\"lightness\": 17},{\"weight\": 1.2}]}]");
								print("});");
								
								print("var marker = new google.maps.Marker({");
									print("map: map,");
									print("position: results[0].geometry.location,");
									print("icon: \"/images/company/logo-marker.png\"");
								print("});");

							print("}");
						print("});");
						
					print("}");
					print("</script>");
					print("<script async defer src=\"https://maps.googleapis.com/maps/api/js?key=".$googleKey."&callback=initMap\"></script>");
			*/
				print("</div>");
			print("</div>");
		
		print("</div>");
	print("</div>");

?>