<?php	

	// Compress given code
	function compressCss($buffer) {
		// Remove comments:
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		// Remove tabs, excessive spaces and newlines
		$buffer = str_replace(["\r\n", "\r", "\n", "\t", '  ', '   '], '', $buffer);
		$buffer = str_replace(["{ ", " {"], '{', $buffer);
		$buffer = str_replace(["} ", " }"], '}', $buffer);
		$buffer = str_replace(["; ", " ;"], ';', $buffer);
		$buffer = str_replace([", ", " ,"], ',', $buffer);
		$buffer = str_replace([": ", " :"], ':', $buffer);
		return $buffer;
	}
	
	// Get site blocks from admin
	function fnGetSiteBlocks($lookupID,$lookupType,$position,$conn) {
			
		global $strsiteurl,$compName, $compPhone, $compMobile, $compFax, $compEmail, $compAddNumber, $compAddStreet, $compAddCity, $compAddCounty, $compAddCountry, $compAddPostcode, $pageID, $pageName, $pageContent, $pageLink, $pageImage, $pageParent, $socialLinks, $newsArticles, $services, $googLat, $googLng, $captchaKey, $accountid;
			
		if (isset($_REQUEST['page'])) $strPage = $_REQUEST['page']; else $strPage = "index";
		if (isset($_REQUEST['view'])) $strview = $_REQUEST['view']; else $strview = "";
			
		$strdbsql = "SELECT site_blocks.*, site_block_position.description AS includePos FROM site_block_relations 
		INNER JOIN site_blocks ON site_block_relations.blockID = site_blocks.recordID 
		INNER JOIN site_block_position ON site_block_relations.positionID = site_block_position.recordID 
		WHERE site_block_relations.$lookupType = :lookupID AND site_block_position.description = :position 
		ORDER BY site_block_relations.pageOrder";
		$blockRelations = query($conn,$strdbsql,"multi",array("lookupID"=>$lookupID,"position"=>$position));
		
		foreach($blockRelations AS $relation) {
			if($relation['type'] == 1 && !isset($_REQUEST['product']) && !isset($_REQUEST['article']) && !isset($_REQUEST['category'])) {
				$GLOBALS['boolSlideshow'] = true;
					
				$strdbsql = "SELECT * FROM site_gallery_images WHERE galleryID = :galleryID ORDER BY galleryOrder";
				$galleryImages = query($conn,$strdbsql,"multi",array("galleryID"=>$relation['galleryID']));
			
				print("<div class='slider banner-slider'>");
					print("<div class='container'>");
						print("<ul class='slides'>");
							foreach($galleryImages AS $galleryImage) {
								print("<li>");
									print("<img src='/images/gallery/".$galleryImage['imageLocation']."' alt='".\ForceUTF8\Encoding::toUTF8($galleryImage['title'])."' />");
									print("<div class='caption'>");
										print("<div class='wrapper'>");
											print("<h1>".\ForceUTF8\Encoding::toUTF8($galleryImage['title'])."</h1>");
											print("<h2>".\ForceUTF8\Encoding::toUTF8($galleryImage['description'])."</h2>");
											if(!empty($galleryImage['linkLabel1']) && !empty($galleryImage['linkLocation1'])) print("<a href='".$galleryImage['linkLocation1']."' class='btn btn-primary ".($galleryImage['linkLocation1'] == "#scroll" ? "btn-scroll" : "")."'>".\ForceUTF8\Encoding::toUTF8($galleryImage['linkLabel1'])."</a>");
											if(!empty($galleryImage['linkLabel2']) && !empty($galleryImage['linkLocation2'])) print("<a href='".$galleryImage['linkLocation2']."' class='btn btn-secondary'>".\ForceUTF8\Encoding::toUTF8($galleryImage['linkLabel2'])."</a>");
										print("</div>");
										print("<div class='breakpoint'></div>");
									print("</div>");
								print("</li>");
							}
						print("</ul>");
					print("</div>");
				print("</div>");

			} else {
				if(!empty($relation['includeName']) && file_exists($_SERVER['DOCUMENT_ROOT']."/includes/".$relation['includeName'])) {
					include($_SERVER['DOCUMENT_ROOT']."/includes/".$relation['includeName']);
				}
				print($relation['content']);
			}
		}
	}
	
	// Function to redirect browser
	function redirect($url) {
	   if (!headers_sent())
			header('Location: '.$url);
	   else
	   {
			echo '<script type="text/javascript">';
			echo 'window.location.href="'.$url.'";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
			echo '</noscript>';
	   }
	}
	

	

	// Check to see if email is valid
	function is_valid_email($email) {
		$result = TRUE;
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) {
			$result = FALSE;
		}
		return $result;
	}
	
	// Get discounts
	function checkdiscount($price, $accountid, $conn) {
		$strdbsql = "SELECT customer_offers.* FROM customer_offers INNER JOIN customer ON customer_offers.recordID = customer.offerID WHERE customer.recordID = :user";
		$result = query($conn,$strdbsql,"single",array("user"=>$accountid));
		if($result) {
			if($result['type'] == "percent") {
				$price = $price * ((100 - $result['value']) / 100);
			} else if($result['type'] == "fixed") {
				// What is discounted price is higher than the price of the product?
				$price = $price - $result['value'];
			}
		}
		return number_format($price, 2);
	}
	
	// Delete basket
	function deletebasket($conn, $basket) {
			
		$strdbsql = "DELETE FROM basket_header WHERE recordID = :basket";
		$delete1 = query($conn,$strdbsql,"delete",["basket" => $basket]);

		$strdbsql = "DELETE FROM basket_items WHERE basketHeaderID = :basket";
		$delete2 = query($conn,$strdbsql,"delete",["basket" => $basket]);
		
		//setcookie("basket", 0, time() + 60 * 60 * 2, "/");
		$_SESSION['basket'] = 0;
	
	}
	
	// Create order
	function createorder($conn, $customerid, $basket, $basketSummary, $firstname, $surname, $address1, $address2, $address3, $address4, $address5, $country, $postcode, $date, $time, $telephone, $email, $notes, $domain, $source, $paymentAmount) {
		
		$strdbsql = "SELECT * FROM basket_header WHERE recordID = :basket";
		$getOrder = query($conn,$strdbsql,"single",["basket"=>$basket]);
	
		if($getOrder) {
		
					

			$conn->beginTransaction();
			try {

				// Get basket items
				$strdbsql = "SELECT basket_items.*, stock.vatID, stock.weight FROM basket_items INNER JOIN stock ON stock.recordID = basket_items.stockID WHERE basket_items.basketHeaderID = :basket";
				$getItems = query($conn,$strdbsql,"multi",["basket"=>$basket]);
			
				
				$strdbsql = "INSERT INTO order_header 
				(customerID, basketHeaderID, amountTotal, amountDelivery, amountStock, amountVat, amountOffer, timestampOrder, timestampPayment, offerCode, vatRate, postageID, addDeliveryCustomerID, addDeliveryFirstname, addDeliverySurname, addDelivery1, addDelivery2, addDelivery3, addDelivery4, addDelivery5, addDelivery6, addDeliveryPostcode, addDeliveryDate, addDeliveryTime, addBillingFirstname, addBillingSurname, addBilling1, addBilling2, addBilling3, addBilling4, addBilling5, addBilling6, addBillingPostcode, telephone, email, notes, domainName, orderSource,paymentAmount) 
				VALUES 
				(:customer, :basket, :total, :delivery, :stock, :vat, :discount, UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), :offer, :rate, :postageID, :customer, :firstname, :surname, :address1, :address2, :address3, :address4, :address5, :address6, :postcode, :date, :time, :firstname, :surname, :address1, :address2, :address3, :address4, :address5, :address6, :postcode, :telephone, :email, :notes, :domain, :source, :payment)";
				$addOrder = query($conn,$strdbsql,"insert",[
					"customer" => $customerid,
					"basket" => $basket,
					"total" => $basketSummary->total + $getOrder['postageAmount'] - $getOrder['discountAmount'],
					"delivery" => $getOrder['postageAmount'],
					"postageID" => $getOrder['postageID'],
					"stock" => $basketSummary->total - $basketSummary->vat,
					"vat" => $basketSummary->vat,
					"discount" => $getOrder['discountAmount'],
					"offer" => $getOrder['offerCodeID'],
					"rate" => 0,
					"firstname" => $firstname,
					"surname" => $surname,
					"address1" => $address1,
					"address2" => $address2,
					"address3" => $address3,
					"address4" => $address4,
					"address5" => $address5,
					"address6" => $country,
					"postcode" => $postcode,
					"date" => $date,
					"time" => $time,
					"telephone" => $telephone,
					"email" => $email,
					"notes" => $notes,
					"domain" => $domain,
					"source" => $source,
					"payment" => $paymentAmount
				]);
				
				
				// Add order items
				if(count($getItems) > 0) {
					foreach($getItems AS $getItem) {
						$strdbsql = "INSERT INTO order_items (orderHeaderID, stockID, quantity, price,vatID) VALUES (:basket, :stock, :quantity, :price, :vatID)";
						$addItems = query($conn,$strdbsql,"insert",["basket"=>$addOrder,"stock"=>$getItem['stockID'],"quantity"=>$getItem['quantity'],"price"=>$getItem['price'], "vatID"=>$getItem['vatID']]);
					}
				} else {
					// No items in basket
					throw new Exception("Basket empty");
				}
				$conn->commit();
				return $addOrder;
			} catch (Exception $e) {
				$conn->rollback();
				throw $e;
			}
			
		} else {
			throw new Exception("Basket not found");
		}
		
	}
	

	
	// Send enquiry email
	function enquiryemail($conn, $enquiry) {
		
		global $strsiteurl, $strdomain, $compName, $compPhone, $compEmail, $compAddNumber, $compAddStreet, $compAddCity, $compAddCounty, $compAddCountry, $compAddPostcode;
		
		// Get enquiry information
		$strdbsql = "SELECT * FROM site_contact_history WHERE recordID = :enquiry";
		$getEnquiry = query($conn,$strdbsql,"single",["enquiry"=>$enquiry]);
		
		if($debug == false) $to = $compEmail;
		else $to = $devEmail;
		
		$subject = $getEnquiry['subject'];
		
		$headers = "From: no-reply@".$strdomain."\r\n";
		$headers .= "Reply-To: ".$getEnquiry['email']."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$headers .= "Content-Transfer-Encoding: base64\r\n";
		
		$html = "<!DOCTYPE html>";
		$html .= "<html>";

		$html .= "<head>";
			$html .= "<meta charset='UTF-8'>";
			$html .= "<title>".$getEnquiry['name']." has sent you a ".$compName." enquiry</title>";
			$html .= "<style>
				body{font-family:arial;}
				h1{font-size:16px;font-weight:normal;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h2{font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h3{font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h4{font-size:14px;font-weight:bold;line-height:18px;mso-line-height-rule:exactly;margin:0;}
				h5{color:#b2b2b2;font-size:12px;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				p{font-size:14px;line-height:18px;mso-line-height-rule:exactly;margin:0;}
				p.price{font-size:14px;}
				ul{list-style:none;margin:0;padding:0;}
				ul li{font-size:14px;line-height:18px;mso-line-height-rule:exactly;}
				address{font-size:14px;line-height:18px;mso-line-height-rule:exactly;}
				.content{background-color:#F5F5F5;}
				.block{background-color:#FFFFFF;}
				.container{background-color:#FFFFFF;padding:0 25px;}
				.dark{background-color:#222222;padding:0 25px;}
				.dark h3{border:none;color:#FFFFFF;font-size:18px;}
				.dark li{color:#FFFFFF;}
				.dark a{color:#FFFFFF;}
				.dark .pipe{color:#FFFFFF;}
				.light{background-color:#5B6ABF;padding:0 25px;}
				.light span{color:#FFFFFF;font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;}
				.horizontal-space{display:inline-block;height:10px;width:100%;}
			</style>";
		$html .= "</head>";
  
		PHP_EOL;
  
		$html .= "<body>";
	
		$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
			$html .= "<tbody>";
				$html .= "<tr>";
					$html .= "<td align='center' class='content'>";
						
						$html .= "<table align='left' border='0' cellpadding='0' cellspacing='2' width='600'>";
							$html .= "<tbody>";
								
								// Header
								$html .= "<tr>";
									$html .= "<td class='container' align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
											
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='10'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
											
												// Logo
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<a href='".$strsiteurl."' target='_blank'>";
															$html .= "<img src='".$strsiteurl."images/company/logo2.png' alt='".$compName."' width='160' />";
														$html .= "</a>";
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='10'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Main Image
								$html .= "<tr>";
									$html .= "<td align='center' class='block'>";
										$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='center'>";
														$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='center'>";
																		$html .= "<img src='".$strsiteurl."images/emails/enquiry-received.jpg' alt='Your Order is Complete' width='100%' />";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Introduction
								$html .= "<tr>";
									$html .= "<td align='left' class='container'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
															
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Opening Quotes
																$html .= "<tr>";
																	$html .= "<td align='left' valign='top' width='15'><img src='".$strsiteurl."images/emails/left-quotes.png' alt='Left Quotes' width='15' /></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='10'></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Heading
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<h1>".$getEnquiry['message']."</h1>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='10'></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Closing Quotes
																$html .= "<tr>";
																	$html .= "<td align='right' colspan='3' valign='bottom' width='15'><img src='".$strsiteurl."images/emails/right-quotes.png' alt='Right Quotes' width='15' /></td>";
																$html .= "</tr>";
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Contact
								$html .= "<tr>";
									$html .= "<td class='dark'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
											
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='20'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Opening Hours
												$html .= "<tr>";
													$html .= "<td align='left'><h3>".$getEnquiry['name']."</h3></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='10'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Contact Links
												$html .= "<tr>";
													$html .= "<td align='left' valign='middle'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='left' valign='middle'>";
																		$html .= "<a href='tel:".$getEnquiry['telephone']."'>";
																			$html .= "<img src='".$strsiteurl."images/emails/bubble.png' class='icon-bubble' alt='Phone' width='24' />";
																		$html .= "</a>";
																	$html .= "</td>";
																	$html .= "<td align='left' valign='middle' width='5'></td>";
																	$html .= "<td align='left' valign='middle'>";
																		$html .= "<a href='tel:".$getEnquiry['telephone']."'>";
																			$html .= $getEnquiry['telephone'];
																		$html .= "</a>";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='left' valign='middle'>";
																		$html .= "<span class='pipe'>&nbsp;&nbsp;|&nbsp;&nbsp;</span>";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='left' valign='middle'>";
																		$html .= "<a href='mailto:".$getEnquiry['email']."'>";
																			$html .= "<img src='".$strsiteurl."images/emails/email.png' class='icon-email' alt='Email' width='24' />";
																		$html .= "</a>";
																	$html .= "</td>";
																	$html .= "<td align='left' valign='middle' width='5'></td>";
																	$html .= "<td align='left' valign='middle'>";
																		$html .= "<a href='mailto:".$getEnquiry['email']."'>";
																			$html .= $getEnquiry['email'];
																		$html .= "</a>";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";														
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='20'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";

								PHP_EOL;
								
							$html .= "</tbody>";
						$html .= "</table>";
						
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</tbody>";
		$html .= "</table>";
		
		// Footer
		$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
			$html .= "<tbody>";
				$html .= "<tr>";
					$html .= "<td align='center' class='content'>";
						$html .= "<table align='left' border='0' cellpadding='0' cellspacing='2' width='600'>";
							$html .= "<tbody>";
			
								// Spacer
								$html .= "<tr>";
									$html .= "<td height='10'></td>";
								$html .= "</tr>";
								
								PHP_EOL;
							
								// Information
								$html .= "<tr>";
									$html .= "<td align='center'>";
										$html .= "<h5>";
											if(!empty($compAddNumber)) $html .= $compAddNumber." ";
											if(!empty($compAddStreet)) $html .= $compAddStreet.", ";
											if(!empty($compAddCity)) $html .= $compAddCity.", ";
											if(!empty($compAddCounty)) $html .= $compAddCounty.", ";
											if(!empty($compAddCountry)) $html .= $compAddCountry.", ";
											if(!empty($compAddPostcode)) $html .= $compAddPostcode;
											$html .= "<span class='pipe'>&nbsp;&nbsp;|&nbsp;&nbsp;</span>";
											$html .= "&copy; ".date("Y")." ".$compName;
										$html .= "</h5>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Spacer
								$html .= "<tr>";
									$html .= "<td height='10'></td>";
								$html .= "</tr>";
								
								PHP_EOL;
				
							$html .= "</tbody>";
						$html .= "</table>";
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</tbody>";
		$html .= "</table>";
	
		$html .= "</body>";

		$html .= "</html>";
		
		// Send email
		if(mail($to, $subject, chunk_split(base64_encode($html)), $headers)) {
			return true;
		}
		
	}
	
	// Send password reset email
	function passwordemail($conn, $customer) {
		
		global $strsiteurl, $strdomain, $compName, $compPhone, $compEmail, $compAddNumber, $compAddStreet, $compAddCity, $compAddCounty, $compAddCountry, $compAddPostcode;
		
		// Get enquiry information
		$strdbsql = "SELECT * FROM customer WHERE recordID = :customer";
		$getCustomer = query($conn,$strdbsql,"single",["customer"=>$customer]);
		
		if($debug == false) $to = $getCustomer['email'];
		else $to = $devEmail;
		
		$subject = "Password Reset";
		
		$headers = "From: no-reply@".$strdomain."\r\n";
		$headers .= "Reply-To: ".$compName."\r\n";
		$headers .= "CC: millerwaite@hotmail.co.uk\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$headers .= "Content-Transfer-Encoding: base64\r\n";
		
		$html = "<!DOCTYPE html>";
		$html .= "<html>";

		$html .= "<head>";
			$html .= "<meta charset='UTF-8'>";
			$html .= "<title>".$getCustomer['firstname']." ".$getCustomer['surname']." has requested a password reset</title>";
			$html .= "<style>
				body{font-family:arial;}
				h1{font-size:16px;font-weight:normal;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h2{font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h3{font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				h4{font-size:14px;font-weight:bold;line-height:18px;mso-line-height-rule:exactly;margin:0;}
				h5{color:#b2b2b2;font-size:12px;line-height:24px;mso-line-height-rule:exactly;margin:0;}
				p{font-size:14px;line-height:18px;mso-line-height-rule:exactly;margin:0;}
				p.price{font-size:14px;}
				ul{list-style:none;margin:0;padding:0;}
				ul li{font-size:14px;line-height:18px;mso-line-height-rule:exactly;}
				address{font-size:14px;line-height:18px;mso-line-height-rule:exactly;}
				.content{background-color:#F5F5F5;}
				.block{background-color:#FFFFFF;}
				.container{background-color:#FFFFFF;padding:0 25px;}
				.dark{background-color:#222222;padding:0 25px;}
				.dark h3{border:none;color:#FFFFFF;font-size:18px;}
				.dark li{color:#FFFFFF;}
				.dark a{color:#FFFFFF;}
				.dark .pipe{color:#FFFFFF;}
				.light{background-color:#5B6ABF;padding:0 25px;}
				.light span{color:#FFFFFF;font-size:14px;font-weight:bold;line-height:24px;mso-line-height-rule:exactly;}
				.horizontal-space{display:inline-block;height:10px;width:100%;}
			</style>";
		$html .= "</head>";
  
		PHP_EOL;
  
		$html .= "<body>";
	
		$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
			$html .= "<tbody>";
				$html .= "<tr>";
					$html .= "<td align='center' class='content'>";
						
						$html .= "<table align='left' border='0' cellpadding='0' cellspacing='2' width='600'>";
							$html .= "<tbody>";
								
								// Header
								$html .= "<tr>";
									$html .= "<td class='container' align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
											
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='10'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
											
												// Logo
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<a href='".$strsiteurl."' target='_blank'>";
															$html .= "<img src='".$strsiteurl."images/company/logo2.png' alt='".$compName."' width='160' />";
														$html .= "</a>";
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='10'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Main Image
								$html .= "<tr>";
									$html .= "<td align='center' class='block'>";
										$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='center'>";
														$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='center'>";
																		$html .= "<img src='".$strsiteurl."images/emails/password-reset.jpg' alt='Password Reset' width='100%' />";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Introduction
								$html .= "<tr>";
									$html .= "<td align='left' class='container'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
															
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Text
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<h1>You recently requested to reset your password for your ".$compName." account. Click the button below to reset it.</h1>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Button
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<table border='0' align='left' cellpadding='0' cellspacing='0'>";
																			$html .= "<tbody>";
																				$html .= "<tr>";
																					$html .= "<td align='center' style='color:#ffffff;font-size:13px;line-height:35px;text-align:center;background-color:#DB0723;padding: 0 20px;display:inline-block;'>";
																						$html .= "<a href='".$strsiteurl."forgot-password/".$getCustomer['username']."/".$getCustomer['requestHash']."' target='_blank' style='color:#ffffff;text-decoration:none;'>";
																							$html .= "Reset your password";
																						$html .= "</a>";
																					$html .= "</td>";
																				$html .= "</tr>";
																			$html .= "</tbody>";
																		$html .= "</table>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																// Text
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<p>This password reset is only valid for the next 30 minutes. If you did not request a password reset, please ignore this email or reply to let us know.</p>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																// Text
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<p>If you are having trouble clicking the password reset button, copy and paste the URL below into your web browser.</p>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																// Link
																$html .= "<tr>";
																	$html .= "<td align='left'>";
																		$html .= "<p><a href='".$strsiteurl."forgot-password/".$getCustomer['username']."/".$getCustomer['requestHash']."'>".$strsiteurl."forgot-password/".$getCustomer['username']."/".$getCustomer['requestHash']."</a></p>";
																	$html .= "</td>";
																$html .= "</tr>";
																
																// Spacer
																$html .= "<tr>";
																	$html .= "<td align='left' height='20'></td>";
																$html .= "</tr>";
																
																PHP_EOL;
																
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Contact
								$html .= "<tr>";
									$html .= "<td class='dark'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
											
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='20'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Opening Hours
												$html .= "<tr>";
													$html .= "<td align='left'><h3>Questions? We're on call.</h3></td>";
												$html .= "</tr>";
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<ul>";
															$html .= "<li>Monday - 08:00 - 17:00</li>";
															$html .= "<li>Tuesday - 08:00 - 17:00</li>";
															$html .= "<li>Wednesday - 08:00 - 15:00</li>";
															$html .= "<li>Thursday - 08:00 - 17:00</li>";
															$html .= "<li>Friday - 08:00 - 17:00</li>";
															$html .= "<li>Saturday - 08:00 - 15:00</li>";
															$html .= "<li>Sunday - Closed</li>";
														$html .= "</ul>";
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Contact Links
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<a href='tel:".$compPhone."'>";
															$html .= "<img src='".$strsiteurl."images/emails/bubble.png' class='icon-bubble' alt='Phone' width='28' />";
															$html .= $compPhone;
														$html .= "</a>";
														$html .= "<span class='pipe'>&nbsp;&nbsp;|&nbsp;&nbsp;</span>";
														$html .= "<a href='mailto:".$compEmail."'>";
															$html .= "<img src='".$strsiteurl."images/emails/email.png' class='icon-email' alt='Email' width='28' />";
															$html .= $compEmail;
														$html .= "</a>";
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td align='left' height='20'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";

								PHP_EOL;
								
							$html .= "</tbody>";
						$html .= "</table>";
						
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</tbody>";
		$html .= "</table>";
		
		// Footer
		$html .= "<table align='center' border='0' cellpadding='0' cellspacing='0' width='100%'>";
			$html .= "<tbody>";
				$html .= "<tr>";
					$html .= "<td align='center' class='content'>";
						$html .= "<table align='left' border='0' cellpadding='0' cellspacing='2' width='600'>";
							$html .= "<tbody>";
			
								// Spacer
								$html .= "<tr>";
									$html .= "<td height='10'></td>";
								$html .= "</tr>";
								
								PHP_EOL;
							
								// Information
								$html .= "<tr>";
									$html .= "<td align='center'>";
										$html .= "<h5>";
											if(!empty($compAddNumber)) $html .= $compAddNumber." ";
											if(!empty($compAddStreet)) $html .= $compAddStreet.", ";
											if(!empty($compAddCity)) $html .= $compAddCity.", ";
											if(!empty($compAddCounty)) $html .= $compAddCounty.", ";
											if(!empty($compAddCountry)) $html .= $compAddCountry.", ";
											if(!empty($compAddPostcode)) $html .= $compAddPostcode;
											$html .= "<span class='pipe'>&nbsp;&nbsp;|&nbsp;&nbsp;</span>";
											$html .= "&copy; ".date("Y")." ".$compName;
										$html .= "</h5>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Spacer
								$html .= "<tr>";
									$html .= "<td height='10'></td>";
								$html .= "</tr>";
								
								PHP_EOL;
				
							$html .= "</tbody>";
						$html .= "</table>";
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</tbody>";
		$html .= "</table>";
	
		$html .= "</body>";

		$html .= "</html>";
		
		// Send email
		if(mail($to, $subject, chunk_split(base64_encode($html)), $headers)) {
			return true;
		}
		
	}
	
	// Check customer information
	function checkcustomer($conn, $accountid, $title, $firstname, $lastname, $telephone, $address1, $address2, $address3, $address4, $address5, $address6, $postcode, $email) {
	
		// Check if the user is trying to create an account
		if($accountid == 0) {
			
			// Check if customer has previously checked out
			$strdbsql = "SELECT * FROM customer WHERE email = :email";
			$customer = query($conn,$strdbsql,"single",["email"=>$email]);
			
			if(!$customer) {
				
				// Create customer account
				$strdbsql = "INSERT INTO customer (groupID, title, firstname, surname, telephone, email) VALUES (:group, :title, :firstname, :lastname, :telephone, :email)";
				$addCustomer = query($conn,$strdbsql,"insert",["group"=>1,"title"=>$title,"firstname"=>$firstname,"lastname"=>$lastname,"telephone"=>$telephone,"email"=>$email]);
				
				$customerid = $addCustomer;
				
				// Create an address for customer
				$strdbsql = "INSERT INTO customer_address (customerID, title, firstname, surname, add1, add2, add3, town, county, country, postcode) VALUES (:customer, :title, :firstname, :lastname, :address1, :address2, :address3, :town, :county, :country, :postcode)";
				$addAddress = query($conn,$strdbsql,"insert",["customer"=>$customerid,"title"=>$title,"firstname"=>$firstname,"lastname"=>$lastname,"address1"=>$address1,"address2"=>$address2,"address3"=>$address3,"town"=>$address4,"county"=>$address5,"country"=>$address6,"postcode"=>$postcode]);
				
				// Assign address to customer
				$strdbsql = "UPDATE customer SET defaultDeliveryAdd = :delivery, defaultBillingAdd = :billing WHERE recordID = :customer";
				$setAddress = query($conn,$strdbsql,"update",["customer"=>$customerid,"delivery"=>$addAddress,"billing"=>$addAddress]);
				
			} else {
				
				$customerid = $customer['recordID'];
				
			}
			
		} else {
			
			$customerid = $accountid;
			
		}
		
		return $customerid;
		
	}
	
	
/**
 * calculate postage
 *
 * @param PDO		$conn		Current Database connection
 * @param int		$option		Postage type selected
 * @param double	$weight		Order Weight
 * @param string	$country		delivery location
 * @param string	$postcode		delivery location
 * @param double	$orderAmount	bastket total
 *
 * @return double					post of delivery
 **/
	function checkPostage($conn,$option,$weight, $country,$postcode,$orderAmount) {
		$postage = null;
		if($country == "") {
			$country = "United Kingdom";
		}
		
		//get base rate for this option and order weight
		$postageQry = "SELECT cost,maxWeight FROM deliveryOptionRates WHERE deliveryOptionID = :option ";
		$rate = query($conn,$postageQry,"multi",['option'=>$option]);
		if(count($rate) == 1 && ($rate[0]['maxWeight'] == 0 || $rate[0]['maxWeight'] > $weight)) {
			$baseRate = $rate[0]['cost'];
		} else {
			//handle it here if rates vary per weight
			foreach($rate As $r) {
				//weight checks go here
					$baseRate = $r['cost'];
			}
		}
		
		
		//adjustments by destination
		$strdbsql = "SELECT zone FROM configCountries WHERE countryName LIKE :country";
		$countryDetails = query($conn,$strdbsql,"single",["country" => $country]);
		if(!empty($countryDetails)) {
			$strdbsql = "SELECT * FROM deliveryLocations WHERE zone LIKE :zone AND (country LIKE :country OR country LIKE '*') AND (minOrderValue <= :orderValue)";
			$locations = query($conn,$strdbsql,"multi",["zone"=>$countryDetails['zone'], "country"=>$country, "orderValue" =>$orderAmount]);
			
			if(count($locations) == 1 && $locations[0]['postcode'] == '*') {
				if($locations[0]['freeIfOver'] > 0 AND $orderAmount > $locations[0]['freeIfOver']) {
					$postage = 0;
				} else {
					$postage = $baseRate+$locations[0]['cost'];
				}
			} else {
				//sort out country & postcode matching
				foreach($locations As $l) {
					//address tests go here
						if($l['freeIfOver'] > 0 AND $orderAmount > $l['freeIfOver']) {
							$postage = 0;
						} else {
							$postage = $baseRate+$l['cost'];
						}
				}
			}
		}
		return $postage;
	}
	
	
/**
 * Calculate details of basket contents
 *
 * @param	PDO 	$conn			current database connection
 * @param	int		$basketid		unique ref for current basket
 *
 * @return array					details of current basket
 **/
 
	function basketCheckContents($conn,$basketid) {
		$basket = new stdClass();
		
		//set defaults
		$basket->rowcount = 0;
		$basket->total = 0;
		$basket->qtyError = false;
		$basket->packSizeWarning = false; 
		$basket->vat = 0;
		
		$seedsPerItem = [];
		
		// Get basket items
		if($basketid > 0) {
			$strdbsql = "SELECT basket_items.*, stock_group_information.productType, stock_units.standard, stock.stockCode, 
			((basket_items.quantity * basket_items.price) - ((basket_items.quantity * basket_items.price)/((100+site_vat.amount)/100))) AS itemVAT 
			FROM basket_items 
			INNER JOIN stock ON basket_items.stockID = stock.recordID 
			INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
			INNER JOIN stock_units ON stock.unit = stock_units.recordID
			INNER JOIN site_vat ON stock.vatID = site_vat.recordID
			WHERE basket_items.basketHeaderID = :basket";
			$result4 = query($conn,$strdbsql,"multi",["basket"=>$basketid]);
			if(count($result4) > 0) {
				$basket->rowcount = count($result4);
				foreach($result4 AS $row4) {
					$basket->total += $row4['price'] * $row4['quantity'];
					if(!isset($seedsPerItem[$row4['stockCode']])) {
						$seedsPerItem[$row4['stockCode']] = 0;
					}
					$seedsPerItem[$row4['stockCode']] += $row4['quantity']*$row4['standard'];
					if($row4['standard'] > 100) {
						$basket->packSizeWarning = true;
					}
					$basket->vat += $row4['itemVAT'];
				}
			}
		}
		$qtyError = false;
		foreach($seedsPerItem AS $item => $qty) {
			if($qty > 25000) {
				$basket->qtyError = true;
			}
		}
		return $basket;
	}

	
/**
 * Calculate discount on this basket
 *
 *
 * @param	PDO 	$conn			current database connection
 * @param	int		$basketid		unique ref for current basket
 * @param	double	$baskettotal	value of current basket
 *
 * @return double					value of currently applied discount
 **/
	
	function getDiscountAmount($conn,$basketid,$baskettotal) {
		$discount = new stdClass();
		$discount->codeID = null;
		$discount->amount = round(0 , 2);
		$discount->description = "";
		// Get basket offers
		if($basketid > 0) {
			$strdbsql = "SELECT basket_header.offerCodeID AS offerID, basket_header.discountAmount AS discountTotal, site_offer_codes.* FROM basket_header 
			LEFT JOIN site_offer_codes ON basket_header.offerCodeID = site_offer_codes.recordID 
			WHERE basket_header.recordID = :basket";
			$result4 = query($conn,$strdbsql,"single",["basket"=>$basketid]);
			
			if($result4['discountTotal'] > 0) {
				//TODO check qualifying items, min amounts etc.
				$discount->codeID = $result4['offerID'];
				$discount->description = $result4['description'];
				if($result4['discountType'] == "percent") $discount->amount = round($baskettotal * (100 - $result4['discountAmount']) / 100, 2);
				else $discount->amount = round($result4['discountAmount'], 2);					
				
				if(true ) { /*offer no longer valid */
					//remove offer
				} else if($discount->amount != $result4['discountTotal']) { /* change in amounts */
					//update
					
				}
			}
		}
		return $discount;
	}
	
	// Bank Holidays
	function fn_findbankholidays($datknowndate,$intdirection) {
		// $datknowndate = The date we know

		// $intdirection = -1 = $intdays before $datknowndate
		// $intdirection = 1  = $intdays after $datknowndate

		// working variables
		$intseconds = $intdirection * 86400;
		$intcountdays = 0;
		$intworkingday = 0;

		// find hour mins and secs of known date, and use these to generate holidays
		$inthour = date("H",$datknowndate);
		$intmins = date("i",$datknowndate);
		$intsecs = date("s",$datknowndate);

		// generate array of bank holidays
		$arrholidays = array();
		
		// normal bank holidays
		$datholiday = strtotime("first Monday of May ".date("Y",$datknowndate)) + 7200;
		$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
		array_push($arrholidays, $datholiday);
		$datholiday = strtotime("last Monday of May ".date("Y",$datknowndate)) + 7200;
		$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
		array_push($arrholidays, $datholiday);
		$datholiday = strtotime("last Monday of August ".date("Y",$datknowndate)) + 7200;
		$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
		array_push($arrholidays, $datholiday);

		// easter
		$intplusdays = easter_days(date("Y",$datknowndate));
		$datholiday = gmmktime($inthour,$intmins,$intsecs,3,21+$intplusdays-2,date("Y",$datknowndate));
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,3,21+$intplusdays+1,date("Y",$datknowndate));
		array_push($arrholidays, $datholiday);

		// christmas this year and last, just in case
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,25,date("Y",$datknowndate));
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,25,date("Y",$datknowndate)-1);
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);

		// boxing day this year and last, just in case
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,26,date("Y",$datknowndate));
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,26,date("Y",$datknowndate)-1);
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);

		// new years day this year and next, just in case
		$datholiday = gmmktime($inthour,$intmins,$intsecs,1,1,date("Y",$datknowndate));
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,1,1,date("Y",$datknowndate)+1);
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);
		
		return $arrholidays;
	}
	
	function expectedDelivery($date, $postage) {
		
		$day = date("N", $date); // Mon(1), Sun(7)
		$time = date("Hi", $date); // 0000 - 2359
		
		// Disallow Saturday/Sunday Delivery
		if($postage == "uk") {
			if(($day == 5 && $time >= 1500) || $day == 6 || $day == 7) $expected = strtotime("next tuesday", $date); // Friday/Friday After Deadline/Saturday/Sunday - Tuesday
			else if(($day == 4 && $time >= 1500) || ($day == 5 && $time < 1500)) $expected = strtotime("next monday", $date); // Thursday After Deadline/Friday Before Deadline - Monday
			else if($time >= 1500) $expected = strtotime("+2 days", $date); // Weekday After Deadline - Two Days
			else $expected = strtotime("+1 days", $date); // Weekday Before Deadline - Next Day
		} else {
			if($day == 5 || $day == 6 || $day == 7) $expected = strtotime("+2 days", strtotime("next monday", $date)); // Friday/Saturday/Sunday - Wednesday
			else if($day == 3) $expected = strtotime("next monday", $date); // Wednesday - Monday
			else if($day == 4) $expected = strtotime("+1 days", strtotime("next monday", $date)); // Thursday - Tuesday
			else $expected = strtotime("+3 days", $date); // Three Days
		}
		
		// Disallow Bank Holiday Delivery
		$holidays = fn_findbankholidays($expected, 1);
		if(in_array($expected, $holidays)) $expected = strtotime("+1 days", $expected);
		
		return date("l jS F", $expected);
	}
	
?>