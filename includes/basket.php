<?php

	if($basketcount > 0) {
		
		print("<div class='row crop-bottom'>");
			print("<div class='col s12 m12 l6 right'>");
				
				print("<div class='note'>");
					print("<i class='material-icons'>help_outline</i>");
					print("Need help with your order?");
					if(!empty($compPhone)) print(" Phone us on ".$compPhone." or <a href='/contact-us'>contact us online</a>.");
					else print(" <a href='/contact-us'>Contact us online</a>");
				print("</div>");
				
				$seedsPerItem = [];
				$packSizeWarning = false;
				
				// Products
				print("<div class='basket-wrapper'>");
					
					// Get items in basket
					$strdbsql = "SELECT 
					(basket_items.quantity * basket_items.price) AS itemPrice, 
					basket_items.quantity AS itemQuantity, 
					stock.recordID, 
					stock.stockCode,
					stock.groupID,
					((basket_items.quantity * basket_items.price) - ((basket_items.quantity * basket_items.price)/((100+site_vat.amount)/100))) AS itemVAT,
					stock_units.description AS unitDescription,
					stock_units.standard,
					stock_group_information.name AS groupName, 
					stock_group_information.summary AS groupDescription 
					FROM basket_items 
					INNER JOIN stock ON basket_items.stockID = stock.recordID 
					LEFT JOIN stock_units ON stock.unit = stock_units.recordID
					INNER JOIN site_vat ON stock.vatID = site_vat.recordID
					INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
					WHERE basket_items.basketHeaderID = :basket";
					$result = query($conn,$strdbsql,"multi",array("basket"=>$basketid));
					
					$vatAmount = 0.00;
					
					if(count($result) > 0) {
						
						print("<div class='basket-container'>");
							
							foreach($result AS $row) {
								
								print("<div id='row-".$row['recordID']."' class='basket-row'>");
								
									// Image gallery
									$strdbsql = "SELECT * FROM stock_images WHERE stockID = :stockID AND imageTypeID = 1 ORDER BY imageOrder LIMIT 1";
									$result2 = query($conn,$strdbsql,"single",array("stockID"=>$row['groupID']));
								
									print("<div class='basket-image'>");
										if($result2) print("<img src='/images/products/main/".$result2['imageLink']."' alt='".$result['groupName']."' />");
										else print("<span>No image available</span>");
									print("</div>");
									
									// Uses /js/custom.js to call /ajax/basket.php
									print("<div class='basket-content'>");
										if(!empty($row['groupName'])) print("<span class='title'>".$row['groupName']."</span>");
										if(!empty($row['unitDescription'])) print("<span>".substr($row['unitDescription'], 0, 100)."</span>");
										print("<div class='basket-options'>");
											print("<form action='#' method='post'>");
												print("<div class='input-field'>");
													print("<button class='btn btn-counter basket-edit negative' data-id='".$row['recordID']."' data-count='-1' ".(number_format($row['itemQuantity'], 0) > 1 ? "" : "disabled='disabled'")." aria-label='Decrease Quantity'><i class='material-icons'>remove</i></button>");
													print("<input type='text' class='counter' name='frm_qty' data-id='".$row['recordID']."' aria-label='Current Quantity' value='".number_format($row['itemQuantity'], 0)."' readonly />");
													print("<button class='btn btn-counter basket-edit positive' data-id='".$row['recordID']."' data-count='1' aria-label='Increase Quantity'><i class='material-icons'>add</i></button>");
													print("<input type='text' class='total' name='frm_total' data-id='".$row['recordID']."' aria-label='Line Total' value='&pound;".number_format($row['itemPrice'], 2)."' readonly />");
													print("<button class='btn btn-tertiary isolate basket-remove' data-id='".$row['recordID']."' aria-label='Remove Item'><i class='material-icons'>close</i></button>");
												print("</div>");
											print("</form>");
										print("</div>");
									print("</div>");
									
								print("</div>");
								if(!isset($seedsPerItem[$row['stockCode']])) {
									$seedsPerItem[$row['stockCode']] = 0;
								}
								$seedsPerItem[$row['stockCode']] += $row['itemQuantity']*$row['standard'];
								if($row['standard'] > 100) {
									$packSizeWarning = true;
								}
								$vatAmount += $row['itemVAT'];
							}
							$qtyError = false;
							foreach($seedsPerItem AS $item => $qty) {
								if($qty > 25000) {
									$qtyError = true;
								}
							}

							// Basket Summary
							print("<div class='basket-summary'>");
								//TODO: Test server side discount processing
								// Customer Offer
								if(isset($basketoffer)) {
									print("<div class='alert warning active'>");
										print("<span><strong>Customer Offer:</strong> ".$basketoffer."</span>");
									print("</div>");
								}
								
								// Discount
								if($offerid > 0) {
									$strdbsql = "SELECT * FROM site_offer_codes WHERE recordID = :offer";
									$result2 = query($conn,$strdbsql,"single",["offer"=>$offerid]);
								}
								
								print("<div id='discount-off' class='input-field'>");
									print("<ul class='collapsible' data-collapsible='accordion'>");
										print("<li>");
											print("<div class='collapsible-header active'><i class='material-icons right'>arrow_drop_down</i>Have a promo code?</div>");
											print("<div class='collapsible-body'>");
												print("<form id='discount' action='' method='post'>");
													print("<div class='input-field'>");
														print("<button type='submit' class='btn btn-contact small postfix'>Apply</button>");
														print("<input type='text' id='frm_discount' name='frm_discount' aria-label='Enter your discount code' placeholder='Enter your discount code...' />");
													print("</div>");
												print("</form>");
											print("</div>");
										print("</li>");
									print("</ul>");
								print("</div>");
								
								print("<div id='discount-on' class='input-field discount ".($basketdiscount == 0 ? "hide" : "")."'>");
									print("<button id='discount-remove' class='btn btn-icon ".($discountoptional > 0 ? "" : "hide")."' title='Remove' aria-label='Remove Discount Code'><i class='material-icons'>close</i></button>");
									print("<span id='discount-label'>".($discountlabel ? $discountlabel : "Discount:")."</span>");
									print("<span id='discount-value' class='right'>- &pound;".$basketdiscount."</span>");
								print("</div>");
								
								//TODO: add in code for user selectable delivery options!
								//current all checked in incsitecommon and can't be changed here
								
								// Delivery
								
								print("<div class='input-field'>");
									print("<span>Delivery Fee:</span>");
									print("<span class='right' id='delivery-value'>".(($deliveryamount !== null) ? "&pound;".number_format($deliveryamount,2) : "TBC")."</span>");
								print("</div>");
								
								// VAT
								print("<div class='input-field'>");
									print("<span>VAT:</span>");
									print("<span class='right' id='vat-amount'>&pound;".number_format($vatAmount,2)."</span>");
								print("</div>");
								
								// Total
								print("<div class='input-field'>");
									print("<span>Total</span>");
									print("<span id='basket-final' class='right'>&pound;".number_format($baskettotal,2)."</span>");
								print("</div>");
								
							print("</div>");
							
						print("</div>");

						print("<div id='qtyWarning' class='alert warning".(($packSizeWarning || $qtyError) ? " active" : "")."'>");
							print("<span>Please contact us for orders over 25,000 seeds and give three weeks for shipping with 10,000 seeds and two weeks for any packet size over 100 seeds.</span>");
						print("</div>");
						
					}
					
				print("</div>");
				
			print("</div>");
			print("<div class='col s12 m12 l6'>");
				print("<div class='sidebar'>");
				
					print("<form id='payment-form' class='checkout' name='payment-form' action='/confirmation' method='post'>");
						
						print("<input type='hidden' id='frm_method' name='frm_method' value='stripe' />");
						
						// Stripe payment
						if($basketcount > 0) {
							print("<div class='payment-options".($baskettotal >= 15 ? "" : " hide")."'>");
								
								// Tabs
								print("<ul class='tabs'>");
									print("<li class='tab'><a class='active' href='#stripe'>Credit/Debit Card</a></li>");
									print("<li class='tab'><a class='active' href='#paypal'><img src='/images/elements/paypal.png' alt='PayPal' /></a></li>");
								print("</ul>");
								
								// Stripe Checkout
								print("<div id='stripe' class='tab-content'>");
								
									// Customer Details
									print("<div class='box'>");
										print("<h3>Your Details</h3>");
										print("<div class='row crop-bottom'>");
										
											// Title
											$titles = array();
											
											$titles[0] = array("label"=>"Mr","value"=>"Mr");
											$titles[1] = array("label"=>"Miss","value"=>"Miss");
											$titles[2] = array("label"=>"Mrs","value"=>"Mrs");
											$titles[3] = array("label"=>"Ms","value"=>"Ms");
											$titles[4] = array("label"=>"Other","value"=>"Other");
											
											print("<div class='col s12 m12 l2'>");
												print("<div class='input-field'>");
													print("<select class='browser-default required".(!empty($customerfirstname) ? " autocomplete" : "")."' aria-label='Title' id='frm_title' name='frm_title' tabindex='1'>");
														print("<option value='' disabled".($accountid == 0 ? " selected" : "").">Title</option>");
														foreach($titles AS $title) {
															print("<option value='".$title['value']."'".($customertitle == $title['label'] ? " selected" : "").">".$title['label']."</option>");
														}
													print("</select>");
												print("</div>");
											print("</div>");
											
											// First Name
											print("<div class='col s12 m12 l5'>");
												print("<div class='input-field'>");
													print("<input type='text' class='required".(!empty($customerfirstname) ? " autocomplete" : "")."' aria-label='First Name' id='frm_firstname' name='frm_firstname' tabindex='2' placeholder='First Name' value='".$customerfirstname."' />");
												print("</div>");
											print("</div>");
											
											// Last Name
											print("<div class='col s12 m12 l5'>");
												print("<div class='input-field'>");
													print("<input type='text' class='required".(!empty($customersurname) ? " autocomplete" : "")."' aria-label='Last Name' id='frm_lastname' name='frm_lastname' tabindex='2' placeholder='Last Name' value='".$customersurname."' />");
												print("</div>");
											print("</div>");
											
											// Email Address
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<input type='email' class='required".(!empty($customersurname) ? " autocomplete" : "")."' aria-label='Email' id='frm_email' name='frm_email' tabindex='4' placeholder='Email Address' value='".$customeremail."' />");
												print("</div>");
											print("</div>");
											
										print("</div>");
									print("</div>");
									
									// Delivery Address
									print("<div class='box'>");
										print("<h3>Delivery Address</h3>");
										print("<div class='row'>");

											print("<div class='col s12 m10 l8'>");
												print("<div class='input-field'>");
													print("<select class='required browser-default ".(!empty($customercountry) ? " autocomplete" : "")."' aria-label='country' id='frm_country' name='frm_country' data-label='country' tabindex='6' placeholder='Country' >");
													$q = "SELECT configCountries.countryName FROM configCountries INNER JOIN deliveryLocations ON configCountries.zone = deliveryLocations.zone WHERE deliveryLocations.allowDelivery = 1 ORDER BY deliveryLocations.recordID, configCountries.countryName";
													$r = query($conn,$q,"multi");
												//	error_log("Basket: ".$customercountry. " ".implode(":",debug_backtrace ()));
													foreach($r AS $c) {
														print("<option value='".$c['countryName']."' ".((!empty($customercountry) && $customercountry == $c['countryName'])  ? " selected" : "").">".$c['countryName']."</option>");
													}
													print("</select>");
												print("</div>");
											print("</div>");
											
											// Postcode
											print("<div class='col s12 m10 l8'>");
												print("<div class='input-field'>");
													print("<button id='pcButton' class='btn btn-tertiary small postfix postcode-search'>Find Address</button>");
													print("<input type='text' aria-label='Postal Code' class='required leader code".(!empty($customerpostcode) ? " autocomplete" : "")."' id='frm_postcode' name='frm_postcode' data-label='postcode' maxlength='7' tabindex='6' placeholder='Postcode' value='".$customerpostcode."' />");
												print("</div>");
											print("</div>");
											print("<div class='col s12 m12 l4'>");
												print("<div class='input-field'>");
													print("<a id='postcodeLookupToggle' href='javascript:pcLookup()' class='btn transparent'>(Enter Manually)</a>");
												print("</div>");
											print("</div>");
											
											print("<div id='address'".(empty($customeraddress1) && empty($customertown) && empty($customercounty) ? " class='hide'" : "").">");
												print("<div class='col s12'>");
													print("<div class='input-field' id='address1'>");
														print("<input type='text' class='required depend ".(!empty($customeraddress1) ? "autocomplete" : "")."' id='frm_address' name='frm_address' aria-label='Street Address' tabindex='7' value='".$customeraddress1."' />");
													print("</div>");
												print("</div>");
												print("<div class='col s12 m12 l6'>");
													print("<div class='input-field'>");
														print("<input type='text' class='required depend ".(!empty($customertown) ? "autocomplete" : "")."' id='frm_town' name='frm_town' aria-label='Town' tabindex='8' value='".$customertown."' />");
													print("</div>");
												print("</div>");
												print("<div class='col s12 m12 l6'>");
													print("<div class='input-field'>");
														print("<input type='text' class='required depend ".(!empty($customercounty) ? "autocomplete" : "")."' id='frm_county' aria-label='County' name='frm_county' tabindex='9' value='".$customercounty."' />");
													print("</div>");
												print("</div>");
											print("</div>");
											
										print("</div>");
									
										// Delivery Date/Time
								/*		print("<div class='row crop-bottom'>");
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<label>When would you like us to deliver your order? Orders received before 4pm will qualify for next day delivery. We do not deliver on Sundays.</label>");
												print("</div>");
												print("<div class='input-field crop-top'>");
													print("<div class='row crop-bottom'>");
														print("<div class='col s12 m6'>");
															print("<input type='text' class='datepicker' id='frm_date' name='frm_date' tabindex='10' value='' readonly />");
														print("</div>");
														
														// Delivery Time
														$strdbsql = "SELECT * FROM deliveryTimes WHERE enabled = 1 ORDER BY recordID";
														$times = query($conn,$strdbsql,"multi",null);
														
														if(count($times) > 0) {
															print("<div class='col s12 m6'>");
																print("<select class='browser-default' id='frm_time' name='frm_time' tabindex='11'>");
																	print("<option value='0' selected>Any</option>");
																	foreach($times AS $time) {
																		print("<option value='".$time['recordID']."'>".substr($time['startTime'], 0, 5)." - ".substr($time['endTime'], 0, 5)."</option>");
																	}
																print("</select>");
															print("</div>");
														}
													print("</div>");
												print("</div>");
											print("</div>");
										print("</div>");
										print("<div id='nextday' class='alert warning' style='margin-top:10px;'>Next day delivery is unavailable when ordering after 4pm.</div>");*/
										
										// Delivery Instructions
									print("<div class='row crop-bottom'>");
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<label for='frm_note'>Where shall we leave your delivery if you aren't home? Is there anything that will help us find you?</label>");
													print("<textarea class='materialize-textarea".(!empty($customernote) ? " autocomplete" : "")."' id='frm_note' name='frm_note' data-length='500' maxlength='500' tabindex='13' placeholder=\"For example: We're up the lane by the school. Please leave the boxes in the porch.\">".$customernote."</textarea>");
												print("</div>");
											print("</div>");
										print("</div>");
									print("</div>");
									
									// Contact Information
									print("<div class='box'>");
										print("<div class='row'>");
											print("<h3>Contact Information</h3>");
										
											// Telephone
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<input type='tel' class='required".(!empty($customertelephone) ? " autocomplete" : "")."' aria-label='Telephone or Mobile Number' name='frm_telephone' data-label='contact number' tabindex='14' maxlength='11' placeholder='Telephone/Mobile Number' value='".$customertelephone."' />");
												print("</div>");
											print("</div>");
											
											// Marketing
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<label for='frm_hear'>How did you hear about us?</label>");
													print("<select class='browser-default' id='frm_hear' name='frm_hear' data-extra='marketing' tabindex='15'>");
														print("<option value='' disabled selected>Please Select</option>");
														print("<option value='Recommended by a friend'>Recommended by a friend</option>");
														print("<option value='Social media'>Social media</option>");
														print("<option value='Internet'>Internet</option>");
														print("<option value='other'>Other</option>");
													print("</select>");
												print("</div>");
											print("</div>");
											
											// Additional Marketing
											print("<div id='marketing' class='col s12 hide'>");
												print("<div class='input-field'>");
													print("<textarea class='materialize-textarea' aria-label='Please describe how you heard about us' id='frm_hear_more' name='frm_hear_more' tabindex='16' data-length='500' maxlength='500' placeholder='For example: I Discovered one of your leaflets at a local supermarket.'></textarea>");
												print("</div>");
											print("</div>");
											
										print("</div>");
										
										// Newsletter
										print("<div class='row crop-bottom'>");
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<label for='frm_newsletter'>");
														print("<input type='checkbox' class='filled-in' id='frm_newsletter' name='frm_newsletter' tabindex='17' />");
														print("<span>Tick this one to hear about our ".$compName." exclusive offers, insider news and much more (no rubbish we promise).</span>");
													print("</label>");
												print("</div>");
											print("</div>");
										print("</div>");
										
									print("</div>");
								
									print("<div class='box'>");
										print("<div class='row crop-bottom'>");
											print("<h3>Payment Information</h3>");
											
											// Card Details
											print("<div class='input-field crop-top'>");
												print("<div class='form-row'>");
													print("<div id='card-element'></div>");
													print("<div id='card-errors' class='label-note error' role='alert'></div>");
												print("</div>");
											print("</div>");
										
											// Button
											print("<div class='input-field'>");
												print("<button type='submit' tabindex='18' class='btn btn-primary wide'>Complete Order</button>");
											print("</div>");
											
										print("</div>");
									print("</div>");
									
								print("</div>");
								
								// PayPal Checkout
								print("<div id='paypal' class='tab-content'>");
									
									// Delivery Address
									print("<div class='box'>");
										print("<h3>Delivery Address</h3>");
										print("<div class='row crop-bottom'>");

											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<label for='frm_ppcountry'>Please note that delivery fees may vary based on your delivery address.</label>");
												print("</div>");
											print("</div>");
											print("<div class='col s12 m10 l8'>");
												print("<div class='input-field crop-top'>");
													print("<select class='browser-default ".(!empty($customercountry) ? " autocomplete" : "")."' id='frm_ppcountry' aria-label='Delivery Country' name='frm_ppcountry' data-label='country' tabindex='6' placeholder='Country' >");
													
													$q = "SELECT configCountries.countryName FROM configCountries INNER JOIN deliveryLocations ON configCountries.zone = deliveryLocations.zone WHERE deliveryLocations.allowDelivery = 1 ORDER BY deliveryLocations.recordID, configCountries.countryName";
													$r = query($conn,$q,"multi");
													
												//	error_log("Basket: ".$customercountry. " ".implode(":",debug_backtrace ()));
													print("<option value='' selected disabled>Select a Country</option>");
													foreach($r AS $c) {
													//	print("<option value='".$c['countryName']."' ".((!empty($customercountry) && $customercountry == $c['countryName'])  ? " selected" : "").">".$c['countryName']."</option>");
														print("<option value='".$c['countryName']."'>".$c['countryName']."</option>");
													}
													
													print("</select>");
												print("</div>");
											print("</div>");
											
											// Postcode
											//TODO if postcode is used in delivery calculate add class required to field and remove class hide from column
											print("<div class='col s12 m10 l8 hide'>");
												print("<div class='input-field'>");
													print("<input type='text' class='leader code".(!empty($customerpostcode) ? " autocomplete" : "")."' id='frm_pppostcode' aria-label='Delivery Postcode' name='frm_pppostcode' data-label='postcode' maxlength='7' tabindex='6' placeholder='Postcode' value='".$customerpostcode."' />");
												print("</div>");
											print("</div>");
											
										print("</div>");
									print("</div>");
									
								//	print("<div class='input-field'>");
								//		print("<p>We don't store any financial information.</p>");
								//	print("</div>");
								
									print("<div id='paypal-box' class='box hide'>");
										print("<h3>Proceed To Payment</h3>");
										print("<div class='row crop-bottom'>");
											print("<div class='input-field crop-top'>");
												print("<p id='msg' class='hide'></p>");
												print("<div id='paypal-button'></div>");
											print("</div>");
										print("</div>");
									print("</div>");
								
								print("</div>");
							print("</div>");
							
							print("<div id='minOrder' class='alert warning".(($baskettotal >= 15) ? "" : " active")."'>");
								print("<span>");
									print("SelectSunflowers.com requires a minimum purchase total of £15.00 on all orders. We realize that this policy is not always convenient to you, our valued customer.<br/><br/>");
									print("However, the real constraints of packaging, shipping, and internet sales dictate this minimum transaction amount if we are to remain a viable business that continues to bring you innovative sunflower offerings. <br/><br/>");
									print("We invite you to sample other varieties to complete your minimum order.");
								print("</span>");
							print("</div>");

						}
						
					print("</form>");
				print("</div>");
			print("</div>");
		print("</div>");
		
		print("<div id='order-cancel' class='modal small'>");
			print("<div class='modal-content'>");
				print("<h4 class='h3'><i class='material-icons right modal-close'>close</i>Order Cancelled</h4>");
				print("<p>Your order has been cancelled and you have not been charged. We have saved your basket for later.</p>");
				print("<div class='panel'>");
					print("<a href='/contact-us' class='btn btn-tertiary'>Report this issue</a>");
				print("</div>");
			print("</div>");
		print("</div>");
		
		print("<div id='order-error' class='modal small'>");
			print("<div class='modal-content'>");
				print("<h4 class='h3'><i class='material-icons right modal-close'>close</i>Something Went Wrong</h4>");
				print("<p>There was an issue processing your order. Not to worry, we still have your items saved.</p>");
				print("<div class='panel'>");
					print("<a href='/contact-us' class='btn btn-tertiary'>Report this issue</a>");
				print("</div>");
			print("</div>");
		print("</div>");
		
	} else {
		
		print("<div class='row'>");
			print("<div class='col s12'>");
				print("<h1>Your basket is currently empty.</h1>");
				print("<p class='crop-bottom'>Continue shopping and add items to your basket to access the checkout:</p>");
			print("</div>");
		print("</div>");
		
		print("<div class='row'>");
			print("<div class='col s12'>");
				print("<a href='".$strsiteurl."' class='btn btn-primary large' style='margin-right:10px;'>Home</a>");
				print("<a href='".$strsiteurl."shop' class='btn btn-tertiary large'>Continue Shopping</a>");
			print("</div>");
		print("</div>");
		
	}
	
?>