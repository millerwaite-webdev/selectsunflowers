<?php

	// Contact info
	print("<div class='row extra contact-info'>");
		if($compPhone || $compMobile) {
			print("<div class='col s12 m6 l2'>");
				print("<img src='/images/elements/contact-icon1.png' alt='Talk to Us' />");
				print("<h3>Talk to Us</h3>");
				if($compPhone) print("<p>Call us:<br/><a href='tel:".str_replace(" ", "", $compPhone)."'>".$compPhone."</a></p>");
				if($compMobile) print("<p>Call us:<br/><a href='tel:".str_replace(" ", "", $compMobile)."'>".$compMobile."</a></p>");
			print("</div>");
		}
		if($compEmail) {
			print("<div class='col s12 m6 l3'>");
				print("<img src='/images/elements/contact-icon2.png' alt='Email Us' />");
				print("<h3>Email Us</h3>");
				if($compEmail) print("<p>Enquiries:<br/><a href='mailto:".$compEmail."'>".$compEmail."</a></p>");
			print("</div>");
		}
		print("<div class='col s12 m6 l3'>");
			print("<img src='/images/elements/contact-icon3.png' alt='Write to Us' />");
			print("<h3>Write to Us</h3>");
			print("<address class='crop-bottom'>");
				if(!empty($compName)) print($compName."<br/>");
				if(!empty($compAddNumber) && !empty($compAddStreet)) print(", ".(!empty($compAddNumber) ? $compAddNumber." " : "").(!empty($compAddStreet) ? ", ".$compAddStreet : "").",<br/>");
				if(!empty($compAddCity)) print($compAddCity.", ");
				if(!empty($compAddCounty)) print($compAddCounty.", "."<br/>");
				print("United Kingdom, <br/>");
				if(!empty($compAddPostcode)) print($compAddPostcode);
			print("</address>");
		print("</div>");
	/*	print("<div class='col s12 m6 l3'>");
			print("<img src='/images/elements/contact-icon3.png' alt='Write to Us' />");
			print("<h3>Write to Us</h3>");
			print("<address class='crop-bottom'>");
				print("SunflowerSelections.com <br/>");
				print("P.O. Box 266 <br/>");
				print("Woodland, CA 95776<br/>");
				print("USA");
			print("</address>");
		print("</div>");*/
	print("</div>");
	
?>