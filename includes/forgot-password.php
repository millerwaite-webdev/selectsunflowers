<?php
	
	$strdbsql = "SELECT *, UNIX_TIMESTAMP(NOW() - INTERVAL 30 MINUTE) AS time FROM customer WHERE username = :customer AND requestHash LIKE :hash AND requestStamp > UNIX_TIMESTAMP(NOW() - INTERVAL 30 MINUTE)";
	$result = query($conn,$strdbsql,"single",["customer"=>$_REQUEST['customer'],"hash"=>$_REQUEST['hash']]);
	
	// If password request in last 30 mins
	if($result) {
		
		print("<div class='heading'>");
			print("<h1>Reset Password</h1>");
		print("</div>");

		print("<div class='row'>");
			print("<div class='col s12 m12 l6'>");
				print("<div class='sidebar'>");
					print("<form id='reset-password' method='post' action=''>");
						print("<input type='hidden' id='frm_customer' name='frm_customer' value='".$result['recordID']."'>");
						print("<div class='input-field crop-top'>");
							print("<label for='frm_password'>Enter your new account password below. Once confirmed, you’ll be logged into your account and your new password will be active.</label>");
						print("</div>");
						print("<div class='input-field crop-top'>");
							print("<button class='btn btn-tertiary small postfix' type='submit'>Reset my Password</button>");
							print("<input type='password' class='required' id='frm_password' name='frm_password' data-label='password' tabindex='1' placeholder='New Password' value=''>");
						print("</div>");
					print("</form>");
				print("</div>");
			print("</div>");
		print("</div>");
		
	} else {
	
		print("<div class='heading'>");
			print("<h1>Forgot Password</h1>");
		print("</div>");

		print("<div class='row'>");
			print("<div class='col s12 m12 l6'>");
				print("<div class='sidebar'>");
					print("<form id='forgot-password' method='post' action=''>");
						print("<div class='input-field crop-top'>");
							print("<label for='frm_email'>Enter your email address below and we'll send you a link to reset your password.</label>");
						print("</div>");
						print("<div class='input-field crop-top'>");
							print("<button class='btn btn-tertiary small postfix' type='submit'>Reset my Password</button>");
							print("<input type='email' class='required' id='frm_email' name='frm_email' data-label='email address' tabindex='1' placeholder='Email Address' value=''>");
						print("</div>");
					print("</form>");
				print("</div>");
			print("</div>");
		print("</div>");
		
		print("<p>Oops, got to this page by accident? Did you mean <a href='#modal-login' class='modal-trigger'>Login</a> or <a href='/register'>Register</a>?</p>");
		
	}

?>