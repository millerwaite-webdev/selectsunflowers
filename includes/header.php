<?php

	$pageDataQuery = 'SELECT * FROM site_pages WHERE pageName = :page AND visible = 1';
	$arrType = 'single';
	$pageDataParams = array('page' => $strPage);
	$pageData = query($conn, $pageDataQuery, $arrType, $pageDataParams);
	
	$strCanonical = strip_tags($strPage);
	$strType = "website";

	if($strPage == "basket") {
		$strTitle = "Basket | ".$compName;
		$strDescription = "We offer fresh fruit and veg delivery to local wholesalers, businesses and communities across the Horwich area.";
		$strPageData .= "";
		$pageData['pageContent'] = "";
	} else if($strPage == "confirmation") {
		$strTitle = "Payment Complete | ".$compName;
		$strPageData .= "";
		$pageData['pageContent'] = "";
	} else if($strPage == "testemails") {
		$strTitle = "Test Emails | ".$compName;
		$strPageData .= "";
		$pageData['pageContent'] = "";
	} else if($strPage == "search") {
		$strTitle = "Search | ".$compName;
		$strPageData .= "";
		$pageData['pageContent'] = "";
	}	else if($strPage == "account") {
		$strTitle = "Account | ".$compName;
		$strPageData .= "";
		$pageData['pageContent'] = "";
	} else if($strPage == "register") {
		$strTitle = "Register | ".$compName;
		$strDescription = "Register for a Salad Bowl account and benefit from offers, promotions and exciting new updates.";
		$strPageData .= "";
		$pageData['pageContent'] = "";
	} else if($strPage == "forgot-password") {
		$strTitle = "Forgot Password | ".$compName;
		$strDescription = "We pride ourselves on building a trusty relationship with all of our customers. Forgot your password? We can sort that for you!";
		$strPageData .= "";
		$pageData['pageContent'] = "";
	} else if($strPage == "news" && $_REQUEST['article']) {
		$strdbsql = "SELECT * FROM site_news_events WHERE link = :article AND enabled = 1";
		$result = query($conn,$strdbsql,"single",["article"=>$_REQUEST['article']]);
		$strTitle = $result['title']." | Tips &amp; Advice | ".$compName;
		$strDescription = substr(strip_tags($result['content']), 0 ,200)."...";
		$strPageData .= "";
		$pageData['pageContent'] = "";
		$strCanonical = $strPage."/".strip_tags($_GET['article']);
		$strType = "article";
		$strdbsql = "SELECT imageRef FROM site_news_images WHERE newsID = :article ORDER BY imageOrder";
		$result2 = query($conn,$strdbsql,"single",["article"=>$result['recordID']]);
		$strImage = $strsiteurl."images/news/".$images['imageRef'];
	} else if(empty($pageData)) {
		header("HTTP/1.0 404 Not Found");
		$strPage = "404";
		$strTitle = "Page Not Found";
		$strPageData .= "This page has not been found";
		$pageData['pageContent'] = "<div class='starter'><h1>Page not found</h1><p>The link you have followed is broken or has expired. We would appreciate it if you could report this issue so that we can fix it. Thank you!</p></div><a class='btn' href='/'>Return to homepage</a><a style='margin:0 30px;' class='btn btn-text text-error' href='mailto:".$compEmail."?subject=Page not found'><i class='material-icons'>error_outline</i>Report issue</a>";
	} else {
		if(!empty($pageData['metaPageLink'])){
			$strTitle = $pageData['metaPageLink']." | ".$strTitle;
		}
		$strDescription = $pageData['metaDescription'];
		$strKeywords = $pageData['metaKeywords'];
		$strPageData = $pageData['pageContent'];
		
		if($strPage == "shop") {
			if(isset($_GET['product']) && $_GET['product'] != "") {
				$strdbsql = "SELECT recordID, name, summary, metaLink FROM stock_group_information 
				WHERE metaLink LIKE :product";
				$result = query($conn,$strdbsql,"single",array("product"=>$_GET['product']));
				if(isset($_REQUEST['category'])) $strCanonical = $strPage."/".$_REQUEST['category']."/".strip_tags($result['metaLink']);
				else $strCanonical = $strPage."/".$_REQUEST['category']."/".strip_tags($result['metaLink']);
				$strTitle = $result['name']." | ".$pageData['metaPageLink']." | ".$compName;
				$strDescription = $result['summary'];
				$strdbsql = "SELECT * FROM stock_images WHERE stockID = :recordID AND imageTypeID = 1 ORDER BY imageOrder LIMIT 1";
				$result2 = query($conn,$strdbsql,"single",array("recordID"=>$result['recordID']));
				$strImage = $strsiteurl."images/products/main/".$result2['imageLink'];
			} else {
			//	unset($strCanonical);
				if(isset($_REQUEST['category'])) $strCanonical = "shop/".$_REQUEST['category'];
				else $strCanonical = "shop";
			}
		}
	}

	print("<head>");
		// Import Fonts
		print("<link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>");
		print("<link href='https://use.fontawesome.com/releases/v5.0.8/css/all.css' rel='stylesheet' />");
		print("<link href='/css/material-design-iconic-font.min.css' rel='stylesheet' />");
	
		// Import Plugins
		print("<link rel='stylesheet' href='/css/materialize.min.css' />");
		print("<link rel='stylesheet' href='/css/slick.min.css' />");
		print("<link rel='stylesheet' href='/css/nice-select.min.css' />");
		print("<link rel='stylesheet' href='/css/jplist.preloader-control.min.css' />");
		
		// Import Custom Styles
		print("<link rel='stylesheet' href='/css/custom.css?v=12.03.19' />");

		// Browser/Device Confiration
		print("<meta charset='UTF-8'/>");
		print("<meta http-equiv='X-UA-Compatible' content='IE=edge'/>");
		print("<meta name='viewport' content='width=device-width, initial-scale=1.0'/>");
		print("<meta name='description' content='".\ForceUTF8\Encoding::toUTF8($strDescription)."'/>");
		print("<meta name='keywords' content='".\ForceUTF8\Encoding::toUTF8($strKeywords)."'/>");
		print("<meta name='theme-color' content='#000000'/>");
		print("<meta name='msapplication-navbutton-color' content='#000000'/>");
		print("<meta name='apple-mobile-web-app-status-bar-style' content='#000000'/>");
		
		// Facebook Sharing
		if(!empty($strTitle)) print("<meta property='og:title' content='".$strTitle."' />");
		if(!empty($strType))print("<meta property='og:type' content='".$strType."' />");
		if(!empty($strImage))print("<meta property='og:image' content='".$strImage."' />");
		if(!empty($strCanonical))print("<meta property='og:url' content='".$strsiteurl.$strCanonical."' />");
		if(!empty($strDescription))print("<meta property='og:description' content='".$strDescription."' />");
		
		// Page Information
		print("<title>".\ForceUTF8\Encoding::toUTF8($strTitle)."</title>");

		// SEO/Crawl
		if($strPage == "index") print("<link rel='canonical' href='".$strsiteurl."' />");
		else if(isset($strCanonical) && $strCanonical !== null) {
			print("<link rel='canonical' href='".$strsiteurl.$strCanonical."' />");
		}
	
		// Fav Icons
		print("<link rel='shortcut icon' href='/favicon.ico'>");
		
		// Google Imports
		print("<script src='https://www.google.com/recaptcha/api.js'></script>");
		
		?>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126446647-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-132742384-1');
		</script>

		<!-- Hotjar Tracking Code for https://selectsunflowers.com -->
		<script>
    		(function(h,o,t,j,a,r){
       		 	h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        		h._hjSettings={hjid:1164083,hjsv:6};
        		a=o.getElementsByTagName('head')[0];
        		r=o.createElement('script');r.async=1;
       			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        		a.appendChild(r);
    		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
		</script>
		
		<?php
		
	print("</head>");
	
	// Share This
	print("<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5bf2b077d329fa00111f746b&product=inline-share-buttons' async='async'></script>");
	
?>