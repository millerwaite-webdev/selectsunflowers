<?php
	// Define database details
	define("DRIVER", "mysql");
	define("HOST", "localhost");
	define("USER", "db_ss_user");
	define("PASS", '3ve#8Js4');
	if($debug == false) define("DB", "db_ss");
	else define("DB", "db_ss");
	
	$conn = connect();
	
	$strdbsql = "SELECT * FROM site_company_details";
	$companyDetails = query($conn, $strdbsql, "single");
	
	$strdbsql = "SELECT * FROM site_pages 
	WHERE displayInNav = 1 AND visible = 1 AND parentPageID IS NULL ORDER BY pageOrder";
	$menuLinks = query($conn, $strdbsql, "multi");
	
	$strdbsql = "SELECT * FROM site_pages 
	WHERE displayInFoot = 1 AND visible = 1 ORDER BY pageOrder";
	$footerLinks = query($conn, $strdbsql, "multi");
	
	$strdbsql = "SELECT site_social_media.*, site_social_sites.* FROM site_social_media 
	INNER JOIN site_social_sites ON site_social_media.socialName = site_social_sites.siteSite";
	$socialLinks = query($conn, $strdbsql, "multi");
	
	$strdbsql = "SELECT * FROM site_pages 
	WHERE pageName = :page";
	$pageData = query($conn, $strdbsql, "single", ["page"=>$strPage]);
	
	$strdbsql = "SELECT COUNT(*) AS total FROM site_block_relations 
	INNER JOIN site_pages ON site_block_relations.pageID = site_pages.recordID 
	WHERE site_block_relations.positionID = 2 AND site_pages.pageName = :page";
	$sideData = query($conn, $strdbsql, "single", ["page"=>$strPage]);
	
	//Site details
	$strsitehref = "https://www.".$_SERVER['SERVER_NAME'];
	$strsiteurl = "https://".$_SERVER['SERVER_NAME']."/";
	$strdomain = $_SERVER['SERVER_NAME'];
	
	// Create company variables (From Frontend)
	$compName = \ForceUTF8\Encoding::toUTF8($companyDetails['companyName']);
	$compEmail = $companyDetails['companyEmail'];
	$compEmail2 = $companyDetails['companyEmail2'];
	$officeEmail = "no-reply@".$strdomain;
	$compNumber = $companyDetails['companyNumber'];
	
	$compLogo = $companyDetails['companyLogo'];
	
	$compAddNumber = $companyDetails['addressNumber'];
	$compAddStreet = $companyDetails['addressStreet'];
	$compAddCity = $companyDetails['addressCity'];
	$compAddCounty = $companyDetails['addressCounty'];
	$compAddCountry = $companyDetails['addressCountry'];
	$compAddPostcode = $companyDetails['addressPostCode'];
	
	$compPhone = $companyDetails['companyPhone'];
	$compFax = $companyDetails['companyFax'];
	$compMobile = $companyDetails['companyMob'];
	
	
	
	// Create company variables (From Admin)
	$strcompanyname = \ForceUTF8\Encoding::toUTF8($companyDetails['companyName']);
	$companyName = \ForceUTF8\Encoding::toUTF8($companyDetails['companyName']);
	$companyEmail = $companyDetails['companyEmail'];
	$companyEmail2 = $companyDetails['companyEmail2'];
	$companyEmail = "no-reply@".$strdomain;
	$companyRegNumber = $companyDetails['companyNumber'];
	
	$strlogo = $companyDetails['companyLogo'];
	$strcompanylogo = "<a href='/' ><img src='".$companyDetails['companyLogo']."' alt='' style='border:0px;'/>";
	
	$compAddNumber = $companyDetails['addressNumber'];
	$compAddStreet = $companyDetails['addressStreet'];
	$compAddCity = $companyDetails['addressCity'];
	$compAddCounty = $companyDetails['addressCounty'];
	$compAddCountry = $companyDetails['addressCountry'];
	$compAddPostcode = $companyDetails['addressPostCode'];
	
	
	$companyPhone = $companyDetails['companyPhone'];
	$companyFax = $companyDetails['companyFax'];
	$companyMobile = $companyDetails['companyMob'];
	$companyWebsite = $strdomain;
	

	$companyAddress = trim($compAddNumber." ".$compAddStreet.", ".$compAddCity.", ".$compAddCounty.", ".$compAddPostcode);

	$companyVATNumber = "";
	
	//If someone has some time on there hands they could tidy those variables up so there was one uniform set thoughout the site.
?>