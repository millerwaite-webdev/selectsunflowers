<?php
	
	if(isset($_REQUEST['product'])) {
		
		$product = $_REQUEST['product'];

		$strdbsql = "SELECT stock.*, stock_group_information.name AS stockName, stock_group_information.metaLink AS url, stock_group_information.summary, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost, stock_units.description AS unit FROM stock 
		INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
		INNER JOIN stock_units ON stock.unit = stock_units.recordID 
		WHERE stock_group_information.metaLink LIKE :product AND stock_group_information.enabled = 1 AND `stock`.`status` = 1";
		$result = query($conn,$strdbsql,"multi",array("product"=>$product));
		
		if($result) {
			
			$groupID = $result[0]['groupID'];
			$type = $result[0]['productType'];
			$title = $result[0]['stockName'];
			$summary = $result[0]['summary'];
			$description = $result[0]['description'];
			$url = $strsiteurl."shop/".$result[0]['url'];
			$sku = $result[0]['stockCode'];
			$options  =[];
			foreach($result AS $r) {
				$options[] = $r;
			}
			if($type > 0) $price = $result['productCost'];
			else $price = $result['price'];
			$unit = $result['unit'];
		
			print("<div class='row crop-bottom' itemscope itemtype='https://schema.org/Product'>");
			
				// Image gallery
				$strdbsql = "SELECT * FROM stock_images WHERE stockID = :stockID AND imageTypeID = 1 ORDER BY imageOrder";
				$result = query($conn,$strdbsql,"multi",array("stockID"=>$groupID));
				
				if(count($result) > 0) {
					print("<div class='col s12 m12 l5'>");
						print("<div class='item-image'>");
						
							// Stickers
							$strdbsql = "SELECT * FROM stock_category_relations 
							INNER JOIN category ON stock_category_relations.categoryID = category.recordID 
							INNER JOIN category_relations ON stock_category_relations.categoryID = category_relations.childID 
							WHERE stock_category_relations.stockID = :product AND contentMenuImage IS NOT NULL AND contentMenuImage <> '' AND showSticker = 1
							ORDER BY category_relations.menuOrder
							LIMIT 1";
							$result3 = query($conn,$strdbsql,"multi",array("product"=>$groupID));
							
							foreach($result3 AS $row3) {
								if($row3['contentMenuImage']) print("<div class='sticker'><img src='/images/elements/".$row3['contentMenuImage']."' alt='".$row3['contentName']."'></div>");
							}
							
							if($type == 3) print("<div class='sticker'><img src='/images/elements/recipe.png' alt='Recipe'></div>");
							
							if(count($result) > 1) {
								print("<div class='slick-slider'>");									
									foreach($result AS $row) {
										print("<div>");
											print("<img src='/images/products/main/".$row['imageLink']."' alt='".\ForceUTF8\Encoding::toUTF8((!empty($row['description']) ? $row['description'] : $title))."' />");
										print("</div>");
									}
								print("</div>");
								print("<div class='slick-nav'>");
									foreach($result AS $row) {
										print("<div>");
											print("<img src='/images/products/thumbnails/".$row['imageLink']."' alt='".\ForceUTF8\Encoding::toUTF8((!empty($row['description']) ? $row['description'] : $title))."' />");
										print("</div>");
									}
								print("</div>");
							} else if(count($result) == 1) {
								print("<img class='product-image materialboxed' src='/images/products/main/".$result[0]['imageLink']."' data-caption='".\ForceUTF8\Encoding::toUTF8((!empty($result[0]['description']) ? $result[0]['description'] : $title))."' alt='".\ForceUTF8\Encoding::toUTF8((!empty($result[0]['description']) ? $result[0]['description'] : $title))."' itemprop='image' />");
							}
						print("</div>");
					print("</div>");
				}

				print("<div class='col s12 m12 l7'>");
					print("<div class='item'>");
						print("<div class='item-details'>");
				
							// Title
							print("<h1 itemprop='name'>".\ForceUTF8\Encoding::toUTF8($title)."</h1>");
							print("<meta itemprop='sku' content='".$sku."' />");
					
							// Reviews
							$strdbsql = "SELECT * FROM stock_reviews WHERE stockGroupID = :recordID AND shownOnSite = 1 ORDER BY dateReview";
							$reviews = query($conn,$strdbsql,"multi",array("recordID"=>$groupID));
							
						//	if(count($reviews) > 0) {
								foreach($reviews AS $review) {
									$total += $review['numStars'];
								}
							
								if(count($reviews) == 0) $average = 0;
								else $average = $total / count($reviews);
							
								print("<ul class='reviews' itemprop='aggregateRating' itemscope itemtype='https://schema.org/AggregateRating'>");
									for($stars = 1; $stars <= $average; $stars++) {
										print("<li class='filled'><i class='material-icons'>star</i></li>");
									}
									while($stars <= 5) {
										print("<li><i class='material-icons'>star_border</i></li>");
										$stars++;
									}
									print("<li><span itemprop='ratingValue'>".number_format($average, 1)."</span>(<span itemprop='reviewCount'>".count($reviews)."</span>)</li>");
								print("</ul>");
						//	}
							
							// Stickers
							$strdbsql = "SELECT * FROM stock_category_relations 
							INNER JOIN category ON stock_category_relations.categoryID = category.recordID 
							INNER JOIN category_relations ON stock_category_relations.categoryID = category_relations.childID 
							WHERE stock_category_relations.stockID = :product AND contentMenuImage IS NOT NULL AND contentMenuImage <> ''
							ORDER BY category_relations.menuOrder";
							$result = query($conn,$strdbsql,"multi",array("product"=>$groupID));
							
							// Favourite
							$strdbsql = "SELECT * FROM stock_favourites WHERE stockCode = :product AND customerID = :customer";
							$result2 = query($conn,$strdbsql,"single",array("product"=>$groupID,"customer"=>$accountid));
							
							if(count($result) > 0 || $result2) {
								print("<ul class='key'>");
									$i = 1;
									foreach($result AS $row) {
										if(count($result) > 1 && $i == 1) {
											// Skip first
										} else {
											if(!empty($row['contentMenuImage'])) print("<li><img class='circle' title='".$row['contentName']."' src='/images/elements/".$row['contentMenuImage']."' alt='".$row['contentName']."' /></li>");
										}
										$i++;
									}
									if($result2) print("<li><button class='favourite-toggle active' data-id='".$groupID."' aria-label=\"Remove ".$row['contentName']." From Favorites\" ><i class='fas fa-heart'></i></button></li>");
									else print("<li><button class='favourite-toggle' data-id='".$groupID."' aria-label=\"Add ".$row['contentName']." To Favorites\"><i class='fas fa-heart'></i></button></li>");
								print("</ul>");
							}

							
						print("</div>");
						
						
						print("<div class='item-options' itemprop='offers' itemtype='https://schema.org/Offer' itemscope>");
					
							print("<meta itemprop='availability' content='https://schema.org/InStock' />");
							print("<meta itemprop='priceCurrency' content='GBP' />");
							print("<meta itemprop='itemCondition' content='https://schema.org/UsedCondition' />");
							print("<meta itemprop='price' content='".$options[0]['price']."' />");
							print("<div itemprop='seller' itemtype='http://schema.org/Organization' itemscope>");
								print("<meta itemprop='name' content='".$compName."' />");
							print("</div>");
							print("<meta itemprop='url' content='".$url."'>");
					
							// Basket Options
							print("<form id='basket-add-".$o['recordID']."' action='#' method='post'>");
								print("<div class='row crop-bottom'>");
									print("<div class='col s4'>");
									print("<select id='frm_product' name='frm_product' class='product-select nice-select' aria-label='Product Option'>");
										foreach($options As $o) {
											print("<option value='".$o['recordID']."' data-price='".$o['price']."'>".ucwords($o['unit'])." - &pound;".$o['price']."</option>");
										}
									print("</select>");
									print("</div>");
									print("<div class='col s8 right-align'>");
										print("<div class='input-field'>");
											print("<button class='btn btn-counter count-toggle negative' data-count='-1' disabled='disabled'><i class='material-icons'>remove</i></button>");
											print("<input type='text' id='frm_qty' class='counter' name='frm_qty' value='1' readonly aria-label='Product Quantity'/>");
											print("<button class='btn btn-counter count-toggle positive' data-count='1'><i class='material-icons'>add</i></button>");
											print("<input type='text' id='frm_total' class='total' name='frm_total' value='&pound;".$options[0]['price']."' aria-label='Price' data-price='".$options['price']."' readonly />");
											print("<button class='btn btn-primary basket-add'>Add to Basket</button>");
										print("</div>");
									print("</div>");
								print("</div>");
							print("</form>");
							
						print("</div>");
						
							print("<ul class='tabs small crop-bottom'>");
								print("<li class='tab'><a class='active' href='#details'>Details</a></li>");
							print("</ul>");
							
							print("<div id='details' class='item-info'>");
							
								// Summary
								if(!empty($summary))print("<p itemprop='description'>".\ForceUTF8\Encoding::toUTF8($summary)."</p>");
								
								// Description
								if(!empty($description)) print("<p>".\ForceUTF8\Encoding::toUTF8($description)."</p>");
							
								$strdbsql = "SELECT stock_custom_fields.description AS label, stock_custom_field_values.value AS value FROM stock_custom_field_values 
								INNER JOIN stock_custom_fields ON stock_custom_field_values.customFieldID = stock_custom_fields.recordID 
								WHERE stock_custom_field_values.stockID = :product 
								ORDER BY stock_custom_fields.recordID";
								$result = query($conn,$strdbsql,"multi",array("product"=>$groupID));
									
								if(count($result) > 0) {
									print("<table class='keywords crop-bottom'>");
										print("<tbody>");
											foreach($result AS $row) {
												print("<tr>");
													print("<td valign='top' width='180'>".$row['label']."</td>");
													print("<td valign='top'>".$row['value']."</td>");
												print("</tr>");
											}
										print("</tbody>");
									print("</table>");
								}
							
							print("</div>");
						print("</div>");
						
					//	print("<div class='item-info'>");
							
						/*	if($type == 3) {
								print("<ul class='key large'>");
									print("<li>");
										print("<img title='Time' src='/images/elements/time.png' alt='Time' />");
										print("<span>5 mins</span>");
									print("</li>");
									print("<li>");
										print("<img title='Serves' src='/images/elements/recipe.png' alt='Serves' />");
										print("<span>3 people</span>");
									print("</li>");
								print("</ul>");
							}*/
							
					//	print("</div>");
						
						// Share This
						print("<div class='sharethis-inline-share-buttons crop-bottom'></div>");
						
						// Restrict
						print("<div id='modal-restrict' class='modal small'>");
							print("<div class='modal-content'>");
								print("<h4 class='h3'>Delivery Notice</h4>");
								print("<p>Orders over 1000 seeds may take up to three weeks for delivery, depending on stock at the time of ordering.</p>");
								print("<p class='crop-bottom'>If you require further assistance, <a href='/contact-us'>get in touch</a>.</p>");
							print("</div>");
							print("<div class='modal-footer'>");
								print("<span><a href='#' class='modal-close'>Okay, I understand!</a></span>");
							print("</div>");
						print("</div>");
						
						// Limit
					/*	print("<div id='modal-limit' class='modal small'>");
							print("<div class='modal-content'>");
								print("<h4 class='h3'>Delivery Notice</h4>");
								print("<p>For orders that contain 25,000+ seeds, please <a href='/contact-us'>get in touch</a>.</p>");
							print("</div>");
							print("<div class='modal-footer'>");
								print("<span><a href='#' class='modal-close'>Okay, I understand!</a></span>");
							print("</div>");
						print("</div>");*/
						
						// Ingredients
					/*	if($type == 3) {
							
							$strdbsql = "SELECT stock_group_information.* FROM stock_hamper 
							INNER JOIN stock_group_information ON stock_hamper.stockItemID = stock_group_information.recordID 
							WHERE stock_hamper.stockGroupID = :recordID ORDER BY recordID";
							$result = query($conn,$strdbsql,"multi",array("recordID"=>$groupID));
								
							if(count($result) > 0) {
								print("<div class='ingredients'>");
									print("<div class='heading small'>");
										print("<h3 class='h4'>Ingredients</h3>");
									print("</div>");
									if($accountid > 0 && $accounttype == "wholesale") print("<p>You can customise this box to suit you. Simply <strong>add</strong> and <strong>remove</strong> ingredients to create the box you want. Additional charges may apply.</p>");
									else print("<p>Have a wholesaler account? <a href='#modal-login' class='modal-trigger'>Login now</a> to customise this box to suit you.</p>");
									print("<ul>");
										$i = 1;
										foreach($result AS $row) {
											print("<li>");
												print("<div class='input-field'>");
													print("<label for='ingredient-".$i."'>");
														print("<input type='checkbox' class='filled-in' id='ingredient-".$i."' name='ingredient-".$i."' checked ".($accountid == 0 || $accounttype != "wholesale" ? "disabled" : "")." />");
														print("<span>".$row['name']."</span>");
													print("</label>");
												print("</div>");
											print("</li>");
											$i++;
										}
									print("</ul>");
								print("</div>");
							}
						} else if($type > 0) {
							print("<div class='ingredients'>");
								print("<div class='heading small'>");
									print("<h3 class='h4'>Ingredients</h3>");
								print("</div>");
								print("<p>Our boxes are put together to include a few staple ingredients, some seasonal favourites and the occasional surprise. We will often include instructions on how to cook unfamiliar items and recipe ideas and occasionally include free gifts.</p>");
								print("<p><strong>If you order two boxes, of any kind, we offer a £1 discount to the final price.</strong></p>");
							print("</div>");
						}*/
						
						// Comments
						$strdbsql = "SELECT * FROM stock_reviews WHERE stockGroupID = :recordID AND shownOnSite = 1 ORDER BY dateReview LIMIT 5";
						$result = query($conn,$strdbsql,"multi",array("recordID"=>$groupID));
						
						if(count($result) > 0) {
							print("<div class='comments'>");
								print("<div class='heading small'>");
									print("<h3>Comments (".count($result).")</h3>");
								print("</div>");
								foreach($result AS $row) {
									print("<div class='comment' itemprop='review' itemtype='http://schema.org/Review' itemscope>");
									
										$initials = explode(" ", $row['reviewerName']);
										$initials = substr($initials[0], 0, 1).substr($initials[1], 0, 1);
										
										print("<div class='hide' itemprop='author' itemtype='http://schema.org/Person' itemscope>");
											print("<meta itemprop='name' content='".$reviewerName."' />");
										print("</div>");
										
										print("<img class='profile circle' data-name='".$initials."' alt='".$reviewerName."' title='".$reviewerName."' />");
										
										print("<div class='message'>");
										
											if(!empty($row['title'])) print("<h4>".$row['title']."</h4>");
										
											print("<span class='date'>".date("F, j Y", $row['dateReview'])." at ".date("g:i a", $row['dateReview'])."</span>");
											
											if(!empty($row['description'])) print("<p>".$row['description']."</p>");
											
											$strdbsql = "SELECT stock.*, stock_group_information.name AS stockName, stock_group_information.summary, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost, stock_units.description AS unit FROM stock 
											INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
											INNER JOIN stock_units ON stock.unit = stock_units.recordID 
											WHERE stock.recordID = :product";
											$result = query($conn,$strdbsql,"single",["product"=>$row['stockGroupID']]);
											
											if($result) print("<a href='/shop/".str_replace(" ", "-", strtolower($result['stockCode']))."'><i class='fas fa-tag'></i><span>".$result['stockName']."</span></a>");
											
											$strdbsql = "SELECT * FROM stock_reviews WHERE recordID = :id AND shownOnSite = 1";
											$result = query($conn,$strdbsql,"single",array("id"=>$row['recordID']));
											
											if($result) {
												print("<div itemprop='reviewRating' itemtype='http://schema.org/Rating' itemscope>");
													print("<meta itemprop='ratingValue' content='".$result['numStars']."' />");
													print("<meta itemprop='bestRating' content='5' />");
												print("</div>");
												print("<ul class='reviews'>");
													for($stars = 1; $stars <= $result['numStars']; $stars++) {
														print("<li class='filled'><i class='material-icons'>star</i></li>");
													}
													while($stars <= 5) {
														print("<li><i class='material-icons'>star_border</i></li>");
														$stars++;
													}
													print("<li><span>(".$result['numStars'].")</span></li>");
												print("</ul>");
											}
											
											if(!empty($row['response'])) print("<button class='btn btn-blank small' data-id='response-".$row['recordID']."'><i class='material-icons right'>arrow_drop_down</i>View replies (1)</button>");
											
										print("</div>");
									print("</div>");
									
									if(!empty($row['response'])) {
										print("<div class='comment response hide' id='response-".$row['recordID']."'>");
											print("<img class='profile circle' data-name='SB' alt='' />");
											print("<div class='message'>");
												print("<h4>Response from Salad Bowl</h4>");
												print("<span class='date'>".date("F, j Y", $row['dateResponse'])." at ".date("g:i a", $row['dateResponse'])."</span>");
												print("<p>".$row['response']."</p>");
											print("</div>");
										print("</div>");
									}
									
								}
							print("</div>");
						}
						
					/*	print("<div class='comments blank'>");
							print("<h4>Write a Review</h4>");
							print("<p>Your email address will not be published. Required fields are marked with a *.</p>");
							print("<form id='review' method='POST' action=''>");
								print("<input type='hidden' id='frm_stock' name='frm_stock' value='".$groupID."' />");
								print("<div class='row crop-bottom'>");
									print("<div class='col s12 m12 l5'>");
										print("<div class='input-field'>");
											print("<input type='text' class='required' id='frm_name' name='frm_name' value='' placeholder='Name' aria-label='Name'/>");
										print("</div>");
									print("</div>");
									print("<div class='col s12 m12 l7'>");
										print("<div class='input-field'>");
											print("<input type='email' class='required' id='frm_email' name='frm_email' value='' placeholder='Email' aria-label='Email Address' />");
										print("</div>");
									print("</div>");
									print("<div class='col s12'>");
										print("<div class='input-field'>");
											print("<input type='text' class='required' id='frm_title' name='frm_title' value='' placeholder='Enter your summary here...' aria-label='Review Title'/>");
										print("</div>");
									print("</div>");
									print("<div class='col s12'>");
										print("<div class='input-field'>");
											print("<textarea class='materialize-textarea required' id='frm_description' name='frm_description' data-length='500' maxlength='500' aria-label='Review Text' placeholder='Enter your comments here...'></textarea>");
										print("</div>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='col s12'>");
										print("<div class='input-field'>");
											print("<p id='reviewScore'>Please provide a start rating:</p>");
											print("<select id='example-css' class='browser-default' name='frm_rating' autocomplete='off' aria-labelledby='reviewScore'>");
												print("<option value='1'>1</option>");
												print("<option value='2'>2</option>");
												print("<option value='3'>3</option>");
												print("<option value='4'>4</option>");
												print("<option value='5' selected>5</option>");
											print("</select>");
										print("</div>");
									print("</div>");
								print("</div>");
								print("<div class='row crop-bottom'>");
									print("<div class='col s12'>");
										print("<div class='g-recaptcha' data-sitekey='".$captchaKey."'></div>");
									print("</div>");
									print("<div class='col s12'>");
										print("<div class='input-field'>");
											print("<button type='submit' class='btn btn-tertiary'>Publish</button>");
										print("</div>");
									print("</div>");
								print("</div>");
							print("</form>");
						print("</div>");*/
						
					print("</div>");
				
				print("</div>");
			print("</div>");
			
		} else {
			
			print("<div class='row crop-bottom'>");
				print("<div class='col s12 m12 l7'>");
					print("<div class='item'>");
						print("<div class='item-details'>");
							print("<div class='row'>");
								print("<div class='col'>");
									print("<h1>Product not found</h1>");
									print("<p class='crop-bottom'>The link you have followed is broken or has expired. Use the links below to return to the homepage or report this issue. Thank you!</p>");
								print("</div>");
							print("</div>");
							print("<div class='row crop-bottom'>");
								print("<div class='col'>");
									print("<a href='/' class='btn btn-contact'>Return to homepage</a>");
								print("</div>");
								print("<div class='col'>");
									print("<a href='#' class='btn btn-tertiary'>Report Issue</a>");
								print("</div>");
							print("</div>");
						print("</div>");
					print("</div>");
				print("</div>");
			print("</div>");
			
		}

	} else {
		
		print("<div id='shop'>");
		
			print("<div class='row crop-bottom'>");
				print("<div class='col s12 m12 l3'>");
					print("<div class='sidebar'>");
					
						$strdbsql = "SELECT category.contentName, category.recordID AS categoryID FROM category 
						INNER JOIN category_relations ON category.recordID = category_relations.childID 
						ORDER BY category_relations.menuOrder";
						$result = query($conn,$strdbsql,"multi",null);
							
						if(count($result) > 0) {
					
							print("<div class='filter product-options'>");
								print("<ul class='collapsible expandable jplist-group' data-control-type='checkbox-group-filter' data-control-action='filter' data-control-name='categories' data-control-storage='false'>");
									
									foreach($result AS $row) {
									
										$strdbsql = "SELECT category.* FROM category 
										INNER JOIN category_relations ON category.recordID = category_relations.childID 
										WHERE category_relations.parentID = :parent AND category.recordID <> category_relations.parentID AND category.showCategory = 1 
										ORDER BY menuOrder";
										$result2 = query($conn,$strdbsql,"multi",["parent"=>$row['categoryID']]);
									
										if(count($result2) > 0) {
											print("<li class='active'>");
												print("<div class='collapsible-header active'><i class='material-icons right'>arrow_drop_down</i>".$row['contentName']."</div>");
												print("<div class='collapsible-body'>");
													$i = 0;
													foreach($result2 AS $row2) {
														
														$typeID = 0;
														
														// Get Assigned Products
														$strdbsql = "SELECT COUNT(*) AS total FROM stock_category_relations 
														INNER JOIN stock ON stock_category_relations.stockID = stock.recordID 
														INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
														WHERE stock_category_relations.categoryID = :catID AND stock_group_information.productType = :typeID AND stock_group_information.enabled = 1";
													//	if($_REQUEST['search']) $strdbsql .= " AND stock_group_information.name LIKE :search";
														$params = ["catID"=>$row2['recordID'],"typeID"=>$typeID];
													//	if($_REQUEST['search']) $params['search'] = "%".$_REQUEST['search']."%";
														$result3 = query($conn,$strdbsql,"single",$params);
														
														if($result3['total'] > 0) {
															if($row2['showPage'] > 0 && $row2['showSticker'] > 0 && !empty($row2['contentMenuImage'])) {
																print("<div class='option'>");
																	print("<a href='".$strsiteurl.$strPage."/".$row2['metaPageLink']."'>");
																		if(!empty($row2['contentMenuImage'])) print("<img class='sticker' title='".$row2['contentName']."' src='/images/elements/".$row2['contentMenuImage']."' alt='".$row2['contentName']."'>");
																		print($row2['contentName']);
																	print("</a>");
																	print("<input type='checkbox' class='filled-in check-filter' id='".strtolower(str_replace(" ", "-", $row2['contentName']))."' data-path='.".$row2['recordID']."' data-control-storage='false' value='".$row2['recordID']."' ".($_REQUEST['category'] == $row2['metaPageLink'] ? "checked" : "")." />");
																print("</div>");
															} else {
																print("<div class='input-field'>");
																	print("<label for='".strtolower(str_replace(" ", "-", $row2['contentName']))."'>");
																		print("<input type='checkbox' class='filled-in check-filter' id='".strtolower(str_replace(" ", "-", $row2['contentName']))."' data-path='".$row2['recordID']."' value='".$row2['metaPageLink']."' ".($_REQUEST['category'] == $row2['metaPageLink'] ? "checked" : "")." />");
																		print("<span>");
																			if(!empty($row2['contentMenuImage'])) print("<img class='sticker circle' title='".$row2['contentName']."' src='/images/elements/".$row2['contentMenuImage']."' alt='".$row2['contentName']."'>");
																			print($row2['contentName']." (".$result3['total'].")");
																		print("</span>");
																	print("</label>");
																print("</div>");
															}
														}
														$i++;
													}
												print("</div>");
											print("</li>");
										}
									
									}

								print("</ul>");
							print("</div>");
							
						}
							
						print("<div class='favourite hide-on-med-and-down'>");
							print("<h3 class='h2'>Favourites</h3>");
							if($accountid > 0) print("<a href='".$strsiteurl."account/favourites' class='btn btn-base'>View Favourites</a>");
							else print("<a href='#modal-login' class='btn btn-base modal-trigger'>View Favourites</a>");
						print("</div>");
						
					print("</div>");
				print("</div>");
				print("<div class='col s12 m12 l9'>");
					
					// Check for Category
					$strdbsql = "SELECT * FROM category WHERE metaPageLink = :category AND showPage = 1";
					$categoryInfo = query($conn,$strdbsql,"single",array("category"=>$_REQUEST['category']));
					
					if($categoryInfo && !empty($categoryInfo['contentName']) && !empty($categoryInfo['contentBanner'])) {
						
						print("<div class='slider banner-slider'>");
							print("<ul class='slides'>");
								print("<li>");
									print("<img src='/images/gallery/".$categoryInfo['contentBanner']."' alt='".\ForceUTF8\Encoding::toUTF8($categoryInfo['contentName'])."' />");
									print("<div class='caption wide'>");
										print("<div class='wrapper'>");
											print("<h1>".\ForceUTF8\Encoding::toUTF8($categoryInfo['contentName'])."</h1>");
											print("<h2 class='crop-bottom'>".\ForceUTF8\Encoding::toUTF8($categoryInfo['contentText'])."</h2>");
										print("</div>");
										print("<div class='breakpoint'></div>");
									print("</div>");
								print("</li>");
							print("</ul>");
						print("</div>");
						
					}
					
					//	TOP SORTING PANEL
					print("<div class='product-bar product-options'>");
						
					/*	print("<div class='jplist-sort nice-select' data-control-type='boot-sort-drop-down' data-control-name='sort' data-control-action='sort' data-datetime-format='{year}-{month}-{day}'>");
							print("<span class='current'>Any</span>");
							print("<ul id='sort-dropdown-menu' class='list'>");
								print("<li class='option' data-path='.any' data-order='asc' data-type='number' data-default='true'>Any</li>");
								print("<li class='option' data-path='.price-low' data-order='asc' data-type='number'>Price: Low-High</li>");
								print("<li class='option' data-path='.price-high' data-order='desc' data-type='number'>Price: High-Low</a></li>");
							print("</ul>");
						print("</div>");*/
						
						print("<div class='jplist-pagination-info hide-on-small-only' data-type='<span>Showing {start}-{end} of {all} results</span>' data-control-type='pagination-info' data-control-name='paging' data-control-action='paging'  data-control-animate-to-top='true' data-range='5' data-control-deep-link='false'></div>");
						print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-control-animate-to-top='true' data-range='5'  data-control-animate-to-top='true' data-control-deep-link='false'></ul>");
						print("<div class='text-filter-box hide'>");
							print("<input data-path='.title' type='hidden' value='".$_REQUEST['search']."' class='jplist-no-right-border' data-control-type='textbox' data-control-name='title-filter' data-control-action='filter' data-control-storage='false' />");
						print("</div>");
					print("</div>");
					
					//	SEARCH RESULTS
					print("<div class='products'></div>");
					
					//	NO SEARCH RESULTS
					print("<div class='jplist-no-results'>No sunflowers matching your search criteria!</div>");

					//	BOTTOM PAGINATION PANEL
					print("<div class='product-bar product-options crop-bottom'>");
						print("<div class='dropdown jplist-items-per-page hide' data-control-type='boot-items-per-page-dropdown' data-control-name='paging' data-control-action='paging' data-control-deep-link='false'>");
							print("<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown' id='dropdown-menu-1' aria-expanded='true'>");
								print("<span data-type='selected-text'>Items per Page</span>");
								print("<span class='caret'></span>");
							print("</button>");
							print("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdown-menu-1'>");
								print("<li role='presentation'><a role='menuitem' tabindex='-1' href='#' data-number='20' data-default='true'>20 per page</a></li>");
							print("</ul>");
						print("</div>");
						print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-control-animate-to-top='true' data-range='5'  data-control-deep-link='false'></ul>");
					print("</div>");
				print("</div>");
			print("</div>");
			
		print("</div>");

	}

?>