<?php

	print("<div class='adverts'>");
		print("<div class='row'>");
			print("<div class='col m12 l4'>");
				print("<div class='advert'>");
					print("<img src='http://demo.cmssuperheroes.com/themeforest/raworganic/wp-content/uploads/2018/07/faq-icon1.png' alt='General Enquiries' />");
					print("<div class='advert-wrapper'>");
						print("<h4>Need Help?</h4>");
						print("<p>We're here to help! <a href='".$strsiteurl."contact-us'>Contact us</a> on ".$compMobile." or send us an email: <a href='mailto:".$compEmail."'>".$compEmail."</a>.</p>");
					print("</div>");
				print("</div>");
			print("</div>");
			print("<div class='col m12 l4'>");
				print("<div class='advert'>");
					print("<img src='http://demo.cmssuperheroes.com/themeforest/raworganic/wp-content/uploads/2018/07/faq-icon2.png' alt='Complaints' />");
					print("<div class='advert-wrapper'>");
						print("<h4>Complaints</h4>");
						print("<p>Please email or call us before returning items, call us on ".$compMob." or email <a href='mailto:".$compEmail."'>".$compEmail."</a>.</p>");
					print("</div>");
				print("</div>");
			print("</div>");
			print("<div class='col m12 l4'>");
				print("<div class='advert'>");
					print("<img src='http://demo.cmssuperheroes.com/themeforest/raworganic/wp-content/uploads/2018/07/faq-icon3.png' alt='Order Enquiries' />");
					print("<div class='advert-wrapper'>");
						print("<h4>Order Enquiries</h4>");
						print("<p>Please call us for free from 9am 'till 6pm ".$compPhone." or ".$compEmail."</p>");
					print("</div>");
				print("</div>");
			print("</div>");
		print("</div>");
	print("</div>");

?>