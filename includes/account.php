<?php
		
	if($accountid > 0) {
		
		// Count Favourites
		$strdbsql = "SELECT COUNT(*) AS total FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_favourites ON stock.recordID = stock_favourites.stockCode WHERE stock_favourites.customerID = 1";
		$favourites = query($conn,$strdbsql,"single",["account"=>$accountid]);
		
		// Count Reviews
		$strdbsql = "SELECT COUNT(*) AS total FROM stock_reviews WHERE memberID = :account AND shownOnSite = 1";
		$reviews = query($conn,$strdbsql,"single",["account"=>$accountid]);
		
		switch($accounttype) {
			default:
				$levels = [
					["name"=>"Account Overview","link"=>"","icon"=>"<i class='fas fa-home main'></i>","type"=>1,"count"=>0,"classes"=>""],
					["name"=>"Personal Details","link"=>"personal-details","icon"=>"<i class='fas fa-user main'></i>","type"=>1,"count"=>0,"classes"=>""],
					["name"=>"Processed Orders","link"=>"processed-orders","icon"=>"<i class='fas fa-truck main'></i>","type"=>1,"count"=>2,"classes"=>""],
					["name"=>"Favourites","link"=>"favourites","icon"=>"<i class='fas fa-heart main'></i>","type"=>1,"count"=>$favourites['total'],"classes"=>"favourite-count"],
					["name"=>"My Reviews","link"=>"my-reviews","icon"=>"<i class='fas fa-star main'></i>","type"=>1,"count"=>$reviews['total'],"classes"=>""]
				];
				break;
		}

		print("<div class='heading'>");
			if(isset($_REQUEST['level'])) {
				foreach($levels AS $level) {
					if($_REQUEST['level'] == $level['link']) {
						print("<h1>".$level['name']."</h1>");
					}
				}
			} else {
				print("<h1>Welcome back, ".$customerfirstname."</h1>");
			}
		print("</div>");
		
		print("<div class='row crop-bottom'>");
			print("<div class='col s12 m12 l3'>");
				print("<div class='sidebar'>");
					
					print("<div class='row'>");
						print("<div class='col s12'>");
							print("<a href='/shop' class='btn btn-secondary wide large'>Shop Now</a>");
						print("</div>");
					print("</div>");
					
					if(count($levels) > 0) {
						print("<div class='collection links'>");
							foreach($levels AS $level) {
								print("<a href='".$strsiteurl.($level['type'] == 1 ? "account" : "").($level['link'] != "" ? "/".$level['link'] : "")."' class='collection-item".($_REQUEST['level'] == $level['link'] ? " active" : "").($level['type'] == 2 ? " highlight" : "")."'>");
									print("<i class='zmdi zmdi-arrow-right right'></i>");
								//	print($level['icon']);
									print($level['name']);
								print("</a>");
							}
						print("</div>");
					}
					
					print("<div class='logout'>");
						print("<a href='#' id='logout' class='btn btn-favourite wide'>Logout</a>");
					print("</div>");
					
					print("<div class='help'>");
						print("<h3 class='h2'>Need Help?</h3>");
						print("<a href='".$strsiteurl."contact-us' class='btn btn-base'>Get in Touch</a>");
					print("</div>");
					
				print("</div>");
			print("</div>");
			print("<div class='col s12 m12 l9'>");		
				
				switch($_REQUEST['level']) {
					default:
					
						print("<div class='row'>");
							print("<div class='col s12'>");
								print("<div class='card large main'>");
									print("<div class='card-content' style='background-image: url(/images/gallery/faqs.jpg);'>");
										print("<div class='wrapper center'>");
										//	print("<div class='profile-image'>");
										//		print("<img class='profile-small circle' data-name='".$customerfirstname[0].$customersurname[0]."' alt='".$customername."' />");
										//		print("<a href='/account/personal-details'><i class='material-icons'>settings</i></a>");
										//	print("</div>");
											print("<h3>".$customerfirstname." ".$customersurname."</h3>");
											print("<p>".$customeremail."</p>");
											print("<div class='input-field'>");
												print("<a class='btn btn-tertiary crop-top' href='/account/personal-details'>Account Details</a>");
											print("</div>");
										print("</div>");
									print("</div>");
								print("</div>");
							print("</div>");
						print("</div>");
					
						break;
					case "personal-details":
						
						print("<div class='row'>");
							print("<div class='col s12 m12 l8'>");
							
								print("<form id='account-form' class='account' method='post' action='/account'>");
									
									// Customer Details									
									print("<h3>Your Details</h3>");
							
									print("<div class='row'>");
									
										// Title
										$titles = array();
										
										$titles[0] = array("label"=>"Mr","value"=>"Mr");
										$titles[1] = array("label"=>"Miss","value"=>"Miss");
										$titles[2] = array("label"=>"Mrs","value"=>"Mrs");
										$titles[3] = array("label"=>"Ms","value"=>"Ms");
										$titles[4] = array("label"=>"Other","value"=>"");
										
										print("<div class='col s12 m12 l2'>");
											print("<div class='input-field'>");
												print("<select class='browser-default required' id='frm_title' name='frm_title' tabindex='1' aria-label='Title'>");
													print("<option value='' disabled".($accountid == 0 ? " selected" : "").">Title</option>");
													foreach($titles AS $title) {
														print("<option value='".$title['value']."'".($customertitle == $title['label'] ? " selected" : "").">".$title['label']."</option>");
													}
												print("</select>");
											print("</div>");
										print("</div>");
										
										// First Name
										print("<div class='col s12 m12 l5'>");
											print("<div class='input-field'>");
												print("<input type='text' aria-label='First Name' class='required' id='frm_firstname' name='frm_firstname' tabindex='2' placeholder='First Name' value='".$customerfirstname."' />");
											print("</div>");
										print("</div>");
										
										// Last Name
										print("<div class='col s12 m12 l5'>");
											print("<div class='input-field'>");
												print("<input type='text' aria-label='Last Name' class='required' id='frm_lastname' name='frm_lastname' tabindex='2' placeholder='Last Name' value='".$customersurname."' />");
											print("</div>");
										print("</div>");
										
										// Email Address
										print("<div class='col s12'>");
											print("<div class='input-field'>");
												print("<input type='email' class='required' aria-label='Email Address' id='frm_email' name='frm_email' tabindex='4' placeholder='Email Address' value='".$customeremail."' />");
											print("</div>");
										print("</div>");
										
									print("</div>");
									
									// Delivery Address
									print("<h3>Delivery Address</h3>");
									
									print("<div class='row crop-bottom'>");
										print("<div class='col s12 m10 l8'>");
											print("<div class='input-field'>");
												print("<select class='required browser-default' required aria-label='Country' id='frm_country' name='frm_country' data-label='country' tabindex='6' placeholder='Country' >");
												$q = "SELECT configCountries.countryName FROM configCountries INNER JOIN deliveryLocations ON configCountries.zone = deliveryLocations.zone WHERE deliveryLocations.allowDelivery = 1 ORDER BY deliveryLocations.recordID, configCountries.countryName";
												$r = query($conn,$q,"multi");
												foreach($r AS $c) {
													print("<option value='".$c['countryName']."' ".((isset($customercountry) && $customercountry == $c['countryName']) ? "selected" : "").">".$c['countryName']."</option>");
												}
												print("</select>");
											print("</div>");
										print("</div>");
										
										// Postcode
										print("<div class='col s12 m10 l8'>");
											print("<div class='input-field'>");
												print("<button class='btn btn-tertiary small postfix postcode-search'>Find Address</button>");
												print("<input type='text' class='required leader code' required aria-label='Postcode' id='frm_postcode' name='frm_postcode' data-label='postcode' maxlength='7' tabindex='6' placeholder='Postcode' value='".$customerpostcode."' />");
											print("</div>");
										print("</div>");
										
										print("<div id='address'".(empty($customeraddress1) && empty($customertown) && empty($customercounty) ? " class='hide'" : "").">");
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<input type='text' class='required depend' id='frm_address' required aria-label='Address 1' name='frm_address' tabindex='7' value='".$customeraddress1."' />");
												print("</div>");
											print("</div>");
											if(!empty($customeraddress2)) {
												print("<div class='col s12'>");
													print("<div class='input-field'>");
														print("<input type='text' class='required depend' id='frm_address2' aria-label='Address 2' name='frm_address2' tabindex='8' value='".$customeraddress2."' />");
													print("</div>");
												print("</div>");
											}
											if(!empty($customeraddress3)) {
												print("<div class='col s12'>");
													print("<div class='input-field'>");
														print("<input type='text' class='required depend' id='frm_address3' aria-label='Address 3' name='frm_address3' tabindex='8' value='".$customeraddress3."' />");
													print("</div>");
												print("</div>");
											}
											print("<div class='col s12 m12 l6'>");
												print("<div class='input-field'>");
													print("<input type='text' class='required depend' id='frm_town' name='frm_town' aria-label='Town' tabindex='9' value='".$customertown."' required/>");
												print("</div>");
											print("</div>");
											print("<div class='col s12 m12 l6'>");
												print("<div class='input-field'>");
													print("<input type='text' class='required depend' id='frm_county' name='frm_county' aria-label='County' tabindex='10' value='".$customercounty."' required/>");
												print("</div>");
											print("</div>");
										print("</div>");
								
									print("</div>");
									
									// Delivery Instructions
									print("<div class='row'>");
										print("<div class='col s12'>");
											print("<div class='input-field'>");
												print("<label for='frm_note'>Where shall we leave your boxes when you aren't home? Is there anything that will help us find you?</label>");
												print("<textarea class='materialize-textarea' id='frm_note' name='frm_note' data-length='500' maxlength='500' tabindex='11' placeholder=\"For example: We're up the lane by the school. Please leave the boxes on the porch.\">".$customernote."</textarea>");
											print("</div>");
										print("</div>");
									print("</div>");
									
									// Contact Information
									print("<h3>Contact Information</h3>");
									print("<div class='row'>");
									
										// Telephone
										print("<div class='col s12'>");
											print("<div class='input-field'>");
												print("<input type='tel' class='required' name='frm_telephone' data-label='contact number' aria-label='Telephone/Mobile Number' tabindex='12' maxlength='11' placeholder='Telephone/Mobile Number' value='".$customertelephone."' />");
											print("</div>");
										print("</div>");
										
									print("</div>");
									
									print("<button type='submit' tabindex='13' class='btn btn-primary wide'>Save Changes</button>");
									
								print("</form>");
								
							print("</div>");
						print("</div>");
						
						break;
					case "favourites":
							
						print("<div id='favourites'>");
							
							//	Top Sorting Panel
							print("<div class='product-bar product-options'>");
								print("<div class='jplist-pagination-info hide-on-small-only' data-type='<span>Showing {start}-{end} of {all} results</span>' data-control-type='pagination-info' data-control-name='paging' data-control-action='paging'></div>");
								print("<button class='btn btn-primary right basket-all'>Add <u>All</u> to Basket</button>");
							print("</div>");
							
							//	Search Results
							print("<div class='products'></div>");
							
							//	No Search Results
							print("<div class='jplist-no-results'>");
								print("<div class='advert'>");
									print("<h2>Nothing Found</h2>");
									print("<p>You haven't added any items to your favourites yet. Not to worry, you can add favourites by clicking on the <i class='fas fa-heart'></i> in the shop.</p>");
								print("</div>");
							print("</div>");

							//	Bottom Search Results
							print("<div class='product-bar product-options'>");
								print("<div class='dropdown jplist-items-per-page hide' data-control-type='boot-items-per-page-dropdown' data-control-name='paging' data-control-action='paging'>");
									print("<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown' id='dropdown-menu-1' aria-expanded='true'>");
										print("<span data-type='selected-text'>Items per Page</span>");
										print("<span class='caret'></span>");
									print("</button>");
									print("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdown-menu-1'>");
										print("<li role='presentation'><a role='menuitem' tabindex='-1' href='#' data-number='15' data-default='true'>15 per page</a></li>");
									print("</ul>");
								print("</div>");
								print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-range='5'></ul>");
							print("</div>");
					
						print("</div>");
					
						break;
					case "saved-orders":
						
						print("<div id='saved'>");
							
							//	Top Sorting Panel
							print("<div class='product-bar product-options'>");
								print("<div class='jplist-sort' data-control-type='boot-sort-drop-down' data-control-name='bootstrap-sort-dropdown-demo' data-control-action='sort' data-datetime-format='{year}-{month}-{day}'>");
									print("<button class='dropdown-trigger filter-button' type='button' data-target='sort-dropdown-menu'><i class='material-icons'>sort</i>Order Results<i class='material-icons right'>arrow_drop_down</i></button>");
									print("<ul id='sort-dropdown-menu' class='dropdown-content'>");
										print("<li>");
											print("<a href='#' data-path='.any' data-order='asc' data-type='number' data-default='true'>Any</a>");
										print("</li>");
										print("<li>");
											print("<a href='#' data-path='.recent' data-order='asc' data-type='number'>Most Recent</a>");
										print("</li>");
										print("<li>");
											print("<a rhref='#' data-path='.price' data-order='desc' data-type='number'>Price: High-Low</a>");
										print("</li>");
									print("</ul>");
								print("</div>");
								print("<div class='jplist-pagination-info hide-on-small-only' data-type='<span>Showing {start}-{end} of {all} results</span>' data-control-type='pagination-info' data-control-name='paging' data-control-action='paging'></div>");
								print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-range='5'><li data-type='page' class='jplist-current active' data-active='true' data-number='0'><a href='#'>1</a></li></ul>");
							print("</div>");
							
							//	Search Results
							print("<div class='products'></div>");
							
							//	No Search Results
							print("<div class='jplist-no-results'>You have no saved orders!</div>");
							
							//	Bottom Search Results
							print("<div class='product-bar order-options'>");
								print("<div class='dropdown jplist-items-per-page hide' data-control-type='boot-items-per-page-dropdown' data-control-name='paging' data-control-action='paging'>");
									print("<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown' id='dropdown-menu-1' aria-expanded='true'>");
										print("<span data-type='selected-text'>Items per Page</span>");
										print("<span class='caret'></span>");
									print("</button>");
									print("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdown-menu-1'>");
										print("<li role='presentation'><a role='menuitem' tabindex='-1' href='#' data-number='15' data-default='true'>15 per page</a></li>");
									print("</ul>");
								print("</div>");
								print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-range='5'><li data-type='page' class='jplist-current active' data-active='true' data-number='0'><a href='#'>1</a></li></ul>");
							print("</div>");
					
						print("</div>");
						
						print("<div id='order-modal' class='modal large'>");
							print("<div class='modal-content'>");
								print("<form class='order-save'>");
									print("<div class='input-field crop-top'>");
										print("<h4>Edit <input type='text' name='frm_title' value='' /></h4>");
									print("</div>");
									print("<div class='row'>");
										print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<input type='text' name='frm_description' value='' />");
												print("</div>");
										print("</div>");
									print("</div>");
									print("<button class='btn btn-tertiary order-save'>Save Changes</button>");
								print("</form>");
							print("</div>");
						print("</div>");
					
						break;
					case "processed-orders":
						
						print("<div id='orders'>");
							
							//	Top Sorting Panel
							print("<div class='order-bar order-options'>");
								print("<div class='jplist-sort' data-control-type='boot-sort-drop-down' data-control-name='bootstrap-sort-dropdown-demo' data-control-action='sort' data-datetime-format='{year}-{month}-{day}'>");
									print("<button class='dropdown-trigger filter-button' type='button' data-target='sort-dropdown-menu'><i class='material-icons'>sort</i>Order Results<i class='material-icons right'>arrow_drop_down</i></button>");
									print("<ul id='sort-dropdown-menu' class='dropdown-content'>");
										print("<li>");
											print("<a href='#' data-path='.any' data-order='asc' data-type='number' data-default='true'>Any</a>");
										print("</li>");
										print("<li>");
											print("<a href='#' data-path='.recent' data-order='asc' data-type='number'>Most Recent</a>");
										print("</li>");
										print("<li>");
											print("<a rhref='#' data-path='.price' data-order='desc' data-type='number'>Price: High-Low</a>");
										print("</li>");
									print("</ul>");
								print("</div>");
								print("<div class='jplist-pagination-info hide-on-small-only' data-type='<span>Showing {start}-{end} of {all} results</span>' data-control-type='pagination-info' data-control-name='paging' data-control-action='paging'></div>");
								print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-range='5'><li data-type='page' class='jplist-current active' data-active='true' data-number='0'><a href='#'>1</a></li></ul>");
							print("</div>");
							
							//	Search Results
							print("<div class='table'>");
								print("<div class='table-row-head'>");
									print("<div class='table-cell'><span>Order #</span></div>");
									print("<div class='table-cell'><span>Date Purchased</span></div>");
									print("<div class='table-cell'><span>Status</span></div>");
									print("<div class='table-cell'><span>Total</span></div>");
								//	print("<div class='table-cell'><span>Action</span></div>");
								print("</div>");
								print("<div class='orders table-row-group'></div>");
							print("</div>");
							
							//	No Search Results
							print("<div class='jplist-no-results'>You have no processed orders.</div>");

				/*			print("<div id='order-modal' class='modal large'>");
								print("<div class='modal-content'>");
									print("<form id='order' method='method' action=''>");
										print("<h4 class='h2 crop-bottom'>Edit Order</h4>");
										
										print("<div class='row'>");
											print("<div class='col s12'>");
												print("<div class='input-field'>");
													print("<input type='text' id='frm_name' name='frm_name' value='Test Order' />");#
												print("</div>");
											print("</div>");
										print("</div>");
										
										$strdbsql = "SELECT stock.recordID, stock_group_information.name AS stockGroup, stock_group_information.description AS stockDescription, stock_group_information.productType, stock_group_information.productCost FROM stock 
										LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID";
										$result = query($conn,$strdbsql,"multi",null);
										
										$limit = 10;
										
										if($result > 0) {
											for($i = 1; $i <= $limit; $i++) {
												if($i % 2 == 1) print("<div class='row crop-bottom'>");
													print("<div class='col s12 m12 l6'>");
														print("<div class='input-field'>");
															print("<label for='frm_item".$i."'>Item ".$i."</label>");
															print("<select class='browser-default' name='frm_item".$i."'>");
																foreach($result AS $row) {
																	print("<option value='".$row['recordID']."'>".$row['stockGroup']."</option>");
																}
															print("</select>");
														print("</div>");
													print("</div>");
												if($i % 2 == 0) print("</div>");
											}
										}
										
									print("</form>");
								print("</div>");
								print("<div class='modal-footer'>");
									print("<a href='#!' class='btn btn-default modal-close'>Cancel</a>");
									print("<a href='#!' class='btn btn-tertiary modal-close'>Save</a>");
								print("</div>");
							print("</div>");*/
							
							//	Bottom Search Results
							print("<div class='order-bar order-options'>");
								print("<div class='dropdown jplist-items-per-page hide' data-control-type='boot-items-per-page-dropdown' data-control-name='paging' data-control-action='paging'>");
									print("<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown' id='dropdown-menu-1' aria-expanded='true'>");
										print("<span data-type='selected-text'>Items per Page</span>");
										print("<span class='caret'></span>");
									print("</button>");
									print("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdown-menu-1'>");
										print("<li role='presentation'><a role='menuitem' tabindex='-1' href='#' data-number='15' data-default='true'>15 per page</a></li>");
									print("</ul>");
								print("</div>");
								print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-range='5'><li data-type='page' class='jplist-current active' data-active='true' data-number='0'><a href='#'>1</a></li></ul>");
							print("</div>");
					
						print("</div>");
					
						break;
					case "my-reviews":
							
						print("<div id='reviews'>");
							
							//	Top Sorting Panel
							print("<div class='review-bar comment-options'>");
								print("<div class='jplist-sort' data-control-type='boot-sort-drop-down' data-control-name='bootstrap-sort-dropdown-demo' data-control-action='sort' data-datetime-format='{year}-{month}-{day}'>");
									print("<button class='dropdown-trigger filter-button' type='button' data-target='sort-dropdown-menu'><i class='material-icons'>sort</i>Order Results<i class='material-icons right'>arrow_drop_down</i></button>");
									print("<ul id='sort-dropdown-menu' class='dropdown-content'>");
										print("<li>");
											print("<a href='#' data-path='.any' data-order='asc' data-type='number' data-default='true'>Any</a>");
										print("</li>");
										print("<li>");
											print("<a href='#' data-path='.price' data-order='asc' data-type='number'>Price: Low-High</a>");
										print("</li>");
										print("<li>");
											print("<a rhref='#' data-path='.price' data-order='desc' data-type='number'>Price: High-Low</a>");
										print("</li>");
									print("</ul>");
								print("</div>");
								print("<div class='jplist-pagination-info hide-on-small-only' data-type='<span>Showing {start}-{end} of {all} results</span>' data-control-type='pagination-info' data-control-name='paging' data-control-action='paging'></div>");
								print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-control-animate-to-top='true' data-range='5'></ul>");
							print("</div>");
							
							//	Search Results
							print("<div class='comments'></div>");
							
							//	No Search Results
							print("<div class='jplist-no-results'>You have no reviews. We appreciate any feedback!</div>");

							//	Bottom Search Results
							print("<div class='review-bar comment-options'>");
								print("<div class='dropdown jplist-items-per-page hide' data-control-type='boot-items-per-page-dropdown' data-control-name='paging' data-control-action='paging'>");
									print("<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown' id='dropdown-menu-1' aria-expanded='true'>");
										print("<span data-type='selected-text'>Items per Page</span>");
										print("<span class='caret'></span>");
									print("</button>");
									print("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdown-menu-1'>");
										print("<li role='presentation'><a role='menuitem' tabindex='-1' href='#' data-number='15' data-default='true'>15 per page</a></li>");
									print("</ul>");
								print("</div>");
								print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-range='5'></ul>");
							print("</div>");
					
						print("</div>");
					
						break;
				}
				
			print("</div>");
		print("</div>");
		
	} else {
		
		?>
		
		<script>
		window.location.replace("/register");
		</script>
		
		<?php
		
	}

?>