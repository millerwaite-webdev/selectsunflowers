<?php
		
	if($_REQUEST['article']) {
		
		$strdbsql = "SELECT * FROM site_news_events WHERE link = :article AND enabled = 1";
		$result = query($conn,$strdbsql,"single",array("article"=>$_REQUEST['article']));

		if($result) {
			
			print("<div class='row'>");
				print("<div class='col s12 m12 l3 right'>");
					print("<div class='sidebar'>");
						
						// Popular
						$strdbsql = "SELECT * FROM site_news_events WHERE enabled = 1 ORDER BY views DESC LIMIT 3";
						$result2 = query($conn,$strdbsql,"multi");
						
						if(count($result2) > 0) {
							print("<div class='recent block hide-on-med-and-down'>");
								print("<h3 class='h2'>Popular Posts</h3>");
								print("<ul>");
									foreach($result2 AS $row) {
										print("<li>");
											print("<div class='recent-wrapper'>");
											
												$strdbsql = "SELECT imageRef FROM site_news_images WHERE newsID = :article ORDER BY imageOrder";
												$images = query($conn,$strdbsql,"single",["article"=>$row['recordID']]);
											
												if(count($images) > 0) {
													print("<a href='/".$strPage."/".$row['link']."' aria-label='".$row['title']."'>");
														print("<div class='recent-image' style='background-image:url(/images/news/".$images['imageRef'].");'></div>");
													print("</a>");
												}
												
												print("<div class='recent-content'>");
													print("<h4 class='title ".(strlen($row['title']) > 30 ? "truncate" : "")."'><a href='/".$strPage."/".$row['link']."'>".substr($row['title'], 0, 30)."</a></h4>");
													print("<span class='date'>".date("jS F Y", $row['date'])."</span>");
													print("<a class='link' href='/".$strPage."/".$row['link']."'>Read More</a>");
												print("</div>");
											print("</div>");
										print("</li>");
									}
								print("</ul>");
							print("</div>");
						}
						
						// Contact
						print("<div class='help block'>");
							print("<h3 class='h2'>Need Help?</h3>");
							print("<a href='/contact-us' class='btn btn-base'>Get in Touch</a>");
						print("</div>");
						
					print("</div>");
				print("</div>");
				print("<div class='col s12 m12 l9'>");
					print("<div class='blog'>");
					
						$strdbsql = "SELECT * FROM site_news_images WHERE newsID = :article ORDER BY imageOrder";
						$images = query($conn,$strdbsql,"multi",["article"=>$result['recordID']]);
						
						if(count($images) > 0) {
							print("<div class='blog-image'>");
								
								if(!empty($result['categoryID'])) {
									print("<div class='sticker'>");
										switch($result['categoryID']) {
											case 1:
												print("<img src='/images/elements/offer.png' alt='".$result['category']."'>");
												break;
											case 2:
												print("<img src='/images/elements/new.png' alt='".$result['category']."'>");
												break;
										}
									print("</div>");
								}

								if(!empty($result['title'])) print("<h1 class='h3'>".$result['title']."</h1>");
								
								if(count($images) > 1) {
									print("<div class='slick-slider'>");														
										foreach($images AS $image) {
											print("<div>");
												print("<img src='/images/news/".$image['imageRef']."' alt='".$result['title']."' />");
											print("</div>");
										}
									print("</div>");
								} else if(count($images) == 1) {
									print("<img class='main-image' src='/images/news/".$images[0]['imageRef']."' data-caption='".$result['title']."' alt='".$result['title']."' />");
								}

							print("</div>");
						}
						
						
						print("<div class='blog-options'>");
							
							// Author
							if(!empty($result['author'])) $author = explode(" ", $result['author']);
							else  $author = explode(" ", $compName);
							
							if(count($author) > 0) {
								print("<div class='author'>");
									print("<img class='profile-small circle' data-name='".(count($author) > 1 ? substr($author[0], 0, 1).substr($author[1], 0, 1) : $result['author'])."' alt='".$result['author']."' />");
									print("<span>by ".\ForceUTF8\Encoding::toUTF8($result['author'])."</span>");
								print("</div>");
							}
							
							// Date
							print("<span class='date'>".date("jS F Y", $result['date'])."</span>");
							
						print("</div>");
						print("<div class='blog-content'>");
							if(!empty($result['content'])) print(\ForceUTF8\Encoding::toUTF8($result['content']));
						print("</div>");
						print("<div class='blog-buttons'>");
							print("<div class='sharethis-inline-share-buttons'></div>");
						print("</div>");
					print("</div>");
				print("</div>");
			print("</div>");
			
			$strdbsql = "UPDATE site_news_events SET views = views + 1 WHERE link = :article AND enabled = 1";
			$result = query($conn,$strdbsql,"update",array("article"=>$_REQUEST['article']));
			
		} else {
			
			print("<div class='row crop-bottom'>");
				print("<div class='col s12 m9'>");
					print("<h1 class='h3'>Blog not found</h1>");
					print("<p>The link you have followed is broken or has expired. Use the links below to return to the blog page or report this issue. Thank you!</p>");
				print("</div>");
			print("</div>");
			
			print("<div class='row crop-bottom'>");
				print("<div class='col'>");
					print("<a href='/blog' class='btn btn-contact'>Return to Blog</a>");
				print("</div>");
				print("<div class='col'>");
					print("<a href='/contact' class='btn btn-tertiary'>Report Issue</a>");
				print("</div>");
			print("</div>");
			
		}
		
	} else {
		
		print("<div id='events'>");
			print("<div class='row'>");
				print("<div class='col s12 m12 l3 right'>");
					print("<div class='sidebar'>");
						
						// Search
						print("<div class='event-bar event-options block'>");
							print("<div class='text-filter-box'>");
								print("<h3 class='h2'>Search Posts</h3>");
								print("<div class='input-field'>");
									print("<button type='button' id='post-search' class='btn transparent isolate postfix center'>");
										print("<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'><defs><style>.cls-1{fill:none;stroke:#ababab;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs><title>search</title><g><g><circle class='cls-1' cx='9' cy='9' r='8'></circle><line class='cls-1' x1='19' y1='19' x2='14.65' y2='14.65'></line></g></g></svg>");
									print("</button>");
									print("<input data-path='.blog' type='text' value='' class='jplist-no-right-border' placeholder='Search keywords' aria-label='Search Keywords' data-control-type='textbox' data-control-name='blog-filter' data-control-action='filter' data-button='#post-search' />");
								print("</div>");
							print("</div>");
						print("</div>");
						
						// Popular
						$strdbsql = "SELECT * FROM site_news_events WHERE enabled = 1 ORDER BY views DESC LIMIT 3";
						$result = query($conn,$strdbsql,"multi");
						
						if(count($result) > 0) {
							print("<div class='recent block hide-on-med-and-down'>");
								print("<h3 class='h2'>Popular Posts</h3>");
								print("<ul>");
									foreach($result AS $row) {
										print("<li>");
											print("<div class='recent-wrapper'>");
											
												$strdbsql = "SELECT imageRef FROM site_news_images WHERE newsID = :article ORDER BY imageOrder";
												$images = query($conn,$strdbsql,"single",["article"=>$row['recordID']]);
											
												if(count($images) > 0) {
													print("<a href='/".$strPage."/".$row['link']."' aria-label='".$row['title']."'>");
														print("<div class='recent-image' style='background-image:url(/images/news/".$images['imageRef'].");'></div>");
													print("</a>");
												}
												print("<div class='recent-content'>");
													print("<h4 class='title ".(strlen($row['title']) > 30 ? "truncate" : "")."'><a href='/".$strPage."/".$row['link']."'>".substr($row['title'], 0, 30)."</a></h4>");
													print("<span class='date'>".date("jS F Y", $row['date'])."</span>");
													print("<a class='link' href='/".$strPage."/".$row['link']."'>Read More</a>");
												print("</div>");
											print("</div>");
										print("</li>");
									}
								print("</ul>");
							print("</div>");
						}
						
						// Contact
						print("<div class='help block'>");
							print("<h3 class='h2'>Need Help?</h3>");
							print("<a href='/contact-us' class='btn btn-base'>Get in Touch</a>");
						print("</div>");
						
					print("</div>");
				print("</div>");
				print("<div class='col s12 m12 l9'>");
					
					//	Search Results
					print("<div class='events'></div>");
					
					//	No Search Results
					print("<div class='jplist-no-results'>");
						print("<h1 class='h2'>Nothing Found</h1>");
						print("<p>Sorry, but nothing matched your search terms. Please try again!</p>");
					print("</div>");

					//	Bottom Search Results
					print("<div class='event-bar event-options bottom'>");
						print("<div class='dropdown jplist-items-per-page hide' data-control-type='boot-items-per-page-dropdown' data-control-name='paging' data-control-action='paging'>");
							print("<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown' id='dropdown-menu-1' aria-expanded='true'>");
								print("<span data-type='selected-text'>Items per Page</span>");
								print("<span class='caret'></span>");
							print("</button>");
							print("<ul class='dropdown-menu' role='menu' aria-labelledby='dropdown-menu-1'>");
								print("<li role='presentation'><a role='menuitem' tabindex='-1' href='#' data-number='15' data-default='true'>15 per page</a></li>");
							print("</ul>");
						print("</div>");
						print("<ul class='pagination right jplist-pagination' data-control-type='boot-pagination' data-control-name='paging' data-control-action='paging' data-range='5'></ul>");
					print("</div>");
				
				print("</div>");
			print("</div>");
		print("</div>");

	}

?>