<?php

	namespace Data;

	use PDO;
	
	class SeasonMethods
	{
		// Properties
		public $conn;
		
		// Constructor
		function __construct(PDO $conn) {
			$this->conn = $conn;
		}
		
		// Current Season
		public function currentSeason() {
			$date = date("2020-m-d");
			$strdbsql = "SELECT seasonName AS name, seasonLabel AS label, link, imageFile AS image, themeColour AS colour, typeID AS type FROM seasons 
			WHERE dateStart <= '$date' AND dateEnd >= '$date' 
			ORDER BY typeID DESC";
			$result = query($this->conn,$strdbsql,"single");
			return $result;
		}
	}
	
	class ProductMethods
	{
		// Properties
		public $conn, $id, $code;
		
		// Constructor
		function __construct(PDO $conn) {
			$this->conn = $conn;
		}
		
		// Product Data
		public function productData() {
			$strdbsql = "SELECT stock.*, stock_group_information.name AS stockName, stock_group_information.summary, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost, stock_units.description AS unit FROM stock 
			INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
			INNER JOIN stock_units ON stock.unit = stock_units.recordID 
			WHERE stockCode LIKE :product AND stock_group_information.enabled = 1";
			$result = query($this->conn,$strdbsql,"single",["product"=>$this->code]);
			return $result;
		}
		
		// Product HTML
		public function productHTML() {
			
			$strdbsql = "SELECT stock.*, stock_group_information.name AS stockName, stock_group_information.metaLink AS url, stock_group_information.summary, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost, stock_units.description AS unit FROM stock 
			INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
			INNER JOIN stock_units ON stock.unit = stock_units.recordID 
			WHERE stock.groupID = :product AND stock_group_information.enabled = 1 AND `stock`.`status` = 1 
			ORDER BY stock.recordID";
			$result = query($this->conn,$strdbsql,"single",["product"=>$this->id]);
		
			$this->id = $id = $result['groupID'];
			$this->account = $_COOKIE['account'];
			
			$code = $result['stockCode'];
			$name = $result['stockName'];
			$description = substr($result['summary'], 0, 100);
			$link = "/shop/".($strcategory ? $strcategory : "all")."/".str_replace(" ", "-", strtolower($result['url']));
			$type = $result['productType'];
			$unit = $result['unit'];
			$price = ($type > 0 ? $result['productCost'] : $result['price']);
			
			$html .= "<div class='product' itemscope itemtype='https://schema.org/Product'>";
				$html .= "<div class='product-wrapper'>";
				
					// Favourite
					$favourite = $this->checkFavourite();
					$html .= "<button class='favourite-toggle".($favourite ? " active" : "")."' data-id='".$id."'><i class='fas fa-heart'></i></button>";
					
					if($result2) $html .= "<button class='favourite-toggle active' data-id='".$row['groupID']."' aria-label=\"Remove ".$row['contentName']." From Favorites\"><i class='fas fa-heart'></i></button>";
					else $html .= "<button class='favourite-toggle' data-id='".$row['groupID']."' aria-label=\"Add ".$row['contentName']." To Favorites\"><i class='fas fa-heart'></i></button>";
					
					// Price
					$html .= "<span class='price' itemprop='offers' itemscope itemtype='https://schema.org/AggregateOffer'>From ";
						$html .= "&pound;";
						$html .= "<span itemprop='lowPrice'>".$price."</span>";
						$html .= "<meta itemprop='priceCurrency' content='GBP' />";
					$html .= "</span>";
					
					// Image
					$html .= "<div class='product-image' data-group='".$row['groupID']."'>";
						$html .= "<a href='".$link."'>";
							
							// Stickers
							$sticker = $this->mainSticker();							
							if(!empty($sticker['contentMenuImage'])) $html .= "<div class='sticker'><img src='/images/elements/".$sticker['contentMenuImage']."' alt='".\ForceUTF8\Encoding::toUTF8($sticker['contentName'])."'></div>";
							
							// Image
							$image = $this->mainImage();
							if($image) $html .= "<img src='/images/products/thumbnails/".$image['imageLink']."' alt='".\ForceUTF8\Encoding::toUTF8($name)."' itemprop='image' />";
							else $html .= "<img src='/images/products/other/no-images.jpg' alt='".$row['stockGroup']."' />";
							
						$html .= "</a>";
					$html .= "</div>";
					
					//	PRODUCT DETAILS
					$html .= "<div class='product-content'>";

						// Title
						$html .= "<a href='".$link."'><span class='title' itemprop='name'>".\ForceUTF8\Encoding::toUTF8($name)."</span></a>";
						
						// Description
						$html .= "<span class='description' itemprop='description'>".\ForceUTF8\Encoding::toUTF8(substr($description, 0, 100))."</span>";
						
						// Reviews
					/*	$reviews = $this->collectReviews();
						
						foreach($reviews AS $review) $total += $review['numStars'];
						
						if(count($reviews) > 0) $average = $total / count($reviews);
						else $average = 0;
						
						$html .= "<ul class='reviews' itemprop='aggregateRating' itemscope itemtype='https://schema.org/AggregateRating'>";
							for($stars = 1; $stars <= $average; $stars++) {
								$html .= "<li class='filled'><i class='material-icons'>star</i></li>";
							}
							while($stars <= 5) {
								$html .= "<li><i class='material-icons'>star_border</i></li>";
								$stars++;
							}
							$html .= "<li><span itemprop='ratingValue'>".number_format($average, 1)."</span>(<span itemprop='ratingCount'>".count($reviews)."</span>)</li>";
						$html .= "</ul>";*/
						
						// Units
						$html .= "<span class='unit hide'>".$unit."</span>";

						$html .= "<a class='btn btn-primary small' href='".$link."'>Buy Now</a>"; // Change to Learn More
						
						// Stickers
					/*	$stickers = $this->allStickers();
						if(count($stickers) > 0) {
							$html .= "<ul class='key'>";
								foreach($stickers AS $key => $sticker) {
									if(count($stickers) > 1 && $key == 0) {
										// Skip first
									} else {
										if(!empty($sticker['contentMenuImage'])) $html .= "<li><img title='".$sticker['contentName']."' src='/images/elements/".$sticker['contentMenuImage']."' alt='".$sticker['contentName']."' /></li>";
									}
								}
							$html .= "</ul>";
						}*/
						
					$html .= "</div>";
				$html .= "</div>";
			$html .= "</div>";
			
			return $html;
		}
		
		// Gallery Images
		public function galleryImages() {
			$strdbsql = "SELECT * FROM stock_images 
			WHERE stockID = :product AND imageTypeID = 1 
			ORDER BY imageOrder";
			$result = query($this->conn,$strdbsql,"multi",["product"=>$this->id]);
			return $result;
		}
		
		// Main Image
		public function mainImage() {
			$strdbsql = "SELECT * FROM stock_images 
			WHERE stockID = :product AND imageTypeID = 1 
			ORDER BY imageOrder LIMIT 1";
			$result = query($this->conn,$strdbsql,"single",["product"=>$this->id]);
			return $result;
		}
		
		// Check Favourite
		public function checkFavourite() {
			$strdbsql = "SELECT * FROM stock_favourites 
			WHERE stockCode = :product AND customerID = :customer";
			$result = query($this->conn,$strdbsql,"single",["product"=>$this->id,"customer"=>$this->account]);
			return $result;
		}
		
		// Filter Sticker
		public function mainSticker() {
			$strdbsql = "SELECT * FROM stock_category_relations 
			INNER JOIN category ON stock_category_relations.categoryID = category.recordID 
			INNER JOIN category_relations ON stock_category_relations.categoryID = category_relations.childID 
			WHERE stock_category_relations.stockID = :product AND contentMenuImage IS NOT NULL AND contentMenuImage <> '' AND showSticker = 1
			ORDER BY category_relations.menuOrder
			LIMIT 1";
			$result = query($this->conn,$strdbsql,"single",["product"=>$this->id]);
			return $result;
		}
		
		// Filter Sticker
		public function allStickers() {
			$strdbsql = "SELECT * FROM stock_category_relations 
			INNER JOIN category ON stock_category_relations.categoryID = category.recordID 
			INNER JOIN category_relations ON stock_category_relations.categoryID = category_relations.childID 
			WHERE stock_category_relations.stockID = :product AND contentMenuImage IS NOT NULL AND contentMenuImage <> ''
			ORDER BY category_relations.menuOrder";
			$result = query($this->conn,$strdbsql,"multi",["product"=>$this->id]);
			unset($result[0]);
			return $result;
		}
		
		// Collect Reviews
		public function collectReviews() {
			$strdbsql = "SELECT * FROM stock_reviews 
			WHERE stockGroupID = :product AND shownOnSite = 1 ORDER BY dateReview";
			$result = query($this->conn,$strdbsql,"multi",["product"=>$this->id]);
			return $result;
		}
		
		// Get Unit
		public function getUnit() {
			$strdbsql = "SELECT stock_units.abbreviation AS unit FROM stock 
			INNER JOIN stock_units ON stock.unit = stock_units.recordID 
			WHERE stock.recordID = :product";
			$result = query($this->conn,$strdbsql,"single",["product"=>$this->id]);
			return $result['unit'];
		}
		
		// Get Categories
		public function getCategory($id) {
			$strdbsql = "SELECT * FROM category 
			WHERE metaPageLink = :category";
			$result = query($this->conn,$strdbsql,"single",["category"=>$this->id]);
			return $result;
		}
	}

?>