<?php

	print("<h1 class='h2'>Sitemap</h1>");

	print("<div class='row'>");
	
		// General
		$strdbsql = "SELECT * FROM site_pages WHERE visible = 1 ORDER BY pageOrder";
		$result = query($conn,$strdbsql,"multi");
	
		if(count($result) > 0) {
			print("<div class='col s12 m6 l4'>");
				print("<h2 class='h3'>General</h2>");
				print("<ul class='sitemap'>");
					foreach($result AS $row) {
						print("<li><a href='".$strsiteurl.$row['pageName']."' class='truncate'>".\ForceUTF8\Encoding::toUTF8($row['pageTitle'])."</a></li>");
					}
				print("</ul>");
			print("</div>");
		}
		
		// Categories
	/*	$strdbsql = "SELECT category.contentName, category.recordID AS categoryID FROM category 
		INNER JOIN category_relations ON category.recordID = category_relations.childID 
		WHERE category_relations.parentID = category_relations.childID 
		ORDER BY category_relations.menuOrder";
		$result = query($conn,$strdbsql,"multi");
	
		if(count($result) > 0) {
			print("<div class='col s12 m6 l4'>");
				foreach($result AS $row) {
					print("<h2 class='h3'>".$row['contentName']."</h2>");
					print("<ul class='sitemap'>");
						$strdbsql = "SELECT category.* FROM category 
						INNER JOIN category_relations ON category.recordID = category_relations.childID 
						WHERE category_relations.parentID = :parent AND category.recordID <> category_relations.parentID 
						ORDER BY menuOrder";
						$result2 = query($conn,$strdbsql,"multi",["parent"=>$row['categoryID']]);
						foreach($result2 AS $row2) {
							print("<li><a href='".$strsiteurl."shop#categories:pathGroup=".$row2['metaPageLink']."' class='truncate'>".\ForceUTF8\Encoding::toUTF8($row2['contentName'])."</a></li>");
						}
					print("</ul>");
				}
			print("</div>");
		}*/
		
		// Blogs
		$strdbsql = "SELECT * FROM site_news_events WHERE enabled = 1 ORDER BY date DESC";
		$result = query($conn,$strdbsql,"multi");
	
		if(count($result) > 0) {
			print("<div class='col s12 m6 l4'>");
				print("<h2 class='h3'>Tips &amp; Advice</h2>");
				print("<ul class='sitemap'>");
					foreach($result AS $row) {
						print("<li><a href='".$strsiteurl."news/".$row['link']."' class='truncate'>".\ForceUTF8\Encoding::toUTF8($row['title'])."</a></li>");
					}
				print("</ul>");
			print("</div>");
		}
		
		// Account
		if($accountid > 0) {
			print("<div class='col s12 m6 l4'>");
				print("<h2 class='h3'>Account</h2>");
				print("<ul class='sitemap'>");
					print("<li><a href='".$strsiteurl."account' class='truncate'>Account Overview</a></li>");
					print("<li><a href='".$strsiteurl."account/personal-details' class='truncate'>Personal Details</a></li>");
					print("<li><a href='".$strsiteurl."account/processed-orders' class='truncate'>Processed Orders</a></li>");
					print("<li><a href='".$strsiteurl."account/favourites' class='truncate'>Favourites</a></li>");
					print("<li><a href='".$strsiteurl."account/my-reviews' class='truncate'>My Reviews</a></li>");
					print("<li><a href='".$strsiteurl."register' class='truncate'>Register</a></li>");
				print("</ul>");
			print("</div>");
		}
		
	print("</div>");
			
?>