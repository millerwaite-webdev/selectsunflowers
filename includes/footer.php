<?php

	print("<footer class='page-footer decor-top'>");
		print("<div class='container'>");
			
			print("<div class='row'>");
				print("<div class='col s4 m4 l8'>");
			
					// Newsletter Signup
					print("<form id='newsletter' name='newsletter' action='' method='post'>");
						print("<div class='input-field'>");
							print("<label class='hide sub-text-resp' for='frm_subscribe'>Enter your email address to subscribe to our newsletter</label>");
							print("<input class='subscribe-resp' type='email' id='frm_subscribe' name='frm_subscribe' placeholder='Enter your email address to subscribe to our newsletter' value='' />");
							print("<button class='btn btn-primary postfix small center-btn' type='submit'>Subscribe</button>");
						print("</div>");
					print("</form>");
					
					print("<div class='row'>");
						print("<div class='col s4 m4 l4'>");
						
							// Logo
						/*	print("<div class='logo hide-on-med-and-down'>");
								print("<a href='".$strsiteurl."'>");
									print("<img src='/images/company/logo-vertical.png' alt='".$compName."' />");
								print("</a>");
							print("</div>");*/
							
							// Site Links
							if(count($footerLinks) > 0) {
								print("<div class='links'>");
									print("<h5>Useful links</h5>");
									print("<ul>");
										foreach($footerLinks AS $footerLink) {
											print("<li><a href='".$strsiteurl.$footerLink['pageName']."'>".$footerLink['pageTitle']."</a></li>");
										}
										print("<li><a href='".$strsiteurl."sitemap' class='active'>View All Pages</a></li>");
									print("</ul>");
								print("</div>");
							}
							
						print("</div>");
						print("<div class='col s8 m8 l8'>");
						
							// Product Search
						/*	print("<form id='products' class='hide-on-med-and-down' name='products' action='/shop' method='post'>");
								print("<div class='input-field'>");
									print("<button type='submit' class='btn transparent isolate postfix center'>");
										print("<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'><defs><style>.cls-1{fill:none;stroke:#ababab;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}</style></defs><title>search</title><g><g><circle class='cls-1' cx='9' cy='9' r='8'></circle><line class='cls-1' x1='19' y1='19' x2='14.65' y2='14.65'></line></g></g></svg>");
									print("</button>");
									print("<label class='hide' for='footer_search'>Search Sunflowers</label>");
									print("<input type='text' id='footer_search' name='search' placeholder='Search Sunflowers' value='".$_REQUEST['search']."' />");
								print("</div>");
							print("</form>");*/
							
							// Categories
							$strdbsql = "SELECT category.recordID, category.contentName AS name FROM category 
							INNER JOIN category_relations ON category.recordID = category_relations.childID 
							WHERE category_relations.parentID = category_relations.childID
							ORDER BY category_relations.level, category_relations.menuOrder
							LIMIT 2";
							$categories = query($conn,$strdbsql,"multi");
							
							if(count($category) > 0) {
								print("<div class='row crop-bottom'>");
									foreach($categories AS $category) {
										print("<div class='col s6 m6'>");
							
											// Subcategories
											$strdbsql = "SELECT category.recordID, category.contentName AS name, category.metaPageLink FROM category 
											INNER JOIN category_relations ON category.recordID = category_relations.childID 
											WHERE category_relations.parentID = :category AND category.recordID <> category_relations.parentID AND category.showCategory = 1 
											ORDER BY menuOrder LIMIT 5";
											$subcategories = query($conn,$strdbsql,"multi",["category"=>$category['recordID']]);
						
											if(count($subcategories) > 0) {
												print("<h5>".$category['name']."</h5>");
												print("<ul>");
													foreach($subcategories AS $subcategory) {
														print("<li class='truncate'><a href='".$strsiteurl."shop/".$subcategory['metaPageLink']."'>".$subcategory['name']."</a></li>");
													}
													print("<li><a href='".$strsiteurl."shop/all' class='active'>Shop All ".$category['name']."</a></li>");
												print("</ul>");
											}
											
										print("</div>");
									}
								print("</div>");
							}
						
						print("</div>");
					print("</div>");
				print("</div>");
				print("<div class='col s12 m12 l4'>");
				
					// Widget
					print("<div class='widget'>");
						print("<h3>Why ".$compName."?</h3>");
						print("<ul>");
							print("<li>Only the Highest Quality Seeds</li>");
							print("<li>Worldwide Shipping &amp; Delivery</li>");
							print("<li>No Hassle Shopping Experience</li>");
							print("<li>Expert Online Support</li>");
						print("</ul>");
					print("</div>");
					
					// Payment Methods
					print("<div class='cards'>");
						print("<img src='/images/elements/card-paypal.png' alt='PayPal' />");
						print("<img src='/images/elements/card-visa.png' alt='Visa' />");
						print("<img src='/images/elements/card-mastercard.png' alt='MasterCard' />");
						print("<img src='/images/elements/card-maestro.png' alt='Maestro' />");
						print("<img src='/images/elements/card-american-express.png' alt='American Express' />");
					print("</div>");
					
				print("</div>");				
			print("</div>");
			print("<div class='footer-copyright'>");
				print("<p>Copyright &copy; ".date("Y").". ".$compName." is a division of SeedSense. All Rights Reserved.</p>");
			print("</div>");
		print("</div>");
	print("</footer>");

?>	