<?php

	use Data\ProductMethods;
	
	if(session_status() != PHP_SESSION_ACTIVE) session_start();

	header("content-type: text/html; charset=utf-8");

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");
	
	// Location Check
	if(!isset($_COOKIE['locationCheck']) || $_COOKIE['locationCheck'] == 'fail') {

		$clientIP = $_SERVER['REMOTE_ADDR'];
		$ipData = IP2Location\Database::ipVersionAndNumber($clientIP); 
		/* $ipData = [
			0=> int, //IP Type (4 or 6)
			1=> longint //IP in stored as a long integar same as in DB
		];*/
		
		if($ipData[0] == 4) {
			$strdbsql = "SELECT `configCountries`.`zone`, `ip2location_db1`.`country_code` FROM `ip2location_db1` LEFT JOIN `configCountries` ON `ip2location_db1`.`country_code` LIKE `configCountries`.`countryCode` WHERE `ip_from` <= :clientIP AND `ip_to` >= :clientIPcopy";
			$q = query($conn,$strdbsql,"single",["clientIP" => $ipData[1], "clientIPcopy" => $ipData[1]]);
		} else {
			$strdbsql = "SELECT `configCountries`.`zone`, `ip2location_db1_ipv6`.`country_code` FROM `ip2location_db1_ipv6` LEFT JOIN `configCountries` ON `ip2location_db1_ipv6`.`country_code` LIKE `configCountries`.`countryCode` WHERE `ip_from` <= :clientIP AND `ip_to` >= :clientIPcopy";
			$q = query($conn,$strdbsql,"single",["clientIP" => $ipData[1], "clientIPcopy" => $ipData[1]]);
		}
		if(in_array($q['zone'],['UK', 'EU', 'UKDEPENDANCIES'])) {
			setcookie("locationCheck","pass",time()+(3*60*60));
		} else {
			setcookie("locationCheck","fail",time()+(3*60*60));
		}
	}

	print("<!DOCTYPE html>");
	print("<html lang='en'>");
	
	include("includes/header.php");
	
	print("<body>");
	
		print("<div class='bookmark'></div>");
		
		if(!isset($_COOKIE['cookies'])) {
			print("<div class='notification cookie'>");
				print("<div class='container'>");
					print("<p>We use cookies on our site to optimise your online experience. Read our cookie policy to <a href='/privacy-policy'>learn more</a>.</p>");
					print("<button class='close cookie-toggle'><i class='material-icons'>close</i></button>");
				print("</div>");
			print("</div>");
		}
		
		include("includes/navigation.php");
		
		if($accountid <= 0) {
			print("<div id='modal-login' class='modal small'>");
				print("<div class='modal-content'>");
					print("<h4 class='h3'>Log In To Your Account</h4>");
					print("<form id='login' method='post' action=''>");
						print("<div class='alert'></div>");
						print("<div class='input-field'>");
							print("<i class='material-icons prefix'>account_circle</i>");
							print("<label class='hide' for='username'>Username</label>");
							print("<input type='text' id='username' name='username' placeholder='Username' />");
						print("</div>");
						print("<div class='input-field'>");
							print("<i class='material-icons prefix'>account_circle</i>");
							print("<label class='hide' for='password'>Password</label>");
							print("<input type='password' id='password' name='password' placeholder='Password' />");
						print("</div>");
						print("<div class='input-field'>");
							print("<button type='submit' class='btn btn-primary large wide'>Login</button>");
						print("</div>");
					print("</form>");
				print("</div>");
				print("<div class='modal-footer'>");
					print("<span>New to ".$compName."? <a href='/register'>Register</a></span>");
				print("</div>");
				print("<div class='modal-under'>");
					print("<span><a href='/forgot-password'>Forgot your password?</a></span>");
				print("</div>");
			print("</div>");
		}
		
		switch($strPage) {
			case "index":
				
				if(!isset($_COOKIE['newsletter'])) {
					print("<div id='modal-newsletter' class='modal image small'>");
						print("<div class='modal-content'>");
							print("<h4 class='h3'>Subscribe to our Newsletter</h4>");
							print("<p>Join the Sunflower community to hear about our SelectSunflowers&trade; exclusive offers, insider news and much more (no rubbish we promise).</p>");
							print("<form id='subscribe' method='post' action=''>");
								print("<div class='alert'></div>");
								print("<div class='input-field'>");
									print("<label class='hide' for='email'>Email</label>");
									print("<button type='submit' class='btn btn-primary small postfix'>Subscribe</button>");
									print("<input type='text' id='frm_newsletter' name='frm_newsletter' placeholder='Email Address' />");
								print("</div>");
							print("</form>");
						print("</div>");
						print("<div class='modal-under'>");
							print("<span>We will not share your details, for more information please read our <a href='/privacy-policy'>privacy policy</a>.</span>");
						print("</div>");
					print("</div>");
				}
				
				print("<main class='dark'>");
					print("<div class='container'>");
					
						fnGetSiteBlocks($pageData['recordID'],"pageID","Above",$conn);
					
						print("<div class='row thin'>");
							print("<div class='col s12 m12 l3'>");
								
								if($accountid > 0) {
									print("<div class='favourite'>");
										print("<h3 class='h2'>Favourites</h3>");
										print("<p>View your favourites.</p>");
										print("<a href='#modal-login' class='btn btn-base modal-trigger'>View Favourites</a>");
									print("</div>");
								}
								
								// Popular
								$strdbsql = "SELECT * FROM site_news_events WHERE enabled = 1 ORDER BY views DESC LIMIT 3";
								$result = query($conn,$strdbsql,"multi");
								
								if(count($result) > 0) {
									print("<div class='recent small section'>");
										print("<h5>Latest News</h5>");
										print("<ul>");
											foreach($result AS $row) {
												print("<li>");
													print("<div class='recent-wrapper'>");
													
														$strdbsql = "SELECT imageRef FROM site_news_images WHERE newsID = :article ORDER BY imageOrder";
														$images = query($conn,$strdbsql,"single",["article"=>$row['recordID']]);
													
														if(count($images) > 0) {
															print("<a href='/news/".$row['link']."' aria-label='".$row['title']."'>");
																print("<div class='recent-image' style='background-image:url(/images/news/".$images['imageRef'].");'></div>");
															print("</a>");
														}
														print("<div class='recent-content'>");
															print("<h4 class='title ".(strlen($row['title']) > 30 ? "truncate" : "")."'><a href='/news/".$row['link']."'>".substr($row['title'], 0, 30)."</a></h4>");
															print("<span class='date'>".date("jS F Y", $row['date'])."</span>");
															print("<a class='link' href='/news/".$row['link']."'>Read More</a>");
														print("</div>");
													print("</div>");
												print("</li>");
											}
										print("</ul>");
									print("</div>");
								}
								
							print("</div>");
						
							print("<div class='col s12 m12 l9'>");
							
								print("<div class='banner'>");
									print("<ul>");
										print("<li>");
											?><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510.818 510.818" style="enable-background:new 0 0 510.818 510.818;" xml:space="preserve"><g><g><path d="M502.849,193.588l-45.088-26.208l45.088-26.112c4.992-2.912,8.032-8.256,7.968-14.08c-0.064-5.792-3.264-11.072-8.352-13.824l-176-96c-4.864-2.656-10.848-2.592-15.68,0.192l-55.36,32.192l-55.36-32.192c-4.768-2.784-10.784-2.88-15.68-0.192l-176,96c-5.12,2.752-8.32,8.064-8.384,13.824c-0.064,5.824,2.976,11.168,7.968,14.048l45.088,26.112L7.969,193.556c-4.992,2.912-8.032,8.288-7.968,14.048c0.064,5.792,3.264,11.072,8.32,13.856l55.68,30.56v131.392c0,5.856,3.2,11.264,8.352,14.016l176,96c0.192,0.096,0.416,0.064,0.608,0.16c2.144,1.088,4.48,1.824,7.04,1.824c2.56,0,4.896-0.736,7.072-1.824c0.192-0.096,0.416-0.064,0.608-0.16l176-96c5.12-2.752,8.32-8.16,8.32-14.016V251.38l54.496-29.92c5.056-2.784,8.256-8.064,8.32-13.856C510.881,201.844,507.841,196.468,502.849,193.588z M319.073,49.78l143.104,78.016l-36.32,21.024L287.265,68.276L319.073,49.78z M48.673,127.828l143.104-78.016l31.808,18.496L84.961,148.82L48.673,127.828z M48.545,206.996l36.416-21.152l138.496,80.192l-32.864,19.008L48.545,206.996z M240.001,452.468l-144-78.56v-104.32l87.136,47.84c2.368,1.312,5.024,1.984,7.68,1.984c2.784,0,5.536-0.704,8.032-2.144l41.152-23.84V452.468z M255.393,247.508l-138.56-80.224l138.56-80.512l138.56,80.512L255.393,247.508z M416.001,373.908l-144,78.56V294.1l39.968,23.136c2.496,1.472,5.248,2.176,8.032,2.176c2.656,0,5.312-0.672,7.68-1.984l88.32-48.48V373.908z M320.225,285.012l-32.864-19.008l138.496-80.192l36.416,21.184L320.225,285.012z"/></g></g></svg><?php
											print("<h5>Fast Shipping &amp; Delivery</h5>");
											print("<span>UK &amp; Europe</span>");
										print("</li>");
									/*	print("<li>");
											?><svg id="Capa_1" enable-background="new 0 0 512.004 512.004" height="512" viewBox="0 0 512.004 512.004" width="512" xmlns="http://www.w3.org/2000/svg"><g><path d="m499.6 303.545c-16.525-18.589-45.126-20.307-63.753-3.827l-24.825 21.96v-141.678h5c8.284 0 15-6.716 15-15v-70c0-8.284-6.716-15-15-15h-71.035c3.848-7.507 6.035-16 6.035-25 0-30.327-24.673-55-55-55-15.75 0-29.964 6.665-40 17.31-10.036-10.645-24.25-17.31-40-17.31-30.327 0-55 24.673-55 55 0 9 2.187 17.493 6.035 25h-71.035c-8.284 0-15 6.716-15 15v70c0 8.284 6.716 15 15 15h5v113.561c-9.175 4.448-17.854 10.063-25.839 16.798l-11.978 10.101-53.092 22.752c-3.656 1.567-6.541 4.522-8.019 8.216-1.478 3.693-1.427 7.823.14 11.479l60 140c2.438 5.688 7.975 9.095 13.794 9.095 1.971 0 3.975-.391 5.902-1.217l52.316-22.42h198.125c18.527 0 36.323-6.817 50.107-19.195l113.537-101.944c18.458-16.573 20.066-45.141 3.585-63.681zm-118.578 44.671-20.458 18.097c.076-1.068.115-2.148.115-3.236 0-24.813-20.187-45-45.025-45l-44.632.074v-138.151h110zm20-198.216h-130v-40h130zm-105-120c13.785 0 25 11.215 25 25s-11.215 25-25 25h-25v-25c0-13.785 11.215-25 25-25zm-105 25c0-13.785 11.215-25 25-25s25 11.215 25 25v25h-25c-13.785 0-25-11.215-25-25zm-80 55h130v40h-130zm20 70h110v136.673c-20.747-20.447-47.806-32.789-77.132-34.998-11.139-.838-22.174-.183-32.868 1.89zm-47.122 297.304-48.182-112.425 27.572-11.816 48.182 112.425zm392.072-132.398-113.536 101.945c-8.271 7.427-18.948 11.517-30.064 11.517h-191.313l-51.727-120.698 5.192-4.379c8.113-6.843 17.171-12.159 26.816-15.852.205-.078.413-.149.614-.235 12.509-4.687 25.989-6.645 39.683-5.614 24.498 1.845 46.91 13.035 63.106 31.508 2.848 3.249 6.958 5.111 11.278 5.111h.025l19.337-.032c.213.009.423.032.639.032.23 0 .454-.024.682-.034l58.975-.098c8.271 0 15 6.729 15 15s-6.729 15-15 15h-103.234c-8.284 0-15 6.716-15 15s6.716 15 15 15h129.138c10.992 0 21.58-4.011 29.815-11.295l84.326-74.594c6.271-5.546 15.893-4.967 21.454 1.288 5.546 6.239 5.005 15.853-1.206 21.43z"/></g></svg><?php
											print("<h5>Free Gift Package</h5>");
											print("<span>Order 6+ Packets of Seeds</span>");
										print("</li>");*/
										print("<li>");
											?><svg height="511pt" viewBox="-31 1 511 511.99943" width="511pt" xmlns="http://www.w3.org/2000/svg"><path d="m.5 497c0 8.285156 6.714844 15 15 15h419.964844c8.28125 0 14.996094-6.714844 14.996094-15 0-8.28125-6.714844-15-14.996094-15h-76.589844c19.480469-19.070312 31.59375-45.640625 31.59375-74.992188 0-8.28125-6.71875-15-15-15h-59.996094c-29.351562 0-55.921875 12.113282-74.992187 31.59375v-48.210937c27.5 12.878906 63.691406-.730469 72.445312-33.988281 38.765625 10.453125 74.390625-25.113282 63.933594-63.921875 38.917969-10.238281 51.949219-59.050781 23.417969-87.453125 28.4375-28.316406 15.667968-77.15625-23.421875-87.445313 10.457031-38.789062-25.140625-74.402343-63.949219-63.925781-10.277344-38.75-59.078125-51.707031-87.417969-23.359375-28.257812-28.277344-77.128906-15.546875-87.441406 23.351563-38.746094-10.457032-74.417969 25.082031-63.945313 63.925781-39.109374 10.285156-51.855468 59.148437-23.425781 87.453125-28.53125 28.40625-15.523437 77.203125 23.410157 87.453125-10.46875 38.8125 25.148437 74.414062 63.949218 63.941406 8.746094 33.222656 44.871094 46.902344 72.449219 33.988281v48.191406c-19.070313-19.480468-45.640625-31.59375-74.992187-31.59375h-59.996094c-8.285156 0-15 6.71875-15 15 0 29.351563 12.113281 55.921876 31.59375 74.992188h-76.589844c-8.285156 0-15 6.71875-15 15zm314.972656-75.003906h43.488282c-6.96875 34.1875-37.269532 60.003906-73.484376 60.003906h-43.488281c6.96875-34.1875 37.269531-60.003906 73.484375-60.003906zm-164.984375-231.96875c0-41.351563 33.640625-74.996094 74.992188-74.996094 41.351562 0 74.992187 33.644531 74.992187 74.996094 0 41.351562-33.640625 74.992187-74.992187 74.992187-41.351563 0-74.992188-33.640625-74.992188-74.992187zm118.28125 159.238281-.832031.222656c-13.816406 3.675781-27.457031-6.773437-27.457031-21.046875v-34.5c8.34375-1.199218 16.375-3.378906 23.96875-6.425781l17.226562 29.839844s0 0 .003907.003906l.019531.035156c7.128906 12.351563.652343 28.179688-12.929688 31.871094zm73.644531-42.894531-.597656.597656c-10.078125 10.027344-27.050781 7.707031-34.164062-4.613281l-17.246094-29.875c6.507812-5.132813 12.394531-11.023438 17.527344-17.53125l29.898437 17.261719c12.3125 7.128906 14.605469 24.085937 4.582031 34.160156zm21.484376-101.347656c14.25 0 24.726562 13.636718 21.054687 27.4375l-.230469.855468c-3.695312 13.601563-19.539062 20.046875-31.875 12.925782l-.03125-.015626c-.003906-.003906-.003906-.003906-.007812-.007812l-29.835938-17.226562c3.046875-7.59375 5.226563-15.625 6.425782-23.96875zm20.820312-58.285157.222656.828125c3.679688 13.820313-6.769531 27.460938-21.046875 27.460938h-34.5c-1.199219-8.34375-3.378906-16.375-6.425781-23.96875l29.839844-17.226563c.003906 0 .007812-.003906.011718-.007812l.027344-.015625c12.378906-7.144532 28.1875-.628906 31.871094 12.929687zm-42.894531-73.648437.601562.601562c10 10.054688 7.730469 27.027344-4.601562 34.15625l-29.886719 17.253906c-5.136719-6.507812-11.023438-12.394531-17.53125-17.527343l17.246094-29.875c7.117187-12.324219 24.085937-14.644531 34.171875-4.609375zm-73.886719-42.335938.832031.222656c13.628907 3.703126 20.03125 19.375 12.925781 31.683594l-17.246093 29.875c-7.597657-3.046875-15.625-5.226562-23.96875-6.425781v-34.496094c0-14.203125 13.578125-24.554687 27.457031-20.859375zm-85.730469.203125.820313-.21875c13.867187-3.683593 27.453125 6.644531 27.453125 20.863281v34.507813c-8.339844 1.199219-16.371094 3.382813-23.964844 6.425781l-17.257813-29.882812c-7.097656-12.296875-.722656-27.988282 12.949219-31.695313zm-73.664062 42.714844.601562-.597656c10.054688-10 27.035157-7.726563 34.164063 4.617187l17.25 29.882813c-6.507813 5.132812-12.394532 11.019531-17.53125 17.527343l-29.886719-17.253906c-12.320313-7.117187-14.632813-24.089844-4.597656-34.175781zm-42.535157 73.886719.21875-.816406c3.6875-13.609376 19.535157-20.0625 31.878907-12.9375l.039062.023437s.003907 0 .003907.003906l29.84375 17.230469c-3.046876 7.59375-5.226563 15.621094-6.425782 23.964844h-34.507812c-14.257813 0-24.726563-13.617188-21.050782-27.46875zm.21875 85.75-.222656-.828125c-3.675781-13.839844 6.804688-27.457031 21.054688-27.457031h34.507812c1.199219 8.34375 3.378906 16.375 6.425782 23.96875l-29.859376 17.242187c-.003906 0-.007812.003906-.011718.003906l-.015625.007813c-12.355469 7.136718-28.195313.65625-31.878907-12.9375zm42.890626 73.679687-.605469-.605469c-10.003907-10.054687-7.730469-27.03125 4.609375-34.167968l29.90625-17.265625c5.132812 6.507812 11.019531 12.394531 17.527344 17.53125l-17.257813 29.894531c-7.121094 12.335938-24.097656 14.640625-34.179687 4.613281zm73.917968 42.515625-.839844-.222656c-13.605468-3.691406-20.0625-19.535156-12.9375-31.878906l17.257813-29.886719c7.59375 3.046875 15.621094 5.226563 23.964844 6.425781v34.507813c0 14.234375-13.605469 24.730469-27.445313 21.054687zm-91.035156 72.503906h43.488281c36.214844 0 66.515625 25.804688 73.488281 59.992188h-43.492187c-36.214844 0-66.515625-25.804688-73.484375-59.992188zm0 0"/></svg><?php
											print("<h5>Highest Quality Seeds</h5>");
											print("<span>For Perfect Flowers</span>");
										print("</li>");
										print("<li>");
											?><svg height="511pt" viewBox="-28 1 511 511.9995" width="511pt" xmlns="http://www.w3.org/2000/svg"><path d="m408.101562 404.441406c-26.210937-23.058594-61.871093-40.769531-101.402343-50.519531l-.296875-31.535156c5.035156-5.59375 9.421875-11.636719 13.128906-18.03125 42.820312-.390625 77.519531-30.597657 77.519531-67.6875 0-18.609375-8.453125-35.921875-23.808593-48.75 1.015624-18.3125 2.515624-60.449219-3.480469-82.699219-7.957031-29.527344-25.320313-55.457031-48.894531-73.003906-27.742188-20.65625-59.589844-31.796875-92.097657-32.21093775-.109375-.00390625-.253906-.00390625-.363281-.00390625-.320312.00390625-1.554688.00390625-1.863281 0-.121094 0-.25 0-.375 0-32.507813.417969-64.355469 11.558594-92.097657 32.214844-24.371093 18.144531-41.992187 45.058594-49.613281 75.78125-5.28125 21.289062-2.417969 59.464844-.632812 77.398437-16.679688 12.425781-27.269531 30.792969-27.269531 51.273438 0 37.332031 35.148437 67.707031 78.355468 67.707031h1.171875c3.820313 6.582031 8.363281 12.789062 13.585938 18.523438v30.835937c-39.703125 9.675781-75.527344 27.347656-101.871094 50.410156-30.5 26.699219-47.296875 59.675781-47.296875 92.855469 0 8.285156 6.714844 15 15 15h424.605469c8.28125 0 15-6.714844 15-15 0-33.023438-16.691407-65.894531-47.003907-92.558594zm-234.339843-275.128906c10.703125-6.019531 26.429687-3.328125 46.730469 7.996094 4.785156 2.671875 10.644531 2.519531 15.285156-.394532 17.722656-11.125 31.90625-14.191406 42.160156-9.109374 18.207031 9.023437 25.035156 42.792968 26.011719 52.292968v70.363282c0 8.242187-1.300781 16.285156-3.757813 23.914062h-52.386718c-8.285157 0-15 6.714844-15 15 0 8.28125 6.714843 15 15 15h34.367187c-.175781.179688-.339844.371094-.519531.550781-14.328125 14.550781-33.441406 22.742188-53.816406 23.0625h-.066407c-20.378906-.320312-39.488281-8.511719-53.816406-23.0625-6.222656-6.320312-11.230469-13.53125-14.914063-21.335937-.070312-.164063-.152343-.324219-.226562-.484375-4.683594-10.101563-7.15625-21.1875-7.15625-32.644531v-70.773438c.421875-10.265625 5.332031-40.941406 22.105469-50.375zm130.1875 50.433594c-.003907-.15625 0-.261719 0-.316406zm-124.277344 166.273437c14.601563 7.585938 30.867187 11.722657 47.765625 11.96875h.214844.300781.21875c17.160156-.25 33.667969-4.519531 48.449219-12.332031l.191406 20.292969c-.003906 8.660156-9.113281 20.125-24.996094 31.453125-8.808594 6.28125-17.773437 11.179687-23.445312 14.050781-21.542969-10.734375-48.699219-30.476563-48.699219-45.683594zm151.917969-73.015625c1.554687-7.332031 2.359375-14.875 2.359375-22.539062v-49.558594c5.699219 1.464844 10.9375 3.742188 15.476562 6.660156.128907.082032.253907.167969.382813.25 10.53125 6.910156 17.246094 17.277344 17.246094 28.851563-.003907 17.308593-15.042969 31.921875-35.464844 36.335937zm-218.011719-157.785156c5.953125-24 19.59375-44.933594 38.410156-58.941406 22.597657-16.828125 48.335938-25.910156 74.433594-26.277344.523437.003906 1.574219.003906 2.097656 0 26.097657.367188 51.835938 9.449219 74.433594 26.277344 18.191406 13.542968 31.628906 33.695312 37.84375 56.75 3.550781 13.175781 3.703125 39.335937 3.070313 59.484375-3.621094-1.046875-7.339844-1.859375-11.132813-2.4375-2.878906-17.082031-12.070313-52.953125-39.351563-68.039063-18.765624-10.371094-40.933593-8.675781-65.992187 5.027344-27.15625-13.285156-50.40625-14.4375-69.195313-3.402344-26 15.265625-33.535156 49.421875-35.6875 66.15625-3.328124.460938-6.585937 1.105469-9.757812 1.917969-1.453125-18.816406-2.359375-43.679687.828125-56.515625zm-27.023437 121.449219c0-12.402344 7.71875-23.421875 19.605468-30.296875.539063-.257813 1.058594-.550782 1.558594-.867188 4.21875-2.246094 8.914062-3.972656 13.9375-5.09375v50.050782c0 7.808593.832031 15.492187 2.445312 22.953124-21.476562-3.839843-37.546874-18.851562-37.546874-36.746093zm-54.445313 245.332031c9.167969-42.863281 56.765625-81.699219 121.351563-98.300781 14.042968 33.171875 62.609374 55.316406 68.972656 58.097656 3.832031 1.671875 8.1875 1.671875 12.019531 0 6.320313-2.765625 54.566406-24.765625 68.5625-57.871094 64.097656 16.722657 111.332031 55.453125 120.480469 98.074219zm0 0"/></svg><?php
											print("<h5>Expert Online Support</h5>");
											print("<span>Professional Advice</span>");
										print("</li>");
									print("</ul>");
								print("</div>");
								
								// New Products
								$strdbsql = "SELECT stock.groupID FROM stock 
								LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID
								INNER JOIN stock_category_relations ON stock.groupID = stock_category_relations.stockID
								WHERE stock_group_information.productType = 0 AND stock_group_information.enabled = 1 AND stock.status = 1 AND stock_category_relations.categoryID = 37 
								GROUP BY stock.groupID 
								LIMIT 10";
								$mother = query($conn,$strdbsql,"multi");
								
								// Popular Products
								$strdbsql = "SELECT stock.groupID FROM stock 
								LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID
								INNER JOIN stock_category_relations ON stock.groupID = stock_category_relations.stockID
								WHERE stock_group_information.productType = 0 AND stock_group_information.enabled = 1 AND stock.status = 1 AND stock_category_relations.categoryID = 35 
								GROUP BY stock.groupID 
								LIMIT 10";
								$popular = query($conn,$strdbsql,"multi");
								
								if(count($popular) > 0 || count($mother) > 0) {
									print("<div class='tabs-wrapper'>");
										
										print("<ul class='tabs small'>");
										//	if(count($popular) > 0) print("<li class='tab'><a class='active' href='#category1'>Mother's Day</a></li>");
											if(count($mother) > 0) print("<li class='tab'><a href='#category2'>Popular Picks</a></li>");
										//	if($accountid > 0) print("<li class='tab'><a href='#category3'>My Wishlist</a></li>");
											print("<li class='indicator'></li>");
										print("</ul>");
										
									/*	if(count($mother) > 0) {
											print("<div id='category1' class='tab-content'>");
												print("<div class='products'>");
													print("<div id='slick-category1' class='slick-products'>");
														foreach($mother AS $row) {
															print("<div>");
																$products = new ProductMethods($conn);
																$products->id = $row['groupID'];
																$product = $products->productHTML();
																print($product);
															print("</div>");
														}
													print("</div>");
												print("</div>");
											print("</div>");
										}*/
										
										if(count($popular) > 0) {
											print("<div id='category2' class='tab-content'>");
												print("<div class='products'>");
													print("<div id='slick-category1' class='slick-products'>");
														foreach($popular AS $row) {
															print("<div>");
																$products = new ProductMethods($conn);
																$products->id = $row['groupID'];
																$product = $products->productHTML();
																print($product);
															print("</div>");
														}
													print("</div>");
												print("</div>");
											print("</div>");
										}
										
									print("</div>");
								}
									
						print("</div>");
					
					print("</div>");
				print("</main>");
				
				print($pageData['pageContent']);
				fnGetSiteBlocks($pageData['recordID'],"pageID","Side",$conn);
				fnGetSiteBlocks($pageData['recordID'],"pageID","Below",$conn);
				
			break;
			case "shop":
					
				print("<main>");
			
				if(!isset($_REQUEST['product'])) {
				
					fnGetSiteBlocks($pageData['recordID'],"pageID","Above",$conn);
					
					print("<div class='container'>");
						
						print($pageData['pageContent']);
						
						fnGetSiteBlocks($pageData['recordID'],"pageID","Side",$conn);						
						fnGetSiteBlocks($pageData['recordID'],"pageID","Inside",$conn);
						
					print("</div>");
				
			
				} else {
					
					fnGetSiteBlocks($pageData['recordID'],"pageID","Above",$conn);
					
					print("<div class='container'>");
						include("includes/products.php");
					print("</div>");
					
				}
				
				print("</main>");
				
			break;
			case "basket";

			/*	if($strPage == "basket" && $basketcount > 0 && !isset($_COOKIE['exit'])) {
					print("<div class='detector' data-trigger='modal-exit' onmouseover='checkoutExit(this);'></div>");
					print("<div id='modal-exit' class='modal small center image offer'>");
						print("<div class='modal-content'>");
							print("<h4 class='h3'>Before you go! Get 10% off when you use the code:</h4>");
							print("<h5 class='code'>SPRING10</h5>");
						print("</div>");
						print("<div class='modal-under'>");
							print("<span>Not to be used in conjunction with any other offer.</span>");
						print("</div>");
					print("</div>");
				}*/
			
				print("<main ".($basketcount > 0 ? "" : "class='large'").">");
				//	include("includes/breadcrumb.php");
					print("<div class='container ".($basketcount > 0 ? "" : "center")."'>");
						include("includes/basket.php");
					print("</div>");
				print("</main>");
			
			break;
			case "confirmation";
			
				print("<main>");
					include("includes/breadcrumb.php");
					print("<div class='container'>");
						include("includes/confirmation.php");
					print("</div>");
				print("</main>");
			
			break;
			case "testemails";
			
				print("<main>");
					print("<div class='container'>");
						resendconfirmation($conn,5);
					print("</div>");
				print("</main>");
			
			break;
			case "account":
			
				print("<main>");
					include("includes/breadcrumb.php");
					print("<div class='container'>");
						include("includes/account.php");
					print("</div>");
				print("</main>");
			
			break;
			case "register":
			
				print("<main>");
					include("includes/breadcrumb.php");
					print("<div class='container'>");
						include("includes/register.php");
					print("</div>");
				print("</main>");
			
			break;
			case "forgot-password":
			
				print("<main>");
					include("includes/breadcrumb.php");
					print("<div class='container'>");
						include("includes/forgot-password.php");
					print("</div>");
				print("</main>");
			
			break;
			case "404":
			
				print("<main class='large'>");
					print("<div class='container center'>");
						print("<div class='row'>");
							print("<div class='col s12'>");
								print("<h1>Oops! That Page Can't Be Found.</h1>");
								print("<p class='crop-bottom'>It looks like nothing was found at this location. Maybe try one of the links below or a search?</p>");
							print("</div>");
						print("</div>");
						print("<div class='row crop-bottom'>");
							print("<div class='col s12'>");
								print("<a href='/' class='btn btn-primary large'>Return to homepage</a>");
							print("</div>");
						print("</div>");
					print("</div>");
				print("</main>");
			
			break;
			default:
			
				print("<main>");
					fnGetSiteBlocks($pageData['recordID'],"pageID","Above",$conn);
					print("<div class='container'>");
						print("<div class='row crop-bottom'>");
							if($sideData['total']) {
								print("<div class='col s12 m12 l3'>");
									print("<div class='sidebar'>");								
										fnGetSiteBlocks($pageData['recordID'],"pageID","Side",$conn);
									print("</div>");
								print("</div>");
								print("<div class='col s12 m12 l9'>");							
									print(\ForceUTF8\Encoding::toUTF8($pageData['pageContent']));								
									fnGetSiteBlocks($pageData['recordID'],"pageID","Inside",$conn);
								print("</div>");
							} else {
								print("<div class='col s12'>");							
									print(\ForceUTF8\Encoding::toUTF8($pageData['pageContent']));								
									fnGetSiteBlocks($pageData['recordID'],"pageID","Inside",$conn);
								print("</div>");
							}
						print("</div>");
					print("</div>");
				print("</main>");
				
			break;
			
		}

		include("includes/footer.php");
	
		?>
		
		<!--Import jQuery before materialize.js-->
		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="/js/jquery.cookie.js"></script>
		<!--<script src="/js/materialize.min.js"></script>-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
		
		<!--Import slick.js-->
		<script src="/js/slick.min.js"></script>
		
		<!--Import init.js-->
		<script src="/js/initial.min.js"></script>
		
		<?php
		
		if($strPage == "basket") {
		
			?>
		
			<!--Import stripe.js-->
			<script src="https://js.stripe.com/v3/"></script>
			<script>
			var stripe = Stripe('<?php print($stripeClient); ?>');
			</script>
			<script src="/js/stripe.min.js"></script>
		
			<!--Import paypal-->
			<script src="https://www.paypalobjects.com/api/checkout.js"></script>
			<script>
			var paypalType = "<?php print($paypalType); ?>";
			</script>
			<script src="/js/paypal.js"></script>
		
			<?php
		
		}
		
		?>
		
		<!--Import custom.js-->
		<script src="/js/custom.js?v=1.1.2"></script>
		
		<!--Import jplist.js-->
		<script src="/js/jplist.core.min.js"></script>
		<script src="/js/jplist.core-ajax.min.js"></script>

		<script src="/js/jplist.bootstrap-filter-dropdown.min.js"></script>
		<script src="/js/jplist.filter-dropdown-bundle.min.js"></script>
		<script src="/js/jplist.filter-toggle-bundle.min.js"></script>
		<script src="/js/jplist.textbox-filter.min.js"></script>
		<script src="/js/jplist.bootstrap-pagination-bundle.min.js"></script>
		<script src="/js/jplist.bootstrap-sort-dropdown.min.js"></script>
		<script src="/js/jplist.jquery-ui-bundle.min.js"></script>
		
		<!--Import stars-->
		<script src='/js/jquery.barrating.min.js'></script>
		<script src='/js/examples.min.js'></script>
		
		<!--Import Masonry-->
		<!--<script src="/js/masonry.pkgd.min.js"></script>-->
		<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
		
		<!--Import nice select-->
		<script src='/js/jquery.nice-select.min.js'></script>

		<?php
 
		// Location Modal
		if($_COOKIE['locationCheck'] == "fail") {
			print("<div id='modal-location' class='modal medium'>");
				print("<div class='modal-content' style='padding:50px 80px;background-image:url(/images/elements/american-corner-small.png), url(/images/elements/uk-eu-small.png); background-position:top left, top right;background-repeat:no-repeat, no-repeat;'>");
					print("<h4 class='h3'>Are you on the best site for you?</h4>");
					print("<p>You are currently viewing our UK/EU web site which may not ship to you current location.</p>");
					print("<p>Would you like to be redirected to our US site which ships throughout the rest of the world?</p>");
				print("</div>");
				print("<div class='modal-footer'>");
					print("<span><a href='https://www.sunflowerselections.com/' class='left'>Go to USA/International Site</a></span> ");
					print("<span><a href='#' class='modal-close right'>Continue to UK/European site</a></span>");
				print("</div>");
			print("</div>");
	
			?>
	
			<script type='text/javascript'>
			var loc;
			var instances;
			$(document).ready(function() {
				var options = {
					dismissible:false,
					onCloseStart: function() {
						var d = new Date();
						d.setTime(d.getTime() + (3*60*60*1000));
						var expires = "expires="+ d.toUTCString();
						document.cookie = "locationCheck=bypass";
					}
				};
				var elems = document.querySelectorAll('#modal-location');
				instances = M.Modal.init(elems, options);
				instances[0].open();
			});
			</script>
	
			<?php
		
		}
		
	print("</body>");
	
	print("</html>");
	
?>