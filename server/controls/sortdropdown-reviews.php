<?php

	/**
	* get sort dropdown control query
	* @param {Object} $status
	* @return {string} - a part of order by expression
	* sort example
	* {
    *     "action": "sort",
    *     "name": "sort",
    *     "type": "drop-down",
    *     "data": {
    *         "path": ".like",
    *         "type": "number",
    *         "order": "asc",
    *         "dateTimeFormat": "{month}/{day}/{year}"
    *     },
    *     "cookies": true
    * }
	*/
	function bootsortdropdown($status){
		
		$query = "";
		$data = $status->data;
		$order = "desc";
		
		if(isset($data) && isset($data->path)){
		
			switch($data->path){
				default:
				case "any":
					$query = "recordID";
					break;
				case ".recent":
					$query = "dateReview";
					break;
				case ".stars":
					$query = "numStars";
					break;
				
			}
			
			if(isset($data->order)) $order = strtolower($data->order);
			
			$order = ($order == "desc") ? "desc" : "asc";
			
			if($query) $query = $query . " " . $order;

		}
		
		return $query;
	}
?>	