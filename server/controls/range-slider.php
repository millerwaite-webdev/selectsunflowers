<?php

    /**
    * get partial model
    * @param {string} $query
    * @param {number} $param
    */
    function getPartialModel($query, $param){
        
        $model = new FilterResultModel();
        
        $model->query = $query;
        $model->param = $param;
        
        return $model;
    }

	/**
	* get jQuery UI range slider control query
	* @param {Object} $status
	* @return {Object}
	* status example
	* {
        "action": "filter",
        "name": "range-slider-likes",
        "type": "range-slider",
        "data": {
            "path": ".like",
            "type": "number",
            "filterType": "range",
            "min": 0,
            "max": 350,
            "prev": 0,
            "next": 350
        },
        "inStorage": true,
        "inAnimation": true,
        "isAnimateToTop": false,
        "inDeepLinking": true,
        "initialIndex": 0
    }
	*/
	function rangeslider($status){
		
		$data = $status->data;
		$result = null;
		
		if(($data->filterType == "range") && ($data->type == "number") && isset($data->path, $data->prev, $data->next)){
		
			switch($data->path){
			
				case ".bedrooms":{
                    
                    $result = new FilterResultCollection('and');
                    
                    array_push($result->filterResults, getPartialModel(" tbl_residential.fld_propertyBedrooms >= ? ", "$data->prev"));
                    array_push($result->filterResults, getPartialModel(" tbl_residential.fld_propertyBedrooms <= ? ", "$data->next"));
                    
					break;
				}
				
				case ".price-sales":{
        
                    $result = new FilterResultCollection('and');
                    
                    array_push($result->filterResults, getPartialModel(" tbl_residentialSales.fld_price >= ? ", "$data->prev"));
                    array_push($result->filterResults, getPartialModel(" tbl_residentialSales.fld_price <= ? ", "$data->next"));
					break;
				}
				case ".price-rental":{
        
                    $result = new FilterResultCollection('and');
                    
                    array_push($result->filterResults, getPartialModel(" tbl_residentialLet.fld_rent >= ? ", "$data->prev"));
                    array_push($result->filterResults, getPartialModel(" tbl_residentialLet.fld_rent <= ? ", "$data->next"));
					break;
				}
				case ".price-commercial":{
        
                    $result = new FilterResultCollection('and');
                    
                    array_push($result->filterResults, getPartialModel(" (tbl_commercial.fld_priceTo+tbl_commercial.fld_rentTo) >= ? ", "$data->prev"));
                    array_push($result->filterResults, getPartialModel(" (tbl_commercial.fld_priceTo+tbl_commercial.fld_rentTo) <= ? ", "$data->next"));
					break;
				}
				
			}
		}
        
		return $result;
	}
?>	