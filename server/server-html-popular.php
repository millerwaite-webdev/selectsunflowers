<?php
	
	//added standard php/mysql config file with host, user and password info
	require $_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php";
	
	//models and collections
	require "domain/models/filter-result-model.php";
	require "domain/collection/filter-result-collection.php";
	
	//domain
	require "domain/action.php";
	require "domain/sorting.php";
	require "domain/pagination-popular.php";
	require "domain/filtering.php";
	require "domain/server-popular.php";
	
	//controls
	require "controls/sortdropdown-popular.php";
	require "controls/textbox.php";
	require "controls/checkboxgroupfilter.php";
	require "controls/filterdropdown.php";
	require "controls/filterselect.php";
	require "controls/range-slider.php";
	require "controls/range-filter.php";
	require "controls/button-text-filter.php";
		
	class jPListHTML extends jPListServer{
			
		/**
		* get html for one item
		* @param {Object} $item
		* @return {string} html
		*/
		
		private function getHTML($row){
		
			$conn = connect();
		
			$accountid = $_SESSION['account'];
			
			$html = "";
		
			//	PRODUCT CONTAINER
			$html .= "<div class='product'>";
				
				//	PRODUCT FAVOURITE
				$strdbsql = "SELECT * FROM stock_favourites WHERE stockCode = :product AND customerID = :customer";
				$result2 = query($conn,$strdbsql,"single",array("product"=>$row['recordID'],"customer"=>$accountid));
				
				if($result2) $html .= "<button class='favourite-toggle active' data-id='".$row['recordID']."'><i class='fas fa-heart'></i></button>";
				else $html .= "<button class='favourite-toggle' data-id='".$row['recordID']."'><i class='fas fa-heart'></i></button>";
				
				//	GET ALL IMAGES FOR CURRENT PRODUCT
				$strdbsql = "SELECT * FROM stock_images WHERE stockID = :recordID AND imageTypeID = 1 ORDER BY imageOrder LIMIT 1";
				$result2 = query($conn,$strdbsql,"single",array("recordID"=>$row['recordID']));
				
				// Image
				$html .= "<div class='product-image'>";
					$html .= "<a href='/wholesale/".str_replace(" ", "-", strtolower($row['stockCode']))."'>";
						
						// Stickers
						$strdbsql = "SELECT * FROM stock_category_relations 
						INNER JOIN category ON stock_category_relations.categoryID = category.recordID 
						INNER JOIN category_relations ON stock_category_relations.categoryID = category_relations.childID 
						WHERE stock_category_relations.stockID = :product AND contentMenuImage IS NOT NULL AND contentMenuImage <> '' AND showSticker = 1
						ORDER BY category_relations.menuOrder
						LIMIT 1";
						$result3 = query($conn,$strdbsql,"multi",array("product"=>$row['recordID']));
						
						foreach($result3 AS $row3) {
							if(!empty($row3['contentMenuImage'])) $html .= "<div class='sticker'><img src='/images/elements/".$row3['contentMenuImage']."' alt='".$row3['contentName']."'></div>";
						}
						
						// Image
						if($result2) $html .= "<img src='/images/products/main/".$result2['imageLink']."' alt='".$result2['description']."' />";
						else $html .= "<img src='/images/products/other/no-images.jpg' alt='".$row['stockGroup']."' />";
						
					$html .= "</a>";
				$html .= "</div>";
				
				//	PRODUCT DETAILS
				$html .= "<div class='product-content'>";

					// Title
					$html .= "<span class='title'><a href='/wholesale/".str_replace(" ", "-", strtolower($row['stockCode']))."'>".$row['stockGroup']."</a></span>";
					
					// Description
					$html .= "<span>".substr($row['stockDescription'], 0, 100)."</span>";
					
					// Reviews
					$strdbsql = "SELECT * FROM stock_reviews WHERE stockGroupID = :recordID AND shownOnSite = 1 ORDER BY dateReview";
					$result4 = query($conn,$strdbsql,"multi",array("recordID"=>$row['recordID']));
					
					foreach($result4 AS $row4) {
						$total += $row4['numStars'];
					}
					
					if(count($result4) > 0) $average = $total / count($result4);
					else $average = 0;
					
					$html .= "<ul class='reviews'>";
						for($stars = 1; $stars <= $average; $stars++) {
							$html .= "<li class='filled'><i class='material-icons'>star</i></li>";
						}
						while($stars <= 5) {
							$html .= "<li><i class='material-icons'>star_border</i></li>";
							$stars++;
						}
						$html .= "<li><span>(".count($result4).")</span></li>";
					$html .= "</ul>";
					
					// Price
					$html .= "<span class='price'>";
						$html .= "&pound;";
						$html .= ($row['productType'] > 0 ? $row['productCost'] : $row['price']);
					$html .= "</span>";
					
					$html .= "<button class='btn btn-primary small right basket-add' data-id='".$row['recordID']."'>Add to Basket</button>";
					
					switch($row['unit']) {
						case "each":
							$html .= "<span class='unit'>(each)</span>";
							break;
						default:
							$html .= "<span class='unit'>(per ".$row['unit'].")</span>";
							break;
					}
					
					// Stickers
					$strdbsql = "SELECT * FROM stock_category_relations 
					INNER JOIN category ON stock_category_relations.categoryID = category.recordID 
					INNER JOIN category_relations ON stock_category_relations.categoryID = category_relations.childID 
					WHERE stock_category_relations.stockID = :product AND contentMenuImage IS NOT NULL AND contentMenuImage <> ''
					ORDER BY category_relations.menuOrder";
					$result5 = query($conn,$strdbsql,"multi",array("product"=>$row['recordID']));
					
					if(count($result5) > 0) {
						$html .= "<ul class='key'>";
							$i = 1;
							foreach($result5 AS $row5) {
								if(count($result5) > 1 && $i == 1) {
									// Skip first
								} else {
									if(!empty($row5['contentMenuImage'])) $html .= "<li><img title='".$row5['contentName']."' src='/images/elements/".$row5['contentMenuImage']."' alt='".$row5['contentName']."' /></li>";
								}
								$i++;
							}
						$html .= "</ul>";
					}
					
				$html .= "</div>";
			$html .= "</div>";

			return $html;
		
		}
		
		/**
		* get the whole html
		* @param {string} $itemsHtml - items html
		* @return {string} html
		*/
		private function getHTMLWrapper($itemsHtml){
			
			$html = "";
			
			$html .= "<div data-type='jplist-dataitem' data-count='" . $this->pagination->numberOfPages . "' class='box'>";
				$html .= $itemsHtml;
			$html .= "</div>";		
			
			return $html;
			
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			$html = "";
			
			try{
                parent::__construct();
				
				if(isset($this->statuses)){
					
					$items = $this->getData();
					
					if($items){
						foreach($items as $item){
							$html .= $this->getHTML($item);					
						}
					}
						
                    ob_clean();
                    
					//print html
					echo($this->getHTMLWrapper($html));
				}
				
				//close the database connection
				$this->db = NULL;
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
	
	/**
	* start
	*/
	new jPListHTML();
?>	