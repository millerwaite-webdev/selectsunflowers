<?php
	
	//added standard php/mysql config file with host, user and password info
	require $_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php";
	
	//models and collections
	require "domain/models/filter-result-model.php";
	require "domain/collection/filter-result-collection.php";
	
	//domain
	require "domain/action.php";
	require "domain/sorting.php";
	require "domain/pagination-reviews.php";
	require "domain/filtering.php";
	require "domain/server-reviews.php";
	
	//controls
	require "controls/sortdropdown-reviews.php";
	require "controls/textbox.php";
	require "controls/checkboxgroupfilter.php";
	require "controls/filterdropdown.php";
	require "controls/filterselect.php";
	require "controls/range-slider.php";
	require "controls/range-filter.php";
	require "controls/button-text-filter.php";
		
	class jPListHTML extends jPListServer{
			
		/**
		* get html for one item
		* @param {Object} $item
		* @return {string} html
		*/
		
		private function getHTML($row){
		
			$conn = connect();
			
			$html = "";
		
			//	Container
			$html .= "<div class='comment'>";
				
				//	Details
				$initials = explode(" ", $row['reviewerName']);
				$initials = substr($initials[0], 0, 1).substr($initials[1], 0, 1);
			
				$html .= "<img class='profile circle' data-name='".$initials."' alt='".$row['reviewerName']."' />";
								
				$html .= "<div class='message'>";
				
					if(!empty($row['title'])) $html .= "<h4>".$row['title']."</h4>";
				
					$html .= "<span class='date'>".date("F, j Y", $row['dateReview'])." at ".date("g:i a", $row['dateReview'])."</span>";
					
					if(!empty($row['description'])) $html .= "<p>".$row['description']."</p>";
					
					$strdbsql = "SELECT stock.*, stock_group_information.name AS stockName, stock_group_information.summary, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost, stock_units.description AS unit FROM stock 
					INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
					INNER JOIN stock_units ON stock.unit = stock_units.recordID 
					WHERE stock.recordID = :product";
					$result = query($conn,$strdbsql,"single",["product"=>$row['stockGroupID']]);
					
					if($result) $html .= "<a href='/wholesale/".str_replace(" ", "-", strtolower($result['stockCode']))."'><i class='fas fa-tag'></i><span>".$result['stockName']."</span></a>";
					
					$strdbsql = "SELECT * FROM stock_reviews WHERE recordID = :id AND shownOnSite = 1";
					$result = query($conn,$strdbsql,"single",["id"=>$row['recordID']]);
					
					if($result) {
						$html .= "<ul class='reviews'>";
							for($stars = 1; $stars <= $result['numStars']; $stars++) {
								$html .= "<li class='filled'><i class='material-icons'>star</i></li>";
							}
							while($stars <= 5) {
								$html .= "<li><i class='material-icons'>star_border</i></li>";
								$stars++;
							}
							$html .= "<li><span>(".$result['numStars'].")</span></li>";
						$html .= "</ul>";
					}
					
					if(!empty($row['response'])) $html .= "<button class='btn btn-blank small' data-id='response-".$row['recordID']."'><i class='material-icons right'>arrow_drop_down</i>View replies (1)</button>";
					
				$html .= "</div>";
			$html .= "</div>";
			
			if(!empty($row['response'])) {
				$html .= "<div class='comment response hide' id='response-".$row['recordID']."'>";
					$html .= "<img class='profile circle' data-name='SB' alt='' />";
					$html .= "<div class='message'>";
						$html.= "<h4>Response from Salad Bowl</h4>";
						$html .= "<span class='date'>".date("F, j Y", $row['dateResponse'])." at ".date("g:i a", $row['dateResponse'])."</span>";
						$html .= "<p>".$row['response']."</p>";
					$html .= "</div>";
				$html .= "</div>";
			}

			return $html;
		
		}
		
		/**
		* get the whole html
		* @param {string} $itemsHtml - items html
		* @return {string} html
		*/
		private function getHTMLWrapper($itemsHtml){
			
			$html = "";
			
			$html .= "<div data-type='jplist-dataitem' data-count='" . $this->pagination->numberOfPages . "' class='box'>";
				$html .= $itemsHtml;
			$html .= "</div>";		
			
			return $html;
			
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			$html = "";
			
			try{
                parent::__construct();
				
				if(isset($this->statuses)){
					
					$items = $this->getData();
					
					if($items){
						foreach($items as $item){
							$html .= $this->getHTML($item);					
						}
					}
						
                    ob_clean();
                    
					//print html
					echo($this->getHTMLWrapper($html));
				}
				
				//close the database connection
				$this->db = NULL;
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
	
	/**
	* start
	*/
	new jPListHTML();
?>	