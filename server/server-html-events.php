<?php
	
	//added standard php/mysql config file with host, user and password info
	require $_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php";
	
	//models and collections
	require "domain/models/filter-result-model.php";
	require "domain/collection/filter-result-collection.php";
	
	//domain
	require "domain/action.php";
	require "domain/sorting.php";
	require "domain/pagination-events.php";
	require "domain/filtering.php";
	require "domain/server-events.php";
	
	//controls
	require "controls/sortdropdown-events.php";
	require "controls/textbox.php";
	require "controls/checkboxgroupfilter.php";
	require "controls/filterdropdown.php";
	require "controls/filterselect.php";
	require "controls/range-slider.php";
	require "controls/range-filter.php";
	require "controls/button-text-filter.php";
		
	class jPListHTML extends jPListServer{
			
		/**
		* get html for one item
		* @param {Object} $item
		* @return {string} html
		*/
		
		private function getHTML($row){
		
			$conn = connect();
			
			$html = "";
		
			//	Container
			$html .= "<div class='event grid-item s12 m6'>";
				$html .= "<a href='/news/".$row['link']."'>";
				
					// Image
					$strdbsql = "SELECT * FROM site_news_images WHERE newsID = :article ORDER BY imageOrder LIMIT 1";
					$result = query($conn,$strdbsql,"single",["article"=>$row['recordID']]);
				
					$html .= "<div class='card'>";
						$html .= "<div class='card-content' ".($result ? "style='background-image:url(/images/news/".$result['imageRef']."' alt='".htmlspecialchars($row['title'], ENT_QUOTES).");'" : "").">";
							$html .= "<div class='wrapper'>";
									
								// Date
								$html .= "<span class='date'>".date("jS F Y", $row['date'])."</span>";
								
								// Title
								if(!empty($row['title'])) $html .= "<h3>".$row['title']."</h3>";
								
							$html .= "</div>";
						$html .= "</div>";
					$html .= "</div>";

				$html .= "</a>";
			$html .= "</div>";

			return $html;
		
		}
		
		/**
		* get the whole html
		* @param {string} $itemsHtml - items html
		* @return {string} html
		*/
		private function getHTMLWrapper($itemsHtml){
			
			$html = "";
			
			$html .= "<div data-type='jplist-dataitem' data-count='" . $this->pagination->numberOfPages . "' class='box'>";
				$html .= $itemsHtml;
			$html .= "</div>";		
			
			return $html;
			
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			$html = "";
			
			try{
                parent::__construct();
				
				if(isset($this->statuses)){
					
					$items = $this->getData();
					
					if($items){
						foreach($items as $item){
							$html .= $this->getHTML($item);					
						}
					}
						
                    ob_clean();
                    
					//print html
					echo($this->getHTMLWrapper($html));
				}
				
				//close the database connection
				$this->db = NULL;
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
	
	/**
	* start
	*/
	new jPListHTML();
?>	