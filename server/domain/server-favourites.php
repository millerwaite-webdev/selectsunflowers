<?php

	class jPListServer{
	
		// database instance
		protected $db;
		
		// sorting
		protected $sorting;
		
		// filter
		protected $filter;
		
		// pagination
		protected $pagination;
		
		// jplist statuses
		protected $statuses;
				
		// execute query and get data from db
		// @return {Object} data
		protected function getData(){
			
			$items = null;
			
			//init qury
			$query = "SELECT stock.*, stock_group_information.name AS stockGroup, stock_group_information.metaLink, stock_group_information.summary AS stockDescription, stock_group_information.productType, stock_group_information.productCost FROM stock 
			LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID
			INNER JOIN stock_favourites ON stock.groupID = stock_favourites.stockCode";

			if($this->filter->filterQuery){				
				$query .= " " . $this->filter->filterQuery . " ";
				$query .= " AND stock_favourites.customerID = :account AND stock_group_information.enabled = 1";
			} else {
				$query .= " WHERE stock_favourites.customerID = :account AND stock_group_information.enabled = 1";	
			}
			
			$query .= " GROUP BY stock.groupID";
            
			//get queries / fields				
			if($this->sorting->sortQuery){
				$query .= " " . $this->sorting->sortQuery . " ";
			}

			if($this->pagination->paginationQuery){
				$query .= " " . $this->pagination->paginationQuery . " ";
			}
			
			$items = query($this->db,$query,"multi",["account"=>$_SESSION['account']]);

			return $items;
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			try{
						
				//connect to database 
				$this->db = new PDO("mysql:host=" . HOST . ";dbname=" . DB, USER, PASS);			
				$this->statuses = $_POST["statuses"];
				
				if(isset($this->statuses)){
					
					//statuses => array
					$this->statuses = json_decode(urldecode($this->statuses));	
					
					$this->sorting = new Sorting($this->statuses);
					$this->filter = new Filter($this->statuses);
					$this->pagination = new Pagination($this->statuses, $this->filter, $this->db);															
				}
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
?>	