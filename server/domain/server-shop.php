<?php
	
	class jPListServer{
	
		// database instance
		protected $db;
		
		// sorting
		protected $sorting;
		
		// filter
		protected $filter;
		
		// pagination
		protected $pagination;
		
		// jplist statuses
		protected $statuses;
				
		// execute query and get data from db
		// @return {Object} data
		protected function getData(){
			
			$items = null;
			
			//init qury
			$query = "SELECT stock.*, stock_group_information.name AS stockGroup,stock_group_information.metaLink, stock_group_information.summary AS stockDescription, stock_group_information.productType, stock_group_information.productCost, stock_units.abbreviation AS unit FROM stock 
			LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID
			INNER JOIN stock_category_relations ON stock.groupID = stock_category_relations.stockID
			INNER JOIN category ON category.recordID = stock_category_relations.categoryID 
			INNER JOIN stock_units ON stock.unit = stock_units.recordID";
			
			if($this->filter->filterQuery) {				
				$query .= " " . $this->filter->filterQuery . " ";
				$query .= " AND stock_group_information.productType = 0 AND stock_group_information.enabled = 1 AND stock.status = 1";
			} else {
				$query .= " WHERE stock_group_information.productType = 0 AND stock_group_information.enabled = 1 AND stock.status = 1";
			}
			
			$query .= " GROUP BY stock.groupID ";
			
			//get queries / fields				
			if($this->sorting->sortQuery) $query .= " " . $this->sorting->sortQuery . " ";

			if($this->pagination->paginationQuery) $query .= " " . $this->pagination->paginationQuery . " ";
			
			if(count($this->filter->preparedParams) > 0){	
				$stmt = $this->db->prepare($query);
				$stmt->execute($this->filter->preparedParams);
				$items = $stmt->fetchAll();
			} else{
				$items = $this->db->query($query);
			}

			return $items;
			
		}
		
		// constructor
		public function __construct(){
			try {
						
				//connect to database 
				$this->db = new PDO("mysql:host=" . HOST . ";dbname=" . DB, USER, PASS);			
				$this->statuses = $_POST["statuses"];
				
				if(isset($this->statuses)){
					//statuses => array
					$this->statuses = json_decode(urldecode($this->statuses));	
					$this->sorting = new Sorting($this->statuses);
					$this->filter = new Filter($this->statuses);
					$this->pagination = new Pagination($this->statuses, $this->filter, $this->db);															
				}
				
			} catch(PDOException $ex) {
				print "Exception: " . $ex->getMessage();
			}
		}
	}
	
?>	