<?php
	class jPListServer{
	
		/**
		* database instance
		*/
		protected $db;
		
		/**
		* sorting
		*/
		protected $sorting;
		
		/**
		* filter
		*/
		protected $filter;
		
		/**
		* pagination
		*/
		protected $pagination;
		
		/**
		* jplist statuses
		*/
		protected $statuses;
				
		/**
		* execute query and get data from db
		* @return {Object} data
		*/
		protected function getData(){
			
			$items = null;
			
			//init qury
			$query = "SELECT site_news_events.*, site_news_categories.description AS category FROM site_news_events LEFT JOIN site_news_categories ON site_news_events.categoryID = site_news_categories.recordID";

			if($this->filter->filterQuery){				
				$query .= " " . $this->filter->filterQuery . " ";
				$query .= " AND site_news_events.enabled = 1";
			} else {
				$query .= " WHERE site_news_events.enabled = 1";	
			}
            
			//get queries / fields				
			if($this->sorting->sortQuery){
				$query .= " " . $this->sorting->sortQuery . " ";
			}
			
			error_log("Test: ".$query);
			
		//	$query .= " ORDER BY site_news_events.date DESC";	
			
			if(count($this->filter->preparedParams) > 0){	
                
				$stmt = $this->db->prepare($query);
				$stmt->execute($this->filter->preparedParams);
				$items = $stmt->fetchAll();
			}
			else{
				$items = $this->db->query($query);
			}

			return $items;
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			try{
						
				//connect to database 
				$this->db = new PDO("mysql:host=" . HOST . ";dbname=" . DB, USER, PASS);			
				$this->statuses = $_POST["statuses"];
				
				if(isset($this->statuses)){
					
					//statuses => array
					$this->statuses = json_decode(urldecode($this->statuses));	
					
					$this->sorting = new Sorting($this->statuses);
					$this->filter = new Filter($this->statuses);
					$this->pagination = new Pagination($this->statuses, $this->filter, $this->db);															
				}
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
?>	