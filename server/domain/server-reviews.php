<?php
	class jPListServer{
	
		/**
		* database instance
		*/
		protected $db;
		
		/**
		* sorting
		*/
		protected $sorting;
		
		/**
		* filter
		*/
		protected $filter;
		
		/**
		* pagination
		*/
		protected $pagination;
		
		/**
		* jplist statuses
		*/
		protected $statuses;
				
		/**
		* execute query and get data from db
		* @return {Object} data
		*/
		protected function getData(){
			
			$items = null;
			
			//init query
			$query = "SELECT * FROM stock_reviews WHERE shownOnSite = 1 AND memberID = :account";
			
			if($this->filter->filterQuery) $query .= " " . $this->filter->filterQuery . " ";
					
			if($this->sorting->sortQuery) $query .= " " . $this->sorting->sortQuery . " ";

			if($this->pagination->paginationQuery) $query .= " " . $this->pagination->paginationQuery . " ";
			
			$items = query($this->db,$query,"multi",["account"=>$_SESSION['account']]);

			return $items;

			return $items;
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			try{
						
				//connect to database 
				$this->db = new PDO("mysql:host=" . HOST . ";dbname=" . DB, USER, PASS);			
				$this->statuses = $_POST["statuses"];
				
				if(isset($this->statuses)){
					
					//statuses => array
					$this->statuses = json_decode(urldecode($this->statuses));	
					
					$this->sorting = new Sorting($this->statuses);
					$this->filter = new Filter($this->statuses);
					$this->pagination = new Pagination($this->statuses, $this->filter, $this->db);															
				}
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
?>	