<?php
	
	//added standard php/mysql config file with host, user and password info
	require $_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php";
	
	//models and collections
	require "domain/models/filter-result-model.php";
	require "domain/collection/filter-result-collection.php";
	
	//domain
	require "domain/action.php";
	require "domain/sorting.php";
	require "domain/pagination-saved.php";
	require "domain/filtering.php";
	require "domain/server-saved.php";
	
	//controls
	require "controls/sortdropdown-saved.php";
	require "controls/textbox.php";
	require "controls/checkboxgroupfilter.php";
	require "controls/filterdropdown.php";
	require "controls/filterselect.php";
	require "controls/range-slider.php";
	require "controls/range-filter.php";
	require "controls/button-text-filter.php";
		
	class jPListHTML extends jPListServer{
			
		/**
		* get html for one item
		* @param {Object} $item
		* @return {string} html
		*/
		
		private function getHTML($row){
		
			$conn = connect();
			
			$html = "";
		
			//	PRODUCT CONTAINER
			$html .= "<div class='product'>";

				// Get Items
				$strdbsql = "SELECT order_items.stockID, order_items.quantity, stock_images.imageLink AS image, stock_images.description AS alt FROM order_items 
				INNER JOIN stock_images ON order_items.stockID = stock_images.stockID 
				WHERE order_items.orderHeaderID = :order 
				GROUP BY stockID";
				$items = query($conn,$strdbsql,"multi",["order"=>$row['recordID']]);
				
				// Image
				$html .= "<div class='product-image'>";
					$html .= "<a href='/boxes/".str_replace(" ", "-", strtolower($row['stockCode']))."'>";
						
						$html .= "<div class='sticker'><div class='count'>".count($items)."</div></div>";
						
						// Image
						if($result2) $html .= "<img src='/images/products/main/".$result2['imageLink']."'>";
						else $html .= "<img src='/images/products/other/no-images.jpg'>";
						
					$html .= "</a>";
				$html .= "</div>";
				
				//	Details
				$html .= "<div class='product-content'>";

					// Title
					if(!empty($row['title'])) $html .= "<span class='title'>".$row['title']."</span>";
					else $html .= "<span class='title'>Order #".$row['basketHeaderID']."</span>";
					
					// Description
					$html .= "<span>".substr($row['description'], 0, 100)."</span>";
				
					$html .= "<div class='row crop-bottom'>";
						$html .= "<div class='col s4 left-align'>";
							$html .= "<span>Items (".count($items)."):</span>";
						$html .= "</div>";
						$html .= "<div class='col s8 right-align'>";
							$html .= "<span>Date Ordered: ".date("d/m/y", $row['timestampOrder'])."</span>";
						$html .= "</div>";
					$html .= "</div>";
					
					// Price
					$html .= "<span class='price'>&pound;".$row['amountTotal']."</span>";
					
					$html .= "<button class='btn btn-primary small right basket-add' data-id='".$row['recordID']."'>Add to Basket</button>";
					
				$html .= "</div>";
			$html .= "</div>";

			return $html;
		
		}
		
		/**
		* get the whole html
		* @param {string} $itemsHtml - items html
		* @return {string} html
		*/
		private function getHTMLWrapper($itemsHtml){
			
			$html = "";
			
			$html .= "<div data-type='jplist-dataitem' data-count='" . $this->pagination->numberOfPages . "' class='box'>";
				$html .= $itemsHtml;
			$html .= "</div>";		
			
			return $html;
			
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			$html = "";
			
			try{
                parent::__construct();
				
				if(isset($this->statuses)){
					
					$items = $this->getData();
					
					if($items){
						foreach($items as $item){
							$html .= $this->getHTML($item);					
						}
					}
						
                    ob_clean();
                    
					//print html
					echo($this->getHTMLWrapper($html));
				}
				
				//close the database connection
				$this->db = NULL;
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
	
	/**
	* start
	*/
	new jPListHTML();
?>	