<?php	
	
	// Define DB Functions
	function connect($driver = DRIVER, $username = USER, $password = PASS, $host = HOST, $db = DB) {
		try {
			// Define new PHP Data Object connection
			$conn = new PDO('' . $driver . ':host=' . $host . ';dbname=' . $db, $username, $password);
			// Sets error reporting level
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		} catch(PDOException $e) {
			// If failed, display error
			echo "ERROR: " . $e->getMessage();
		}
	}
	
	function bookConnect($driver = BOOKDRIVER, $username = BOOKUSER, $password = BOOKPASS, $host = BOOKHOST, $db = BOOKDB) {
		try {
			// Define new PHP Data Object connection
			$bookConn = new PDO(''.$driver.':host='.$host.';dbname='.$db, $username, $password);
			// Sets error reporting level
			$bookConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $bookConn;
		} catch(PDOException $e) {
			// If failed, display error
			echo "ERROR: " . $e->getMessage();
		}
	}
	
	function query($conn, $querystring, $querytype, $values = null) {
		if($conn) {
			// Prepares query
			$query = $conn->prepare($querystring);
			
			// Binds values and executes query
			$query->execute($values);
			
			switch ($querytype) {
				case "single":
					// Returns associative array of single result
					$result = $query->fetch(PDO::FETCH_ASSOC);
				break;

				case "multi":
					// Returns multidimensional associative array of all results
					$result = $query->fetchAll(PDO::FETCH_ASSOC);
				break;

				case "count":
					// Returns number of selected rows
					$result = $query->rowCount();
				break;

				case "insert":
					// Insert new row and return is fld_counter (auto increment) of the inserted line
					$result = $conn->lastInsertID();
				break;

				case "update":
					// Return number of rows updated by query
					$result = $query->rowCount();
				break;

				case "delete":
					// Return number of rows deleted by query
					$result = $query->rowCount();
				break;

				default:
					// Invalid query type
					$result = "Invalid !";
				break;
			}
			return $result;
		} else {
			return "Failure !";
		}
	}
	
	//	Compress given code
	function compressCss($buffer) {
		// Remove comments:
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		// Remove tabs, excessive spaces and newlines
		$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '   '), '', $buffer);
		$buffer = str_replace(array("{ ", " {"), '{', $buffer);
		$buffer = str_replace(array("} ", " }"), '}', $buffer);
		$buffer = str_replace(array("; ", " ;"), ';', $buffer);
		$buffer = str_replace(array(", ", " ,"), ',', $buffer);
		$buffer = str_replace(array(": ", " :"), ':', $buffer);
		return $buffer;
	}
	
	
	//	Get site blocks from admin
	function fnGetSiteBlocks($lookupID,$lookupType,$position,$conn){
			
		global $strsiteurl,$compName, $compPhone, $compMobile, $compFax, $compEmail, $compAddNumber, $compAddStreet, $compAddCity, $compAddCounty, $compAddCountry, $compAddPostcode, $pageID, $pageName, $pageContent, $pageLink, $pageImage, $pageParent, $socialLinks, $newsArticles, $services, $googLat, $googLng, $captchaKey, $accountid;
			
		if (isset($_REQUEST['page'])) $strPage = $_REQUEST['page']; else $strPage = "index";
		if (isset($_REQUEST['view'])) $strview = $_REQUEST['view']; else $strview = "";
			
		$strdbsql = "SELECT site_blocks.*, site_block_position.description AS includePos FROM site_block_relations INNER JOIN site_blocks ON site_block_relations.blockID = site_blocks.recordID INNER JOIN site_block_position ON site_block_relations.positionID = site_block_position.recordID WHERE site_block_relations.$lookupType = :lookupID AND site_block_position.description = :position ORDER BY site_block_relations.pageOrder";
		$returnType = "multi";
		$dataParam = array("lookupID"=>$lookupID,"position"=>$position);
		$blockRelations = query ($conn, $strdbsql, $returnType, $dataParam);
		
		foreach($blockRelations AS $relation) {
			if($relation['type'] == 1 && !isset($_REQUEST['product']) && !isset($_REQUEST['article'])) {
				$GLOBALS['boolSlideshow'] = true;
				
				$strdbsql = "SELECT * FROM site_gallery_images WHERE galleryID = :galleryID ORDER BY galleryOrder";
				$returnType = "multi";
				$dataParam = array("galleryID"=>$relation['galleryID']);
				$galleryImages = query ($conn, $strdbsql, $returnType, $dataParam);
				
				print("<div class='slider banner-slider'>");
					print("<div class='container'>");
						print("<ul class='slides'>");
							foreach($galleryImages AS $galleryImage) {
								print("<li class='decor-left'>");
									print("<img src='/images/gallery/".$galleryImage['imageLocation']."' alt='".$galleryImage['title']."' />");
									print("<div class='caption'>");
										print("<div class='wrapper decor-both' style='background-color:#FFFFFF;border:0;margin:0;width:100%;'>");
											print("<h1>".$galleryImage['title']."</h1>");
											print("<h2>".$galleryImage['description']."</h2>");
											if(!empty($galleryImage['linkLabel1']) && !empty($galleryImage['linkLocation1'])) print("<a href='".$galleryImage['linkLocation1']."' class='btn btn-tertiary ".($galleryImage['linkLocation1'] == "#scroll" ? "btn-scroll" : "")." small'>".$galleryImage['linkLabel1']."</a>");
											if(!empty($galleryImage['linkLabel2']) && !empty($galleryImage['linkLocation2'])) print("<a href='".$galleryImage['linkLocation2']."' class='btn btn-contact small'>".$galleryImage['linkLabel2']."</a>");
										print("</div>");
									print("</div>");
								print("</li>");
							}
						print("</ul>");
					print("</div>");
				print("</div>");
			} else {
				if(!empty($relation['includeName']) && file_exists($_SERVER['DOCUMENT_ROOT']."/includes/".$relation['includeName'])) {
					include($_SERVER['DOCUMENT_ROOT']."/includes/".$relation['includeName']);
				}
				print($relation['content']);
			}
		}
	}
	
	//	Function to redirect browser
	function redirect($url)
	{
	   if (!headers_sent())
			header('Location: '.$url);
	   else
	   {
			echo '<script type="text/javascript">';
			echo 'window.location.href="'.$url.'";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
			echo '</noscript>';
	   }
	}
	
	//
	function abbr($str, $maxLen) {
		if (strlen($str) > $maxLen && $maxLen > 1) {
			preg_match("#^.{1,".$maxLen."}\.#s", $str, $matches);
			return $matches[0];
		} else {
			return $str;
		}
	}
	
	//	Convert unixtimestamp to specified format
	function fnconvertunixtime ($strtime, $strformat = "d/m/Y") {
		return date($strformat, $strtime);
	}
	
	//	Check to see if email is valid
	function is_valid_email($email) {
		$result = TRUE;
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) {
			$result = FALSE;
		}
		return $result;
	}
	
	// Get discounts
	function checkdiscount($price, $accountid, $conn) {
		$strdbsql = "SELECT customer_offers.* FROM customer_offers INNER JOIN customer ON customer_offers.recordID = customer.offerID WHERE customer.recordID = :user";
		$result = query($conn,$strdbsql,"single",array("user"=>$accountid));
		if($result) {
			if($result['type'] == "percent") {
				$price = $price * ((100 - $result['value']) / 100);
			} else if($result['type'] == "fixed") {
				// What is discounted price is higher than the price of the product?
				$price = $price - $result['value'];
			}
		}
		return number_format($price, 2);
	}
	
	// Delete basket
	function deletebasket($conn, $basket) {
			
		$strdbsql = "DELETE FROM basket_header WHERE recordID = :basket";
		$delete1 = query($conn,$strdbsql,"delete",["basket" => $basket]);

		$strdbsql = "DELETE FROM basket_items WHERE basketHeaderID = :basket";
		$delete2 = query($conn,$strdbsql,"delete",["basket" => $basket]);
		
		setcookie("basket", 0, time() + 60 * 60 * 2, "/");
	
	}
	
	// Create order
	function createorder($conn, $customerid, $basket, $total, $offer, $firstname, $surname, $address1, $address2, $address3, $address4, $address5, $address6, $postcode, $time, $email, $domain, $source) {
		
		$strdbsql = "SELECT * FROM basket_header WHERE recordID = :basket";
		$getOrder = query($conn,$strdbsql,"single",["basket"=>$basket]);
	
		if($getOrder) {
		
			// Get offers
			if(!empty($offer)) {
				$strdbsql = "SELECT * FROM site_offer_codes WHERE recordID = :offer";
				$getOffer = query($conn,$strdbsql,"single",["offer"=>$offer]);
				
				$discount = $getOffer['discountAmount'];
				
				switch($getOffer['discountType']) {
					case "fixed":
						$total -= number_format($discount, 2);
						break;
					case "percent":
						$total *= number_format((100 - $discount) / 100, 2);
						break;
				}
			}
		
			$strdbsql = "INSERT INTO order_header 
			(customerID, basketHeaderID, amountTotal, amountDelivery, amountStock, amountVat, amountOffer, timestampOrder, timestampPayment, offerCode, vatRate, addDeliveryCustomerID, addDeliveryFirstname, addDeliverySurname, addDelivery1, addDelivery2, addDelivery3, addDelivery4, addDelivery5, addDelivery6, addDeliveryPostcode, addDeliveryTime, addBillingFirstname, addBillingSurname, addBilling1, addBilling2, addBilling3, addBilling4, addBilling5, addBilling6, addBillingPostcode, email, domainName, orderSource) 
			VALUES 
			(:customer, :basket, :total, :delivery, :stock, :vat, :discount, UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), :offer, :rate, :customer, :firstname, :surname, :address1, :address2, :address3, :address4, :address5, :address6, :postcode, :time, :firstname, :surname, :address1, :address2, :address3, :address4, :address5, :address6, :postcode, :email, :domain, :source)";
			$addOrder = query($conn,$strdbsql,"insert",[
				"customer" => $customerid,
				"basket" => $basket,
				"total" => $total,
				"delivery" => 0,
				"stock" => $total,
				"vat" => 0,
				"discount" => $discount,
				"offer" => $offer,
				"rate" => 0,
				"firstname" => $firstname,
				"surname" => $surname,
				"address1" => $address1,
				"address2" => $address2,
				"address3" => $address3,
				"address4" => $address4,
				"address5" => $address5,
				"address6" => $address6,
				"postcode" => $postcode,
				"time" => $time,
				"email" => $email,
				"domain" => $domain,
				"source" => $source
			]);
			
			// Get basket items
			$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basket";
			$getItems = query($conn,$strdbsql,"multi",["basket"=>$basket]);
			
			// Add order items
			if(count($getItems) > 0) {
				foreach($getItems AS $getItem) {
					$strdbsql = "INSERT INTO order_items (orderHeaderID, stockID, quantity, price) VALUES (:basket, :stock, :quantity, :price)";
					$addItems = query($conn,$strdbsql,"insert",["basket"=>$addOrder,"stock"=>$getItem['stockID'],"quantity"=>$getItem['quantity'],"price"=>$getItem['price']]);
				}
			} else {
				// No items in basket
			}
			
		}
		
	}
	
	function confirmationemail($conn, $basket) {
		
		global $strsiteurl, $strdomain, $compName, $compPhone, $compEmail, $compAddNumber, $compAddStreet, $compAddCity, $compAddCounty, $compAddCountry, $compAddPostcode;
		
		//overide for testing
		$strsiteurl = "https://saladbowlhorwich.co.uk";
		
		$strdbsql = "SELECT * FROM order_header WHERE basketHeaderID = :basket";
		$getOrder = query($conn,$strdbsql,"single",["basket"=>$basket]);
		
	//	$to = $getOrder['email'];
		$to = "carl@millerwaite.com";
		
		$subject = "Order Confirmation";
		
		$headers = "From: no-reply@".$strdomain."\r\n";
		$headers .= "Reply-To: hello@".$strdomain."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$html = "<!DOCTYPE html>";
		$html .= "<html>";

		$html .= "<head>";
			$html .= "<meta charset='UTF-8'>";
			$html .= "<title>Thank you for your order (#".$getOrder['basketHeaderID'].")</title>";
			$html .= "<style>
				.box-ext{padding:0 25px;}
				.cx-info-box{background:#222222;color:#FFFFFF;}
				.info-cta{width:100%;}
				.info-cta .title{font-size:22px;font-weight:bold;padding-bottom:5px;}
				.info-cta .links{padding-top:10px;}
				.info-cta .links img{margin-right:3px;vertical-align:middle;width:28px;}
				.lead{font-size:18px;letter-spacing:.045em;line-height:24px;mso-line-height-rule:exactly;}
				.lead-secondary{font-size:14px;font-weight:bold;letter-spacing:.045em;line-height:24px;mso-line-height-rule:exactly;}
				.divider{border-bottom:solid 2px #F5F5F5;}
				.em-spacer-0{height:20px;}
				.em-spacer-1{height:26px;}
				.em-spacer-2{height:52px;}
				.em-spacer-3{height:5px;}
				.em-spacer-5{height:32px;}
				.row-spacer{height:8px;}
				.casper-header-logo{border:0;display:block;outline:0;width:160px;}
				.ledger{font-size:14px;line-height:22px;margin:0 auto;width:546px;}
				.column{margin:0 auto;}
				.two-col .column{width:270px;}
				.total-row{font-size:14px;}
				.total-row .copy{text-align:right;}
				.total-row .value{text-align:right;}
				.pipe{margin:0 10px;}
				.line-item .title{font-size:13px;font-weight:bold;margin-bottom:3px;}
				.line-item .sub{font-size:13px;margin-bottom:3px;}
				.line-item .price{margin-top:20px;}
				.line-item .ships-by-message{font-size:13px;}
				.product-thumb-column{width:95px;}
				.product-thumb{vertical-align:middle;width:85px;}
				.product-price-column{text-align:right;vertical-align:middle;width:100px;}
			</style>";
		$html .= "</head>";
  
		PHP_EOL;
  
		$html .= "<body style='color:#222222;font-family:Helvetica,sans-serif;margin:0;padding:0;-webkit-font-smoothing:antialiased;'>";
    
		$html .= "<table class='wrapper' style='background-color:#FFFFFF;border:0;margin:0;width:100%;'>";
			$html .= "<tbody>";
				$html .= "<tr>";
					$html .= "<td class='content-wrap' style='background-color:#F5F5F5;border:0;width:600px;'>";
						$html .= "<table class='content' style='border:0;margin:0;width:600px;'>";
							$html .= "<tbody>";
								
								// Logo
								$html .= "<td class='box' style='background-color:#FFFFFF;'>";
									$html .= "<table class='ledger'>";
										$html .= "<tbody>";
										
											// Spacer
											$html .= "<tr>";
												$html .= "<td class='em-spacer-3'></td>";
											$html .= "</tr>";
											
											PHP_EOL;
										
											$html .= "<tr class='casper-header-row'>";
												$html .= "<td class='casper-header-td'>";
													$html .= "<a href='".$strsiteurl."' target='_blank'>";
														$html .= "<img width='600px' src='".$strsiteurl."images/company/logo2.png' class='casper-header-logo' alt='".$compName."' />";
													$html .= "</a>";
												$html .= "</td>";
											$html .= "</tr>";
											
											// Spacer
											$html .= "<tr>";
												$html .= "<td class='em-spacer-3'></td>";
											$html .= "</tr>";
											
											PHP_EOL;
											
										$html .= "</tbody>";
									$html .= "</table>";
								$html .= "</td>";
								
								PHP_EOL;
								
								// Main Image
								$html .= "<tr>";
									$html .= "<td class='box' style='background-color:#FFFFFF;'>";
										$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td align='left' class='em_hero_td'>";
																		$html .= "<img src='".$strsiteurl."images/emails/order-complete.jpg' alt='Your Order is Complete' />";
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Introduction
								$html .= "<tr>";
									$html .= "<td class='box' style='background-color:#FFFFFF;'>";
										$html .= "<table class='ledger'>";
											$html .= "<tbody>";
												$html .= "<tr>";
													$html .= "<td align='left'>";
														$html .= "<table align='left' border='0' cellpadding='0' cellspacing='0' width='100%'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td class='em-spacer-1'></td>";
																$html .= "</tr>";
																$html .= "<tr class='lead-wrap'>";
																	$html .= "<td class='lead'>";
																		$html .= "Thank you for shopping with Salad Bowl. We are currently processing your goods and they will be with you shortly.";
																	$html .= "</td>";
																$html .= "</tr>";
																$html .= "<tr>";
																	$html .= "<td class='em-spacer-1'></td>";
																$html .= "</tr>";
																$html .= "<tr>";
																	$html .= "<td class='order-link-wrap' style='text-align: left;font-size: 14px;'>";
																		$html .= "<strong>Order: ".$getOrder['basketHeaderID']."";
																		$html .= "<span class='pipe'> | </span>";
																		$html .= "Date ordered: ".date("d/m/y", $getOrder['timestampOrder'])."</strong>";
																	$html .= "</td>";
																$html .= "</tr>";
																$html .= "<tr>";
																	$html .= "<td class='em-spacer-0'></td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Order
								$html .= "<tr>";
									$html .= "<td class='box box-ext order-summary-box' style='background-color:#FFFFFF;'>";
										$html .= "<table class='two-col ledger'>";
											$html .= "<tbody>";
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-1'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Customer Details
												$html .= "<tr>";
													$html .= "<td>";
														$html .= "<table align='left' class='column column-1'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td>";
																		$html .= "<strong class='title' style='text-transform: uppercase;display: block;margin-bottom: 5px;'>Billing Address</strong>";
																		$html .= (!empty($getOrder['addDeliveryTitle']) ? $getOrder['addDeliveryTitle']." " : "").(!empty($getOrder['addDeliveryFirstname']) ? $getOrder['addDeliveryFirstname']." " : "").(!empty($getOrder['addDeliverySurname']) ? $getOrder['addDeliverySurname'] : "")."<br>";
																		if(!empty($getOrder['addDelivery1'])) $html .= $getOrder['addDelivery1'];
																		if(!empty($getOrder['addDelivery2'])) $html .= ", ".$getOrder['addDelivery2'];
																		if(!empty($getOrder['addDelivery3'])) $html .= ", ".$getOrder['addDelivery3']."<br>";
																		if(!empty($getOrder['addDelivery4'])) $html .= $getOrder['addDelivery4'];
																		if(!empty($getOrder['addDelivery5'])) $html .= ", ".$getOrder['addDelivery5'];
																		if(!empty($getOrder['addDeliveryPostcode'])) $html .= "<br/>".$getOrder['addDeliveryPostcode'];
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
														$html .= "<table align='left' class='column column-2'>";
															$html .= "<tbody>";
																$html .= "<tr>";
																	$html .= "<td>";
																		$html .= "<strong class='title' style='text-transform: uppercase;display: block;margin-bottom: 5px;'>Shipping Address</strong>";
																		$html .= (!empty($getOrder['addBillingTitle']) ? $getOrder['addBillingTitle']." " : "").(!empty($getOrder['addBillingFirstname']) ? $getOrder['addBillingFirstname']." " : "").(!empty($getOrder['addBillingSurname']) ? $getOrder['addBillingSurname'] : "")."<br>";
																		if(!empty($getOrder['addBilling1'])) $html .= $getOrder['addBilling1'];
																		if(!empty($getOrder['addBilling2'])) $html .= ", ".$getOrder['addBilling2'];
																		if(!empty($getOrder['addBilling3'])) $html .= ", ".$getOrder['addBilling3'];
																		if(!empty($getOrder['addBilling4'])) $html .= "<br/>".$getOrder['addBilling4'];
																		if(!empty($getOrder['addBilling5'])) $html .= ", ".$getOrder['addBilling5'];
																		if(!empty($getOrder['addBillingPostcode'])) $html .= "<br/>".$getOrder['addBillingPostcode'];
																	$html .= "</td>";
																$html .= "</tr>";
															$html .= "</tbody>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-1'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
										
										$html .= "<table class='order-summary' style='margin: 0 auto;text-align: left;width: 546px;border-top: 2px solid #f5f5f5;'>";
											$html .= "<tbody>";
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-0'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Items
												$strdbsql = "SELECT order_items.quantity, order_items.price, stock_group_information.name, stock_images.imageLink AS image, stock_images.description AS alt, stock_units.abbreviation AS unit FROM order_items 
												INNER JOIN stock ON order_items.stockID = stock.recordID 
												INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
												INNER JOIN stock_images ON order_items.stockID = stock_images.stockID 
												INNER JOIN stock_units ON stock.unit = stock_units.recordID 
												WHERE order_items.orderHeaderID = :order 
												GROUP BY stock_images.stockID";
												$items = query($conn,$strdbsql,"multi",["order"=>$getOrder['recordID']]);
												
												if(count($items) > 0) {
													foreach($items AS $item) {
														$html .= "<tr class='line-item'>";
															$html .= "<td class='line-item product-thumb-column'>";
																$html .= "<img class='product-thumb' width='85px' src='".$strsiteurl."images/products/main/".$item['image']."' alt='".$item['alt']."' />";
																$html .= "<div class='row-spacer'></div>";
															$html .= "</td>";
															$html .= "<td class='line-item product-info-column'>";
																$html .= "<div class='title'>".$item['name']."</div>";
																$html .= "<div class='sub'>".($item['unit'] == "kg" ? "1kg" : ucwords($item['unit']))."</div>";
																$html .= "<div class='sub'>Qty: ".$item['quantity']."</div>";
															$html .= "</td>";
															$html .= "<td class='line-item product-price-column'>";
																$html .= "<div class='sub price'>&pound;".$item['price']."</div>";
																$html .= "<div class='ships-by-message'>Preparing...</div>";
															$html .= "</td>";
														$html .= "</tr>";
														PHP_EOL;
													}
												}
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-0'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Divider
												$html .= "<tr>";
													$html .= "<td class='divider' colspan='3'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-1'></td>";
												$html .= "</tr>";
												
												// Calculations
												$html .= "<tr class='total-row'>";
													$html .= "<td class='copy' colspan='2'>Subtotal</td>";
													$html .= "<td class='value'>&pound;".$getOrder['amountStock']."</td>";
												$html .= "</tr>";
												$html .= "<tr class='total-row'>";
													$html .= "<td class='copy' colspan='2'>Delivery Fee</td>";
													$html .= "<td class='value'>&pound;".$getOrder['amountDelivery']."</td>";
												$html .= "</tr>";
												$html .= "<tr class='total-row'>";
													$html .= "<td class='copy' colspan='2'>VAT @ ".$getOrder['vatRate']."%</td>";
													$html .= "<td class='value'>&pound;".$getOrder['amountVat']."</td>";
												$html .= "</tr>";
												$html .= "<tr class='total-row total'>";
													$html .= "<td class='copy' colspan='2' style='padding-top: 10px;'>";
														$html .= "<strong>Total</strong>";
													$html .= "</td>";
													$html .= "<td class='value' style='padding-top: 10px;'>";
														$html .= "<strong>&pound;".$getOrder['amountTotal']."</strong>";
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-0'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Divider
											//	$html .= "<tr>";
											//		$html .= "<td class='divider' colspan='3' style='border-bottom: 2px solid #eee;'></td>";
											//	$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
											//	$html .= "<tr>";
											//		$html .= "<td class='em-spacer-2'></td>";
											//	$html .= "</tr>";
												
												// Note
											//	$html .= "<tr align='left'>";
											//		$html .= "<td class='lead-secondary' colspan='3'>";
											//			$html .= "Items in your order may ship separately.<br>View <a href='#' target='_blank'>your order</a> for shipping updates.";
											//		$html .= "</td>";
											//	$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
											//	$html .= "<tr>";
											//		$html .= "<td class='em-spacer-2'></td>";
											//	$html .= "</tr>";
												
												PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
								// Contact
								$html .= "<tr>";
									$html .= "<td class='box box-ext cx-info-box' style='background-color:#FFFFFF;'>";
										$html .= "<table class='info-cta'>";
											$html .= "<tbody>";
											
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-0'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Text
												$html .= "<tr>";
													$html .= "<td class='title'>Questions? We're on call.</td>";
												$html .= "</tr>";
												$html .= "<tr>";
													$html .= "<td class='info'>Monday to Saturday 8am - 5pm</td>";
												$html .= "</tr>";
												$html .= "<tr>";
													$html .= "<td class='info'>Sunday Closed</td>";
												$html .= "</tr>";
												$html .= "<tr>";
													$html .= "<td class='links'>";
														$html .= "<a href='#'>";
															$html .= "<img src='".$strsiteurl."images/emails/bubble.png' class='icon-bubble' alt='Phone' />";
															$html .= $compPhone;
														$html .= "</a>";
														$html .= "<span class='pipe'>|</span>";
														$html .= "<a href='#'>";
															$html .= "<img src='".$strsiteurl."images/emails/email.png' class='icon-email' alt='Email' />";
															$html .= $compEmail;
														$html .= "</a>";
													$html .= "</td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
												// Spacer
												$html .= "<tr>";
													$html .= "<td class='em-spacer-0'></td>";
												$html .= "</tr>";
												
												PHP_EOL;
												
											$html .= "</tbody>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
								
								PHP_EOL;
								
							$html .= "</tbody>";
						$html .= "</table>";
						
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</tbody>";
		$html .= "</table>";
		$html .= "<table style='margin: 0 auto;'>";
			$html .= "<tbody>";
				$html .= "<tr>";
					$html .= "<td class='copyright-footer' style='padding-top: 15px;padding-bottom: 20px;font-size: 11px;color: #b2b2b2;'>";
						if(!empty($compAddNumber)) $html .= $compAddNumber." ";
						if(!empty($compAddStreet)) $html .= $compAddStreet.", ";
						if(!empty($compAddCity)) $html .= $compAddCity.", ";
						if(!empty($compAddCounty)) $html .= $compAddCounty.", ";
						if(!empty($compAddCountry)) $html .= $compAddCountry.", ";
						if(!empty($compAddPostcode)) $html .= $compAddPostcode;
						$html .= "<span class='pipe'>&nbsp;|&nbsp;</span>";
						$html .= "&copy; ".date("Y")." ".$compName;
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</tbody>";
		$html .= "</table>";
	
		$html .= "</body>";

		$html .= "</html>";
		
		// Send email
		if(mail($to, $subject, $html, $headers)) {
			
			// Update confirmation email status on order
			$strdbsql = "UPDATE order_header SET sentEmailConfirmation = 1 WHERE basketHeaderID = :basket";
			$result = query($conn,$strdbsql,"update",["basket"=>$basket]);
			
		}
		
	}
	
	// Check customer information
	function checkcustomer($conn, $accountid, $title, $firstname, $lastname, $telephone, $address1, $address2, $address3, $address4, $address5, $address6, $postcode, $email) {
	
		// Check if the user is trying to create an account
		if($accountid == 0) {
			
			// Check if customer has previously checked out
			$strdbsql = "SELECT * FROM customer WHERE email = :email";
			$customer = query($conn,$strdbsql,"single",["email"=>$email]);
			
			if(!$customer) {
				
				// Create customer account
				$strdbsql = "INSERT INTO customer (groupID, title, firstname, surname, telephone, email) VALUES (:group, :title, :firstname, :lastname, :telephone, :email)";
				$addCustomer = query($conn,$strdbsql,"insert",["group"=>3,"title"=>$title,"firstname"=>$firstname,"lastname"=>$lastname,"telephone"=>$telephone,"email"=>$email]);
				
				$customerid = $addCustomer;
				
				// Create an address for customer
				$strdbsql = "INSERT INTO customer_address (customerID, title, firstname, surname, add1, add2, add3, town, county, country, postcode) VALUES (:customer, :title, :firstname, :lastname, :address1, :address2, :address3, :town, :county, :country, :postcode)";
				$addAddress = query($conn,$strdbsql,"insert",["customer"=>$customerid,"title"=>$title,"firstname"=>$firstname,"lastname"=>$lastname,"address1"=>$address1,"address2"=>$address2,"address3"=>$address3,"town"=>$address4,"county"=>$address5,"country"=>$address6,"postcode"=>$postcode]);
				
				// Assign address to customer
				$strdbsql = "UPDATE customer SET defaultDeliveryAdd = :delivery, defaultBillingAdd = :billing WHERE recordID = :customer";
				$setAddress = query($conn,$strdbsql,"update",["customer"=>$customerid,"delivery"=>$addAddress,"billing"=>$addAddress]);
				
			} else {
				
				$customerid = $customer['recordID'];
				
			}
			
		} else {
			
			$customerid = $accountid;
			
		}
		
		return $customerid;
		
	}
	
?>