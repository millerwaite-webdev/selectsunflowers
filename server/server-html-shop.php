<?php
	
	use Data\ProductMethods;
	
	//added standard php/mysql config file with host, user and password info
	require $_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php";
	
	//models and collections
	require "domain/models/filter-result-model.php";
	require "domain/collection/filter-result-collection.php";
	
	//domain
	require "domain/action.php";
	require "domain/sorting.php";
	require "domain/pagination-shop.php";
	require "domain/filtering.php";
	require "domain/server-shop.php";
	
	//controls
	require "controls/sortdropdown-shop.php";
	require "controls/textbox.php";
	require "controls/checkboxgroupfilter.php";
	require "controls/filterdropdown.php";
	require "controls/filterselect.php";
	require "controls/range-slider.php";
	require "controls/range-filter.php";
	require "controls/button-text-filter.php";
		
	class jPListHTML extends jPListServer{
			
		/**
		* get html for one item
		* @param {Object} $item
		* @return {string} html
		*/
		
		private function getHTML($row){
		
			$conn = connect();
			
			$products = new ProductMethods($conn);
			$products->id = $row['groupID'];
			$product = $products->productHTML();

			return $product;
		
		}
		
		/**
		* get the whole html
		* @param {string} $itemsHtml - items html
		* @return {string} html
		*/
		private function getHTMLWrapper($itemsHtml){
			
			$html = "";
			
			$html .= "<div data-type='jplist-dataitem' data-count='" . $this->pagination->numberOfPages . "' class='box'>";
				$html .= $itemsHtml;
			$html .= "</div>";		
			
			return $html;
			
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			$html = "";
			
			try{
                parent::__construct();
				
				if(isset($this->statuses)){
					
					$items = $this->getData();
					
					if($items){
						foreach($items as $item){
							$html .= $this->getHTML($item);					
						}
					}
						
                    ob_clean();
                    
					//print html
					echo($this->getHTMLWrapper($html));
				}
				
				//close the database connection
				$this->db = NULL;
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
	
	/**
	* start
	*/
	new jPListHTML();
?>	