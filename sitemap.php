<?php

//	**********************************************************************
//	* by Lee Watts                                                       *
//	* Email address: leewatts@millerwaite.co.uk                          *
//	*                                                                    *
//	* For: Croft Care Trust                                              *
//	*                                                                    *
//	* sitemap.php                                                        *
//	*--------------------------------------------------------------------*
//		$strthisver = "V1.0 - Jul 2016" 
//	**********************************************************************
//	XML sitemap for search engines auto updating from db.

	$strrootpath = $_SERVER['DOCUMENT_ROOT']."/";
	include($strrootpath."includes/incsitecommon.php");
	header("Content-type: text/xml");
	
	$conn = connect();

	print("<?xml version='1.0'?>\n");
	print("<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9' xmlns:xhtml='http://www.w3.org/1999/xhtml'>");
	
	// Pages
	$strdbsql = "SELECT * FROM site_pages WHERE visible = 1";
	$result = query($conn,$strdbsql,"multi");

	if(count($result) > 0) {
		foreach($result AS $row) {
			print("<url>");
				print("<loc>".$strsiteurl.($row['pageName'] != "index" ? $row['pageName'] : "")."</loc>");
				print("<changefreq>monthly</changefreq>");
				print("<priority>0.3</priority>");
			print("</url>");
		}
	}
	
	// News
	$strdbsql = "SELECT DISTINCT link FROM site_news_events	WHERE enabled = 1";
	$result = query($conn,$strdbsql,"multi");
	
	if(count($result) > 0) {
		foreach($result AS $row) {
			print("<url>");
				print("<loc>".$strsiteurl."news/".str_replace(" ", "-", strtolower($row['link']))."</loc>");
				print("<changefreq>monthly</changefreq>");
				print("<priority>0.3</priority>");
			print("</url>");
		}
	}
	
	// Product Categories
	$strdbsql = "SELECT DISTINCT metaPageLink FROM category WHERE showProducts = 1";
	$result = query($conn,$strdbsql,"multi");
	
	if(count($result) > 0) {
		foreach($result AS $row) {
			print("<url>");
				print("<loc>".$strsiteurl."shop/".str_replace(" ", "-", strtolower($row['metaPageLink']))."</loc>");
				print("<changefreq>monthly</changefreq>");
				print("<priority>0.3</priority>");
			print("</url>");
		}
	}
	
	// Products
	$strdbsql = "SELECT DISTINCT stock_group_information.metaLink FROM stock 
	LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
	WHERE stock_group_information.enabled = 1";
	$result = query($conn,$strdbsql,"multi");
	
	if(count($result) > 0) {
		foreach($result AS $row) {
			print("<url>");
				print("<loc>".$strsiteurl."shop/all/".str_replace(" ", "-", strtolower($row['metaLink']))."</loc>");
				print("<changefreq>monthly</changefreq>");
				print("<priority>0.3</priority>");
			print("</url>");
		}
	}
	
	// Other
	$result = [
		1 => "register",
		2 => "forgot-password"
	];
	
	if(count($result) > 0) {
		foreach($result AS $row) {
			print("<url>");
				print("<loc>".$strsiteurl.$row."</loc>");
				print("<changefreq>monthly</changefreq>");
				print("<priority>0.3</priority>");
			print("</url>");
		}
	}
	
	print("</urlset>");
	
	$conn = NULL;

?>