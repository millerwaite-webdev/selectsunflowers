<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * editAccount.php                                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	
	// Ensure user is logged on
	// --------------------
	if (empty($_SESSION['userID'])) { header ("Location: account.php"); }

	
	$strpage = "editAccount"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// ************* Requests processing ****************** //
	//=====================================================//
	if(isset($_REQUEST['command'])) $strcmd = $_REQUEST['command'];
	else $strcmd = "";
	if(isset($_REQUEST['referer'])) $strreferer = $_REQUEST['referer'];
	else $strreferer = "editAccount.php";
	
	if(isset($_GET['error'])) $strerror = "Sorry - your password could not be reset. Please try again.";
	
	switch ($strcmd)
	{
		case "updatepwd":
		
			$strerror = '';
			if(empty($_POST['oldpassword']) || empty($_POST['newpassword']) || empty($_POST['cnewpassword'])) {
				$strerror .= "- Please enter your old password, new password and a confirmation of this password.<br/>";
			}
			if(!empty($_POST['oldpassword'])) {
				$strdbsql = "SELECT password FROM customer WHERE recordID = ".$intuserid;
				$arrType = "single";
				$resultdata = query($conn, $strdbsql, $arrType);
				if(md5($_POST['oldpassword']) != $resultdata['password']) {
					$strerror .= "- Old password was not correct.<br/>";
				}
			}
			if($_POST['newpassword'] != $_POST['cnewpassword']) {
				$strerror .= "- New password and re-typed password did not match.";
			}
			if($strerror == '') {
				$strdbsql = "UPDATE customer SET password = '".md5($_POST['newpassword'])."' WHERE recordID = ".$intuserid;
				$arrType = "update";
				if(query($conn, $strdbsql, $arrType)) {
					$strinfo = "Password successfully updated.";
				} else {
					$strerror = "Error while updating password.";
				}
			}
			break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	print("<div id='mainContent'>");
?>

<script type="Text/JavaScript">
<!--

function jschangepwd()
{
	if (document.frm_form.newpassword.value.length < 7 || document.frm_form.newpassword.value.length > 16)
	{
		alert("Please make sure you password is between 7 and 15 characters in length.");
		return false;
        }
        else if (document.frm_form.newpassword.value == "" || document.frm_form.newpassword.value != document.frm_form.cnewpassword.value)
	{
		alert("Please enter a Password and ensure it is confirmed correctly.");
		return false;
	}
	else
	{
		document.frm_form.command.value = "updatepwd";
		return true;
	}
}

function jspwd()
{
  if (jschangepwd()) document.frm_form.submit();
}
function jscancel() {
	window.location = "account.php";
}
-->
</script>
<?php
		print ("<div class='textbox'>");
			print ("<form name='frm_form' action='editAccount.php' method='post' onSubmit='return jschangepwd()'>");
				print ("<input type='hidden' name='command' />");
				
				if ($intuserid > 1)
				{

					print ("<h2>Change Password</h2>");
	
					if($strerror != "") {
						print("<div class='notification-error'>");
							print("<h3>Error</h3>");
							print("<p>".$strerror."</p>");
						print("</div>");
					}
					if($strinfo != "") {
						print("<div class='notification-info'>");
							print("<h3>Info</h3>");
							print("<p>".$strinfo."</p>");
						print("</div>");
					}
					
					print ("<div class='bodytext'>");
						print ("<table>");
							print ("<tr><td align='right'><label for='changeusername'>Username : </label></td><td align='left'>$strusername</td></tr>");
							print ("<tr><td align='right'><label for='oldpassword'>Current Password : </label></td><td align='left'><input name='oldpassword' type='password' size='30'  maxlength='15'/></td></tr>");
							print ("<tr><td align='right'><label for='newpassword'>New Password : </label></td><td align='left'><input name='newpassword' type='password' size='30' maxlength='15'/></td></tr>");
							print ("<tr><td align='right'><label for='cnewpassword'>Confirm New Password : </label></td><td align='left'><input name='cnewpassword' type='password' size='30' maxlength='15'/></td></tr>");
						print("</table>");
						print ("<br/>");
						print ("<div class='buttons'><button type='submit' id='updateLogin' onclick='jspwd()' class='short_button' >Update Password</button><a href='account.php' class='short_button'>Cancel Changes</a></div>");
					print("</div>");
				}
				else
				{
					redirect("account.php");
				}
				
			print ("</form>");
		
		print("</div>");
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>