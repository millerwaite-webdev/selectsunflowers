<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * delAccount.php                                                     * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	
	// Ensure user is logged on
	// --------------------
	if (empty($_SESSION['userID'])) { header ("Location: account.php"); }

	
	$strpage = "editAccount"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// ************* Requests processing ****************** //
	//=====================================================//
	if(isset($_REQUEST['command'])) $strcmd = $_REQUEST['command'];
	else $strcmd = "";
	if(isset($_REQUEST['referer'])) $strreferer = $_REQUEST['referer'];
	else $strreferer = "delAccount.php";
	
	switch ($strcmd)
	{
		case "delaccount":

			$username = $_REQUEST['delusername'];
			$password = $_REQUEST['delpassword'];
			
			$basicUserDetails = checkUserLogin($username, $password, $conn);

			if ($basicUserDetails['num_valid'] > 0)
			{
				deleteUser($username, $password);

				unset($_SESSION);
				$_SESSION['userID'] = 0;
				$intuserid = -1;
				setcookie ("stoid", "$intuserid", time() + 60*60*2, "/", $strcookiehref);
				header ("Location: ".$strsiteurl);
			}
			
			break;
		default:
			
			if (!empty($_SESSION['email']))
			{
				$email = $_SESSION['email'];
			}
			break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
?>
<script type="Text/JavaScript">
	<!--

	function jsdeleteaccount()
	{
		document.frm_form.command.value = "delaccount";
		document.frm_form.submit();
		return true;
	}

	function jsback() {
		window.location = "account.php";
	}

	-->
</script>
<?php	
	print("<div id='mainContent'>");
	
		print ("<form name='frm_form' action='delAccount.php' method='post' onsubmit='return jsdeleteaccount()'>");
				print ("<input type='hidden' name='command' />");
				print ("<h2>Delete Account - ".$email."</h2>");
				print ("<h3>Confirm Account Details:</h3>");
				print("<table style='margin-left: 6px;'>");
					print("<tr><td><label for='delusername'>Email Address : </label></td><td><input name='delusername' id='delusername' type='text' size='30' tabindex='1' value='".$email."'/></td></tr>");
					print ("<tr><td><label for='delpassword'>Password : </label></td><td><input name='delpassword' id='delpassword' type='password' size='30' tabindex='2'/></td></tr>");
				print("</table>");
				print ("<div class='buttons'>");
					print("<p><span style='font-weight:bold;color:red;'>Are you Sure?</span>");
					print("	This will delete your account from our web site records, you will not receive any further warning. This process can not be reversed but you will be able to register a new account with the same details. This will only delete your online account, if you wish us to remove you from our system entirely please <a href='/contact.php'>contact us</a>.</p> ");
					print ("<button type='submit' id='delAccount' class='short_button' tabindex='3' onclick='jsdeleteaccount()'>Delete Account</button>");
					print ("<input type='button' onclick='jsback()' class='short_button' value='Return to Account' />");
				print("</div>");
		print ("</form>");
	
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>