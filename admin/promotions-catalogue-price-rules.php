<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * promotions-catalogue-price-rules.php	                           * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "promotions-catalogue-price-rules"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['offerCodeID'])) $offerCodeID = $_REQUEST['offerCodeID']; else $offerCodeID = "";
	
	switch($strcmd)
	{
		case "insertOfferCode":
		case "updateOfferCode":
			
			if(!empty($_POST['frm_startdate']))
			{
				$startDate = strtotime(str_replace("/", "-", $_POST['frm_startdate']));
			}
			else
			{
				$startDate = 0;
			}
			
			if(!empty($_POST['frm_expirydate']))
			{
				$expiryDate = strtotime(str_replace("/", "-", $_POST['frm_expirydate']));
			}
			else
			{
				$expiryDate = 0;
			}
			
			$categoryList = "";
			foreach($_POST['frm_categories'] AS $category)
			{
				$categoryList .= $category.",";
			}
			$categoryList = rtrim($categoryList, ',');
			
			$stockList = "";
			foreach($_POST['frm_stock'] AS $stock)
			{
				$stockList .= $stock.",";
			}
			$stockList = rtrim($stockList, ',');
			
			$arrdbparams = array(
								"code" => $_POST['frm_code'],
								"description" => $_POST['frm_description'],
								"allowedUses" => $_POST['frm_alloweduses'],
								"startDate" => $startDate,
								"expiryDate" => $expiryDate,
								"minimumSpend" => $_POST['frm_minspend'],
								"discountAmount" => $_POST['frm_discount'],
								"discountType" => $_POST['frm_discounttype'],
								"categoryList" => $categoryList,
								"stockList" => $stockList
							);
			
			if ($strcmd == "insertOfferCode")
			{	
				$strdbsql = "INSERT INTO site_offer_codes (code, description, allowedUses, startDate, expiryDate, minimumSpend, discountAmount, discountType, categoryList, stockList) 
							VALUES (:code, :description, :allowedUses, :startDate, :expiryDate, :minimumSpend, :discountAmount, :discountType, :categoryList, :stockList)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateOfferCode")
			{
				$strdbsql = "UPDATE site_offer_codes SET code = :code, description = :description, allowedUses = :allowedUses, startDate = :startDate, expiryDate = :expiryDate, minimumSpend = :minimumSpend,
							 discountAmount = :discountAmount, discountType = :discountType, categoryList = :categoryList, stockList = :stockList WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $offerCodeID;
				$strType = "update";
			}
			
			$updateOfferCode = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertOfferCode")
			{
				$offerCodeID = $updateOfferCode;
			}
			
			if ($strcmd == "insertOfferCode")
			{
				if ($updateOfferCode > 0)
				{
					$strsuccess = "Offer code successfully added";
				}
				elseif ($updateOfferCode == 0)
				{
					$strerror = "An error occurred while adding the offer code";
				}
			}
			elseif ($strcmd == "updateOfferCode")
			{
				if ($updateOfferCode <= 1)
				{
					$strsuccess = "Offer code successfully updated";
				}
				elseif ($updateOfferCode > 1)
				{
					$strwarning = "An error may have occurred while updating this offer code";
				}
			}
			
			$strcmd = "viewOfferCode";
			
		break;
		
		case "deleteOfferCode":
			
			$strdbsql = "DELETE FROM site_offer_codes WHERE recordID = :offerCodeID";
			$strType = "delete";
			$arrdbparams = array( "offerCodeID" => $offerCodeID );
			$deleteOfferCode = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Offer Code Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddOfferCode() {
					document.form.cmd.value="addOfferCode";
					document.form.submit();
				}
				function jsviewOfferCode(offerCodeID) {
					document.form.cmd.value="viewOfferCode";
					document.form.offerCodeID.value=offerCodeID;
					document.form.submit();
				}
				function jsdeleteOfferCode(offerCodeID) {
					if(confirm("Are you sure you want to delete this offer code?"))
					{
						document.form.cmd.value="deleteOfferCode";
						document.form.offerCodeID.value=offerCodeID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertOfferCode() {
					document.form.cmd.value='insertOfferCode';
					document.form.submit();
				}
				function jsupdateOfferCode() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateOfferCode';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
			</script>
			<?php
		
			print("<form action='promotions-catalogue-price-rules.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='offerCodeID' id='offerCodeID' value='".$offerCodeID."' />");
				
				switch($strcmd)
				{
					case "viewOfferCode":
					case "addOfferCode":
						
						print("<div class='row'>");
							print("<div class='col-md-6'>");
								print("<div class='section'>");
						
									if ($strcmd == "viewOfferCode")
									{
										$strdbsql = "SELECT * FROM site_offer_codes WHERE recordID = :recordID";
										$strType = "single";
										$arrdbparams = array("recordID" => $offerCodeID);
										$offerCodeDetails = query($conn, $strdbsql, $strType, $arrdbparams);
										
										$startDate = date("d/m/Y", $offerCodeDetails['startDate']);
										$expiryDate = date("d/m/Y", $offerCodeDetails['expiryDate']);
										
										$categoryListArray = explode(",", $offerCodeDetails['categoryList']);
										$stockListArray = explode(",", $offerCodeDetails['stockList']);
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Offer Code Details</legend>");
									}
									elseif ($strcmd == "addOfferCode")
									{
										$pageDetails = array(
											"code" => "",
											"description" => "",
											"allowedUses" => "",
											"startDate" => "",
											"expiryDate" => "",
											"minimumSpend" => "",
											"discountAmount" => "",
											"discountType" => "",
											"categoryList" => "",
											"stockList" => ""
										);
										$startDate = "";
										$expiryDate = "";
										
										$categoryListArray = array();
										$stockListArray = array();
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Offer Code Details</legend>");
									}
								
									print("<div class='row'>");
									
										print("<div class='col-sm-12 form-group'>
											<label for='frm_code' class='control-label'>Code</label>
											<input type='text' class='form-control' id='frm_code' name='frm_code' value='".$offerCodeDetails['code']."'>
										</div>
										
										<div class='col-sm-12 form-group'>
											<label for='frm_alloweduses' class='control-label'>Total Allowed Uses (leave blank if unlimited)</label>
											<input type='text' class='form-control' id='frm_alloweduses' name='frm_alloweduses' value='".$offerCodeDetails['allowedUses']."'>
										</div>
										
										<div class='col-sm-12 form-group'>
											<label for='frm_description' class='control-label'>Description</label>
											<input type='text' class='form-control' id='frm_description' name='frm_description' value='".$offerCodeDetails['description']."' />
										</div>
										
										<div class='col-sm-12 form-group'>
											<label for='frm_startdate' class='control-label'>Start Date</label>
											<input type='text' class='form-control' id='frm_startdate' name='frm_startdate' value='".$startDate."' />
										</div>
									  
										<div class='col-sm-12 form-group'>
											<label for='frm_expirydate' class='control-label'>Expiry Date</label>
											<input type='text' class='form-control' id='frm_expirydate' name='frm_expirydate' value='".$expiryDate."' />
										</div>
										
										<div class='col-sm-12 form-group'>
											<label for='frm_minspend' class='control-label'>Minimum Spend (&pound;)</label>
											<input type='text' class='form-control' id='frm_minspend' name='frm_minspend' value='".$offerCodeDetails['minimumSpend']."' />
										</div>
									  
										<div class='col-sm-12 form-group'>
											<label for='frm_discount' class='control-label'>Discount Amount</label>
											<input type='text' class='form-control' id='frm_discount' name='frm_discount' value='".$offerCodeDetails['discountAmount']."' />
										</div>
									  
										<div class='col-sm-12 form-group'>
											<label for='frm_discounttype' class='control-label'>Discount Type</label>
											<select class='form-control' id='frm_discounttype' name='frm_discounttype'>");
											$strSelected = "";
											if ($offerCodeDetails['discountType'] == "percent")
											{
												$strSelected = " selected";
											}
											print("<option value='percent'".$strSelected.">Percent</option>");
											$strSelected = "";
											if ($offerCodeDetails['discountType'] == "pound")
											{
												$strSelected = " selected";
											}
											print("<option value='pound'".$strSelected.">Pound</option>");
											print("</select>
										</div>");
											
									print("</div>");
									
									print("</fieldset>");
								
								print("</div>");
							print("</div>");
							print("<div class='col-sm-6 hide'>");
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
									
										print("<div class='row'>");
									
											print("<div class='col-sm-12 form-group'>
												<label for='frm_categories' class='control-label'>Category List</label>
												<select class='form-control' id='frm_categories' name='frm_categories[]' multiple>");
												$strdbsql = "SELECT recordID, contentName FROM category ORDER BY category.contentName ASC";
												$strType = "multi";
												$categories = query($conn, $strdbsql, $strType);
												foreach($categories AS $category)
												{
													$strSelected = "";
													if (in_array($category['recordID'], $categoryListArray) || $categoryListArray[0] == "all")
													{
														$strSelected = " selected";
													}
													print("<option value='".$category['recordID']."'".$strSelected.">".$category['contentName']."</option>");
												}
												print("</select>
											</div>
										  
											<div class='col-sm-12 form-group'>
												<label for='frm_stock' class='control-label'>Stock List</label>
												<select class='form-control' id='frm_stock' name='frm_stock[]' multiple>");
												$strdbsql = "SELECT recordID, stockCode FROM stock ORDER BY stockCode ASC";
												$strType = "multi";
												$stockItems = query($conn, $strdbsql, $strType);
												foreach($stockItems AS $stockItem)
												{
													$strSelected = "";
													if (in_array($stockItem['recordID'], $stockListArray) || $stockListArray[0] == "all")
													{
														$strSelected = " selected";
													}
													print("<option value='".$stockItem['recordID']."'".$strSelected.">".$stockItem['stockCode']."</option>");
												}
												print("</select>
											</div>");
									
										print("</div>");
									print("</fieldset>");
								print("</div>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='col-sm-12'>");
								if ($strcmd == "addOfferCode") print("<button onclick='return jsinsertOfferCode();' type='submit' class='btn btn-success pull-right'>Save</button> ");
								else if($strcmd == "viewOfferCode") print("<button onclick='return jsupdateOfferCode();' type='submit' class='btn btn-success pull-right'>Save</button>");
								print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
							print("</div>");
						print("</div>");
					
						break;
						
					default:
						
						$strdbsql = "SELECT * FROM site_offer_codes";
						$strType = "multi";
						$offerCodes = query($conn, $strdbsql, $strType);
						
						print("<div class='section'>");
							print("<table id='codes-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th>Code</th>");
									print("<th>Description</th>");
									print("<th>Allowed Uses</th>");
									print("<th>Total Uses</th>");
									print("<th>Start Date</th>");
									print("<th>Expiry Date</th>");
									print("<th>Minimum Spend</th>");
									print("<th>Discount Amount</th>");
									print("<th>Discount Type</th>");
									print("<th>View Offer Code</th>");
									print("<th>Delete Offer Code</th>");
								print("</tr></thead><tbody>");
								foreach($offerCodes AS $offerCode)
								{
									print("<tr>");
										print("<td>".$offerCode['code']."</td>");
										print("<td>".$offerCode['description']."</td>");
										print("<td>".$offerCode['allowedUses']."</td>");
										print("<td>".$offerCode['totalUses']."</td>");
										print("<td>".date("d/m/Y", $offerCode['startDate'])."</td>");
										print("<td>".($offerCode['expiryDate'] > 0 ? date("d/m/Y", $offerCode['expiryDate']) : "None")."</td>");
										print("<td>&pound;".$offerCode['minimumSpend']."</td>");
										print("<td>".$offerCode['discountAmount']."</td>");
										print("<td>".$offerCode['discountType']."</td>");
										print("<td><button onclick='return jsviewOfferCode(\"".$offerCode['recordID']."\");' type='submit' class='btn btn-primary circle'><i class='fa fa-pencil'></i></button></td>");
										print("<td><button onclick='return jsdeleteOfferCode(\"".$offerCode['recordID']."\");' type='submit' class='btn btn-danger circle'><i class='fa fa-trash'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<button onclick='return jsaddOfferCode();' type='submit' class='btn btn-success'>Add New</button>");
						
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#codes-table').DataTable();
			
			$( "#frm_startdate" ).datepicker({
				dateFormat: 'dd/mm/yy',
				onSelect: function(dateText, inst) {
					if($('#frm_expirydate').val() == '') {
						var current_date = $.datepicker.parseDate('dd/mm/yy', dateText);
						current_date.setDate(current_date.getDate()+1);
						$('#frm_expirydate').datepicker('setDate', current_date);
					}
				},
				onClose: function( selectedDate, test) {
					var day = ('0' + (parseInt(test.selectedDay))).slice(-2);
					var month = ('0' + (test.selectedMonth)).slice(-2);
					var year = test.selectedYear;
					var tempDate = new Date(year, month, day);
					tempDate.setDate(tempDate.getDate()+1);
					var MyDateString = ('0' + (parseInt(tempDate.getDate()))).slice(-2) + '/' + ('0' + (tempDate.getMonth()+1)).slice(-2) + '/' + tempDate.getFullYear();
					$("#frm_expirydate").datepicker( "option", "minDate", MyDateString);
				}
			});
			
			$( "#frm_expirydate" ).datepicker({
				dateFormat: 'dd/mm/yy'
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>