<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * config-letters.php                                      * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "config-letters"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['recordID'])) $recordID = $_REQUEST['recordID']; else $recordID = "";
	if (isset($_REQUEST['letterID'])) $letterID = $_REQUEST['letterID']; else $letterID = "";
	if (isset($_REQUEST['letterTypeID'])) $letterTypeID = $_REQUEST['letterTypeID']; else $letterTypeID = "";
	if (isset($_REQUEST['letterMatrixID'])) $letterMatrixID = $_REQUEST['letterMatrixID']; else $letterMatrixID = "";
	if (isset($_REQUEST['communicationType'])) $communicationType = $_REQUEST['communicationType']; else $communicationType = "";
	if (isset($_REQUEST['newMatrixType'])) $newMatrixType = $_REQUEST['newMatrixType']; else $newMatrixType = "dormant";
	if (isset($_REQUEST['returnLocation'])) $returnLocation = $_REQUEST['returnLocation']; else $returnLocation = "";
	if (isset($_REQUEST['updateTable'])) $updateTable = $_REQUEST['updateTable']; else $updateTable = "";
	if (isset($_REQUEST['reload'])) $reload = $_REQUEST['reload']; else $reload = "";

	function fnShowLetterHelp($datnow, $letterID, $conn, $emailDetails,$mandrillApiKey){

		
			if($letterID != NULL) {
				$dataarray= fnGetLetterFields($letterID,$conn);
				print("<h3>Letter Information</h3>");
				print ("<p>Usually best not to have a carriage return at the top of the document as this can cause issues with spacing on each letter.</p>");
				print ("<p>If compatibility mode is enabled it may not be opossible to have the same merge field more than once in the document template.</p>");
				print ("<p>The file format needs to be .docx </p>");
				print ("<p>Lock images in place – the location it is locked to can be changed using cut and paste. To set the initial anchor location, click next to the text you want to anchor the image to and then click the anchor option.</p>");

			} else {
				print("<h3>Field Information</h3>");
				$dataarray =null;
			}
			print ("<p>Add the following merge field to show the number of letters in this print run: countofletters</p>");
			print ("<p>The following are merge fields configured for auto completion and therefore don’t need matching up when new templates are uploaded. Address details cannot be auto matched. If a field has been found in the document it will be indicated in the last column.</p>");

			print ("<br/><table style='table-layout: fixed;' class='table  table-bordered table-hover table-condensed' ><thead>");
				print ("<tr>");
					print ("<th style='width:60%'>Description</th>");
					print ("<th style='width:30%'>Merge Field</th>");
					if($dataarray != null)print ("<th style='width:10%'>Matched</th>");
				print ("</tr></thead><tbody>");
			$strdbsql = "SELECT * FROM letterMergeFieldGroups  ORDER BY groupType,groupID ASC";
			$strType = "multi";
			$arrdbparams = array();
			$groups = query($conn, $strdbsql, $strType, $arrdbparams);
			$strlast = "";
			foreach ($groups AS $key=>$group){
				if($strlast != $group['groupType']) {
					print("<tr style='background-color:#DBFFEC'>");
						print("<td colspan='".(($dataarray != null) ? 3 : 2)."'>".$group['groupType']."</td>");
					print("</tr>");
					$strlast = $group['groupType'];
				}
				$strdbsql = "SELECT * FROM letterMergeFields WHERE fieldGroup = :group ORDER BY mergeField";
				$strType = "multi";
				$arrdbparams = array("group" => $group['groupID']);
				$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
				if (count($resultdata) > 0) {
					print("<tr style='background-color:".((intVal($key/2) == $key/2) ? "#FFFFFF" : "#DBECFF")."'>");
						print("<td rowspan='".count($resultdata)."'>".$group['groupDescription']."</td>");
						foreach ($resultdata as $key2=>$row) {	
							if(intVal($key/2) == $key/2) {
								$normal = "#FFFFFF";
								$alt = "#EEEEEE";
							} else {
								$normal = "#DBECFF";
								$alt = "#EAF4FF";
							}
							if($key2 != 0) print("<tr style='background-color:".((intVal($key2/2) == $key2/2) ? $normal : $alt)."'>");
								print ("<td>".$row['mergeField']."</td>");
								if($dataarray != null) print("<td style='color:green'>".((in_array($row['mergeField'],$dataarray)) ? "✔ Matched" : "")."</td>");
							print("</tr>");
						}
				}
			}
			print ("</tbody></table>");
	}
	
	function fnShowLetterHistory($datnow, $letterGroupID, $conn){
		print("<h3>Letter Template History</h3>");
			
		
		print ("<table style='table-layout: fixed;background-color:white' class='table  table-bordered table-hover table-condensed' ><thead>");
			print ("<tr>");
				print ("<th style='width:60%'>Date</th>");
				print ("<th style='width:30%'>Operator</th>");
				print ("<th style='width:30%'>Enabled</th>");
				print ("<th style='width:30%'>File Version</th>");
				print ("<th style='width:30%'>Field Data</th>");
				
			print ("</tr>");
		print("</thead><tbody>");
			$strdbsql = "SELECT letterTemplateHistory.*, operator.username FROM letterTemplateHistory INNER JOIN operator ON letterTemplateHistory.operatorID = operator.operatorID WHERE letterGroupID = :groupID ORDER BY timestamp ASC";
			$strType = "multi";
			$arrdbparams = array("groupID"=>$letterGroupID);
			$changes = query($conn, $strdbsql, $strType, $arrdbparams);
			foreach ($changes AS $key=>$row){
				print("<tr style='background-color:".((intVal($key/2) != $key/2) ? "#FFFFFF" : "#EAF4FF")."'>");
					print("<td>".date("d/m/Y H:i:s", $row['groupDescription'])."</td>");
					print ("<td>".$row['username']."</td>");
					print("<td>".(($row['enabled']) ? "✔ Enabled" : "Disabled")."</td>");
					print("<td></td>");
					print("<td></td>");
				print("</tr>");
			}
		print ("</tbody></table>");
	}
    
	function fnLetterTypeDisplay($datnow, $conn){

		
		//Batch Letters
		$strdbsql = "SELECT letterTypes.*, letterGroupType.description AS groupTypeDescription FROM letterTypes INNER JOIN letterGroupType ON letterTypes.letterGroupTypeID = letterGroupType.recordID ORDER BY letterGroupTypeID ASC, letterTypes.recordID";
		$strType = "multi";
		$arrdbparams = array();
		$letterTypes = query($conn,$strdbsql,$strType,$arrdbparams);
		
		$previousGroupType = 0;
		foreach ($letterTypes AS $row) {
		
			if (!empty($row['letterGroupID'])) {
				$strdbsql = "SELECT letters.fileLocation, letters.description AS letterDescription, letters.letterID FROM letters WHERE groupID = :letterGroupID ORDER BY version DESC LIMIT 1";
				$strType = "single";
				$arrdbparams = array("letterGroupID"=>$row['letterGroupID']);
				$letter = query($conn,$strdbsql,$strType,$arrdbparams);
			} else {
				$letter = NULL;
			}
			
			
			if ($previousGroupType != $row['letterGroupTypeID']) {
				if ($previousGroupType != 0) {
					print ("</tbody></table>");
					print ("</fieldset>");
					print("</div>");
				}
				print("<div  id='".str_replace(" ","",$row['groupTypeDescription'])."'>");
				print("<p style='text-align:right'><a href='#' >Go To Top</a></p>");
				print ("<fieldset class='inlineLabels'><legend>".$row['groupTypeDescription']."</legend>");
				print ("<table style='table-layout: fixed;' class='table table-striped table-bordered table-hover table-condensed' ><thead>");
				print ("<tr>");
					print ("<th >Description</th>");
					print ("<th style='width:310px'>Letter</th>");
					print ("<th style='width: 70px;'>Last Mod.</th>");
					print ("<th style='width: 60px;'>Status</th>");
				print ("</tr></thead><tbody>");
			}
		
			if ($row['enabled'] == 1 AND $letter['letterDescription'] != NULL) $tablecolour = "success"; else $tablecolour = "danger";
			print ("<tr id='row-types-".$row['recordID']."'>");
				print ("<td class='$tablecolour'>".$row['name']."</td>");
				if ($letter['letterDescription'] != NULL) {
					$arr = explode("/", $letter['fileLocation']);
					$filename = $arr[(count($arr)-1)];
					$arr = explode("_",$filename);
					$dateTime = $arr[0];
					$dateTime = explode(" ",str_replace ("-", "/",preg_replace('/-/', ' ', $dateTime, 1)));
					print ("<td class='$tablecolour'>");
						print("<a style='margin-right: 15px;' href='JavaScript:fnTestTemplate(".$letter['letterID'].");'>View Example</a>");
						print("<a style='margin-right: 15px;' href='".$letter['fileLocation']."' >Download Template</a>");
						print("<a style='margin-right: 0px;' href='JavaScript:fnModifyLetter(".$row['recordID'].");' >Change Letter</a>");
					print("</td>");
					
				} else {
					print ("<td class='$tablecolour' ><input type='file' id='fileToUpload-types".$row['recordID']."' name='fileToUpload-types".$row['recordID']."' onchange='fnNewLetterTemplate(".$row['recordID'].",\"types\", \"\"); return false;'></td>");
					$dateTime = Array("","");
				}
				print ("<td class='$tablecolour' style='color:grey;text-align:center'><a href='javascript:fnLetterHistory(".$row['letterGroupTypeID'].")'>".$dateTime[1]." ".$dateTime[0]."</a></td>");	
				
				print ("<td style='width: 60px;text-align:center' class='$tablecolour'><div class='switch'><input type='checkbox' id='switchtypes".$row['recordID']."' class='cmn-toggle cmn-toggle-round' onChange='fnEnableDisable(".$row['recordID'].",\"types\"); return false;' ".($row['enabled'] == 1 ? "checked" : "" )." /><label for='switchtypes".$row['recordID']."' class='switch-label'>".($row['enabled'] == 1 ? "Enabled" : "Disabled" )."</label></div></td>");
			print ("</tr>");
			
			$previousGroupType = $row['letterGroupTypeID'];					
		}
		print ("</tbody></table>");
		print ("</fieldset>");
		print("</div>");

	}
	
   function fnLetterMatrixDisplay($datnow, $conn){
		
		//low balance letters
		print("<div id='lowCredit'>");
		print("<p style='text-align:right'><a href='#' >Go To Top</a></p>");
		print ("<fieldset class='inlineLabels'><legend >Low Balance Letters</legend>");
			print ("<table style='table-layout: fixed;' class='table table-striped table-bordered table-hover table-condensed' ><thead>");
			print ("<tr>");
				print ("<th >Payment Method</th>");
				print ("<th style='width: 120px;'></th>");
				print ("<th style='width: 120px;'></th>");
				print ("<th style='width:310px;'>Letter</th>");
				print ("<th style='width: 70px;'>Last Mod.</th>");
				print ("<th style='width: 60px;'>Status</th>");
			print ("</tr></thead><tbody>");

			$strdbsql = "SELECT letterMatrix.*, configPaymentMethods.description AS payDesc
			FROM letterMatrix 
			INNER JOIN configPaymentMethods ON letterMatrix.paymentMethod = configPaymentMethods.paymentMethodID
			WHERE weeksDormant = 0 ORDER BY paymentMethod, lowBalanceTrigger ASC";
			$strType = "multi";
			$arrdbparams = array();
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if (count($resultdata) > 0) {
				foreach ($resultdata as $row) {	

					if (!empty($row['letterID'])) {
						$strdbsql = "SELECT letters.fileLocation, letters.description AS letterDescription, letters.letterID FROM letters WHERE groupID = :letterGroupID ORDER BY version DESC LIMIT 1";
						$strType = "single";
						$arrdbparams = array("letterGroupID"=>$row['letterID']);
						$letter = query($conn,$strdbsql,$strType,$arrdbparams);
					} else {
						
						$letter = null;
					}
					

					if ($row['enabled'] == 1 AND $letter['letterDescription'] != NULL) $tablecolour = "success"; else $tablecolour = "danger";
					print ("<tr id='row-matrix-".$row['recordID']."'>");
						print ("<td class='$tablecolour'>".$row['payDesc']."</td>"); 
						print ("<td class='$tablecolour'>".(($row['lowBalanceTrigger'] == -1) ? "No draws remaining": $row['lowBalanceTrigger']." draw/s remaining")."</td>");
						print ("<td class='$tablecolour'>");
						switch ($row['paymentMadeCheck']) {
							case 2: print ("With Payments"); break;
							case 1: print ("No Payments"); break;
						}
						print ("</td>");
						if ($letter['letterDescription'] != NULL) {
							print ("<td class='$tablecolour'>");
								print("<a style='margin-right: 15px;' href='JavaScript:fnTestTemplate(".$letter['letterID'].");'>View Example</a>");
								print("<a style='margin-right: 15px;' href='".$letter['fileLocation']."' >Download Template</a>");
								print("<a style='margin-right: 0px;' href='JavaScript:fnEditMatrix(".$row['recordID'].",\"lowBalance\", \"letter\");' >Change Letter</a>");
							print("</td>");

							$arr = explode("/", $letter['fileLocation']);
							$filename = $arr[(count($arr)-1)];
							$arr = explode("_",$filename);
							$dateTime = $arr[0];
							$dateTime = explode(" ",str_replace ("-", "/",preg_replace('/-/', ' ', $dateTime, 1)));							

						} else {
							print ("<td class='$tablecolour' ><input type='file' id='fileToUpload-matrix".$row['recordID']."' name='fileToUpload-matrix".$row['recordID']."' onChange='fnNewLetterTemplate(".$row['recordID'].",\"matrix\", \"lowBalance\"); return false;'></td>");
							$dateTime = Array("","");
						}
						print ("<td class='$tablecolour'  style='color:grey;text-align:center'><a href='javascript:fnLetterHistory(".$row['letterID'].")'>".$dateTime[1]." ".$dateTime[0]."</a></td>");
					
						
						print ("<td style='width: 60px;text-align:center' class='$tablecolour'><div class='switch'><input type='checkbox' id='switchmatrix".$row['recordID']."' class='cmn-toggle cmn-toggle-round' onChange='fnEnableDisable(".$row['recordID'].",\"matrix\"); return false;' ".($row['enabled'] == 1 ? "checked" : "" )." /><label for='switchmatrix".$row['recordID']."' class='switch-label'>".($row['enabled'] == 1 ? "Enabled" : "Disabled" )."</label></div></td>");
					print ("</tr>");
				}
			} else {
				print ("<tr><td class='danger' colspan='4'>No low balance letters found.</td></tr>");
			}
			print ("</tbody></table>");
			print ("<div class='form-group'>");
				print ("<div class='col-sm-14'>");
					print ("<button class='btn btn-primary' onclick='fnNewMatrix(\"lowBalance\"); return false;'>Add Low Balance Letter</button>");
				print ("</div>");
			print ("</div>");
		print ("</fieldset>");
		print("</div>");
		
		
		print("<div id='dormant'>");
					print("<p style='text-align:right'><a href='#' >Go To Top</a></p>");
		//dormant letters
		print ("<fieldset class='inlineLabels'><legend >Dormant Letters</legend>");
			print ("<table style='table-layout: fixed;' class='table table-striped table-bordered table-hover table-condensed' ><thead>");
			print ("<tr>");
				print ("<th >Payment Method</th>");
				print ("<th style='width: 120px;'></th>");
				print ("<th style='width: 120px;'></th>");
				print ("<th style='width:310px'>Letter</th>");
				print ("<th style='width: 70px;'>Last Mod.</th>");
				
				print ("<th style='width: 60px;'>Status</th>");
			print ("</tr></thead><tbody>");
			$strdbsql = "SELECT letterMatrix.*, configPaymentMethods.description AS payDesc
			FROM letterMatrix 
			INNER JOIN configPaymentMethods ON letterMatrix.paymentMethod = configPaymentMethods.paymentMethodID
			WHERE weeksDormant > 0 ORDER BY paymentMethod, weeksDormant ASC";
			$strType = "multi";
			$arrdbparams = array();
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			if (count($resultdata) > 0) {
				foreach ($resultdata as $row) {
				
					if (!empty($row['letterID'])) {
						$strdbsql = "SELECT letters.fileLocation, letters.description AS letterDescription, letters.letterID FROM letters WHERE groupID = :letterGroupID ORDER BY version DESC LIMIT 1";
						$strType = "single";
						$arrdbparams = array("letterGroupID"=>$row['letterID']);
						$letter = query($conn,$strdbsql,$strType,$arrdbparams);
					} else { 
						$letter = null;
					}
					
					
				
					if ($row['enabled'] == 1 AND $letter['letterDescription'] != NULL) $tablecolour = "success"; else $tablecolour = "danger";
					print ("<tr  id='row-matrix-".$row['recordID']."'>");
						print ("<td class='$tablecolour'>".$row['payDesc']."</td>"); 
						print ("<td class='$tablecolour'>".$row['weeksDormant']." draw/s missed</td>");
						print ("<td class='$tablecolour'>");
						switch ($row['paymentMadeCheck']) {
							case 2: print ("With Payments"); break;
							case 1: print ("No Payments"); break;
						}
						print ("</td>");
						if ($letter['letterDescription'] != NULL) {
							print ("<td class='$tablecolour'>");
								print("<a style='margin-right: 15px;' href='JavaScript:fnTestTemplate(".$letter['letterID'].");'>View Example</a>");
								print("<a style='margin-right: 15px;' href='".$letter['fileLocation']."' >Download Template</a>");
								print ("<a style='margin-right: 0px;' href='JavaScript:fnEditMatrix(".$row['recordID'].",\"dormat\",\"letter\");' >Change Letter</a>");
							print("</td>");
							$arr = explode("/", $letter['fileLocation']);
							$filename = $arr[(count($arr)-1)];
							$arr = explode("_",$filename);
							$dateTime = $arr[0];
							$dateTime = explode(" ",str_replace ("-", "/",preg_replace('/-/', ' ', $dateTime, 1)));
						} else {
							print ("<td class='$tablecolour' ><input type='file' id='fileToUpload-matrix".$row['recordID']."' name='fileToUpload-matrix".$row['recordID']."' onChange='fnNewLetterTemplate(".$row['recordID'].",\"matrix\", \"dormant\"); return false;'></td>");
							$dateTime =Array("","");
						}										
						print ("<td class='$tablecolour'  style='color:grey;text-align:center' ><a href='javascript:fnLetterHistory(".$row['recordID'].")'>".$dateTime[1]." ".$dateTime[0]."</a></td>");
						
						print ("<td style='width: 60px;text-align:center' class='$tablecolour'><div class='switch'><input type='checkbox' id='switchmatrix".$row['recordID']."' class='cmn-toggle cmn-toggle-round' onChange='fnEnableDisable(".$row['recordID'].",\"matrix\"); return false;' ".($row['enabled'] == 1 ? "checked" : "" )." /><label for='switchmatrix".$row['recordID']."' class='switch-label'>".($row['enabled'] == 1 ? "Enabled" : "Disabled" )."</label></div></td>");
					
					//	print ("<td style='width: 100px;' class='$tablecolour'><button onclick='fnEnableDisable(".$row['recordID'].",\"matrix\"); return false;'".($row['enabled'] == 1 ? "class='btn btn-success'>Enabled" : "class='btn btn-danger'>Disabled" )."</button></td>");
					print ("</tr>");
				}
			} else {
				print ("<tr><td class='danger' colspan='7'>No dormant letters found.</td></tr>");
			}
			print ("</tbody></table>");
			print ("<div class='form-group'>");
				print ("<div class='col-sm-14'>");
					print ("<button class='btn btn-primary' onclick='fnNewMatrix(\"dormant\"); return false;'>Add Dormant Letter</button>");
				print ("</div>");
			print ("</div>");
		print ("</fieldset>");
		print("</div>");
	}
	
   function fnLetterModification($letterTypeID, $datnow, $conn){
		
		$strdbsql = "SELECT * FROM letterTypes WHERE recordID = :recordID";
		$arrdbvalues = array("recordID" => $letterTypeID);
		$strType = "single";
		$letterType = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		$strdbsql = "SELECT * FROM letters WHERE groupID = :letterGroupID ORDER BY version DESC LIMIT 1";
		$arrdbvalues = array("letterGroupID"=>$letterType['letterGroupID']);
		$strType = "single";
		$letterDetails = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		print("<div class='section'>");
			print ("<h3>".$letterType['name']."</h3>");
			print ("<input type='hidden' class='nodisable' name='frm_letterID' id='frm_letterID' value='".$letterDetails['letterID']."'/>");
			print ("<input type='hidden' class='nodisable' name='frm_letterTypeID' id='frm_letterTypeID' value='".$letterTypeID."'/>");
			
			print("<div class='row'>");
				print("<div class='form-group col-sm-2 even-left'>");
					print ("<label for='fileToUpload' control-label'>Change File</label>");
					print ("<p class='help-block'>".$letterDetails['fileLocation']."</p>");
				print ("</div>");
				print("<div class='form-group col-sm-6 even-left'>");
					print ("<input type='file' id='fileToUpload' name='fileToUpload'>");
					print ("<p class='help-block'>Version will be incremented when changing the file. Valid Files: .docx</p>");
				print("</div>");
			print("</div>");
			
			print("<div class='row'>");
				print("<div class='col-xs-12 style='text-align:right;'>");
					print("<button onclick='return fnSaveTemplate(1);' type='submit' class='btn btn-success right' style='display:inline-block;'>Save and Reload</button> ");
				print("</div>");
			print("</div>");

			

		print ("</div>");
		
		print("<div class='section'>");
			print("<div class='row'>");
				fnLetterDataMatch($letterDetails['letterID'], "", $datnow, $conn);
			print("</div>");
			print("<br/>");
			print("<div class='row'>");
				print("<div class='col-xs-6' style='text-align:left;'>");
					print("<button onclick='return fnCancelTemplate();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
				print("</div>");
				print("<div class='col-xs-6 style='text-align:right;'>");
					print("<button onclick='return fnSaveTemplate(0);' type='submit' class='btn btn-success right' style='display:inline-block;'>Save</button> ");
					print("<button onclick='return fnSaveTemplate(1);' type='submit' class='btn btn-success right' style='display:inline-block; margin-right:10px;'>Save and Reload</button> ");
				print("</div>");
			print("</div>");
		print ("</div>");
		
		print("<div class='section'>");
		fnShowLetterHelp($datnow, $letterDetails['letterID'], $conn);
		print ("</div>");
	}
	
    function fnMatrixLetterModification($letterMatrixID, $communicationType, $newMatrixType, $datnow, $conn){
		
		$strdbsql = "SELECT * FROM letterMatrix WHERE recordID = :recordID";
		$arrdbvalues = array("recordID" => $letterMatrixID);
		$strType = "single";
		$matrixDetails = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		$strdbsql = "SELECT * FROM letters WHERE groupID = :letterGroupID ORDER BY version DESC LIMIT 1";
		$arrdbvalues = array("letterGroupID"=>$matrixDetails['letterID']);
		$strType = "single";
		$letterDetails = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		if ($newMatrixType == "dormant" || $matrixDetails['weeksDormant'] > 0) {
			$newMatrixType = "Dormant";
		} else {
			$newMatrixType = "Low Balance";
		}
		
		print ("<fieldset class='inlineLabels'><legend>$newMatrixType Letter</legend>");
		
			//letterIDs
			print ("<input type='hidden' class='nodisable' name='frm_letterID' id='frm_letterID' value='".$letterDetails['letterID']."'/>");
			print ("<input type='hidden' class='nodisable' name='frm_letterMatrixID' id='frm_letterMatrixID' value='".$letterMatrixID."'/>");
			
			//change file
			print ("<div class='form-group'>");
				print ("<label for='fileToUpload' class='col-sm-2 control-label'>Change File</label><div class='col-sm-10'>");
				print ("<p class='help-block'>".$letterDetails['fileLocation']."</p>");
				print ("<input type='file' id='fileToUpload' name='fileToUpload'>");
				print ("<p class='help-block'>Version will be incremented when changing the file. Valid Files: .docx</p>");
			print ("</div></div>");
			print ("<div class='form-group'>");
				print ("<div class='col-sm-offset-2 col-sm-6'>");
					print ("<button class='btn btn-success' style='margin-left:10px;' onclick='fnSaveMatrix(1); return false;'>Save and Reload</button>");
				print ("</div>");
			print ("</div>");

			//payment method
			print ("<div class='form-group'><label for='frm_paymenttype' class='col-sm-2 control-label'>Payment Type: </label>");
				print ("<div class='col-sm-4'>");
					print ("<select name='frm_paymenttype' id='frm_paymenttype' class='form-control' >");
						print ("<option value=''>-- Please Select --</option>");
						$strdbsql = "SELECT * FROM configPaymentMethods ORDER BY description ASC";
						$strType = "multi";
						$resultdata = query($conn, $strdbsql, $strType);
						
						foreach ($resultdata as $row) {
							if ($row['paymentMethodID'] == $matrixDetails['paymentMethod']) $selected = "selected"; else $selected = "";
							print ("<option $selected value='".$row['paymentMethodID']."'>".$row['description']."</option>");
						}
					print ("</select>");
				print ("</div>");
			print ("</div>");
			
			switch ($newMatrixType){
				case "Low Balance":
					//low balance trigger
					print ("<div class='form-group'><label for='frm_lowbalance' class='col-sm-2 control-label'>Low Balance Trigger: </label>");
						print ("<div class='col-sm-3'>");
							print ("<select name='frm_lowbalance' id='frm_lowbalance' class='form-control' >");
							print ("<option value=''>Not Required</option>");
							if ($matrixDetails['lowBalanceTrigger'] == -1) $selected = "selected"; else $selected = "";
							print ("<option $selected value='-1'>No credit</option>");
							for ($i = 1; $i < 15; $i++) {
								if ($matrixDetails['lowBalanceTrigger'] == $i) $selected = "selected"; else $selected = "";
								print "<option $selected value='$i'>$i draw/s remaining</option>";
							}
							print ("</select>");
						print ("</div>");
					print ("</div>");
				break;
				case "Dormant":
					//dormant trigger
					print ("<div class='form-group'><label for='frm_drawsdormant' class='col-sm-2 control-label'>Draws Dormant: </label>");
						print ("<div class='col-sm-3'>");
							print ("<select name='frm_drawsdormant' id='frm_drawsdormant' class='form-control' >");
							print ("<option value=''>Not Required</option>");
							for ($i = 1; $i < 15; $i++) {
								if ($matrixDetails['weeksDormant'] == $i) $selected = "selected"; else $selected = "";
								print "<option $selected value='$i'>$i draw/s missed</option>";
							}
							print ("</select>");
						print ("</div>");
					print ("</div>");
				break;
			}
			
			//payments check
			$checkPaymentsArray[0] = "Not Required";
			$checkPaymentsArray[1] = "Only Accounts without Payments";
			$checkPaymentsArray[2] = "Only Accounts with Payments";
			print ("<div class='form-group'><label for='frm_paymentscheck' class='col-sm-2 control-label'>Payments Check: </label>");
				print ("<div class='col-sm-3'>");
					print ("<select name='frm_paymentscheck' id='frm_paymentscheck' class='form-control' >");
					foreach ($checkPaymentsArray AS $key => $value) {
						if ($matrixDetails['paymentMadeCheck'] == $key) $selected = "selected"; else $selected = "";
						print "<option $selected value='$key'>$value</option>";
					}
					print ("</select>");
				print ("</div>");
			print ("</div>");
			
			print ("</fieldset>");	
			
			if (!empty($letterDetails['letterID'])) {
				fnLetterDataMatch($letterDetails['letterID'], "", $datnow, $conn);
			}
			
			

			//buttons
			print ("<div class='form-group'>");
				print ("<div class='col-sm-offset-2 col-sm-6'>");
					print ("<button class='btn btn-success' onclick='fnSaveMatrix(0); return false;'>Save</button>");
					print ("<button class='btn btn-success' style='margin-left:10px;' onclick='fnSaveMatrix(1); return false;'>Save and Reload</button>");
					print ("<button class='btn btn-default' style='margin-left:10px;' onclick='fnCancelMatrix(); return false;'>Cancel</button>");
				print ("</div>");
			print ("</div>");
			
			fnShowLetterHelp($datnow, $letterDetails['letterID'], $conn);
		
	}
	
    function fnGetLetterFields($letterID,$conn) {
		$strdbsql = "SELECT * FROM letters WHERE letterID = :letterID";
		$arrdbvalues = array("letterID" => $letterID);
		$strType = "single";
		$letterDetails = query($conn, $strdbsql, $strType, $arrdbvalues);

		$filelocation = $_SERVER['DOCUMENT_ROOT'].$letterDetails['fileLocation'];

		$zip = new ZipArchive();
		if( $zip->open($filelocation, ZIPARCHIVE::CHECKCONS ) !== TRUE ) { echo "failed to open template"; exit; }
		$file = 'word/document.xml';
		$data = $zip->getFromName($file);
		$zip->close();

		// Create the XML parser and create an array of the results
		$parser = xml_parser_create_ns();
		xml_parse_into_struct($parser, $data, $vals, $index);
		xml_parser_free($parser);

		// Cycle the index array looking for the important key and save those items to another array
		foreach ($index as $key => $indexitem) {
			if ($key == 'HTTP://SCHEMAS.OPENXMLFORMATS.ORG/WORDPROCESSINGML/2006/MAIN:INSTRTEXT') {
				$found = $indexitem;
				break;
			}
		}

		// Cycle *that* array looking for "MERGEFIELD" and grab the field name to yet another array
		// Make sure to check for duplicates since fields may be re-used
		if ($found) {
			$dataarray = array();
			foreach ($found as $field) {
				if (substr($vals[$field]['value'], 0, 11) == " MERGEFIELD") {
					if (!in_array(strtolower(str_replace('"', '', trim(substr($vals[$field]['value'], 12)))), $dataarray)) {
						$datadetails = explode("\\",strtolower(str_replace('"', '', trim(substr($vals[$field]['value'], 12)))));
						$dataitem = trim($datadetails[0]);
						unset($datadetails);
						$dataarray[] = $dataitem;
					}
				}
			}
			return $dataarray;
		} else {
			return false;
		}
		
	}
	
    function fnLetterDataMatch($letterID, $returnLocation, $datnow, $conn){
		
		$dataarray= fnGetLetterFields($letterID,$conn);

		$strdbsql = "SELECT * FROM letters WHERE letterID = :letterID";
		$arrdbvalues = array("letterID" => $letterID);
		$strType = "single";
		$letterDetails = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		print ("<h3>Data Match</h3>");
		
		$strdbsql = "SELECT * FROM letterMergeFields";
		$strType = "multi";
		$arrdbparams = array();
		$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
		
		$customrules = array();
		if (count($resultdata) > 0) {
			foreach ($resultdata as $row) {	
				$customrules[] = $row['mergeField'];
			}
		}
		
		//each merge item found in the file needs matching up
		foreach ($dataarray AS $dataitem) {
			if (!in_array($dataitem, $customrules)) {
			
					print ("<label for='frm_match[$dataitem]' class='col-sm-2 control-label'>".$dataitem."</label>");
					print ("<div class='col-sm-4'><select name='frm_match[$dataitem]' id='frm_match[$dataitem]' class='form-control' >");
						print ("<option value=''>-- Do not match --</option>");
						$requiredData = unserialize($letterDetails['requiredData']);
						foreach ($requiredData['tables'] AS $table) {
							$strdbsql = "SHOW COLUMNS FROM $table";
							$strType = "multi";
							$resultdata = query($conn, $strdbsql, $strType);
							foreach ($resultdata as $row) {
								$selected = "";
								if (isset($requiredData['columns']) && $requiredData['columns'][$dataitem] == $table.".".$row['Field']) {
									$selected = "selected";
								} else {
									switch ($dataitem) {
										case "add1":
										case "address1":
										case "playeraddress1":
											if ($row['Field'] == "add1" AND (!isset($requiredData['columns'])  OR !in_array("memberContact.add1", $requiredData['columns']))) $selected = "selected";
										break;
										case "add2":
										case "address2":
										case "playeraddress2":
											if ($row['Field'] == "add2" AND (!isset($requiredData['columns'])  OR !in_array("memberContact.add2", $requiredData['columns']))) $selected = "selected";
										break;
										case "add3":
										case "address3":
										case "playeraddress3":
											if ($row['Field'] == "add3" AND (!isset($requiredData['columns'])  OR !in_array("memberContact.add3", $requiredData['columns']))) $selected = "selected";
										break;
										case "town":
										case "playerposttown":
											if ($row['Field'] == "town" AND (!isset($requiredData['columns'])  OR !in_array("memberContact.town", $requiredData['columns']))) $selected = "selected";
										break;
										case "county":
										case "playerpostcounty":
											if ($row['Field'] == "county" AND (!isset($requiredData['columns'])  OR !in_array("memberContact.county", $requiredData['columns']))) $selected = "selected";
										break;
										case "country":
											if ($row['Field'] == "country" AND (!isset($requiredData['columns'])  OR !in_array("memberContact.country", $requiredData['columns']))) $selected = "selected";
										break;
										case "postcode":
										case "playerpostcode":
											if ($row['Field'] == "postcode" AND (!isset($requiredData['columns'])  OR !in_array("memberContact.postcode", $requiredData['columns']))) $selected = "selected";
										break;
									}
								}
								print ("<option $selected value='".$table.".".$row['Field']."'>".$table.".".$row['Field']."</option>");
							}
						}
						
					print ("</select>");
					print ("</div>");
			} else {
				print ("<input type='hidden' class='nodisable' name='frm_match[$dataitem]' id='frm_match[$dataitem]' value=''/>");
			}
		}
		
		/*print ("<div class='form-group'>");
			print ("<div class='col-sm-offset-2 col-sm-4'>");
				print ("<button class='btn btn-success' onclick='fnSaveDataMatch(\"$returnLocation\"); return false;'>Save</button>");
				print ("<button class='btn btn-default' style='margin-left:10px;' onclick='fnCancelMatch(\"$returnLocation\"); return false;'>Cancel</button>");
			print ("</div>");
		print ("</div>");*/
	}
	

	switch($strcmd){
		
		case "saveTemplate":
		
			$letterID = $_REQUEST['frm_letterID'];
			$letterTypeID = $_REQUEST['frm_letterTypeID'];
		
			$strdbsql = "SELECT * FROM letterTypes WHERE recordID = :recordID";
			$arrdbvalues = array("recordID"=>$letterTypeID);
			$strType = "single";
			$letterType = query($conn, $strdbsql, $strType, $arrdbvalues);
			
			if ($_FILES['fileToUpload'] AND !empty($_FILES['fileToUpload'])) {

				$file = $_FILES['fileToUpload'];
				if ($file['name'] != "") {

					$fulldata = array();
					$original_extension = (false === $pos = strrpos($file["name"], '.')) ? '' : substr($file["name"], $pos);
					$checkSum = md5($file["name"].$file["size"]);					

					$directory = "/files/letters/";
					$filename = $directory.fnconvertunixtime($datnow,"H:i-d-m-Y")."_".$file["name"];
			
					//create directory if it does not currently exist
					if (!file_exists($directory)){$makedir = recursive_mkdir($directory);}
					
					//copy file into the raw directory
					$moveResult = copy($file["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$filename);
					if ($moveResult != true) {
						$strerror = ("Could not move file to the raw data directory ($filename)");
					} else {

						if (strtolower($original_extension) == '.docx') {

							if ($letterID != "") {
								
								$strdbsql = "INSERT INTO letters (description, version, status, requiredData, groupID, fileLocation, contactType) SELECT description, version + 1, 1, requiredData, groupID, :fileLocation, contactType FROM letters WHERE letterID = :letterID";
								$strType = "insert";
								$arrdbparams = array("fileLocation"=>$filename,"letterID"=>$letterID);
								$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
								$letterID = $resultdata;
							
							} else {
								
								$requiredData = array();
								$strdbsql = "SELECT databaseTable FROM exportTables ORDER BY description ASC";
								$strType = "multi";
								$resultdata = query($conn, $strdbsql, $strType);
								foreach ($resultdata AS $row){
									$requiredData['tables'][] = $row['databaseTable'];
								}
								$requiredData = serialize($requiredData);
								
								$strdbsql = "INSERT INTO letters (description, fileLocation, version, status, requiredData) VALUES (:templateDescription, :fileLocation, 1, 1, :requiredData)";
								$strType = "insert";
								$arrdbparams = array("templateDescription"=>$file["name"], "fileLocation"=>$filename,"requiredData"=>$requiredData);
								$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
								$letterID = $resultdata;
								
								$strdbsql = "UPDATE letters SET groupID = :groupID WHERE letterID = :letterID";
								$strType = "update";
								$arrdbparams = array("letterID"=>$letterID, "groupID"=>$letterID);
								$updateresult = query($conn, $strdbsql, $strType, $arrdbparams);
								
								$strdbsql = "UPDATE letterTypes SET letterGroupID = :letterGroupID WHERE recordID = :recordID";
								$strType = "update";
								$arrdbparams = array("letterGroupID"=>$letterID, "recordID"=>$letterTypeID);
								$updateresult = query($conn, $strdbsql, $strType, $arrdbparams);
							}

						} else {
							$strerror = ("File Extension $original_extension not recognised or not supported.");
						}
					}
				}
			}

			//get the required data columns
			$strdbsql = "SELECT requiredData FROM letters WHERE letterID = :letterID";
			$strType = "single";
			$arrdbparams = array("letterID"=>$letterID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$requiredData = unserialize($resultdata['requiredData']);
			$requiredData['columns'] = $_REQUEST['frm_match'];
			$requiredData = serialize($requiredData);
			
			$strdbsql = "UPDATE letters SET requiredData = :requiredData WHERE letterID = :letterID";
			$strType = "update";
			$arrdbparams = array("requiredData"=>$requiredData,"letterID"=>$letterID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

			
			switch ($reload) {
				case 0: //do not reload
					$strsuccess = "Letter Type Successfully Saved";
					$strcmd = "";
				break;
				case 1: //reload
					$strsuccess = "Letter Type Successfully Saved";
					$strcmd = "letterModification";
				break;
			}
		
		break;		
		
		case "letterCreate":
		
			switch ($updateTable) {
				case "types":
					$file = $_FILES['fileToUpload-types'.$recordID];
				break;
				case "matrix":
					$file = $_FILES['fileToUpload-matrix'.$recordID];
				break;
			}
			if ($file) { //file uploaded
			
				$fulldata = array();
				$original_extension = (false === $pos = strrpos($file["name"], '.')) ? '' : substr($file["name"], $pos);
				$checkSum = md5($file["name"].$file["size"]);					

				$directory = "/files/letters/";
				$filename = $directory.fnconvertunixtime($datnow,"H:i-d-m-Y")."_".$file["name"];
						
				//create directory if it does not currently exist
				if (!file_exists($directory)){$makedir = recursive_mkdir($directory);}
				
				//copy file into the raw directory
				$moveResult = copy($file["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$filename);
				if ($moveResult != true) {
					$strerror = ("Could not move file to the raw data directory ($filename)");
					$strcmd = "";
				} else {

					if (strtolower($original_extension) == '.docx') {
						
						$strsuccess = "File successfully uploaded - ".$file['name']." to $filename";
						
						$requiredData = array();
						$strdbsql = "SELECT databaseTable FROM exportTables ORDER BY description ASC";
						$strType = "multi";
						$resultdata = query($conn, $strdbsql, $strType);
						foreach ($resultdata AS $row){
							$requiredData['tables'][] = $row['databaseTable'];
						}
						$requiredData = serialize($requiredData);
						
						$strdbsql = "INSERT INTO letters (description, fileLocation, version, status, requiredData) VALUES (:templateDescription, :fileLocation, 1, 1, :requiredData)";
						$strType = "insert";
						$arrdbparams = array("templateDescription"=>$file["name"], "fileLocation"=>$filename,"requiredData"=>$requiredData);
						$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
						$letterID = $resultdata;
						
						$strdbsql = "UPDATE letters SET groupID = :groupID WHERE letterID = :letterID";
						$strType = "update";
						$arrdbparams = array("letterID"=>$letterID, "groupID"=>$letterID);
						$updateresult = query($conn, $strdbsql, $strType, $arrdbparams);
						
						switch ($updateTable) {
							case "types": 
								$letterTable = "letterTypes"; 
								$strcmd = "letterModification";
								
								//update database
								$strdbsql = "UPDATE letterTypes SET letterGroupID = :letterGroupID WHERE recordID = :recordID";
								$strType = "update";
								$arrdbparams = array("letterGroupID"=>$letterID, "recordID"=>$recordID);
								$updateresult = query($conn, $strdbsql, $strType, $arrdbparams);
								$letterTypeID = $recordID;
								
							break;
							case "matrix": 
								$letterTable = "letterMatrix"; 
								$strcmd = "matrixLetterModification";
								
								//update database
								$strdbsql = "UPDATE letterMatrix SET letterID = :letterGroupID WHERE recordID = :recordID";
								$strType = "update";
								$arrdbparams = array("letterGroupID"=>$letterID, "recordID"=>$recordID);
								$updateresult = query($conn, $strdbsql, $strType, $arrdbparams);
								$letterMatrixID = $recordID;
								
							break;
						}
						$strsuccess = "New letter template uploaded";	
					
					} else {
						$strerror = ("File Extension $original_extension not recognised or not supported.");
						$strcmd = "";
					}
				}
				
			} else {
				$strerror = "No file to upload";
				$strcmd = "";
			}

		break;
		case "saveDataMatch":
			
			//get the required data columns
			$strdbsql = "SELECT requiredData FROM letters WHERE letterID = :letterID";
			$strType = "single";
			$arrdbparams = array("letterID"=>$letterID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$requiredData = unserialize($resultdata['requiredData']);
			$requiredData['columns'] = $_REQUEST['frm_match'];
			$requiredData = serialize($requiredData);
			
			$strdbsql = "UPDATE letters SET requiredData = :requiredData WHERE letterID = :letterID";
			$strType = "update";
			$arrdbparams = array("requiredData"=>$requiredData,"letterID"=>$letterID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

			$strsuccess = "Data match successfully saved";
			$strcmd = "modifyFormat";

		break;
		case "testTemplate":
		
			$strdbsql = "SELECT member.payContact AS recordID, account.membershipID AS memberID, account.accountID, number.numberID FROM number INNER JOIN account ON number.accountID = account.accountID INNER JOIN member ON account.membershipID = member.membershipID WHERE number.status = 1 GROUP BY account.membershipID ORDER BY account.paymentMethod LIMIT 5 ";
			$strType = "multi";
			$arrdbparams = array();
			$lookupData = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "SELECT groupID FROM letters WHERE letterID = :letterID";
			$strType = "single";
			$arrdbparams = array("letterID"=>$letterID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			printLetter($resultdata['groupID'], $lookupData, $datnow, $conn, false, false, "");

		break;
		
		case "enableDisableLetter":
			
			switch ($updateTable) {
				case "types": $letterTable = "letterTypes"; break;
				case "matrix": $letterTable = "letterMatrix"; break;
			}
			
			$strdbsql = "UPDATE $letterTable SET enabled = CASE WHEN enabled = 0 THEN 1 ELSE 0 END WHERE recordID = :recordID";
			$strType = "update";
			$arrdbparams = array("recordID"=>$recordID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			$strcmd = "";
			
		break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");
		
			print ("<h1>Communication Configuration</h1>");

			//Print out debug and error messages
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

			?>	
			<script language='Javascript' >	
				function fnSaveTemplate(reload) {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "saveTemplate";
						document.getElementById("reload").value = reload;
						document.getElementById("form").submit();
					}
				}
				function fnSaveEmailTemplate(reload) {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "saveEmailTemplate";
						document.getElementById("reload").value = reload;
						document.getElementById("form").submit();
					}
				}
				function fnCancelTemplate(){
					document.getElementById("cmd").value = "cancelTemplate";
					document.getElementById("form").submit();
				}	
				function fnFieldList(){
					document.getElementById("cmd").value = "fieldList";
					document.getElementById("form").submit();
				}			
				function fnDataMatch() {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "dataMatch";
						document.getElementById("form").submit();
					}
				}
				function fnSaveDataMatch(returnLocation){
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "saveDataMatch";
						document.getElementById("returnLocation").value = returnLocation;
						document.getElementById("form").submit();
					}
				}
				function fnCancelMatch(returnLocation){
					document.getElementById("cmd").value = returnLocation;
					document.getElementById("form").submit();
				}				
				function fnModifyLetter(letterTypeID){
					if ($('#form').valid()) {
						document.getElementById("letterTypeID").value = letterTypeID;
						document.getElementById("cmd").value = "letterModification";
						document.getElementById("form").submit();
					}
				}
				function fnNewLetterTemplate(recordID,updateTable,newMatrixType){
					if ($('#form').valid()) {
						document.getElementById("recordID").value = recordID;
						document.getElementById("updateTable").value = updateTable;
						document.getElementById("newMatrixType").value = newMatrixType;
						document.getElementById("cmd").value = "letterCreate";
						document.getElementById("form").submit();
					}
				}
				function fnTestTemplate(letterID) {
					if ($('#form').valid()) {
						document.getElementById("letterID").value = letterID;
						document.getElementById("cmd").value = "testTemplate";
						document.getElementById("form").submit();
					}
				}
				function fnEnableDisable(recordID,updateTable){
					if ($('#form').valid()) {
						/*document.getElementById("recordID").value = recordID;
						document.getElementById("updateTable").value = updateTable;
						document.getElementById("cmd").value = "enableDisableLetter";
						document.getElementById("form").submit();*/
						var checkbox = $("#switch"+updateTable+recordID);
						
						$.ajax({
						  type: "POST",
						  url: "/includes/ajax_configLetters.php",
						  data: "cmd=enableDisableLetter&updateTable="+updateTable+"&recordID="+recordID,
						  success: function(jsonData) {
							  var dia;
							 // alert("Oi");
							 console.log(jsonData);
							 if(jsonData.status == 1) {
								 if($("#row-"+updateTable+"-"+recordID+" > .danger").length > 0) {
									$("#row-"+updateTable+"-"+recordID+" > .danger").removeClass("danger").addClass("success");
								 } else {
									$("#row-"+updateTable+"-"+recordID+" > .success").removeClass("success").addClass("danger");
								 }
							 } else {
								dia = $("<div><div class='notification-error not-erro'><h3>Error</h3><p>"+jsonData.msg+"<p></div></div>").dialog();
								checkbox.prop("checked",false);
								$("#row-"+updateTable+"-"+recordID+" > .success").removeClass("success").addClass("danger");
								setTimeout(function() {dia.dialog("close");},3000);
							 }
							 return true;
						  },
						  error:function(jsonData) {
							  var tmpData = "An error has occured.";
							  if(jsonData.error.length > 0) tmpData = jsonData.msg;
							$("<div><div class='notification-error not-erro'><h3>Error</h3><p>"+tmpData+"<p></div></div>").dialog();
							
							checkbox.prop("checked",!checkbox.prop("checked"));
							
						  },
						  dataType: "json"
						});
					}
				}			
				function fnLetterHistory(letterID,updateTable){
					if ($('#form').valid()) {
						document.getElementById("letterID").value = letterID;
						document.getElementById("updateTable").value = updateTable;
						document.getElementById("cmd").value = "history";
						document.getElementById("form").submit();
					}
				}
				
			</script>
			<?php

			print ("<form action='config-letters.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data'>");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' value='$strcmd'/>");
			print ("<input type='hidden' class='nodisable' name='recordID' id='recordID' value='$recordID'/>");
			print ("<input type='hidden' class='nodisable' name='letterID' id='letterID' value='$letterID'/>");
			print ("<input type='hidden' class='nodisable' name='letterTypeID' id='letterTypeID' value='$letterTypeID'/>");
			print ("<input type='hidden' class='nodisable' name='letterMatrixID' id='letterMatrixID' value='$letterMatrixID'/>");
			print ("<input type='hidden' class='nodisable' name='communicationType' id='communicationType' value='$communicationType'/>");
			print ("<input type='hidden' class='nodisable' name='newMatrixType' id='newMatrixType' value='$newMatrixType'/>");
			print ("<input type='hidden' class='nodisable' name='returnLocation' id='returnLocation' value='$returnLocation'/>");
			print ("<input type='hidden' class='nodisable' name='updateTable' id='updateTable' value='$updateTable'/>");
			print ("<input type='hidden' class='nodisable' name='reload' id='reload' value='$reload'/>");
								
			switch ($strcmd) {
				case "letterDataMatch":
					fnLetterDataMatch($letterID, $returnLocation, $datnow, $conn);
				break;
				case "letterCreate":
				case "letterModification":
					fnLetterModification($letterTypeID, $datnow, $conn);
				break;
				case "fieldList":
					print ("<div class='form-group'>");
						print ("<div class='col-sm-offset-6 col-sm-6'>");
							print ("<button class='btn btn-default right' style='margin-left:10px;' onclick='fnCancelTemplate(); return false;'>Cancel</button>");
						print ("</div>");
					print ("</div>");
					fnShowLetterHelp($datnow, null, $conn, null,null);
					print ("<div class='form-group'>");
						print ("<div class='col-sm-offset-6 col-sm-6'>");
							print ("<button class='btn btn-default right' style='margin-left:10px;' onclick='fnCancelTemplate(); return false;'>Cancel</button>");
						print ("</div>");
					print ("</div>");
				break;
				case "history":
					print ("<div class='form-group'>");
						print ("<div class='col-sm-offset-6 col-sm-6'>");
							print ("<button class='btn btn-default right' style='margin-left:10px;' onclick='fnCancelTemplate(); return false;'>Cancel</button>");
						print ("</div>");
					print ("</div>");
					fnShowLetterHistory($datnow, $letterID, $conn);
					print ("<div class='form-group'>");
						print ("<div class='col-sm-offset-6 col-sm-6'>");
							print ("<button class='btn btn-default right' style='margin-left:10px;' onclick='fnCancelTemplate(); return false;'>Cancel</button>");
						print ("</div>");
					print ("</div>");
				break;
				default:
					fnLetterTypeDisplay($datnow, $conn);
				break;
			}

			print ("</form>");
		print("</div>");
	print("</div>");

	?>
	<script language='Javascript' >
		$().ready(function() {
			// validate signup form on keyup and submit
			$("#form").validate({
				errorPlacement: function(error,element) {
					error.insertAfter(element);
				},
				rules: {
				}
			});
		});
	</script>
	<?php
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null;

?>