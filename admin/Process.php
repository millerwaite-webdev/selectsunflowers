<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * customer-groups.php			                                       * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "paymentSenseProcess"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	require_once ('PaymentFormHelper.php');
	include ('PaymentSenseConfig.php');

	if (isset($_REQUEST['orderHeaderID'])) $orderHeaderID = $_REQUEST['orderHeaderID']; else $orderHeaderID = "";
	if (isset($_REQUEST['amount'])) $amount = $_REQUEST['amount']; else $amount = 0;

	if (!empty($orderHeaderID)){
		$strdbsql = "SELECT order_header.*, customer.firstname, customer.surname, customer.title, customer.company, customer.mobile, customer.fax FROM order_header LEFT JOIN customer ON order_header.customerID = customer.recordID WHERE order_header.recordID = :recordID";
		$strType = "single";
		$arrdbparams = array("recordID"=>$orderHeaderID);
		$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
		
		$strfirstname = $orderHeader['firstname'];
		$strsurname = $orderHeader['surname'];
		$strtitle = $orderHeader['title'];
		$strcompany = $orderHeader['company'];
		$strphone = $orderHeader['telephone'];
		$strmobile = $orderHeader['mobile'];
		$stremail = $orderHeader['email'];
		$strfax = $orderHeader['fax'];
		$strnotes = $orderHeader['notes'];
		$strdelfirstname = $orderHeader['addDeliveryFirstname'];
		$strdelsurname = $orderHeader['addDeliverySurname'];
		$strdeltitle = $orderHeader['addDeliveryTitle'];
		$strdeladd1 = $orderHeader['addDelivery1'];
		$strdeladd2 = $orderHeader['addDelivery2'];
		$strdeladd3 = $orderHeader['addDelivery3'];
		$strdelTown = $orderHeader['addDelivery4'];
		$strdelCnty = $orderHeader['addDelivery5'];
		$strdelCntry = $orderHeader['addDelivery6'];
		$strdelPstCde = $orderHeader['addDeliveryPostcode'];
		$strbilfirstname = $orderHeader['addBillingFirstname'];
		$strbilsurname = $orderHeader['addBillingSurname'];
		$strbiltitle = $orderHeader['addBillingTitle'];
		$strbiladd1 = $orderHeader['addBilling1'];
		$strbiladd2 = $orderHeader['addBilling2'];
		$strbiladd3 = $orderHeader['addBilling3'];
		$strbilTown = $orderHeader['addBilling4'];
		$strbilCnty = $orderHeader['addBilling5'];
		$strbilCntry = $orderHeader['addBilling6'];
		$strbilPstCde = $orderHeader['addBillingPostcode'];
	}
	
	
	
	$Width = 800;
	$FormAction = 'https://mms.'.$PaymentProcessorDomain.'/Pages/PublicPages/PaymentForm.aspx';
	include ('Templates/ProcessFormHeader.tpl');

	// the amount in *minor* currency (i.e. �10.00 passed as "1000")
	$szAmount = $amount * 100;
	// the currency	- ISO 4217 3-digit numeric (e.g. GBP = 826, USD = 840, EUR = 978)
	$szCurrencyCode = 826;
	// echo card type
	$szEchoCardType = $EchoCardType;
	// order ID
	$szOrderID = $orderHeaderID;
	// the transaction type - can be SALE or PREAUTH
	$szTransactionType = "SALE";
	// the GMT/UTC relative date/time for the transaction (MUST either be in GMT/UTC 
	// or MUST include the correct timezone offset)
	$szTransactionDateTime = date('Y-m-d H:i:s P');
	// order description
	$szOrderDescription = $strcompanyname." Sales Order";
	// these variables allow the payment form to be "seeded" with initial values
	$szCustomerName = $strbiltitle." ".$strbilfirstname." ".$strbilsurname;
	$szAddress1 = $strbiladd1; // Important for AVS Check 
	$szAddress2 = $strbiladd2;
	$szAddress3 = $strbiladd3;
	$szAddress4 = $strdelTown;
	$szCity = $strbilCnty;
	$szState = $strbilCntry;
	$szPostCode = $strbilPstCde; // Important for AVS Check
	// the country code - ISO 3166-1  3-digit numeric (e.g. UK = 826)
	$szCountryCode = 826;
	//Email Address
    $_SESSION['email'] = $stremail;//Paymentsense Amendment
	$szEmailAddress = $stremail;
	//Phone Number
	$_SESSION['phone_number'] = $strphone;//Paymentsense Amendment
	$szPhoneNumber = $strphone;
	// use these to control which fields on the hosted payment form are
	//editable
	$szEmailAddressEditable = $EmailAddressEditable;
	$szPhoneNumberEditable = $PhoneNumberEditable;
	// mandatory
	$szCV2Mandatory = $CV2Mandatory;
	$szAddress1Mandatory = $Address1Mandatory;
	$szCityMandatory = $CityMandatory;
	$szPostCodeMandatory = $PostCodeMandatory;
	$szStateMandatory = $StateMandatory;
	$szCountryMandatory = $CountryMandatory;
	// the URL on this system that the payment form will push the results to (only applicable for 
	// ResultDeliveryMethod = "SERVER")
	if ($ResultDeliveryMethod != "SERVER")
	{
		$szServerResultURL = '';
	}
	else
	{
		$szServerResultURL = PaymentFormHelper::getSiteSecureBaseURL().'ReceiveTransactionResult.php';
	}
	// set this to true if you want the hosted payment form to display the transaction result
	// to the customer (only applicable for ResultDeliveryMethod = "SERVER")
	if ($ResultDeliveryMethod != 'SERVER')
	{
		$szPaymentFormDisplaysResult = '';
	}
	else
	{
		$szPaymentFormDisplaysResult = PaymentFormHelper::boolToString(false);
	}
	// the callback URL on this site that will display the transaction result to the customer
	// (always required unless ResultDeliveryMethod = "SERVER" and PaymentFormDisplaysResult = "true")
	if ($ResultDeliveryMethod == 'SERVER' && PaymentFormHelper::stringToBool($szPaymentFormDisplaysResult) == false)
	{
		$szCallbackURL = PaymentFormHelper::getSiteSecureBaseURL().'callback.php';
	}
	else
	{
		$szCallbackURL = PaymentFormHelper::getSiteSecureBaseURL().'callback.php'; 
	}

	// get the string to be hashed
	$szStringToHash = PaymentFormHelper::generateStringToHash($MerchantID,
			        										  $Password,
			        										  $szAmount,
															  $szCurrencyCode,
															  $szEchoCardType,
															  $szOrderID,
															  $szTransactionType,
															  $szTransactionDateTime,
															  $szCallbackURL,
															  $szOrderDescription,
															  $szCustomerName,
															  $szAddress1,
															  $szAddress2,
															  $szAddress3,
															  $szAddress4,
															  $szCity,
															  $szState,
															  $szPostCode,
															  $szCountryCode,
															  $szEmailAddress,
															  $szPhoneNumber,
															  $szEmailAddressEditable,
															  $szPhoneNumberEditable,
															  $szCV2Mandatory,
															  $szAddress1Mandatory,
															  $szCityMandatory,
															  $szPostCodeMandatory,
															  $szStateMandatory,
															  $szCountryMandatory,
															  $ResultDeliveryMethod,
															  $szServerResultURL,
															  $szPaymentFormDisplaysResult,
			         		                                  $PreSharedKey,
			         		                                  $HashMethod);

	// pass this string into the hash function to create the hash digest
	$szHashDigest = PaymentFormHelper::calculateHashDigest($szStringToHash,
                        								   $PreSharedKey, 
                        								   $HashMethod);

														   
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Payment Sense Processing</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
													   
			print("<div class='section'>");


				print "<div style='font-size: 16pt; color: #000;'><p>Transferring to <strong><font color='#F7941D'>Payment</font><font color='#1E4783'>Sense</font></strong> Secure Payment Form</p></div>";
				print "<input type='hidden' name='HashDigest' value='$szHashDigest' />";
				print "<input type='hidden' name='MerchantID' value='$MerchantID' />";
				print "<input type='hidden' name='Amount' value='$szAmount' />";
				print "<input type='hidden' name='CurrencyCode' value='$szCurrencyCode' />";
				print "<input type='hidden' name='EchoCardType' value='$szEchoCardType' />";
				print "<input type='hidden' name='OrderID' value='$szOrderID' />";
				print "<input type='hidden' name='TransactionType' value='$szTransactionType' />";
				print "<input type='hidden' name='TransactionDateTime' value='$szTransactionDateTime' />";
				print "<input type='hidden' name='CallbackURL' value='$szCallbackURL' />";
				print "<input type='hidden' name='OrderDescription' value='$szOrderDescription' />";
				print "<input type='hidden' name='CustomerName' value='$szCustomerName' />";
				print "<input type='hidden' name='Address1' value='$szAddress1' />";
				print "<input type='hidden' name='Address2' value='$szAddress2' />";
				print "<input type='hidden' name='Address3' value='$szAddress3' />";
				print "<input type='hidden' name='Address4' value='$szAddress4' />";
				print "<input type='hidden' name='City' value='$szCity' />";
				print "<input type='hidden' name='State' value='$szState' />";
				print "<input type='hidden' name='PostCode' value='$szPostCode' />";
				print "<input type='hidden' name='CountryCode' value='$szCountryCode' />";
				print "<input type='hidden' name='EmailAddress' value='$szEmailAddress' />";
				print "<input type='hidden' name='PhoneNumber' value='$szPhoneNumber' />";
				print "<input type='hidden' name='EmailAddressEditable' value='$szEmailAddressEditable' />";
				print "<input type='hidden' name='PhoneNumberEditable' value='$szPhoneNumberEditable' />";
				print "<input type='hidden' name='CV2Mandatory' value='$szCV2Mandatory' />";
				print "<input type='hidden' name='Address1Mandatory' value='$szAddress1Mandatory' />";
				print "<input type='hidden' name='CityMandatory' value='$szCityMandatory' />";
				print "<input type='hidden' name='PostCodeMandatory' value='$szPostCodeMandatory' />";
				print "<input type='hidden' name='StateMandatory' value='$szStateMandatory' />";
				print "<input type='hidden' name='CountryMandatory' value='$szCountryMandatory' />";
				print "<input type='hidden' name='ResultDeliveryMethod' value='$ResultDeliveryMethod' />";
				print "<input type='hidden' name='ServerResultURL' value='$szServerResultURL' />";
				print "<input type='hidden' name='PaymentFormDisplaysResult' value='$szPaymentFormDisplaysResult' />";
				print "<input type='hidden' name='ServerResultURLCookieVariables' value='' />";
				print "<input type='hidden' name='ServerResultURLFormVariables' value='' />";
				print "<input type='hidden' name='ServerResultURLQueryStringVariables' value='' />";

			print ("</div>");

		print("</div>");
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>