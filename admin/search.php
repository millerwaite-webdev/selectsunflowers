<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * search.php                                                         * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "search"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	$intpage = $_GET['page'];
	if ($intpage == 0)
	{
		//page is 'search.php'
		$strquery = strip_tags($_POST['q']);
		if ($strquery == "")
		{
			$strquery = $_GET['q'];
			if ($strquery == "") {
				//defualt page loaded
				$intpage = 1;
				//$intcat = "x";
				$intdisplay = 10;
				$strsort = "stock.stockCode";
			}
		}
		else
		{
			//page loaded following a search form submission
			$intpage = 1;
			$strquery = strip_tags($_POST['q']);
			//$intcat = $_POST['cat'];
			$intdisplay = $_POST['display'];
			$strsort = $_POST['sort'];
		}
	}
	else {
		//page is 'search.php?page=...'
		//page loaded by clicking first/next/previous/last or basket add
		$strquery = strip_tags($_GET['q']);
		//$intcat = $_GET['cat'];
		$intdisplay = $_GET['display'];
		$strsort = $_GET['sort'];
	}
	
	// check default term/page associations
	$searchTermPagesQuery = "SELECT * FROM site_search_pages ORDER BY recordID";
	$strType = "multi";
	$searchTermPages = query($conn, $searchTermPagesQuery, $strType);

	//print("<span style='display: none;' id='searchquery'>".$strquery."</span>");
	
	foreach ($searchTermPages AS $searchTermPage)
	{
		//print("<span style='display: none;' id='searchpagedata'>".$resultdata['searchTerms']."</span>");
		$terms = unserialize($searchTermPage['searchTermsArray']);
		foreach($terms AS $term)
		{
			//print("<span style='display: none;' class='searchpageitems'>".$term."</span>");
			if(strcasecmp($term, $strquery) == 0)
			{
				header("Location: /".$searchTermPage['location']);
			}
		}
	}

	// Find Category Information from Database and Text File
	/*$getCategoryQuery = "SELECT * FROM category WHERE recordID = :catID";
	$strType = "single";
	$arrParam = array ( "catID" => $intcat );
	$category = query($conn, $getCategoryQuery, $strType, $arrParam);
	
	if (count($category) > 0)
	{
		$strcategory = "All Categories";
	}
	else
	{
		$strcategory = $category['contentName'];
	}*/
	
	if($strquery != "")
	{
		$meta['metaTitle'] = "Search Results for ".$strquery."";
	}
	else
	{
		$meta['metaTitle'] = "Search Products";
	}
	$meta['metaPageLink'] = "search.php";
	$meta['description'] = "Search products results page";
	$meta['keywords'] = "";
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
?>
<div id='basketpopup'></div>
<div id="pagemiddle">
<?php
	include("includes/inc_sidebar.php");
?>
    <div id="content">
            <div id="pagetext">
            <div id="search">
             <h2>Stollers Furniture &amp; Interiors - Search</h2>
<?php
		print ("<form id='productSearch' action='search.php' method='post' accept-charset='UTF-8'>");

		print ("<p><label for='searchString'>Search for:</label> <input id='searchString' name='q' type='text' size='50' value='".htmlentities($strquery, ENT_QUOTES)."' tabindex='1'/></p>");
		/*print ("<p><label for='searchCategory'>In category:</label>");
		print ("<select name='cat' id='searchCategory' tabindex='2'>");
			print ("<option value='x'>All Categories</option>");
			
			$getCategoriesQuery = "SELECT * FROM category INNER JOIN category_relations ON category.recordID = category_relations.parentID WHERE category.recordID = category_relations.childID AND level = 1 ORDER BY contentName ASC";
			$strType = "multi";
			$categories = query($conn, $getCategoriesQuery, $strType);
			
			foreach($categories AS $category)
			{
				print ("<option value='".$category['recordID']."'");
					if ($intcat == $category['recordID']) print (" selected='selected'");
				print (">".$category['contentName']."</option>");
			}
?>
                   </select></p>
<?php*/             print ("<p><label for='display'>Results per page: </label>");
                  print ("<select id='display' name='display' tabindex='3'>");
                  print ("<option value='10' ");
				  if($intdisplay == 10) print("selected='selected'");
				  print (">10</option>");
                  print ("<option value='25' ");
				  if($intdisplay == 25) print("selected='selected'");
				  print (">25</option>");
                  print ("<option value='50' ");
				  if($intdisplay == 50) print("selected='selected'");
				  print (">50</option>");
                  print ("<option value='100' ");
				  if($intdisplay == 100) print("selected='selected'");
				  print (">100</option>");
                  print ("</select>&nbsp;&nbsp;&nbsp;<label for='sort'>Sort by: </label>");
	              print ("<select id='sort' name='sort' tabindex='4'>");
                  print ("<option value='0' ");
				  if($strsort == "0") print("selected='selected'");
				  print (">Product Code</option>");
                  print ("<option value='1' ");
				  if($strsort == "1") print("selected='selected'");
				  print (">Name</option>");
                  print ("<option value='2' ");
				  if($strsort == "2") print("selected='selected'");
				  print (">Price</option>");
                  print ("</select></p>");
?>
                        <input class='short_button' id="submit" type="submit" value="Search" tabindex='5'/>
                 </form>
             </div><!-- end of search-->
<?php
if ($strquery != "") {
	switch($_REQUEST['sort'])
	{
		case 0:
			$strsort = "stock.stockCode";
			break;
		case 1:
			$strsort = "stock_group_information.name";
			break;
		case 2:
			$strsort = "stock.price";
			break;
	}
	$arrdbparams = array(
						"query" => "%".$strquery."%",
						"query2" => "%".$strquery."%",
						"query3" => "%".$strquery."%",
						"query4" => "%".$strquery."%"
					);
	$strType = "multi";
	
	/*if ($intcat != 'x') {
		$strdbsql = "SELECT DISTINCT stock.stockCode, stock.recordID AS stockRecordID, stock_brands.brandName, stock_group_information.*, stock.price, stock_dimensions.*, stock_attributes.description AS colourDescription FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID LEFT JOIN stock_category_relations ON stock_group_information.recordID = stock_category_relations.stockID LEFT JOIN stock_brands ON stock_brands.recordID = stock_group_information.brandID LEFT JOIN stock_attribute_relations ON stock.recordID = stock_attribute_relations.stockID LEFT JOIN stock_dimensions ON stock.dimensionID = stock_dimensions.recordID LEFT JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID AND stock_attributes.attributeTypeID = 3 WHERE (stock.stockCode LIKE :query OR stock_group_information.name LIKE :query2 OR stock_group_information.metaLink LIKE :query3 OR stock_group_information.description LIKE :query4) AND stock_category_relations.categoryID = :categoryID AND stock.stockCode != 'test' AND stock_group_information.statusID != 1 AND stock_group_information.statusID != 2 ORDER BY ".$strsort." ASC";
		$arrdbparams["categoryID"] =  $intcat;
	}
	else {*/
		$strdbsql = "SELECT DISTINCT stock.stockCode, stock.recordID AS stockRecordID, stock_brands.brandName, stock_group_information.*, stock.price, stock_dimensions.*, stock_attributes.description AS colourDescription FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID LEFT JOIN stock_category_relations ON stock_group_information.recordID = stock_category_relations.stockID LEFT JOIN stock_brands ON stock_brands.recordID = stock_group_information.brandID LEFT JOIN stock_attribute_relations ON stock.recordID = stock_attribute_relations.stockID LEFT JOIN stock_dimensions ON stock.dimensionID = stock_dimensions.recordID LEFT JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID AND stock_attributes.attributeTypeID = 3 WHERE (stock.stockCode LIKE :query OR stock_group_information.name LIKE :query2 OR stock_group_information.metaLink LIKE :query3 OR stock_group_information.description LIKE :query4) AND stock.stockCode != 'test' AND stock_group_information.statusID != 1 AND stock_group_information.statusID != 2 ORDER BY ".$strsort." ASC";
	//}
	//echo $strdbsql."<br/>";
	//var_dump($arrdbparams);
	$searchResults = query($conn, $strdbsql, $strType, $arrdbparams);
	
	$intresults = count($searchResults);
	$intlinetoggle = 1;
	$intresult = 1;
	$intfirst = (($intpage-1)*$intdisplay) +1;
	$intlast = ($intpage*$intdisplay);
	if ($intlast > $intresults) {
	    $intlast = $intresults;
	}
	if ($intresults%$intdisplay == 0) {
	$intpages = $intresults/$intdisplay;
	}
	else
	{
	$intpages = (($intresults-($intresults%$intdisplay))/$intdisplay)+1;
	}
	if ($intresults == 0)
	{
		print ("<div id='noResults'><!--should be shown instead of results table if search finds no matches-->");
		print ("<h2>Your search for '$strquery'<!-- in $strcategory returned no results--></h2>");
		print ("</div><!--end of no results message-->");
		print ("<br /><br />");
	}
	else
	{
		print ("<div id='results'>");
		print ("<div id='pages'><p>");
		//if ($intpage != 1) print ("<a href='search.php?page=1&amp;q=$strquery&amp;cat=$intcat&amp;display=$intdisplay&amp;sort=$strsort'>First</a> | ");
		if ($intpage != 1) print ("<a href='search.php?page=1&amp;q=$strquery&amp;display=$intdisplay&amp;sort=$strsort'>First</a> | ");
		//if ($intpage > 2) print ("<a href='search.php?page=".($intpage-1)."&amp;q=$strquery&amp;cat=$intcat&amp;display=$intdisplay&amp;sort=$strsort'>Previous</a> | ");
		if ($intpage > 2) print ("<a href='search.php?page=".($intpage-1)."&amp;q=$strquery&amp;display=$intdisplay&amp;sort=$strsort'>Previous</a> | ");
		print ("Page $intpage of $intpages");
		//if ($intpage < $intpages-1) print (" | <a href='search.php?page=".($intpage+1)."&amp;q=$strquery&amp;cat=$intcat&amp;display=$intdisplay&amp;sort=$strsort'>Next</a>");
		if ($intpage < $intpages-1) print (" | <a href='search.php?page=".($intpage+1)."&amp;q=$strquery&amp;display=$intdisplay&amp;sort=$strsort'>Next</a>");
		//if ($intpage != $intpages) print (" | <a href='search.php?page=$intpages&amp;q=$strquery&amp;cat=$intcat&amp;display=$intdisplay&amp;sort=$strsort'>Last</a>");
		if ($intpage != $intpages) print (" | <a href='search.php?page=$intpages&amp;q=$strquery&amp;display=$intdisplay&amp;sort=$strsort'>Last</a>");
		print ("</p></div>");
		print ("<br />");
			//print ("<form name='frm_form' action='http://jhbenson.co.uk/search.php?page=$intpage&amp;cat=$intcat&amp;q=$strquery&amp;display=$intdisplay&amp;sort=$strsort' method='post' onsubmit='javascript:pageTracker._linkByPost(this)'>");
			print ("<form name='frm_form' action='http://jhbenson.co.uk/search.php?page=$intpage&amp;q=$strquery&amp;display=$intdisplay&amp;sort=$strsort' method='post' onsubmit='javascript:pageTracker._linkByPost(this)'>");
			print ("<input type='hidden' name='command'/>");
			print ("<input type='hidden' name='product'/>");
		print ("<table id='resultsTable'>");
		print ("<thead><tr>");
		print ("<th style='width: 10%;'>Product Code</th>");
		print ("<th style='width: 30%;'>Name</th>");
		print ("<th style='width: 10%;'>Size</th>");
		print ("<th style='width: 15%;'>Colour</th>");
		print ("<th style='width: 10%;'>Price</th>");
		print ("<th style='width: 15%;'>Options</th>");
		print ("</tr></thead>\n<tbody>");

		foreach ($searchResults AS $searchResult)
		{
			if ($intresult>= $intfirst && $intresult<=$intlast) {
			if ($intlinetoggle == 1) {
				print ("<tr>");
				$intlinetoggle = 0;
			}
			else {
				print ("<tr class='evenrow'>");
				$intlinetoggle = 1;
			}
				print ("<td>".$searchResult['stockCode']."</td>");

				print ("<td><a href='/".getItemHref($searchResult)."'>".$searchResult['brandName']." ".$searchResult['name']."</a></td>");
				print ("<td>".$searchResult['description']."</td>");
				print ("<td>".$searchResult['colourDescription']."</td>");
				print ("<td style='text-align:right;'>");
				if ($searchResult['price'] > 0) {
					print ("&pound;&nbsp;".number_format($searchResult['price'],2));
				}
				print ("</td>");
				print ("<td style='text-align:right;'>");
				if ($searchResult['price'] > 0 && $searchResult['statusID'] != 5) {
					print("<input type='hidden' id='qty_".$searchResult['stockRecordID']."' value = '1' /><a href='#' class='add-basket' name='but".$searchResult['stockRecordID']."' onclick='jsadd(\"".$searchResult['stockRecordID']."\")'>Add to Basket</a>");
				
				}
				print("</td>");
				print ("</tr>");
			}
			$intresult = $intresult + 1;
		}
		print ("</tbody></table>");
		print ("</form>");
		print ("<br />");
		print ("<div id='noofresults'><p>$intfirst - $intlast of $intresults products found in search for '$strquery'<!-- in&nbsp; '$strcategory'--></p></div>");
		print ("</div><!--end of search results-->");
	}
	if($intpage == 1) {
		$strdbsql = "SELECT * FROM site_search_history WHERE searchTerm = :query";
		$strType = "multi";
		$arrdbparam = array ( "query" => $strquery );
		$result = query($conn, $strdbsql, $strType, $arrdbparam);
		if (count($result) == 0) {
			$strdbsql = "INSERT INTO site_search_history (searchTerm, numSearches, numResults) VALUES (:query, 1, $intresults)";
			$strType = "insert";
			$result = query($conn, $strdbsql, $strType, $arrdbparam);
		} else {
			$strdbsql = "UPDATE site_search_history SET numSearches = numSearches+1, numResults = $intresults WHERE searchTerm = :query";
			$strType = "update";
			$result = query ($conn, $strdbsql, $strType, $arrdbparam);
		}
	}
}
?>

    </div></div></div><!--end of 'pagetext', 'contents' and 'pagemiddle'-->
<?php	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>