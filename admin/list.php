<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * list.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

error_log("listpage");

	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "list"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	$boolerror = TRUE; // edit if page info found

	//Requests
	if(isset($_GET['view'])) $strview = $_GET['view'];
	else $strview = 'main';
	if(isset($_GET['page'])) {
		$strpagetype = "shop";
		$strcat = strip_tags($_GET['page']);
		$level = 1;
	} else $strpagetype = "";

	//Redirect if linked to using old links
	if (isset($_GET['cat']) && $_GET['cat'] != "")
	{
		switch ($strview)
		{
			case "category":
				$strdbsql = "SELECT * FROM category WHERE recordID = :recordID";
				
			break;
			
			case "brand":
				$strdbsql = "SELECT * FROM stock_brands WHERE recordID = :recordID";
				
			break;
			
			case "range":
				$strdbsql = "SELECT * FROM stock_ranges WHERE recordID = :recordID";
				
			break;
		}
		$strType = "single";
		$arrdbparam = array( "recordID" => $_GET['cat'] );
		$group = query($conn, $strdbsql, $strType, $arrdbparam);
		
		if (!empty($group))
		{
			header( "HTTP/1.1 301 Moved Permanently" );
			$strlocation = "http://www.jhbenson.co.uk/".$strview."_".$group['metaPageLink'];
			header("Location: $strlocation");
			exit;
		}
		$_SESSION['cat']= $group['recordID'];
	}

	// Find Category Information from Database and Text File
	if ($strpagetype == "shop")
	{
		$level = 1;
		switch ($strview)
		{
			case "main":
			case "category":
				
				$strdbsql = "SELECT * FROM category WHERE metaPageLink = :metaPageLink";
				
			break;
			
			case "brand":
				
				$strdbsql = "SELECT * FROM stock_brands WHERE metaPageLink = :metaPageLink";
				
			break;
			
			case "range":
				
				$strdbsql = "SELECT * FROM stock_ranges WHERE metaPageLink = :metaPageLink";
				
			break;
		}
		$strType = "single";
		$arrdbparam = array( "metaPageLink" => $_GET['page'] );
		$groupDetails = query($conn, $strdbsql, $strType, $arrdbparam);
		
		$meta = array();
		$meta['metaTitle'] = $groupDetails['metaTitle']." - Stollers Furniture & Interiors";
		$meta['description'] = $groupDetails['metaDescription'];
		$meta['keywords'] = $groupDetails['metaKeywords'];
		$meta['metaPageLink'] = $strview."_".$groupDetails['metaPageLink'];
		
		
		if (!empty($groupDetails))
		{
			$strcommand = "display";
			$intGroup = $groupDetails['recordID'];
			$arrdbparams = array();
			switch ($strview)
			{
				case "category":
					
					$intcats = " parentID = :catID AND level = :level";
					$strgroup = $groupDetails['contentName'];
					$strbanner = $groupDetails['contentBanner'];
					$strsidebar = $groupDetails['contentSidebar'];
					$strmaincontent = $groupDetails['contentText'];
					$intshowcats = $groupDetails['showCategories'];
					$intshowcatbrand = $groupDetails['showBrands'];
					$intshowcatrange = $groupDetails['showRanges'];
					$strmenuimg = $groupDetails['contentMenuImage'];
					
					$strdbsql = "SELECT * FROM category INNER JOIN category_relations ON category.recordID = category_relations.childID WHERE category_relations.parentID = :catID AND category_relations.level = :level";
					$strType = "multi";
					$arrdbparams = array(
										"catID" => $intGroup,
										"level" => $level + 1
									);
					$subcats = query($conn, $strdbsql, $strType, $arrdbparams);
					
					
					
					/*foreach($subcats AS $subcat)
					{
						//$intcats .= " OR fld_category = '$resultdata2->fld_counter' OR fld_category = '$resultdata2->fld_counter' ";
					}*/
					
				break;
				
				case "brand":
					
					$intcats = " brandID = :brandID";
					$arrdbparams['brandID'] = $intGroup;
					$strgroup = $groupDetails['brandName'];
					$strbanner = $groupDetails['brandBanner'];
					$strsidebar = $groupDetails['brandSidebar'];
					$strmaincontent = $groupDetails['brandDescription'];
					$intshowcatrange = $groupDetails['showRanges'];
					$strmenuimg = $groupDetails['brandImage'];
					
					$strdbsql = "SELECT * FROM stock_ranges INNER JOIN stock_brands ON stock_ranges.brandID = stock_brands.recordID WHERE stock_ranges.brandID = :brandID";
					$strType = "multi";
					$arrdbparams = array(
										"brandID" => $intGroup
									);
					$ranges = query($conn, $strdbsql, $strType, $arrdbparams);
					foreach($ranges AS $range)
					{
						//$intcats .= " OR fld_category = '$resultdata2->fld_counter' OR fld_category = '$resultdata2->fld_counter' ";
					}
					
				break;
				
				case "range":
					
					$intcats = " brandID = :brandID";
					$strgroup = $groupDetails['rangeName'];
					$strbanner = $groupDetails['rangeBanner'];
					$strsidebar = $groupDetails['rangeSidebar'];
					$strmaincontent = $groupDetails['rangeDescription'];
					$strmenuimg = $groupDetails['rangeImage'];
					
				break;
			}
			$_SESSION['cat']= $intGroup;
			$intshowproducts = $groupDetails['showProducts'];
			
			
			$strcatfld = "category";
			if($strview == 'main') {
				$strkeywords = $groupDetails['metaKeywords'];
				$strdescription = $groupDetails['metaDescription'];
				$strdoctitle = ucwords(strtolower($strgroup))." Products - ".$groupDetails['metaTitle']." - Stollers Furniture World, Cumbria";
				$strcanonical = "shop_".$strcat;
			}
			else {
				$strkeywords = $groupDetails['metaKeywords'];
				$strdescription = "More Information - ".$groupDetails['metaDescription'];
				$strdoctitle = ucwords(strtolower($strgroup))." Information - ".$groupDetails['metaTitle']." - Stollers Furniture World, Cumbria";
				$strcanonical = "category_more_".$strcat;
			}

			$strsubcatonly = $strgroup;
			$strcatimg = "cat".$intGroup.".jpg";
			$boolerror = FALSE;
			$strdbsql = "SELECT * FROM category INNER JOIN category_relations ON category.recordID = category_relations.childID WHERE category_relations.parentID = :catID AND level = :level";
			$strType = "multi";
			$arrdbparam = array(
								"catID" => $intcat,
								"level" => $level + 1
							);
			$subcats = query($conn, $strdbsql, $strType, $arrdbparam);
			if (count($subcats) == 0 && $intshowproducts == 1)
			{
				$strdisplay = "products";
			}
			else
			{
				$strdisplay = "subcats";
			}
		}
	}

	// if no page found and not redirected use 404 error
	if($boolerror)
	{
		$strgroup = "Unknown";
		$strcommand = "error";
		$strcattext = "";
		$strcatmore = "";
		$strcatimg = "";
		header("HTTP/1.0 404 Not Found");
		$strcatfld = "error";
		$strcanonical = "shop_error";

	}

	if(!isset($strcanonical)) $strcanonical = strip_tags($_SERVER['REQUEST_URI']);
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
?>


	<div id="pagemiddle">
    <div id="content">
<?php
	//breadcrumb links
	$strbreadcrumbs = "<div class='pro_breadcrumbs-indent'><ul class='pro_breadcrumbs-one'>";
	$strbreadcrumbs .= "<li><a href='index.php'>Home</a></li>";
	if ($strview == 'main') {
		if ($strcatfld == "category") 
		{
			$strbreadcrumbs .= "<li><a href='#' class='current'>".str_replace("& ","&amp; ",$strgroup)."</a></li>";
		}
		else if ($strcatfld == "subcategory")
		{
			if ($strmaincat != '') $strbreadcrumbs .= "<li><a href='shop_".$strmaincat."'>".str_replace("& ","&amp; ",ucwords(strtolower($strmaincatdesc)))."</a></li>";
			if ($strsubcat2 != '') $strbreadcrumbs .= "<li><a href='category_".$strsubcat2."'>".str_replace("& ","&amp; ",ucwords(strtolower($strsubcatdesc2)))."</a></li>";
			if ($strsubcat1 != '') $strbreadcrumbs .= "<li><a href='category_".$strsubcat1."'>".str_replace("& ","&amp; ",ucwords(strtolower($strsubcatdesc1)))."</a></li>";
			$strbreadcrumbs .= "<li><a href='#' class='current'>".str_replace("& ","&amp; ",$strsubcatdesc)."</a></li>";
		}
	}
	else {
		if ($strcatfld == "category")
		{
			$strbreadcrumbs .= "<li><a href='shop_".$strcat."'>".str_replace("& ","&amp; ",$strgroup)."</a></li><li><a href='#' class='current'>More</a></li>";
		}
		else if ($strcatfld == "subcategory")
		{
			$strbreadcrumbs .= "<li><a href='shop_".$strmaincat."'>".str_replace("& ","&amp; ",ucwords(strtolower($strmaincatdesc)))."</a></li>";
			$strbreadcrumbs .= "<li><a href='category_".$strcat."'>".str_replace("& ","&amp; ",$strsubcatdesc)."</a></li><li><a href='#' class='current'>More</a></li>";
		}
	}
	$strbreadcrumbs .= "</ul></div>";
	//generate as string then paste;
//	print ($strbreadcrumbs);
//	include ($strrootpath."includes/incerrors.php");

?>
<div id="pagetext">
<?php

if ($strview == 'more') {

	/********************************************** More Info Page ****************************************/
	print ("<h2>".str_replace("& ","&amp; ",$strgroup)." - More Information</h2>");
	print ("<div id='sectioninfo'>");
	print ("$strcatmore");
	if ($strcatfld == "subcategory") $morelink = "/category_";
	if ($strcatfld == "category") $morelink = "/shop_";

     print ("<div class='backlink'><a class='short_button' href='".$morelink.$strcat."'>Back...</a></div>");
	print ("\n\n</div>\n\n");
}
else if ($strview == 'category' || $strview == 'brand' || $strview == 'range')
{

	/****************************************** Main Info Page *************************************/
	if ($strbanner == '' AND $strmaincontent == '') print ("<h2>".str_replace("& ","&amp; ",$strgroup)."</h2>\n");

	print ("$strbanner");
	print ("<div class='shop-sidebar' >");

			if ($intshowcats == 0) {

			
				//TYPE
				if ($strview == "category") {
					$strdbsql = "SELECT * FROM category INNER JOIN category_relations ON category.recordID = category_relations.childID WHERE category_relations.parentID = :catID AND category_relations.level = :level ORDER BY menuOrder";
					$strType = "multi";
					$arrdbparams = array(
										"catID" => $intGroup,
										"level" => $level + 1
									);
					$subCategories = query($conn, $strdbsql, $strType, $arrdbparams);

					$strdbsql = "SELECT * FROM category_relations WHERE childID = parentID AND childID = :catID";
					$strType = "multi";
					$arrdbparams = array("catID" => $intGroup);
					$catrelationcount = query($conn, $strdbsql, $strType, $arrdbparams);

					if (!empty($subCategories)) {
						$strdir = "categories/";
						
						//echo str_replace("//","/",$strrootpath."/images/".$strdir.$strmenuimg)."<br/>";
						//echo "File exists: ".file_exists(str_replace("//","/",$strrootpath."/images/".$strdir.$strmenuimg));
						if (count($catrelationcount) > 0) $intheight = 400;
						else $intheight = 320;
						
						if (!empty($strmenuimg) && file_exists(str_replace("//","/",$strrootpath."/images/".$strdir.$strmenuimg)) && ! is_dir(str_replace("//","/",$strrootpath."/images/".$strdir.$strmenuimg))) print ("\t\t<ul id='sub-type' style='background-image:url(images/$strdir/$strmenuimg);'>\n");
						else print ("\t\t<ul id='sub-type'>\n");
						$strtypemenutitle = $strsubcatonly." by Type ...";
						if(strlen($strtypemenutitle)> 25) $strtypemenutitle = "Browse By Type...";

						//print "<li class='odd title'><h4>".strtoupper($strtypemenutitle)."</h4></li>";
						$inti = 2;
						foreach($subCategories AS $subCategory)
						{
							if ($inti/2 == intval($inti/2)) $strclass = "even";
							else $strclass = "odd";
							print "\t\t\t<li class='$strclass'><a href='$strsiteurl/category_".$subCategory['metaPageLink']."'>".$subCategory['contentName']."</a></li>\n";
							$inti = $inti+1;
						}
						$inti = ($inti-1);
						if($inti < 11) {
							$height = $intheight-($inti*32)-16;
							if ($inti/2 == intval($inti/2)) $strclass = "odd";
							else $strclass = "even";
							print("\t\t\t<li class='$strclass $inti' style='height:".$height."px'>&nbsp;</li>");
						}
						print "\t\t</ul>\n";
					}
				}
			}
			print($strsidebar);

	switch($strdisplay)
	{
		case "subcats":
			if ($intshowcats == 0) {
				
				//BRAND
				if ($intshowcatbrand == 1) {
					$strdbsql = "SELECT * FROM stock_brands INNER JOIN category_brand_relations ON stock_brands.recordID = category_brand_relations.brandID WHERE category_brand_relations.categoryID = :groupID ORDER BY category_brand_relations.menuOrder";
					$strType = "multi";
					$arrdbparam = array( "groupID" => $intGroup );
					$associatedBrands = query($conn, $strdbsql, $strType, $arrdbparam);
					if (!empty($associatedBrands)) {
						print "\t\t<ul class='subnav' id='sub-brand".$groupDetails['recordID']."' class='sub'>\n";
							print "<li><h4>Brands</h4></li>";
							foreach($associatedBrands AS $associatedBrand)
							{
									print "\t\t\t<li><a href='$strsiteurl/brand_".$associatedBrand['metaPageLink']."'>".$associatedBrand['brandName']."</a></li>\n";
							}
						print "\t\t</ul>\n";
					}
				}
				
				//RANGE
				if ($intshowcatrange == 1) {
					$strdbsql = "SELECT * FROM stock_ranges WHERE brandID = :groupID ORDER BY menuOrder";
					$strType = "multi";
					$arrdbparam = array( "groupID" => $intGroup );
					$associatedRanges = query($conn,$strdbsql, $strType, $arrdbparam);
					if (!empty($associatedRanges)) {
						print "\t\t<ul class='subnav' id='sub-range".$groupDetails['recordID']."' class='sub'>\n";
						print "<li><h4>Ranges</h4></li>";
						foreach($associatedRanges AS $associatedRange)
						{
							print "\t\t\t<li><a href='$strsiteurl/range_".$associatedRange['metaPageLink']."'>".$associatedRange['rangeName']."</a></li>\n";
						}
						print "\t\t</ul>\n";
					}
				}
			}
			if ($intshowproducts == 1) $strdisplay = "products";
		break;
	}
	print ("</div>");
	print ("<div class='catcontent' >$strmaincontent</div>");



    /******************************************** Item List **************************************/

	//error_log($intshowproducts);
	
	if ($intshowproducts == 1) {
		
		include("includes/inc_shoplist.php");
		if($intend == 0) {
			/********************************************** No Products ****************************************/
			if ($strbanner == '') {
				print ("<div class='itemListing'>");
				print ("<hr />");
					print ("<div class='itemdescription'>");
						print ("<div class='itemtext'><p>No Products to List</p></div>");
					print ("</div>");
				print ("</div>");
				print ("<img src='/images/site_update.jpg' alt='Sorry no products to list' />");
			}
		}
		else
		{

			$stockItems = query($conn, $strshopsqllimit, $strType, $arrdbparams);
			//var_dump($stockItems);
			//echo "<br/><br/>";
			//echo $strshopsqllimit;
			//echo "<br/><br/>";
			//var_dump($arrdbparams);
			print("<div id='shop_navbartop' class='h2' style='clear:both;height:30px; line-height:16px;'></div>");
			print("<div id='productList'>");
			print ("<input type='hidden' value='$intend' id='totalProducts'/>");
			
			/********************************************** Filters ****************************************/
				print ("\n\n\n<div class='h2' style='height: 36px; padding: 5px 0; line-height: 0px;' >\n");
					
					$priceOptions = array();
					$priceOptions[] = "50";
					$priceOptions[] = "100";
					$priceOptions[] = "200";
					$priceOptions[] = "300";
					$priceOptions[] = "400";
					$priceOptions[] = "500";
					$priceOptions[] = "800";
					$priceOptions[] = "1000";

					//price low
					print ("<span style='margin-left: 10px;' >Lowest Price: <select name='priceLow' style='width:80px; margin-left: 10px;' id='priceLow' onchange='applyFilter()'><option value='' >Show All</option>");
					foreach ($priceOptions AS $price) { 
						if ($_POST['priceLow'] == $price) $selected = "selected"; else $selected = "";
						print ("<option value='$price' $selected>&pound;".number_format($price,2)."</option>");
					}
					print ("</select></span>");
					
					//price high
					print ("<span style='margin-left: 10px;' >Highest Price: <select name='priceHigh' style='width:80px; margin-left: 10px;' id='priceHigh' onchange='applyFilter()'><option value='' >Show All</option>");
					foreach ($priceOptions AS $price) { 
						if ($_POST['priceHigh'] == $price) $selected = "selected"; else $selected = "";
						print ("<option value='$price' $selected>&pound;".number_format($price,2)."</option>");
					}
					print ("</select></span>");
				
					//brand
					print ("<span style='margin-left: 10px;' >Brand: <select name='brand' style='width:200px; margin-left: 10px;' id='brand' onchange='applyFilter()'><option value='' >Show All</option>");
					$getBrandsQuery = "SELECT DISTINCT stock_brands.* FROM stock_brands INNER JOIN stock_group_information ON stock_group_information.brandID = stock_brands.recordID WHERE stock_group_information.recordID IN ($strstockgroupids)";
					$strType = "multi";
					$brands = query($conn, $getBrandsQuery, $strType);
					foreach($brands AS $brand)
					{
						if ($_POST['brand'] == $brand['recordID']) $selected = "selected"; else $selected = "";
						print("<option value='".$brand['recordID']."' $selected>".$brand['brandName']."</option>\n");
					}
					print ("</select></span>");
					//echo $getBrandsQuery."<br/><br/>";
					
					//finish
					print ("<span style='margin-left: 10px;' >Finish: <select name='finish' style='width:220px; margin-left: 10px;' id='finish' onchange='applyFilter()'><option value='' >Show All</option>");
					$getFinishesQuery = "SELECT DISTINCT stock_attributes.* FROM stock_attributes INNER JOIN stock_attribute_relations ON stock_attributes.recordID = stock_attribute_relations.stockAttributeID WHERE stock_attribute_relations.stockID IN ($strstockids) AND stock_attributes.attributeTypeID = 2";
					$strType = "multi";
					$stockFinishes = query($conn, $getFinishesQuery, $strType);
					foreach($stockFinishes AS $stockFinish)
					{
						if ($_POST['finish'] == $stockFinish['recordID']) $selected = "selected"; else $selected = "";
						print("<option value='".$stockFinish['recordID']."' $selected>".$stockFinish['description']."</option>\n");
					}
					print ("</select></span>");
					
					
				print ("</div>\n\n\n\n");


				/********************************************** Item List ****************************************/
				$itemList = fnItemList($stockItems, $conn);
				print $itemList;
			print("</div>");
			print("<div id='shop_navbarbottom' class='h2' style='clear:both;height:30px; line-height: 16px;'></div>");
		}

		//print ("</div>");
		if($intshowproducts == 1) print ("<a style='padding-top:10px; clear:both; margin-top: 10px;' href='#header' title='RETURN TO TOP' class='short_button'>RETURN TO TOP</a>");
	}
}
?>
</div></div></div>

<script type="text/javascript">
/*<![CDATA[*/
var page = '<?php echo $strcanonical; ?>';

var show;
var start;
var end;

<?php
	if($strcanonical != "category_more_brands")
	{
?>
		var show = <?php echo $intshow; ?>;
		var start = <?php echo $intstart; ?>;
		var end = <?php echo $intend; ?>;
<?php
	}
?>
var sort = "rec";
function getshopcontents(page,show,start,end,sort) {
	
	var priceHighE = document.getElementById("priceHigh");
	var priceHigh = "";
	if(typeof priceHighE != undefined && priceHighE != null){
		if (priceHighE.options[priceHighE.selectedIndex].value === null) priceHigh = ''; else priceHigh = priceHighE.options[priceHighE.selectedIndex].value;
	}
	var priceLowE = document.getElementById("priceLow");
	var priceLow = "";
	if(typeof priceLowE != undefined && priceLowE != null){
		if (priceLowE.options[priceLowE.selectedIndex].value == null) priceLow = ''; else priceLow = priceLowE.options[priceLowE.selectedIndex].value;
	}
	var brandE = document.getElementById("brand");
	var brand = "";
	if(typeof brandE != undefined && brandE != null){
		if (brandE.options[brandE.selectedIndex].value == null) brand = ''; else brand = brandE.options[brandE.selectedIndex].value;
	}
	var finishE = document.getElementById("finish");
	var finish = "";
	if(typeof finishE != undefined && finishE != null){
		if (finishE.options[finishE.selectedIndex].value == null) finish = ''; else finish = finishE.options[finishE.selectedIndex].value;
	}
	
	
	document.getElementById("productList").innerHTML="<span id='loadmessage' style='margin:115px 320px;vertical-align: middle;display:inline-block;width:auto;height:auto;color: #3082CC;line-height: 19px !important;'>Loading ...</span>";

	
	var request = "/ajax/ajaxlist.php";
	var vars = "page="+page+"&show="+show+"&start="+start+"&end="+end+"&sort="+sort+"&priceHigh="+priceHigh+"&priceLow="+priceLow+"&brand="+brand+"&finish="+finish;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response = xmlhttp.responseText;
			removeElementById("loadmessage");
			if(document.getElementById("productList").innerHTML=response)
			{
				redrawnav();
			}

		} else if (xmlhttp.readyState==4) {
			alert("Error");
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
}

function applyFilter(){
	start = 0;
	getshopcontents(page,show,start,end,sort);
}

function gotopage(pagenumber) {
	start = (pagenumber-1)*show;
	getshopcontents(page,show,start,end,sort);
}
function changelimit(limitElem) {
	show = limitElem.value;
	start = 0;
	getshopcontents(page,show,start,end,sort);
}
function changeorder(orderElem) {
	sort = orderElem.value;
	start = 0;
	getshopcontents(page,show,start,end,sort);
}
function redrawnav() {
	var content = "";

	//pagecontrols
	var totalProducts = document.getElementById("totalProducts");
	console.log(totalProducts);
	if(typeof totalProducts != undefined && totalProducts != null){
		end = totalProducts.value;
	}
	pagenumber = ((start/show)-(start%show))+1;
	if(end%show > 0) totalpages = ((end/show)-((end%show)/show))+1;
	else totalpages = end/show;
	content += "<div class='page' style='width:50%;display:inline-block;'>";
     if (totalpages > 1) {
		if (pagenumber > 1) content += "<a href='javascript:gotopage("+(pagenumber-1)+")'> < Previous </a> | ";

		for (var i = 1; i <= totalpages; i++) {
			if (i == pagenumber) content += i+" ";
			else content += "<a href='javascript:gotopage("+i+")'> "+i+" </a> ";
		}
		if(pagenumber < totalpages) content += " | <a href='javascript:gotopage("+(pagenumber+1)+")'> Next > </a>";
	}
	content += "</div>";


	//sort control
	content += " <div style='width:25%;display:inline-block;'>";
//	content += "Sort by: <select class='sortcontrol' onchange='changeorder(this)'>";
//	content += (sort == "rec" ? "<option value='rec' selected='selected'>Recommended</option>" : "<option value='rec' >Recommended</option>");
//	content += (sort == "high" ? "<option value='high' selected='selected'>Price: High to Low</option>" : "<option value='high' >Price: High to Low</option>");
//	content += (sort == "low" ? "<option value='low' selected='selected'>Price: Low to High</option>" : "<option value='low' >Price: Low to High</option>");
//	content += "</select>";
	content += "</div>";

	//limit control
	content += "<div style='width:22%;display:inline-block;'>Show <select class='limitcontrol' onchange='changelimit(this)'>";
	content += (show == 12 ? "<option value='10' selected='selected'>12</option>" : "<option value='12' >12</option>");
	content += (show == 24 ? "<option value='25' selected='selected'>24</option>" : "<option value='24' >24</option>");
	content += (show == 48 ? "<option value='50' selected='selected'>48</option>" : "<option value='48' >48</option>");
	content += (show == 100 ? "<option value='100' selected='selected'>100</option>" : "<option value='100' >100</option>");
	content += (show == end ? "<option value='"+end+"' selected='selected'>All</option>" : "<option value='"+end+"' >All</option>");
	content += "</select> Products per page</div>";
	document.getElementById('shop_navbartop').innerHTML = content;
	document.getElementById('shop_navbarbottom').innerHTML = content;

}
function removeElementById(id)
{
	var elm=document.getElementById(id);
	if(elm != null) {
		elm.parentNode.removeChild(elm);
		return true;
	} else {
		return false;
	}
}


<?php
	if($strcanonical != "category_more_brands")
	{
?>
		window.onload = redrawnav;
<?php
	}
?>


OAS_rn = new String (Math.random());
OAS_rns = OAS_rn.substring (2, 11);
var tfsm_protocol = window.location.protocol;
if (tfsm_protocol == "https:") {
 DataColl="https://";
} else { DataColl="http://"; 
 } 
document.write('<img alt=""  SRC="'+DataColl+'oas.newsquestdigital.co.uk/RealMedia/ads/adstream.track/1'+OAS_rns+'?XE&epmAccountKey=2609&epmXTransKey=1381&epmXtransStep=0&ProductCategory=<?php echo urlencode (trim($groupDetails['contentName'])); ?>&ItemDescription=&XE" style="width:0px;height:0px;border:none" />');
/*]]>*/
</script>
<?php	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>