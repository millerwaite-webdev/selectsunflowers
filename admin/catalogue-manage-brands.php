<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-manage-categories.php                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-manage-brands"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	$strcmd = (isset($_REQUEST['cmd'])) ? $_REQUEST['cmd'] : "";
	$brandID = (isset($_REQUEST['brandID'])) ? $_REQUEST['brandID'] : "";
	
	switch($strcmd)
	{
		case "insertBrand" || "updateBrand":
			$strshowProducts = (isset($_REQUEST['frm_showproducts'])) ? 1 : 0;
			$strshowFilters = (isset($_REQUEST['frm_showfilters'])) ? 1 : 0;
			$strshowRanges = (isset($_REQUEST['frm_showranges'])) ? 1 : 0;
			
			$arrdbparams = array(
								"brandName" => $_POST['frm_brandname'],
								"brandImage" => $_POST['frm_brandimage'],
								"brandBanner" => $_POST['frm_brandbanner'],
								"brandDescription" => $_POST['frm_branddescription'],
								"brandSidebar" => $_POST['frm_brandsidebar'],
								"metaTitle" => $_POST['frm_metatitle'],
								"metaPageLink" => $_POST['frm_metapagelink'],
								"metaDescription" => $_POST['frm_metadescription'],
								"metaKeywords" => $_POST['frm_metakeywords'],
								"showProducts" => $strshowProducts,
								"showFilters" => $strshowFilters,
								"showRanges" => $strshowRanges
							);
			
			if ($strcmd == "insertBrand")
			{	
				$strdbsql = "INSERT INTO stock_brands (brandName, brandDescription, brandSidebar, brandBanner, brandImage, metaTitle, metaPageLink, metaDescription, metaKeywords, showProducts, showFilters, showRanges) 
							VALUES (:brandName, :brandDescription, :brandSidebar, :brandImage, :brandBanner, :metaTitle, :metaPageLink, :metaDescription, :metaKeywords, :showProducts,:showFilters, :showRanges)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateBrand")
			{
				$strdbsql = "UPDATE stock_brands SET brandName = :brandName, brandDescription = :brandDescription, brandSidebar = :brandSidebar, brandImage = :brandImage, brandBanner = :brandBanner, 
							metaTitle = :metaTitle, metaPageLink = :metaPageLink, metaDescription = :metaDescription, metaKeywords = :metaKeywords, showProducts = :showProducts, 
							showFilters = :showFilters, showRanges = :showRanges WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $brandID;
				$strType = "update";
			}
			
			$updateBrand = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertBrand")
			{
				$brandID = $updateBrand;
			}
			
			if ($strcmd == "insertBrand")
			{
				if ($updateBrand > 0)
				{
					$strsuccess = "Brand successfully added";
				}
				elseif ($updateBrand == 0)
				{
					$strerror = "An error occurred while adding the brand";
				}
			}
			elseif ($strcmd == "updateBrand")
			{
				if ($updateBrand <= 1)
				{
					$strsuccess = "Brand successfully updated";
				}
				elseif ($updateBrand > 1)
				{
					$strwarning = "An error may have occurred while updating this brand";
				}
			}
			
			$strcmd = "viewBrand";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_image']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print ("$upfile - saving image<br>");

			if ($_FILES['frm_image']['error'] > 0)
			{
				switch ($_FILES['frm_image']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print ("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_image']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print ("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_image']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_image']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print ("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strbrandpath = $strimguploadpath."brands/";
				
				$strbranddir = $strrootpath.$strbrandpath;
				
				$newfile = $strbranddir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				
				unlink($upfile);
				
				if (isset($_REQUEST['frm_replacecurrent'])) $replaceCurrent = $_REQUEST['frm_replacecurrent']; else $replaceCurrent = 0;
				
				if($replaceCurrent)
				{
					$updateBrandImageQuery = "UPDATE stock_brands SET brandImage = :brandImage WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array (
									"brandImage" => $newfilename,
									"recordID" => $brandID
								);
					$updateBrandImage = query($conn, $updateBrandImageQuery, $strType, $arrdbparams);
				}
			}
			
			$strsuccess = "New brand image added";
			
			$strcmd = "viewBrand";
			
		break;
		
		case "addSiteBlock":
			
			$strdbsql = "SELECT COUNT(*) AS max_order FROM site_block_relations WHERE brandID = :brandID AND positionID = :positionID";
			$strType = "single";
			$arrdbparams = array(
								"brandID" => $brandID,
								"positionID" => $_POST['frm_siteblockposition']
							);
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparams);
			$order = $maxOrder['max_order'] + 1;
			
			foreach($_POST['frm_siteblocks'] AS $siteBlock)
			{
				$strdbsql = "INSERT INTO site_block_relations (blockID, positionID, brandID, pageOrder) VALUES (:blockID, :positionID, :brandID, :pageOrder)";
				$strType = "insert";
				$arrdbparams = array( 
									"blockID" => $siteBlock,
									"positionID" => $_POST['frm_siteblockposition'],
									"brandID" => $brandID,
									"pageOrder" => $order
								);
				$insertBlock = query($conn, $strdbsql, $strType, $arrdbparams);
				$order++;
			}
			
			$strcmd = "viewBrand";
			
		break;
		
		case "deleteBlocks":
			
			$strdbsql = "DELETE FROM site_block_relations WHERE recordID IN (".$_POST['deleteBlocks'].")";
			$strType = "delete";
			$removeImages = query($conn, $strdbsql, $strType);
			
			$strdbsql = "SELECT recordID FROM site_block_relations WHERE brandID = :brandID AND positionID = :positionID ORDER BY pageOrder";
			$strType = "multi";
			$arrdbparams = array(
							"brandID" => $brandID,
							"positionID" => $_POST['blockPositionID']
						);
			//echo "get products\n";
			$pageBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
			//var_dump($categories);
			
			$i = 1;
			foreach($pageBlocks AS $pageBlock)
			{
				$strdbsql = "UPDATE site_block_relations SET pageOrder = ".$i." WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparam = array(
								"recordID" => $pageBlock['recordID']
							);
				//echo "update final ordering\n";
				query($conn, $strdbsql, $strType, $arrdbparam);
				$i++;
			}
			
			$strcmd = "viewBrand";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Brand Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddBrand() {
					document.form.cmd.value='addBrand';
					document.form.submit();
				}
				function jsinsertBrand() {
					document.form.cmd.value='insertBrand';
					document.form.submit();
				}
				function jsviewBrand(brandID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewBrand';
						document.form.brandID.value=brandID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateBrand() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateBrand';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
				
					$("#assignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#assignedProducts")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["productID"] = $(item).attr('id');
									dataArray[i]["brandID"] = $("#brandID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									console.log(dataArray);
								}
								else if ($(item).parent().is("ul#unassignedProducts")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["productID"] = $(item).attr('id');
									dataArray[i]["brandID"] = $("#brandID").attr("value");
									dataArray[i]["cmd"] = "delete";
									console.log(dataArray);
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_brandproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#unassignedRanges").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#assignedRanges").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#assignedRanges'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedRanges")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["rangeID"] = $(item).attr('id');
									dataArray[i]["brandID"] = $("#brandID").attr("value");
									dataArray[i]["cmd"] = "add";
									console.log(dataArray);
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_brandrangemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					$("#assignedRanges").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedRanges").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedRanges'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#assignedRanges")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#assignedRanges").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["rangeID"] = $(item).attr('id');
									dataArray[i]["brandID"] = $("#brandID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									console.log(dataArray);
								}
								else if ($(item).parent().is("ul#unassignedRanges")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["rangeID"] = $(item).attr('id');
									dataArray[i]["brandID"] = $("#brandID").attr("value");
									dataArray[i]["cmd"] = "delete";
									console.log(dataArray);
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#assignedRanges").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_brandrangemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#unassignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#assignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#assignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedProducts")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["productID"] = $(item).attr('id');
									dataArray[i]["brandID"] = $("#brandID").attr("value");
									dataArray[i]["cmd"] = "add";
									console.log(dataArray);
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_brandproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#brandID").attr("value");
								dataArray[i]["type"] = "brand";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
				
			</script>
			<?php
		
			print ("<form action='catalogue-manage-brands.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='brandID' id='brandID' value='$brandID'/>");
				print ("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				switch($strcmd)
				{
					case "viewBrand":
					case "addBrand":
						
						if ($strcmd == "viewBrand")
						{
							$strdbsql = "SELECT * FROM stock_brands WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $brandID);
							$brandDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print ("<pre>");
							print_r($brandDetails);
							print ("</pre>");&*/
							
							print ("<fieldset class='inlineLabels'> <legend>Change Brand Details</legend>");
						}
						elseif ($strcmd == "addBrand")
						{
							$brandDetails = array(
												"brandName" => "",
												"brandImage" => "",
												"brandDescription" => "",
												"brandBanner" => "",
												"brandSidebar" => "",
												"metaTitle" => "",
												"metaPageLink" => "",
												"metaDescription" => "",
												"metaKeywords" => "",
												"showProducts" => 0,
												"showFilters" => 0,
												"showRanges" => 0
											);
							
							print ("<fieldset class='inlineLabels'> <legend>Add Brand Details</legend>");
						}
						print ("<div class='form-group'>
							<label for='frm_brandname' class='col-sm-2 control-label'>Name</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_brandname' name='frm_brandname' value='".$brandDetails['brandName']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metapagelink' class='col-sm-2 control-label'>Meta Link</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metapagelink' name='frm_metapagelink' value='".$brandDetails['metaPageLink']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metatitle' class='col-sm-2 control-label'>Meta Doc Title</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metatitle' name='frm_metatitle' value='".$brandDetails['metaTitle']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metakeywords' class='col-sm-2 control-label'>Meta Keywords</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metakeywords' name='frm_metakeywords' value='".$brandDetails['metaKeywords']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_brandimage' class='col-sm-2 control-label'>Brand Image</label>
							<div class='col-sm-10'>
							  <select name='frm_brandimage' id='frm_brandimage' class='form-control valid' style='width: 300px;'>
								<option value=''>None</option>");
							    $dh = opendir("../images/brands/");
								while (false !== ($image = readdir($dh))) {
									if ($image != "." && $image != "..") {
										if($image == $brandDetails['brandImage'])
										{
											$strselected = " selected";
										}
										else
										{
											$strselected = "";
										}
										print ("<option value='".$image."'".$strselected.">".$image."</option>");
									}
								}
						print ("</select>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_brandbanner' class='col-sm-2 control-label'>Banner</label>
							<div class='col-sm-10'>
							  <textarea class='form-control' id='frm_brandbanner' name='frm_brandbanner' rows='5' style='width: 654px;'>".$brandDetails['brandBanner']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metadescription' class='col-sm-2 control-label'>Meta Description</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_metadescription' name='frm_metadescription' rows='3'>".$brandDetails['metaDescription']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_branddescription' class='col-sm-2 control-label'>Description</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_branddescription' name='frm_branddescription' rows='5'>".$brandDetails['brandDescription']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_brandsidebar' class='col-sm-2 control-label'>Sidebar</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_brandsidebar' name='frm_brandsidebar' rows='5'>".$brandDetails['brandSidebar']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_productdescription' class='col-sm-2 control-label'>Show Options</label>
						  </div>
						  <div class='form-group'>
							<label for='frm_showproducts' class='col-sm-1 control-label'>Products</label>
							<div class='col-sm-1'>");
							  if($brandDetails['showProducts'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_showproducts' name='frm_showproducts' value='1' ".$strChecked." />
							</div>
							<label for='frm_showfilters' class='col-sm-1 control-label'>Filters</label>
							<div class='col-sm-1'>");
							  if($brandDetails['showFilters'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_showfilters' name='frm_showfilters' value='1' ".$strChecked." />
							</div>
							<label for='frm_showranges' class='col-sm-1 control-label'>Ranges</label>
							<div class='col-sm-1'>");
							  if($brandDetails['showRanges'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_showranges' name='frm_showranges' value='1' ".$strChecked." />
							</div>
						  </div>
						  <div class='form-group'>
							<div class='col-sm-10'>");
							if ($strcmd == "addBrand")
							{
							  print ("<button onclick='return jsinsertBrand();' type='submit' class='btn btn-success'>Update Brand</button> ");
							}
							elseif ($strcmd == "viewBrand")
							{
							  print ("<button onclick='return jsupdateBrand();' type='submit' class='btn btn-success'>Update Brand</button> ");
							}
							print  ("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>
							</div>
						  </div>");
						print ("</fieldset>");
						
						print ("<br/>");
						
						if ($strcmd == "viewBrand")
						{
							print("<br/>");
							print("<legend>Add New Image</legend>");
							
							print ("<div class='form-group'>
										<label for='frm_image' class='col-sm-2 control-label'>Image</label>
										<div class='col-sm-10'>
										  <input style='width:600px;' type='file' class='form-control' id='frm_image' name='frm_image' />
										</div>
									</div>
									<div class='form-group'>
										<label for='frm_replacecurrent' class='col-sm-2 control-label'>Replace current range image</label>
										<div class='col-sm-1'>");
											print ("<input type='checkbox' id='frm_replacecurrent' name='frm_replacecurrent' value='1' />
										</div>
									</div>
									<div class='form-group'>
										<div class='col-sm-offset-2 col-sm-10'>");
											print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success'>Insert Image</button>
										</div>
									</div>");
							print ("<fieldset class='inlineLabels'> <legend>Manage Associated Ranges</legend>");
							
								$strdbsql = "SELECT recordID, rangeName, metaPageLink FROM stock_ranges WHERE brandID IS NULL ORDER BY rangeName ASC";
								$strType = "multi";
								$unassignedRanges = query($conn, $strdbsql, $strType);
								
								$strdbsql = "SELECT recordID, rangeName, metaPageLink, menuOrder FROM stock_ranges WHERE brandID = :brandID ORDER BY menuOrder ASC";
								$strType = "multi";
								$arrdbparams = array("brandID" => $brandID);
								$assignedRanges = query($conn, $strdbsql, $strType, $arrdbparams);
								
								/*print ("<h1>Subs</h1>");
								print ("<pre>");
								print_r($subCategories);
								print ("</pre>");*/
								
								/*print ("<h1>All less subs</h1>");
								print ("<pre>");
								print_r($categories);
								print ("</pre>");*/
								print("<div class='form-group'>
									<div class='col-sm-3'>
										<label for='frm_stocklevels' class='control-label'>Ranges</label>
										<br/>
										<ul id='unassignedRanges'>");
											foreach($unassignedRanges AS $unassignedRange)
											{
												print("<li id='".$unassignedRange['recordID']."'><span>".$unassignedRange['rangeName']." (".$unassignedRange['metaPageLink'].")</span></li>");																
											}
									print ("</ul>");
								print ("</div>");
								print ("<div class='col-sm-3'>
										<label for='frm_stocklevels' class='control-label'>Assigned Ranges</label>
										<br/>
										<ul id='assignedRanges'>");
											foreach($assignedRanges AS $assignedRange)
											{
												print("<li id='".$assignedRange['recordID']."' data-order='".$assignedRange['menuOrder']."'><span>".$assignedRange['rangeName']." (".$assignedRange['metaPageLink'].")</span></li>");																
											}
									print ("</ul>");
								print ("</div>
								</div>
							</fieldset>");
						
							print ("<br/>");
						
							print ("<fieldset class='inlineLabels'> <legend>Manage Associated Products</legend>");
							
								$strdbsql = "SELECT recordID, name, metaLink FROM stock_group_information WHERE brandID IS NULL ORDER BY name ASC";
								$strType = "multi";
								$unassignedProducts = query($conn, $strdbsql, $strType);
								
								$strdbsql = "SELECT recordID, name, metaLink, brandOrder FROM stock_group_information WHERE brandID = :brandID ORDER BY brandOrder ASC";
								$strType = "multi";
								$arrdbparams = array("brandID" => $brandID);
								$assignedProducts = query($conn, $strdbsql, $strType, $arrdbparams);
								
								/*print ("<h1>Subs</h1>");
								print ("<pre>");
								print_r($subCategories);
								print ("</pre>");*/
								
								/*print ("<h1>All less subs</h1>");
								print ("<pre>");
								print_r($categories);
								print ("</pre>");*/
								print("<div class='form-group'>
									<div class='col-sm-3'>
										<label for='frm_stocklevels' class='control-label'>Products</label>
										<br/>
										<ul id='unassignedProducts'>");
											foreach($unassignedProducts AS $unassignedProduct)
											{
												print("<li id='".$unassignedProduct['recordID']."'><span>".$unassignedProduct['name']." (".$unassignedProduct['metaLink'].")</span></li>");																
											}
									print ("</ul>");
								print ("</div>");
								print ("<div class='col-sm-3'>
										<label for='frm_stocklevels' class='control-label'>Assigned Products</label>
										<br/>
										<ul id='assignedProducts'>");
											foreach($assignedProducts AS $assignedProduct)
											{
												print("<li id='".$assignedProduct['recordID']."' data-order='".$assignedProduct['brandOrder']."'><span>".$assignedProduct['name']." (".$assignedProduct['metaLink'].")</span></li>");																
											}
									print ("</ul>");
								print ("</div>
								</div>
							</fieldset>");
							
							print ("<fieldset class='inlineLabels'> <legend>Site Blocks</legend>");
								$strdbsql = "SELECT * FROM site_block_position";
								$strType = "multi";
								$siteBlockPositions = query($conn, $strdbsql, $strType);
								$siteBlockPositionSelectOptions = "";
								
								print ("<div class='form-group'>");
									foreach($siteBlockPositions AS $siteBlockPosition)
									{
										print ("<div class='col-sm-2'>");
											print ("<h3>".$siteBlockPosition['description']."</h3>");
											print ("<ul class='blockPosList' id='pos-".$siteBlockPosition['recordID']."'>");
												$strdbsql = "SELECT * FROM site_blocks INNER JOIN site_block_relations ON site_block_relations.blockID = site_blocks.recordID WHERE site_block_relations.brandID = :brandID AND site_block_relations.positionID = :positionID ORDER BY site_block_relations.pageOrder";
												$strType = "multi";
												$arrdbparams = array(
																	"brandID" => $brandID,
																	"positionID" => $siteBlockPosition['recordID']
																);
												$siteBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
												foreach($siteBlocks AS $siteBlock)
												{
													print ("<li id='".$siteBlock['recordID']."' data-order='".$siteBlock['pageOrder']."' data-pos='".$siteBlockPosition['recordID']."'><span>".$siteBlock['description']."</span></li>");
												}
											print ("</ul>");
										print ("</div>");
										$siteBlockPositionSelectOptions .= "<option value='".$siteBlockPosition['recordID']."'>".$siteBlockPosition['description']."</option>";
									}
								print ("</div>");
								print ("<div class='form-group'>");
									print ("<button onclick='return jsdeleteSiteBlocks();' type='button' class='btn btn-danger'>Delete Selected</button>");
									print ("<input type='hidden' name='deleteBlocks' id='deleteBlocks' />");
								print ("</div>");
								
								print ("<br/>");
								print ("<div class='form-group'>");
									print ("<label for='frm_siteblocks' class='col-sm-2 control-label'>Available Blocks</label>");
									print ("<br/>");
									print ("<br/>");
									$strdbsql = "SELECT * FROM site_blocks ORDER BY description ASC";
									$strType = "multi";
									$siteBlocks = query($conn, $strdbsql, $strType);
									
									print ("<select name='frm_siteblocks[]' id='frm_siteblocks' class='form-control valid' style='width: 300px;' multiple>");
										foreach($siteBlocks AS $siteBlock)
										{
											print ("<option value='".$siteBlock['recordID']."'>".$siteBlock['description']."</option>");
										}
									print ("</select>");
									
								print ("</div>");
								print ("<div class='form-group'>");
									print ("<label for='frm_siteblockposition' class='col-sm-2 control-label'>Block Position</label>");
									print ("<br/>");
									print ("<br/>");
									
									print ("<select name='frm_siteblockposition' id='frm_siteblockposition' class='form-control valid' style='width: 300px;'>");
										print ($siteBlockPositionSelectOptions);
									print ("</select>");
									
								print ("</div>");
								print ("<div class='form-group'>
											<div class='col-sm-10'>");
												print ("<button onclick='return jsaddSiteBlock();' type='submit' class='btn btn-success'>Add Site Block</button> ");
									print ("</div>
										</div>");
							print ("</fieldset>");
						}
						
						break;
						
					default:
						
						$strdbsql = "SELECT * FROM stock_brands ORDER BY brandName ASC";
						$strType = "multi";
						$brands = query($conn, $strdbsql, $strType);
						
						print ("<button onclick='return jsaddBrand();' type='submit' class='btn btn-success'>Add New</button>");
						print ("<br/>");
						print ("<br/>");
						print ("<table class='table table-striped table-bordered table-hover table-condensed' id='brands-table'>");
							print ("<thead>");
								print ("<th>Brand Name</th>");
								print ("<th>Page Link</th>");
								print ("<th></th>");
							print ("</thead>");
							print ("<tbody>");
							if (count($brands) > 0)
							{
								foreach($brands AS $brand)
								{
									print ("<tr>");
										print ("<td>".$brand['brandName']."</td>");
										print ("<td>".$brand['metaPageLink']."</td>");
										print ("<td><button onclick='return jsviewBrand(\"".$brand['recordID']."\");' type='submit' class='btn btn-primary'>Update Brand</button></td>");
									print ("</tr>");
								}
							}
							print ("</tbody>");
						print ("</table>");
						
						print ("<button onclick='return jsaddBrand();' type='submit' class='btn btn-success'>Add New</button>");
						
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#brands-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>