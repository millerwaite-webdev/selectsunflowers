<?php


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "import-files"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	//the classes that are needed to read excel files
	require_once 'lib/PHPExcel.php';
	require_once 'lib/PHPExcel/IOFactory.php';
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['importFormatID'])) $importFormatID = $_REQUEST['importFormatID']; else $importFormatID = "";
	if (isset($_REQUEST['fileComment'])) $strfileComment = $_REQUEST['fileComment']; else $strfileComment = "";
	
	function fnSplitString($string, $positions){
		$stringArray = array();
		foreach ($positions as $position){
			$stringArray[] = rtrim(substr($string, 0, $position + 0));
			$string = substr($string, $position + 0);
		}
		return $stringArray;
	}
	function xml2array($xml){
		$arr = array();

		foreach ($xml as $element)
		{
			$tag = $element->getName();
			$e = get_object_vars($element);
			if (!empty($e))
			{
				$arr[$tag] = $element instanceof SimpleXMLElement ? xml2array($element) : $e;
			}
			else
			{
				$arr[$tag] = trim($element);
			}
		}

		return $arr;
	}
	
	switch($strcmd){
		case "fileUploaded":
		
			//files uploaded
			if(count($_FILES['fileToUpload'])) {
				
				$fileArray = reArrayFiles($_FILES['fileToUpload']);
				foreach ($fileArray as $file) {

					$fulldata = array();
									
					//check to see if the file already exists in the database based on a checksum of size and name
					$original_extension = (false === $pos = strrpos($file["name"], '.')) ? '' : substr($file["name"], $pos);
					$checkSum = md5($file["name"].$file["size"]);					
					$strdbsql = "SELECT * FROM fileImport WHERE fileExtension = :fileExtension AND checkSum = :checkSum";
					$strdbparams = array("fileExtension" => $original_extension, "checkSum" => $checkSum);
					$arrType = "multi";
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
					
					if (count($resultdata) == 0){
					
						$directory = "/files/".$_SESSION['lottery']."/raw/";
						$filename = $directory.fnconvertunixtime($datnow,"H:i-d-m-Y")."_".$file["name"];
						
						if ($_REQUEST['frm_importreference'] == "") $_REQUEST['frm_importreference'] = $file["name"];
				
						//create directory if it does not currently exist
						if (!file_exists($directory)){$makedir = recursive_mkdir($directory);}
						
						//copy file into the raw directory
						$moveResult = copy($file["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$filename);
						if ($moveResult == true) {

							if (strpos($file["name"],'GENERIC') === false) {
						
								$strdbsql = "SELECT configFileImportFormats.*, configFileImportWorksheets.* FROM configFileImportFormats LEFT JOIN configFileImportWorksheets ON configFileImportFormats.importID = configFileImportWorksheets.importID WHERE configFileImportFormats.importID = :importID";
								$strType = "single";
								$arrdbvalues = array("importID" => $importFormatID);
								$configFileImportFormats = query($conn, $strdbsql, $strType, $arrdbvalues);


								if (strtolower($original_extension) == '.xls' || strtolower($original_extension) == '.xlsx') {
									
									$objPHPExcel = PHPExcel_IOFactory::load($file['tmp_name']);
																
									//Document Data			
									foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
									
										$worksheetsToImport = explode(",",$configFileImportFormats['worksheetsToImport']);
										if (in_array($objPHPExcel->getIndex($worksheet), $worksheetsToImport)) {
										
											$strdbsql = "SELECT directDebitFileTypeID FROM configFileImportWorksheets WHERE importID = :importID AND importIndex = :worksheetID";
											$strType = "single";
											$arrdbvalues = array("importID" => $importFormatID, "worksheetID"=>$objPHPExcel->getIndex($worksheet));
											$worksheetInformation = query($conn, $strdbsql, $strType, $arrdbvalues);
											
											$strdbsql = "INSERT INTO fileImport (filePath, fileExtension, configID, worksheetID, reference, importType, paymentType, checkSum, operator, timestamp, status, directDebitFileTypeID) VALUES (:filePath, :fileExtension, :configID, :worksheetID, :reference, :importType, :paymentType, :checkSum, :operator, :timestamp, :status, :directDebitFileTypeID)";
											$arrType = "insert";
											$strdbparams = array("filePath" => $filename, "fileExtension" => $original_extension, "configID" =>$importFormatID, "worksheetID"=>$objPHPExcel->getIndex($worksheet), "reference" => $_REQUEST['frm_importreference']."-Worksheet".$objPHPExcel->getIndex($worksheet), "importType" => $configFileImportFormats['importType'], "paymentType" => $configFileImportFormats['filePaymentType'], "checkSum" => $checkSum, "operator" => $_SESSION['operatorID'], "timestamp" => $datnow, "status" => 1, "directDebitFileTypeID" => $worksheetInformation['directDebitFileTypeID']);
											$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
											$doccounter = $resultdata;
											
											//resets values
											$highestColumnIndex = 0;
											$worksheetTitle = $worksheet->getTitle();
											$highestRow = $worksheet->getHighestRow(); // e.g. 10
											$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
											$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
											$nrColumns = ord($highestColumn) - 64;
											
											for ($row = 1; $row <= $highestRow; ++ $row) {
											
												for ($col = 0; $col < $highestColumnIndex; ++ $col) {
													$cell = $worksheet->getCellByColumnAndRow($col, $row);
													$val = $cell->getValue();
													$rowdata[$row][$col] = $val;
													if ($val != '') $blank = false;
												}
												
												//insert into temp table
												$strdbsql = "INSERT INTO fileImportData (importID, rowID, dataArray, status) VALUES (:importID, :rowID, :dataArray, :status)";
												$arrType = "insert";
												$strdbparams = array("dataArray" => serialize($rowdata[$row]), "rowID"=>$row, "importID"=>$doccounter, "status"=>1);
												$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
											}
										}
									}
									$strsuccess .= "File successfully uploaded - ".$file['name']." to $filename <br/>";

								} else if (strtolower($original_extension) == '.txt' || strtolower($original_extension) == '.csv') {
								
									$strdbsql = "INSERT INTO fileImport (filePath, fileExtension, configID, worksheetID, reference, importType, paymentType, checkSum, operator, timestamp, status, directDebitFileTypeID) VALUES (:filePath, :fileExtension, :configID, :worksheetID, :reference, :importType, :paymentType, :checkSum, :operator, :timestamp, :status, :directDebitFileTypeID)";
									$arrType = "insert";
									$strdbparams = array("filePath" => $filename, "fileExtension" => $original_extension, "configID" =>$importFormatID, "worksheetID"=>0, "reference" => $_REQUEST['frm_importreference'], "importType" => $configFileImportFormats['importType'], "paymentType" => $configFileImportFormats['filePaymentType'], "checkSum" => $checkSum, "operator" => $_SESSION['operatorID'], "timestamp" => $datnow, "status" => 1, "directDebitFileTypeID" => $configFileImportFormats['directDebitFileTypeID']);
									$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
									$doccounter = $resultdata;

									//csv files
									$fileobject = new SplFileObject($file['tmp_name']);
									$filecontrol = $fileobject->getCsvControl();
									$highestColumnIndex = 0;
									if ($configFileImportFormats['fileDelimiter'] == "\\t") $strmydelimiter = "\t"; else $strmydelimiter = $configFileImportFormats['fileDelimiter']; 
									
									//check to see if we can open the uploaded file
									if (($handle = fopen($file['tmp_name'], "r")) !== FALSE) {
										
										//set the php option for csv files created using a mac
										ini_set('auto_detect_line_endings',TRUE);
									
										$row = 1;
										
										if ($configFileImportFormats['fileDelimiterType'] == 1) { //fixed width
											
											//while($data=fscanf($handle, "%".rtrim(str_replace(",","s%",$configFileImportFormats['fileDelimiter']),"%")."\n")){
											while($data = fgets($handle, 4096)){
												
												$stringArray = fnSplitString($data, explode(",",$configFileImportFormats['fileDelimiter']));
												
												//insert into temp table
												$strdbsql = "INSERT INTO fileImportData (importID, rowID, dataArray, status) VALUES (:importID, :rowID, :dataArray, :status)";
												$arrType = "insert";
												$strdbparams = array("dataArray" => serialize($stringArray), "rowID"=>$row, "importID"=>$doccounter, "status"=>1);
												$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
												$row++;
											}
										
										} else { //character
										
											// while (($data = fgetcsv($handle, 1000, $filecontrol[0], $filecontrol[1])) !== FALSE) {
											while (($data = fgetcsv($handle, 1000, $strmydelimiter)) !== FALSE) {
															
												$num = count($data);
												if ($num > $highestColumnIndex) $highestColumnIndex = $num;
												for ($c=0; $c < $num; $c++) {
													$fulldata[$row][$c] = $data[$c];
												}
												
												//insert into temp table
												$strdbsql = "INSERT INTO fileImportData (importID, rowID, dataArray, status) VALUES (:importID, :rowID, :dataArray, :status)";
												$arrType = "insert";
												$strdbparams = array("dataArray" => serialize($data), "rowID"=>$row, "importID"=>$doccounter, "status"=>1);
												$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
												$row++;
											}
										}
										
										
										fclose($handle);
										$strsuccess .= "File successfully uploaded - ".$file['name']." to $filename <br/>";
										ini_set('auto_detect_line_endings',FALSE);

									} else {
										$strerror .= ("Could not open file ".$file['tmp_name']." <br/>");
									}
									
									
								} else if (strtolower($original_extension) == '.xml') {
								
									$strdbsql = "INSERT INTO fileImport (filePath, fileExtension, configID, worksheetID, reference, importType, paymentType, checkSum, operator, timestamp, status) VALUES (:filePath, :fileExtension, :configID, :worksheetID, :reference, :importType, :paymentType, :checkSum, :operator, :timestamp, :status)";
									$arrType = "insert";
									$strdbparams = array("filePath" => $filename, "fileExtension" => $original_extension, "configID" =>$importFormatID, "worksheetID"=>0, "reference" => $_REQUEST['frm_importreference'], "importType" => $configFileImportFormats['importType'], "paymentType" => $configFileImportFormats['filePaymentType'], "checkSum" => $checkSum, "operator" => $_SESSION['operatorID'], "timestamp" => $datnow, "status" => 1);
									$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
									$doccounter = $resultdata;
									
									$xmlDocument = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].$filename);
									
									
									if ($strerror == "") $strsuccess .= "File successfully uploaded - ".$file['name']." to $filename<br/>";
									
								} else {
									$strerror .= ("File Extension $original_extension not recognised or not supported.<br/>");
								}
							} else {
								$strerror .= ("The file (".$file['name'].") looks like a generic test file and hasnt been uploaded, please contact support if this is incorrect. <br/>");
							}
						} else {
							$strerror .= ("Could not move file to the raw data directory ($filename)<br/>");
						}
					} else {
						$strerror .= ("This file has already been imported<br/>");
					}
					$_REQUEST['frm_importreference'] = "";
				}
			} else {
				$strerror .= ("No file to upload<br/>");
			}
		break;
	}


	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>".$configPageDetails['pageTitle']."</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

?>	
<script language='Javascript' >
	
	function fnImportFile() {
		
		$('select[name="frm_importFormatID"]').rules('add', { required: true });
		if ($('#form').valid()) {
			document.getElementById("cmd").value = "fileUploaded";
			var e = document.getElementById("frm_importFormatID");
			var importFormatID = e.options[e.selectedIndex].value;
			document.getElementById("importFormatID").value = importFormatID;
			document.getElementById("form").submit();
		}
	}
	
	$().ready(function() {
		// validate signup form on keyup and submit
		$("#form").validate({
			errorPlacement: function(error,element) {
				error.insertAfter(element);
			},
			rules: {
			}
		});
	});

</script>
<?php


			print ("<form action='import-files.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data'><!-- IMPORTANT:  FORM's enctype must be 'multipart/form-data' -->");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' value=''/>");
			print ("<input type='hidden' class='nodisable' name='importFormatID' id='importFormatID' value='$importFormatID'/>");
			print ("<div class='section' style='overflow:auto;'>");
			
					$strdbsql = "SELECT configFileImportFormats.*, configFileImportTypes.description AS fileType FROM configFileImportFormats INNER JOIN configFileImportTypes ON configFileImportFormats.importType = configFileImportTypes.recordID WHERE configFileImportFormats.status = 1 ORDER BY configFileImportTypes.description";
					$arrdbvalues = array();
					$strType = "multi";
					$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);

					print ("<div class='form-group'><label for='frm_importFormatID' class='col-sm-2 control-label'>Import Format: </label><div class='col-sm-10'>");
					print ("<select name='frm_importFormatID' id='frm_importFormatID' class='form-control' style='width:400px;'>");
					print ("<option value=''>-- Please Select --</option>");
					foreach ($resultdata as $row) {
						if ($importFormatID == $row['importID']) { $strselected = "selected"; } else { $strselected = ""; }
						print ("<option value='".$row['importID']."' $strselected>".$row['fileType']." Import: ".$row['description']."</option>");
					}
					print ("</select></div></div>");
					
					print ("<div class='form-group'><label for='frm_importreference' class='col-sm-2 control-label'>File Comment:</label><div class='col-sm-10'><input name='frm_importreference' id='frm_importreference' style='width:200px;' size='35' maxlength='45' type='text' class='form-control' value='$strfileComment'/></div></div>");

					print ("<div class='form-group'>");
						print ("<label for='fileToUpload' class='col-sm-2 control-label'>File to Upload</label><div class='col-sm-10'>");
						print ("<input type='file' id='fileToUpload' name='fileToUpload[]' multiple='multiple'>");
						print ("<p class='help-block'>Valid Files: .txt, .csv, .xls, .xlsx, xml</p>");
					print ("</div></div>");
			
			print ("</div>");
			print ("</form>");
			print ("<button onclick='fnImportFile(); return false;' class='btn btn-success '>Import File</button>");
			
		print("</div>");
	print("</div>");
	
// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>