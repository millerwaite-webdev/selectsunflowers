<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-manage-categories.php                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-manage-categories"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['categoryID'])) $categoryID = $_REQUEST['categoryID']; else $categoryID = "";
	if (isset($_REQUEST['brandID'])) $brandID = $_REQUEST['brandID']; else $brandID = "";
	if (isset($_REQUEST['rangeID'])) $rangeID = $_REQUEST['rangeID']; else $rangeID = "";
	
	switch($strcmd)
	{
		case "insertCategory":
		case "updateCategory":
			if (isset($_REQUEST['frm_showproducts'])) { $strshowProducts = 1; } else { $strshowProducts = 0;}
			if (isset($_REQUEST['frm_showcategories'])) { $strshowCategories = 1; } else { $strshowCategories = 0;}
			if (isset($_REQUEST['frm_showbrands'])) { $strshowBrands = 1; } else { $strshowBrands = 0;}
			if (isset($_REQUEST['frm_showranges'])) { $strshowRanges = 1; } else { $strshowRanges = 0;}
			if (isset($_REQUEST['frm_showfilters'])) { $strshowFilters = 1; } else { $strshowFilters = 0;}
			if (isset($_REQUEST['frm_parentcategory'])) { $strparentCategory = 1; } else { $strparentCategory = 0;}
			
			$arrdbparams = array(
				"contentName" => $_POST['frm_contentname'],
				"contentText" => $_POST['frm_contenttext'],
				"contentBanner" => $_POST['frm_contentbanner'],
				"contentSidebar" => $_POST['frm_contentsidebar'],
				"contentMenuImage" => $_POST['frm_contentmenuimage'],
				"metaTitle" => $_POST['frm_metatitle'],
				"metaPageLink" => $_POST['frm_metapagelink'],
				"metaDescription" => $_POST['frm_metadescription'],
				"metaKeywords" => $_POST['frm_metakeywords'],
				"showProducts" => $strshowProducts,
				"showCategories" => $strshowCategories,
				"showBrands" => $strshowBrands,
				"showRanges" => $strshowRanges,
				"showFilters" => $strshowFilters
			);
			
			if ($strcmd == "insertCategory")
			{	
				$strdbsql = "INSERT INTO category (contentName, contentText, contentBanner, contentSidebar, metaTitle, metaPageLink, metaDescription, metaKeywords, contentMenuImage, showProducts, showCategories, showBrands, showRanges, showFilters) VALUES (:contentName, :contentText, :contentBanner, :contentSidebar, :metaTitle, :metaPageLink, :metaDescription, :contentMenuImage, :metaKeywords, :showProducts, :showCategories, :showBrands, :showRanges, :showFilters)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateCategory")
			{
				$strdbsql = "UPDATE category SET contentName = :contentName, contentText = :contentText, contentBanner = :contentBanner, contentSidebar = :contentSidebar, 
				metaTitle = :metaTitle, metaPageLink = :metaPageLink, metaDescription = :metaDescription, contentMenuImage = :contentMenuImage, metaKeywords = :metaKeywords, showProducts = :showProducts, 
				showCategories = :showCategories, showBrands = :showBrands, showRanges = :showRanges, showFilters = :showFilters WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $categoryID;
				$strType = "update";
			}
			
			$updateCategory = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertCategory")
			{
				$categoryID = $updateCategory;
			}
			
			$insertParent = false;
			if ($strcmd == "updateCategory")
			{
				$checkParentQuery = "SELECT COUNT(*) AS num_parent_cats FROM category_relations WHERE parentID = :categoryID AND parentID = childID AND level = 1";
				$strType = "single";
				$arrdbparam = array( "categoryID" => $categoryID );
				$checkParent = query($conn, $checkParentQuery, $strType, $arrdbparam);
				
				if ($checkParent['num_parent_cats'] == 0 && $strparentCategory == 1)
				{
					$insertParent = true;
				}
				elseif ($checkParent['num_parent_cats'] > 0 && $strparentCategory == 0)
				{
					$deleteParentCategoryQuery = "DELETE FROM category_relations WHERE parentID = :categoryID AND parentID = childID AND level = 1";
					$strType = "delete";
					$arrdbparam = array( "categoryID" => $categoryID );
					$deleteParentCategory = query($conn, $deleteParentCategoryQuery, $strType, $arrdbparam);
				}
			}
			elseif ($strcmd = "insertCategory" && $strparentCategory == 1)
			{
				$insertParent = true;
			}
			
			if ($insertParent)
			{
				$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM category_relations WHERE parentID = childID AND level = 1";
				$strType = "single";
				$maxOrder = query($conn, $maxOrderQuery, $strType);
				
				$order = $maxOrder['max_order'] + 1;
				
				$insertParentQuery = "INSERT INTO category_relations (parentID, childID, menuOrder, level) VALUES (:parentID, :childID, :menuOrder, :level)";
				$strType = "insert";
				$arrdbparam = array(
					"parentID" => $categoryID,
					"childID" => $categoryID,
					"menuOrder" => $order,
					"level" => 1
				);
				$insertParentCategory = query($conn, $insertParentQuery, $strType, $arrdbparam);
			}
			
			if ($strcmd == "insertCategory")
			{
				if ($updateCategory > 0)
				{
					$strsuccess = "Category successfully added";
				}
				elseif ($updateCategory == 0)
				{
					$strerror = "An error occurred while adding the category";
				}
			}
			elseif ($strcmd == "updateCategory")
			{
				if ($updateCategory <= 1 || $insertParentCategory > 0 || $deleteParentCategory == 1)
				{
					$strsuccess = "Category successfully updated";
				}
				elseif ($insertParentCategory == 0 && $deleteParentCategory == 0)
				{
					$strerror = "An error occurred while updating the category";
				}
				elseif ($updateCategory > 1 || $deleteParentCategory > 1)
				{
					$strwarning = "An error may have occurred while updating this category";
				}
			}
			
			$strcmd = "viewCategory";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_image']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print("$upfile - saving image<br>");

			if ($_FILES['frm_image']['error'] > 0)
			{
				switch ($_FILES['frm_image']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_image']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_image']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_image']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strcategorypath = $strimguploadpath."categories/";
				
				$strcategorydir = $strrootpath.$strcategorypath;
				
				$newfile = $strcategorydir.$newfilename;
				$category=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				
				unlink($upfile);
				
				if (isset($_REQUEST['frm_replacecurrent'])) $replaceCurrent = $_REQUEST['frm_replacecurrent']; else $replaceCurrent = 0;
				
				if($replaceCurrent)
				{
					$updateCategoryImageQuery = "UPDATE category SET contentMenuImage = :contentMenuImage WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array (
									"contentMenuImage" => $newfilename,
									"recordID" => $categoryID
								);
					$updateCategoryImage = query($conn, $updateCategoryImageQuery, $strType, $arrdbparams);
				}
			}
			
			$strcmd = "viewCategory";
			
		break;
		
		case "addSiteBlock":
			
			$strdbsql = "SELECT COUNT(*) AS max_order FROM site_block_relations WHERE categoryID = :categoryID AND positionID = :positionID";
			$strType = "single";
			$arrdbparams = array(
								"categoryID" => $categoryID,
								"positionID" => $_POST['frm_siteblockposition']
							);
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparams);
			$order = $maxOrder['max_order'] + 1;
			
			foreach($_POST['frm_siteblocks'] AS $siteBlock)
			{
				$strdbsql = "INSERT INTO site_block_relations (blockID, positionID, categoryID, pageOrder) VALUES (:blockID, :positionID, :categoryID, :pageOrder)";
				$strType = "insert";
				$arrdbparams = array( 
									"blockID" => $siteBlock,
									"positionID" => $_POST['frm_siteblockposition'],
									"categoryID" => $categoryID,
									"pageOrder" => $order
								);
				$insertBlock = query($conn, $strdbsql, $strType, $arrdbparams);
				$order++;
			}
			
			$strcmd = "viewCategory";
			
		break;
		
		case "deleteBlocks":
			
			$strdbsql = "DELETE FROM site_block_relations WHERE recordID IN (".$_POST['deleteBlocks'].")";
			$strType = "delete";
			$removeImages = query($conn, $strdbsql, $strType);
			
			$strdbsql = "SELECT recordID FROM site_block_relations WHERE categoryID = :categoryID AND positionID = :positionID ORDER BY pageOrder";
			$strType = "multi";
			$arrdbparams = array(
							"categoryID" => $categoryID,
							"positionID" => $_POST['blockPositionID']
						);
			//echo $strdbsql;
			//var_dump($arrdbparams);
			//echo "get products\n";
			$pageBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
			//var_dump($pageBlocks);
			
			$i = 1;
			foreach($pageBlocks AS $pageBlock)
			{
				$strdbsql = "UPDATE site_block_relations SET pageOrder = ".$i." WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparam = array(
								"recordID" => $pageBlock['recordID']
							);
				//echo "update final ordering\n";
				query($conn, $strdbsql, $strType, $arrdbparam);
				$i++;
			}
			
			$strcmd = "viewCategory";
			
		break;
		
		case "deleteCategory":
			
			$removeSiteBlocksCategoryQuery = "UPDATE site_block_relations SET categoryID = NULL WHERE categoryID = :categoryID";
			$strType = "update";
			$arrdbparam = array(
							"categoryID" => $categoryID
						);
			$removeSiteBlocksCategory = query($conn, $removeSiteBlocksCategoryQuery, $strType, $arrdbparam);
			
			$deleteStockCatRelationsQuery = "DELETE FROM stock_category_relations WHERE categoryID = :categoryID";
			$strType = "delete";
			$arrdbparam = array(
							"categoryID" => $categoryID
						);
			$deleteStockCatRelations = query($conn, $deleteStockCatRelationsQuery, $strType, $arrdbparam);
			
			$deleteCatBrandRelationsQuery = "DELETE FROM category_brand_relations WHERE categoryID = :categoryID";
			$strType = "delete";
			$arrdbparam = array(
							"categoryID" => $categoryID
						);
			$deleteCatBrandRelations = query($conn, $deleteCatBrandRelationsQuery, $strType, $arrdbparam);
			
			$deleteCatRelationsQuery = "DELETE FROM category_relations WHERE parentID = :parentID OR childID = :childID";
			$strType = "delete";
			$arrdbparam = array(
							"parentID" => $categoryID,
							"childID" => $categoryID
						);
			$deleteCatRelations = query($conn, $deleteCatRelationsQuery, $strType, $arrdbparam);
			
			$deleteCategoryQuery = "DELETE FROM category WHERE recordID = :recordID";
			$strType = "delete";
			$arrdbparam = array(
							"recordID" => $categoryID
						);
			$deleteCategory = query($conn, $deleteCategoryQuery, $strType, $arrdbparam);
			
			$strcmd = "";
			$strsuccess = "Category Deleted";
			
		break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Category Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddCategory() {
					document.form.cmd.value='addCategory';
					document.form.submit();
				}
				function jsdeleteCategory() {
					if(confirm("Are you sure you want to delete this category?"))
					{
						document.form.cmd.value="deleteCategory";
						document.form.submit();
					}
					else
					{
						return false;
					}
					
				}
				function jsinsertCategory() {
					document.form.cmd.value='insertCategory';
					document.form.submit();
				}
				function jsviewCategory(categoryID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewCategory';
						document.form.categoryID.value=categoryID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateCategory() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateCategory';
						$("#frm_parentcategory").removeAttr("disabled");
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				function sortableBehaviour(className) {
					//console.log($("."+className).children("li").children("span"));
					$("."+className).children("li").children("span").click(function (e) {
						//console.log("test");
						if (e.ctrlKey || e.metaKey) {
							$(this).parent().toggleClass("selected");
						} else {
							$(this).parentsUntil("#form").find('.selected').removeClass('selected');
							$(this).parent().addClass("selected");
							$("#sortableSubCategories").children().removeClass('selected');
						}
					});
					$("."+className).sortable({
						connectWith: ['#sortableSubCategories'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							 //Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						receive: function(e,ui) {
							//copyHelper= null;
							$(ui.item.data('multidrag')).each(function(index, item) {
								$(item).prepend("<span><span class='expand_collapse'>+</span> ");
								$(item).append("</span><ul class='sortableCategoryList' style='padding-left: 20px; display: none; border-left: 1px dotted; min-height: 17px;'></ul>");
								//console.log("Length is: "+$(ui.item).parent().parent("[data-level]").length);
								if ($(ui.item).parent().parent("[data-level]").length) {
									//console.log(parseInt($(ui.item).parent().parent().attr("data-level")) + 1);
									$(item).attr("data-level", parseInt($(ui.item).parent().parent().attr("data-level")) + 1);
								} else {
									$(item).attr("data-level", 1);
								}
								//console.log(item);
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								$(item).attr("data-parentid", $(item).parent().parent().attr('id'));
								$(item).find(".expand_collapse").click(function() {
									expandCollapse(this);
								});
								sortableBehaviour(className);
							});
						},
						stop: function( event, ui ) {
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul.sortableCategoryList"));
								if ($(item).parent().is("ul.sortableCategoryList")) {
									$(item).find(".expand_collapse").click(function() {
										expandCollapse(this);
									});
									//console.log($(item).find("ul.sortableCategoryList").children());
									$(item).find("ul.sortableCategoryList").find("span").click(function (e) {
										//console.log("test");
										if (e.ctrlKey || e.metaKey) {
											$(this).parent().toggleClass("selected");
										} else {
											$(this).parentsUntil("#form").find('.selected').removeClass('selected');
											$(this).parent().addClass("selected");
											$("#sortableSubCategories").children().removeClass('selected');
										}
									});
									var oldOrder = $(item).attr("data-order");
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									$(item).attr("data-parentid", $(item).parent().parent().attr('id'));
									
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["childID"] = $(item).attr('id');
									dataArray[i]["parentID"] = $(item).attr('data-parentid');
									dataArray[i]["level"] = $(item).attr('data-level');
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);
								} else if ($(item).parent().is("#sortableSubCategories")) {
									//console.log(item);
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr('data-order');
									dataArray[i]["childID"] = $(item).attr('id');
									dataArray[i]["parentID"] = $(item).attr('data-parentid');
									dataArray[i]["level"] = $(item).attr('data-level');
									dataArray[i]["cmd"] = "delete";
									sortableBehaviour("sortableCategoryList");
									//console.log(dataArray);
									if ($(item).attr('data-level') == 1) {
										if ($("#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = 1][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").parent().length == 1) {
											$("#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = 1][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").each(function(index, item) {
												$(item).attr("data-order", $(item).index() + 1);
											});
										}
									} else {
										if ($("li#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = '"+parseInt($(item).attr('data-level'))+"'][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").parent().length == 1) {
											$("li#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = '"+parseInt($(item).attr('data-level'))+"'][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").each(function(index, item) {
												$(item).attr("data-order", $(item).index() + 1);
											});
										}
									}
									//console.log($("li#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = '"+parseInt($(item).attr('data-level'))+"'][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").parent().length);
									//console.log($("#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = 1][id != '"+$(item).attr('data-parentid')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']"));
									//console.log($(item).parent().find("li#"+$(item).attr('id')).size());
									if ($(item).parent().find("li#"+$(item).attr('id')).size() > 1) {
										$(item).remove();
									} else {
										$(item).find(".expand_collapse").remove();
										$(item).find(".sortableCategoryList").remove();
										$(item).removeAttr("data-level");
										$(item).removeAttr("data-order");
										$(item).removeAttr("data-parentid");
										//console.log($.trim($(item).children().first().text()));
										$.trim($(item).children().first());
										//console.log(item);
									}
								}
								//console.log($(item).find("ul").attr('class'));
								sortableBehaviour(className);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorystructure.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				}
				function expandCollapse(control) {
					$(control).parent().siblings("ul").toggle("blind", 500, function() {
						if ($(control).parent().siblings("ul").is(':visible'))
						{
							$(control).html("-");
						}
						else
						{
							$(control).html("+");
						}
					});
				}
				$().ready(function() {
					sortableBehaviour('sortableCategoryList');
					
					$("#sortableSubCategories").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$(".sortableCategoryList").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['.sortableSubcategoryList'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						forcePlaceholderSize: false,
						helper: function (e, item) {
							
							 //Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							var selected = $("#sortableSubCategories li.selected");
							
							$(selected).each(function(index, item) {
								$(item).attr("data-orig-index", $(item).parent().children().index(item));
							});
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						remove: function (e, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							
							$(elements).each(function(index, item) {
								$("#sortableSubCategories li:eq("+parseInt($(item).attr("data-orig-index"))+")").before($(item).clone().removeAttr("data-orig-index").removeClass("selected"));
								$(item).removeAttr("data-orig-index");
							});
						},
						stop: function (e, ui) {
							
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							
							//`elements` now contains the originally selected items from the source list (the dragged items)!!
							
							//Finally I insert the selected items after the `item`, then remove the `item`, since 
							//  item is a duplicate of one of the selected items.
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								//console.log(item);
								if ($(item).parent().is("ul.sortableCategoryList")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									$(item).attr("data-parentid", $(item).parent().parent().attr('id'));
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr('data-order');
									dataArray[i]["childID"] = $(item).attr('id');
									dataArray[i]["parentID"] = $(item).attr('data-parentid');
									dataArray[i]["level"] = $(item).attr('data-level');
									dataArray[i]["cmd"] = "add";
									sortableBehaviour("sortableCategoryList");
									i++;
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorystructure.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#assignedBrands").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedBrands").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedBrands'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#assignedBrands")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#assignedBrands").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["brandID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
								} else if ($(item).parent().is("ul#unassignedBrands")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["brandID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "delete";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
									
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#assignedBrands").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorybrandmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#unassignedBrands").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#assignedBrands").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#assignedBrands'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedBrands")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["brandID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "add";
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorybrandmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#assignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#assignedProducts")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
								} else if ($(item).parent().is("ul#unassignedProducts")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "delete";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
									
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_categoryproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#unassignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#assignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#assignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedProducts")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "add";
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_categoryproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$(".expand_collapse").click(function() {
						expandCollapse(this);
					});
					
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#categoryID").attr("value");
								dataArray[i]["type"] = "category";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print("<form action='catalogue-manage-categories.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='categoryID' id='categoryID' value='$categoryID'/>");
				print("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				switch($strcmd)
				{
					case "addCategory":
					case "viewCategory":
						
						print("<div class='row'>");
							print("<div class='col-md-12'>");
								print("<div class='section'>");
						
									if ($strcmd == "viewCategory")
									{
										$strdbsql = "SELECT * FROM category WHERE recordID = :recordID";
										$strType = "single";
										$arrdbparams = array("recordID" => $categoryID);
										$categoryDetails = query($conn, $strdbsql, $strType, $arrdbparams);
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Change Category Details</legend>");
									}
									elseif ($strcmd == "addCategory")
									{
										$categoryDetails = array(
											"contentName" => "",
											"contentText" => "",
											"contentBanner" => "",
											"contentSidebar" => "",
											"contentMenuImage" => "",
											"metaTitle" => "",
											"metaPageLink" => "",
											"metaDescription" => "",
											"metaKeywords" => "",
											"showProducts" => 0,
											"showCategories" => 0,
											"showBrands" => 0,
											"showRanges" => 0,
											"showFilters" => 0
										);
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Add Category Details</legend>");
									}
									
										print("<div class='row'>");
											print("<div class='col-md-6 form-group'>");
												print("<label for='frm_contentname' class='control-label'>Name</label>");
												print("<input type='text' class='form-control' id='frm_contentname' name='frm_contentname' value='".$categoryDetails['contentName']."'>");
											print("</div>");
										print("</div>");
										
										if($strcmd == "addCategory" || $isChild['is_child'] == 0)
										{
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_parentcategory' class='control-label'>Top Level Menu Item</label>");
													if($strcmd == "viewCategory")
													{
														// check if top level exists for ID
														$isParentQuery = "SELECT COUNT(*) AS is_parent FROM category_relations WHERE parentID = :parentID AND parentID = childID AND level = 1";
														$strType = "single";
														$arrdbparam = array( "parentID" => $categoryID );
														$isParent = query($conn, $isParentQuery, $strType, $arrdbparam);
														if($isParent['is_parent'] == 1)
														{
															$strChecked = " checked";
															// check if children exist for ID
															$numChildrenQuery = "SELECT COUNT(*) AS num_children FROM category_relations WHERE parentID = :parentID AND parentID <> childID AND level = 2";
															$strType = "single";
															$arrdbparam = array( "parentID" => $categoryID );
															$numChildren = query($conn, $numChildrenQuery, $strType, $arrdbparam);
															if($numChildren['num_children'] > 0)
															{
																$strDisabled = " disabled";
															}
															else
															{
																$strDisabled = "";
															}
														}
														else
														{
															$strChecked = "";
														}
													}
													print("<input type='checkbox' id='frm_parentcategory' name='frm_parentcategory' value='1' ".$strChecked.$strDisabled." />");
												print("</div>");
											print("</div>");
										}
										
										if ($strcmd == "viewCategory")
										{
											print("<div class='section hide'>");
												print("<fieldset class='inlineLabels'>");
													print("<legend>Manage Associated Brands</legend>");
												
													$strdbsql = "SELECT recordID, brandName FROM stock_brands ORDER BY brandName ASC";
													$strType = "multi";
													$brands = query($conn, $strdbsql, $strType);
													
													$strdbsql = "SELECT stock_brands.recordID, brandName, category_brand_relations.menuOrder FROM stock_brands INNER JOIN category_brand_relations ON stock_brands.recordID = category_brand_relations.brandID WHERE category_brand_relations.categoryID = :categoryID ORDER BY menuOrder ASC";
													$strType = "multi";
													$arrdbparams = array("categoryID" => $categoryID);
													$assignedBrands = query($conn, $strdbsql, $strType, $arrdbparams);
													
													$assignedBrandOptions = "";
													
													foreach ($assignedBrands AS $assignedBrand)
													{
														$assignedBrandOptions .= "<li id='".$assignedBrand['recordID']."' data-order='".$assignedBrand['menuOrder']."'><span>".$assignedBrand['brandName']."</span></li>";
														unset($assignedBrand['menuOrder']);
														$key = array_search($assignedBrand, $brands);
														if($key !== false)
														{
															unset($brands[$key]);
														}
													}
													
													print("<div class='row'>");
														print("<div class='col-md-6 form-group'>");
															print("<label for='frm_brands' class='control-label'>Brands</label>");
															print("<ul id='unassignedBrands'>");
																foreach($brands AS $brand)
																{
																	print("<li id='".$brand['recordID']."'><span>".$brand['brandName']."</span></li>");																
																}
															print("</ul>");
														print("</div>");
														print("<div class='col-md-6 form-group'>");
															print("<label for='frm_stocklevels' class='control-label'>Assigned Brands</label>");
															print("<ul id='assignedBrands'>");
																print($assignedBrandOptions);
															print("</ul>");
														print("</div>");
													print("</div>");
												print("</fieldset>");
											print("</div>");
											
											print("<fieldset class='inlineLabels'>");
												print("<legend>Manage Associated Products</legend>");
												print("<p style='margin:-15px 0 15px;padding:0 15px 15px;'>Drag and drop items from each of the lists to assign/unassign products from the category.</p>");
										
												$getProductsQuery = "SELECT * FROM stock_group_information ORDER BY name ASC";
												$strType = "multi";
												$unassignedProducts = query($conn, $getProductsQuery, $strType);
												
												$getAssignedProductsQuery = "SELECT stock_group_information.*, stock_category_relations.stockOrder FROM stock_group_information INNER JOIN stock_category_relations ON stock_group_information.recordID = stock_category_relations.stockID WHERE categoryID = :categoryID ORDER BY stock_category_relations.stockOrder";
												$strType = "multi";
												$arrdbparam = array("categoryID" => $categoryID);
												$assignedProducts = query($conn, $getAssignedProductsQuery, $strType, $arrdbparam);
												$assignedProductOptions = "";
												
												foreach ($assignedProducts AS $assignedProduct)
												{
													$assignedProductOptions .= "<li id='".$assignedProduct['recordID']."' data-order='".$assignedProduct['stockOrder']."'><span>".$assignedProduct['name']." (".$assignedProduct['metaLink'].")</span></li>";
													unset($assignedProduct['stockOrder']);
													$key = array_search($assignedProduct, $unassignedProducts);
													if($key !== false)
													{
														unset($unassignedProducts[$key]);
													}
												}
											
												print("<div class='row'>");
													print("<div class='col-md-6 form-group even-left'>");
														print("<label for='frm_stocklevels' class='control-label'>Products</label>");
														print("<ul id='unassignedProducts'>");
															foreach($unassignedProducts AS $unassignedProduct)
															{
																print("<li id='".$unassignedProduct['recordID']."'><span>".$unassignedProduct['name']."</span></li>");																
															}
														print("</ul>");
													print("</div>");
													print("<div class='col-md-6 form-group even-right'>");
														print("<label for='frm_stocklevels' class='control-label'>Assigned Products</label>");
														print("<ul id='assignedProducts'>");
															print($assignedProductOptions);
														print("</ul>");
													print("</div>");
												print("</div>");
												
											print("</fieldset>");

											print("<div class='section hide'>");
												print("<fieldset class='inlineLabels'>");
													print("<legend>Site Blocks</legend>");
												
													$strdbsql = "SELECT * FROM site_block_position";
													$strType = "multi";
													$siteBlockPositions = query($conn, $strdbsql, $strType);
													$siteBlockPositionSelectOptions = "";
												
													print("<div class='row'>");
														print("<div class='form-group'>");
															foreach($siteBlockPositions AS $siteBlockPosition)
															{
																print("<div class='col-sm-4'>");
																	print("<h3>".$siteBlockPosition['description']."</h3>");
																	print("<ul class='blockPosList' id='pos-".$siteBlockPosition['recordID']."'>");
																		$strdbsql = "SELECT * FROM site_blocks INNER JOIN site_block_relations ON site_block_relations.blockID = site_blocks.recordID WHERE site_block_relations.categoryID = :categoryID AND site_block_relations.positionID = :positionID ORDER BY site_block_relations.pageOrder";
																		$strType = "multi";
																		$arrdbparams = array(
																							"categoryID" => $categoryID,
																							"positionID" => $siteBlockPosition['recordID']
																						);
																		$siteBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
																		foreach($siteBlocks AS $siteBlock)
																		{
																			print("<li id='".$siteBlock['recordID']."' data-order='".$siteBlock['pageOrder']."' data-pos='".$siteBlockPosition['recordID']."'><span>".$siteBlock['description']."</span></li>");
																		}
																	print("</ul>");
																print("</div>");
																$siteBlockPositionSelectOptions .= "<option value='".$siteBlockPosition['recordID']."'>".$siteBlockPosition['description']."</option>";
															}
														print("</div>");
													print("</div>");
													
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<button onclick='return jsdeleteSiteBlocks();' type='button' class='btn btn-danger'>Delete Selected</button>");
															print("<input type='hidden' name='deleteBlocks' id='deleteBlocks' />");
														print("</div>");
													print("</div>");
													
												print("</fieldset>");
											print("</div>");
											
											print("<div class='section hide'>");
												print("<fieldset class='inlineLabels'>");
													print("<legend>Add Blocks to Category</legend>");
												
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<label for='frm_siteblocks' class='control-label'>Available Blocks</label>");
															
															$strdbsql = "SELECT * FROM site_blocks ORDER BY description ASC";
															$strType = "multi";
															$siteBlocks = query($conn, $strdbsql, $strType);
															
															print("<select name='frm_siteblocks[]' id='frm_siteblocks' class='form-control valid' multiple>");
																foreach($siteBlocks AS $siteBlock)
																{
																	print("<option value='".$siteBlock['recordID']."'>".$siteBlock['description']."</option>");
																}
															print("</select>");
															
														print("</div>");
													print("</div>");
												
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<label for='frm_siteblockposition' class='control-label'>Block Position</label>");
															print("<select name='frm_siteblockposition' id='frm_siteblockposition' class='form-control valid'>");
																print($siteBlockPositionSelectOptions);
															print("</select>");
														print("</div>");
													print("</div>");
												
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<button onclick='return jsaddSiteBlock();' type='submit' class='btn btn-success'>Add Site Block</button>");
														print("</div>");
													print("</div>");
														
												print("</fieldset>");
											print("</div>");
										}

										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_metapagelink' class='control-label'>Meta Link</label>");
												print("<input type='text' required class='form-control' id='frm_metapagelink' name='frm_metapagelink' value='".$categoryDetails['metaPageLink']."'>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_metatitle' class='control-label'>Meta Doc Title</label>");
												print("<input type='text' class='form-control' id='frm_metatitle' name='frm_metatitle' value='".$categoryDetails['metaTitle']."'>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_metakeywords' class='control-label'>Meta Keywords</label>");
												print("<input type='text' class='form-control' id='frm_metakeywords' name='frm_metakeywords' value='".$categoryDetails['metaKeywords']."'>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_contentmenuimage' class='control-label'>Category Image</label>");
												print("<select name='frm_contentmenuimage' id='frm_contentmenuimage' class='form-control valid'>");
													print("<option value=''>None</option>");
															
													$dh = opendir("../images/elements/");
													
													while (false !== ($image = readdir($dh))) {
														if ($image != "." && $image != "..") {
															if($image == $categoryDetails['contentMenuImage'])
															{
																$strselected = " selected";
															}
															else
															{
																$strselected = "";
															}
															print("<option value='".$image."'".$strselected.">".$image."</option>");
														}
													}
												print("</select>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_contentbanner' class='control-label'>Banner</label>");
												print("<textarea class='form-control' id='frm_contentbanner' name='frm_contentbanner' rows='5'>".$categoryDetails['contentBanner']."</textarea>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_metadescription' class='control-label'>Meta Description</label>");
												print("<textarea class='form-control' id='frm_metadescription' name='frm_metadescription' rows='3'>".$categoryDetails['metaDescription']."</textarea>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_contentsidebar' class='control-label'>Sidebar</label>");
												print("<textarea class='form-control' id='frm_contentsidebar' name='frm_contentsidebar' rows='5'>".$categoryDetails['contentSidebar']."</textarea>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_contenttext' class='control-label'>Content</label>");
												print("<textarea class='form-control' id='frm_contenttext' name='frm_contenttext' rows='5'>".$categoryDetails['contentText']."</textarea>");
											print("</div>");
										print("</div>");
									
										if($strcmd == "viewCategory")
										{
											// check if category is child
											$isChildQuery = "SELECT COUNT(*) AS is_child FROM category_relations WHERE childID = :childID AND parentID <> childID";
											$strType = "single";
											$arrdbparam = array( "childID" => $categoryID );
											$isChild = query($conn, $isChildQuery, $strType, $arrdbparam);
										}
										else
										{
											$isChild['is_child'] = 0;
										}
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_productdescription' class='control-label'>Show Options</label>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_showproducts' class='col-sm-1 control-label'>Products</label>");
												print("<div class='col-sm-1'>");
													if($categoryDetails['showProducts'])
													{
														$strChecked = " checked";
													}
													else
													{
														$strChecked = "";
													}
													print("<input type='checkbox' id='frm_showproducts' name='frm_showproducts' value='1' ".$strChecked." />");
												print("</div>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_showcategories' class='col-sm-1 control-label'>Categories</label>");
												print("<div class='col-sm-1'>");
													if($categoryDetails['showCategories'])
													{
														$strChecked = " checked";
													}
													else
													{
														$strChecked = "";
													}
													print("<input type='checkbox' id='frm_showcategories' name='frm_showcategories' value='1' ".$strChecked." />");
												print("</div>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_showbrands' class='col-sm-1 control-label'>Brands</label>");
												print("<div class='col-sm-1'>");
													if($categoryDetails['showBrands'])
													{
														$strChecked = " checked";
													}
													else
													{
														$strChecked = "";
													}
													print("<input type='checkbox' id='frm_showbrands' name='frm_showbrands' value='1' ".$strChecked." />");
												print("</div>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_showranges' class='col-sm-1 control-label'>Ranges</label>");
												print("<div class='col-sm-1'>");
													if($categoryDetails['showRanges'])
													{
														$strChecked = " checked";
													}
													else
													{
														$strChecked = "";
													}
													print("<input type='checkbox' id='frm_showranges' name='frm_showranges' value='1' ".$strChecked." />");
												print("</div>");
											print("</div>");
										print("</div>");
										
										print("<div class='row hide'>");
											print("<div class='form-group'>");
												print("<label for='frm_showfilters' class='col-sm-1 control-label'>Filters</label>");
												print("<div class='col-sm-1'>");
													if($categoryDetails['showFilters'])
													{
														$strChecked = " checked";
													}
													else
													{
														$strChecked = "";
													}
													print("<input type='checkbox' id='frm_showfilters' name='frm_showfilters' value='1' ".$strChecked." />");
												print("</div>");
											print("</div>");
										print("</div>");
										
									print("</fieldset>");
								print("</div>");
							print("</div>");
								
							print("<div class='col-md-6'>");
							
								print("<div class='section hide'>");
									print("<fieldset class='inlineLabels'>");
										print("<legend>Add New Image</legend>");
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_image' class='col-sm-2 control-label'>Image</label>");
												print("<input type='file' class='form-control' id='frm_image' name='frm_image' />");
											print("</div>");
										print("</div>");
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_replacecurrent' class='col-sm-2 control-label'>Replace current category image</label>");
												print("<div class='col-sm-1'>");
													print("<input type='checkbox' id='frm_replacecurrent' name='frm_replacecurrent' value='1' />");
												print("</div>");
											print("</div>");
										print("</div>");
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success'>Insert Image</button>");
											print("</div>");
										print("</div>");
									print("</fieldset>");
								print("</div>");
								
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='col-sm-12'>");
								if ($strcmd == "addCategory")
								{
									print("<button onclick='return jsinsertCategory();' type='submit' class='btn btn-success right'>Save</button>");
								}
								elseif ($strcmd == "viewCategory")
								{
									print("<button onclick='return jsupdateCategory();' type='submit' class='btn btn-success right'>Save</button>");
								  
									// check if has relations
									$numRelationsQuery = "SELECT COUNT(*) AS num_relations FROM category_relations WHERE parentID = :categoryID OR childID = :categoryID2";
									$strType = "single";
									$arrdbparam = array( "categoryID" => $categoryID, "categoryID2" => $categoryID );
									$numRelations = query($conn, $numRelationsQuery, $strType, $arrdbparam);
									if($numRelations['num_relations'] == 0)
									{
										print("<button onclick='return jsdeleteCategory();' class='btn btn-danger'>Delete</button>");
									}
								}
								print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
							print("</div>");
						print("</div>");
						
					break;
					
					default:
						
						print("<div class='row'>");
							print("<div class='col-sm-6'>");
						
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
										print("<legend>Categories</legend>");
										print("<div class='row'>");
											print("<div class='form-group'>");
						
												print("<ul class='sortableCategoryList'>");
							
													//This needs to be drag and drop to change the order
													
													$maxLevelQuery = "SELECT MAX(level) AS max_level FROM category_relations";
													$strType = "single";
													$maxLevel = query($conn, $maxLevelQuery, $strType);
													$level = 1;
													
													$strdbsql = "SELECT category.* FROM category INNER JOIN category_relations ON category.recordID = category_relations.parentID WHERE category_relations.parentID = category_relations.childID AND level = 1 ORDER BY category_relations.menuOrder";
													$strType = "multi";
													$resultdata = query($conn, $strdbsql, $strType);
													
													if (count($resultdata) != 0)
													{
														$i = 1;
														foreach ($resultdata as $row) {
															$strdbsql = "SELECT COUNT(*) AS num_nested FROM category_relations WHERE parentID = :categoryID AND level = 2";
															$strType = "single";
															$arrdbparam = array( "categoryID" => $row['recordID'] );
															$resultdata = query($conn, $strdbsql, $strType, $arrdbparam);
															
															print("<li id='".$row['recordID']."' data-level='".$level."' data-order='".$i."' data-parentid='form'>");
															
															//print("<li id='".$row['recordID']."' data-level='".$level."'>");
																print("<span><span class='expand_collapse'>+</span>");
																print(" ".$row['contentName']." (".$row['metaPageLink'].") <a href='#' onclick='jsviewCategory(".$row['recordID']."); return false;'>Edit</a></span>");
																print("<ul class='sortableCategoryList sortableSubcategoryList' style='padding-left: 20px; display: none; border-left-width: 1px; border-left-style: dotted; min-height: 17px;'>");
																	if ($resultdata['num_nested'] > 0)
																	{
																		getSubCategories($row['recordID'], $level, $maxLevel['max_level'], $conn);
																	}
																print("</ul>");
															print("</li>");
															$i++;
														}
													} else {
														//print("<tr><td colspan='7'>Currently No Categories</td></tr>");
														print("<li>Currently No Categories</li>");
													}
													
												print("</ul>");
												
											print("</div>");
										print("</div>");
									print("</fieldset>");
								print("</div>");
								
							print("</div>");
							print("<div class='col-sm-6'>");
						
								$strdbsql = "SELECT category.* FROM category WHERE recordID NOT IN (SELECT DISTINCT parentID FROM category_relations WHERE parentID = childID) ORDER BY metaPageLink ASC";
								$strType = "multi";
								$resultdata = query($conn, $strdbsql, $strType);
						
								if (count($resultdata) != 0)
								{
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
											print("<legend>Subcategories</legend>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<ul id='sortableSubCategories'>");
											
														foreach ($resultdata as $row) {
															print("<li id='".$row['recordID']."'><span>".$row['contentName']." (".$row['metaPageLink'].") <a href='#' onclick='jsviewCategory(".$row['recordID']."); return false;'>Edit</a></span></li>");
														}
													
													print("</ul>");
												print("</div>");
											print("</div>");
										print("</fieldset>");
									print("</div>");
								}
								
							print("</div>");
						print("</div>");
						
						print("<button onclick='return jsaddCategory();' type='submit' class='btn btn-success'>Add New</button>");
				
					break;
				}
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>