<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * config-delivery-locations.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "config-delivery-locations"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['deliveryLocationID'])) $deliveryLocationID = $_REQUEST['deliveryLocationID']; else $deliveryLocationID = "";
	
	
	switch($strcmd){
		case "locationSave":
			
			$postcode = (isset($_REQUEST['frm_postcode'])) ? $_REQUEST['frm_postcode'] : "";
			$country = (isset($_REQUEST['frm_country'])) ? $_REQUEST['frm_country'] : "";
			$zone = (isset($_REQUEST['frm_zone'])) ? $_REQUEST['frm_zone'] : "";
			$deliveryDays = (isset($_REQUEST['frm_deliveryDays'])) ? $_REQUEST['frm_deliveryDays'] : "";
			$customsForm = (isset($_REQUEST['frm_customsForm'])) ? $_REQUEST['frm_customsForm'] : "";
			$freeIfOver = (isset($_REQUEST['frm_freeIfOver'])) ? $_REQUEST['frm_freeIfOver'] : "";
			$minSpend = (isset($_REQUEST['frm_minSpend'])) ? $_REQUEST['frm_minSpend'] : "";
			$cost = (isset($_REQUEST['frm_cost'])) ? $_REQUEST['frm_cost'] : "";
			$allowDelivery = (isset($_REQUEST['frm_allowDelivery'])) ? $_REQUEST['frm_allowDelivery'] : "";

			
			if (!empty($deliveryLocationID)){ //update existing
			
				//get delivery location details
				$sqlquery = "SELECT * FROM deliveryLocations WHERE recordID = :recordID";
				$strType = "single";
				$arrdbparams = array("recordID"=>$deliveryLocationID);
				$resultdata = query($conn, $sqlquery, $strType, $arrdbparams);
				
				//update customer
				if(!empty($resultdata['recordID'])){
				
					$sqlquery = "UPDATE deliveryLocations SET postcode = :postcode, country = :country, zone = :zone, deliveryDays = :deliveryDays, minOrderValue = :minSpend, customsForm = :customsForm, freeIfOver = :freeIfOver, cost = :cost, allowDelivery = :allowDelivery WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array("recordID"=>$deliveryLocationID, "postcode"=>$postcode, "country"=>$country, "zone"=>$zone, "minSpend"=>$minSpend, "deliveryDays"=>$deliveryDays, "customsForm"=>$customsForm, "freeIfOver"=>$freeIfOver, "cost"=>$cost, "allowDelivery"=>$allowDelivery);
					$updateResult = query($conn, $sqlquery, $strType, $arrdbparams);
				
					$strsuccess = "Successfully updated delivery location details.";
					$strcmd = "";
				
				} else {
					$strerror = "Delivery location details could not be found. Please try again. ";
					$deliveryLocationID = "";
					$strcmd = "locationEdit";
				}
			
			
			} else { //insert new
			
				//create delivery location
				$sqlquery = "INSERT INTO deliveryLocations (postcode, country, zone, deliveryDays, customsForm, freeIfOver, cost, allowDelivery) VALUES (:postcode, :country, :zone, :deliveryDays, :customsForm, :freeIfOver, :cost, :allowDelivery)";
				$strType = "insert";
				$arrdbparams = array("postcode"=>$postcode, "country"=>$country, "zone"=>$zone, "deliveryDays"=>$deliveryDays, "customsForm"=>$customsForm, "freeIfOver"=>$freeIfOver, "cost"=>$cost, "allowDelivery"=>$allowDelivery);
				$insertResult = query($conn, $sqlquery, $strType, $arrdbparams);
				
				if($insertResult){
					$strsuccess = "Successfully created delivery location details.";
					$strcmd = "";
				} else {
					$strerror = "There was a problem creating the delivery location details please try again.";
					$strcmd = "locationAdd";
				}
			}
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Delivery Locations</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function fnCancel() {
					document.form.cmd.value="";
					document.form.submit();
				}
				function fnDeliveryLocationsAdd(){
					document.form.cmd.value="locationAdd";
					document.form.submit();
				}
				function fnEditDeliveryLocations(deliveryLocationID){
					document.form.cmd.value="locationEdit";
					document.form.deliveryLocationID.value=deliveryLocationID;
					document.form.submit();
				}
				function fnDeliveryLocationSave(){
					document.form.cmd.value="locationSave";
					document.form.submit();
				}
			</script>
			<?php
		
			print("<form action='config-delivery-locations.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='deliveryLocationID' id='deliveryLocationID' value='$deliveryLocationID'/>");

				
				switch($strcmd) {

					case "locationAdd":
					case "locationEdit":
							
							if (!empty($deliveryLocationID) AND $strcmd == "locationEdit") {
								$customerDetailsQuery = "SELECT * FROM deliveryLocations WHERE recordID = :deliveryLocationID";
								$strType = "single";
								$arrdbparam = array("deliveryLocationID"=>$deliveryLocationID);
								$resultdata = query($conn, $customerDetailsQuery, $strType, $arrdbparam);
								
								if (!empty($resultdata['recordID'])) {
									$postcode = $resultdata['postcode'];
									$country = $resultdata['country'];
									$zone = $resultdata['zone'];
									$deliveryDays = $resultdata['deliveryDays'];
									$customsForm = $resultdata['customsForm'];
									$freeIfOver = $resultdata['freeIfOver'];
									$minSpend = $resultdata['minOrderValue'];
									$cost = $resultdata['cost'];
									$allowDelivery = $resultdata['allowDelivery'];
								} else {
									$strerror = "Delivery location could not be loaded";
									$postcode = (isset($_REQUEST['frm_postcode'])) ? $_REQUEST['frm_postcode'] : "";
									$country = (isset($_REQUEST['frm_country'])) ? $_REQUEST['frm_country'] : "United Kingdom";
									$zone = (isset($_REQUEST['frm_zone'])) ? $_REQUEST['frm_zone'] : "";
									$deliveryDays = (isset($_REQUEST['frm_deliveryDays'])) ? $_REQUEST['frm_deliveryDays'] : "";
									$customsForm = (isset($_REQUEST['frm_customsForm'])) ? $_REQUEST['frm_customsForm'] : "";
									$freeIfOver = (isset($_REQUEST['frm_freeIfOver'])) ? $_REQUEST['frm_freeIfOver'] : "";
									$minSpend = (isset($_REQUEST['frm_minSpend'])) ? $_REQUEST['frm_minSpend'] : "";
									$cost = (isset($_REQUEST['frm_cost'])) ? $_REQUEST['frm_cost'] : "";
									$allowDelivery = (isset($_REQUEST['frm_allowDelivery'])) ? $_REQUEST['frm_allowDelivery'] : "";
								}
								
							} else {
								$postcode = (isset($_REQUEST['frm_postcode'])) ? $_REQUEST['frm_postcode'] : "";
								$country = (isset($_REQUEST['frm_country'])) ? $_REQUEST['frm_country'] : "United Kingdom";
								$zone = (isset($_REQUEST['frm_zone'])) ? $_REQUEST['frm_zone'] : "";
								$deliveryDays = (isset($_REQUEST['frm_deliveryDays'])) ? $_REQUEST['frm_deliveryDays'] : "";
								$customsForm = (isset($_REQUEST['frm_customsForm'])) ? $_REQUEST['frm_customsForm'] : "";
								$freeIfOver = (isset($_REQUEST['frm_freeIfOver'])) ? $_REQUEST['frm_freeIfOver'] : "";
								$minSpend = (isset($_REQUEST['frm_minSpend'])) ? $_REQUEST['frm_minSpend'] : "";
								$cost = (isset($_REQUEST['frm_cost'])) ? $_REQUEST['frm_cost'] : "";
								$allowDelivery = (isset($_REQUEST['frm_allowDelivery'])) ? $_REQUEST['frm_allowDelivery'] : "";
							}
							

							print("<div class='row'>");
								print("<div class='col-md-6 split even-left'>");

									print("<div class='box-light' id='deltable".$strclass."'>");
										print("<div class='section'>");
											print("<fieldset>");
												print("<legend id='addLegend'>Delivery Location</legend>");

												print("<div class='row'>");
													
													print("<div class='form-group col-sm-6 even-left'>");
														print("<label for='frm_postcode' class='control-label'>Postcode:</label>");
														print("<input class='form-control' id='frm_postcode' name='frm_postcode' type='text' placeholder='Postcode' value='$postcode' />");
													print("</div>");

													print("<div class='form-group col-sm-6 even-right'>");
														print("<label for='frm_country' class='control-label'>Country:</label>");
														if ($country == "") $country = "United Kingdom";
														$strdbsql = "SELECT * FROM configCountries ORDER BY countryName";
														$arrdbparams = array();
														$strType = "multi";
														$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
														print ("<select name='frm_country' id='frm_country' class='form-control' placeholder='Country' >");
															print ("<option ".(($country == "*") ? "selected" : "")." value='".$row['countryName']."'>Any Country in Zone</option>");
														foreach($resultdata AS $row){	
															if ($country == $row['countryName']) $selected = "selected"; else $selected = "";
															print ("<option $selected value='".$row['countryName']."'>".$row['countryName']."</option>");
														}
														print ("</select>");
													
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<label for='frm_zone' class='control-label'>Zone:</label>");
														print("<input class='form-control' id='frm_zone' name='frm_zone' type='text' placeholder='Zone' value='$zone' />");
													print("</div>");
	
													print("<div class='form-group col-sm-6 even-right hide'>");
														print("<label for='frm_deliveryDays' class='control-label'>Delivery Days:</label>");
														print("<input class='form-control' id='frm_deliveryDays' name='frm_deliveryDays' type='text' placeholder='Delivery Days' value='$deliveryDays' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<label for='frm_customsForm' class='control-label'>Customs Form:</label>");
														print("<input class='form-control' id='frm_customsForm' name='frm_customsForm' type='text' placeholder='Customs Form' value='$customsForm' />");
													print("</div>");
	
													print("<div class='form-group col-sm-6 even-right hide'>");
														print("<label for='frm_freeIfOver' class='control-label'>Free if Over:</label>");
														print("<input class='form-control' id='frm_freeIfOver' name='frm_freeIfOver' type='text' placeholder='Free if Over £' value='$freeIfOver' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<label for='frm_minSpend' class='control-label'>Minimum Spend:</label>");
														print("<input class='form-control' id='frm_minSpend' name='frm_minSpend' type='text' placeholder='Minimum Spend' value='$minSpend' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<label for='frm_cost' class='control-label'>Cost Modifier:</label>");
														print("<input class='form-control' id='frm_cost' name='frm_cost' type='text' placeholder='Cost Modifier' value='$cost' />");
													print("</div>");

													print("<div class='form-group col-sm-6 even-right'>");
														print("<label for='frm_allowDelivery' class='control-label'>Allow Delivery:</label>");
														print ("<select name='frm_allowDelivery' id='frm_allowDelivery' class='form-control' placeholder='Allow Delivery' >");
															if ($allowDelivery == 1) $selected = "selected"; else $selected = "";
															print ("<option $selected value='1'>Delivery Allowed</option>");
															if ($allowDelivery == 0) $selected = "selected"; else $selected = "";
															print ("<option $selected value='0'>Delivery Not Allowed</option>");
														print ("</select>");
													print("</div>");
												print("</div>");

											print("</fieldset>");
										print("</div>");
									print("</div>");
	
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-3' style='text-align:left;'>");
									print("<button onclick='return fnCancel();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-3' style='text-align:right;'>");
									print("<button onclick='return fnDeliveryLocationSave(this);' type='submit' class='btn btn-success right' style='display:inline-block;'>Save</button> ");
								print("</div>");
							print("</div>");
					break;
					default:				
						print("<button style='margin-bottom: 15px;' onclick='return fnDeliveryLocationsAdd();' type='submit' class='btn btn-success'>Add New</button>");
						print("<div class='section'>");
							print("<table id='' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th>Postcode</th>");
									print("<th>Country</th>");
									print("<th>Zone</th>");
									print("<th>Delivery Days</th>");
									print("<th>Customs Form</th>");
									print("<th>Minimum Spend</th>");
									print("<th>Free If Order Over</th>");
									print("<th>Cost Modifier</th>");
									print("<th>Delivery</th>");
									print("<th style='text-align:center;'>Edit</th>");
								print ("</tr></thead><tbody>");
							
								$strdbsql = "SELECT * FROM deliveryLocations ORDER BY country,postcode DESC";
								$strType = "multi";
								$arrdbparams = array();
								$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

								if (count($resultdata) > 0) {
									foreach ($resultdata as $row) {
										print ("<tr>");
											if ($row['allowDelivery'] == 0) $tablecolour = "danger";
											else $tablecolour = "success";
											print ("<td class='$tablecolour'>".$row['postcode']."</td>");
											print ("<td class='$tablecolour'>".$row['country']."</td>");
											print ("<td class='$tablecolour'>".$row['zone']."</td>");
											print ("<td class='$tablecolour'>".$row['deliveryDays']."</td>");
											print ("<td class='$tablecolour'>".$row['customsForm']."</td>");
											print ("<td class='$tablecolour'>".number_format($row['minOrderValue'],2)."</td>");
											print ("<td class='$tablecolour'>".number_format($row['freeIfOver'],2)."</td>");
											print ("<td class='$tablecolour'>".number_format($row['cost'],2)."</td>");
											print ("<td class='$tablecolour'>");
												switch ($row['allowDelivery']) {
													case 0: print ("Delivery Not Allowed"); break;
													case 1: print ("Delivery Allowed"); break;
												}
											print ("</td>");											
											print("<td class='$tablecolour'><button onclick='fnEditDeliveryLocations(".$row['recordID']."); return false;' class='btn btn-primary circle' type='submit' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");									
										print ("</tr>");
									}
								}
							print ("</tbody></table>");
						print("</div>");
						print("<button onclick='return fnDeliveryLocationsAdd();' type='submit' class='btn btn-success'>Add New</button>");
					break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
	?>
	<script language='Javascript'>
		$().ready(function() {
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
		});
	</script>
	<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>