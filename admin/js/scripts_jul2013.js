/* Home Page JCarousel */
(function(g){var q={vertical:!1,rtl:!1,start:1,offset:1,size:null,scroll:3,visible:null,animation:"normal",easing:"swing",auto:0,wrap:null,initCallback:null,setupCallback:null,reloadCallback:null,itemLoadCallback:null,itemFirstInCallback:null,itemFirstOutCallback:null,itemLastInCallback:null,itemLastOutCallback:null,itemVisibleInCallback:null,itemVisibleOutCallback:null,animationStepCallback:null,buttonNextHTML:"<div></div>",buttonPrevHTML:"<div></div>",buttonNextEvent:"click",buttonPrevEvent:"click", buttonNextCallback:null,buttonPrevCallback:null,itemFallbackDimension:null},m=!1;g(window).bind("load.jcarousel",function(){m=!0});g.jcarousel=function(a,c){this.options=g.extend({},q,c||{});this.autoStopped=this.locked=!1;this.buttonPrevState=this.buttonNextState=this.buttonPrev=this.buttonNext=this.list=this.clip=this.container=null;if(!c||c.rtl===void 0)this.options.rtl=(g(a).attr("dir")||g("html").attr("dir")||"").toLowerCase()=="rtl";this.wh=!this.options.vertical?"width":"height";this.lt=!this.options.vertical? this.options.rtl?"right":"left":"top";for(var b="",d=a.className.split(" "),f=0;f<d.length;f++)if(d[f].indexOf("jcarousel-skin")!=-1){g(a).removeClass(d[f]);b=d[f];break}a.nodeName.toUpperCase()=="UL"||a.nodeName.toUpperCase()=="OL"?(this.list=g(a),this.clip=this.list.parents(".jcarousel-clip"),this.container=this.list.parents(".jcarousel-container")):(this.container=g(a),this.list=this.container.find("ul,ol").eq(0),this.clip=this.container.find(".jcarousel-clip"));if(this.clip.size()===0)this.clip= this.list.wrap("<div></div>").parent();if(this.container.size()===0)this.container=this.clip.wrap("<div></div>").parent();b!==""&&this.container.parent()[0].className.indexOf("jcarousel-skin")==-1&&this.container.wrap('<div class=" '+b+'"></div>');this.buttonPrev=g(".jcarousel-prev",this.container);if(this.buttonPrev.size()===0&&this.options.buttonPrevHTML!==null)this.buttonPrev=g(this.options.buttonPrevHTML).appendTo(this.container);this.buttonPrev.addClass(this.className("jcarousel-prev"));this.buttonNext= g(".jcarousel-next",this.container);if(this.buttonNext.size()===0&&this.options.buttonNextHTML!==null)this.buttonNext=g(this.options.buttonNextHTML).appendTo(this.container);this.buttonNext.addClass(this.className("jcarousel-next"));this.clip.addClass(this.className("jcarousel-clip")).css({position:"relative"});this.list.addClass(this.className("jcarousel-list")).css({overflow:"hidden",position:"relative",top:0,margin:0,padding:0}).css(this.options.rtl?"right":"left",0);this.container.addClass(this.className("jcarousel-container")).css({position:"relative"}); !this.options.vertical&&this.options.rtl&&this.container.addClass("jcarousel-direction-rtl").attr("dir","rtl");var j=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null,b=this.list.children("li"),e=this;if(b.size()>0){var h=0,i=this.options.offset;b.each(function(){e.format(this,i++);h+=e.dimension(this,j)});this.list.css(this.wh,h+100+"px");if(!c||c.size===void 0)this.options.size=b.size()}this.container.css("display","block");this.buttonNext.css("display","block");this.buttonPrev.css("display", "block");this.funcNext=function(){e.next()};this.funcPrev=function(){e.prev()};this.funcResize=function(){e.resizeTimer&&clearTimeout(e.resizeTimer);e.resizeTimer=setTimeout(function(){e.reload()},100)};this.options.initCallback!==null&&this.options.initCallback(this,"init");!m&&g.browser.safari?(this.buttons(!1,!1),g(window).bind("load.jcarousel",function(){e.setup()})):this.setup()};var f=g.jcarousel;f.fn=f.prototype={jcarousel:"0.2.8"};f.fn.extend=f.extend=g.extend;f.fn.extend({setup:function(){this.prevLast= this.prevFirst=this.last=this.first=null;this.animating=!1;this.tail=this.resizeTimer=this.timer=null;this.inTail=!1;if(!this.locked){this.list.css(this.lt,this.pos(this.options.offset)+"px");var a=this.pos(this.options.start,!0);this.prevFirst=this.prevLast=null;this.animate(a,!1);g(window).unbind("resize.jcarousel",this.funcResize).bind("resize.jcarousel",this.funcResize);this.options.setupCallback!==null&&this.options.setupCallback(this)}},reset:function(){this.list.empty();this.list.css(this.lt, "0px");this.list.css(this.wh,"10px");this.options.initCallback!==null&&this.options.initCallback(this,"reset");this.setup()},reload:function(){this.tail!==null&&this.inTail&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+this.tail);this.tail=null;this.inTail=!1;this.options.reloadCallback!==null&&this.options.reloadCallback(this);if(this.options.visible!==null){var a=this,c=Math.ceil(this.clipping()/this.options.visible),b=0,d=0;this.list.children("li").each(function(f){b+=a.dimension(this, c);f+1<a.first&&(d=b)});this.list.css(this.wh,b+"px");this.list.css(this.lt,-d+"px")}this.scroll(this.first,!1)},lock:function(){this.locked=!0;this.buttons()},unlock:function(){this.locked=!1;this.buttons()},size:function(a){if(a!==void 0)this.options.size=a,this.locked||this.buttons();return this.options.size},has:function(a,c){if(c===void 0||!c)c=a;if(this.options.size!==null&&c>this.options.size)c=this.options.size;for(var b=a;b<=c;b++){var d=this.get(b);if(!d.length||d.hasClass("jcarousel-item-placeholder"))return!1}return!0}, get:function(a){return g(">.jcarousel-item-"+a,this.list)},add:function(a,c){var b=this.get(a),d=0,p=g(c);if(b.length===0)for(var j,e=f.intval(a),b=this.create(a);;){if(j=this.get(--e),e<=0||j.length){e<=0?this.list.prepend(b):j.after(b);break}}else d=this.dimension(b);p.get(0).nodeName.toUpperCase()=="LI"?(b.replaceWith(p),b=p):b.empty().append(c);this.format(b.removeClass(this.className("jcarousel-item-placeholder")),a);p=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible): null;d=this.dimension(b,p)-d;a>0&&a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))-d+"px");this.list.css(this.wh,f.intval(this.list.css(this.wh))+d+"px");return b},remove:function(a){var c=this.get(a);if(c.length&&!(a>=this.first&&a<=this.last)){var b=this.dimension(c);a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+b+"px");c.remove();this.list.css(this.wh,f.intval(this.list.css(this.wh))-b+"px")}},next:function(){this.tail!==null&&!this.inTail?this.scrollTail(!1): this.scroll((this.options.wrap=="both"||this.options.wrap=="last")&&this.options.size!==null&&this.last==this.options.size?1:this.first+this.options.scroll)},prev:function(){this.tail!==null&&this.inTail?this.scrollTail(!0):this.scroll((this.options.wrap=="both"||this.options.wrap=="first")&&this.options.size!==null&&this.first==1?this.options.size:this.first-this.options.scroll)},scrollTail:function(a){if(!this.locked&&!this.animating&&this.tail){this.pauseAuto();var c=f.intval(this.list.css(this.lt)), c=!a?c-this.tail:c+this.tail;this.inTail=!a;this.prevFirst=this.first;this.prevLast=this.last;this.animate(c)}},scroll:function(a,c){!this.locked&&!this.animating&&(this.pauseAuto(),this.animate(this.pos(a),c))},pos:function(a,c){var b=f.intval(this.list.css(this.lt));if(this.locked||this.animating)return b;this.options.wrap!="circular"&&(a=a<1?1:this.options.size&&a>this.options.size?this.options.size:a);for(var d=this.first>a,g=this.options.wrap!="circular"&&this.first<=1?1:this.first,j=d?this.get(g): this.get(this.last),e=d?g:g-1,h=null,i=0,k=!1,l=0;d?--e>=a:++e<a;){h=this.get(e);k=!h.length;if(h.length===0&&(h=this.create(e).addClass(this.className("jcarousel-item-placeholder")),j[d?"before":"after"](h),this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)));j=h;l=this.dimension(h);k&&(i+=l);if(this.first!==null&&(this.options.wrap=="circular"||e>=1&&(this.options.size===null||e<= this.options.size)))b=d?b+l:b-l}for(var g=this.clipping(),m=[],o=0,n=0,j=this.get(a-1),e=a;++o;){h=this.get(e);k=!h.length;if(h.length===0){h=this.create(e).addClass(this.className("jcarousel-item-placeholder"));if(j.length===0)this.list.prepend(h);else j[d?"before":"after"](h);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)))}j=h;l=this.dimension(h);if(l===0)throw Error("jCarousel: No width/height set for items. This will cause an infinite loop. Aborting..."); this.options.wrap!="circular"&&this.options.size!==null&&e>this.options.size?m.push(h):k&&(i+=l);n+=l;if(n>=g)break;e++}for(h=0;h<m.length;h++)m[h].remove();i>0&&(this.list.css(this.wh,this.dimension(this.list)+i+"px"),d&&(b-=i,this.list.css(this.lt,f.intval(this.list.css(this.lt))-i+"px")));i=a+o-1;if(this.options.wrap!="circular"&&this.options.size&&i>this.options.size)i=this.options.size;if(e>i){o=0;e=i;for(n=0;++o;){h=this.get(e--);if(!h.length)break;n+=this.dimension(h);if(n>=g)break}}e=i-o+ 1;this.options.wrap!="circular"&&e<1&&(e=1);if(this.inTail&&d)b+=this.tail,this.inTail=!1;this.tail=null;if(this.options.wrap!="circular"&&i==this.options.size&&i-o+1>=1&&(d=f.intval(this.get(i).css(!this.options.vertical?"marginRight":"marginBottom")),n-d>g))this.tail=n-g-d;if(c&&a===this.options.size&&this.tail)b-=this.tail,this.inTail=!0;for(;a-- >e;)b+=this.dimension(this.get(a));this.prevFirst=this.first;this.prevLast=this.last;this.first=e;this.last=i;return b},animate:function(a,c){if(!this.locked&& !this.animating){this.animating=!0;var b=this,d=function(){b.animating=!1;a===0&&b.list.css(b.lt,0);!b.autoStopped&&(b.options.wrap=="circular"||b.options.wrap=="both"||b.options.wrap=="last"||b.options.size===null||b.last<b.options.size||b.last==b.options.size&&b.tail!==null&&!b.inTail)&&b.startAuto();b.buttons();b.notify("onAfterAnimation");if(b.options.wrap=="circular"&&b.options.size!==null)for(var c=b.prevFirst;c<=b.prevLast;c++)c!==null&&!(c>=b.first&&c<=b.last)&&(c<1||c>b.options.size)&&b.remove(c)}; this.notify("onBeforeAnimation");if(!this.options.animation||c===!1)this.list.css(this.lt,a+"px"),d();else{var f=!this.options.vertical?this.options.rtl?{right:a}:{left:a}:{top:a},d={duration:this.options.animation,easing:this.options.easing,complete:d};if(g.isFunction(this.options.animationStepCallback))d.step=this.options.animationStepCallback;this.list.animate(f,d)}}},startAuto:function(a){if(a!==void 0)this.options.auto=a;if(this.options.auto===0)return this.stopAuto();if(this.timer===null){this.autoStopped= !1;var c=this;this.timer=window.setTimeout(function(){c.next()},this.options.auto*1E3)}},stopAuto:function(){this.pauseAuto();this.autoStopped=!0},pauseAuto:function(){if(this.timer!==null)window.clearTimeout(this.timer),this.timer=null},buttons:function(a,c){if(a==null&&(a=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="first"||this.options.size===null||this.last<this.options.size),!this.locked&&(!this.options.wrap||this.options.wrap=="first")&&this.options.size!==null&& this.last>=this.options.size))a=this.tail!==null&&!this.inTail;if(c==null&&(c=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="last"||this.first>1),!this.locked&&(!this.options.wrap||this.options.wrap=="last")&&this.options.size!==null&&this.first==1))c=this.tail!==null&&this.inTail;var b=this;this.buttonNext.size()>0?(this.buttonNext.unbind(this.options.buttonNextEvent+".jcarousel",this.funcNext),a&&this.buttonNext.bind(this.options.buttonNextEvent+".jcarousel",this.funcNext), this.buttonNext[a?"removeClass":"addClass"](this.className("jcarousel-next-disabled")).attr("disabled",a?!1:!0),this.options.buttonNextCallback!==null&&this.buttonNext.data("jcarouselstate")!=a&&this.buttonNext.each(function(){b.options.buttonNextCallback(b,this,a)}).data("jcarouselstate",a)):this.options.buttonNextCallback!==null&&this.buttonNextState!=a&&this.options.buttonNextCallback(b,null,a);this.buttonPrev.size()>0?(this.buttonPrev.unbind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev), c&&this.buttonPrev.bind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev),this.buttonPrev[c?"removeClass":"addClass"](this.className("jcarousel-prev-disabled")).attr("disabled",c?!1:!0),this.options.buttonPrevCallback!==null&&this.buttonPrev.data("jcarouselstate")!=c&&this.buttonPrev.each(function(){b.options.buttonPrevCallback(b,this,c)}).data("jcarouselstate",c)):this.options.buttonPrevCallback!==null&&this.buttonPrevState!=c&&this.options.buttonPrevCallback(b,null,c);this.buttonNextState= a;this.buttonPrevState=c},notify:function(a){var c=this.prevFirst===null?"init":this.prevFirst<this.first?"next":"prev";this.callback("itemLoadCallback",a,c);this.prevFirst!==this.first&&(this.callback("itemFirstInCallback",a,c,this.first),this.callback("itemFirstOutCallback",a,c,this.prevFirst));this.prevLast!==this.last&&(this.callback("itemLastInCallback",a,c,this.last),this.callback("itemLastOutCallback",a,c,this.prevLast));this.callback("itemVisibleInCallback",a,c,this.first,this.last,this.prevFirst, this.prevLast);this.callback("itemVisibleOutCallback",a,c,this.prevFirst,this.prevLast,this.first,this.last)},callback:function(a,c,b,d,f,j,e){if(!(this.options[a]==null||typeof this.options[a]!="object"&&c!="onAfterAnimation")){var h=typeof this.options[a]=="object"?this.options[a][c]:this.options[a];if(g.isFunction(h)){var i=this;if(d===void 0)h(i,b,c);else if(f===void 0)this.get(d).each(function(){h(i,this,d,b,c)});else for(var a=function(a){i.get(a).each(function(){h(i,this,a,b,c)})},k=d;k<=f;k++)k!== null&&!(k>=j&&k<=e)&&a(k)}}},create:function(a){return this.format("<li></li>",a)},format:function(a,c){for(var a=g(a),b=a.get(0).className.split(" "),d=0;d<b.length;d++)b[d].indexOf("jcarousel-")!=-1&&a.removeClass(b[d]);a.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-"+c)).css({"float":this.options.rtl?"right":"left","list-style":"none"}).attr("jcarouselindex",c);return a},className:function(a){return a+" "+a+(!this.options.vertical?"-horizontal":"-vertical")}, dimension:function(a,c){var b=g(a);if(c==null)return!this.options.vertical?b.outerWidth(!0)||f.intval(this.options.itemFallbackDimension):b.outerHeight(!0)||f.intval(this.options.itemFallbackDimension);else{var d=!this.options.vertical?c-f.intval(b.css("marginLeft"))-f.intval(b.css("marginRight")):c-f.intval(b.css("marginTop"))-f.intval(b.css("marginBottom"));g(b).css(this.wh,d+"px");return this.dimension(b)}},clipping:function(){return!this.options.vertical?this.clip[0].offsetWidth-f.intval(this.clip.css("borderLeftWidth"))- f.intval(this.clip.css("borderRightWidth")):this.clip[0].offsetHeight-f.intval(this.clip.css("borderTopWidth"))-f.intval(this.clip.css("borderBottomWidth"))},index:function(a,c){if(c==null)c=this.options.size;return Math.round(((a-1)/c-Math.floor((a-1)/c))*c)+1}});f.extend({defaults:function(a){return g.extend(q,a||{})},intval:function(a){a=parseInt(a,10);return isNaN(a)?0:a},windowLoaded:function(){m=!0}});g.fn.jcarousel=function(a){if(typeof a=="string"){var c=g(this).data("jcarousel"),b=Array.prototype.slice.call(arguments, 1);return c[a].apply(c,b)}else return this.each(function(){var b=g(this).data("jcarousel");b?(a&&g.extend(b.options,a),b.reload()):g(this).data("jcarousel",new f(this,a))})}})(jQuery);
/* All Shop Pages Cookie PopUps */
var cookieStatus = 0;
var hoverStatus = 0;

//loading popup with jQuery magic!
function loadCookiePopup(){
	//loads popup only if it is disabled
	if(cookieStatus==0 && hoverStatus==0){
		$("#backgroundCookie").css({
			"opacity": "0.7"
		});
		$("#backgroundCookie").fadeIn("slow");
		$("#popupCookie").fadeIn("slow");
		cookieStatus = 1;
	}
}

//disabling popup with jQuery magic!
function disableCookiePopup(){
	//disables popup only if it is enabled
	if(cookieStatus>0){
		$("#backgroundCookie").fadeOut("slow", function() { $(this).remove(); });
	 	$("#popupCookie").fadeOut("slow", function() { $(this).remove(); });
		cookieStatus = 0;
	}
}

//centering popup
function centerCookiePopup(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popupCookie").height();
	var popupWidth = $("#popupCookie").width();
	//centering    
	if(windowHeight > popupHeight) {
	$("#popupCookie").css({
		"position": "absolute",
		"top": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	} else {
	$("#popupCookie").css({
		"position": "absolute",
		"top": 20,
		"left": windowWidth/2-popupWidth/2
	});
	}
	//only need force for IE6

	$("#backgroundCookie").css({
		"height": windowHeight
	});
	
}
function showcookie() {if(hoverStatus==0){ $("#cookie").fadeTo("slow",1.0); $("#cookie").animate({height:"60px"},"slow","linear"); } }
function hidecookie() { if(hoverStatus==0){ $("#cookie").fadeTo("slow",0.9); $("#cookie").animate({height:"20px"},"slow","linear"); } }

function deletecookie() {
	hoverStatus = 1;
	disableCookiePopup();
	setCookie("eucl",1,365);
	$("#cookie").hide();
}
function cookieinfo() {
	var request = "/calcs/ajaxcookie.php";
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("GET",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response = xmlhttp.responseText;
			$(document.body).append(response);
			$("#popupCookieClose").click(function(){deletecookie();});
			$("#backgroundCookie").click(function(){deletecookie();});
			$(document).keypress(function(e){ if(e.keyCode==27 && cookieStatus<2){ deletecookie(); } });
			centerCookiePopup();
			loadCookiePopup();
		}
	}
	xmlhttp.send();
}
function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}

//CONTROLLING EVENTS IN jQuery
$(document).ready(
	function(){ 
		setTimeout(function(){hidecookie();},1000); 
		$("#cookie").mouseenter(function(){showcookie();}); 
		$("#cookie").mouseleave(function(){hidecookie();});
	}
);

/* Shop List and Item Page Functions */
function preload(arrayOfImages) {
  $(arrayOfImages).each(function(){
    $('<img/>')[0].src = this;
  });
}
/** Basket **/

function jsaddmain()
{
	var product = document.frm_form.product.value;
	var qty = document.frm_form.qty.value;
	jsajaxadd(product, qty);
}

function jsadd(strstockcode)
{
	var qty = document.getElementById("qty_"+strstockcode).value;
	jsajaxadd(strstockcode, qty);
}
function jsshopadd(strstockcode)
{
	strstockcode2 = eval("document.frm_form.type_" + strstockcode + ".value");
	quantity = eval("document.frm_form.qty_" + strstockcode + ".value");
	jsajaxadd(strstockcode2, quantity);
}
function jsupdate()
{
	var postage;
	var radios = document.getElementsByName('postage');
	for (var i = 0, length = radios.length; i < length; i++) {
		if (radios[i].checked) {
			postage = radios[i].value;
    		}
	}
	jsajaxbasket("changepostage", null, postage);
	var code = document.frm_form.discount2.value;
	jsajaxbasket("discount", null, code);
}
function jsoffer()
{
	var code = document.frm_form.discount.value;
	document.frm_form.discount2.value = code;
	jsajaxbasket("discount", null, code);
}

function jsnewqty(intbasketcounter,strchange,intoldqty)
{
	var intnewqty;
	switch (strchange)
	{
		case "m": intnewqty = (intoldqty * 1) - 1; break;
		case "a":	intnewqty = (intoldqty * 1) + 1; break;
		case "r":	intnewqty = 0;	break;
	}
	jsajaxbasket("qty", intbasketcounter, intnewqty);
	var code = document.frm_form.discount2.value;
	jsajaxbasket("discount", null, code);
}


function jsajaxadd(product, qty)
{
	var request = "/calcs/ajaxadd.php";
	var vars = "command=ajaxadd&product="+product+"&qty="+qty+"&xml=true";
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			var backetcontents = xml.getElementsByTagName("header")[0].childNodes[0].nodeValue;
			var popupcontents = xml.getElementsByTagName("popup")[0].childNodes[0].nodeValue;
			document.getElementById("topbasket_items").innerHTML=backetcontents;
			document.getElementById("basketpopup").innerHTML = "<h2 style='font-size:14px'>Your Basket has been updated</h2>\n"+popupcontents+"    <a onclick='this.parentNode.style.display=\"none\"' class='short_button'>continue shopping</a> <a href='/basket.php' class='short_button'>view basket</a>";
			document.getElementById("basketpopup").style.display = "block";
		} else if (xmlhttp.readyState==4) {
			document.getElementById("basketpopup").innerHTML = "<h2 style='font-size:14px'>Error</h2>\n<p>There has been an error updating your basket.</p> <a onclick='this.parentNode.style.display=\"none\"' class='short_button'>continue shopping</a> <a href='/basket.php' class='short_button'>view basket</a>";
			document.getElementById("basketpopup").style.display = "block";
			
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
}
function jsajaxbasket(command,product,adjustment)
{
	var request = "/calcs/ajaxbasket.php";
	var vars = "command="+command
	switch (command) {
		case "qty": vars += "&basket="+product+"&newqty="+adjustment; break;
		case "discount": vars += "&discount="+adjustment; break;
		case "changepostage": vars += "&postage="+adjustment; break;
	}
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			var backetcontents = xml.getElementsByTagName("header")[0].childNodes[0].nodeValue;
			document.getElementById("topbasket_items").innerHTML=backetcontents;

			var goodstotal = xml.getElementsByTagName("goodstotal")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_goodstotal").innerHTML="&pound; "+goodstotal;

			var goodsitems = xml.getElementsByTagName("goodsitems")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_items").innerHTML=goodsitems;
			
			var subtotal = xml.getElementsByTagName("subtotal")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_subtotal").innerHTML="&pound; "+subtotal;

			var ordertotal = xml.getElementsByTagName("total")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_total").innerHTML="&pound; "+ordertotal;

			var offertext = xml.getElementsByTagName("offerdesc")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_offertext").innerHTML=offertext;
			
			var offercode = xml.getElementsByTagName("offercode")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_offercode").innerHTML=offercode;
			
			var offeramt = xml.getElementsByTagName("offeramount")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_offeramt").innerHTML=offeramt;
			
			var getsoffer = xml.getElementsByTagName("getsoffer")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_getsdiscount").innerHTML=getsoffer;
			
			var standardpostage = xml.getElementsByTagName("standardpost")[0].childNodes[0].nodeValue;
			document.getElementById("ajax_stanpostage").innerHTML="&pound; "+standardpostage;
			
			var delarray = xml.getElementsByTagName("deleterow");
			if(delarray.length != 0) {
				var deleteid = delarray[0].childNodes[0].nodeValue;
				 var element = document.getElementById("row_"+deleteid);
				 element.parentNode.removeChild(element);
			}

			var editarray = xml.getElementsByTagName("rowID");
			if(editarray.length != 0) {
				for (i = 0; i<editarray.length; i= i+1) {
					var editid = editarray[i].childNodes[0].nodeValue;
					var editdata = xml.getElementsByTagName("rowQty")[i].childNodes[0].nodeValue;
					document.getElementById("qty_"+editid).innerHTML = editdata;
					var editdata = xml.getElementsByTagName("rowTotal")[i].childNodes[0].nodeValue;
					document.getElementById("price_"+editid).innerHTML = "&pound; "+editdata;
				}
			}
			document.getElementById("ajax_status").innerHTML = "";
			if(goodsitems == "0 items") document.location.reload(true);
			//document.location.reload(true);

		} else if (xmlhttp.readyState==4) {
			document.getElementById("ajax_status").innerHTML = " <span>There has been an error updating your basket please try again.</span> ";
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
	document.getElementById("ajax_status").innerHTML = " <img src='img/ajax-loader.gif' /> <span> Updating your basket</span> ";
}
function jsajaxbulkadd(products, qtys)
{
	for (i = 0; i<products.length; i= i+1) {
		var product = products[i];
		var qty = qtys[i];
		var request = "/calcs/ajaxadd.php";
		var vars = "command=ajaxadd&product="+product+"&qty="+qty+"&xml=true&multi=true";
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}

		if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

		xmlhttp.open("POST",request,true);
		xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			var backetcontents = xml.getElementsByTagName("header")[0].childNodes[0].nodeValue;
			var popupcontents = xml.getElementsByTagName("popup")[0].childNodes[0].nodeValue;
			document.getElementById("topbasket_items").innerHTML=backetcontents;
			document.getElementById("basketpopup").innerHTML = "<h2 style='font-size:14px'>Your Basket has been updated</h2>\n"+popupcontents+"    <a onclick='this.parentNode.style.display=\"none\"' class='short_button'>continue shopping</a> <a href='/basket.php' class='short_button'>view basket</a>";
			document.getElementById("basketpopup").style.display = "block";
		} else if (xmlhttp.readyState==4) {
			document.getElementById("basketpopup").innerHTML = "<h2 style='font-size:14px'>Error</h2>\n<p>There has been an error updating your basket.</p> <a onclick='this.parentNode.style.display=\"none\"' class='short_button'>continue shopping</a> <a href='/basket.php' class='short_button'>view basket</a>";
			document.getElementById("basketpopup").style.display = "block";
			
		}
		}
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send(vars);
	}
}

/***** Reveiws ****/
function jsaddreview()
{
	var product = document.frm_form.revstock.value;
	var revtitle = document.frm_form.revtitle.value;
	var revdetail = document.frm_form.revdetail.value;
	var revscore = document.frm_form.revscore.value;
	var revname = document.frm_form.revname.value;
	var revemail = document.frm_form.revemail.value;
	var request = "/calcs/ajaxreviews.php";
	var vars = "command=addreview&revstock="+product+"&revtitle="+revtitle+"&revdetail="+revdetail+"&revscore="+revscore+"&revname="+revname+"&revemail="+revemail;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response = xmlhttp.responseText;
			document.getElementById("popupReview").innerHTML = response;
		} else if (xmlhttp.readyState==4) {
			var response = xmlhttp.responseText;
			document.getElementById("reviewerror").innerHTML = response;
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);

}
function jsajaxreviews(product)
{
	var request = "/calcs/ajaxreviews.php";
	var vars = "id="+product;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response = xmlhttp.responseText;
			document.getElementById("reviewlist").innerHTML = response;
		} else if (xmlhttp.readyState==4) {
			alert("An error occured with your request, please try again later.");
			
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
}
var popup2Status = 0;
function loadPopup2(){
	//loads popup only if it is disabled
	if(popup2Status==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		$("#backgroundPopup").fadeIn("slow");
		$("#popupReview").fadeIn("slow");
		popup2Status = 1;
	}
}
function centerPopup2(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popupReview").height();
	var popupWidth = $("#popupReview").width();
	//centering
	$("#popupReview").css({"position": "fixed"});

	if (windowHeight > popupHeight) $("#popupReview").css({"top": (windowHeight/2)-(popupHeight/2)-14});
	else $("#popupReview").css({"top": 20});

	if (windowWidth > popupWidth) $("#popupReview").css({"left": (windowWidth/2)-(popupWidth/2)-14});
	else $("#popupReview").css({"left": 20});

	//only need force for IE6
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
	
}
//disabling popup with jQuery magic!
function disablePopup(){
	//disables popup only if it is enabled
	if(popup2Status==1){
		$("#backgroundPopup").fadeOut("slow");
		$("#popupReview").fadeOut("slow");
		popup2Status = 0;
	}
	$("#popupBasket").fadeOut("slow");
}
function updateproductselection(col, size) {
	var matched = false;
	var summary = "";

	//get appropriate row in array
	for (x in products) {
		if(products[x][2] == size && products[x][3] == col) {
			document.getElementById('itemImage').src = products[x][4];
			if(products[x][4] == "/images/no_image.gif") document.getElementById('itemImage').style.display = 'none';
			else document.getElementById('itemImage').style.display = '';
			document.getElementById('stockCode').innerHTML = products[x][0];
			document.frm_form.product.value = products[x][0];
	
			price = products[x][1].replace('.', '');
			price = parseInt(products[x][1].replace(',', ''));
			oldprice = products[x][8].replace('.', '');
			oldprice = parseInt(products[x][8].replace(',', ''));
	
			if(price < oldprice) document.getElementById('itemPrice').innerHTML = "<span style='color:red; text-decoration:line-through;font-size:small'>&pound; " + products[x][8] +"</span> &pound; " + products[x][1];
			else document.getElementById('itemPrice').innerHTML = "&pound; " + products[x][1];
			if (size != "" || col != "") summary = col + " " + size + " - &pound; " + products[x][1];
			else summary = "&pound; " + products[x][1];
			document.getElementById('itemSum').innerHTML = summary;
			matched = products[x][0];
			window.location = "#" + products[x][0];
		}
	}
	return matched;
}

function selectcolour(colour) {
  var size = document.frm_form.size.value;
  document.frm_form.colour.value = colour;
  previous = prodselected;
  prodselected = updateproductselection(colour,size);
  if (prodselected != false) {
    //update size selections presetting this one
    popsizes(colour,size);
    popcolours(size,colour);
  }
  else {
    //update size options and alert that the size selected was not available
    alert("The colour you have selected is not available in that size.");
    //set colour back to previous...
    prodselected = previous;
    for (x in products)
    {
      if(products[x][0] == previous) {
         selectcolour(products[x][3]);
         popsizes(products[x][3],size);
         popcolours(size,products[x][3]);
      }
    }
  }
}
function selectsize() {
  var size = document.frm_form.size.value;
  var colour = document.frm_form.colour.value;
  previous = prodselected;
  prodselected = updateproductselection(colour,size);
  if (prodselected != false) {
    //update colour selections presetting this one
    popcolours(size,colour);
    popsizes(colour,size);
  } else {
    //update colour options and alert that the size selected was not available

    alert("The size you have selected is not available in that colour.");
    //set size back to previous...
    prodselected = previous;
    for (x in products)
    {
      if(products[x][0] == previous) {
         document.frm_form.size.value = products[x][2];
         popsizes(colour,products[x][2]);
         popcolours(products[x][2],colour);
      }
    }
  }
}
function popsizes(selectedcolour, presetsize) {

  var counter = 0;
  document.frm_form.size.options.length = 0;
for (x in products)
{
  if(products[x][2] != "") {
    if (products[x][2] == presetsize && products[x][3] == selectedcolour) { //current size and colour
       document.frm_form.size.options[counter]=new Option(products[x][2] + " - � " + products[x][1], products[x][2], true, true);
       counter = counter + 1;
    }
    else if (products[x][3] == selectedcolour) { //current colour different size
      document.frm_form.size.options[counter]=new Option(products[x][2] + " - � " + products[x][1], products[x][2], false, false);
      counter = counter + 1;
    }
    else {
      //not something we want to include
    }
  }
}

}

function popcolours (selectedsize, presetcolour) {
	if (presetcolour != null) {document.frm_form.colour.value = presetcolour};
	var swatchstring = "";
	for (x in products)
	{
		if(products[x][3] != "") {
			if (products[x][2] == selectedsize && products[x][3] == presetcolour) { //current size and colour
				document.getElementById('itemImage').src = products[x][4];
				document.getElementById('stockCode').innerHTML = products[x][0];
				swatchstring = swatchstring + "<div class='swatchbox' ><a onclick='selectcolour(\"" + products[x][3] + "\")' class='swatch' title='" + products[x][3] + "' style='background-color:" + products[x][5] + ";background: url(\"" + products[x][4] + "\") no-repeat; color:#000000;border:solid 2px #000000;width:56px;height:56px;line-height:9px;'>" + products[x][3] + "</a></div>";
			} else if (products[x][2] == selectedsize) {
				swatchstring = swatchstring + "<div class='swatchbox' ><a onclick='selectcolour(\"" + products[x][3] + "\")' class='swatch' title='" + products[x][3] + "' style='background-color:" + products[x][5] + ";background: url(\"" + products[x][4] + "\") no-repeat; color:#000000;border:solid 2px #000000;width:56px;height:56px;line-height:9px;'>" + products[x][3] + "</a></div>";
			}
		}
	}
	document.getElementById('coloursbox').innerHTML = swatchstring;
}

function itempageloaded () {
	var matched = false;
	var summary = "";
	if(prodselected.toLowerCase() == "itemreview") {
		prodselected = products[0][0].toLowerCase();
		itempageloaded();
	} else {
		for (y in products) {
			if(products[y][0].toLowerCase() == prodselected.toLowerCase()) {
				size = products[y][2];
				colour = products[y][3];

				popsizes(colour,size);
				popcolours(size,colour);

				document.getElementById('itemImage').src = products[y][4];
				if(products[y][4] == "/images/no_image.gif") document.getElementById('itemImage').style.display = 'none';
				else document.getElementById('itemImage').style.display = '';
				document.getElementById('stockCode').innerHTML = products[y][0];
				document.frm_form.product.value = products[y][0];

				price = products[y][1].replace('.', '');
				price = parseInt(products[y][1].replace(',', ''));
				oldprice = products[y][8].replace('.', '');
				oldprice = parseInt(products[y][8].replace(',', ''));

				if(price < oldprice) document.getElementById('itemPrice').innerHTML = "<span style='color:red; text-decoration:line-through;font-size:small'> �" + products[y][8] +"</span> &pound; " + products[y][1];
				else document.getElementById('itemPrice').innerHTML = "� " + products[y][1];

				if (size != "" || colour != "") summary = colour + " " + size + " - � " + products[y][1];
				else summary = "&pound; " + products[y][1];
				document.getElementById('itemSum').innerHTML = summary;
				//document.getElementById('itemColour').innerHTML = colour;
				matched = true;
			}
		}
		if (matched == false) {
			alert("The product is not current available in the colour and size you selected.");
			prodselected = products[0][0].toLowerCase();
			itempageloaded();
		}
	}
}
function popup(mylink, askpopup)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
if(askpopup == 'ask') window.open(href, askpopup, 'width=615,height=680,scrollbars=yes');
else if(askpopup == 'video') window.open(href, askpopup, 'width=420,height=320,scrollbars=yes');
 
return false;
}


function linkchange(strstockcode) {
  var x = eval("document.frm_form.type_" + strstockcode + "");
  var x_selected = x.selectedIndex;
  var x_text = x.options[x_selected].text;
  strstockcode2 = x.value.toLowerCase();

  var baselink = document.getElementById('titlelink_'+strstockcode).href; //get current link
  if(baselink.lastIndexOf("#") > 0) baselink = baselink.substring(0,baselink.lastIndexOf("#")); //strip off any pagination
  var newlink = baselink+"#"+strstockcode2; //add new pagination

   document.getElementById('titlelink_'+strstockcode).href = newlink;
   document.getElementById('imagelink_'+strstockcode).href = newlink;

}
function showpopup(colour, event) {
	document.getElementById('popup').style.display = 'block';
	document.getElementById('popuplink').href = popups[colour][0];
	document.getElementById('popupimg').src = popups[colour][1];
	document.getElementById('popupimg').alt = popups[colour][2];
	document.getElementById('popuplink').href = popups[colour][0];
    var el = document.getElementById(colour);
    var _x = 0;
    var _y = 0;
    if( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x = el.offsetLeft +20;
        _y = el.offsetTop + 20;
    }
	document.getElementById('popup').style.left = _x + 'px';
	document.getElementById('popup').style.top = _y + 'px';
}
function hidepopup() {
	document.getElementById('popup').style.display = 'none';
}
/*********** Calcs *************************/
function capitaliseFirstLetter(string)
{
	return string.charAt(0).toUpperCase() + string.slice(1);
}
function numval(value) {
	if (value == null || value == '' || !isNumber(value)) value = 0;
	value = parseFloat(value);
	return value;
}

function textval(value) {
	if(value == 0 || value == null || !isNumber(value)) value = '';
	return value;
}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
/*********** Roof Calcs ********************/

function roof_select(type) {
	document.getElementById("rooftype").value = type;
	document.getElementById("type-label").innerHTML = capitaliseFirstLetter(type);
	document.getElementById("select-view").style.display = 'none';
	document.getElementById("input-view").style.display = 'block';
	document.getElementById("input-img").src = "images/"+type+"_select.png";
	switch (type) {
		case "cross":
			document.getElementById("width2").style.display = 'block';
			document.getElementById("length2").style.display = 'block';
			document.getElementById("length1").style.left = "20px";
			document.getElementById("length1").style.bottom = "110px";
			document.getElementById("height").style.left = "250px";
			document.getElementById("width1").style.left = "50px";
			break;
		default:
			document.getElementById("width2").style.display = 'none';
			document.getElementById("length2").style.display = 'none';
			document.getElementById("height").style.left = "100px";
			document.getElementById("length1").style.left = "200px";
			document.getElementById("length1").style.bottom = "50px";
			document.getElementById("width1").style.left = "50px";
			break;
	}
}
function roof_clearpage() {
	document.getElementById("rooftype").value = '';
	document.getElementById("type-label").innerHTML = '';
	document.getElementById("select-view").style.display = 'block';
	document.getElementById("input-view").style.display = 'none';
	document.getElementById("height").value = '';
	document.getElementById("width1").value = '';
	document.getElementById("width2").value = '';
	document.getElementById("length1").value = '';
	document.getElementById("length2").value = '';
	document.getElementById("total").value = '';
	document.getElementById("resultTable").innerHTML= '';
}
function roof_calculate() {
	var type =  document.getElementById("rooftype").value;
	var height = numval(document.getElementById("height").value);
	var width1 = numval(document.getElementById("width1").value);
	var width2 = numval(document.getElementById("width2").value);
	var length1 = numval(document.getElementById("length1").value);
	var length2 = numval(document.getElementById("length2").value);
	var product = document.frm_form.product.value;
	var total = 0;

	switch (type) {
		case "gable":
			if (width1 <= 0 || length1 <= 0 || height <= 0) total = -1;
			else if (height <= width1/2) total = 0;
			else total = 2*height*length1;
			break;
		case "hip":
			if (width1 <= 0 || length1 <= 0 || height <= 0) total = -1;
			else if (height > width1/2) total = 2*Math.sqrt(Math.pow(height,2)-Math.pow(width1/2,2))*length1;
			else total = 0;
			break;
		case "pyramid":
			if (width1 <= 0 || length1 <= 0 || height <= 0) total = -1;
			else if(height > width1/2 && height > length1/2) total = length1*Math.sqrt(Math.pow(height,2)-Math.pow(length1/2,2))+width1*Math.sqrt(Math.pow(height,2)-Math.pow(width1/2,2));
			else total = 0;
			break;
		case "cross":
			if (width1 <= 0 || length1 <= 0 || width2 <= 0 || length2 <= 0 || height <= 0) total = -1;
			else if (height > width2/2) total = 2*width2*height+2*Math.sqrt(Math.pow(Math.sqrt(Math.pow(height,2)-Math.pow(length2/2,2)),2)+Math.pow(width1/2,2))*(length1+length2);
			else total = 0;
			break;
		default:
			total = -1;
			break;	
	}
	document.getElementById("height").value = textval(height);
	document.getElementById("width1").value = textval(width1);
	document.getElementById("width2").value = textval(width2);
	document.getElementById("length1").value = textval(length1);
	document.getElementById("length2").value = textval(length2);
	if (total == -1) {
		document.getElementById("total").value = "";
		document.getElementById("resultTable").innerHTML = "";
	} else if(total > 0) {
		document.getElementById("total").value = textval(total);
		if(prevtotal != total || prevprod != product) {
			getRoofingStockData(product, total);
			prevtotal = total;
			prevprod = product;
		}
	} else {
		document.getElementById("total").value = "";
		alert("Please Check your Measurements.");
		document.getElementById("resultTable").innerHTML = "";
	}

}


	function roofSetEvents () {
		var inputs = document.getElementsByTagName("input");
		for (i=0; i<inputs.length; i++){
			inputs[i].onchange = roof_calculate;
		}
	}
	function getRoofingStockData(product,total) {

		var request = "/app-data/covcalc.php?id="+product+"&area="+total;
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}

		if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

		xmlhttp.open("GET",request,true);
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var xml = xmlhttp.responseXML;
				var html = "<table style='width:100%;padding:20px 0px;'><tr><th>Product</th><th>Color</th><th>Size</th><th>Code</th><th>Qty</th><th>Price</th><th></th></tr>";

				var productName = xml.getElementsByTagName("productName")[0].childNodes[0].nodeValue;
				var productColour = xml.getElementsByTagName("productColour")[0].childNodes[0].nodeValue;
				var sizes = xml.getElementsByTagName("product");
				for (i = 0; i<sizes.length; i= i+1) {
					var sizeCode = xml.getElementsByTagName("sizeCode")[i].childNodes[0].nodeValue;
					var sizeDescription = xml.getElementsByTagName("sizeDescription")[i].childNodes[0].nodeValue;
					var sizePrice = xml.getElementsByTagName("sizePrice")[i].childNodes[0].nodeValue;
					var sizeQty = xml.getElementsByTagName("sizeQty")[i].childNodes[0].nodeValue;
					html = html+"\n<tr><td>"+productName+"</td><td>"+productColour+"</td><td>"+sizeDescription+"</td><td>"+sizeCode+"</td><td>"+sizeQty+"</td><td>&pound;"+sizePrice+"</td><td><img style='height:34px;width:160px;float:right;cursor: pointer;' onclick='jsajaxadd(\""+sizeCode+"\","+sizeQty+")' src='/img/addtobasket.jpg' alt='Add to Basket' class='basketbutton' /></td></tr>";
				}
				var cost = xml.getElementsByTagName("totalCost")[0].childNodes[0].nodeValue;
				html = html+"<tr><th colspan='5'>Total</th><th>&pound; "+cost+"</th></tr></table>";
				document.getElementById("resultTable").innerHTML=html;
			}
		}
		xmlhttp.send();
	}
	
/**** Tanking Calcs ****/
	function tanking_check()
	{
		totallength = 0;
		wallarea = 0;
		floorarea = 0;
		totalarea = 0;

		for (var i=0; i<walls.length; i++)
		{
			var length = numval(document.getElementById('walllength'+i).value);
			var height = numval(document.getElementById('wallheight'+i).value);

			var area = length*height;
			document.getElementById('wallarea'+i).value = textval(area);
			document.getElementById('walllength'+i).value = textval(length);
			document.getElementById('wallheight'+i).value = textval(height);

			totallength = totallength+length;
			wallarea = wallarea+area;
			totalarea = totalarea+area;

			walls[i][0] = textval(length);
			walls[i][1] = textval(height);
		}

		for (var i=0; i<floors.length; i++)
		{
			var length = numval(document.getElementById('floorlength'+i).value);
			var width = numval(document.getElementById('floorwidth'+i).value);

			var area = length*width;
			document.getElementById('floorarea'+i).value = textval(area);
			document.getElementById('floorlength'+i).value = textval(length);
			document.getElementById('floorwidth'+i).value = textval(width);

			floorarea = floorarea+area;
			totalarea = totalarea+area;

			floors[i][0] = textval(length);
			floors[i][1] = textval(width);
		}

		document.calcform.totalLength.value = textval(totallength);
		document.calcform.wallArea.value = textval(wallarea);
		document.calcform.floorArea.value = textval(floorarea);
		document.calcform.totalArea.value = textval(totalarea);
		if(totallength != 0 && wallarea != 0 && floorarea != 0 && totalarea != 0) {
			getTankingStockData(wallarea,totallength,totalarea);
		}
	}
	function tanking_addMeasurement(type) {
		var tableElement = document.getElementById(type+'table');
		if(type == 'wall') {
			var content = '';
			var headrow = "<table>\n<tr><th> </th><th>Length</th><th>Height</th><th>Area</th></tr>";
			var currentcount = walls.length;
			walls[currentcount] = new Array ('','','');
			for (var i=0; i<walls.length; i++)
			{
				content = content+"\n<tr><th>Wall "+(i+1)+":</th><td><input type='text' id='walllength"+i+"' value='"+walls[i][0]+"'/></td><td><input type='text' id='wallheight"+i+"' value='"+walls[i][1]+"' /></td><td><input type='text' id='wallarea"+i+"' disabled='disabled'  /></td></tr>";
			}
			var tailrow = "<tr><th>Total:</th><td><input type='text' id='totalLength' disabled='disabled' /></td><td></td><td><input type='text' id='wallArea' disabled='disabled'/></td></tr>";
			tailrow = tailrow+"\n<tr><td colspan='4' style='text-align:center'><input type='button' value='Add Wall' onclick='addMeasurement(\"wall\")' /></td></tr>\n</table>";

		}
		else {
			var content = '';
			var headrow = "<table>\n<tr><th> </th><th>Length</th><th>Width</th><th>Area</th></tr>";
			var currentcount = floors.length;
			floors[currentcount] = new Array ('','','');
			for (var i=0; i<floors.length; i++)
			{
				content = content+"\n<tr><th>Floor "+(i+1)+":</th><td><input type='text' id='floorlength"+i+"' value='"+floors[i][0]+"'/></td><td><input type='text' id='floorwidth"+i+"' value='"+floors[i][1]+"' /></td><td><input type='text' id='floorarea"+i+"' disabled='disabled' /></td></tr>";
			}
			var tailrow = "<tr><th>Total:</th><td></td><td></td><td><input type='text' name='floorArea' disabled='disabled'/></td></tr>";
			tailrow = tailrow+"\n<tr><td colspan='4' style='text-align:center'><input type='button' value='Add Floor' onclick='addMeasurement(\"floor\")' /></td></tr>\n</table>";
		}
		tableElement.innerHTML = headrow+content+tailrow;
		tanking_check();
		tankSetEvents();
	}


	function tankSetEvents () {
		var inputs = document.getElementsByTagName("input");
		for (i=0; i<inputs.length; i++){
			inputs[i].onchange = tanking_check;
		}
	}
	function getTankingStockData(wall,length,area) {
				document.getElementById("resultTable").innerHTML="Ajax Fired";

		var request = "/app-data/tankcalc.php";
		var data = "wallarea="+wall+"&total="+area+"&length="+length;
		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}

		if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

		xmlhttp.open("POST",request,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange=function() {
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				products = new Array();
				quantities = new Array();
				var xml = xmlhttp.responseXML;
				var html = "<table style='width:100%;padding:20px 0px;'><tr><th>Product</th><th>Size</th><th>Code</th><th>Qty</th><th>Price</th><th></th></tr>";

				var sizes = xml.getElementsByTagName("product");
				for (i = 0; i<sizes.length; i= i+1) {
					var productName = xml.getElementsByTagName("productName")[i].childNodes[0].nodeValue;
					var sizeCode = xml.getElementsByTagName("sizeCode")[i].childNodes[0].nodeValue;
					var sizeDescription = xml.getElementsByTagName("sizeDescription")[i].childNodes[0].nodeValue;
					var sizePrice = xml.getElementsByTagName("sizePrice")[i].childNodes[0].nodeValue;
					var sizeQty = xml.getElementsByTagName("sizeQty")[i].childNodes[0].nodeValue;
					html = html+"\n<tr><td>"+productName+"</td><td>"+sizeDescription+"</td><td>"+sizeCode+"</td><td>"+sizeQty+"</td><td>&pound;"+sizePrice+"</td><td><img style='height:34px;width:160px;float:right;cursor: pointer;' onclick='jsajaxadd(\""+sizeCode+"\","+sizeQty+")' src='/img/addtobasket.jpg' alt='Add to Basket' class='basketbutton' /></td></tr>";
					products[products.length] = sizeCode;
					quantities[quantities.length] = sizeQty;
				}
				var cost = xml.getElementsByTagName("totalCost")[0].childNodes[0].nodeValue;
				html = html+"<tr><th colspan='5'>Total</th><th>&pound; "+cost+"</th></tr></table><br/><br/>Buy All : <img style='height:34px;width:160px;float:right;cursor: pointer;' onclick='jsajaxbulkadd(products,quantities)' src='/img/addtobasket.jpg' alt='Add to Basket' class='basketbutton' />";
				document.getElementById("resultTable").innerHTML=html;
			} else if (xmlhttp.readyState==4) {
				document.getElementById("resultTable").innerHTML="Ajax Returned Code:"+xmlhttp.status+"<br/><br/>Response:<br/>"+xmlhttp.responseText;
				
			}
		}
		xmlhttp.send(data);
	}
	
