$(document).ready(function() {
	$('ul#menu a.expand-collapse').click(function(event) {
		accordion(this);
		event.preventDefault();
	});
});

//following functions are used to display a popup lightbox window. - uses jquery
function loadPopup(){
	$("#popupBackground").css({ "opacity": "0.7" });
	$("#popupBackground").fadeIn("slow");
	$("#popup").fadeIn("slow");
}
function disablePopup(returnLocation){
	$("#popupBackground").fadeOut("slow", function() { $(this).remove(); });
	$("#popup").fadeOut("slow", function() { $(this).remove(); });
	document.getElementById(returnLocation).innerHTML = "";
}
function centerPopup(){
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popup").height();
	var popupWidth = $("#popup").width();
	if(windowHeight > popupHeight) {
		$("#popup").css({ "position": "absolute", "top": windowHeight/2-popupHeight/2, "left": windowWidth/2-popupWidth/2 });
	} else {
		$("#popup").css({ "position": "absolute", "top": 20, "left": windowWidth/2-popupWidth/2 });
	}
	$("#popupBackground").css({ "height": windowHeight });
}
function popupRequest(page, strq) {
	var request = "/includes/"+page+".php"+strq;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("GET",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response = xmlhttp.responseText;
			$(document.body).append(response);
			$("#popupClose").click(function(){disablePopup("");});
			$("#popupBackground").click(function(){disablePopup("");});
			$(document).keypress(function(e){ if(e.keyCode==27 && cookieStatus<2){ disablePopup(""); } });
			centerPopup();
			loadPopup();
		}
	}
	xmlhttp.send();
}

//Ajax call so the page does not need to be refreshed. This is a generic call, it takes the pagename, a query string, destination to put the code that is returned, wait time (1 for 1 second etc) and a true or false if need to wait for the response e.g. if it is processing an SQL statement.
function jsajaxcall(ajaxpage, querystring, destination, wait, torf)
{
	querystring += "&ie="+new Date();

	if (destination != "")
	{
		if (wait == 1) { document.getElementById(destination).innerHTML = "Please wait....."; }
	}

	var request = ajaxpage+".php"+querystring;

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)
	{
		return "Your browser doesn't seem to support XMLHttpRequests.";
	}
	else
	{
		if (torf == 1)
		{
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					if (destination != "")
					{
						document.getElementById(destination).innerHTML = xmlhttp.responseText;
					}
				}
			}
			xmlhttp.open("GET",request,true);
			xmlhttp.send();
		}
		else
		{
			xmlhttp.open("GET",request,false);
			xmlhttp.send();
			if (destination != "")
			{
				document.getElementById(destination).innerHTML = xmlhttp.responseText;
				document.getElementById(destination).value = xmlhttp.responseText;
			}
		}
	}
}

//used to validate
function jsajaxcallValidate(ajaxpage, querystring, destination, wait, torf)
{
	querystring += "&ie="+new Date();

	if (destination != "")
	{
		if (wait == 1) { document.getElementById(destination).innerHTML = "Please wait..."; }
	}

	var request = ajaxpage+".php"+querystring;

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)
	{
		return "Your browser doesn't seem to support XMLHttpRequests.";
	}
	else
	{
		if (torf == 1)
		{
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					if (destination != "")
					{
						document.getElementById(destination).innerHTML = xmlhttp.responseText;
					}
				}
			}
			xmlhttp.open("GET",request,true);
			xmlhttp.send();
		}
		else
		{
			xmlhttp.open("GET",request,false);
			xmlhttp.send();
			if (destination != "")
			{
				document.getElementById(destination).innerHTML = xmlhttp.responseText;
				document.getElementById(destination).value = xmlhttp.responseText;
			}
		}
		return xmlhttp.responseText;
	}
}

//code to lookup and search for a players address - uses ajax.
function jsfindaddress(postcode,returnLocation){
	document.getElementById(returnLocation).style.display = "block";
	var request = "/includes/ajax_postcode.php";
	var vars = "postcode="+postcode;
	var xmlhttp;
	var resultsList;
	var error = "";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			var result = jsGetAddressByTag("Result",xml);
			if (result == "") {
				result = jsGetAddressByTag("result",xml);
			}
			if(result != '-2' && result != "") {

				var addlist = "<h3>Post Code lookup - Click an address to add</h3><ul >";			
				var x=xml.getElementsByTagName("Item");
				for (i=0;i<x.length;i++)
				{
					var strkey = x[i].getElementsByTagName("Key")[0].childNodes[0].nodeValue;
					var strlist = x[i].getElementsByTagName("List")[0].childNodes[0].nodeValue;
					strkey = strkey.replace(/(^,)|(,$)/g, "");
					strkey = strkey.replace(/(^ )|(,$)/g, "");
					strlist = strlist.replace(postcode.toUpperCase(),"");
					strlist = strlist.replace(/(^,)|(,$)/g, "");
					strlist = strlist.replace(/(^ )|(,$)/g, "");
					addlist += "<li><a href='javascript:jsaddsel(\""+strkey+"\",\""+returnLocation+"\")'>"+strlist+"</a></li>";
				}
				addlist += "</ul>";
				resultsList = addlist;
				
				var response = xmlhttp.responseText;
				$(document.body).append("<div id='popup'>"+resultsList+"</div><div id='popupBackground' ></div>");
				$("#popupClose").click(function(){disablePopup(returnLocation);});
				$("#popupBackground").click(function(){disablePopup(returnLocation);});
				$(document).keypress(function(e){ if(e.keyCode==27){ disablePopup(returnLocation); } });
				centerPopup();
				loadPopup();

			} else {
				document.getElementById(returnLocation).innerHTML = xml.getElementsByTagName("ErrorText")[0].childNodes[0].nodeValue;
				document.getElementById(returnLocation).className = "notification-warning";
			}
			
			
			
		} else if (xmlhttp.readyState==4) {
			document.getElementById(returnLocation).innerHTML = "There has been an error finding your address please try again.";
			document.getElementById(returnLocation).className = "notification-error";
		}
		
		if (error != ""){
			var response = xmlhttp.responseText;
			$(document.body).append("<div id='popup'>"+error+"</div><div id='popupBackground' ></div>");
			$("#popupClose").click(function(){disablePopup(returnLocation);});
			$("#popupBackground").click(function(){disablePopup(returnLocation);});
			$(document).keypress(function(e){ if(e.keyCode==27){ disablePopup(returnLocation); } });
			centerPopup();
			loadPopup();
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
	document.getElementById(returnLocation).innerHTML = " <img src='/css/images/ajax-loader.gif' />";
	//document.getElementById("ajaxPostcodeResults").innerHTML = "";
}

function jsGetAddressByTag(element1,xmldoc) {
	var result = "";

	if(xmldoc.getElementsByTagName(element1)[0] != null){
		if (xmldoc.getElementsByTagName(element1)[0].childNodes.length) {
			result = xmldoc.getElementsByTagName(element1)[0].childNodes[0].nodeValue;
		}
	}
	return result;
}

//submits form when a user clicks on an address that is found from the postcode lookup
function jsaddsel(address,returnLocation){
	
	document.getElementById(returnLocation).style.display = "block";
	var request = "/includes/ajax_postcode.php";
	var vars = "lstResults="+address;
	var xmlhttp;
	var error = "";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xmldoc = xmlhttp.responseXML;
			var result = jsGetAddressByTag("Result",xmldoc);
			if (result == "") {
				result = jsGetAddressByTag("result",xmldoc);
			}
			if(result == 1) {

				//document.getElementById("ajax_address").innerHTML = xmlhttp.responseText;
				addressArray = new Array();
				addressArray["name"] = jsGetAddressByTag("Name",xmldoc);
				addressArray["organisation"] = jsGetAddressByTag("Organisation",xmldoc);
				addressArray["property"] = jsGetAddressByTag("Property",xmldoc);
				addressArray["street"] = jsGetAddressByTag("Street",xmldoc);
				addressArray["locality"] = jsGetAddressByTag("Locality",xmldoc);
				addressArray["town"] = jsGetAddressByTag("Town",xmldoc);
				addressArray["postalcounty"] = jsGetAddressByTag("PostalCounty",xmldoc);
				addressArray["abbreviatedpostalcounty"] = jsGetAddressByTag("AbbreviatedPostalCounty",xmldoc);
				addressArray["optionalcounty"] = jsGetAddressByTag("OptionalCounty",xmldoc);
				addressArray["abbreviatedoptionalcounty"] = jsGetAddressByTag("AbbreviatedoptionalCounty",xmldoc);
				addressArray["traditionalcounty"] = jsGetAddressByTag("TraditionalCounty",xmldoc);
				addressArray["administrativecounty"] = jsGetAddressByTag("AdministrativeCounty",xmldoc);
				addressArray["frm_memberContactpostcode"] = jsGetAddressByTag("Postcode",xmldoc);
				addressArray["dps"] = jsGetAddressByTag("DPS",xmldoc);
				addressArray["postcodefrom"] = jsGetAddressByTag("PostcodeFrom",xmldoc);
				addressArray["postcodetype"] = jsGetAddressByTag("PostcodeType",xmldoc);
				addressArray["mailsortcode"] = jsGetAddressByTag("MailsortCode",xmldoc);
				addressArray["phone"] = jsGetAddressByTag("Phone",xmldoc);
				addressArray["gride"] = jsGetAddressByTag("gridE",xmldoc);
				addressArray["gridn"] = jsGetAddressByTag("gridN",xmldoc);
				addressArray["miles"] = jsGetAddressByTag("Miles",xmldoc);
				addressArray["km"] = jsGetAddressByTag("Km",xmldoc);
				addressArray["latitude"] = jsGetAddressByTag("Latitude",xmldoc);
				addressArray["longitude"] = jsGetAddressByTag("Longitude",xmldoc);
				addressArray["justbuilt"] = jsGetAddressByTag("JustBuilt",xmldoc);
				addressArray["urbanruralcode"] = jsGetAddressByTag("UrbanRuralCode",xmldoc);
				addressArray["urbanruralname"] = jsGetAddressByTag("UrbanRuralName",xmldoc);
				addressArray["wardcode"] = jsGetAddressByTag("WardCode",xmldoc);
				addressArray["wardname"] = jsGetAddressByTag("Wardname",xmldoc);
				addressArray["constituencycode"] = jsGetAddressByTag("ConstituencyCode",xmldoc);
				addressArray["constituency"] = jsGetAddressByTag("Constituency",xmldoc);
				addressArray["eercode"] = jsGetAddressByTag("EERCode",xmldoc);
				addressArray["eername"] = jsGetAddressByTag("EERName",xmldoc);
				addressArray["authoritycode"] = jsGetAddressByTag("AuthorityCode",xmldoc);
				addressArray["authority"] = jsGetAddressByTag("Authority",xmldoc);
				addressArray["leacode"] = jsGetAddressByTag("LEACode",xmldoc);
				addressArray["leaname"] = jsGetAddressByTag("LEAName",xmldoc);
				addressArray["tvregion"] = jsGetAddressByTag("TVRegion",xmldoc);
				addressArray["occupancy"] = jsGetAddressByTag("Occupancy",xmldoc);
				addressArray["occupancydescription"] = jsGetAddressByTag("OccupancyDescription",xmldoc);
				addressArray["addresstype"] = jsGetAddressByTag("AddressType",xmldoc);
				addressArray["addresstypedescription"] = jsGetAddressByTag("AddressTypeDescription",xmldoc);
				addressArray["udprn"] = jsGetAddressByTag("UDPRN",xmldoc);
				addressArray["nhscode"] = jsGetAddressByTag("NHSCode",xmldoc);
				addressArray["nhsname"] = jsGetAddressByTag("NHSName",xmldoc);
				addressArray["nhsregioncode"] = jsGetAddressByTag("NHSRegionCode",xmldoc);
				addressArray["nhsregionname"] = jsGetAddressByTag("NHSRegionName",xmldoc);
				addressArray["pctcode"] = jsGetAddressByTag("PCTCode",xmldoc);
				addressArray["pctname"] = jsGetAddressByTag("PCTName",xmldoc);
				addressArray["subcountryname"] = jsGetAddressByTag("SubCountryName",xmldoc);
				addressArray["devolvedconstituencycode"] = jsGetAddressByTag("DevolvedConstituencyCode",xmldoc);
				addressArray["devolvedconstituencyname"] = jsGetAddressByTag("DevolvedConstituencyName",xmldoc);
				addressArray["censationcode"] = jsGetAddressByTag("CensationCode",xmldoc);
				addressArray["censationlabel"] = jsGetAddressByTag("CensationLabel",xmldoc);
				addressArray["affluence"] = jsGetAddressByTag("Affluence",xmldoc);
				addressArray["lifestage"] = jsGetAddressByTag("Lifestage",xmldoc);
				addressArray["additionalcensusinfo"] = jsGetAddressByTag("AdditionalCensusInfo",xmldoc);
				addressArray["soalower"] = jsGetAddressByTag("SOALower",xmldoc);
				addressArray["soamiddle"] = jsGetAddressByTag("SOAMiddle",xmldoc);
				addressArray["residency"] = jsGetAddressByTag("Residency",xmldoc);
				addressArray["householdcomposition"] = jsGetAddressByTag("HouseholdComposition",xmldoc);
				addressArray["business"] = jsGetAddressByTag("Business",xmldoc);
				addressArray["size"] = jsGetAddressByTag("Size",xmldoc);
				addressArray["siccode"] = jsGetAddressByTag("SICCode",xmldoc);
				addressArray["locationtype"] = jsGetAddressByTag("LocationType",xmldoc);
				addressArray["branchcount"] = jsGetAddressByTag("BranchCount",xmldoc);
				addressArray["groupid"] = jsGetAddressByTag("GroupID",xmldoc);
				addressArray["modelledturnover"] = jsGetAddressByTag("ModelledTurnover",xmldoc);
				addressArray["nationalsize"] = jsGetAddressByTag("NationalSize",xmldoc);
				addressArray["oneditedroll"] = jsGetAddressByTag("OnEditedRoll",xmldoc);
				addressArray["gender"] = jsGetAddressByTag("Gender",xmldoc);
				addressArray["forename"] = jsGetAddressByTag("Forename",xmldoc);
				addressArray["middleinitial"] = jsGetAddressByTag("MiddleInitial",xmldoc);
				addressArray["surname"] = jsGetAddressByTag("Surname",xmldoc);
				addressArray["dateofbirth"] = jsGetAddressByTag("DateOfBirth",xmldoc);
				addressArray["counciltaxband"] = jsGetAddressByTag("CouncilTaxBand",xmldoc);
				addressArray["product"] = jsGetAddressByTag("Product",xmldoc);
				addressArray["key"] = jsGetAddressByTag("Key",xmldoc);
				addressArray["list"] = jsGetAddressByTag("List",xmldoc);
				addressArray["country"] = jsGetAddressByTag("Country",xmldoc);
				addressArray["countryiso"] = jsGetAddressByTag("CountryISO",xmldoc);
				
				switch(returnLocation){
					case "ajax_addressDel":
						addressArray["deladd1"] = addressArray["property"];
						addressArray["deladd2"] = addressArray["street"];
						addressArray["deladd3"] = addressArray["locality"];
						addressArray["delTown"] = addressArray["town"];
						addressArray["delCnty"] = addressArray["postalcounty"];
						addressArray["delCntry"] = addressArray["country"];
					break;
					case "ajax_addressBill":
						addressArray["biladd1"] = addressArray["property"];
						addressArray["biladd2"] = addressArray["street"];
						addressArray["biladd3"] = addressArray["locality"];
						addressArray["bilTown"] = addressArray["town"];
						addressArray["bilCnty"] = addressArray["postalcounty"];
						addressArray["bilCntry"] = addressArray["country"];
					break;
				}

				var returninfo = "";
				for (var key in addressArray) {
					//alert("key " + key+ " has value "+ addressArray[key]);

					var element = document.getElementById(key);
					if (element != null) {
						element.value = addressArray[key];
					} else if (addressArray[key] != "") { //return a hidden field with this value because the field doesnt already exist on the page
						returninfo += "<input type=\"hidden\" name=\"addressResult["+key+"]\" value=\"" + addressArray[key] + "\">";
					}
				}
				document.getElementById(returnLocation).innerHTML = returninfo;
				document.getElementById(returnLocation).className = "";
				disablePopup(returnLocation);

			} else {
				error = "<div class='notification-error' ><h3>Error</h3>"+jsGetAddressByTag("ErrorText",xmldoc)+"</div>";
			}
		} else if (xmlhttp.readyState==4) {
			error = "<div class='notification-error' ><h3>Error</h3>There has been an error finding your address please try again.</div>";
		} 
		
		if (error != ""){
			var response = xmlhttp.responseText;
			$(document.body).append("<div id='popup'>"+error+"</div><div id='popupBackground' ></div>");
			$("#popupClose").click(function(){disablePopup(returnLocation);});
			$("#popupBackground").click(function(){disablePopup(returnLocation);});
			$(document).keypress(function(e){ if(e.keyCode==27){ disablePopup(returnLocation); } });
			centerPopup();
			loadPopup();
		}
		
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
	//document.getElementById(returnLocation).innerHTML = " <img src='/css/images/ajax-loader.gif' /> <span> Search...</span> ";
	document.getElementById(returnLocation).innerHTML = "";
	
}

//Masked the input phone number to display in "123-123-1234" format
function maskInput(input, textbox) {

    //Get the delimiter positons
	var location = '2,5';
    var locs = location.split(',');
	
	delimiter = '-';

    //Iterate until all the delimiters are placed in the textbox
    for (var delimCount = 0; delimCount <= locs.length; delimCount++) {
        for (var inputCharCount = 0; inputCharCount <= input.length; inputCharCount++) {

            //Check for the actual position of the delimiter
            if (inputCharCount == locs[delimCount]) {

                //Confirm that the delimiter is not already present in that position
                if (input.substring(inputCharCount, inputCharCount + 1) != delimiter) {
                    input = input.substring(0, inputCharCount) + delimiter + input.substring(inputCharCount, input.length);
                }
            }
        }
    }
    textbox.value = input;
}