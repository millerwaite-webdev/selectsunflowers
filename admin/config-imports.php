<?php
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "config-imports"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	//the classes that are needed to read excel files
	require_once 'lib/PHPExcel.php';
	require_once 'lib/PHPExcel/IOFactory.php';
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	function fnTestFormat($importConfigID,$conn){
	
		$strdbsql = "SELECT * FROM configFileImportFormats WHERE importID = :importID";
		$arrdbvalues = array("importID" => $importConfigID);
		$strType = "single";
		$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		print ("<div class='section'>");
		print ("<fieldset class='inlineLabels'><legend>Test Format of '".$resultdata['description']."'</legend>");					
			
			//upload file and process 
			$strdbsql = "SELECT * FROM fileImport WHERE status != 3";
			$arrdbparams = array();
			$strType = "multi";
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			print ("<div style='clear:both;'><label for='frm' class='col-sm-2 control-label'>File Import: </label><div class='col-sm-10'>");
			print ("<select name='frm_importID' id='frm_importID' class='form-control' style='width:300px;'>");
			print ("<option value=''>-- Please Select --</option>");
			foreach ($resultdata as $row) {
				print ("<option value='".$row['importID']."'>".$row['reference']." ".fnconvertunixtime($row['timestamp'])."</option>");
			}
			print ("</select></div></div>");
										
			print ("<div style='clear:both;'>");
				print ("<div class='col-sm-offset-2 col-sm-3'>");
					print ("<button onclick='fnLoadTestFile(); return false;' class='btn btn-success '>Load File</button>");
					print ("<button class='btn btn-default' style='margin-left:10px;' onclick='fnCancelFormat(); return false;'>Return</button>");
				print ("</div>");
			print ("</div>");
			
			
			if ($_REQUEST['frm_importID'] != "") {
				
				//file information
				$strdbsql = "SELECT fileImport.*, fileImportTypes.description AS importDescription, admin_operator.username FROM fileImport 
				INNER JOIN admin_operator ON fileImport.operator = admin_operator.operatorID
				INNER JOIN fileImportTypes ON fileImport.importType = fileImportTypes.recordID
				WHERE fileImport.importID = :importID";
		
				$arrdbparams = array("importID"=>$_REQUEST['frm_importID']);
				$strType = "single";
				$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
				
				//file imformation
				print ("<div class='col-sm-4'>");
				print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
				print ("<thead><tr>");
					print ("<th colspan='2'>File Details</th>");
				print ("</tr></thead><tbody>");
					print ("<tr>");
						print ("<td>Import Reference:</td>");
						print ("<td>".$resultdata['reference']."</td>");
					print ("</tr>");
					
					print ("<tr>");
						print ("<td>File Path:</td>");
						print ("<td><a href='".$resultdata['filePath']."' >".$resultdata['filePath']."</a></td>");
					print ("</tr>");
					
					print ("<tr>");
						print ("<td>Import Type:</td>");
						print ("<td>".$resultdata['importDescription']."</td>");
					print ("</tr>");
					
				print ("</tbody>");
				print ("</table>");
				print ("</div>");
				
				//import format information
				$strdbsql = "SELECT configFileImportFormats.*, configFileImportMatchTypes.description AS accountMatchTypeName FROM configFileImportFormats 
				INNER JOIN configFileImportMatchTypes ON configFileImportFormats.accountMatchType = configFileImportMatchTypes.recordID
				WHERE configFileImportFormats.importID = :importID";
				$arrdbparams = array("importID"=>$importConfigID);
				$strType = "single";
				$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

				//file imformation
				print ("<div class='col-sm-4'>");
				print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
				print ("<thead><tr>");
					print ("<th colspan='2'>Format Details</th>");
				print ("</tr></thead><tbody>");
					print ("<tr>");
						print ("<td>Format Name:</td>");
						print ("<td>".$resultdata['formatName']."</td>");
					print ("</tr>");
					print ("<tr>");
						print ("<td>File Format:</td>");
						print ("<td>".$resultdata['fileFormat']."</td>");
					print ("</tr>");
					print ("<tr>");
						print ("<td>Account Match Type:</td>");
						print ("<td>".$resultdata['accountMatchTypeName']."</td>");
					print ("</tr>");

				print ("</tbody>");
				print ("</table>");
				print ("</div>");
				
				//colour key
				print ("<div class='col-sm-3'>");
				print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
				print ("<thead><tr>");
					print ("<th style='width:40px;'>&nbsp;</th>");
					print ("<th>Description</th>");
				print ("</tr></thead><tbody>");
					print ("<tr>");
						print ("<td class='danger'>&nbsp;</td>");
						print ("<td>Rows that are ignored</td>");
					print ("</tr>");
					print ("<tr>");
						print ("<td class='success'>&nbsp;</td>");
						print ("<td>Payment Amount</td>");
					print ("</tr>");
					print ("<tr>");
						print ("<td class='info'>&nbsp;</td>");
						print ("<td>Payment Date</td>");
					print ("</tr>");
					print ("<tr>");
						print ("<td class='active'>&nbsp;</td>");
						print ("<td>Payment Code</td>");
					print ("</tr>");
					print ("<tr>");
						print ("<td class='warning'>&nbsp;</td>");
						print ("<td>Deceased Flag</td>");
					print ("</tr>");
					print ("<tr>");
						print ("<td class='danger'>&nbsp;</td>");
						print ("<td>Cancelled Flag</td>");
					print ("</tr>");
				print ("</tbody>");
				print ("</table>");
				print ("</div>");
				
				$strdbsql = "SELECT * FROM fileImportData WHERE importID = :importID";
				$arrdbparams = array("importID"=>$_REQUEST['frm_importID']);
				$strType = "multi";
				$datarows = query($conn, $strdbsql, $strType, $arrdbparams);
				
				//setup table
				print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
				print ("<thead><tr>");
				$columnNames = explode(",", $resultdata['columnNames']);
				for ($i = 0; $i < $resultdata['noColumns']; $i++) {
					print ("<th>".$columnNames[$i]."</th>");
				}
				print ("</tr></thead><tbody>");
				
				//get highest row of data and then loop around all displaying each data row
				$noofdatarows = count($datarows);
				for ($j = 0; $j < $noofdatarows; $j++) {
											
					print ("<tr>");
					$rowData = unserialize($datarows[$j]['dataArray']);
					for ($i = 0; $i < $resultdata['noColumns']; $i++) {
						
						//ignore from top
						if ($resultdata['noIgnoreTop'] > $j) {
							$tablecolour = "class='danger'";
						//ignore from bottom
						} else if (($noofdatarows - $resultdata['noIgnoreBottom']) <= $j) {
							$tablecolour = "class='danger'";
						//payment amount
						} else if ($resultdata['columnPayment'] == $i) {
							$tablecolour = "class='success'";
						//payment date
						} else if ($resultdata['columnDate'] == $i) {
							$tablecolour = "class='info'";
						//payment code
						} else if ($resultdata['columnPaymentCode'] == $i) {
							$tablecolour = "class='active'";
						//column deceased
						} else if ($resultdata['columnDeceased'] == $i) {
							$tablecolour = "class='warning'";
						//column cancelled
						} else if ($resultdata['columnCancelled'] == $i) {
							$tablecolour = "class='danger'";
						} else {
							$tablecolour = "class=''";
						}
						print ("<td $tablecolour>".$rowData[$i]."</td>");
					}
					print ("</tr>");
				}
				
				print ("</tbody>");
				print ("</table>");
			}
			
		print ("</fieldset>");	
		print ("</div>");
	}
	function fnModifyColumns($importWorksheetRecordID,$conn,$connmaster){
		
		$strdbsql = "SELECT configFileImportWorksheets.*, configFileImportFormats.importType FROM configFileImportWorksheets INNER JOIN configFileImportFormats ON configFileImportWorksheets.importID = configFileImportFormats.importID WHERE recordID = :recordID";
		$arrdbvalues = array("recordID"=>$importWorksheetRecordID);
		$strType = "single";
		$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);
		$columnNames = explode(",", $resultdata['columnNames']);
		
		print ("<div class='section'>");
			print ("<fieldset class='inlineLabels'><legend>Configure Columns</legend>");

				if ($resultdata['importType'] == 3) { //sales order import
					
					$columnDataMatches = unserialize($resultdata['columnDataMatches']);
					$columnsToMatch['frm_orderID'] = "Order ID";
					$columnsToMatch['frm_timestamp'] = "Date/Time";
					$columnsToMatch['frm_firstname'] = "First Name";
					$columnsToMatch['frm_surname'] = "Last Name";
					$columnsToMatch['frm_voucherCode'] = "Voucher Code";
					$columnsToMatch['frm_price'] = "Total Price";
					$columnsToMatch['frm_email'] = "eMail Address";
					$columnsToMatch['frm_telephone'] = "Telephone";
					$columnsToMatch['frm_paymenttype'] = "Payment Type";
					$columnsToMatch['frm_orderstatus'] = "Order Status";
					$columnsToMatch['frm_deliveryprice'] = "Delivery Price";
					$columnsToMatch['frm_tax'] = "VAT/Tax";
					$columnsToMatch['frm_deliverName'] = "Delivery Name";
					$columnsToMatch['frm_deliverPrice'] = "Delivery Price";
					$columnsToMatch['frm_instructions'] = "Further Instructions";
					$columnsToMatch['frm_message'] = "Message";
					$columnsToMatch['frm_deliveryDate'] = "Delivery Date";
					$columnsToMatch['frm_billName'] = "Billing Company Name";
					$columnsToMatch['frm_billAdd1'] = "Billing Address 1";
					$columnsToMatch['frm_billAdd2'] = "Billing Address 2";
					$columnsToMatch['frm_billTown'] = "Billing Town";
					$columnsToMatch['frm_billCounty'] = "Billing County";
					$columnsToMatch['frm_billCountry'] = "Billing Country";
					$columnsToMatch['frm_billPostcode'] = "Billing Postcode";
					$columnsToMatch['frm_delName'] = "Delivery Company Name";
					$columnsToMatch['frm_delAdd1'] = "Delivery Address 1";
					$columnsToMatch['frm_delAdd2'] = "Delivery Address 2";
					$columnsToMatch['frm_delTown'] = "Delivery Town";
					$columnsToMatch['frm_delCounty'] = "Delivery County";
					$columnsToMatch['frm_delCountry'] = "Delivery Country";
					$columnsToMatch['frm_delPostcode'] = "Delivery Postcode";
					$columnsToMatch['frm_itemSKU'] = "Item SKU";
					$columnsToMatch['frm_itemName'] = "Item Name";
					$columnsToMatch['frm_itemOption'] = "Item Option";
					$columnsToMatch['frm_itemQty'] = "Item Quantity";
					$columnsToMatch['frm_itemPrice'] = "Item Price";
					$columnsToMatch['frm_promocode'] = "Promotion Code";
					$columnsToMatch['frm_paymentCheckColumn'] = "Payment Check Value";

					foreach ($columnsToMatch AS $key => $value) {
						print ("<div style='clear:both;'>");
							print ("<label for='$key' class='col-sm-2 control-label' >$value Column: </label><div class='col-sm-4'>");
							print ("<select name='columnDataMatch[$key][option]' id='columnDataMatch[$key][option]' class='form-control' style='width:300px;'>");
							print ("<option value=''>Select Column</option>");
							
							$concatCount = count(unserialize($resultdata['columnConcatenation']));
							for ($i = 0; $i < $concatCount; $i++) {
								if ($columnDataMatches[$key]['option'] == "concat$i") $selected = "selected"; else $selected = "";
								print ("<option $selected value='concat$i'>Concatenated Column ".($i+1)."</option>");
							}

							$columnExplodeArray = unserialize($resultdata['columnExplode']);
							for ($i = 0; $i < count($columnExplodeArray); $i++) {
								for($j = 0; $j < $columnExplodeArray[$i]['noofexplodecolumns']; $j++){
									if ($columnDataMatches[$key]['option'] == "explode".$i."-".$j) $selected = "selected"; else $selected = "";
									print ("<option $selected value='explode".$i."-".$j."'>Explode - ".$columnExplodeArray[$i]['name']." value ".$j."</option>");
								}
							}
							
							for ($i = 1; $i <= $resultdata['noColumns']; $i++) {
								if ($columnDataMatches[$key]['option'] == $i) $selected = "selected"; else $selected = "";
								print ("<option $selected value='".$i."'>".$i." - ".$columnNames[$i-1]."</option>");
							}
							print ("</select>");
							//if payment amount select the payment format
							$paymentFormats = array("pounds","pence");
							if ($key == "frm_cCPaymentAmount") {
								print ("<select name='columnDataMatch[paymentAmountFormatCC][value]' id='columnDataMatch[paymentAmountFormatCC][value]' class='form-control' style='width:300px;'>");
								foreach ($paymentFormats AS $paymentFormat){
									if ($columnDataMatches['paymentAmountFormatCC']['value'] == $paymentFormat) $selected = "selected"; else $selected = "";
									print ("<option $selected value='".$paymentFormat."'>".$paymentFormat."</option>");
								}
								print ("</select>");
							}
							if ($key == "frm_cCRPaymentAmount") {
								print ("<select name='columnDataMatch[paymentAmountFormatCCR][value]' id='columnDataMatch[paymentAmountFormatCCR][value]' class='form-control' style='width:300px;'>");
								foreach ($paymentFormats AS $paymentFormat){
									if ($columnDataMatches['paymentAmountFormatCCR']['value'] == $paymentFormat) $selected = "selected"; else $selected = "";
									print ("<option $selected value='".$paymentFormat."'>".$paymentFormat."</option>");
								}
								print ("</select>");
							}
							if ($key == "frm_paymentCheckColumn") {
								print ("<input type='text' value='".$columnDataMatches['paymentCheckAcceptValue']['value']."' class='form-control' style='width:300px;' id='columnDataMatch[paymentCheckAcceptValue][value]' name='columnDataMatch[paymentCheckAcceptValue][value]'>");
							}
							
							print ("</div>");
							
			
							print ("<label class='col-sm-1 control-label' for='columnDataMatch[$key][value]' >Or Value: </label>");
							print ("<div class='col-sm-3'><input type='text' value='".$columnDataMatches[$key]['value']."' class='form-control' maxlength='50' size='35' id='columnDataMatch[$key][value]' name='columnDataMatch[$key][value]'></div>");
							
							if(isset($columnDataMatches[$key]['required'])) {if ($columnDataMatches[$key]['required'] == 1) $checked = "checked"; else $checked = "";} else {$checked = "";}
							print ("<div class='col-sm-1'><div class='checkbox'><label><input $checked id='columnDataMatch[$key][required]' tabindex='18' name='columnDataMatch[$key][required]' type='checkbox' value='1'>Required</label></div></div>");
							

						print ("</div>");
					}

				}

				//columnNames
				$fileDelimiterParams = explode(",", $resultdata['fileDelimiter']);
				for ($i = 1; $i <= $resultdata['noColumns']; $i++) {
					print ("<div style='clear:both;'>");						
						print ("<label class='col-sm-2 control-label' for='frm_column-$i-Name'>Column $i Description:</label>");
						print ("<div class='col-sm-3'><input type='text' value='".$columnNames[$i-1]."' class='form-control' maxlength='50' size='35' id='frm_column-$i-Name' name='frm_column-$i-Name'></div>");
						
						//if space delimited then allow the position to be set
						if ($resultdata['fileDelimiterType'] == 1) {
							print ("<label class='col-sm-2 control-label' for='frm_column-$i-Name'>Column Width:</label>");
							print ("<div class='col-sm-1'><input type='text' value='".$fileDelimiterParams[$i-1]."' class='form-control' maxlength='50' size='35' id='frm_column-$i-delimiter' name='frm_column-$i-delimiter'></div>");
						}
					print ("</div>");
				}
			print ("</fieldset><br/>");	




			// ------------------------------------ Start of Account Join --------------------------
				//columnAccountMatch
				print ("<fieldset class='inlineLabels'><legend>Account Match Configuration</legend>");	

					//$accountLookupArray = unserialize(rawurldecode($resultdata['columnAccountMatch']));
					$accountLookupArray = unserialize($resultdata['columnAccountMatch']);
					print ("<input type='hidden' class='nodisable' name='noofjoins' id='noofjoins' value='".count($accountLookupArray)."'/>");

					for ($i = 0; $i < count($accountLookupArray); $i++) {
					
						//column to match
						print ("<div id='accountMatchSection$i'>");
							
							print ("<div>");
								print ("<label for='frm_columnMatch$i' class='col-sm-2 control-label'>Account Match Column: </label><div class='col-sm-10'>");
								print ("<select name='frm_columnMatch[][column]' id='frm_columnMatch$i' class='form-control' style='width:300px;'>");
								print ("<option value=''>Select Column</option>");
								for ($j = 1; $j <= $resultdata['noColumns']; $j++) {
									if ($accountLookupArray[$i]['column'] == $j) $selected = "selected"; else $selected = "";
									print ("<option $selected value='".$j."'>".$j." - ".$columnNames[$j-1]."</option>");
								}
								print ("</select></div>");
							print ("</div>");
							
							//append characters
							print ("<div>");
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchAppend$i'>Append Characters:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['append']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchAppend$i' name='frm_columnMatch[$i][append]'></div>");
							
							//remove characters
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchRemove$i'>Remove Characters:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['remove']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchRemove$i' name='frm_columnMatch[$i][remove]'></div>");
							print ("</div>");
							
							//prefix characters
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchPrefix$i'>Prefix Characters:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['prefix']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchPrefix$i' name='frm_columnMatch[$i][prefix]'></div>");
							
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchExplodeChar$i'>Explode Character:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['explodeChar']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchExplodeChar$i' name='frm_columnMatch[$i][explodeChar]'></div>");
							print ("</div>");
							
							//amount of characters
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchTotalLength$i'>Total Length (Not Required):</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['totalLength']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchTotalLength$i' name='frm_columnMatch[$i][totalLength]'></div>");
							print ("</div>");
							
							//pad characters
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchPadChar$i'>Pad Characters:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['padChar']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchPadChar$i' name='frm_columnMatch[$i][padChar]'></div>");
							
							//pad direction
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchPadDirection$i'>Pad Side:</label>");
							print ("<div class='col-sm-2'><select name='frm_columnMatch[$i][padDirection]' id='frm_columnMatchPadDirection$i' class='form-control' style='width:300px;'>");
							print ("<option value=''>Select Column</option>");
							$padDirections = array("STR_PAD_LEFT","STR_PAD_RIGHT","STR_PAD_BOTH");
							foreach ($padDirections AS $padDirection) {
								if ($accountLookupArray[$i]['padDirection'] == $padDirection) $selected = "selected"; else $selected = "";
								print ("<option $selected value='".$padDirection."'>".$padDirection."</option>");
							}
							print ("</select></div>");
							print ("</div>");
							
							//trim charaters - left
							print ("<div>");
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchLTrim$i'>Amount to Trim from Left:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['ltrim']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchLTrim$i' name='frm_columnMatch[$i][ltrim]'></div>");
							
							//trim charaters - right
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchRTrim$i'>Amount to Trim from Right:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['rtrim']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchRTrim$i' name='frm_columnMatch[$i][rtrim]'></div>");
							print ("</div>");
							
							//trim charaters - left
							print ("<div>");
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchLTrimChar$i'>Trim Left from Character:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['ltrimChar']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchLTrimChar$i' name='frm_columnMatch[$i][ltrimChar]'></div>");
							
							//trim charaters - right
							print ("<label class='col-sm-2 control-label' for='frm_columnMatchRTrimChar$i'>Trim Right from Character:</label>");
							print ("<div class='col-sm-2'><input type='text' value='".$accountLookupArray[$i]['rtrimChar']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_columnMatchLTrimChar$i' name='frm_columnMatch[$i][rtrimChar]'></div>");
							print ("</div>");
						print ("</div>");
					}

					print ("<div>");
						print ("<div class='col-sm-offset-2 col-sm-5'>");
							if (count($accountLookupArray) == 1) $visable = "display: none;"; else $visable = "";
							print ("<button type='button' class='btn btn-primary' id='addNewJoin'>Add Column Join</button>");
							print ("<button type='button' class='btn btn-danger' style='margin-left:10px; $visable' id='deleteJoin'>Remove Last Join</button>");
						print ("</div>");
					print ("</div>");
				print ("</fieldset>");	
			// ------------------------------------ End of Account Join ----------------------------



			if ($resultdata['importType'] == 2 || $resultdata['importType'] == 3) {
			// ------------------------------------ Start of Concatenate Columns --------------------------
				//column Concatenate
				$columnConcatenateMainArray = unserialize($resultdata['columnConcatenation']);
				if (count($columnConcatenateMainArray) == 0) {$columnConcatenateMainArray = array(); $columnConcatenateMainArray[0][0]['column'] = ""; $columnConcatenateMainArray[0][0]['append'] = "";}
				print ("<input type='hidden' class='nodisable' name='noofconcats' id='noofconcats' value='".count($columnConcatenateMainArray)."'/>");
				$k = 0;
				foreach ($columnConcatenateMainArray AS $columnConcatenateArray) {

					print ("<div id='concatBlock$k' name='concatBlock$k'><fieldset class='inlineLabels'><legend>Column Concatenation ".($k+1)."</legend>");
						print ("<input type='hidden' class='nodisable' name='noofcolumns$k' id='noofcolumns$k' value='".count($columnConcatenateArray)."'/>");
					
						for ($i = 0; $i < count($columnConcatenateArray); $i++) {
					
							//column to match
							print ("<div id='concatMatchSection$k-$i' name='concatMatchSection$k' class='concatMatchSection$k'>");
								
								print ("<div style='clear:both;'>");
									print ("<label for='frm_concatMatch$k-$i' class='col-sm-2 control-label'>Concatenate Column: </label><div class='col-sm-10'>");
									print ("<select name='frm_concatMatch[$k][$i][column]' id='frm_concatMatch$k-$i' class='form-control' style='width:300px;'>");
									print ("<option value=''>Select Column</option>");
									for ($j = 1; $j <= $resultdata['noColumns']; $j++) {
										if ($columnConcatenateArray[$i]['column'] == $j) $selected = "selected"; else $selected = "";
										print ("<option $selected value='".$j."'>".$j." - ".$columnNames[$j-1]."</option>");
									}
									print ("</select></div>");
								print ("</div>");								
								
								//append characters
								print ("<div style='clear:both;'>");
								print ("<label class='col-sm-2 control-label' for='frm_concatMatchAppend$k-$i'>Append Characters: </label>");
								print ("<div class='col-sm-2'><input type='text' value='".$columnConcatenateArray[$i]['append']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_concatMatchAppend$k-$i' name='frm_concatMatch[$k][$i][append]'></div>");
								print ("</div>");
								
							print ("</div>");
						}
					
						print ("<div style='clear:both;'>");
							print ("<div class='col-sm-offset-2 col-sm-5'>");
								if (count($columnConcatenateArray) == 1) $visable = "display: none;"; else $visable = "";
								print ("<button type='button' onclick='addNewConcatColumn($k); return false;' class='btn btn-primary' id='addNewColumn$k' name='addNewColumn$k'>Add Column</button>");
								print ("<button type='button' onclick='deleteConcatColumn($k); return false;' class='btn btn-danger' style='margin-left:10px; $visable' id='deleteColumn$k' name='deleteColumn$k'>Remove Last Column</button>");
							print ("</div>");
						print ("</div>");
					print ("</fieldset></div>");
					$k++;
				}
				print ("<fieldset class='inlineLabels'><legend></legend>");
					print ("<div style='clear:both;'>");
						print ("<div class='col-sm-offset-2 col-sm-5'>");
							if (count($columnConcatenateMainArray) == 1) $visable = "display: none;"; else $visable = "";
							print ("<button type='button' class='btn btn-primary' id='addNewConcatenate'>Add New Concatenate</button>");
							print ("<button type='button' class='btn btn-danger' style='margin-left:10px; $visable' id='deleteConcatenate'>Remove Last Concatenate</button>");
						print ("</div>");
					print ("</div>");
				print ("</fieldset>");

					
			// ------------------------------------ End of Concatenate Columns ----------------------------
			}
			
			// ------------------------------------ Start of Explode Columns --------------------------
				
				print ("<fieldset class='inlineLabels'><legend>Explode Columns</legend>");	

					$columnExplodeArray = unserialize($resultdata['columnExplode']);
					print ("<input type='hidden' class='nodisable' name='noofexplodes' id='noofexplodes' value='".count($columnExplodeArray)."'/>");
					
					for ($i = 0; $i < count($columnExplodeArray); $i++) {
					
						
						print ("<div id='explodeMatchSection$i'>");
							
							//column to match
							print ("<div style='clear:both;'>");
								print ("<label for='frm_explodeMatch$i' class='col-sm-2 control-label'>Explode Column: </label><div class='col-sm-10'>");
								print ("<select name='frm_explodeMatch[][column]' id='frm_explodeMatch$i' class='form-control' style='width:300px;'>");
								print ("<option value=''>Select Column</option>");
								for ($j = 1; $j <= $resultdata['noColumns']; $j++) {
									if ($columnExplodeArray[$i]['column'] == $j) $selected = "selected"; else $selected = "";
									print ("<option $selected value='".$j."'>".$j." - ".$columnNames[$j-1]."</option>");
								}
								print ("</select></div>");
							print ("</div>");								
							
							//name of explode
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_explodeName$i'>Explode Name: </label>");
							print ("<div class='col-sm-2'><input type='text' value='".$columnExplodeArray[$i]['name']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_explodeName$i' name='frm_explodeMatch[$i][name]'></div>");
							print ("</div>");
							
							//explode character
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_explodeChar$i'>Explode Character: </label>");
							print ("<div class='col-sm-2'><input type='text' value='".$columnExplodeArray[$i]['explodeChar']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_explodeChar$i' name='frm_explodeMatch[$i][explodeChar]'></div>");
							print ("</div>");
							
							//num of explodes
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_explodeNum$i'>Num of Explode Values: </label>");
							print ("<div class='col-sm-2'><input type='text' value='".$columnExplodeArray[$i]['noofexplodecolumns']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_explodeNum$i' name='frm_explodeMatch[$i][noofexplodecolumns]'></div>");
							print ("</div>");
							
						print ("</div>");
					}

					print ("<div style='clear:both;'>");
						print ("<div class='col-sm-offset-2 col-sm-5'>");
							if (count($columnExplodeArray) == 1) $visable = "display: none;"; else $visable = "";
							print ("<button type='button' class='btn btn-primary' id='addNewExplode'>Add Column Explode</button>");
							print ("<button type='button' class='btn btn-danger' style='margin-left:10px; $visable' id='deleteExplode'>Remove Last Explode</button>");
						print ("</div>");
					print ("</div>");
				print ("</fieldset>");	
			// ------------------------------------ End of Explode Columns ----------------------------
			
			// ------------------------------------ Start of Convert Columns --------------------------
				
				print ("<fieldset class='inlineLabels'><legend>Convert Columns</legend>");	

					$columnConversionArray = unserialize($resultdata['columnConversion']);
					print ("<input type='hidden' class='nodisable' name='noofconversions' id='noofconversions' value='".count($columnConversionArray)."'/>");

					for ($i = 0; $i < count($columnConversionArray); $i++) {

						print ("<div id='conversionMatchSection$i'>");
							
							//column to match
							print ("<div style='clear:both;'>");
								print ("<label for='frm_conversionMatch$i' class='col-sm-2 control-label'>Conversion Column: </label><div class='col-sm-10'>");
								print ("<select name='frm_conversionMatch[][column]' id='frm_conversionMatch$i' class='form-control' style='width:300px;'>");
								print ("<option value=''>Select Column</option>");
								
								$columnExplodeArray = unserialize($resultdata['columnExplode']);
								for ($k = 0; $k < count($columnExplodeArray); $k++) {
									for($l = 0; $l < $columnExplodeArray[$k]['noofexplodecolumns']; $l++){
										if ($columnConversionArray[$i]['column'] == "explode".$k."-".$l) $selected = "selected"; else $selected = "";
										print ("<option $selected value='explode".$k."-".$l."'>Explode - ".$columnExplodeArray[$k]['name']." value ".$l."</option>");
									}
								}
								
								for ($j = 1; $j <= $resultdata['noColumns']; $j++) {
									if ($columnConversionArray[$i]['column'] == $j) $selected = "selected"; else $selected = "";
									print ("<option $selected value='".$j."'>".$j." - ".$columnNames[$j-1]."</option>");
								}
								
								
								
								
								print ("</select></div>");
							print ("</div>");								
							
							//search string
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_conversionSearchString$i'>Search String: </label>");
							print ("<div class='col-sm-8'><input type='text' value='".$columnConversionArray[$i]['searchString']."' class='form-control' id='frm_conversionSearchString$i' name='frm_conversionMatch[$i][searchString]'></div>");
							print ("</div>");
							
							//replace if not found
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_conversionReplaceNotFound$i'>Replace (if not found): </label>");
							print ("<div class='col-sm-2'><select name='frm_conversionMatch[$i][notfound]' id='frm_conversionReplaceNotFound$i' class='form-control' style='width:300px;'>");
							if ($columnConversionArray[$i]['notfound'] == 0) $selected = "selected"; else $selected = "";
							print ("<option $selected value='0'>No</option>");
							if ($columnConversionArray[$i]['notfound'] == 1) $selected = "selected"; else $selected = "";
							print ("<option $selected value='1'>Yes</option>");
							print ("</select></div>");
							print ("</div>");
							
							//replace if not found value
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_conversionReplaceNotFoundValue$i'>Replace (if not found) value: </label>");
							print ("<div class='col-sm-2'><input type='text' value='".$columnConversionArray[$i]['notfoundvalue']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_conversionReplaceNotFoundValue$i' name='frm_conversionMatch[$i][notfoundvalue]'></div>");
							print ("</div>");
							
							//replace if found
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_conversionReplaceFound$i'>Replace (if found): </label>");
							print ("<div class='col-sm-2'><select name='frm_conversionMatch[$i][found]' id='frm_conversionReplaceFound$i' class='form-control' style='width:300px;'>");
							if ($columnConversionArray[$i]['found'] == 0) $selected = "selected"; else $selected = "";
							print ("<option $selected value='0'>No</option>");
							if ($columnConversionArray[$i]['found'] == 1) $selected = "selected"; else $selected = "";
							print ("<option $selected value='1'>Yes</option>");
							print ("</select></div>");
							print ("</div>");
							
							//replace if found value
							print ("<div style='clear:both;'>");
							print ("<label class='col-sm-2 control-label' for='frm_conversionReplaceFoundValue$i'>Replace (if found) value: </label>");
							print ("<div class='col-sm-2'><input type='text' value='".$columnConversionArray[$i]['foundvalue']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_conversionReplaceFoundValue$i' name='frm_conversionMatch[$i][foundvalue]'></div>");
							print ("</div>");

						print ("</div>");
					}

					print ("<div style='clear:both;'>");
						print ("<div class='col-sm-offset-2 col-sm-5'>");
							if (count($columnConversionArray) == 1) $visable = "display: none;"; else $visable = "";
							print ("<button type='button' class='btn btn-primary' id='addNewConversion'>Add Column Conversion</button>");
							print ("<button type='button' class='btn btn-danger' style='margin-left:10px; $visable' id='deleteConversion'>Remove Last Conversion</button>");
						print ("</div>");
					print ("</div>");
				print ("</fieldset>");	
			// ------------------------------------ End of Convert Columns ----------------------------
			
			// ------------------------------------ Start of Unique Reference Column --------------------------
				print ("<fieldset class='inlineLabels'><legend>Unique Import Reference</legend>");	
					print ("<div style='clear:both;'>");
						print ("<label for='frm_columnUniqueRef' class='col-sm-2 control-label'>Unique Import Reference: </label><div class='col-sm-10'>");
						print ("<select name='frm_columnUniqueRef' id='frm_columnUniqueRef' class='form-control' style='width:300px;'>");
						print ("<option value=''>Select Column</option>");
						for ($j = 1; $j <= $resultdata['noColumns']; $j++) {
							if ($resultdata['columnUniqueRef'] == $j) $selected = "selected"; else $selected = "";
							print ("<option $selected value='".$j."'>".$j." - ".$columnNames[$j-1]."</option>");
						}
						print ("</select></div>");
					print ("</div>");
				print ("</fieldset>");	
				
			// ------------------------------------ End of Unique Reference Column ----------------------------

		print ("</div>");

		print ("<button class='btn btn-success pull-right' onclick='fnSaveColumns(0); return false;'>Save</button>");
		print ("<button class='btn btn-success pull-right' style='margin-right:10px;' onclick='fnSaveColumns(1); return false;'>Save and Reload</button>");
		print ("<button class='btn btn-danger pull-left' onclick='fnCancelColumnEdit(); return false;'>Cancel</button>");

	}
	function fnModifyWorksheet($worksheet,$importConfigID,$conn){
	
		$strdbsql = "SELECT * FROM configFileImportWorksheets WHERE importID = :importID AND importIndex = :worksheetIndex";
		$arrdbvalues = array("importID" => $importConfigID, "worksheetIndex"=>$worksheet);
		$strType = "count";
		$resultcount = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		if ($resultcount == 0) {
			$strdbsql = "INSERT INTO configFileImportWorksheets (importID,importIndex) VALUES (:importID,:worksheetIndex)";
			$arrdbvalues = array("importID" => $importConfigID, "worksheetIndex"=>$worksheet);
			$strType = "insert";
			$resultinsert = query($conn, $strdbsql, $strType, $arrdbvalues);
		}
		
		$strdbsql = "SELECT configFileImportWorksheets.*,configFileImportFormats.filePaymentType, configFileImportFormats.fileFormat, configFileImportFormats.importType FROM configFileImportWorksheets INNER JOIN configFileImportFormats ON configFileImportWorksheets.importID = configFileImportFormats.importID WHERE configFileImportWorksheets.importID = :importID AND configFileImportWorksheets.importIndex = :worksheetIndex";
		$arrdbvalues = array("importID" => $importConfigID, "worksheetIndex"=>$worksheet);
		$strType = "single";
		$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		print ("<div class='section'>");
		print ("<fieldset class='inlineLabels'><legend>Modify Import Worksheet</legend>");

			//account match type
			/*print ("<div style='clear:both;'>");
				$strdbsql = "SELECT * FROM configFileImportMatchTypes ORDER BY description ASC";
				$strType = "multi";
				$resultdata2 = query($conn, $strdbsql, $strType);

				print ("<label for='frm_matchType' class='col-sm-3 control-label'>Account Match Type: </label><div class='col-sm-9'>");
				print ("<select name='frm_matchType' id='frm_matchType' class='form-control' style='width:300px;'>");
				foreach ($resultdata2 as $row) {
					if ($resultdata['accountMatchType'] == $row['recordID']) $selected = "selected"; else $selected = "";
					print ("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
				}
				print ("</select></div>");
			print ("</div>");*/
			
			//direct debit account type
			/*print ("<div style='clear:both;'>");
				$strdbsql = "SELECT * FROM configDirectDebitFileTypes WHERE exportOrImport = 1 ORDER BY description ASC";
				$strType = "multi";
				$resultdata2 = query($conn, $strdbsql, $strType);

				print ("<label for='frm_ddFileType' class='col-sm-3 control-label'>Direct Debit File Type: </label><div class='col-sm-9'>");
				print ("<select name='frm_ddFileType' id='frm_ddFileType' class='form-control' style='width:300px;'>");
				print ("<option value=''>Not Required</option>");
				foreach ($resultdata2 as $row) {
					if ($resultdata['directDebitFileTypeID'] == $row['recordID']) $selected = "selected"; else $selected = "";
					print ("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
				}
				print ("</select></div>");
			print ("</div>");*/
			
			//select member import type
			/*print ("<div style='clear:both;'>");
				$strdbsql = "SELECT * FROM configFileImportMembersType";
				$strType = "multi";
				$resultdata2 = query($conn, $strdbsql, $strType);

				print ("<label for='frm_memberimporttype' class='col-sm-3 control-label'>Member Import Type: </label><div class='col-sm-9'>");
				print ("<select name='frm_memberimporttype' id='frm_memberimporttype' class='form-control' style='width:300px;'>");
				print ("<option value=''>Not Required</option>");
				foreach ($resultdata2 as $row) {
					if ($resultdata['memberImportFileTypeID'] == $row['recordID']) $selected = "selected"; else $selected = "";
					print ("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
				}
				print ("</select></div>");
			print ("</div>");*/

			//ignore rows
			print ("<div style='clear:both;'>");
				print ("<label class='col-sm-3 control-label' for='frm_IgnoreRowsTop'>Ignore rows from top:</label>");
				print ("<div class='col-sm-1'><input type='text' value='".$resultdata['noIgnoreTop']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_IgnoreRowsTop' name='frm_IgnoreRowsTop'></div>");
			print ("</div>");
			print ("<div style='clear:both;'>");						
				print ("<label class='col-sm-3 control-label' for='frm_IgnoreRowsBottom'>Ignore rows from bottom:</label>");
				print ("<div class='col-sm-1'><input type='text' value='".$resultdata['noIgnoreBottom']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_IgnoreRowsBottom' name='frm_IgnoreRowsBottom'></div>");
			print ("</div>");
			
			//if payment type is payroll then we need the option to create new members
			if ($resultdata['filePaymentType'] == 3) {
				print ("<div style='clear:both;'>");
					print ("<label class='col-sm-3 control-label' for='frm_createNewMembers'>New Member:</label>");
					if ($resultdata['createNewMembers'] == 1) { $strchecked = "checked"; } else { $strchecked = ""; }
					print ("<div class='col-sm-3'><div class='checkbox'><label><input id='frm_createNewMembers' tabindex='16' name='frm_createNewMembers' type='checkbox' value='1' $strchecked>If this import is to create a new membership, please tick here</label></div></div>");
				print ("</div>");
			}
			

			
			
			//select negative payment action
			if ($resultdata['importType'] == 1) {
				
				$negPaymentActions = array();
				$negPaymentActions[0] = "Not Required";
				$negPaymentActions[1] = "Ignore Negatives";
				$negPaymentActions[2] = "Convert Negatives to Positive";
				$negPaymentActions[3] = "Flag Negatives to Operator";
								
				print ("<div style='clear:both;'>");
					print ("<label for='frm_worksheetNegPaymentAction' class='col-sm-3 control-label'>Negative Payment Action: </label>");
					print ("<div class='col-sm-7'><select name='frm_worksheetNegPaymentAction' id='frm_worksheetNegPaymentAction' class='form-control' style='width:300px; float:left;'>");
					foreach ($negPaymentActions as $key=>$value) {
						if ($resultdata['worksheetNegPaymentAction'] == $key) $selected = "selected"; else $selected = "";
						print ("<option $selected value='".$key."'>".$value."</option>");
					}
					print ("</select>".fnInfoTooltip("Currently only setup for payroll imports")."</div>");
				print ("</div>");
			}
			
			

			//delimiter type
			if ($resultdata['fileFormat'] == ".csv" || $resultdata['fileFormat'] == ".txt") {
				print ("<div style='clear:both;'>");
					$strdbsql = "SELECT * FROM configFileImportDelimiters ORDER BY description ASC";
					$strType = "multi";
					$resultdata2 = query($conn, $strdbsql, $strType);

					print ("<label for='frm_delimiterType' class='col-sm-3 control-label'>Delimiter Type: </label><div class='col-sm-9'>");
					print ("<select name='frm_delimiterType' onChange='fnDelimiterTypeChange(); return false;' id='frm_delimiterType' class='form-control' style='width:300px;'>");
					foreach ($resultdata2 as $row) {
						if ($resultdata['fileDelimiterType'] == $row['recordID']) $selected = "selected"; else $selected = "";
						print ("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
					}
					print ("</select></div>");
				print ("</div>");
				
				//delimiter
				if ($resultdata['fileDelimiterType'] == 1) $hidden = "display:none;"; else $hidden = "";
				print ("<div style='clear:both;' style='$hidden' id='delimiterContainer'>");
					print ("<label class='col-sm-3 control-label' for='frm_delimiter'>Delimiter:</label>");
					print ("<div class='col-sm-9'><input type='text' value='".$resultdata['fileDelimiter']."' class='form-control' maxlength='50' size='35' style='width:130px;' id='frm_delimiter' name='frm_delimiter' /></div>");
				print ("</div>");
			}
			
			//noColumns
			print ("<div style='clear:both;'>");
				print ("<label class='col-sm-3 control-label' for='frm_noColumns'>Number of Columns:</label>");
				print ("<div class='col-sm-1'><input type='text' value='".$resultdata['noColumns']."' class='form-control' maxlength='50' size='35' id='frm_noColumns' name='frm_noColumns'></div>");
				print ("<div class='col-sm-3'><button class='btn btn-primary' style='margin-left:10px;' onclick='fnModifyColumns(".$resultdata['recordID']."); return false;'>Modify Columns</button></div>");
			print ("</div>");
			
		print ("</fieldset>");
		print ("</div>");
		
		print ("<button class='btn btn-success pull-right' style='display:inline-block;' onclick='fnSaveWorksheet(".$resultdata['recordID']."); return false;'>Save</button>");
		print ("<button class='btn btn-danger pull-left' style='display:inline-block;' onclick='fnCancelWorksheetEdit(); return false;'>Cancel</button>");

	}
	function fnModifyFormat($importConfigID,$conn){
		
		if ($importConfigID != 0) {
			$strdbsql = "SELECT * FROM configFileImportFormats WHERE importID = :importID";
			$arrdbvalues = array("importID" => $importConfigID);
			$strType = "single";
			$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);
		} else {
			$resultdata['description'] = "";
			$resultdata['importType'] = "";
			$resultdata['fileFormat'] = "";
			$resultdata['filePaymentType'] = "";
			$resultdata['worksheets'] = 1;
			$resultdata['worksheetsToImport'] = "";
		}
		
		print ("<div class='section' style='overflow:auto;'>");
					
			//format name
			print ("<div style='clear:both;'>");
				print ("<label class='col-sm-2 control-label' for='frm_formatName'>Format Name:</label>");
				print ("<div class='col-sm-3'><input type='text' value='".$resultdata['description']."' class='form-control' maxlength='50' size='35' style='width:200px;' id='frm_formatName' name='frm_formatName'></div>");
			print ("</div>");
			
			//import type
			print ("<div style='clear:both;'>");
				$strdbsql = "SELECT * FROM configFileImportTypes";
				$strType = "multi";
				$resultdata2 = query($conn, $strdbsql, $strType);

				print ("<label for='frm_importType' class='col-sm-2 control-label'>Import Type: </label><div class='col-sm-3'>");
				print ("<select onChange='fnImportTypeChanged(); return false;' name='frm_importType' id='frm_importType' class='form-control' style='width:300px;'>");
				print ("<option value=''>-- Please Select --</option>");
				foreach ($resultdata2 as $row) {
					if ($resultdata['importType'] == $row['recordID']) $selected = "selected"; else $selected = "";
					print ("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
				}
				print ("</select></div>");
			print ("</div>");
			
			//file format
			print ("<div style='clear:both;'>");
				$strdbsql = "SELECT * FROM configFileFormats";
				$strType = "multi";
				$resultdata2 = query($conn, $strdbsql, $strType);

				print ("<label for='frm_fileFormat' class='col-sm-2 control-label'>File Format: </label><div class='col-sm-3'>");
				print ("<select onChange='fnFileFormatChanged(); return false;' name='frm_fileFormat' id='frm_fileFormat' class='form-control' style='width:300px;'>");
				print ("<option value=''>-- Please Select --</option>");
				foreach ($resultdata2 as $row) {
					if ($resultdata['fileFormat'] == $row['description']) $selected = "selected"; else $selected = "";
					print ("<option $selected value='".$row['description']."'>".$row['description']."</option>");
				}
				print ("</select></div>");
			print ("</div>");
			
			
			
			//worksheets no
			print ("<div id='fileFormatDIV1' style='clear:both;'>");
				print ("<label class='col-sm-2 control-label' for='frm_worksheets'>Number of Worksheets:</label>");
				print ("<div class='col-sm-10'>");
					print ("<select name='frm_worksheets' id='frm_worksheets' class='form-control' style='width:130px;'>");

					for ($i = 1; $i <= 20; $i++) {
						if ($resultdata['worksheets'] == $i) $selected = "selected"; else $selected = "";
						print ("<option $selected value='".$i."'>".$i."</option>");
					}
					print ("</select>");
				print ("</div>");
			print ("</div>");
			
			//worksheets to import
			print ("<div id='fileFormatDIV2' style='clear:both;'>");
				print ("<label class='col-sm-2 control-label' for='frm_worksheetsList'>Worksheets to import:</label>");
				print ("<div class='col-sm-10'><input type='text' value='".$resultdata['worksheetsToImport']."' class='form-control' maxlength='50' size='35' style='width:200px;' id='frm_worksheetsList' name='frm_worksheetsList' /><label class='control-label' >Index of worksheets (comma separated). Begins at 0.</label></div>");
			print ("</div>");
			
			if ($importConfigID == 0) {
				print ("<div style='clear:both;'>");
					print ("<label for='fileToUpload' class='col-sm-2 control-label'>File Example:</label><div class='col-sm-10'>");
					print ("<input type='file' id='fileToUpload' name='fileToUpload[]' multiple='multiple'>");
					print ("<p class='help-block'>Valid Files: .txt, .csv, .xls, .xlsx</p>");
				print ("</div></div>");
			}
		
		print ("</div>");

		print ("<button class='btn btn-success pull-right' style='display:inline-block;' onclick='fnSaveFormat(); return false;'>Save</button>");
		print ("<button class='btn btn-danger pull-left' style='display:inline-block;' onclick='fnCancelFormatEdit(); return false;'>Cancel</button>");


		print ("<script language='Javascript' >
			$().ready(function() {
				fnImportTypeChanged();
				fnFileFormatChanged();
			});
		</script>");
	}
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['reload'])) $reload = $_REQUEST['reload']; else $reload = "";
	if (isset($_REQUEST['importConfigID'])) $importConfigID = $_REQUEST['importConfigID']; else $importConfigID = "";
	if (isset($_REQUEST['importWorksheetID'])) $importWorksheetID = $_REQUEST['importWorksheetID']; else $importWorksheetID = "";
	if (isset($_REQUEST['importWorksheetRecordID'])) $importWorksheetRecordID = $_REQUEST['importWorksheetRecordID']; else $importWorksheetRecordID = "";
	
	switch($strcmd){

		case "saveFormat":

			//files uploaded
			if(count($_FILES['fileToUpload'])) {
				
				$fileArray = reArrayFiles($_FILES['fileToUpload']);
				foreach ($fileArray as $file) {

					$fulldata = array();
					$original_extension = (false === $pos = strrpos($file["name"], '.')) ? '' : substr($file["name"], $pos);
					$directory = "/files/".$_SESSION['lottery']."/raw/";
					$delimiter = "";
					$filename = $directory.fnconvertunixtime($datnow,"H:i-d-m-Y")."_".$file["name"];
					$moveResult = copy($file["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$filename);//copy file into the raw directory
					if ($moveResult == true) {
						if (strtolower($original_extension) == '.xls' || strtolower($original_extension) == '.xlsx') {
							
							$objPHPExcel = PHPExcel_IOFactory::load($file['tmp_name']);
														
							//Document Data
							$worksheetindex = 0;
							foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
						
								//resets values
								$highestColumnIndex = 0;
								$worksheetTitle = $worksheet->getTitle();
								$highestRow = $worksheet->getHighestRow(); // e.g. 10
								$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
								$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
								$nrColumns = ord($highestColumn) - 64;
								
								for ($row = 1; $row <= 1; ++ $row) {
								
									if ($highestColumnIndex > 1) {
										for ($col = 0; $col < $highestColumnIndex; ++ $col) {
											$cell = $worksheet->getCellByColumnAndRow($col, $row);
											$val = $cell->getValue();
											$fulldata[$worksheetindex][$row][$col] = $val;
											if ($val != '') $blank = false;
										}
									}
								}
								
								$worksheetindex++;
							}
							
							$strsuccess .= "File successfully uploaded - ".$file['name']." to $filename <br/>";

						} else if (strtolower($original_extension) == '.txt' || strtolower($original_extension) == '.csv') {
						
							//csv files
							$fileobject = new SplFileObject($file['tmp_name']);
							$filecontrol = $fileobject->getCsvControl();
							$delimiter = $filecontrol[0];
							$highestColumnIndex = 0;
							
							//check to see if we can open the uploaded file
							if (($handle = fopen($file['tmp_name'], "r")) !== FALSE) {
								
								//set the php option for csv files created using a mac
								ini_set('auto_detect_line_endings',TRUE);
							
								$row = 1;
								if (!empty($filecontrol[0])){
									$data = fgetcsv($handle, 1000, $filecontrol[0], $filecontrol[1]);
									$num = count($data);
									if ($num > $highestColumnIndex) $highestColumnIndex = $num;
									for ($c=0; $c < $num; $c++) {
										$fulldata[0][1][$c] = $data[$c];
									}
								}
								
								fclose($handle);
								$strsuccess .= "File successfully uploaded - ".$file['name']." to $filename <br/>";
								ini_set('auto_detect_line_endings',FALSE);

							} else $strerror .= ("Could not open file ".$file['tmp_name']." <br/>");
						} else $strerror .= ("File Extension $original_extension not recognised or not supported.<br/>");
					} else $strerror .= ("Could not move file to the raw data directory ($filename)<br/>");
				}
			}
		
			//check to see if the name already exists
			$strdbsql = "SELECT * FROM configFileImportFormats WHERE description = :description AND status = 1 AND importID != :importID";
			$strType = "count";
			$arrdbparams = array("importID"=>$importConfigID,"description"=>$_REQUEST['frm_formatName']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			if ($resultdata == 0) {
				if (isset($_REQUEST['frm_paymenttype'])) $frm_paymenttype = $_REQUEST['frm_paymenttype']; else $frm_paymenttype = "";
				if (isset($_REQUEST['frm_worksheets'])) $frm_worksheets = $_REQUEST['frm_worksheets']; else $frm_worksheets = 1;
				if (isset($_REQUEST['frm_worksheetsList'])) $frm_worksheetsList = $_REQUEST['frm_worksheetsList']; else $frm_worksheetsList = 0;
				if ($importConfigID == "") { //insert
					$strdbsql = "INSERT INTO configFileImportFormats (description, importType, fileFormat, filePaymentType, worksheets, worksheetsToImport, status) VALUES (:description, :importType, :fileFormat, :filePaymentType, :worksheets, :worksheetsToImport, :status);";
					$strType = "insert";
					$arrdbparams = array(
						"description"=>$_REQUEST['frm_formatName'], 
						"importType"=>$_REQUEST['frm_importType'], 
						"fileFormat"=>$_REQUEST['frm_fileFormat'], 
						"filePaymentType"=>$frm_paymenttype, 
						"worksheets"=>$frm_worksheets, 
						"worksheetsToImport"=>$frm_worksheetsList, 
						"status"=>1
					);					
					$importConfigID = query($conn, $strdbsql, $strType, $arrdbparams);
				} else {
					$strdbsql = "UPDATE configFileImportFormats SET description = :description, importType = :importType, fileFormat = :fileFormat, filePaymentType = :filePaymentType, worksheets = :worksheets, worksheetsToImport = :worksheetsToImport, status = :status WHERE importID = :importID";
					$strType = "update";
					$arrdbparams = array(
						"description"=>$_REQUEST['frm_formatName'], 
						"importType"=>$_REQUEST['frm_importType'], 
						"fileFormat"=>$_REQUEST['frm_fileFormat'], 
						"filePaymentType"=>$frm_paymenttype, 
						"worksheets"=>$frm_worksheets, 
						"worksheetsToImport"=>$frm_worksheetsList, 
						"status"=>1,
						"importID"=>$importConfigID
					);
					$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
				}
				
				$delimitedFile = 2;
				$worksheetsArray = explode(",",$frm_worksheetsList);
				for ($i = 0; $i < $frm_worksheets; $i++) {
				
					if (in_array($i, $worksheetsArray)) {
					
						$noColumns = count($fulldata[$i][1]);
						$columnNames = implode(",",$fulldata[$i][1]);
					
						$strdbsql = "INSERT INTO configFileImportWorksheets (importID,importIndex,noColumns,columnNames,fileDelimiterType,fileDelimiter,noIgnoreTop) VALUES (:importID,:importIndex,:noColumns,:columnNames,:fileDelimiterType,:fileDelimiter,:noIgnoreTop)";
						$arrdbvalues = array("importID"=>$importConfigID, "importIndex"=>$i, "noColumns"=>$noColumns,"columnNames"=>$columnNames,"fileDelimiterType"=>$delimitedFile,"fileDelimiter"=>$delimiter,"noIgnoreTop"=>1);
						$strType = "insert";
						$resultinsert = query($conn, $strdbsql, $strType, $arrdbvalues);
				
					}
				}

				$strsuccess = "Format successfully saved";
				$importConfigID = "";
				$strcmd = "";
			} else {
				$strerror = "Format Name must be unique, '".$_REQUEST['frm_formatName']."' already exists.";
				$strcmd = "modifyFormat";
			}
			break;
		
		case "modifyColumns":
		case "saveWorksheet":

			$strdbsql = "UPDATE configFileImportWorksheets SET noColumns = :noColumns, accountMatchType = :accountMatchType, noIgnoreTop = :noIgnoreTop, noIgnoreBottom = :noIgnoreBottom, fileDelimiterType = :fileDelimiterType, fileDelimiter = :fileDelimiter, createNewMembers = :createNewMembers, worksheetPaymentType = :worksheetPaymentType, directDebitFileTypeID = :directDebitFileTypeID, memberImportFileTypeID = :memberImportType, worksheetNegPaymentAction = :worksheetNegPaymentAction WHERE recordID = :recordID";
			$strType = "update";
			if (isset($_REQUEST['frm_delimiterType'])) $frm_delimiterType = $_REQUEST['frm_delimiterType']; else $frm_delimiterType = 2;
			if (isset($_REQUEST['frm_delimiter'])) $frm_delimiter = $_REQUEST['frm_delimiter']; else $frm_delimiter = "";
			if (isset($_REQUEST['frm_createNewMembers'])) $frm_createNewMembers = $_REQUEST['frm_createNewMembers']; else $frm_createNewMembers = 0;
			if (isset($_REQUEST['frm_worksheetPaymentType'])) $frm_worksheetPaymentType = $_REQUEST['frm_worksheetPaymentType']; else $frm_worksheetPaymentType = 0;
			$arrdbparams = array(
				"recordID"=>$_REQUEST['importWorksheetRecordID'], 
				"noColumns"=>$_REQUEST['frm_noColumns'], 
				"accountMatchType"=>3, 
				"noIgnoreTop"=>$_REQUEST['frm_IgnoreRowsTop'], 
				"noIgnoreBottom"=>$_REQUEST['frm_IgnoreRowsBottom'], 
				"fileDelimiterType"=>$frm_delimiterType,
				"fileDelimiter"=>$frm_delimiter,
				"createNewMembers"=>$frm_createNewMembers,
				"worksheetPaymentType"=>$frm_worksheetPaymentType,
				"worksheetNegPaymentAction"=>$_REQUEST['frm_worksheetNegPaymentAction'],
				"directDebitFileTypeID"=>"",
				"memberImportType"=>""
			);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			
			if ($strcmd == 'modifyColumns') {
				$strcmd = "modifyColumns";
			} else {
				$strcmd = "";
				$strsuccess = "Worksheet Saved";
			}
			break;
		
		case "saveColumns":
		
			$strdbsql = "SELECT * FROM configFileImportWorksheets WHERE recordID = :recordID";
			$arrdbvalues = array("recordID" => $importWorksheetRecordID);
			$strType = "single";
			$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);
			
			$columnNames = "";
			if ($resultdata['fileDelimiterType'] == 1) $delimiterParams = "";
			for ($i = 1; $i <= $resultdata['noColumns']; $i++) {
				$columnNames .= str_replace(",", "",$_REQUEST["frm_column-$i-Name"]).",";
				if ($resultdata['fileDelimiterType'] == 1) $delimiterParams .= str_replace(",", "",$_REQUEST["frm_column-$i-delimiter"]).",";
			}
			$columnNames = substr($columnNames,0,strlen($columnNames)-1);
			
			if ($resultdata['fileDelimiterType'] == 1) $delimiterParams = rtrim($delimiterParams, ",");
			if ($resultdata['fileDelimiterType'] == 2) $delimiterParams = $resultdata['fileDelimiter'];

			$serializedMatchArray = serialize($_REQUEST['frm_columnMatch']);
			$serializedColumnDataMatch = serialize($_REQUEST['columnDataMatch']);
			$serializedConcatArray = serialize($_REQUEST['frm_concatMatch']);
			$serializedExplodeArray = serialize($_REQUEST['frm_explodeMatch']);
			$serializedConversionArray = serialize($_REQUEST['frm_conversionMatch']);
			$serializedPaymentArray = serialize($_REQUEST['frm_columnPayment']);
			//$serializedMatchArray = rawurlencode($serializedMatchArray);
			$columnUniqueRef = $_REQUEST['frm_columnUniqueRef'];

			$strdbsql = "UPDATE configFileImportWorksheets SET columnPayment = :columnPayment, columnUniqueRef = :columnUniqueRef, columnNames = :columnNames, fileDelimiter = :fileDelimiter, columnDeceased = :columnDeceased, columnCancelled = :columnCancelled, columnTxReference = :columnTxReference, columnCreditPayment = :columnCreditPayment, columnCreditPaymentFormat = :columnCreditPaymentFormat, columnDebitPayment = :columnDebitPayment, columnDebitPaymentFormat = :columnDebitPaymentFormat, columnDate = :columnDate, columnPaymentCode = :columnPaymentCode, columnAccountMatch = :columnAccountMatch, columnDataMatches = :columnDataMatches, columnCancelledValue=:columnCancelledValue, columnConcatenation=:columnConcatenation, columnExplode=:columnExplode, columnConversion=:columnConversion WHERE recordID = :recordID";
			$strType = "update";
			$arrdbparams = array("columnPayment"=>$serializedPaymentArray, "columnUniqueRef"=>$columnUniqueRef, "columnNames"=>$columnNames, "fileDelimiter"=>$delimiterParams, "columnDeceased"=>$_REQUEST['frm_columnDeceased'], "columnCancelled"=>$_REQUEST['frm_columnCancelled'], "columnTxReference"=>$_REQUEST['frm_columnTxReference'], "columnCreditPayment"=>$_REQUEST['frm_columnCreditPayment'], "columnCreditPaymentFormat"=>$_REQUEST['frm_columnCreditPaymentFormat'], "columnDebitPayment"=>$_REQUEST['frm_columnDebitPayment'], "columnDebitPaymentFormat"=>$_REQUEST['frm_columnDebitPaymentFormat'], "columnDate"=>$_REQUEST['frm_columnDate'], "columnPaymentCode"=>$_REQUEST['frm_columnPaymentCode'], "columnDataMatches"=>$serializedColumnDataMatch, "columnAccountMatch"=>$serializedMatchArray, "columnCancelledValue"=>$_REQUEST['frm_canccontent'], "columnConcatenation"=>$serializedConcatArray, "columnExplode"=>$serializedExplodeArray, "columnConversion"=>$serializedConversionArray, "recordID"=>$importWorksheetRecordID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

			$strsuccess = "Columns Saved";
			if ($reload == 1) {
				$strcmd = "modifyColumns";
			} else {
				$strcmd = "modifyWorksheet";
			}
			break;
		case "deleteFormat":
			$strdbsql = "UPDATE configFileImportFormats SET status = 0 WHERE importID = :importConfigID";
			$strType = "update";
			$arrdbparams = array("importConfigID"=>$importConfigID);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			$strsuccess = "Import Format Deleted";
			$importConfigID = "";
			$importWorksheetID = "";
			$strcmd = "";
			break;
		case "cancelFormat":
			$strcmd = "";
			$importConfigID = "";
			break;
		case "cancelWorksheet":
			$strcmd = "";
			$importConfigID = "";
			break;
		case "cancelColumn":
			$strcmd = "modifyWorksheet";
			break;
	}

	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>".$configPageDetails['pageTitle']."</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

?>	
<script language='Javascript' >
	function fnNewFormat() {
		if ($('#form').valid()) {
			document.getElementById("cmd").value = "createFormat";
			document.getElementById("form").submit();
		}
	}
	function fnModifyFormat(importConfigID) {
		if ($('#form').valid()) {
			document.getElementById("importConfigID").value = importConfigID;
			document.getElementById("cmd").value = "modifyFormat";
			document.getElementById("form").submit();
		}
	}
	function fnLoadTestFile(importConfigID){
		document.getElementById("importConfigID").value = importConfigID;
		document.getElementById("cmd").value = "testFormat";
		document.getElementById("form").submit();
	}
	function fnDeleteFormat(importConfigID){
		document.getElementById("importConfigID").value = importConfigID;
		document.getElementById("cmd").value = "deleteFormat";
		document.getElementById("form").submit();
	}
	function fnModifyWorksheet(importConfigID) {
		document.getElementById("importConfigID").value = importConfigID;
		var worksheetSelect = document.getElementById("frm_worksheets"+importConfigID);
		var importWorksheetID = worksheetSelect.options[worksheetSelect.selectedIndex].value;
		document.getElementById("importWorksheetID").value = importWorksheetID;
		document.getElementById("cmd").value = "modifyWorksheet";
		document.getElementById("form").submit();
	}
	function fnModifyColumns(importWorksheetRecordID) {
		document.getElementById("importWorksheetRecordID").value = importWorksheetRecordID;
		document.getElementById("cmd").value = "modifyColumns";
		document.getElementById("form").submit();
	}
	function fnSaveColumns(reload) {
		if ($('#form').valid()) {
			document.getElementById("cmd").value = "saveColumns";
			document.getElementById("reload").value = reload;
			document.getElementById("form").submit();
		}
	}
	function fnSaveWorksheet(importWorksheetRecordID) {
		if ($('#form').valid()) {
			document.getElementById("cmd").value = "saveWorksheet";
			document.getElementById("importWorksheetRecordID").value = importWorksheetRecordID;
			document.getElementById("form").submit();
		}
	}
	function fnSaveFormat() {
		if ($('#form').valid()) {
			document.getElementById("cmd").value = "saveFormat";
			document.getElementById("form").submit();
		}
	}
	function fnCancelColumnEdit(){
		document.getElementById("cmd").value = "cancelColumn";
		document.getElementById("form").submit();
	}
	function fnCancelWorksheetEdit(){
		document.getElementById("cmd").value = "cancelWorksheet";
		document.getElementById("form").submit();
	}
	function fnCancelFormatEdit(){
		document.getElementById("cmd").value = "cancelFormat";
		document.getElementById("form").submit();
	}
	function fnDelimiterTypeChange(){
		var delimiterType = document.getElementById("frm_delimiterType").value;
		if (delimiterType == 1) {
			$('#delimiterContainer').hide();
		} else {
			$('#delimiterContainer').show();
		}						
	}
	function fnImportTypeChanged(){
		var e = document.getElementById("frm_importType");
		var importType = e.options[e.selectedIndex].value;
		switch (importType){
			case "1": //payment
				$("#paymentTypeDIV").show();
				$("#frm_paymenttype").prop('disabled', false);			
			break;
			default:
				$("#paymentTypeDIV").hide();
				$("#frm_paymenttype").prop('disabled', true);
			break;
		}
	}
	function fnFileFormatChanged(){
		var e = document.getElementById("frm_fileFormat");
		var importType = e.options[e.selectedIndex].value;
		switch (importType){
			case ".xlsx": //payment
			case ".xls": //payment
				$("#fileFormatDIV1").show();
				$("#fileFormatDIV2").show();
				$("#frm_worksheets").prop('disabled', false);
				$("#frm_worksheetsList").prop('disabled', false);
			break;
			default:
				$("#fileFormatDIV1").hide();
				$("#fileFormatDIV2").hide();
				$("#frm_worksheets").prop('disabled', true);
				$("#frm_worksheetsList").prop('disabled', true);
			break;
		}
	}
	function addNewConcatColumn(concatID){
	
		//get the current count of columns (less 1 as we start at 0 where as count starts at 1)
		var count = $('#noofcolumns'+concatID).val() - 1;
		
		//get the count of concatenations
		var concatcount = $('#noofconcats').val() - 1;
		
		//clone the last concatMatchSection
		var $clone = $('#concatMatchSection'+concatID+'-'+count+':eq(0)').clone();
		newcount = count+1;
		
		//increment each elements ID by 1 and blank the value
		$clone.find('[id]').each(function(){
			this.id = this.id.replace('['+concatcount+']['+count+']','['+concatcount+']['+newcount+']');
			this.value='';
			this.name = this.name.replace('['+concatcount+']['+count+']','['+concatcount+']['+newcount+']');
		});
		
		//change the cloned elements ID
		$clone.attr('id', "concatMatchSection"+concatID+"-"+newcount);
		
		//append the cloned element to the last concatMatchSection
		$('#concatMatchSection'+concatID+'-'+count+':eq(0)').after($clone);
		
		//increase the number of joins
		$('#noofcolumns'+concatID).val(newcount+1);

		//all delete button
		$('#deleteColumn'+concatID).show();
	}
	function deleteConcatColumn(concatID){
		var count = $('#noofcolumns'+concatID).val() - 1;
		$('#concatMatchSection'+concatID+'-'+count).remove();
		$('#noofcolumns'+concatID).val(count);
		if (count>1)  $('#deleteColumn'+concatID).show();
		else $('#deleteColumn'+concatID).hide();
	}
</script>
<?php
		print ("<form action='config-imports.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data'>");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' value='$strcmd'/>");
			print ("<input type='hidden' class='nodisable' name='reload' id='reload' value='0'/>");
			print ("<input type='hidden' class='nodisable' name='importConfigID' id='importConfigID' value='$importConfigID'/>");
			print ("<input type='hidden' class='nodisable' name='importWorksheetID' id='importWorksheetID' value='$importWorksheetID'/>");
			print ("<input type='hidden' class='nodisable' name='importWorksheetRecordID' id='importWorksheetRecordID' value='$importWorksheetRecordID'/>");

										
			switch ($strcmd) {
				case "createFormat":
					fnModifyFormat(0,$conn);
					break;
				case "modifyFormat":
					fnModifyFormat($importConfigID,$conn);
					break;
				case "modifyWorksheet":
					fnModifyWorksheet($importWorksheetID,$importConfigID,$conn);
					break;
				case "modifyColumns":
					fnModifyColumns($importWorksheetRecordID,$conn,$connmaster);
					break;
				case "testFormat":
					fnTestFormat($importConfigID,$conn);
					break;				
				default:
					
					$strdbsql = "SELECT configFileImportFormats.*, configFileImportTypes.description AS importTypeDesc FROM configFileImportFormats
					INNER JOIN configFileImportTypes ON configFileImportFormats.importType = configFileImportTypes.recordID
					WHERE configFileImportFormats.status = 1 ORDER BY configFileImportFormats.description";
					$strType = "multi";
					$arrdbparams = array();
					$resultdatamulti = query($conn,$strdbsql,$strType,$arrdbparams);

					
					print ("<div class='section'>");
						print ("<table id='fileImportFormats' class='table table-striped table-bordered table-hover table-condensed'>");
							print ("<thead>");
							print ("<tr>");
							print ("<th>Description</th>");
							print ("<th>Import Type</th>");
							print ("<th>File Format</th>");
							print ("<th>Worksheets / Pages</th>");
							print ("<th>Format Options</th>");
							print ("</tr></thead><tbody>");

							foreach ($resultdatamulti as $row) {
								print ("<tr>");
									print ("<td>".$row['description']."</td>");
									if ($row['importType'] == 1) $importType = ($row['importTypeDesc']." (".$row['paymentDesc'].")"); else $importType = $row['importTypeDesc'];
									print ("<td>".$importType."</td>");
									print ("<td>".$row['fileFormat']."</td>");
									print ("<td>");
										print ("<div style='display:inline; float:left;'>");
										print ("<select name='frm_worksheets".$row['importID']."' id='frm_worksheets".$row['importID']."' class='form-control' style='width:130px; display: inline;'>");
										$worksheets = explode(",",$row['worksheetsToImport']);
										foreach ($worksheets AS $worksheet){
											print ("<option value='".$worksheet."'>".$worksheet."</option>");
										}
										print ("</select>");								
										print ("</div>");								
									print ("<button style='margin-left:10px; display: inline;' class='center btn btn-primary circle' onclick='fnModifyWorksheet(".$row['importID']."); return false;'><i class='fa fa-pencil'></i></button></td>");
									print ("<td><button class='btn btn-primary  center circle' onclick='fnModifyFormat(".$row['importID']."); return false;' style='display:inline;'><i class='fa fa-pencil'></i></button>");
									//print ("<button class='btn btn-primary' style='margin-left:10px;' onclick='fnLoadTestFile(".$row['importID']."); return false;'>Test Format</button>");
									print ("<button class='btn btn-danger center circle' style='margin-left:10px; display:inline;' onclick='fnDeleteFormat(".$row['importID']."); return false;' ><i class='fa fa-trash'></i></button></td>");
								print ("</tr>");
							}
							print ("</tbody>");
						print ("</table>");
					print ("</div>");
					print ("<button class='btn btn-success right' onclick='fnNewFormat(); return false;'>Create Format</button>");

					
					break;
			}
			print ("</form>");
		print("</div>");
	print("</div>");

?>	
<script language='Javascript' >
	$().ready(function() {
		$('#addNewJoin').click(function(){
			
			//get the current count of joins (less 1 as we start at 0 where as count starts at 1)
			var count = $('#noofjoins').val() - 1;
			
			//clone the last accountMatchSection
			var $clone = $('#accountMatchSection'+count+':eq(0)').clone();
			newcount = count+1;
			
			//increment each elements ID by 1 and blank the value
			$clone.find('[id]').each(function(){
				this.id = this.id.replace('['+count+']','['+newcount+']');
				this.value='';
				this.name = this.name.replace('['+count+']','['+newcount+']');
			});
			
			//change the cloned elements ID
			$clone.attr('id', "accountMatchSection"+newcount);
			
			//append the cloned element to the last accountMatchSection
			$('#accountMatchSection'+count+':eq(0)').after($clone);
			
			//increase the number of joins
			$('#noofjoins').val(newcount+1);

			//all delete button
			$('#deleteJoin').show();
		});
			
		$('#deleteJoin').click(function(){
			var count = $('#noofjoins').val() - 1;
			$('#accountMatchSection'+count).remove();
			$('#noofjoins').val(count);
			if (count>1)  $('#deleteJoin').show();
			else $('#deleteJoin').hide();
		});

		
		/*Concatenations*/
		$('#addNewConcatenate').click(function(){
			
			//get the current count of joins (less 1 as we start at 0 where as count starts at 1)
			var count = $('#noofconcats').val() - 1;
			
			//clone the last concatMatchSection
			var $clone = $('#concatBlock'+count+':eq(0)').clone();
			newcount = count+1;
			
			//increment each elements ID by 1 and blank the value
			$("legend", $clone).text('Column Concatenation '+(newcount+1));
			$("#noofcolumns"+count, $clone).attr('id', "noofcolumns"+newcount);
			$("#noofcolumns"+newcount, $clone).attr('name', "noofcolumns"+newcount);
			$("#noofcolumns"+newcount, $clone).val(1);

			$("#addNewColumn"+count, $clone).attr('id', "addNewColumn"+newcount);
			$("#addNewColumn"+newcount, $clone).attr('name', "addNewColumn"+newcount);
			$("#addNewColumn"+newcount, $clone).attr('onclick', "addNewConcatColumn("+newcount+")");
			
			$("#deleteColumn"+count, $clone).attr('id', "deleteColumn"+newcount);
			$("#deleteColumn"+newcount, $clone).attr('name', "deleteColumn"+newcount);
			$("#deleteColumn"+newcount, $clone).attr('onclick', "deleteConcatColumn("+newcount+")");
			
			//remove all columns except first one
			$(".concatMatchSection"+count, $clone).each(function(){
				if(this.id != "concatMatchSection"+count+"-0"){
					this.remove();
				}			
			});	
			
			$("#concatMatchSection"+count+"-0", $clone).find('[id]').each(function(){
				this.id = this.id.replace('['+count+'][0]','['+newcount+'][0]');
				this.value='';
				this.name = this.name.replace('['+count+'][0]','['+newcount+'][0]');
			});

			$("#concatMatchSection"+count+"-0", $clone).attr('name', "concatMatchSection"+newcount);			
			$("#concatMatchSection"+count+"-0", $clone).attr('class', "concatMatchSection"+newcount);			
			$("#concatMatchSection"+count+"-0", $clone).attr('id', "concatMatchSection"+newcount+"-0");			
			
			//change the cloned elements ID
			$clone.attr('id', "concatBlock"+newcount);
			$clone.attr('name', "concatBlock"+newcount);
			
			//append the cloned element to the last concatMatchSection
			$('#concatBlock'+count+':eq(0)').after($clone);
			
			//increase the number of joins
			$('#noofconcats').val(newcount+1);
			
			//all delete button
			$('#deleteColumn'+newcount).hide();
			$('#deleteConcatenate').show();
		});
		
		$('#deleteConcatenate').click(function(){
			var count = $('#noofconcats').val() - 1;
			$('#concatBlock'+count).remove();
			$('#noofconcats').val(count);
			if (count>1)  $('#deleteConcatenate').show();
			else $('#deleteConcatenate').hide();
		});
		

		/*Explodes*/
		$('#addNewExplode').click(function(){
			
			//get the current count of explodes (less 1 as we start at 0 where as count starts at 1)
			var count = $('#noofexplodes').val() - 1;
			
			//clone the last explodeMatchSection
			var $clone = $('#explodeMatchSection'+count+':eq(0)').clone();
			newcount = count+1;
			
			//increment each elements ID by 1 and blank the value
			$clone.find('[id]').each(function(){
				this.id = this.id.replace('['+count+']','['+newcount+']');
				this.value='';
				this.name = this.name.replace('['+count+']','['+newcount+']');
			});
			
			//change the cloned elements ID
			$clone.attr('id', "explodeMatchSection"+newcount);
			
			//append the cloned element to the last concatMatchSection
			$('#explodeMatchSection'+count+':eq(0)').after($clone);
			
			//increase the number of joins
			$('#noofexplodes').val(newcount+1);

			//all delete button
			$('#deleteExplode').show();
		});
			
		$('#deleteExplode').click(function(){
			var count = $('#noofexplodes').val() - 1;
			$('#explodeMatchSection'+count).remove();
			$('#noofexplodes').val(count);
			if (count>1)  $('#deleteExplode').show();
			else $('#deleteExplode').hide();
		});
		
		/*Conversions*/
		$('#addNewConversion').click(function(){
			
			//get the current count of Conversion (less 1 as we start at 0 where as count starts at 1)
			var count = $('#noofconversions').val() - 1;
			
			//clone the last conversionMatchSection
			var $clone = $('#conversionMatchSection'+count+':eq(0)').clone();
			newcount = count+1;
			
			//increment each elements ID by 1 and blank the value
			$clone.find('[id]').each(function(){
				this.id = this.id.replace('['+count+']','['+newcount+']');
				this.value='';
				this.name = this.name.replace('['+count+']','['+newcount+']');
			});
			
			//change the clone elements ID
			$clone.attr('id', "conversionMatchSection"+newcount);
			
			//append the clone element to the last concatMatchSection
			$('#conversionMatchSection'+count+':eq(0)').after($clone);
			
			//increase the number of joins
			$('#noofconversions').val(newcount+1);

			//all delete button
			$('#deleteConversion').show();
		});
			
		$('#deleteConversion').click(function(){
			var count = $('#noofconversions').val() - 1;
			$('#conversionMatchSection'+count).remove();
			$('#noofconversions').val(count);
			if (count>1)  $('#deleteConversion').show();
			else $('#deleteConversion').hide();
		});


		$('#fileImportFormats').DataTable({
			"dom": 'T<"clear">lfrtip',
			"tableTools": {
				"sSwfPath": "js/copy_csv_xls_pdf.swf"
			}
		});

	});
</script>
<?php
	
// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>