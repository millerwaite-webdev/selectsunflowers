<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-manage-products.php                                      * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-manage-products"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	$thumbWidth = 297;
	$thumbHeight = 220;
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['image'])) $strimage = $_REQUEST['image']; else $strimage = "";
	if (isset($_REQUEST['stockGroupID']))
	{
		$stockGroupID = $_REQUEST['stockGroupID'];
	}
	elseif(isset($_REQUEST['searchStockGroupID']))
	{
		$stockGroupID = $_REQUEST['searchStockGroupID'];
	}
	else
	{
		$stockGroupID = "";
	}
	if (isset($_REQUEST['hamperStockID'])) $hamperStockID = $_REQUEST['hamperStockID']; else $hamperStockID = "";
	if (isset($_REQUEST['stockOptionID'])) $stockOptionID = $_REQUEST['stockOptionID']; else $stockOptionID = "";
	if (isset($_REQUEST['overlayType'])) $overlayType = $_REQUEST['overlayType']; else $overlayType = "";
	
	switch($strcmd)
	{
		case "insertStockGroup":
		case "updateStockGroup":
			
			$strdbsql = "SELECT * FROM stock_attribute_type";
			$arrType = "multi";
			$attributeTypes = query($conn, $strdbsql, $arrType);
			
			$attributesList = "";
			$key = "frm_config";
			
			foreach($attributeTypes AS $attributeType)
			{
				$optionKey = $key.$attributeType['description'];
				if (isset($_REQUEST[$optionKey])) { $attributesList .= $attributeType['recordID'].","; }
			}
			$attributesList = rtrim($attributesList, ",");
			
			if ($strcmd == "insertStockGroup")
			{
				$strdbsql = "INSERT INTO stock_group_information (name, summary, description, retailPrice, metaLink, metaDoctitle, metaDescription, metaKeywords, statusID, brandID, brandOrder, optionsList, dateCreated, productType, productCost) VALUES (:name, :summary, :description, :retailPrice, :metaLink, :metaDoctitle, :metaDescription, :metaKeywords, :statusID, :brandID, :brandOrder, :optionsList, ".time().", :productType, :productCost)";
				$strType = "insert";
				
			}
			elseif ($strcmd == "updateStockGroup")
			{
				$strdbsql = "UPDATE stock_group_information SET name = :name, summary = :summary, description = :description, retailPrice = :retailPrice, metaLink = :metaLink, metaDoctitle = :metaDoctitle, metaDescription = :metaDescription, metaKeywords = :metaKeywords, statusID = :statusID, brandID = :brandID, brandOrder = :brandOrder, optionsList = :optionsList, dateLastModified = ".time().", productType = :productType, productCost = :productCost WHERE recordID = :recordID";
				$strType = "update";
			}
			
			$arrdbparams = array(
				"name" => $_POST['frm_productname'],
				"summary" => $_POST['frm_productsummary'],
				"description" => $_POST['frm_productdescription'],
				"retailPrice" => $_POST['frm_retailprice'],
				"metaLink" => $_POST['frm_metaLink'],
				"metaDoctitle" => $_POST['frm_metadoctitle'],
				"metaDescription" => $_POST['frm_metadescription'],
				"metaKeywords" => $_POST['frm_metakeywords'],
				"statusID" => $_POST['frm_status'],
				"productType" => $_POST['frm_productType'],
				"productCost" => ($_POST['frm_productCost'] ? $_POST['frm_productCost'] : 0),
				"optionsList" => $attributesList
			);
			if($_POST['frm_brand'] == "null")
			{
				$arrdbparams["brandID"] = null;
				$arrdbparams["brandOrder"] = null;
			}
			else
			{
				$arrdbparams["brandID"] = $_POST['frm_brand'];
				$getMaxBrandOrder = "SELECT COUNT(recordID) AS max_brand_order FROM stock_group_information WHERE brandID = :brandID";
				$maxBrandOrderType = "single";
				$maxBrandOrderParam = array("brandID" => $_POST['frm_brand']);
				$maxBrandOrder = query($conn, $getMaxBrandOrder, $maxBrandOrderType, $maxBrandOrderParam);
				$brandOrder = intval($maxBrandOrder['max_brand_order'] + 1);
				$arrdbparams["brandOrder"] = $brandOrder;
			}
			
			if ($strcmd == "updateStockGroup")
			{
				$arrdbparams['recordID'] = $stockGroupID;
			}
			
			if ($strcmd == "updateStockGroup")
			{
				$arrdbparams['recordID'] = $stockGroupID;
			}
			$updateStockGroup = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertStockGroup")
			{
				if ($updateStockGroup > 0)
				{
					$strsuccess = "Stock group successfully added";

					$strdbsqlstock = "INSERT INTO stock (stockCode, price, weight, vatID, groupID) VALUES (:stockCode, :price, :weight, :vatID, :groupID)";
					$strType2 = "insert";
					$arrdbparams2 = array(
						"stockCode"=>str_replace(" ", "-", strtolower($_POST['frm_productname'])),
						"price"=>0, 
						"weight"=>0, 
						"vatID"=>1, 
						"groupID"=>$updateStockGroup
					);
					$insertstock = query($conn, $strdbsqlstock, $strType2, $arrdbparams2);
					
				}
				elseif ($updateStockGroup == 0)
				{
					$strerror = "An error occurred while adding the stock group";
				}
				$stockGroupID = $updateStockGroup;
			}
			elseif ($strcmd == "updateStockGroup")
			{
				if ($updateStockGroup <= 1)
				{
					$strsuccess = "Stock group successfully updated";
				}
				elseif ($updateStockGroup > 1)
				{
					$strwarning = "An error may have occurred while updating this stock group";
				}
			}
			
			$strcmd = "viewStockGroup";
			
		break;
		
		case "toggleStockGroup":
		
			$strdbsql = "SELECT * FROM stock_group_information WHERE recordID = :recordID";
			$getStockGroup = query($conn, $strdbsql, "single", array("recordID"=>$stockGroupID));
			
			if($getStockGroup['statusID'] != 4) $toggle = 4;
			else $toggle = 1;
		
			$strdbsql = "UPDATE stock_group_information SET statusID = :toggle WHERE recordID = :recordID";
			$toggleStockGroup = query($conn, $strdbsql, "update", array("recordID"=>$stockGroupID, "toggle"=>$toggle));
			
			if($toggleStockGroup == 1) $strsuccess = "Stock group successfully disabled";
			else if($toggleStockGroup == 0) $strerror = "An error occurred while disabling the stock group";
			else if($toggleStockGroup > 0) $strwarning = "An error may have occurred while disabling this stock group";
			
			$strcmd = "";
		
		break;
		
		case "deleteStockOption":
			
			$strdbsql = "UPDATE `stock` SET `status` = 0 WHERE `recordID` = :recordID";
			$strType = "update";
			$arrdbparams = array(
								"recordID" => $stockOptionID
							);
							
			$deleteStockOption = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($deleteStockOption == 1)
			{
				$strsuccess = "Stock option successfully deleted";
			}
			else
			{
				$strwarning = "An error may have occurred while deleting this stock option, please confirm it is no longer visible";
			}
			
			$strcmd = "viewStockGroup";
			
		break;
		
		case "insertStockOption":
		case "updateStockOption":
		
			if ($strcmd == "insertStockOption")
			{
				$strdbsqlcolumns = "INSERT INTO stock (stockCode, stockLevels, priceControl, price, dimensionID, weight, unit, vatID";
				$strdbsqlvalues = "VALUES (:stockCode, :stockLevels, :priceControl, :price, :dimensionID, :weight, :unit, :vatID";
				$strType = "insert";
			}
			else
			{
				$strdbsql = "UPDATE stock SET stockCode = :stockCode, stockLevels = :stockLevels, priceControl = :priceControl, price = :price, dimensionID = :dimensionID, weight = :weight, unit = :unit, vatID = :vatID";
				$strType = "update";
			}
			
			$arrdbparams = array(
				"stockCode" => str_replace(" ", "-", strtolower($_POST['frm_stockcode'])),
				"stockLevels" => $_POST['frm_stocklevels'],
				"priceControl" => $_POST['frm_pricecontrol'],
				"price" => $_POST['frm_price'],
				"weight" => $_POST['frm_weight'],
				"unit" => $_POST['frm_unit'],
				"vatID" => $_POST['frm_vat']
			);
			if($_POST['frm_dimension'] == "null")
			{
				$arrdbparams["dimensionID"] = null;
			}
			else
			{
				$arrdbparams["dimensionID"] = $_POST['frm_dimension'];
			}
			
			
			if ($strcmd == "insertStockOption")
			{
				$strdbsql = $strdbsqlcolumns.", groupID) ".$strdbsqlvalues.", :groupID)";
				$arrdbparams['groupID'] = $stockGroupID;
			}
			else
			{
				$arrdbparams['recordID'] = $stockOptionID;
				$strdbsql .= " WHERE recordID = :recordID";
			}
			$updateStockOption = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertStockOption")
			{
				if ($updateStockOption > 0)
				{
					$strsuccess = "Stock option successfully added";
					
					$dirProduct = mkdir("../images/products/".$updateStockOption, 0777, true);
					$dirList = mkdir("../images/products/".$updateStockOption."/list", 0777, true);
					$dirMain = mkdir("../images/products/".$updateStockOption."/main", 0777, true);
					$dirZoom = mkdir("../images/products/".$updateStockOption."/zoom", 0777, true);
					
					if(!$dirProduct || !$dirList || !$dirMain || !$dirZoom)
					{
						$strwarning = "Not all image directories could be created for this new stock option";
					}
				}
				elseif ($updateStockOption == 0)
				{
					$strerror = "An error occurred while adding the stock option";
				}
				$stockOptionID = $updateStockOption;
			}
			elseif ($strcmd == "updateStockOption")
			{
				if ($updateStockOption <= 1)
				{
					$strsuccess = "Stock option successfully updated";
				}
				elseif ($updateStockOption > 1)
				{
					$strwarning = "An error may have occurred while updating this stock option";
				}
			}
			
			
			$strdbsql = "SELECT * FROM stock_attribute_type";
			$strType = "multi";
			$attributeTypes = query($conn, $strdbsql, $strType);
			
			$key = "frm_";
			foreach($attributeTypes AS $attributeType)
			{
				$attributeKey = $key.$attributeType['description'];
				if (isset($_POST[$attributeKey]))
				{
					$arrdbparams = array(
										"stockAttributeID" => $_POST[$attributeKey],
										"stockID" => $stockOptionID
									);
					if($strcmd == "updateStockOption")
					{
						$strdbsql = "SELECT stock_attribute_relations.stockAttributeID FROM stock_attribute_relations INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID WHERE stock_attributes.attributeTypeID = :stockAttributeTypeID AND stock_attribute_relations.stockID = :stockID";
						$strType = "single";
						$arrdbattparams = array(
											"stockAttributeTypeID" => $attributeType['recordID'],
											"stockID" => $stockOptionID
										);
						$optionAssigned = query($conn, $strdbsql, $strType, $arrdbattparams);
					}
					
					if($strcmd == "insertStockOption" || empty($optionAssigned['stockAttributeID']))
					{
						$strdbsql = "INSERT INTO stock_attribute_relations (stockAttributeID, stockID) VALUES (:stockAttributeID, :stockID)";
						$strType = "insert";
					}
					elseif($strcmd == "updateStockOption" && !empty($optionAssigned['stockAttributeID']))
					{
						$strdbsql = "UPDATE stock_attribute_relations SET stockAttributeID = :stockAttributeID WHERE stockID = :stockID AND stockAttributeID = :oldStockAttributeID";
						$strType = "update";
						$arrdbparams['oldStockAttributeID'] = $optionAssigned['stockAttributeID'];
					}
					$updateStockAttribute = query($conn, $strdbsql, $strType, $arrdbparams);
				}
			}
			
			$strcmd = "viewStockGroup";
			
			break;
			case "addHamperStock":
			
				$strdbsql = "INSERT INTO stock_hamper (stockGroupID, stockItemID) VALUES (:stockGroupID, :stockItemID)";
				$strType = "insert";
				$arrdbparams = array("stockGroupID"=>$stockGroupID, "stockItemID"=>$_REQUEST['frm_hamperStock']);
				$insertHamperStock = query($conn, $strdbsql, $strType, $arrdbparams);
				
				if ($insertHamperStock > 1) $strsuccess = "Hamper stock successfully added";
				$strcmd = "viewStockGroup";
			
			break;
			case "deleteHamperStock":
			
				$strdbsql = "DELETE FROM stock_hamper WHERE recordID = :recordID";
				$strType = "delete";
				$arrdbparams = array("recordID"=>$hamperStockID);
				$deleteStockOption = query($conn, $strdbsql, $strType, $arrdbparams);
				
				if ($deleteStockOption == 1) $strsuccess = "Hamper stock successfully deleted";
				elseif ($deleteStockOption == 0) $strerror = "An error occurred while deleting";
				elseif ($deleteStockOption > 0) $strwarning = "An error may have occurred while deleting";
				
				$strcmd = "viewStockGroup";
			
			break;
			case "insertImage":
				
				$newfilename = $_FILES['frm_newimage']['name'];
				$strfileextn = getExtension($newfilename);
				
				$strimguploadpath = "images/";
				$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
				$upfile = $struploaddir.$newfilename;

				if($_FILES['frm_newimage']['error'] > 0) {
					switch($_FILES['frm_newimage']['error']) {
						case 1: $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file."; break;
						case 2: $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file."; break;
						case 3: $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file."; break;
						case 4: $strerror = "Problem: No file selected. Please try again."; break;
					}
				} else {
					// Does the file have the right extension ?		
					if(strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0) {
						$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
					} else {
						if(is_uploaded_file($_FILES['frm_newimage']['tmp_name'])) {
							if(!move_uploaded_file($_FILES['frm_newimage']['tmp_name'], $upfile)) {
								$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_newimage']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
							}
						} else {
							$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_newimage']['name'].". Please try again.<br/>";
						}
					}
				}
				
				if($strerror != "") {
					$strcommand = "ERRORMSG";
				} else {
				
					$strpagepath = $strimguploadpath."products/main/".$strimage."/";
					$strthumbpath = $strimguploadpath."products/thumbnails/".$strimage."/";
					
					$strpagedir = $strrootpath.$strpagepath;
					
					$newfile = $strpagedir.$newfilename;
					
					$thumbfile = $strrootpath.$strthumbpath.$newfilename;
					
					error_log("Hello World: ".$upfile);
					
					$range = copy($upfile,$newfile);
					chmod ($newfile, 0777);
					$range2 = image_scaleandcrop($upfile,$thumbfile,$thumbWidth,$thumbHeight);
					chmod ($thumbfile, 0777);
					
					unlink($upfile);
					
					$imageOrder = "SELECT * FROM stock_images WHERE imageTypeID = 1 AND stockID = :stock ORDER BY imageOrder DESC LIMIT 1";
					$result = query($conn,$imageOrder,"multi",array("stock"=>$stockGroupID));
					
					if(count($result) > 0) $order = $result[0]['imageOrder'] + 1;
					else $order = 1;
					
					$updatePageImageQuery = "INSERT INTO stock_images (description, imageLink, imageTypeID, stockID, imageOrder) VALUES (:description, :image, 1, :stock, :order)";
					$updatePageImage = query($conn,$updatePageImageQuery,"insert",array("description"=>$_POST['frm_productname'],"image"=>$newfilename,"stock"=>$stockGroupID,"order"=>$order));
					
					$strsuccess = "Image Added";
				}
				
				$strcmd = "viewStockGroup";
				
			break;
			case "deleteImages":
				
				$image = str_replace("/images/products/main/", "", $_REQUEST['deleteImages']);
				
				$strdbsql = "DELETE FROM stock_images WHERE imageLink = :image AND stockID = :stock";
				$result = query($conn,$strdbsql,"delete",array("image"=>$image,"stock"=>$stockGroupID));
				
				if($result) {
					if(unlink($_SERVER['DOCUMENT_ROOT'].$_REQUEST['deleteImages'])) {
						$strsuccess = "Image Deleted";
					} else {
						$strerror = "There was a problem deleting this image. Please try again.";
					}
				} else {
					$strerror = "There was a problem deleting this image. Please try again.";
				}
				
				$strcmd = "viewStockGroup";
				
			break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Stock Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddStockGroup() {
					document.form.cmd.value='addStockGroup';
					document.form.submit();
				}
				function jsviewStockGroup(stockGroupID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewStockGroup';
						document.form.stockGroupID.value=stockGroupID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertStockGroup() {
					if ($('#form').valid()) {
						document.form.cmd.value='insertStockGroup';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateStockGroup() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateStockGroup';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jstoggleStockGroup(stockGroupID) {
					if ($('#form').valid()) {
						document.form.cmd.value='toggleStockGroup';
						document.form.stockGroupID.value=stockGroupID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsaddStockOption() {
					document.form.cmd.value='addStockOption';
					document.form.submit();
				}
				function jsinsertStockOption() {
					document.form.cmd.value='insertStockOption';
					document.form.submit();
				}
				function jsupdateStockOption() {
					document.form.cmd.value='updateStockOption';
					document.form.submit();
				}
				function jseditStockOption(stockOptionID) {
					document.form.stockOptionID.value=stockOptionID;
					document.form.cmd.value='editStockOption';
					document.form.submit();
				}
				function jsdeleteStockOption(stockOptionID) {
					if(confirm("Are you sure you want to delete this stock option?"))
					{
						document.form.stockOptionID.value=stockOptionID;
						document.form.cmd.value='deleteStockOption';
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertImage(image) {
					document.form.image.value=image;
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddGalleryImage() {
					document.form.cmd.value='addGalleryImage';
					document.form.submit();
				}
				function jsdeleteImages() {
					if($("#sortableGalleryImages li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this image(s)?"))
						{
							var deleteImages = "";
							$("#sortableGalleryImages li.selected img").each(function (index, item) {
								deleteImages += $(item).attr("src")+",";
							});
							deleteImages = deleteImages.replace(/,\s*$/, "");
							document.form.cmd.value="deleteImages";
							document.form.deleteImages.value=deleteImages;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the image(s) you wish to delete");
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				function fnProductTypeChange(elem){
					if(elem.value > 0) {
						$("#productTypeCost input").prop("disabled", false);
						document.getElementById('hamperStock').style.display = "block";
						document.getElementById('stockOptions').style.display = "none";
					} else {
						$("#productTypeCost input").prop("disabled", true);
						document.getElementById('hamperStock').style.display = "none";
						document.getElementById('stockOptions').style.display = "block";
					}
				}
				function jsAddHamperStock(){
					document.form.cmd.value='addHamperStock';
					document.form.submit();
				}
				function jsDeleteHamperStock(hamperStockID){
					
					if(confirm("Are you sure you want to delete this stock item from the hamper?"))
					{
						document.form.hamperStockID.value=hamperStockID;
						document.form.cmd.value='deleteHamperStock';
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				$().ready(function() {
					$("#sortableGalleryImages").on('click', 'li', function (e) {
						if(e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						placeholder: "ui-state-highlight",
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if(!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								var oldOrder = $(item).attr("data-order");
								$("ul#sortableGalleryImages").children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if(parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if(parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["galleryOrder"] = $(item).attr("data-order");
								dataArray[i]["imageLocation"] = /[^/]*$/.exec($(item).children("img").attr("src"))[0];
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["galleryID"] = $("#galleryID").attr("value");
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if(sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.galleryOrder) - parseInt(b.galleryOrder);
								}
								else if(sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.galleryOrder) - parseInt(a.galleryOrder);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_productimagemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				/*	$("#frm_unit").change(function() {
						if($(this).val() == 2) {
							$("#weight-toggle").removeClass("hide")
						} else {
							$("#weight-toggle").addClass("hide")
						}
					});*/
				});
			</script>
			<?php
		
			print("<form action='catalogue-manage-products.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='image' id='image'/>");
				print("<input type='hidden' name='stockGroupID' id='stockGroupID' value='$stockGroupID'/>");
				print("<input type='hidden' name='stockOptionID' id='stockOptionID' value='$stockOptionID'/>");
				print("<input type='hidden' name='hamperStockID' id='hamperStockID' value='$hamperStockID'/>");
				print("<input type='hidden' name='overlayType' id='overlayType' value='$overlayType'/>");
				print("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				switch($strcmd)
				{
					case "addStockGroup":
					case "viewStockGroup":
						
						print("<input type='hidden' name='galleryID' id='galleryID' value='".$stockGroupID."' />");
						
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
								print("<div class='section'>");
									
									if ($strcmd == "viewStockGroup")
									{
										$strdbsql = "SELECT * FROM stock_group_information WHERE recordID = :recordID";
										$stockGroupDetails = query($conn, $strdbsql,"single",["recordID"=>$stockGroupID]);		
										print("<fieldset class='inlineLabels'> <legend>Group Details</legend>");
									}
									elseif ($strcmd == "addStockGroup")
									{
										$stockGroupDetails = array("name" => "","retailPrice" => "","brandID" => "","statusID" => 1,"description" => "");
										print("<fieldset class='inlineLabels'> <legend>Group Details</legend>");
									}
									
									print("<div class='row'>");
										
										//product name
										print("<div class='form-group col-md-6 even-left'>");
											print("<label for='frm_productname' class='control-label'>Name</label>");
											print("<input type='text' class='form-control' id='frm_productname' name='frm_productname' value='".$stockGroupDetails['name']."'>");
										print("</div>");
										
										//product type
										print("<div class='form-group col-md-6 even-right'>");
											print("<label for='frm_productType' class='control-label'>Type</label>");
											print("<select name='frm_productType' id='frm_productType' onChange='fnProductTypeChange(this)' class='form-control'>");
												$productTypeArray = array();
												$productTypeArray[0] = "Standard Product";
												$productTypeArray[1] = "Standard Box";								  
												$productTypeArray[2] = "Office Box";								  
												$productTypeArray[3] = "Recipe Box";								  
												foreach ($productTypeArray AS $key=>$productType)
												{
													$strSelected = "";
													if($key == $stockGroupDetails['productType']) $strSelected = " selected";
													print("<option value='".$key."'".$strSelected.">".$productType."</option>");
												}
											print("</select>");
										print("</div>");
										
									print("</div>");
									
									print("<div class='row'>");

										//product status
										print("<div class='form-group col-md-6 even-left'>");
											print("<label for='frm_status' class='control-label'>Availability</label>");
											print("<select name='frm_status' id='frm_status' class='form-control'>");
												
												$strdbsql = "SELECT * FROM stock_status";
												$stockStatuses = query($conn,$strdbsql,"multi");
												
												$i = 1;
												
												foreach ($stockStatuses AS $stockStatus)
												{
													$strSelected = "";
													if($stockStatus['recordID'] == $stockGroupDetails['statusID']) $strSelected = " selected";
													else if($i == 1) $strSelected = " selected";
													print("<option value='".$stockStatus['recordID']."'".$strSelected.">".$stockStatus['description']."</option>");
													$i++;
												}
												
											print("</select>");
										print("</div>");
									
										//hamper cost										
										print("<div id='productMetaLink'>");
											print("<div class='form-group col-md-6 even-right'>");
												print("<label for='frm_metaLink' class='control-label'>Web Link</label>");
												print("<input type='text' class='form-control' id='frm_metaLink' name='frm_metaLink' value='".$stockGroupDetails['metaLink']."' required>");
											print("</div>");
										print("</div>");
										
									print("</div>");

									//summary
									print("<div class='row'>");
										print("<div class='form-group'>");
											print("<label for='frm_productsummary' class='control-label'>Summary</label>");
											print("<textarea class='form-control' id='frm_productsummary' name='frm_productsummary' rows='5' style='min-width:100%;max-width:100%;resize:none;'>".$stockGroupDetails['summary']."</textarea>");
										print("</div>");
									print("</div>");
									
									//description
									print("<div class='row'>");
										print("<div class='form-group'>");
											print("<label for='frm_productdescription' class='control-label'>Description</label>");
											print("<textarea class='form-control' id='frm_productdescription' name='frm_productdescription' rows='5' style='min-width:100%;max-width:100%;resize:none;'>".$stockGroupDetails['description']."</textarea>");
										print("</div>");
									print("</div>");
									
								print("</div>");
									
								if ($strcmd == "viewStockGroup")
								{
									if($stockGroupDetails['productType'] == 0) {
										$stockOptionsDisplay = "block";
										$hamperStockDisplay = "none";
									} else {
										$stockOptionsDisplay = "none";
										$hamperStockDisplay = "block";
									}
								
									//stock items
									print("<div id='stockOptions' style='display:$stockOptionsDisplay'>");
										print("<div class='section'>");
											print("<fieldset class='inlineLabels'>");
												print("<legend style='position:relative;'>Manage Stock Options <button onclick='return jsaddStockOption();' type='submit' class='btn btn-success circle' style='position:absolute;top:8px;right:16px;'><i class='fa fa-plus' style='color:#FFFFFF;'></i></button></legend>");						
											//	print("<legend style='position:relative;'>Manage Stock Options</legend>");						
									
												$strdbsql = "SELECT stock.*, stock_units.description FROM stock INNER JOIN stock_units ON stock.unit = stock_units.recordID WHERE groupID = :groupID AND `status` = 1 ORDER BY price";
												$strType = "multi";
												$arrdbparams = array("groupID" => $stockGroupID);
												$stockOptions = query($conn, $strdbsql, $strType, $arrdbparams);
												
												if (count($stockOptions) != 0) {
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<table class='table table-striped table-bordered table-hover table-condensed' >");
																print("<tr><thead>");
																	print("<th>Stock Code</th>");
																	print("<th>Unit</th>");
																	print("<th>Price (&pound;)</th>");
																	print("<th style='text-align:center'>Update</th>");
																	print("<th style='text-align:center'>Delete</th>");
																print("</tr></thead><tbody>");
																foreach ($stockOptions as $stockOption) {
																	print("<tr>");
																		print("<td>".$stockOption['stockCode']."</td>");
																		print("<td>".$stockOption['description']."</td>");
																		print("<td>".$stockOption['price']."</td>");
																		print("<td style='width: 83px;text-align:center;'><button onclick='return jseditStockOption(\"".$stockOption['recordID']."\", \"".$stockGroupDetails['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");
																		print("<td style='width: 78px;text-align:center;'><button onclick='return jsdeleteStockOption(\"".$stockOption['recordID']."\", \"".$stockGroupDetails['recordID']."\");' type='submit' class='btn btn-danger circle' style='display:inline-block;'><i class='fa fa-trash'></i></button></td>");
																	print("</tr>");
																}
																print("</tbody>");
															print("</table>");
														print("</div>");
													print("</div>");
												}
									
											print("</fieldset>");
										print("</div>");
									print("</div>");
									
									//hamper products
									print("<div id='hamperStock' style='display:$hamperStockDisplay'>");
										print("<div class='section'>");
											print("<fieldset class='inlineLabels'>");
												print("<legend>Manage Box/Recipe Contents</legend>");	
												
												$strdbsql = "SELECT stock_hamper.recordID AS hamperID, stock.*, stock_group_information.summary FROM stock_hamper 
												INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID 
												INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
												WHERE stockGroupID = :groupID";
												$strType = "multi";
												$arrdbparams = array("groupID"=>$stockGroupID);
												$stockOptions = query($conn, $strdbsql, $strType, $arrdbparams);
									
												if (count($stockOptions) != 0) {
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<table class='table table-striped table-bordered table-hover table-condensed' >");
															print("<tr><thead>");
																print("<th>Stock Code</th>");
																print("<th>Summary</th>");
																print("<th>Price (&pound;)</th>");
																print("<th></th>");
															print("</tr></thead><tbody>");
															foreach ($stockOptions as $stockOption) {
																print("<tr>");
																	print("<td>".$stockOption['stockCode']."</td>");
																	print("<td>".$stockOption['summary']."</td>");
																	print("<td>".$stockOption['price']."</td>");
																	print("<td><button onclick='return jsDeleteHamperStock(\"".$stockOption['hamperID']."\");' type='submit' class='btn btn-danger circle'><i class='fa fa-trash'></i></button></td>");
																print("</tr>");
															}
															print("</tbody>");
														print("</table>");
													print("</div>");
												print("</div>");
												}
									
												print("<div class='row'>");
													print("<div class='form-group'>");
									
													//product status
													print("<div class='row'>");
														print("<div class='col-xs-9' style='padding-right:7.5px;'>");
															print("<select name='frm_hamperStock' id='frm_hamperStock' class='form-control search'>");
																
																$strdbsql = "SELECT stock.*, stock_group_information.name FROM stock 
																INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
																WHERE stock_group_information.productType = 0 AND stock_group_information.enabled = 1 AND `stock`.`status` = 1 AND stock_group_information.recordID <> :product 
																ORDER BY stock.stockCode";
																$stock = query($conn,$strdbsql,"multi",["product"=>$stockGroupID]);								  
																
																foreach($stock AS $stockItem)
																{
																	print("<option value='".$stockItem['recordID']."'>".$stockItem['name']." (&pound;".$stockItem['price'].")</option>");
																}
															print("</select>");
														print("</div>");
														print("<div class='col-xs-3' style='padding-left:7.5px;'>");
															print("<button onclick='return jsAddHamperStock();' type='submit' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
														print("</div>");
													print("</div>");
												print("</div>");
											print("</fieldset>");
										print("</div>");
									print("</div>");
								}
									
							print("</div>");
							print("<div class='col-md-6 split even-right'>");
									
								if ($strcmd == "viewStockGroup")
								{
									$unusedImages = array();
									
									$strdbsql = "SELECT * FROM stock_images WHERE imageTypeID = 1 AND stockID = :stock ORDER BY imageOrder";
									$result = query($conn,$strdbsql,"multi",array("stock"=>$stockGroupID));
									
									foreach($result AS $row) {
										$unusedImages[] = [
											$row['imageLink'],
											$row['imageOrder'],
											$row['recordID']
										];
									}
									
									$assignedImages = "";
									if(count($unusedImages) > 0)
									{
										foreach($unusedImages AS $galleryImage)
										{
											$assignedImages .= "<li id='".$galleryImage[2]."' data-order='".$galleryImage[1]."'>";
												if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/products/main/".$galleryImage[0])) {
													$assignedImages .= "<img src='/images/products/main/".$galleryImage[0]."'>";
												} else if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/main/".$galleryImage[0])) {
													$assignedImages .= "<img src='/images/products/main/".$galleryImage[0]."'>";
												}
											$assignedImages .= "</li>";
											$key = array_search($galleryImage, $unusedImages);
											if($key !== false)
											{
												unset($unusedImages[$key]);
											}
										}
									}
									
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
											print("<legend>Images</legend>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_newimage' class='control-label'>Image</label>");
													print("<div class='row'>");
														print("<div class='col-xs-9' style='padding-right:7.5px;'>");
															print("<input type='file' class='form-control' id='frm_newimage' name='frm_newimage' style='padding:6px 12px;'/>");
															print("<input type='hidden' name='deleteImages' id='deleteImages' />");
														print("</div>");
														print("<div class='col-xs-3' style='padding-left:7.5px;'>");
															print("<button onclick='return jsinsertImage(\"".$pageDetails['image']."\");' type='button' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
															print("<button onclick='return jsdeleteImages();' type='button' class='btn btn-danger circle pull-left' style='margin-left:7.5px;'><i class='fa fa-trash'></i></button>");
														print("</div>");
													print("</div>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<ul id='sortableGalleryImages' style='height:376px;'>");
														print($assignedImages);
													print("</ul>");
												print("</div>");
											print("</div>");
										print("</fieldset>");
									print("</div>");
									
								}
				
							print("</div>");
						print("</div>");
								
						//add / update buttons
						print("<div class='row'>");
							print("<div class='col-md-6' style='text-align:left;'>");
								print("<button style='display:inline-block;' onclick='return jscancel(\"\");' class='btn btn-danger pull-left'>Cancel</button>");
							print("</div>");
							print("<div class='col-md-6' style='text-align:right'>");
								if ($strcmd == "viewStockGroup"){
									print("<button style='display:inline-block;' onclick='return jsupdateStockGroup();' type='submit' class='btn btn-success'>Update</button> ");
								} else if ($strcmd == "addStockGroup") {
									print("<button style='display:inline-block;' onclick='return jsinsertStockGroup();' type='submit' class='btn btn-success'>Add</button> ");
								}
							print("</div>");
						print("</div>");
						
					break;
					
					case "addStockOption":
					case "editStockOption":
					
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
								print("<div class='section'>");
					
									$strdbsql = "SELECT * FROM stock_group_information WHERE recordID = :recordID";
									$strType = "single";
									$arrdbparams = array("recordID" => $stockGroupID);
									$stockGroupDetails = query($conn, $strdbsql, $strType, $arrdbparams);
									
									if ($strcmd == "editStockOption")
									{
										$strdbsql = "SELECT * FROM stock WHERE recordID = :recordID";
										$strType = "single";
										$arrdbparams = array("recordID" => $stockOptionID);
										$stockOptionDetails = query($conn, $strdbsql, $strType, $arrdbparams);
										print("<fieldset class='inlineLabels'> <legend>Change Item Option Details</legend>");
									}
									elseif ($strcmd == "addStockOption")
									{
										$stockOptionDetails = array("stockCode" => "","stockLevels" => 0,"priceControl" => "0.00","price" => "0.00","colourID" => 0,"fabricID" => 0,"dimensionID" => 0,"baseID" => 0,"vatID"=>3,"weight"=>0);
										print("<fieldset class='inlineLabels'> <legend>Add Item Option Details</legend>");
									}
									
									print("<div class='row'>");
									
										// Stockcode
										print("<div class='form-group col-md-6 even-left'>");
											print("<label for='frm_stockcode' class='control-label'>Stock Code</label>");
											print("<input type='text' class='form-control' id='frm_stockcode' name='frm_stockcode' value='".$stockOptionDetails['stockCode']."' />");
										print("</div>");
									  
										// Price
										print("<div class='form-group col-md-6 even-right'>");
											print("<label for='frm_price' class='control-label'>Current Price (&pound;)</label>");
											print("<input type='text' class='form-control' id='frm_price' name='frm_price' value='".$stockOptionDetails['price']."' />");
										print("</div>");
										
									print("</div>");
									print("<div class='row'>");
									  
										// Unit
										$strdbsql = "SELECT * FROM stock_units ORDER BY recordID";
										$stockUnits = query($conn, $strdbsql, "multi");
										
										if(count($stockUnits)) {
											print("<div class='form-group col-md-6 even-left'>");
												print("<label for='frm_unit' class='control-label'>Sale Unit</label>");
												print("<select name='frm_unit' id='frm_unit' class='form-control'>");
												foreach($stockUnits AS $stockUnit)
												{
													print("<option value='".$stockUnit['recordID']."'".($stockUnit['recordID'] == $stockOptionDetails['unit']).">".$stockUnit['description']."</option>");
												}
												print("</select>");
											print("</div>");
										}
									  
										// Weight
										print("<div class='form-group col-md-6 even-right hide'>");
											print("<label for='frm_weight' class='control-label'>Weight</label>");
											print("<input type='text' class='form-control' id='frm_weight' name='frm_weight' value='".$stockOptionDetails['weight']."' />");
										print("</div>");
										

									// VAT rate
										print("<div class='form-group col-md-6 even-right'>");
											print("<label for='frm_vat' class='control-label'>VAT Rate</label>");
											print("<select name='frm_vat' id='frm_vat' class='form-control'>");
											
											$strdbsql = "SELECT * FROM site_vat";
											$strType = "multi";
											$vatRates = query($conn, $strdbsql, $strType);								  
											foreach ($vatRates AS $vatRate)
											{
												$strSelected = "";
												if($vatRate['recordID'] == $stockOptionDetails['vatID']) $strSelected = " selected";
												print("<option value='".$vatRate['recordID']."'".$strSelected.">".$vatRate['description']." - ".number_format($vatRate['amount'],2)."</option>");
											}
											
											print("</select>");
										print("</div>");
									print("</div>");
									
								print("</div>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
						
								print("<div class='row'>");
									print("<div class='form-group col-xs-6' style='text-align:left;padding-left:15px;padding-right:15px;'>");

										//buttons
										print("<button onclick='return jscancel(\"viewStockGroup\");' class='btn btn-danger' style='display:inline-block;'>Cancel</button>");
								
									print("</div>");
									print("<div class='form-group col-xs-6' style='text-align:right;padding-left:15px;padding-right:15px;'>");
							
										if ($strcmd == "editStockOption")
										{
											print("<button onclick='return jsupdateStockOption();' type='submit' class='btn btn-success' style='display:inline-block;'>Update </button> ");
										}
										elseif ($strcmd == "addStockOption")
										{
											print("<button onclick='return jsinsertStockOption();' type='submit' class='btn btn-success' style='display:inline-block;'>Add</button> ");
										} 
								
									print("</div>");
								print("</div>");
						
							print("</div>");
						print("</div>");
					
					break;
					
					default:
						
						$strdbsql = "SELECT GROUP_CONCAT(CONCAT(stock_units.description,' - £',stock.price) ORDER BY stock.price) As `items`, stock.groupID, stock.stockCode, stock_group_information.name AS stockGroup,stock_group_information.metaLink AS link, stock_group_information.productType, stock_group_information.productCost, stock_group_information.enabled 
						FROM stock 
						LEFT JOIN stock_units ON stock.unit = stock_units.recordID
						LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE `stock`.`status` = 1 GROUP BY stock.groupID ORDER BY stock_group_information.name ";
						$result = query($conn,$strdbsql,"multi");
						
						print("<div class='section'>");
							print("<table class='table table-striped table-bordered table-hover table-condensed dataTable'>");
								print("<thead>");
									print("<tr>");
										print("<th style='width:20%;'>Name</th>");
										print("<th style='width:20%;'>Stock Code</th>");
										print("<th style='width:20%;'>Link</th>");
										print("<th style='width:20%;'>Price(s)</th>");
										print("<th style='text-align:center;width:10%;'>Show/Hide</th>");
										print("<th style='text-align:center;width:10%;'>Edit</th>");
									print("</tr>");
								print("<tbody>");
									foreach($result AS $row) {
										print("<tr>");
											print("<td>".$row['stockGroup']."</td>");
											print("<td>".$row['stockCode']."</td>");
											print("<td>".$row['link']."</td>");
											print("<td>");
											$options = explode(',',$row['items']);
											foreach($options As $k=>$o) {
												if($k != 0) print("<br/>");
												print($o);
											}
											print("</td>");
											print("<td style='text-align:center;'>");
												print("<div class='switch' style='display:inline-block;margin:10px 0 0;'>");
													if($row['enabled'] == 0) print("<input id='show-toggle-".$row['groupID']."' class='show-toggle cmn-toggle-round' type='checkbox'>");
													else print("<input id='show-toggle-".$row['groupID']."' class='show-toggle cmn-toggle-round' type='checkbox' checked>");
													print("<label for='show-toggle-".$row['groupID']."'></label>");
												print("</div>");
											print("</td>");
											print("<td style='text-align:center;'><button onclick='jsviewStockGroup(".$row['groupID']."); return false;' class='center btn btn-primary circle' style='display:inline-block;' type='submit'><i class='fa fa-pencil'></i></button></td>");
										print("</tr>");
									}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<button onclick='return jsaddStockGroup();' type='submit' class='btn btn-success'>Add New</button>");
				
					break;
				}
			print("</form>");
		print("</div>");
	print("</div>");
	
	?>
	
	<script>
	$(document).ready(function() {
	
		// validate signup form on keyup and submit
		$("#form").validate({
			rules: {
			},
			messages: {
			}
		});
		
		// Datatable
		$('.dataTable').DataTable({
			"drawCallback": function(settings) {
				$(".show-toggle").click(function(){	
					var idstring = $(this).attr('id');
					if(this.checked) var show = 1;
					else var show = 0;
					var idsplit = idstring.split('-')[2];
					$.ajax({
						type: "GET",
						url: "/admin/includes/ajax_productvisible.php?pageID="+idsplit+"&visible="+show
					});
				});
			}
		});
		
	});
	</script>
		
	<?php
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>