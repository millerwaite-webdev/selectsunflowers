<?php namespace Tests;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 16:18
 */
require("../../classes/database/Brand.php");
require("../../classes/database/Category.php");
require("../../classes/database/Stock.php");

use Database\Brand;
use Database\Category;
use Database\Stock;

class DatabaseTests extends \PHPUnit_Framework_TestCase
{
    #region Brand
    public function testCheckBrand()
    {
        $brandname = 'Halo Dining';
        $result = Brand::checkBrandExists($brandname);
        $this->assertTrue($result);
    }
    #endregion

    #region Categories
    public function testCheckCategories()
    {
        $categoryname = 'Leather Sofas';
        $result = Category::checkCategoryExists($categoryname);
        $this->assertTrue($result);
    }
    #endregion

    #region Product
    public function testCheckProduct()
    {
        // name
        $productname = 'Franklite';
        $result = Stock::checkStockExistsByName($productname);
        //$this->assertTrue($result);
        // sku
        $sku = 'CLE-ANIKA-LARGE-SIDEBOARD';
        $result = Stock::checkStockExistsBySku($sku);
        $this->assertTrue($result);
    }
    #endregion
}