<?php namespace Tests;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 08/07/2015
 * Time: 10:29
 */

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */

require("../../includes/inc_functions.php");
require("../../includes/inc_sitecommon.php");

use Login\LoginMethods;
use Login\SessionObject;

class LoginTest extends \PHPUnit_Framework_TestCase
{
    #region Test Constraints
    protected $companyAddress = '';
    protected $companyPhone = '';
    protected $companyFax = '';
    protected $companyEmail = 'carl@millerwaite.com';
    protected $companyName = 'Stollers';
    protected $strcookiedom = 'www.stollers.co.uk';
    protected $conn;
    protected $sessionobject;
    protected $strcmd;
    #endregion

    public function setUp()
    {
        $this->strcmd = 'login';
        $this->conn = connect();
        // setup session object
        $sessionobject = new SessionObject();
        //get submitted data before we load inc_sitecommon
        if (isset($_REQUEST['cmd'])) {
            $this->strcmd = $_REQUEST['cmd'];
            $sessionobject->stripaddress = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : "Unknown";
            $sessionobject->strusername = (!empty($_REQUEST['username'])) ? $_REQUEST['username'] : "";
            $sessionobject->stremail = (!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "";
            $sessionobject->strpassword = (!empty($_REQUEST['password'])) ? md5($_REQUEST['password']) : md5("");
            $sessionobject->memorable1 = (!empty($_REQUEST['character1'])) ? $_REQUEST['character1'] : "";
            $sessionobject->memorable2 = (!empty($_REQUEST['character2'])) ?  $_REQUEST['character2'] : "";
            $sessionobject->memorable3 =  (!empty($_REQUEST['character3'])) ? $_REQUEST['character3'] : "";
            $sessionobject->memorablePos1 = (!empty($_REQUEST['position1'])) ? $_REQUEST['position1'] : "";
            $sessionobject->memorablePos2 = (!empty($_REQUEST['position2'])) ? $_REQUEST['position2'] : "";
            $sessionobject->memorablePos3 = (!empty($_REQUEST['position3'])) ? $_REQUEST['position3'] : "";
        } else {
            $this->strcmd = "";
        }
    }

    public function testPagePermission()
    {
        $pagetotest = 'index';
        // open connection to database
        $conn = connect();
        pagePermission($pagetotest, $conn);
    }

    public function testLoginMethods()
    {
        $loginmethods = new LoginMethods(
            $this->sessionobject, $this->conn,
            getCompanyDetails(
                $this->companyAddress,
                $this->companyPhone,
                $this->companyFax,
                $this->companyEmail,
                $this->companyName));
        $dotnow = gmmktime(22, 59, 59, date("m", time()), date("d", time())+1, date("Y", time()));
        $strcookiedom = "www.stollers.co.uk";
        $loginmethods->login($dotnow, $strcookiedom);
    }

    public function testForgotUsername()
    {
        $loginmethods = new LoginMethods(
            $this->sessionobject, $this->conn,
            getCompanyDetails(
                $this->companyAddress,
                $this->companyPhone,
                $this->companyFax,
                $this->companyEmail,
                $this->companyName));
        $loginmethods->forgotUsername();
    }

    public function testForgotPassword()
    {
        $loginmethods = new LoginMethods(
            $this->sessionobject, $this->conn,
            getCompanyDetails(
                $this->companyAddress,
                $this->companyPhone,
                $this->companyFax,
                $this->companyEmail,
                $this->companyName));
        $loginmethods->forgotPassword();
    }

    public function testForgotMemorableInfo()
    {
        $loginmethods = new LoginMethods(
            $this->sessionobject, $this->conn,
            getCompanyDetails(
                $this->companyAddress,
                $this->companyPhone,
                $this->companyFax,
                $this->companyEmail,
                $this->companyName));
        $loginmethods->forgotMemorableInfo();
    }
}