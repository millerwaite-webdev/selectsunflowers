<?php namespace Tests;

require("../../specific/feed/data_feed.php");

use Feed\FeedProcessor;

/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 11:11
 */
class FeedTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test/Do Magento feed import
     */
    public function testMagentoProductCSVRead()
    {
        $csvfilename = 'export_allproducts.csv';
        $feedprocessor = new FeedProcessor(
            $csvfilename,
            "C:\\Development\\Edwards\\Files\\edwardsfiles_split~~\\httpdocs\\media\\catalog\\product\\",
            "C:\\Development\\JHBenson\\Website\\images\\products\\");
        $recordcount = $feedprocessor->getProductRecords();
        $this->assertTrue($recordcount > 0);
    }

    public function testMagentoCategoryFeedImport()
    {
        $csvfilename = 'catalog_category_entity.csv';
        $feedprocessor = new FeedProcessor(
            $csvfilename,
            "C:\\Development\\Edwards\\Files\\edwardsfiles_split~~\\httpdocs\\media\\catalog\\product\\",
            "C:\\Development\\JHBenson\\Website\\images\\products\\");
        $feedprocessor->importCategories();
    }

    public function testMagentoProductFeedImport()
    {
        // increase the allocated memory otherwise we'll hit a roof
        ini_set('memory_limit', '1024M');
        $csvfilename = 'catalog_category_entity.csv';
        $feedprocessor = new FeedProcessor(
            $csvfilename,
            "C:\\Development\\Edwards\\Files\\edwardsfiles_split~~\\httpdocs\\media\\catalog\\product\\",
            "C:\\Development\\JHBenson\\Website\\images\\products\\");
        // prerequisites
        $feedprocessor->getCategoryRecords();
        $feedprocessor->getAllObjectNames();
        // do product import
        $csvfilename = 'export_allproducts.csv';
        $feedprocessor->Initialise($csvfilename);
        $feedprocessor->importProducts();
    }

    public function testMagentoCustomerFeedImport()
    {
        // increase the allocated memory otherwise we'll hit a roof
        ini_set('memory_limit', '1024M');
        $csvfilename = 'export_customers.csv';
        $feedprocessor = new FeedProcessor(
            $csvfilename,
            "C:\\Development\\Edwards\\Files\\edwardsfiles_split~~\\httpdocs\\media\\catalog\\product\\",
            "C:\\Development\\JHBenson\\Website\\images\\products\\");
        $feedprocessor->importCustomers();
    }

    public function test360ImageImport()
    {
        ini_set('memory_limit', '1024M');
        $csvfilename = 'export_allproducts.csv';
        $feedprocessor = new FeedProcessor(
            $csvfilename,
            "C:\\Development\\Edwards\\Files\\edwardsfiles_split~~\\httpdocs\\media\\catalog\\product\\",
            "C:\\Development\\JHBenson\\Website\\images\\products\\");
        $feedprocessor->Initialise($csvfilename);
        $feedprocessor->importProducts();
        $feedprocessor->getAllObjectNames();
    }

    public function doCompleteImport()
    {
        $this->testMagentoCategoryFeedImport();
        $this->testMagentoProductFeedImport();
        $this->testMagentoCustomerFeedImport();
    }
}