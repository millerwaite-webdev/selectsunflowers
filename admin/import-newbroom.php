<?php
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "import-newbroom"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	function connectNewBroom($driver = DRIVER, $username = USER, $password = PASS, $host = HOST, $db = "newbroom") {
		try
		{
			// Define new PHP Data Object connection
			$conn = new PDO(''.$driver.':host='.$host.';dbname='.$db, $username, $password);
			// Sets error reporting level
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			// Sets charset
			$conn->exec("set NAMES utf8");
			$conn->exec("SET CHARSET utf8");
			return $conn;
		}
		catch(PDOException $e)
		{
			// If failed, display error
			$strdebug = "ERROR: " . $e->getMessage();
		}
	}
	$connNewBroom = connectNewBroom();
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	function fnAddStock($orderID,$itemSKU,$itemName,$itemOption,$itemQty,$itemPrice,$promocode,$conn){
	
		$strdbsql = "SELECT stock_group_information.*, stock.vatID FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock.stockCode = :stockCode";
		$strType = "single";
		$arrdbparams = array("stockCode"=>$itemSKU);
		$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
		
		if (!empty($resultdata['recordID'])) {
			
			$stockDetailsArray = array();
			//check to see if this is a hamper product				
			if ($resultdata['productType'] == 1) {
				$stockDetailsArray['stockCode'] = $itemSKU;
				$stockDetailsArray['name'] = $itemName." (Hamper)";
				$stockDetailsArray['description'] = $resultdata['description'];
			} else {
				$stockDetailsArray['stockCode'] = $itemSKU;
				$stockDetailsArray['name'] = $itemName;
				$stockDetailsArray['description'] = $resultdata['description'];
			}
			$quantityAdd = ($itemQty > 0) ? $itemQty : 1;
			$discountPerc = 0;
			$priceAdd = $itemPrice;
			$vatSelectAdd = (!empty($resultdata['vatID'])) ? $resultdata['vatID'] : 1;
			
			//insert record into details table
			$strdbsql = "INSERT INTO order_items(orderHeaderID, stockID, stockDetailsArray, quantity, price, discountPerc, vatID, display, hamperOrderID) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price, :discountPerc, :vatID, 1, :hamperOrderID);";
			$strType = "insert";
			$arrdbparams = array(
				"orderHeaderID"=>$orderID, 
				"stockID"=>$resultdata['recordID'], 
				"stockDetailsArray"=>serialize($stockDetailsArray), 
				"quantity"=>$quantityAdd, 
				"price"=>$priceAdd, 
				"discountPerc"=>$discountPerc, 
				"vatID"=>$vatSelectAdd,
				"hamperOrderID"=>0
			);
			$orderInsert = query($conn,$strdbsql,$strType,$arrdbparams);
			
			
			//if hamper then insert the individual products into our order details table
			if ($resultdata['productType'] == 1) {
			
				//update original hamper detail with its own ID
				$strdbsql = "UPDATE order_items SET hamperOrderID = :hamperOrderID WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparams = array(
					"recordID"=>$orderInsert,
					"hamperOrderID"=>$orderInsert
				);
				$orderDetailUpdate = query($conn,$strdbsql,$strType,$arrdbparams);
			
				$strdbsql = "SELECT count(*) AS qtyToAdd, stock_group_information.name, stock_group_information.description, stock.*, stock_status.description AS statusDesc FROM stock_hamper INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_hamper.stockGroupID = :hamperID GROUP BY stockCode";
				$arrType = "multi";
				$strdbparams = array("hamperID"=>$resultdata['recordID']);
				$resultdata2 = query($conn, $strdbsql, $arrType, $strdbparams);
			
				foreach($resultdata2 AS $row2){
				
					$stockDetailsArray = array();
					$stockDetailsArray['stockCode'] = $row2['stockCode'];
					$stockDetailsArray['name'] = $row2['name'];
					$stockDetailsArray['description'] = $row2['description'];
					$vatSelectAdd = (!empty($row2['vatID'])) ? $row2['vatID'] : 1;
					
					//insert record into details table
					$strdbsql = "INSERT INTO order_items(orderHeaderID, stockID, stockDetailsArray, quantity, price, discountPerc, vatID, display, hamperOrderID) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price, :discountPerc, :vatID, 0, :hamperOrderID);";
					$strType = "insert";
					$arrdbparams = array(
						"orderHeaderID"=>$orderID, 
						"stockID"=>$row2['recordID'], 
						"stockDetailsArray"=>serialize($stockDetailsArray), 
						"quantity"=>$row2['qtyToAdd'] * $quantityAdd, 
						"price"=>0, 
						"discountPerc"=>0, 
						"vatID"=>$vatSelectAdd,
						"hamperOrderID"=>$orderInsert
					);
					$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
				}
			}
		}
	}
	function fnMemberAddReturn($strtitle,$strfirstname,$strsurname,$strcompany,$stremail,$strphone,$strmobile,$deliverName,$strbilltitle,$strbillfirstname,$strbillsurname,$strbilladd1,$strbilladd2,$strbilladd3,$strbillTown,$strbillCnty,$strbillCntry,$strbillPstCde,$strdeltitle,$strdelfirstname,$strdelsurname,$strdeladd1,$strdeladd2,$strdeladd3,$strdelTown,$strdelCnty,$strdelCntry,$strdelPstCde,$conn){
		
		//check if member already exists, if yes get id otherwise create new 
		$strsql = "SELECT customer.* FROM customer INNER JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE customer_address.postcode = :billaddpostcode AND customer.surname = :surname";
		$strType = "single";
		$arrdbparams = array(
			"surname" => $strsurname,
			"billaddpostcode" => $strbillPstCde
		);
		$resultdata = query($conn, $strsql, $strType, $arrdbparams);
		if (!empty($resultdata['recordID'])) { //use existing details
			
			$customerAccountID = $resultdata['recordID'];
		
		} else {
			$customerAccountID = ""; //dont return a customer ID if not found in system
			error_log(serialize($arrdbparams));
		}
		/*else { //create new member

			//create account
			$createAccountQuery = "INSERT INTO customer (groupID, username, password, title, firstname, surname, company, telephone, mobile, email) VALUES ('1', :username, :password, :title, :firstname, :surname, :company, :telephone, :mobile, :email)";
			$strType = "insert";
			$arrdbparams = array(
				"username" =>"",
				"password" =>"",
				"title" => $strtitle,
				"firstname" => $strfirstname,
				"surname" => $strsurname,
				"company" => $strcompany,
				"telephone" => $strphone,
				"mobile" => $strmobile,
				"email" => $stremail
			);
			$customerAccountID = query($conn, $createAccountQuery, $strType, $arrdbparams);
			
			if($customerAccountID)
			{	
				//create account with two addresses
				$insertAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Delivery Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
				$strType = "insert";
				$arrdbparams = array(
					"customerID" => $customerAccountID,
					"title" => $strdeltitle,
					"firstname" => $strdelfirstname,
					"surname" => $strdelsurname,
					"add1" => $strdeladd1,
					"add2" => $strdeladd2,
					"add3" => $strdeladd3,
					"town" => $strdelTown,
					"county" => $strdelCnty,
					"postcode" => $strdelPstCde
				);
				$delAddID = query($conn, $insertAddressQuery, $strType, $arrdbparams);
				
				$billAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Billing Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
				$strType = "insert";
				$arrdbparams = array(
					"customerID" => $customerAccountID,
					"title" => $strbilltitle,
					"firstname" => $strbillfirstname,
					"surname" => $strbillsurname,
					"add1" => $strbilladd1,
					"add2" => $strbilladd2,
					"add3" => $strbilladd3,
					"town" => $strbillTown,
					"county" => $strbillCnty,
					"postcode" => $strbillPstCde
				);
				$billAddID = query($conn, $billAddressQuery, $strType, $arrdbparams);
				$values = array(
					"billingadd" => $billAddID,
					"deliveryadd" => $delAddID,
					"custid" => $customerAccountID
				);
				$assignCustomerAddresses = updateUserBillDelAddresses($values, "billanddel", $conn);
			}
		}*/
		
		return $customerAccountID;
	}
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	
	switch($strcmd){
		case "processOrders":
				
			//file information
			$strdbsql = "SELECT `Delivery`.DeliveryID, `Order`.OrderID, `Order`.OrderDate, `Order`.Total, Person.EMailAddress, Person.HomePhone, Person.FaxPhone, Address.AddressLine1, Address.AddressLine2, Address.AddressLine3, Address.PostTown, Address.County, Address.Country, Address.Postcode, Person.TitlePrefix, Person.FirstName, Person.LastName FROM Delivery INNER JOIN `Order` ON Delivery.OrderID = `Order`.OrderID INNER JOIN `Address` ON `Delivery`.AddressID = Address.`AddressID` INNER JOIN Person ON Address.PersonID = Person.PersonID WHERE Delivery.tmp_processed = 0";
			$arrdbparams = array();
			$strType = "multi";
			$newBroomOrders = query($connNewBroom, $strdbsql, $strType, $arrdbparams);
						
			foreach ($newBroomOrders as $row){
				
				
				
				//check external orderID doesnt already existing
				$strdbsql = "SELECT * FROM order_header WHERE externalOrderID = :externalOrderID;";
				$strType = "count";
				$arrdbparams = array("externalOrderID"=>$row['DeliveryID']);
				$orderCheck = query($conn,$strdbsql,$strType,$arrdbparams);
				
				if ($orderCheck == 0){
				
					$externalOrderID = $row['OrderID'];
					$timestamp = $row['OrderDate'];
					$strtitle = $row['TitlePrefix'];
					$strfirstname = $row['FirstName'];
					$strsurname = $row['LastName'];
					$strcompany = "";
					$voucherCode = "";
					$price = $row['Total'];
					$stremail = $row['EMailAddress'];
					$strphone = $row['HomePhone'];
					$strmobile = "";					
					$strfax = $row['FaxPhone'];
					
					$strdeltitle = $row['TitlePrefix'];
					$strdelfirstname = $row['FirstName'];
					$strdelsurname = $row['LastName'];
					$strdeladd1 = $row['AddressLine1'];
					$strdeladd2 = $row['AddressLine2'];
					$strdeladd3 = $row['AddressLine3'];
					$strdelTown = $row['PostTown'];
					$strdelCnty = $row['County'];
					$strdelCntry = $row['Country'];
					$strdelPstCde = $row['Postcode'];
					$strbilltitle = $row['TitlePrefix'];
					$strbillfirstname = $row['FirstName'];
					$strbillsurname = $row['LastName'];
					$strbilladd1 = $row['AddressLine1'];
					$strbilladd2 = $row['AddressLine2'];
					$strbilladd3 = $row['AddressLine3'];
					$strbillTown = $row['PostTown'];
					$strbillCnty = $row['County'];
					$strbillCntry = $row['Country'];
					$strbillPstCde = $row['Postcode'];
					
					
					
					
					$paymentType = "Unknown";
					//$orderStatusDesc = $row['Status'];
					$deliverName = $row['TitlePrefix']." ".$row['FirstName']." ".$row['LastName'];
					$instructions = "";
					$giftMessage = "";
					
					$orderSource = 1; //1 is unknown, 7 is web
					$orderDate = strtotime(str_replace("/","-",$timestamp));
					$dispatchAdd = 1; //1 = on
					$dispatchAddDate = strtotime(str_replace("/","-",$timestamp));
					$orderStatus = 5; //1 = order placed, 2 = processing, 3 = cancelled, 5 = despatched
					$paymentStatusAdd = 1; //1 = unknown
					
					//check if member already exists, if yes get id otherwise create new 
					$customerID = fnMemberAddReturn($strtitle,$strfirstname,$strsurname,$strcompany,$stremail,$strphone,$strmobile,$deliverName,$strbilltitle,$strbillfirstname,$strbillsurname,$strbilladd1,$strbilladd2,$strbilladd3,$strbillTown,$strbillCnty,$strbillCntry,$strbillPstCde,$strdeltitle,$strdelfirstname,$strdelsurname,$strdeladd1,$strdeladd2,$strdeladd3,$strdelTown,$strdelCnty,$strdelCntry,$strdelPstCde,$conn);

					//work out order status based on the 
					/*switch($orderStatusDesc){
						case "Payment Declined":
							$orderStatus = 1; //1 = order placed, 2 = processing, 3 = cancelled, 5 = despatched
							$paymentStatusAdd = 3; //3 = refused
						break;
						case "Paid: awaiting despatch":
							$orderStatus = 2;
							$paymentStatusAdd = 2; //2 = authorised
						break;
						case "Order Completed":
							$orderStatus = 5;
							$paymentStatusAdd = 2; //2 = authorised
						break;
						default:
							$orderStatus = 1; //1 = order placed, 2 = processing, 3 = cancelled, 5 = despatched
							$paymentStatusAdd = 1; //1 = unknown
						break;
					}*/
					

					if (!empty($customerID)) {
						
						$strdbsql = "INSERT INTO order_header (customerID, timestampOrder, addDeliveryTitle, addDeliveryFirstname, addDeliverySurname, addDelivery1, addDelivery2, addDelivery3, addDelivery4, addDelivery5, addDelivery6, addDeliveryPostcode, addDeliveryMessage, addBillingTitle, addBillingFirstname, addBillingSurname, addBilling1, addBilling2, addBilling3, addBilling4, addBilling5, addBilling6, addBillingPostcode, telephone, email, notes, orderStatus, orderSource, giftMessage, dispatch, dispatchDate, externalOrderID) VALUES (:customerID, :timestampOrder, :addDeliveryTitle, :addDeliveryFirstname, :addDeliverySurname, :addDelivery1, :addDelivery2, :addDelivery3, :addDelivery4, :addDelivery5, :addDelivery6, :addDeliveryPostcode, :addDeliveryMessage, :addBillingTitle, :addBillingFirstname, :addBillingSurname, :addBilling1, :addBilling2, :addBilling3, :addBilling4, :addBilling5, :addBilling6, :addBillingPostcode, :telephone, :email, :notes, :orderStatus, :orderSource, :giftMessage, :dispatch, :dispatchDate, :externalOrderID);";
						$strType = "insert";
						$arrdbparams = array(
							"customerID"=>$customerID, 
							"timestampOrder"=>$orderDate, 
							"addDeliveryTitle"=>$strdeltitle, 
							"addDeliveryFirstname"=>$strdelfirstname, 
							"addDeliverySurname"=>$strdelsurname, 
							"addDelivery1"=>$strdeladd1, 
							"addDelivery2"=>$strdeladd2, 
							"addDelivery3"=>$strdeladd3, 
							"addDelivery4"=>$strdelTown, 
							"addDelivery5"=>$strdelCnty, 
							"addDelivery6"=>$strdelCntry, 
							"addDeliveryPostcode"=>$strdelPstCde, 
							"addDeliveryMessage"=>"", 
							"addBillingTitle"=>$strbilltitle, 
							"addBillingFirstname"=>$strbillfirstname, 
							"addBillingSurname"=>$strbillsurname, 
							"addBilling1"=>$strbilladd1, 
							"addBilling2"=>$strbilladd2, 
							"addBilling3"=>$strbilladd3, 
							"addBilling4"=>$strbillTown, 
							"addBilling5"=>$strbillCnty, 
							"addBilling6"=>$strbillCntry, 
							"addBillingPostcode"=>$strbillPstCde, 
							"telephone"=>$strphone, 
							"email"=>$stremail, 
							"notes"=>$instructions, 
							"orderStatus"=>$orderStatus, 
							"orderSource"=>$orderSource,
							"giftMessage"=>$giftMessage,
							"dispatch"=>$dispatchAdd, 
							"dispatchDate"=>$dispatchAddDate,
							"externalOrderID"=>$externalOrderID
						);
						$orderID = query($conn,$strdbsql,$strType,$arrdbparams);

						if (!empty($orderID)){
							
							$strdbsql = "UPDATE `Delivery` SET tmp_processed = 1 WHERE DeliveryID = :DeliveryID";
							$arrdbparams = array("DeliveryID"=>$row['DeliveryID']);
							$strType = "update";
							$newBroomOrders = query($connNewBroom, $strdbsql, $strType, $arrdbparams);
							
							//add payment 
							$paymentDateAdd = strtotime(str_replace("/","-",$timestamp));
							$paymentSourceAdd = $paymentType;
							$paymentAmountAdd = $price;

							//insert record into details table
							$strdbsql = "INSERT INTO order_payment_details(orderHeaderID, transactionSource, transactionTimestamp, transactionAmount, paymentStatus) VALUES (:orderHeaderID, :transactionSource, :transactionTimestamp, :transactionAmount, :paymentStatus);";
							$strType = "insert";
							$arrdbparams = array(
								"orderHeaderID"=>$orderID, 
								"transactionSource"=>$paymentSourceAdd, 
								"transactionTimestamp"=>$paymentDateAdd, 
								"transactionAmount"=>number_format($paymentAmountAdd,2), 
								"paymentStatus"=>$paymentStatusAdd
							);
							$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);

							$strdbsql = "SELECT * FROM DeliveryItem INNER JOIN Delivery ON DeliveryItem.DeliveryID = Delivery.DeliveryID INNER JOIN CatItem ON DeliveryItem.CatID = CatItem.CatID WHERE Delivery.DeliveryID = :DeliveryID";
							$arrdbparams = array("DeliveryID"=>$row['DeliveryID']);
							$strType = "multi";
							$orderStock = query($connNewBroom, $strdbsql, $strType, $arrdbparams);
							
							foreach ($orderStock AS $stockLine){
								
								fnAddStock($orderID,$stockLine['CatItemCode'],$stockLine['DescLong'],"",$stockLine['Quantity'],$stockLine['Cost'],"",$conn);
							
							}
						}
					}
				} else {
					$strdbsql = "UPDATE `Delivery` SET tmp_processed = 1 WHERE DeliveryID = :DeliveryID";
					$arrdbparams = array("DeliveryID"=>$row['DeliveryID']);
					$strType = "update";
					$newBroomOrders = query($connNewBroom, $strdbsql, $strType, $arrdbparams);
				}
			}
			$newBroomOrders = null;
			$strcmd = "";
			$strsuccess = "Successfully processed records";

		break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			//print("<h1>".$configPageDetails['pageTitle']."</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

?>	
<script language='Javascript'>
	function fnCancel() {
		document.getElementById("cmd").value = "";
		document.getElementById("form").submit();
	}
	function fnProcessOrders() {
		document.getElementById("cmd").value = "processOrders";
		document.getElementById("form").submit();
	}
	
</script>
<?php
			print ("<div class='section' style='overflow:auto;'>");
			print ("<form action='import-newbroom.php' class='uniForm' method='post' name='form' id='form'>");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' value='$strcmd'/>");
			
			print ("<label for='frm' class='col-sm-2 control-label'></label><div class='col-sm-10'>");
			print ("<button onclick='fnProcessOrders(); return false;' class='btn btn-success pull-left'>Process Orders</button></div>");


			print ("</form>");
			print("</div>");
		print("</div>");
	print("</div>");

	?>	
	<script language='Javascript' >
		$().ready(function() {
			// validate signup form on keyup and submit
			$("#form").validate({
				errorPlacement: function(error,element) {
					error.insertAfter(element);
				},
				rules: {
				}
			});
		});
	</script>
	<?php
	
// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>