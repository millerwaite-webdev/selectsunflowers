<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * cms-pages.php					                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

//	use function inc\connect;
//use function inc\query;

session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-pages"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['pageRecordID'])) $pageRecordID = $_REQUEST['pageRecordID']; else $pageRecordID = "";
	if (isset($_REQUEST['cmn-toggle'])) $pageToggle = $_REQUEST['cmn-toggle']; else $pageToggle = "0";
	if (isset($_REQUEST['cmn-visible'])) $visible = $_REQUEST['cmn-visible']; else $visible = "0";
	if (isset($_REQUEST['frm_pageparent']) && $_REQUEST['frm_pageparent'] == "Null") $parentPageID = null; else $parentPageID = $_REQUEST['frm_pageparent'];
	
	switch($strcmd)
	{
		case "insertPage":
		case "updatePage":
			
			$arrdbparams = array(
								"pageName" => $_POST['frm_pagename'],
								"metaPageLink" => $_POST['frm_metapagelink'],
								"pageTitle" => $_POST['frm_pagetitle'],
								"metaKeywords" => $_POST['frm_metakeywords'],
								"metaDescription" => $_POST['frm_metadescription'],
								"pageContent" => $_POST['frm_pagecontent'],
								"pageImage" => $_POST['frm_image'],
								"displayInNav" => $pageToggle,
								"parentPageID" =>$parentPageID,
								"visible" =>$visible
							);
			
			if ($strcmd == "insertPage")
			{	
				$strdbsql = "INSERT INTO site_pages (pageName, metaPageLink, pageTitle, metaKeywords, metaDescription, pageContent, pageImage, displayInNav, parentPageID, visible) 
							VALUES (:pageName, :metaPageLink, :pageTitle, :metaKeywords, :metaDescription, :pageContent, :pageImage, :displayInNav, :parentPageID, :visible)";
				$strType = "insert";
			}
			elseif ($strcmd == "updatePage")
			{
				$strdbsql = "UPDATE site_pages SET pageName = :pageName, metaPageLink = :metaPageLink, pageTitle = :pageTitle, metaKeywords = :metaKeywords, metaDescription = :metaDescription, 
							pageContent = :pageContent, pageImage = :pageImage, displayInNav = :displayInNav, parentPageID = :parentPageID, visible = :visible WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $pageRecordID;
				$strType = "update";
				
				if($pageToggle == 0){
					$strunassignedparent = "UPDATE site_pages SET parentPageID = NULL WHERE parentPageID = :pageID";
					$arrparams['pageID'] = $pageRecordID;
					query($conn, $strunassignedparent, $strType, $arrparams);
				}
			}
			
			$updatePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertPage")
			{
				$pageRecordID = $updatePage;
			}
			
			if ($strcmd == "insertPage")
			{
				if ($updatePage > 0)
				{
					$strsuccess = "Page successfully added";
				}
				elseif ($updatePage == 0)
				{
					$strerror = "An error occurred while adding the brand";
				}
			}
			elseif ($strcmd == "updatePage")
			{
				if ($updatePage <= 1)
				{
					$strsuccess = "Page successfully updated";
				}
				elseif ($updatePage > 1)
				{
					$strwarning = "An error may have occurred while updating this page";
				}
			}
			
			$strcmd = "viewPage";
			
		break;
		
		case "deletePage":
			
			$strdbsql = "DELETE FROM site_pages WHERE recordID = :pageRecordID";
			$strType = "delete";
			$arrdbparams = array( "pageRecordID" => $pageRecordID );
			$deletePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
		
		case "addSiteBlock":
			
			$strdbsql = "SELECT COUNT(*) AS max_order FROM site_block_relations WHERE pageID = :pageRecordID AND positionID = :positionID";
			$strType = "single";
			$arrdbparams = array(
								"pageRecordID" => $pageRecordID,
								"positionID" => $_POST['frm_siteblockposition']
							);
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparams);
			$order = $maxOrder['max_order'] + 1;
			
			foreach($_POST['frm_siteblocks'] AS $siteBlock)
			{
				$strdbsql = "INSERT INTO site_block_relations (blockID, positionID, pageID, pageOrder) VALUES (:blockID, :positionID, :pageRecordID, :pageOrder)";
				$strType = "insert";
				$arrdbparams = array( 
									"blockID" => $siteBlock,
									"positionID" => $_POST['frm_siteblockposition'],
									"pageRecordID" => $pageRecordID,
									"pageOrder" => $order
								);
				$insertBlock = query($conn, $strdbsql, $strType, $arrdbparams);
				$order++;
			}
			
			$strcmd = "viewPage";
			
		break;
		
		case "deleteBlocks":
			
			$strdbsql = "DELETE FROM site_block_relations WHERE recordID IN (".$_POST['deleteBlocks'].")";
			$strType = "delete";
			$removeImages = query($conn, $strdbsql, $strType);
			
			$strdbsql = "SELECT recordID FROM site_block_relations WHERE pageID = :pageRecordID AND positionID = :positionID ORDER BY pageOrder";
			$strType = "multi";
			$arrdbparams = array(
							"pageRecordID" => $pageRecordID,
							"positionID" => $_POST['blockPositionID']
						);
			//echo "get products\n";
			$pageBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
			//var_dump($categories);
			
			$i = 1;
			foreach($pageBlocks AS $pageBlock)
			{
				$strdbsql = "UPDATE site_block_relations SET pageOrder = ".$i." WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparam = array(
								"recordID" => $pageBlock['recordID']
							);
				//echo "update final ordering\n";
				query($conn, $strdbsql, $strType, $arrdbparam);
				$i++;
			}
			
			$strcmd = "viewPage";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_newimage']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print("$upfile - saving image<br>");

			if ($_FILES['frm_newimage']['error'] > 0)
			{
				switch ($_FILES['frm_newimage']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				   print("$newfilename $strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_newimage']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_newimage']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_newimage']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_newimage']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strpagepath = $strimguploadpath."pages/";
				
				$strpagedir = $strrootpath.$strpagepath;
				
				$newfile = $strpagedir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				
				unlink($upfile);
				
				$updatePageImageQuery = "UPDATE site_pages SET pageImage = :image WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparams = array (
								"image" => $newfilename,
								"recordID" => $pageRecordID
							);
				$updatePageImage = query($conn, $updatePageImageQuery, $strType, $arrdbparams);

			}
			
			$strsuccess = "New page image added";
			
			$strcmd = "viewPage";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Manage Pages</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddPage() {
					document.form.cmd.value="addPage";
					document.form.submit();
				}
				function jsviewPage(pageRecordID) {
					document.form.cmd.value="viewPage";
					document.form.pageRecordID.value=pageRecordID;
					document.form.submit();
				}
				function jsdeletePage(pageRecordID) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deletePage";
						document.form.pageRecordID.value=pageRecordID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertPage() {
					document.form.cmd.value='insertPage';
					document.form.submit();
				}
				function jsupdatePage() {
					if ($('#form').valid()) {
						document.form.cmd.value='updatePage';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#pageRecordID").attr("value");
								dataArray[i]["type"] = "page";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
				$(document).ready(function(){
					$(".show-toggle").click(function(){
					
						var idstring = $(this).attr('id');
					
						if(this.checked) {
							var show = 1;
						}
						else {
							var show = 0;
						}
						
						var idsplit = idstring.split('-')[2];
						
						$.ajax({
							type: "GET",
							url: "/admin/includes/ajax_pagevisible.php?pageID="+idsplit+"&visible="+show
						});
						
					});
				});
			</script>
			<?php
		
			print("<form action='cms-pages.php' class='uniForm col-sm-12' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='pageRecordID' id='pageRecordID' value='$pageRecordID'/>");
				print("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				switch($strcmd)
				{
					case "viewPage":
					case "addPage":
						
						if ($strcmd == "viewPage")
						{
							$strdbsql = "SELECT * FROM site_pages WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $pageRecordID);
							$pageDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							/*print("<pre>");
							print_r($pageDetails);
							print("</pre>");*/
							
							print("<div class='row'>");
								print("<div class='col-sm-6 crop-sm-right'>");
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Details</legend>");
						}
						elseif ($strcmd == "addPage")
						{
							$pageDetails = array(
												"pageName" => "",
												"pageTitle" => "",
												"pageContent" => "",
												"metaTitle" => "",
												"metaPageLink" => "",
												"metaDescription" => "",
												"metaKeywords" => "",
												"pageImage" => ""
											);
							
							print("<div class='row'>");
								print("<div class='col-sm-6 crop-sm-right'>");
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Details</legend>");
						}
						
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_metapagelink' class='control-label'>Name</label>");
													print("<input type='text' class='form-control' id='frm_metapagelink' name='frm_metapagelink' value='".htmlspecialchars($pageDetails['metaPageLink'], ENT_QUOTES)."'>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_pagename' class='control-label'>Link <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='We recommend you use hyphens to join separated words as an alternative to underscores as it will improve the ranking of your site.'></i></label>");
													print("<input type='text' class='form-control' id='frm_pagename' name='frm_pagename' value='".$pageDetails['pageName']."'>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_image' class='control-label'>Image <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='You can only add one image using this feature. Any other images you wish to add to this page must be done in the CONTENT section.'></i></label>");
													print("<select id='frm_image' name='frm_image' class='form-control' >");
														print("<option value=''>None</option>");
															
															$dh = opendir("../images/pages/");
															
															while (false !== ($image = readdir($dh))) {
																if ($image != "." && $image != "..") {
																	if($image == $pageDetails['pageImage'])
																	{
																		$strselected = " selected";
																	}
																	else
																	{
																		$strselected = "";
																	}
																	print("<option value='".$image."'".$strselected.">".$image."</option>");
																}
															}
															
													print("</select>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_pagecontent' class='control-label'>Content</label>");
													print("<textarea class='form-control tinymce' id='frm_pagecontent' name='frm_pagecontent' rows='5'>".$pageDetails['pageContent']."</textarea>");
												print("</div>");
											print("</div>");
						
										print("</fieldset>");
									print("</div>");
									
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Meta Info</legend>");
									
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_pagetitle' class='control-label'>Meta Title <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='This should range from 50-60 characters.'></i></label>");
													print("<input type='text' class='form-control' id='frm_pagetitle' name='frm_pagetitle' value='".$pageDetails['pageTitle']."'>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_metadescription' class='control-label'>Meta Description <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='This should range from 155-165 characters.'></i></label>");
													print("<textarea class='form-control' id='frm_metadescription' name='frm_metadescription' rows='3'>".$pageDetails['metaDescription']."</textarea>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_metakeywords' class='control-label'>Meta Keywords <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='Separate keywords and phrases with COMMAS (e.g. keyword1, keyword2, keyword3)'></i></label>");
													print("<input type='text' class='form-control' id='frm_metakeywords' name='frm_metakeywords' value='".$pageDetails['metaKeywords']."'>");
												print("</div>");
											print("</div>");
											
										print("</fieldset>");
									print("</div>");
								print("</div>");
											
								print("<div class='col-sm-6 crop-sm-left'>");										
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
											
											print("<legend>Add New Image</legend>");
									
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_newimage' class='control-label'>Image</label>");
													print("<div class='row'>");
														print("<div class='col-xs-9' style='padding-right: 7.5px;'>");
															print("<input type='file' class='form-control' id='frm_newimage' name='frm_newimage' style='padding:6px 12px;'/>");
														print("</div>");
														print("<div class='col-xs-3' style='padding-left: 7.5px;'>");
															print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
														print("</div>");
													print("</div>");
												print("</div>");
											print("</div>");
											
										print("</fieldset>");
									print("</div>");
									
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
											
											print("<legend>Make page visible");
											
											print("<div class='switch pull-right' style='margin:0;'>");
												if($pageDetails['visible'] == 1)
												{
													print("<input id='cmn-visible' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-visible' value='1' checked>");
												}
												else
												{
													print("<input id='cmn-visible' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-visible' value='1'>");
												}
												print("<label for='cmn-visible'></label>");
											print("</div>");
											
											print("</legend>");
											
											print("<legend>Display as parent in menu");
											
											$strdbsql = "SELECT * FROM site_pages WHERE displayInNav = 1 AND parentPageID is NULL"; 
											$arrparams = null;
											if($strcmd != "addPage"){
												$strdbsql .= " AND recordID != :pageID";
												$arrparams = array("pageID" => $pageDetails['recordID']);
											}
											$strType = "multi";
											$links = query($conn, $strdbsql, $strType, $arrparams);
											
											print("<div class='switch pull-right' style='margin:0;'>");
												if($pageDetails['displayInNav'] == 1)
												{
													print("<input id='cmn-toggle' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-toggle' value='1' checked>");
												}
												else
												{
													print("<input id='cmn-toggle' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-toggle' value='1'>");
												}
												print("<label for='cmn-toggle'></label>");
											print("</div>");
											
											print("</legend>");
													
											if($pageDetails['displayInNav'] == 0){
											
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<label for='frm_pageparent' class='control-label'>Page Parent <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='Choosing another existing page will assign this page as a drop-down, with the selected page as the parent element. Selecting NONE will not display this page anywhere in the menu.'></i></label>");
														print("<select id='frm_pageparent' name='frm_pageparent' class='form-control' >");
															
															print("<option value='Null'>None</option>");	
															
															foreach($links AS $nav){
															
															$selected = "";
															
															if($nav['recordID'] == $pageDetails['parentPageID']){
															
															$selected = " selected";
															
															}
															
															print("<option value='".$nav['recordID']."' ".$selected.">".$nav['metaPageLink']."</option>");	

															}	
															
														print("</select>");
													print("</div>");
												print("</div>");
												
											}
											else{
											
												print("<input type='hidden' name='frm_pageparent' value='Null'/>");
											
											}
											
										print("</fieldset>");
									print("</div>");
								
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
									
											print("<legend>Site Blocks</legend>");
												
											$strdbsql = "SELECT * FROM site_block_position";
											$strType = "multi";
											$siteBlockPositions = query($conn, $strdbsql, $strType);
											$siteBlockPositionSelectOptions = "";
											
											print("<div class='row'>");
												
												foreach($siteBlockPositions AS $siteBlockPosition)
												{
													
													print("<div class='form-group col-md-4 blockPos'>");
														print("<label>".$siteBlockPosition['description']."</label>");
														print("<ul class='blockPosList' id='pos-".$siteBlockPosition['recordID']."'>");
															
															$strdbsql = "SELECT * FROM site_blocks INNER JOIN site_block_relations ON site_block_relations.blockID = site_blocks.recordID WHERE site_block_relations.pageID = :pageID AND site_block_relations.positionID = :positionID ORDER BY site_block_relations.pageOrder";
															$strType = "multi";
															$arrdbparams = array(
																				"pageID" => $pageRecordID,
																				"positionID" => $siteBlockPosition['recordID']
																			);
															$siteBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
															
															foreach($siteBlocks AS $siteBlock)
															{
																print("<li id='".$siteBlock['recordID']."' data-order='".$siteBlock['pageOrder']."' data-pos='".$siteBlockPosition['recordID']."'><span>".$siteBlock['description']."</span></li>");
															}
															
														print("</ul>");
													print("</div>");
													
													$siteBlockPositionSelectOptions .= "<option value='".$siteBlockPosition['recordID']."'>".$siteBlockPosition['description']."</option>";
													
												}
												
											print("</div>");
											print("<div class='row'>");
												print("<div class='col-xs-12'>");
													print("<div class='buttons' style='text-align:right;margin-top:0;'>");
														print("<button onclick='return jsdeleteSiteBlocks();' type='button' class='btn btn-danger' style='display:inline-block;'>Delete</button>");
														print("<input type='hidden' name='deleteBlocks' id='deleteBlocks' />");
													print("</div>");
												print("</div>");
											print("</div>");
									
										print("</fieldset>");
									print("</div>");
									
									print("<div class='section col-sm-12'>");
										print("<fieldset class='inlineLabels'>");
											
											print("<legend>Add Blocks to Page</legend>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_siteblocks' class='control-label'>Available Blocks</label>");
													
													$strdbsql = "SELECT * FROM site_blocks ORDER BY description ASC";
													$strType = "multi";
													$siteBlocks = query($conn, $strdbsql, $strType);
													
													print("<select name='frm_siteblocks[]' id='frm_siteblocks' class='form-control valid' multiple style='height:115px;'>");
														
														foreach($siteBlocks AS $siteBlock)
														{
															print("<option value='".$siteBlock['recordID']."'>".$siteBlock['description']."</option>");
														}
														
													print("</select>");
													
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_siteblockposition' class='control-label'>Block Position</label>");
													print("<select name='frm_siteblockposition' id='frm_siteblockposition' class='form-control valid'>");
														print($siteBlockPositionSelectOptions);
													print("</select>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
														print("<button onclick='return jsaddSiteBlock();' type='submit' class='btn btn-success pull-right'>Add</button> ");
												print("</div>");									
											print("</div>");									
											
										print("</fieldset>");
									print("</div>");
									
								print("</div>");
							print("</div>");
								
							print("<div class='row'>");
								print("<div class='col-xs-6' text-align:left;>");
									print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-6' style='text-align:right;'>");
								
									if ($strcmd == "addPage")
									{
									  print("<button onclick='return jsinsertPage();' type='submit' class='btn btn-success' style='display:inline-block;'>Create</button> ");
									}
									elseif ($strcmd == "viewPage")
									{
									  print("<button onclick='return jsupdatePage();' type='submit' class='btn btn-success' style='display:inline-block;'>Save</button> ");
									}
									
								print("</div>");
							print("</div>");
					
					break;
						
					default:
						
						$strdbsql = "SELECT * FROM site_pages";
						$strType = "multi";
						$pages = query($conn, $strdbsql, $strType);
						
						print("<div class='section'>");
							print("<table id='pages-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th width='15%'>Page Name</th>");
									print("<th width='55%' class='media-out'>Page Link</th>");
									print("<th width='10%' style='text-align:center;'>Show/Hide</th>");
									print("<th width='10%' style='text-align:center;'>Update</th>");
									print("<th width='10%' style='text-align:center;'>Delete</th>");
								print("</tr></thead><tbody>");
								foreach($pages AS $page)
								{
									print("<tr>");
										print("<td>".$page['metaPageLink']."</td>");
										print("<td class='media-out'>".$page['pageName']."</td>");
										print("<td style='text-align:center;'>");
											print("<div class='switch' style='display:inline-block;'>");
												if($page['visible'] == 0)
												{
													print("<input id='show-toggle-".$page['recordID']."' class='show-toggle cmn-toggle-round' type='checkbox'>");
												}
												else
												{
													print("<input id='show-toggle-".$page['recordID']."' class='show-toggle cmn-toggle-round' type='checkbox' checked>");
												}
												print("<label for='show-toggle-".$page['recordID']."'></label>");
											print("</div>");
										print("</td>");
										print("<td style='text-align:center;'><button onclick='return jsviewPage(\"".$page['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");
										print("<td style='text-align:center;'><button onclick='return jsdeletePage(\"".$page['recordID']."\");' type='submit' class='btn btn-danger circle' style='display:inline-block;'><i class='fa fa-trash'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<div class='buttons' style='text-align:right;'>");
							print("<button onclick='return jsaddPage();' type='submit' class='btn btn-success' style='display:inline-block;'>Add</button>");					
						print("<div>");
							
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#pages-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>