<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * customer-manage.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "customer-manage"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['userID'])) $userID = $_REQUEST['userID']; else $userID = "";
	
	switch($strcmd)
	{
		case "createMember":
			
			$strname = $_REQUEST['frm_name'];
			$stremail = $_REQUEST['frm_email'];

			if(check_email_address($stremail) || $stremail == "") {
				
				$strdbsql = "INSERT INTO customer_newsletter (name, emailAddress) VALUES (:name, :email)";
				$result = query($conn,$strdbsql,"insert",array("name"=>$strname,"email"=>$stremail));

				if($result) {
					$strsuccess = "Successfully created a new customer.";
					$strcmd = "";

				} else {
					$strerror = "There was a problem creating the customer. Please try again.";
					$strcmd = "addMember";
				}
				
			} else {
				$strerror = "Sorry - Invalid Email Address - please re-enter.";
				$strcmd = "addMember";
			}
			
		break;
		case "updateMember":
			
			$customerID = $_REQUEST['userID'];
			$strname = $_REQUEST['frm_name'];
			$stremail = $_REQUEST['frm_email'];

			if(check_email_address($stremail) || $stremail == "") {
				
				$strdbsql = "UPDATE customer_newsletter SET name = :name, emailAddress = :email WHERE recordID = :customer";
				$result = query($conn,$strdbsql,"update",array("name"=>$strname,"email"=>$stremail,"customer"=>$customerID));

				if($result) {
					$strsuccess = "Successfully updated customer.";
					$strcmd = "";

				} else {
					$strerror = "There was a problem updating the customer. Please try again.";
					$strcmd = "viewMember";
				}
				
			} else {
				$strerror = "Sorry - Invalid Email Address - please re-enter.";
				$strcmd = "viewMember";
			}
			
		break;
		case "removeMember":
			
			$customerID = $_REQUEST['userID'];
			
			$strdbsql = "DELETE FROM customer_newsletter WHERE recordID = :customer";
			$result = query($conn,$strdbsql,"delete",array("customer"=>$customerID));
			
			if($result) {
				$strsuccess = "Successfully removed email address.";
			} else {
				$strerror = "There was an error removing this email address.";
			}
			
			$strcmd = "";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Newsletter Subscriptions</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			
			<script>
			function jsCreateMember() {
				document.form.cmd.value='createMember';
				document.form.submit();
			}
			function jsUpdateMember() {
				document.form.cmd.value='updateMember';
				document.form.submit();
			}
			function jsAddMember() {
				document.form.cmd.value='addMember';
				document.form.submit();
			}
			function jsViewMember(userID) {
				document.form.userID.value = userID;
				document.form.cmd.value = "viewMember";
				document.form.submit();
			}
			function jsRemoveMember(userID){
				var r = confirm("Are you sure you want to delete this customer?");
				if (r == true) {
					document.form.userID.value = userID;
					document.form.cmd.value = "removeMember";
					document.form.submit();
				}
			}
			function jscancel(cmdValue) {
				document.form.userID.value='';
				document.form.cmd.value=cmdValue;
				document.form.submit();
			}				
			</script>
			
			<?php
		
			print("<form action='customer-newsletter.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='userID' id='userID' value='".$userID."'/>");
				
				switch($strcmd)
				{
					case "addMember":
					case "viewMember":

						if($strcmd == "viewMember") {
							$strdbsql = "SELECT * FROM customer_newsletter WHERE recordID = :customer";
							$result = query($conn,$strdbsql,"single",array("customer"=>$userID));
							$name = $result['name'];
							$email = $result['emailAddress'];
						} else {
							$name = $_REQUEST['name'];
							$email = $_REQUEST['emailAddress'];
						}
						
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
								print("<div class='section'>");
						
									print("<fieldset class='inlineLabels'>");
										print("<legend>Customer Details</legend>");
										
										print("<div class='row'>");
											print("<div class='form-group col-sm-12'>");
												print("<label for='frm_name'>Name</label>");
												print("<input class='form-control' id='frm_name' name='frm_name' type='text' placeholder='Name' value='".$name."' />");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group col-sm-12'>");
												print("<label for='frm_email'>Email</label>");
												print("<input class='form-control' id='frm_email' name='frm_email' type='text' placeholder='Email' value='".$email."' />");
											print("</div>");
										print("</div>");

									print("</fieldset>");

								print("</div>");
								
								print("<button onclick='return jsCancel();' class='btn btn-danger pull-left'>Cancel</button>");

								if($strcmd == "viewMember") print("<button onclick='return jsUpdateMember();' class='btn btn-success pull-right'>Save</button>");
								else  print("<button onclick='return jsCreateMember();' class='btn btn-success pull-right'>Add</button>");
							
							print("</div>");
						print("</div>");
						
						break;
						
					default:
				
						$strdbsql = "SELECT * FROM customer_newsletter ORDER BY recordID";
						$result = query($conn,$strdbsql,"multi");
				
						print("<div class='section'>");
							print("<table class='table table-striped table-bordered table-hover table-condensed dataTable'>");
								print("<thead>");
									print("<tr>");;
										print("<th>Name</th>");
										print("<th>Email</th>");
										print("<th>Edit</th>");
										print("<th>Remove</th>");
									print("</tr>");
								print("</thead>");
								print("<tbody>");
									foreach($result AS $row) {
										print("<tr>");
											print("<td>".$row['name']."</td>");
											print("<td>".$row['emailAddress']."</td>");
											print("<td><button onclick='return jsViewMember(\"".$row['recordID']."\")' class='btn btn-primary circle' type='submit'><i class='fa fa-pencil'></i></button></td>");
											print("<td><button onclick='return jsRemoveMember(\"".$row['recordID']."\")' class='btn btn-danger circle' type='submit'><i class='fa fa-trash'></i></button></td>");
										print("</div>");
									}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<button onclick='return jsAddMember();' type='submit' class='btn btn-success'>Add New</button>");
						
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {
			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			$('.dataTable').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>