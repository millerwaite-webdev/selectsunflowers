<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * account.php                                                        * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "account"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	//var_dump($_SESSION);
	
	// ************* Requests processing ****************** //
	//=====================================================//
	if(isset($_REQUEST['command'])) $strcmd = $_REQUEST['command'];
	else $strcmd = "";
	if(isset($_REQUEST['referer'])) $strreferer = $_REQUEST['referer'];
	else $strreferer = "account.php";
	
	if(isset($_GET['error'])) $strerror = "Sorry - your Email Address or Password was not recognised. Please try again.";
	switch ($strcmd)
	{
		case "signin":
			$username = $_REQUEST['username'];
			$password = $_REQUEST['password'];
			
			$basicCustomerDetails = checkUserLogin($username, $password, $conn);
			//var_dump($basicCustomerDetails);
			
			if ($basicCustomerDetails['num_valid'] == 1)
			{
				$intuserhash = $basicCustomerDetails['recordID'];
				setcookie ("stoid", "$intuserhash", time() + 60*60*6, "/", $strcookieurl);
				
				if(is_numeric($intuserhash) && $intuserhash == 1) {
					$_SESSION['userID'] = 1;
				} else {
					if(!empty($basicCustomerDetails['recordID'])) {
						$_SESSION['userID'] = $basicCustomerDetails['recordID'];
						
						if(!empty($basicCustomerDetails['email'])) {
							$_SESSION['email'] = $basicCustomerDetails['email'];
						}
					} else {
						$_SESSION['userID'] = 0;
					}
				}
				//var_dump($_SESSION);
				if ($strreferer != "") { header("Location: ".$strsecurehref.$strreferer); exit; }

			} else {
				$strcmd = "";
				$strerror = "Your username or password appear to be incorrect";
				//if ($strreferer != "") { header("Location: ".$strsecurehref.$strreferer."?error"); exit; }
			}
			break;
		
		case 'logout':
			$intuserid = -1;
			setcookie ("stoid", -1, time() + 60*60*2, "/", $strcookieurl);
			unset($_SESSION['userID']);
			unset($_SESSION['email']);
			$_SESSION['userID'] = 0;
			$strinfo = "Logged out";
			break;
			
		case "deletebasket":
			$inttempbasket = $_POST ['order'];
			deleteBasketDetails($inttempbasket, $conn);
			break;

		case "copybasket":
			$inttempbasket = $_POST ['order'];
			$pastBasketItems = getBasketDetails($inttempbasket, "byheaderid", $conn);
			
			foreach($pastBasketItems AS $pastBasketItem)
			{
				$intProductID = $pastBasketItem['stockID'];
				$intqty = $pastBasketItem['quantity'];
				$ids = array(
					"basketID" => $intbasketid,
					"stockID" => $intProductID
				);
				
				$currentBasketProductDetails = getBasketDetails($ids, "productandbasketid", $conn);
				if(count($currentBasketProductDetails) == 1) {
					
					$values = array(
						"qtytoadd" => $intqty,
						"productid" => $currentBasketProductDetails[0]['stockID'],
						"baskheadid" => $intbasketid
					);
					// var_dump($values);
					updateBasketDetails($values, "addqty", $conn);
				} else {
					// Not sure of equivalent query - assumed matching price in stock on stockID is sufficient
					$stockItemDetails = getStockItemDetails($intProductID, $conn);
					$strdbsql = "SELECT stock_attribute_type.description AS typedesc, stock_attributes.*
					FROM stock_attribute_relations
					INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID
					INNER JOIN stock_attribute_type ON stock_attributes.attributeTypeID = stock_attribute_type.recordID WHERE stock_attribute_relations.stockID = :stockID";
					$arrType = "multi";
					$strdbparams = array("stockID"=>$intProductID);
					$stockAttributes = query($conn, $strdbsql, $arrType, $strdbparams);
					foreach ($stockAttributes AS $attribute) {
						$stockDetails[$attribute['typedesc']] = $attribute;
					}
					$stockDetailsArray = serialize($stockDetails);
					//	print("<!-- $strdbsql2 -->\n");
					if($stockItemDetails && ($stockItemDetails['statusID'] == 3 || $stockItemDetails['statusID'] == 4)) {
						$decprice = $stockItemDetails['price'];
						$values = array(
							"productid" => $intProductID,
							"qtytoadd" => $intqty,
							"baskheadid" => $intbasketid,
							"price" => $decprice,
							"stockDetailsArray" => $stockDetailsArray
						);
						updateBasketDetails($values, "addproduct", $conn);
					}
				}
			}
			
			//set basket variables
			$intnoitems = 0;
			$dectotcost = 0;

			$basketItems = getBasketDetails($intbasketid, "byheaderid", $conn);
			
			if (count($basketItems) > 0)
			{
				foreach($basketItems AS $basketItem)
				{
					$intnoitems = $intnoitems + $basketItem['quantity'];
					$dectotcost = $dectotcost + ($basketItem['quantity'] * $basketItem['price']);

				}
			}			
			break;
		
		case "pwd":
			$strusername = $_REQUEST['username'];

			$userDetails = getUserDetails($strusername, "byemail", $conn);
			if (!empty($userDetails))
			{
				$strpsswd = substr(str_shuffle(MD5(microtime())), 0, 10);
			
				$updateUserPassword = updateUserPassword($strpsswd, $userDetails['recordID'], $conn);
				
				if($updateUserPassword)
				{
					$stremail = $userDetails['email'];

					$strcontents = "Thank you for using the ".$strsitename." website. A request has been made to email your password to you.\n\n";
					$strcontents .= "If you did not make this request, do not be alarmed, your password has only been sent to the registered email address.\n\n";
					$strcontents .= "Your ".$strsitename." Site login details are:-\n\n";
					$strcontents .= "Username: ".$stremail."\n";
					$strcontents .= "Password: ".$strpsswd."\n\n";
					$strcontents .= "If you wish to change your login details please login with the details above then select 'Edit Account' from the Account administration options section on your account home page. When attempting to change your password use the above password in the field marked 'Current Password'.\n\n";
					$strcontents .= "Thank you.";
			//		echo $strcontents;
					mail($stremail, $strsitename." Password Request", $strcontents, "From: ".$companyEmail.PHP_EOL);
			//		mail("test@allaboutspam.com", $strsitename." Password Request", $strcontents, "From: rhian.millerwaite@gmail.com".PHP_EOL);
				
					$strinfo = "Your current login details have been sent to '".$stremail."'";
				}
				else
				{
					$strerror = "An error occurred while updating your password";
				}
			}
			else
			{
				$strerror = "Sorry - Your email address could not be matched to an account in our records";
			}
			break;
	}
	
	$meta = array();
	$meta['metaTitle'] = $stockGroupDetails['metaDoctitle']."My Account";
	$meta['description'] = "Account log in";
	$meta['keywords'] = "account";
	$meta['metaPageLink'] = "account.php";
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	print("<div id='mainContent'>");
	
		if($strerror != "") {
			print("<div class='notification-error'>");
				print("<h3>Error</h3>");
				print("<p>".$strerror."</p>");
			print("</div>");
		}
		if($strinfo != "") {
			print("<div class='notification-info'>");
				print("<h3>Info</h3>");
				print("<p>".$strinfo."</p>");
			print("</div>");
		}
		if ($intuserid <= 1 ) {
?>
			<script type="text/javascript">
				function jsremember () {
					var user = document.logform.username.value;
					if(document.logform.remember.checked) {
						document.cookie="stouser="+user+"; max-age=10000000; path=/;";
					} else {
						document.cookie="stouser=;  max-age=10000000; path=/;";
					}
				}
				
				function password() {
					if (document.logform.username.value.length < 7) {
						alert ("Please enter a valid email address!");
						return false;
					} else {
						document.logform.command.value='pwd';
						document.logform.submit();
					}
				}
				
				function logvalidate() {
					var err = "";
					if (document.logform.username.value.length < 7)
					{
						err += "Please enter a valid email address!";
					}
					if (document.logform.password.value.length < 1)
					{
						if(err.length > 0)
						{
							err += "\n";
						}
						err += "Please enter a password!";
					}
					if(err.length == 0)
					{
						return true;
					}
					else
					{
						alert(err);
						return false;
					}
				}
				
				function logsubmit() {
					jsremember ();
					if (logvalidate()) document.logform.submit();
				}
			</script>
			<div class="textbox" style="margin: 0 0 50px 0; overflow: hidden;<?php if(isset($_GET['new'])) print("display:none;");?>">
				<form action="/account.php" method="post" style="margin: 0;" name="logform" onSubmit="return logvalidate()">
					<!--<div class="logintblbkg" style="width: 100%; text-align:left; padding: 0 0 5px 0;">-->
						<input name="command" value='signin' type='hidden' />
						<h1>Registered Customers</h1>
						<br/>
						<h2>Log in</h2>
						<br/>
						<table>
							<tr>
								<td style='vertical-align: middle;'>
									<label for="username">Email Address :</label>
								</td>
								<td>
									<input name="username" id="username" type="text" class="inputtext" size="49" maxlength="70" value="<?php if(isset($_COOKIE['stouser'])) print($_COOKIE['stouser']); ?>" />
								</td>
							</tr>
							<tr>
								<td style='vertical-align: middle;'>
									<label for="password">Password	:</label>
								</td>
								<td>
									<input name="password" id="password" type="password" class="inputtext" size="19" maxlength="15" value="" />
								</td>
							</tr>
							<tr>
								<td colspan='2'>
									<a class='short_button' onclick='javascript:logsubmit()' >Log in</a>
									<br/>
									<br/>
								</td>
							</tr>
							<tr>
								<td colspan='2'>
									<label for='remember'> 
										<input name="remember" id='remember' type="checkbox" class="inputtext" value="" <?php if(!isset($_COOKIE['stouser']) || $_COOKIE['stouser'] !="") print("checked"); ?> />
										Remember my Email Address?
									</label>
								</td>
							</tr>
						</table>
						<br/>
						<br/>
						<ul style='list-style-type: square;'>
							<li>Forgotten your Password? Fill in your Email Address above and click the Forgotten Password button to have your password sent to you.</li>
						</ul>
						<a class='short_button' href='javascript:password()'>Forgotten Password</a>
						<br/>
						<br/>
						<br/>
						<ul style='list-style-type: square;'>
							<li>New visitor? Use click the button below to create your account.</li>
						</ul>
						<a href='register.php' class='short_button'>Create Account</a>
					<!--</div>-->
				</form>
			</div>
<?php
		}
		else
		{
			// get basic user info
			$userDetails = getUserDetails($intuserhash, "basic", $conn);
			if(!empty($userDetails)) {
				$intbilladdress = $userDetails['defaultBillingAdd'];
				$intdeladdress = $userDetails['defaultDeliveryAdd'];
				
				/*
				// update basket header to assign new owner? Should this be done as part of login?
				$updateBasketHeader = "UPDATE basket_header SET customerID = :userid WHERE fld_counter = :basketid";
				$returnType = "multi";
				$params = array(
					"userid" => $intuserid,
					"basketid" => $intbasketid
				);
				query($conn, $updateBasketHeader, $returnType, $params);*/
			}
?>
			<script type='text/javascript'>
				function jslogout()
				{
					document.regform.command.value = "logout";
					document.regform.submit();
				}
				<?php
					if($intbilladdress == 0 || $intdeladdress == 0) {
				?>
						onload = function() {
							alert("One or more of your addresses is not set. Please check your address before placing an order.");
						}
				<?php
					}
				?>
			</script>
			<div class="textbox">
				<h2>Welcome to <?php echo $meta['title']." - ".$strusername; ?></h2>
				<div class='left-box'>
					<p style='padding:5px'>Use this page to manage your account settings and review your recent orders.</p>
				</div>
				<div class='right-box' style='text-align: right;'>
					<a href='<?php echo $strsiteurl; ?>'  class='short_button'>Continue Shopping</a>
				</div>
				<div style="clear: both;"></div>
				<div class='left-box'>
					<h3>Account Details</h3>
					<div class="headertext" style="padding: 5px;">Account administration options:</div>
					<div class="bodytext" style="clear:both; padding: 5px;">
						<ul>
							<li><a href='editAccount.php'>Change Password</a></li>
							<li><a href='editContact.php'>Edit Account &amp; Contact Details</a></li>
							<li><a href='subscribe.php'>Newsletter subscriptions</a></li>
							<li><a href='manageAddresses.php'>Manage Addresses</a></li>
							<li><a href='delAccount.php'>Delete Account</a></li>
						</ul>
						<a href='javascript:jslogout()' class='short_button'>Log Out</a>
					</div>
				</div>
				<div class='right-box'>
					<h3>Default Addresses</h3>
					<div class="bodytext" style="clear:both; padding: 5px;">
						<b>Delivery:</b><br/>
						<?php
							
							if ($intdeladdress == 0) {
								print("(not set)<br/>");
							} else {
								$delAddressData = getUserDetails($intuserhash, "deladdress", $conn);
								$strname = trim(str_replace ("  ", " ", $delAddressData['title']." ".$delAddressData['firstname']." ".$delAddressData['surname']));
								$straddress = $delAddressData['add1']."<br/>\n";
								$strcountry = $delAddressData['country'];
								$strpostcode = $delAddressData['postcode']."<br/>\n";

								print("$strname<br/>");
								print("$straddress");
								print("$strcountry<br/>");
								print("$strpostcode");
							}
							print("<br/>");
						?>
						<b>Billing:</b><br/>
						<?php

							if ($intbilladdress == 0) {
								print("(not set)<br/>");
							} else {
								$billAddressData = getUserDetails($intuserhash, "billaddress", $conn);
								$strname = trim(str_replace ("  ", " ", $billAddressData['title']." ".$billAddressData['firstname']." ".$billAddressData['surname']));
								$straddress = $billAddressData['add1']."<br/>\n";
								$strcountry = $billAddressData['country'];
								$strpostcode = $billAddressData['postcode']."<br/>\n";


								print("$strname<br/>");
								print("$straddress");
								print("$strcountry<br/>");
								print("$strpostcode");
							}
						?>
						<br/>
						<a href='manageAddresses.php' class='short_button'>Manage Addresses</a>
					</div>
				</div>
				<form name='regform' action='' method='post'>
					<input type='hidden' name='command'/>
				</form>
				<form name='baskform' action='' method='post'>
					<input type='hidden' name='command'/>
					<input type='hidden' name='order' />
				</form>
				<div class="left-box">
					<h3>Saved Baskets</h3>
					<?php
						$intordercounter = 0;
						$blnoddeven = false;

						$previousBaskets = getBasketDetails($intuserid, "recent", $conn);
						
						if(count($previousBaskets) > 0)
						{
							print ("<p style='padding: 5px;'>View your recent activity:</p>");
							print ("<table style='padding: 5px;'>");
								print ("<thead>");
									print ("<tr>");
										print ("<th style='width: 15%;' align='left'>Basket ID</th>");
										print ("<th style='width: 10%;' align='left'>Items</th>");
										print ("<th style='width: 25%;' align='left'>Last Access Date</th>");
										print ("<th style='width: 50%;' align='left' colspan='2'>Options</th>");
									print ("</tr>");
								print ("</thead>");
								print ("<tbody>");

								foreach ($previousBaskets AS $previousBasket)
								{
									if ($blnoddeven)
									{
										$strtdclass = "class='evenrow'";
									}
									else
									{
										$strtdclass = "";
									}
									$blnoddeven = !$blnoddeven;

									print ("<tr>");
										print ("<td $strtdclass>".$previousBasket['recordID']."</td>");
										print ("<td $strtdclass style='text-align:center'>".$previousBasket['itemCount']."</td>");
										print ("<td $strtdclass>".date("jS M Y",$previousBasket['modifiedTimestamp'])."</td>");
										print ("<td $strtdclass><input type='button' class='short_button' value='Delete' onclick='jsdeletebasket(".$previousBasket['recordID'].")'/></td>");
										print ("<td $strdbclass><!--<input type='button' class='short_button' value='Copy contents to Current Basket' onclick='jscopybasket(".$previousBasket['recordID'].")'/>--></td>");
									print ("</tr>");
								}

								print ("</tbody>");
							print ("</table>");
						}
						else
						{
							print ("<p>No available saved baskets</p>");
						}
					?>
				</div>
				<div class="right-box">
					<h3>View your recent orders:</h3>
					<?php
						$completedBaskets = getBasketDetails($intuserid, "completed", $conn);
						
						if(count($completedBaskets) > 0)
						{
						
							print ("<table style='width: 475px; padding: 5px;'>");
							print ("<thead>");
							print ("<tr>");
							print ("<th style='width: 20%;' align='left'>Order Number</th>");
							print ("<th style='width: 15%;' align='left'>Order Date</th>");
							print ("<th style='width: 15%;' align='left'>Amount</th>");
							print ("<th style='width: 15%;' align='left'>Payment Date</th></tr>");
							print ("</thead>");
							print ("<tbody>");


							$intordercounter = 0;
							$blnoddeven = false;

								foreach($completedBaskets AS $completedBasket)
								{
									if ($blnoddeven)
									{
										$strtdclass = "class='evenrow'";
									}
									else
									{
										$strtdclass = "";
									}
									$blnoddeven = !$blnoddeven;

									print ("<tr>");
									print ("<td $strtdclass><a href='order.php?id=".$completedBasket['recordID']."'>".$completedBasket['recordID']."</a></td>");
									print ("<td $strtdclass>".date("jS M Y",$completedBasket['timestampOrder'])."</td>");
									print ("<td $strtdclass>&pound;".number_format($completedBasket['amountTotal'], 2)."</td>");
									if($completedBasket['timestampPayment'] != 0) print ("<td $strtdclass>".date("jS M Y",$completedBasket['timestampPayment'])."</td>");
									else print("<td $strtdclass></td>");
									print ("</tr>");
								}


							print ("</tbody>");
					//		print ("<tfoot>");
					//		print ("<tr>");
					//		print ("<th>Total Outstanding Orders</th>");
					//		print ("<th>&nbsp;</th>");
					//		print ("<th>$intordercounter</th>");
					//		print ("</tr>");
					//		print ("</tfoot>");
							print ("</table>");
						}
						else
						{
							print ("<p>No recent orders placed</p>");
						}
					?>
				</div>
			</div>
<?php
		}
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>