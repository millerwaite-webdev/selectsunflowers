<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * import-files.php                                                * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$datnow = time();
	$strpage = "import-files"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	//the classes that are needed to read excel files
	/*require_once 'lib/PHPExcel.php';
	require_once 'lib/PHPExcel/IOFactory.php';*/
	
	$strview = "";
	$strsuccess = "";
	$strerror = "";
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['importFormatID'])) $importFormatID = $_REQUEST['importFormatID']; else $importFormatID = "";
	if (isset($_REQUEST['fileImportID'])) $fileImportID = $_REQUEST['fileImportID']; else $fileImportID = "";
	if (isset($_REQUEST['fileComment'])) $strfileComment = $_REQUEST['fileComment']; else $strfileComment = "";
	if (isset($_REQUEST['rowID'])) $rowID = $_REQUEST['rowID']; else $rowID = "";
	
	if($strcmd == "deleteRow")
	{
		$strdbsql = "SELECT recordID FROM fileImportData WHERE recordID = :rowID";
		$strdbparams = array("rowID" => $rowID);
		$arrType = "single";
		$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
	
		if (!empty($resultdata['recordID'])) {
			$strdbsql = "DELETE FROM fileImportData WHERE recordID = :rowID";
			$strdbparams = array("rowID" => $rowID);
			$arrType = "delete";
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
			$strsuccess = "Row successfully deleted";
		} else {
			$strerror = ("No file import row found with this ID");
		}
		$strview = "showFileDetails";
		$strcmd = "processFile";
	}
	
	switch($strcmd)
	{
		case "fileUploaded":
		
			//files uploaded
			if ($_FILES['fileToUpload']) {

				$fulldata = array();
				$file = $_FILES['fileToUpload'];
				
				//check to see if the file already exists in the database based on a checksum of size and name
				$original_extension = (false === $pos = strrpos($file["name"], '.')) ? '' : substr($file["name"], $pos);
				$checkSum = md5($file["name"].$file["size"]);					
				$strdbsql = "SELECT * FROM fileImport WHERE fileExtension = :fileExtension AND checkSum = :checkSum";
				$strdbparams = array("fileExtension" => $original_extension, "checkSum" => $checkSum);
				$arrType = "multi";
				$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
				
				if (count($resultdata) == 0){
				
					$directory = "/files/raw/";
					$filename = $directory.fnconvertunixtime($datnow,"H:i-d-m-Y")."_".$file["name"];
			
					//create directory if it does not currently exist
					if (!file_exists($directory)){$makedir = recursive_mkdir($directory);}
					
					//copy file into the raw directory
					$moveResult = copy($file["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$filename);
					if ($moveResult == true) {
					
						$strdbsql = "SELECT configFileImportFormats.*, configFileImportWorksheets.fileDelimiter FROM configFileImportFormats LEFT JOIN configFileImportWorksheets ON configFileImportFormats.importID = configFileImportWorksheets.importID WHERE configFileImportFormats.importID = :importID";
						$strType = "single";
						$arrdbvalues = array("importID" => $importFormatID);
						$configFileImportFormats = query($conn, $strdbsql, $strType, $arrdbvalues);


						/*if (strtolower($original_extension) == '.xls' || strtolower($original_extension) == '.xlsx') {
							
							$objPHPExcel = PHPExcel_IOFactory::load($file['tmp_name']);
														
							//Document Data			
							foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
							
								$worksheetsToImport = explode(",",$configFileImportFormats['worksheetsToImport']);
								if (in_array($objPHPExcel->getIndex($worksheet), $worksheetsToImport)) {
									
									$strdbsql = "INSERT INTO fileImport (filePath, fileExtension, configID, worksheetID, reference, importType, paymentType, checkSum, operator, timestamp, status) VALUES (:filePath, :fileExtension, :configID, :worksheetID, :reference, :importType, :paymentType, :checkSum, :operator, :timestamp, :status)";
									$arrType = "insert";
									$strdbparams = array("filePath" => $filename, "fileExtension" => $original_extension, "configID" =>$importFormatID, "worksheetID"=>$objPHPExcel->getIndex($worksheet), "reference" => $_REQUEST['frm_importreference']."-Worksheet".$objPHPExcel->getIndex($worksheet), "importType" => $configFileImportFormats['importType'], "paymentType" => $configFileImportFormats['filePaymentType'], "checkSum" => $checkSum, "operator" => $_SESSION['operatorID'], "timestamp" => $datnow, "status" => 1);
									$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
									$doccounter = $resultdata;
									
									//resets values
									$highestColumnIndex = 0;
									$worksheetTitle = $worksheet->getTitle();
									$highestRow = $worksheet->getHighestRow(); // e.g. 10
									$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
									$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
									$nrColumns = ord($highestColumn) - 64;
									
									for ($row = 1; $row <= $highestRow; ++ $row) {
									
										for ($col = 0; $col < $highestColumnIndex; ++ $col) {
											$cell = $worksheet->getCellByColumnAndRow($col, $row);
											$val = $cell->getValue();
											$rowdata[$row][$col] = $val;
											if ($val != '') $blank = false;
										}
										
										//insert into temp table
										$strdbsql = "INSERT INTO fileImportData (importID, rowID, dataArray, status) VALUES (:importID, :rowID, :dataArray, :status)";
										$arrType = "insert";
										$strdbparams = array("dataArray" => serialize($rowdata[$row]), "rowID"=>$row, "importID"=>$doccounter, "status"=>1);
										$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
									}
								}
							}
							$strsuccess = "File successfully uploaded - ".$file['name']." to $filename";

						} else*/ if (strtolower($original_extension) == '.txt' || strtolower($original_extension) == '.csv' || strtolower($original_extension) == '.xml') {
						
							$strdbsql = "INSERT INTO fileImport (filePath, fileExtension, configID, worksheetID, reference, importType, paymentType, checkSum, operator, timestamp, status) VALUES (:filePath, :fileExtension, :configID, :worksheetID, :reference, :importType, :paymentType, :checkSum, :operator, :timestamp, :status)";
							$arrType = "insert";
							$strdbparams = array("filePath" => $filename, "fileExtension" => $original_extension, "configID" =>$importFormatID, "worksheetID"=>0, "reference" => $_REQUEST['frm_importreference'], "importType" => $configFileImportFormats['importType'], "paymentType" => $configFileImportFormats['filePaymentType'], "checkSum" => $checkSum, "operator" => $_SESSION['operatorID'], "timestamp" => $datnow, "status" => 1);
							$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
							$doccounter = $resultdata;
						
							//convert xml to csv
							if (strtolower($original_extension) == '.xml') 
							{
								$xml = simplexml_load_file($file["tmp_name"]);
								$f = fopen($_SERVER['DOCUMENT_ROOT']."/files/".$_SESSION['lottery']."/raw/".$file["name"].".csv", 'w');
								createCsv($xml, $f);
								fclose($f);
								$file["tmp_name"] = $_SERVER['DOCUMENT_ROOT']."/files/".$_SESSION['lottery']."/raw/".$file["name"].".csv";
							}

							//csv files
							/*$fileobject = new SplFileObject($file['tmp_name']);
							$filecontrol = $fileobject->getCsvControl();*/
							$highestColumnIndex = 0;
							if ($configFileImportFormats['fileDelimiter'] == "\\t") $strmydelimiter = "\t"; else $strmydelimiter = $configFileImportFormats['fileDelimiter']; 
							
							//check to see if we can open the uploaded file
							if (($handle = fopen($file['tmp_name'], "r")) !== FALSE) {
								
								//set the php option for csv files created using a mac
								ini_set('auto_detect_line_endings',TRUE);
							
								$row = 1;
								// while (($data = fgetcsv($handle, 1000, $filecontrol[0], $filecontrol[1])) !== FALSE) {
								while (($data = fgetcsv($handle, 1000, $strmydelimiter)) !== FALSE) {
												
									$num = count($data);
									if ($num > $highestColumnIndex) $highestColumnIndex = $num;
									for ($c=0; $c < $num; $c++) {
										$fulldata[$row][$c] = $data[$c];
									}
									
									//insert into temp table
									$strdbsql = "INSERT INTO fileImportData (importID, rowID, dataArray, status) VALUES (:importID, :rowID, :dataArray, :status)";
									$arrType = "insert";
									$strdbparams = array("dataArray" => serialize($data), "rowID"=>$row, "importID"=>$doccounter, "status"=>1);
									$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
									$row++;
								}
								fclose($handle);
								$strsuccess = "File successfully uploaded - ".$file['name']." to $filename";
								ini_set('auto_detect_line_endings',FALSE);

							} else {
								$strerror = ("Could not open file ".$file['tmp_name']."");
							}
						} else {
							$strerror = ("File Extension $original_extension not recognised or not supported.");
						}
					} else {
						$strerror = ("Could not move file to the raw data directory ($filename)");
					}
				} else {
					$strerror = ("This file has already been imported");
				}
			} else {
				$strerror = ("No file to upload");
			}
		break;
		case "processFile":
		case "revalidateRow":
		
			//load up file details making sure it hasnt already been processed
			$strdbsql = "SELECT importID, importType FROM fileImport WHERE importID = :fileImportID AND status != 3";
			$strdbparams = array("fileImportID" => $fileImportID);
			$arrType = "single";
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
			
			$importType = $resultdata['importType'];
			if($importType == 2) {
				$rules = array(
							"stockGroupMetaLinkExists",
							"assocStockOrder",
							"assocCatType"
						);
			}
		
			if (!empty($resultdata['importID'])) {
				if($strcmd == "revalidateRow") {
					$strdbsql = "SELECT dataArray FROM fileImportData WHERE recordID = :rowID";
					$strType = "single";
					$arrdbparam = array("rowID" => $rowID);
					$serializedRowData = query($conn, $strdbsql, $strType, $arrdbparam);
					if(!empty($serializedRowData['dataArray'])) {
						$rowData = unserialize($serializedRowData['dataArray']);
						
						if($importType == 1)
						{
							$rules = array();
							if(!empty($rowData[0]) || !empty($rowData[1]) || !empty($rowData[2]) || !empty($rowData[3]) || !empty($rowData[4]) || !empty($rowData[5]) || !empty($rowData[6]) || !empty($rowData[7]) || !empty($rowData[8]) || !empty($rowData[9]))
							{
								array_push(
											$rules,
											"stockGroupMetaLink",
											"groupName",
											"groupDescription",
											"groupRetailPrice",
											"groupMetaDoctitle",
											"groupMetaDescription",
											"groupMetaKeywords",
											"groupStatus",
											"groupBrandMetaLink",
											"groupBrandOrder"
										);
							}
							
							if(!empty($rowData[10]) || !empty($rowData[11]) || !empty($rowData[12]) || !empty($rowData[13]) || !empty($rowData[14]) || !empty($rowData[15]) || !empty($rowData[16]) || !empty($rowData[17]) || !empty($rowData[18]) || !empty($rowData[19]) || !empty($rowData[20]))
							{
								array_push(
											$rules,
											"stockCode",
											"stockPrice",
											"stockPriceControl",
											"stockWidth",
											"stockHeight",
											"stockDepth",
											"stockDiameter",
											"stockDimensionsDescription",
											"stockBase",
											"stockFinish",
											"stockColour",
											"stockFabric"
										);
							}
						}
						elseif($importType == 2)
						{
							if($rowData[3] == "category")
							{
								$rules[] = "assocCatMetaPageLink";
							}
							elseif($rowData[3] == "range")
							{
								$rules[] = "assocCatMetaRangeLink";
							}
							else
							{
								$rules[] = "assocGroupMetaLink";
							}
						}
						
						foreach($rules AS $rule)
						{
							if(isset($_REQUEST['error-'.$rule.'-'.$rowID])) {
								switch($rule)
								{
									case "stockGroupMetaLink":
										$rowData[0] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupName":
										$rowData[1] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupDescription":
										$rowData[2] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupRetailPrice":
										$rowData[3] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupMetaDoctitle":
										$rowData[4] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupMetaDescription":
										$rowData[5] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupMetaKeywords":
										$rowData[6] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupStatus":
										$rowData[7] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupBrandMetaLink":
										$rowData[8] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "groupBrandOrder":
										$rowData[9] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockCode":
										$rowData[10] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockPrice":
										$rowData[11] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockPriceControl":
										$rowData[12] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									
									case "stockWidth":
										$rowData[13] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockHeight":
										$rowData[14] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockDepth":
										$rowData[15] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockDiameter":
										$rowData[16] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockDimensionsDescription":
										$rowData[17] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockBase":
										$rowData[18] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockFinish":
										$rowData[19] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockColour":
										$rowData[20] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "stockFabric":
										$rowData[21] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "assocGroupMetaLink":
										$rowData[1] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "assocStockOrder":
										$rowData[2] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
									case "assocCatType":
										$rowData[3] = $_REQUEST['error-'.$rule.'-'.$rowID];
									break;
								}
							}
						}
						$serializedRowData = serialize($rowData);
						$strdbsql = "UPDATE fileImportData SET dataArray = :serializedData WHERE recordID = :rowID";
						$strType = "update";
						$arrdbparams = array(
											"serializedData" => $serializedRowData,
											"rowID" => $rowID
										);
						$updateRowData = query($conn, $strdbsql, $strType, $arrdbparams);
						if(is_numeric($updateRowData))
						{
							$strsuccess = "Row successfully updated";
						}
						else
						{
							$strerror = "An error occurred while updating this row<br/>";
						}
					} else {
						$strerror = "The relevant row could not be found<br/>";
					}
				}
				unset($_REQUEST);
				
				$strdbsql = "SELECT * FROM fileImportData WHERE importID = :fileImportID";
				$strdbparams = array("fileImportID" => $fileImportID);
				$arrType = "multi";
				$fileImportData = query($conn, $strdbsql, $arrType, $strdbparams);
				
				//loop around each row in the database
				$importRowErrors = array();
				$importRowSuccess = array();
				foreach($fileImportData AS $importDataRow){
					
					$data = unserialize($importDataRow['dataArray']);
					
					$_REQUEST['createdDate'] = $datnow;
					
					switch($importType) {
						case 1:
							
							$rules = array();
							
							if(!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))
							{
								array_push(
										$rules,
										"stockGroupMetaLink",
										"groupName",
										"groupDescription",
										"groupRetailPrice",
										"groupMetaDoctitle",
										"groupMetaDescription",
										"groupMetaKeywords",
										"groupStatus",
										"groupBrandMetaLink",
										"groupBrandOrder"
									);
								$_REQUEST['stockGroupMetaLink'] = $data[0];
								$_REQUEST['groupName'] = $data[1];
								$_REQUEST['groupDescription'] = $data[2];
								$_REQUEST['groupRetailPrice'] = $data[3];
								$_REQUEST['groupMetaDoctitle'] = $data[4];
								$_REQUEST['groupMetaDescription'] = $data[5];
								$_REQUEST['groupMetaKeywords'] = $data[6];
								$_REQUEST['groupStatus'] = $data[7];
								$_REQUEST['groupBrandMetaLink'] = $data[8];
								$_REQUEST['groupBrandOrder'] = $data[9];
							}
							
							if(!empty($data[10]) || !empty($data[11]) || !empty($data[12]) || !empty($data[13]) || !empty($data[14]) || !empty($data[15]) || !empty($data[16]) || !empty($data[17]) || !empty($data[18]) || !empty($data[19]) || !empty($data[20]))
							{
								array_push(
										$rules,
										"stockCode",
										"stockPrice",
										"stockPriceControl",
										"stockWidth",
										"stockHeight",
										"stockDepth",
										"stockDiameter",
										"stockDimensionsDescription",
										"stockBase",
										"stockFinish",
										"stockColour",
										"stockFabric"
									);
								
								$_REQUEST['stockCode'] = $data[10];
								$_REQUEST['stockPrice'] = $data[11];
								$_REQUEST['stockPriceControl'] = $data[12];
								$_REQUEST['stockWidth'] = $data[13];
								$_REQUEST['stockHeight'] = $data[14];
								$_REQUEST['stockDepth'] = $data[15];
								$_REQUEST['stockDiameter'] = $data[16];
								$_REQUEST['stockDimensionsDescription'] = $data[17];
								$_REQUEST['stockBase'] = $data[18];
								$_REQUEST['stockFinish'] = $data[19];
								$_REQUEST['stockColour'] = $data[20];
								$_REQUEST['stockFabric'] = $data[21];
								
								if($_REQUEST['stockWidth'] === NULL)
								{
									$_REQUEST['stockWidth'] = "";
								}
								if($_REQUEST['stockHeight'] === NULL)
								{
									$_REQUEST['stockHeight'] = "";
								}
								if($_REQUEST['stockDepth'] === NULL)
								{
									$_REQUEST['stockDepth'] = "";
								}
								if($_REQUEST['stockDiameter'] === NULL)
								{
									$_REQUEST['stockDiameter'] = "";
								}
								if($_REQUEST['stockDimensionsDescription'] === NULL)
								{
									$_REQUEST['stockDimensionsDescription'] = "";
								}
								
								if($_REQUEST['stockWidth'] != "" || $_REQUEST['stockHeight'] != "" || $_REQUEST['stockDepth'] != "" || $_REQUEST['stockDiameter'] != "" || $_REQUEST['stockDimensionsDescription'] != "")
								{
									$checkDimensionsQuery = "SELECT recordID FROM stock_dimensions WHERE width = :width AND height = :height AND depth = :depth AND diameter = :diameter AND description = :description";
									$strType = "single";
									$arrdbparams = array(
														"width" => $_REQUEST['stockWidth'],
														"height" => $_REQUEST['stockHeight'],
														"depth" => $_REQUEST['stockDepth'],
														"diameter" => $_REQUEST['stockDiameter'],
														"description" => $_REQUEST['stockDimensionsDescription']
													);
									$checkDimensions = query($conn, $checkDimensionsQuery, $strType, $arrdbparams);
									
									if(!empty($checkDimensions))
									{
										$dimensionsID = $checkDimensions['recordID'];
									}
									else
									{
										$insertDimensionsQuery = "INSERT INTO stock_dimensions (width, height, depth, diameter, description) VALUES (:width, :height, :depth, :diameter, :description)";
										$strType = "insert";
										$dimensionsID = query($conn, $insertDimensionsQuery, $strType, $arrdbparams);
									}
								}
								
								if(!empty($_REQUEST['stockBase']))
								{
									$checkBaseQuery = "SELECT recordID FROM stock_attributes WHERE description = :base AND attributeTypeID = :attTypeID";
									$strType = "single";
									$arrdbparams = array(
														"base" => $_REQUEST['stockBase'],
														"attTypeID" => 1
													);
									$checkBase = query($conn, $checkBaseQuery, $strType, $arrdbparams);
									
									if(!empty($checkBase))
									{
										$baseID = $checkBase['recordID'];
									}
									else
									{
										$insertBaseQuery = "INSERT INTO stock_attributes (description, attributeTypeID) VALUES (:base, :attTypeID)";
										$strType = "insert";
										$baseID = query($conn, $insertBaseQuery, $strType, $arrdbparams);
									}
								}
								else
								{
									$baseID = null;
								}
								
								if(!empty($_REQUEST['stockFinish']))
								{
									$checkFinishQuery = "SELECT recordID FROM stock_attributes WHERE description = :finish AND attributeTypeID = :attTypeID";
									$strType = "single";
									$arrdbparams = array(
														"finish" => $_REQUEST['stockFinish'],
														"attTypeID" => 2
													);
									$checkFinish = query($conn, $checkFinishQuery, $strType, $arrdbparams);
									
									if(!empty($checkFinish))
									{
										$finishID = $checkFinish['recordID'];
									}
									else
									{
										$insertFinishQuery = "INSERT INTO stock_attributes (description, attributeTypeID) VALUES (:finish, :attTypeID)";
										$strType = "insert";
										$finishID = query($conn, $insertFinishQuery, $strType, $arrdbparams);
									}
								}
								else
								{
									$finishID = null;
								}
								
								if(!empty($_REQUEST['stockColour']))
								{
									$checkColourQuery = "SELECT recordID FROM stock_attributes WHERE description = :colour AND attributeTypeID = :attTypeID";
									$strType = "single";
									$arrdbparams = array(
														"colour" => $_REQUEST['stockColour'],
														"attTypeID" => 3
													);
									$checkColour = query($conn, $checkColourQuery, $strType, $arrdbparams);
									
									if(!empty($checkColour))
									{
										$colourID = $checkColour['recordID'];
									}
									else
									{
										$insertColourQuery = "INSERT INTO stock_attributes (description, attributeTypeID) VALUES (:colour, :attTypeID)";
										$strType = "insert";
										$colourID = query($conn, $insertColourQuery, $strType, $arrdbparams);
									}
								}
								else
								{
									$colourID = null;
								}
								
								if(!empty($_REQUEST['stockFabric']))
								{
									$checkFabricQuery = "SELECT recordID FROM stock_attributes WHERE description = :fabric AND attributeTypeID = :attTypeID";
									$strType = "single";
									$arrdbparams = array(
														"fabric" => $_REQUEST['stockFabric'],
														"attTypeID" => 4
													);
									$checkFabric = query($conn, $checkFabricQuery, $strType, $arrdbparams);
									
									if(!empty($checkFabric))
									{
										$fabricID = $checkFabric['recordID'];
									}
									else
									{
										$insertFabricQuery = "INSERT INTO stock_attributes (description, attributeTypeID) VALUES (:fabric, :attTypeID)";
										$strType = "insert";
										$fabricID = query($conn, $insertFabricQuery, $strType, $arrdbparams);
									}
								}
								else
								{
									$fabricID = null;
								}
							}
						
						break;
						case 2:
						
							$_REQUEST['stockGroupMetaLink'] = $data[0];
							$_REQUEST['assocGroupMetaLink'] = $data[1];
							$_REQUEST['assocStockOrder'] = $data[2];
							$_REQUEST['assocCatType'] = $data[3];
						
						break;
					}
					
					$importRowErrors[$importDataRow['recordID']] = importDataValidation($rules, $_REQUEST,  $importDataRow['recordID'], $conn);
					
					//if all details are correct add to DB
					if (empty($importRowErrors[$importDataRow['recordID']])) {
						
						switch($importType)
						{
							case 1:
								
							
								if(!empty($data[0]) || !empty($data[1]) || !empty($data[2]) || !empty($data[3]) || !empty($data[4]) || !empty($data[5]) || !empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9]))
								{
									if(!empty($_REQUEST['groupBrandMetaLink']))
									{
										//get brand
										$getBrandID = "SELECT recordID FROM stock_brands WHERE metaPageLink = :brandMetaLink";
										$strType = "single";
										$arrdbparam = array("brandMetaLink" => $_REQUEST['groupBrandMetaLink']);
										$brandInfo = query($conn, $getBrandID, $strType, $arrdbparam);
										$brandID = $brandInfo['recordID'];
										$brandOrder = $_REQUEST['groupBrandOrder'];
									}
									else
									{
										$brandID = null;
										$brandOrder = null;
									}
									
									//get status
									$getStatusID = "SELECT recordID FROM stock_status WHERE description = :statusDescription";
									$strType = "single";
									$arrdbparam = array("statusDescription" => $_REQUEST['groupStatus']);
									$statusID = query($conn, $getStatusID, $strType, $arrdbparam);
									
									//base query
									$strdbsql = "stock_group_information SET name = :name, description = :description, retailPrice = :retailPrice, metaLink = :metaLink, metaDoctitle = :metaDoctitle, metaDescription = :metaDescription, metaKeywords = :metaKeywords, statusID = :statusID, brandID = :brandID, brandOrder = :brandOrder";
									
									//data parameters
									$dataParams = array(
														"name" => $_REQUEST['groupName'],
														"description" => $_REQUEST['groupDescription'],
														"retailPrice" => $_REQUEST['groupRetailPrice'],
														"metaLink" => $_REQUEST['stockGroupMetaLink'],
														"metaDoctitle" => $_REQUEST['groupMetaDoctitle'],
														"metaDescription" => $_REQUEST['groupMetaDescription'],
														"metaKeywords" => $_REQUEST['groupMetaKeywords'],
														"statusID" => $statusID['recordID'],
														"brandID" => $brandID,
														"brandOrder" => $brandOrder
													);
									
									$stockGroupCmd = null;
									
									//check to see if the stock group already exists in the system
									$stockGroupExistsQuery = "SELECT recordID, brandID, brandOrder FROM stock_group_information WHERE metaLink = :groupMetaLink";
									$strType = "single";
									$arrdbparams = array(
														"groupMetaLink" => $_REQUEST['stockGroupMetaLink']
													);
									$stockGroupExists = query($conn, $stockGroupExistsQuery, $strType, $arrdbparams);
									
									if(!empty($stockGroupExists['recordID']))
									{
										$stockGroupCmd = "UPDATE ";
										$strdbsql .= ", dateLastModified  = :dateLastModified";
										$stockGroupID = $stockGroupExists['recordID'];
										$strdbsqlwhere = " WHERE recordID = :stockGroupID";
										$stockGroupStrType = "update";
										$dataParams['stockGroupID'] = $stockGroupID;
										$dataParams['dateLastModified'] = time();
									}
									else
									{
										$stockGroupCmd = "INSERT INTO ";
										$strdbsql .= ", dateCreated  = :dateCreated";
										$strdbsqlwhere = "";
										$stockGroupStrType = "insert";
										$dataParams['dateCreated'] = time();
									}
									
									$strdbsql = $stockGroupCmd.$strdbsql.$strdbsqlwhere;
									//echo "Cmd: ".$stockGroupCmd."<br/>";
									
									if($stockGroupCmd)
									{
										if($strType == "update")
										{
											$stockBrandID = $stockGroupExists['brandID'];
											$stockBrandOrder = $stockGroupExists['brandOrder'];
											
											if(!empty($stockBrandID))
											{
												$updateGroupsInOldBrandQuery = "UPDATE stock_group_information SET brandOrder = brandOrder - 1 WHERE brandID = :brandID AND brandOrder > :brandOrder";
												$strType = "update";
												$arrdbparams = array(
																	"brandID" => $stockBrandID,
																	"brandOrder" => $stockBrandOrder
																);
												$updateGroupsInOldBrand = query($conn, $updateGroupsInOldBrandQuery, $strType, $arrdbparams);
											}
										}
										
										if(!empty($brandID))
										{
											$updateGroupsInNewBrandQuery = "UPDATE stock_group_information SET brandOrder = brandOrder + 1 WHERE brandID = :brandID AND brandOrder >= :brandOrder";
											$strType = "update";
											$arrdbparams = array(
																"brandID" => $brandID,
																"brandOrder" => $brandOrder
															);
											$updateGroupsInNewBrand = query($conn, $updateGroupsInNewBrandQuery, $strType, $arrdbparams);
										}
										
										//insert/update group
										$result = query($conn, $strdbsql, $stockGroupStrType, $dataParams);
										
										if(!is_numeric($result))
										{
											$strerror .= "Error - row ID ".$importDataRow['recordID']." was not successfully marked as processed<br/>";
										}
										else
										{
											$importRowSuccess[$importDataRow['recordID']]['data'] = "<td>".implode("</td><td>",$data)."</td>";
											
											if($stockGroupStrType == "insert")
											{
												$stockGroupID = $result;
											}
										}
									}
									elseif(empty($importRowErrors[$importDataRowID]))
									{
										$strerror .= "Error - could not determine whether stock group for row ID ".$importDataRow['recordID']." should be inserted or updated<br/>";
									}
								}
							
								if(!empty($data[10]) || !empty($data[11]) || !empty($data[12]) || !empty($data[13]) || !empty($data[14]) || !empty($data[15]) || !empty($data[16]) || !empty($data[17]) || !empty($data[18]) || !empty($data[19]) || !empty($data[20]))
								{
									if(!empty($stockGroupID))
									{
										//base query
										$strdbsql = "stock SET groupID = :groupID, stockCode = :stockCode, price = :stockPrice, priceControl = :stockPriceControl, dimensionID = :dimensionID";
										
										//data parameters
										$dataParams = array(
															"groupID" => $stockGroupID,
															"stockCode" => $_REQUEST['stockCode'],
															"stockPrice" => $_REQUEST['stockPrice'],
															"stockPriceControl" => $_REQUEST['stockPriceControl'],
															"dimensionID" => $dimensionsID
														);
										
										$stockCode = null;
										
										//check to see if stock already exists in the system
										$stockExistsQuery = "SELECT recordID FROM stock WHERE stockCode = :stockCode";
										$strType = "single";
										$arrdbparams = array(
															"stockCode" => $_REQUEST['stockCode']
														);
										$stockExists = query($conn, $stockExistsQuery, $strType, $arrdbparams);
										
										if(!empty($stockExists['recordID']))
										{
											$stockCmd = "UPDATE ";
											$stockID = $stockExists['recordID'];
											$strdbsqlwhere = " WHERE recordID = :stockID";
											$stockStrType = "update";
											$dataParams['stockID'] = $stockID;
										}
										else
										{
											$stockCmd = "INSERT INTO ";
											$strdbsqlwhere = "";
											$stockStrType = "insert";
										}
										
										$strdbsql = $stockCmd.$strdbsql.$strdbsqlwhere;
										
										if($stockCmd)
										{
											//insert/update stock
											$result = query($conn, $strdbsql, $stockStrType, $dataParams);
											
											if(!is_numeric($result))
											{
												$strerror .= "Error - row ID ".$importDataRow['recordID']." was not successfully marked as processed<br/>";
											}
											else
											{
												$importRowSuccess[$importDataRow['recordID']]['data'] = "<td>".implode("</td><td>",$data)."</td>";
												
												if($stockStrType == "insert")
												{
													$stockID = $result;
													
													$dirProduct = mkdir("../images/products/".$stockID, 0777, true);
													$dirList = mkdir("../images/products/".$stockID."/list", 0777, true);
													$dirMain = mkdir("../images/products/".$stockID."/main", 0777, true);
													$dirZoom = mkdir("../images/products/".$stockID."/zoom", 0777, true);
												}
											}
											
											//get group options list
											$getGroupOptionsQuery = "SELECT optionsList FROM stock_group_information WHERE recordID = :groupID";
											$strType = "single";
											$arrdbparam = array("groupID" => $stockGroupID);
											$groupOptions = query($conn, $getGroupOptionsQuery, $strType, $arrdbparam);
											if($groupOptions['optionsList'] != "")
											{
												$arrGroupOptions = explode(",", $groupOptions['optionsList']);
											}
											else
											{
												$arrGroupOptions = array();
											}
											
											//set base association appropriately
											$currStockBase = array();
											if($stockStrType == "update")
											{	
												$getStockBase = "SELECT stock_attribute_relations.recordID, stockAttributeID FROM stock_attribute_relations INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID WHERE stock_attribute_relations.stockID = :stockID AND stock_attributes.attributeTypeID = 1";
												$strType = "single";
												$arrdbparam = array("stockID" => $stockID);
												$currStockBase = query($conn, $getStockBase, $strType, $arrdbparam);
												
												if(!empty($currStockBase))
												{
													if($baseID === null)
													{
														$deleteBaseAssocQuery = "DELETE FROM stock_attribute_relations WHERE recordID = :stockAttributeRelationID";
														$strType = "delete";
														$arrdbparams = array(
																			"stockAttributeRelationID" => $currStockBase['recordID']
																		);
														$deleteBaseAssoc = query($conn, $deleteBaseAssocQuery, $strType, $arrdbparams);
														
														$key = array_search(1, $arrGroupOptions);
														if($key !== false) {
															unset($arrGroupOptions[$key]);
														}
													}
													else
													{
														if($currStockBase['stockAttributeID'] != $baseID)
														{
															$updateBaseAssocQuery = "UPDATE stock_attribute_relations SET stockAttributeID = :baseID WHERE recordID = :baseAssocID";
															$strType = "update";
															$arrdbparams = array(
																				"baseID" => $baseID,
																				"baseAssocID" => $currStockBase['recordID']
																			);
															$updateBaseAssoc = query($conn, $updateBaseAssocQuery, $strType, $arrdbparams);

															$key = array_search(1, $arrGroupOptions);
															if($key == false) {
																$arrGroupOptions[] = 1;
															}
														}
													}
												}
											}
											if($stockStrType == "insert" || ($stockStrType == "update" && empty($currStockBase)))
											{
												if($baseID !== null)
												{
													$insertBaseAssocQuery = "INSERT INTO stock_attribute_relations (stockAttributeID, stockID) VALUES (:stockAttributeID, :stockID)";
													$strType = "insert";
													$arrdbparams = array(
																		"stockAttributeID" => $baseID,
																		"stockID" => $stockID
																	);
													$insertBaseAssoc = query($conn, $insertBaseAssocQuery, $strType, $arrdbparams);

													$key = array_search(1, $arrGroupOptions);
													if($key == false) {
														$arrGroupOptions[] = 1;
													}
												}
											}
											
											//set finish association appropriately
											$currStockFinish = array();
											if($stockStrType == "update")
											{	
												$getStockFinish = "SELECT stock_attribute_relations.recordID, stockAttributeID FROM stock_attribute_relations INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID WHERE stock_attribute_relations.stockID = :stockID AND stock_attributes.attributeTypeID = 2";
												$strType = "single";
												$arrdbparam = array("stockID" => $stockID);
												$currStockFinish = query($conn, $getStockFinish, $strType, $arrdbparam);
												
												if(!empty($currStockFinish))
												{
													if($finishID === null)
													{
														$deleteFinishAssocQuery = "DELETE FROM stock_attribute_relations WHERE recordID = :stockAttributeRelationID";
														$strType = "delete";
														$arrdbparams = array(
																			"stockAttributeRelationID" => $currStockFinish['recordID']
																		);
														$deleteFinishAssoc = query($conn, $deleteFinishAssocQuery, $strType, $arrdbparams);
														
														$key = array_search(2, $arrGroupOptions);
														if($key !== false) {
															unset($arrGroupOptions[$key]);
														}
													}
													else
													{
														if($currStockFinish['stockAttributeID'] != $finishID)
														{
															$updateFinishAssocQuery = "UPDATE stock_attribute_relations SET stockAttributeID = :finishID WHERE recordID = :finishAssocID";
															$strType = "update";
															$arrdbparams = array(
																				"finishID" => $finishID,
																				"finishAssocID" => $currStockFinish['recordID']
																			);
															$updateFinishAssoc = query($conn, $updateFinishAssocQuery, $strType, $arrdbparams);

															$key = array_search(2, $arrGroupOptions);
															if($key == false) {
																$arrGroupOptions[] = 2;
															}
														}
													}
												}
											}
											if($stockStrType == "insert" || ($stockStrType == "update" && empty($currStockFinish)))
											{
												if($finishID !== null)
												{
													$insertFinishAssocQuery = "INSERT INTO stock_attribute_relations (stockAttributeID, stockID) VALUES (:stockAttributeID, :stockID)";
													$strType = "insert";
													$arrdbparams = array(
																		"stockAttributeID" => $finishID,
																		"stockID" => $stockID
																	);
													$insertFinishAssoc = query($conn, $insertFinishAssocQuery, $strType, $arrdbparams);

													$key = array_search(2, $arrGroupOptions);
													if($key == false) {
														$arrGroupOptions[] = 2;
													}
												}
											}
											
											//set colour association appropriately
											$currStockColour = array();
											if($stockStrType == "update")
											{	
												$getStockColour = "SELECT stock_attribute_relations.recordID, stockAttributeID FROM stock_attribute_relations INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID WHERE stock_attribute_relations.stockID = :stockID AND stock_attributes.attributeTypeID = 3";
												$strType = "single";
												$arrdbparam = array("stockID" => $stockID);
												$currStockColour = query($conn, $getStockColour, $strType, $arrdbparam);
												
												if(!empty($currStockColour))
												{
													if($colourID === null)
													{
														$deleteColourAssocQuery = "DELETE FROM stock_attribute_relations WHERE recordID = :stockAttributeRelationID";
														$strType = "delete";
														$arrdbparams = array(
																			"stockAttributeRelationID" => $currStockColour['recordID']
																		);
														$deleteColourAssoc = query($conn, $deleteColourAssocQuery, $strType, $arrdbparams);
														
														$key = array_search(3, $arrGroupOptions);
														if($key !== false) {
															unset($arrGroupOptions[$key]);
														}
													}
													else
													{
														if($currStockColour['stockAttributeID'] != $colourID)
														{
															$updateColourAssocQuery = "UPDATE stock_attribute_relations SET stockAttributeID = :colourID WHERE recordID = :colourAssocID";
															$strType = "update";
															$arrdbparams = array(
																				"colourID" => $colourID,
																				"colourAssocID" => $currStockColour['recordID']
																			);
															$updateColourAssoc = query($conn, $updateColourAssocQuery, $strType, $arrdbparams);

															$key = array_search(3, $arrGroupOptions);
															if($key == false) {
																$arrGroupOptions[] = 3;
															}
														}
													}
												}
											}
											if($stockStrType == "insert" || ($stockStrType == "update" && empty($currStockColour)))
											{
												if($colourID !== null)
												{
													$insertColourAssocQuery = "INSERT INTO stock_attribute_relations (stockAttributeID, stockID) VALUES (:stockAttributeID, :stockID)";
													$strType = "insert";
													$arrdbparams = array(
																		"stockAttributeID" => $colourID,
																		"stockID" => $stockID
																	);
													$insertColourAssoc = query($conn, $insertColourAssocQuery, $strType, $arrdbparams);

													$key = array_search(3, $arrGroupOptions);
													if($key == false) {
														$arrGroupOptions[] = 3;
													}
												}
											}
											
											//set fabric association appropriately
											$currStockFabric = array();
											if($stockStrType == "update")
											{	
												$getStockFabric = "SELECT stock_attribute_relations.recordID, stockAttributeID FROM stock_attribute_relations INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID WHERE stock_attribute_relations.stockID = :stockID AND stock_attributes.attributeTypeID = 4";
												$strType = "single";
												$arrdbparam = array("stockID" => $stockID);
												$currStockFabric = query($conn, $getStockFabric, $strType, $arrdbparam);
												
												if(!empty($currStockFabric))
												{
													if($fabricID === null)
													{
														$deleteFabricAssocQuery = "DELETE FROM stock_attribute_relations WHERE recordID = :stockAttributeRelationID";
														$strType = "delete";
														$arrdbparams = array(
																			"stockAttributeRelationID" => $currStockFabric['recordID']
																		);
														$deleteFabricAssoc = query($conn, $deleteFabricAssocQuery, $strType, $arrdbparams);
														
														$key = array_search(4, $arrGroupOptions);
														if($key !== false) {
															unset($arrGroupOptions[$key]);
														}
													}
													else
													{
														if($currStockFabric['stockAttributeID'] != $fabricID)
														{
															$updateFabricAssocQuery = "UPDATE stock_attribute_relations SET stockAttributeID = :fabricID WHERE recordID = :fabricAssocID";
															$strType = "update";
															$arrdbparams = array(
																				"fabricID" => $fabricID,
																				"fabricAssocID" => $currStockFabric['recordID']
																			);
															$updateFabricAssoc = query($conn, $updateFabricAssocQuery, $strType, $arrdbparams);

															$key = array_search(4, $arrGroupOptions);
															if($key == false) {
																$arrGroupOptions[] = 4;
															}
														}
													}
												}
											}
											if($stockStrType == "insert" || ($stockStrType == "update" && empty($currStockFabric)))
											{
												if($fabricID !== null)
												{
													$insertFabricAssocQuery = "INSERT INTO stock_attribute_relations (stockAttributeID, stockID) VALUES (:stockAttributeID, :stockID)";
													$strType = "insert";
													$arrdbparams = array(
																		"stockAttributeID" => $fabricID,
																		"stockID" => $stockID
																	);
													$insertFabricAssoc = query($conn, $insertFabricAssocQuery, $strType, $arrdbparams);

													$key = array_search(4, $arrGroupOptions);
													if($key == false) {
														$arrGroupOptions[] = 4;
													}
												}
											}
											
											sort($arrGroupOptions, SORT_NUMERIC);
											$strGroupOptions = implode(",", $arrGroupOptions);
											
											$updateGroupOptionsQuery = "UPDATE stock_group_information SET optionsList = :optionsList WHERE recordID = :groupID";
											$strType = "update";
											$arrdbparams = array(
																"optionsList" => $strGroupOptions,
																"groupID" => $stockGroupID
															);
											//error_log("Update options query: ".$updateGroupOptionsQuery);
											//error_log("Options: ".$strGroupOptions);
											$updateGroupOptions = query($conn, $updateGroupOptionsQuery, $strType, $arrdbparams);
										}
										elseif(empty($importRowErrors[$importDataRowID]))
										{
											$strerror .= "Error - could not determine whether stock for row ID ".$importDataRow['recordID']." should be inserted or updated<br/>";
										}
									}
								}
							
							break;
							
							case 2:
							
								switch($_REQUEST['assocCatType'])
								{
									case "category":
										
										$strCatType = "category";
										$strCatAssocType = "category";
										
									break;
									
									case "range":
										
										$strCatType = "stock_ranges";
										$strCatAssocType = "range";
										
									break;
								}
								
								$getStockGroupIDQuery = "SELECT recordID FROM stock_group_information WHERE metaLink = :metaLink";
								$strType = "single";
								$arrdbparam = array("metaLink" => $_REQUEST['stockGroupMetaLink']);
								$stockGroupID = query($conn, $getStockGroupIDQuery, $strType, $arrdbparam);
								
								$getCatIDQuery = "SELECT recordID FROM ".$strCatType." WHERE metaPageLink = :metaLink";
								$strType = "single";
								$arrdbparam = array("metaLink" => $_REQUEST['assocGroupMetaLink']);
								$assocID = query($conn, $getCatIDQuery, $strType, $arrdbparam);
								
								$checkAlreadyAssociatedQuery = "SELECT recordID, stockOrder FROM stock_".$strCatAssocType."_relations WHERE stockID = :groupID AND ".$strCatAssocType."ID = :assocID";
								$strType = "single";
								$arrdbparams = array(
													"groupID" => $stockGroupID['recordID'],
													"assocID" => $assocID['recordID']
												);
								$checkAlreadyAssociated = query($conn, $checkAlreadyAssociatedQuery, $strType, $arrdbparams);
								
								if($checkAlreadyAssociated['stockOrder'] != $_REQUEST['assocStockOrder'])
								{
									if(!empty($checkAlreadyAssociated))
									{
										$decreaseAssocCatOrdersQuery = "UPDATE stock_".$strCatAssocType."_relations SET stockOrder = stockOrder - 1 WHERE ".$strCatAssocType."ID = :assocID AND stockOrder > :assocOrder";
										$strType = "update";
										$arrdbparams = array(
															"assocID" => $assocID['recordID'],
															"assocOrder" => $checkAlreadyAssociated['stockOrder']
														);
										$decreaseAssocCatOrders = query($conn, $decreaseAssocCatOrdersQuery, $strType, $arrdbparams);
									}
									$increaseAssocCatOrdersQuery = "UPDATE stock_".$strCatAssocType."_relations SET stockOrder = stockOrder + 1 WHERE ".$strCatAssocType."ID = :assocID AND stockOrder >= :newOrder";
									$strType = "update";
									$arrdbparams = array(
														"assocID" => $assocID['recordID'],
														"newOrder" => $_REQUEST['assocStockOrder']
													);
									$increaseAssocCatOrders = query($conn, $increaseAssocCatOrdersQuery, $strType, $arrdbparams);
									
									if(!empty($checkAlreadyAssociated))
									{
										$updateAssocOrderQuery = "UPDATE stock_".$strCatAssocType."_relations SET stockOrder = :stockOrder WHERE recordID = :assocID";
										$strType = "update";
										$arrdbparams = array(
															"stockOrder" => $_REQUEST['assocStockOrder'],
															"assocID" => $checkAlreadyAssociated['recordID']
														);
										$result = query($conn, $updateAssocOrderQuery, $strType, $arrdbparams);
									}
									else
									{
										$insertAssocOrderQuery = "INSERT INTO stock_".$strCatAssocType."_relations (stockID, ".$strCatAssocType."ID, stockOrder) VALUES (:stockID, :assocID, :stockOrder)";
										$strType = "insert";
										$arrdbparams = array(
															"stockID" => $stockGroupID['recordID'],
															"assocID" => $assocID['recordID'],
															"stockOrder" => $_REQUEST['assocStockOrder']
														);
										$result = query($conn, $insertAssocOrderQuery, $strType, $arrdbparams);
									}
									echo $result."<br/>";
									if(!is_numeric($result))
									{
										$strerror .= "Error - row ID ".$importDataRow['recordID']." was not successfully marked as processed<br/>";
									}
									else
									{
										$importRowSuccess[$importDataRow['recordID']]['data'] = "<td>".implode("</td><td>",$data)."</td>";
									}
								}
							
							break;
						}
					}
				}
				$strview = "showFileDetails";
					
			} else {
				$strerror = ("No file import found with this ID");
			}
		
		break;
		case "completeFile":

			//mark file import as complete
			$strdbsql = "SELECT importID FROM fileImport WHERE importID = :fileImportID";
			$strdbparams = array("fileImportID" => $fileImportID);
			$arrType = "single";
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
		
			if (!empty($resultdata['importID'])) {
				$strdbsql = "UPDATE fileImport SET status = 3 WHERE importID = :fileImportID";
				$strdbparams = array("fileImportID" => $fileImportID);
				$arrType = "update";
				$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
				$strsuccess = "File processed successfully";
			} else {
				$strerror = ("No file import found with this ID");
			}
			
		break;
		case "deleteFile":
			$strdbsql = "SELECT importID FROM fileImport WHERE importID = :fileImportID";
			$strdbparams = array("fileImportID" => $fileImportID);
			$arrType = "single";
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
		
			if (!empty($resultdata['importID'])) {
				$strdbsql = "DELETE FROM fileImport WHERE importID = :fileImportID";
				$strdbparams = array("fileImportID" => $fileImportID);
				$arrType = "delete";
				$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
				$strsuccess = "File successfully deleted";
			} else {
				$strerror = ("No file import found with this ID");
			}
		break;
	}


	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");
		
			print ("<h1>Import File</h1>");

			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

?>	
<script language='Javascript' >
	
	function fnImportFile() {
		$('select[name="frm_importFormatID"]').rules('add', { required: true });
		if ($('#form').valid()) {
			document.getElementById("cmd").value = "fileUploaded";
			var e = document.getElementById("frm_importFormatID");
			var importFormatID = e.options[e.selectedIndex].value;
			document.getElementById("importFormatID").value = importFormatID;
			document.getElementById("form").submit();
		}
	}
	function fnProcessFile() {
		document.getElementById("cmd").value = "processFile";
		document.getElementById("form").submit();
	}
	function fnDeleteFile() {
		document.getElementById("cmd").value = "deleteFile";
		document.getElementById("form").submit();
	}
	function fnRevalidateRow(recordID) {
		document.getElementById("cmd").value = "revalidateRow";
		document.getElementById("rowID").value = recordID;
		document.getElementById("form").submit();
	}
	function fnDeleteRow(recordID) {
		document.getElementById("cmd").value = "deleteRow";
		document.getElementById("rowID").value = recordID;
		document.getElementById("form").submit();
	}
	function fnCancelFile() {
		document.getElementById("cmd").value = "";
		document.getElementById("form").submit();
	}
	function fnCompleteFile() {
		document.getElementById("cmd").value = "completeFile";
		document.getElementById("form").submit();
	}
	
	$().ready(function() {
		// validate signup form on keyup and submit
		$("#form").validate({
			errorPlacement: function(error,element) {
				error.insertAfter(element);
			},
			rules: {
			}
		});
	});

</script>
<?php


			print ("<form action='import-files.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'><!-- IMPORTANT:  FORM's enctype must be 'multipart/form-data' -->");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' value=''/>");
			print ("<input type='hidden' class='nodisable' name='importFormatID' id='importFormatID' value='$importFormatID'/>");
			print ("<div class='cwcontainer'>");
			
				switch($strview) {
				
					case "showFileDetails":
					
						print ("<input type='hidden' class='nodisable' name='fileImportID' id='fileImportID' value='$fileImportID'/>");
						print ("<input type='hidden' class='nodisable' name='rowID' id='rowID' value='$rowID'/>");
						print("<fieldset>");
							print("<legend>Failed Rows</legend>");
							print ("<div style='width:800px;'><table>");
								print ("<table class='table table-striped table-bordered table-hover table-condensed'>");
									print ("<thead>");
										print ("<tr>");
											print ("<th>Reference</th>");
											print ("<th>Error</th>");
											print ("<th>Current Value</th>");
											print ("<th>Revalidate</th>");
											print ("<th>Delete</th>");
										print ("</tr>");
									print ("</thead>");
									print ("<tbody>");
										$boolNoErrs = true;
										foreach($importRowErrors AS $importRowErrorInfo){
											$boolRefCmdsPrinted = false;
											$rowspan = count($importRowErrorInfo);
											foreach($importRowErrorInfo AS $importRowError){
												foreach($importRowError AS $key => $error){
													if(!empty($error))
													{
														$boolNoErrs = false;
														print("<tr>");
															if(!$boolRefCmdsPrinted)
															{
																print("<td rowspan='".$rowspan."' style='vertical-align: middle;'>");
																	switch($importType)
																	{
																		case 1:
																			
																			if($error['stockGroupMetaLink'] != "")
																			{
																				print ($error['stockGroupMetaLink']);
																			}
																			elseif($error['stockCode'] != "")
																			{
																				print ($error['stockCode']);
																			}
																		
																		break;
																		
																		case 2:
																			print ($error['stockGroupMetaLink']);
																		
																		break;
																	}
																
																print ("</td>");
															}
															print("<td>".$error['message']."</td>");
															print("<td>".$error['input']."</td>");
															if(!$boolRefCmdsPrinted)
															{
																print("<td rowspan='".$rowspan."' style='text-align: center; vertical-align: middle;'><a href='#' onclick='fnRevalidateRow($key); return false;' title='Revalidate Row'><img src='/admin/img/icons/proceed.png' /></a></td>");
																print("<td rowspan='".$rowspan."' style='text-align: center; vertical-align: middle;'><a href='#' onclick='fnDeleteRow($key); return false;' title='Delete Row'><img src='/admin/img/icons/cross.png' /></a></td>");
																$boolRefCmdsPrinted = true;
															}
															
														print("</tr>");
													}
												}
											}
										}
										if($boolNoErrs)
										{
											print ("<tr>");
												print("<td colspan='5'>No Failed Rows</td>");
											print ("</tr>");
										}
									print ("</tbody>");
							print ("</table>");
						print("<br/><br/></div></fieldset>");	
						
						print("<div class='col-12' style='padding-bottom: 20px;>");
							print("<input class='btn btn-success' type='button' class='burl' value='Re-Process File' onclick='fnProcessFile(); return false;'>");
						print("</div>");
						
						print("<fieldset>");
							print("<legend>Successful Rows</legend>");
							//var_dump($importRowSuccess);
							print ("<div style='width:100%;'><table class='table table-striped table-bordered table-hover table-condensed'>");
								print ("<thead>");
									print ("<tr>");
										switch($importType)
										{
											case 1:
											
												print ("<th>Stock Group Meta Link</th>");
												print ("<th>Name</th>");
												print ("<th>Description</th>");
												print ("<th>Retail Price</th>");
												print ("<th>Meta Title</th>");
												print ("<th>Meta Description</th>");
												print ("<th>Meta Keywords</th>");
												print ("<th>Status</th>");
												print ("<th>Brand Meta Link</th>");
												print ("<th>Brand Order</th>");
												print ("<th>Stock Code</th>");
												print ("<th>Price</th>");
												print ("<th>Price Control</th>");
												print ("<th>Width</th>");
												print ("<th>Height</th>");
												print ("<th>Depth</th>");
												print ("<th>Diameter</th>");
												print ("<th>Dimension Description</th>");
												print ("<th>Base</th>");
												print ("<th>Finish</th>");
												print ("<th>Colour</th>");
												print ("<th>Fabric</th>");
												
												$colspan = 22;
											
											break;
											
											case 2:
											
												print ("<th>Stock Group Meta Link</th>");
												print ("<th>Association Meta Link</th>");
												print ("<th>Stock Order</th>");
												print ("<th>Association Type</th>");
												
												$colspan = 4;
											
											break;
										}
									print ("</tr>");
								print ("</thead>");
								print ("<tbody>");
								if (count($importRowSuccess) > 0) {
									foreach($importRowSuccess AS $success){
										print ("<tr>");
											print($success['data']);
										print ("</tr>");
									}
								} else {
									print ("<tr>");
										print("<td colspan='".$colspan."'>No Successful Rows</td>");
									print ("</tr>");
								}
								print ("</tbody>");
							print ("</table>");
						print("<br/><br/></div></fieldset>");
						
						print("<div class='col-12'>");
							print("<input style='float:right;' type='button' class='btn btn-success' value='Complete File' onclick='fnCompleteFile(); return false;'>");
							print("<input style='float:left;' type='button' class='btn btn-danger' value='Cancel Process' onclick='fnCancelFile(); return false;'>");
						print("</div>");
					
					break;
					default:
						print("<fieldset>");
							print("<legend>Import CSV Documents</legend>");
							print("<div class='form-group'>");
								print("<label class='control-label col-sm-2' for='fileToUpload'>File to Upload: </label>");
								print("<div class='col-sm-10'>");
									print ("<input style='width: 300px;' type='file' id='fileToUpload' name='fileToUpload'>");
									print ("<br/>");
									print ("<label style='width: 250px;' class='box_label' ><a href='/import/FileExamples/CustomerImport.xls'>View File Example</a></label>");
								print("</div>");
							print ("</div>");
							print("<div class='form-group'>");
								$strdbsql = "SELECT configFileImportFormats.*, configFileImportTypes.description AS fileType FROM configFileImportFormats INNER JOIN configFileImportTypes ON configFileImportFormats.importType = configFileImportTypes.recordID WHERE configFileImportFormats.status = 1 ORDER BY configFileImportTypes.description";
								$arrdbvalues = array();
								$strType = "multi";
								$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);

								print ("<label for='frm_importFormatID' class='col-sm-2 control-label'>Import Format: </label><div class='col-sm-10'>");
								print ("<select name='frm_importFormatID' id='frm_importFormatID' class='form-control' style='width:400px;'>");
								print ("<option value=''>-- Please Select --</option>");
								foreach ($resultdata as $row) {
									if ($importFormatID == $row['importID']) { $strselected = "selected"; } else { $strselected = ""; }
									print ("<option value='".$row['importID']."' $strselected>".$row['fileType']." Import: ".$row['description']."</option>");
								}
								print ("</select></div>");
							print ("</div>");
							print("<div class='form-group'>");
									print("<label class='col-sm-2 control-label' for='frm_importreference'>File Comment: </label><div class='col-sm-10'>");
										print ("<input style='width: 300px;' name='frm_importreference' id='frm_importreference' size='35' maxlength='45' type='text' class='form-control' value='$strfileComment'/>");
										print ("<label style='width: 250px;' class='box_label' >Valid Files: .txt, .csv, <!--.xls, .xlsx, -->xml</label>");
									print("</div>");
							print("</div>");
							print("<div class='form-group'>");
									print("<div class='col-sm-offset-2 col-sm-10'>");
										print ("<button onclick='fnImportFile(); return false;' class='btn btn-success '>Import File</button>");
									print("</div>");
							print("</div>");
						print("</fieldset>");
						print ("<br/>");
						print("<fieldset>");
							print("<legend>Process Imports</legend>");
							print("<div class='colgroup'>");
								print("<div class='col-12'>");
									print("<label class='box_label' for='fileToProcess'>File to Process: </label>");
									print ("<select style='width: 300px;' name='fileImportID' id='fileImportID' class='form-control' >");
										print ("<option value=''>-- Select Import --</option>");
										$strdbsql = "SELECT * FROM fileImport WHERE status != 3";
										$arrtype = "multi";
										$dataparams = array();
										$imports = query($conn, $strdbsql, $arrtype,$dataparams);
										foreach ($imports as $import) {
											print ("<option value='".$import['importID']."'>".$import['reference']." ".fnconvertunixtime($import['timestamp'])."</option>");
										}
									print ("</select>");
								print("</div>");
								print("<div class='col-12'>");
									print ("<br/>");
									print("<button onclick='fnProcessFile(); return false;' class='btn btn-success'>Process File</button>&nbsp;");
									print("<button onclick='fnDeleteFile(); return false;' class='btn btn-danger'>Delete File</button>");
								print("</div>");
							print("</div>");
						print("</fieldset>");
					break;
				}

			print ("</div>");
			print ("</form>");
		print("</div>");
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>