<?php
include ("../includes/inc_sitecommon.php");
$conn = connect(); // Open Connection to database

	if(isset($_POST['command'])) {
		
		$strcommand = $_POST['command'];
		switch ($strcommand) {
			case "ajaxadd":
				
				$strstockcode = $_POST['stockID'];
				$intquantity = $_POST["qty"];
				if ($intquantity == "" || $intquantity < 1) $intquantity = 1;

				$strdbsql = "SELECT stock_group_information.name, stock.*, stock_dimensions.width, stock_dimensions.height, stock_dimensions.depth, stock_dimensions.diameter, stock_dimensions.description FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID LEFT JOIN stock_dimensions ON stock.dimensionID = stock_dimensions.recordID WHERE stock.recordID = :stockID";
				$arrType = "single";
				$strdbparams = array("stockID"=>$strstockcode);
				$stockDetails = query($conn, $strdbsql, $arrType, $strdbparams);
				$decprice = $stockDetails['price'];
				
				$strdbsql = "SELECT stock_attribute_type.description AS typedesc, stock_attributes . *
				FROM stock_attribute_relations
				INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID
				INNER JOIN stock_attribute_type ON stock_attributes.attributeTypeID = stock_attribute_type.recordID WHERE stock_attribute_relations.stockID = :stockID";
				$arrType = "multi";
				$strdbparams = array("stockID"=>$strstockcode);
				$stockAttributes = query($conn, $strdbsql, $arrType, $strdbparams);
				foreach ($stockAttributes AS $attribute) {
					$stockDetails[$attribute['typedesc']] = $attribute;
				}
				$stockDetailsArray = serialize($stockDetails);

				$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basketHeaderID AND stockID = :stockID";
				$arrType = "single";
				$strdbparams = array("basketHeaderID"=>$intbasketid,"stockID"=>$strstockcode);
				$basketDetails = query($conn, $strdbsql, $arrType, $strdbparams);

				if (count($basketDetails['recordID']) == 0)
				{
					$strdbsql = "INSERT INTO basket_items (basketHeaderID, stockID, stockDetailsArray, quantity, price) VALUES (:basketHeaderID, :stockID, :stockDetailsArray, :quantity, :price);";
					$arrType = "insert";
					$strdbparams = array("basketHeaderID"=>$intbasketid,"stockID"=>$strstockcode, "stockDetailsArray"=>$stockDetailsArray, "quantity"=>$intquantity, "price"=>$decprice);
					$insertResult = query($conn, $strdbsql, $arrType, $strdbparams);
					
					$boolbasketpopup = TRUE;
					$strbaskinsert = $strstockcode;
					$intbaskqty = $intquantity;
				}
				else
				{
					$intqty = $basketDetails['quantity'] + $intquantity;
					$strdbsql = "UPDATE basket_items SET quantity = :quantity WHERE recordID = :recordID";
					$arrType = "update";
					$strdbparams = array("quantity"=>$intqty, "recordID"=>$basketDetails['recordID']);
					$updateResult = query($conn, $strdbsql, $arrType, $strdbparams);
					
					$boolbasketpopup = TRUE;
					$strbaskinsert = $strstockcode;
					$intbaskqty = $intqty;
				}
				$strcommand = "";
			break;
		}
	}
	if(isset($_POST['xml'])) {
		//set basket variables
		$intnoitems = 0;
		$dectotcost = 0;
		header ("Content-type: text/xml; charset=utf-8");
		print(utf8_encode ("<?xml version='1.0' encoding='UTF-8' ?>\n"));
		print(utf8_encode ("<basket>\n"));


		if(isset($_POST['multi'])) print(utf8_encode ("<popup><![CDATA[<p><b style='color:#FF6301'>Multiple Items</b><br></p>]]></popup>"));
		else {
			print(utf8_encode ("\t<popup><![CDATA["));
			print(utf8_encode ("<p><b style='color:#FF6301'>".$strname."</b>"));
			if($strcolour!= "") print(utf8_encode ("<br>Colour: ".$strcolour));
			if($strsize!= "") print(utf8_encode ("<br>Size: ".$strsize));
			print(utf8_encode ("</p>"));
			print(utf8_encode ("<p>Price per Item: &pound;".number_format($decprice,2)." Qty: ".$intbaskqty." </p>"));
			print(utf8_encode ("]]></popup>\n"));
		}
		$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basketID";
		$arrType = "multi";
		$strdbparams = array("basketID"=>$intbasketid);
		$basketDetails = query($conn, $strdbsql, $arrType, $strdbparams);
		foreach ($basketDetails AS $detail) {
			$intnoitems = $intnoitems + $detail['quantity'];
			$dectotcost = $dectotcost + ($detail['quantity'] * $detail['price']);
		}
		print(utf8_encode ("\t<header><![CDATA[$intnoitems items = &pound; ".number_format($dectotcost,2)."]]></header>\n"));
		print(utf8_encode ("</basket>"));
		
	} else {

		$intnoitems = 0;
		$dectotcost = 0;
		$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basketID";
		$arrType = "multi";
		$strdbparams = array("basketID"=>$intbasketid);
		$basketDetails = query($conn, $strdbsql, $arrType, $strdbparams);
		
		foreach ($basketDetails AS $detail) {
			$intnoitems = $intnoitems + $detail['quantity'];
			$dectotcost = $dectotcost + ($detail['quantity'] * $detail['price']);
		}
		print("$intnoitems items = &pound; ".number_format($dectotcost,2));
	}
	
$conn = null; // close the database connection after all processing
?>