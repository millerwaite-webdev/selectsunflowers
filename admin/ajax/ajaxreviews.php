<?php
include ("../includes/inc_sitecommon.php");
$conn = connect(); // Open Connection to database

	/*if(isset($_POST['id'])) {
		
		$stockGroupID = $_POST['id'];
		$strdbsql = "SELECT * FROM stock_reviews WHERE stockGroupID=:stockGroupID AND fld_hidden='0' ORDER BY dateReview DESC";
		$strType = "multi";
		$arrdbparams = array("stockGroupID"=>$stockGroupID);
		$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
		
		foreach ($resultdata AS $row) {
			
			print ("<div class='review'>");
			print ("<h4 class='reviewtitle'>".$row['title']."</h4>");
			print ("<p class='reviewcredit'>Reviewed by ".$row['name']." on ".date("jS F Y",$row['dateReview'])."</p>");
			print ("<p>".$row['description']."</p>");
			print ("<p>Rating: ");
			print ("<img src='".$strimageurl."images/ratings/".($row['numStars']*2)."_full.png' alt='".$row['numStars']."/5'/>");
			print ("</p>");
			if($row['response'] != "") {
				print("<div class='response'>");
				print("<p class='reviewcredit'>Responded on ".date("jS F Y",$row['dateResponse'])."</p>");
				print ("<p>".$row['response']."</p>");
				print("</div>");
			}
			print ("</div>");
		}

	}*/
	
	$strcommand = $_POST['command'];
	if ($strcommand == "addreview")
	{
		$strrestock   = strip_tags(str_replace("'","\'",$_POST['revstock']));
		$strrevtitle  = strip_tags(str_replace("'","\'",$_POST['revtitle']));
		$strrevdetail = strip_tags(str_replace("'","\'",$_POST['revdetail']));
		$intrevscore  = strip_tags(str_replace("'","\'",$_POST['revscore']));
		$strrevname   = strip_tags(str_replace("'","\'",$_POST['revname']));
		$strrevemail   = strip_tags(str_replace("'","\'",$_POST['revemail']));

		$strdbsql = "INSERT INTO stock_reviews (stockGroupID, memberID, numStars, description, title, dateReview, reviewerName, reviewerEmail) VALUES (:stockGroupID, :memberID, :numStars, :description, :title, :dateReview, :reviewerName, :reviewerEmail);";
		$strType = "insert";
		$arrdbparams = array(
			"stockGroupID"=>$strrestock,
			"memberID"=>$intuserid,
			"numStars"=>$intrevscore,
			"description"=>$strrevdetail,
			"title"=>$strrevtitle,
			"dateReview"=>$datnow,
			"reviewerName"=>$strrevname,
			"reviewerEmail"=>$strrevemail
		);
		$result = query($conn,$strdbsql,$strType,$arrdbparams);
		
		if($result > 0) print("<h2 class='itemreviewheader' style='margin:0;'>Success</h2><a href='#' id='popupReviewClose' onclick='fnCloseReviewEntry(); return false'>x</a><p>Your review has been submitted and will be posted on the site as soon as it has been checked by our staff.</p>");
		else {
			header ("HTTP/1.0 500 Internal Server Error");
			print("<h2>Error with Submission</h2><p>Unfortunately there has been an error submitting your review. Please try again.</p>");
		}
	}
$conn = null; // close the database connection after all processing
