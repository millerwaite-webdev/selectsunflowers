<?php
	include("../includes/inc_sitecommon.php");

	$conn = connect();
	
	$stockGroupID = $_POST["array"][0]['groupID'];
	unset($_POST["array"][0]);
	$filters = $_POST["array"];
	
	$getStockGroupInformation = "SELECT * FROM stock_group_information WHERE recordID = :groupID";
	$returnType = "single";
	$dataParam = array("groupID" => $stockGroupID);
	$stockGroupDetails = query($conn, $getStockGroupInformation, $returnType, $dataParam);
	
	$groupItemDetails = getStockGroupItems($stockGroupID, $conn);
	
	$getSelectedItemID = "SELECT stock.recordID AS stockID, stock.stockCode FROM stock ";
	$arrdbparams = array( "groupID" => $stockGroupID );
	
	
	$boolUnsetFilter = false;
	if(array_key_exists("dimensionID", $filters[1]))
	{
		$dimensionWhere = " AND dimensionID = :dimensionID";
		$arrdbparams["dimensionID"] = $filters[1]["dimensionID"];
		$boolUnsetFilter = true;
	}
	else
	{
		$dimensionWhere = "";
	}
	if(array_key_exists("qty", $filters[1]))
	{
		$qty = $filters[1]["qty"];
		$boolUnsetFilter = true;
	}
	else
	{
		$qty = 1;
	}
	if($boolUnsetFilter)
	{
		unset($filters[1]);
	}
	
	$i = 1;
	foreach($filters AS $filter)
	{
		$getSelectedItemID .= "INNER JOIN stock_attribute_relations stock_attribute_relations".$i." ON stock.recordID = stock_attribute_relations".$i.".stockID AND stock_attribute_relations".$i.".stockAttributeID = :type".$i." ";
		$arrdbparams["type".$i] = $filter['attID'];
		$i++;
	}
	
	$getSelectedItemID .= "WHERE groupID = :groupID".$dimensionWhere;
	$strType = "single";
	//echo $getSelectedItemID;
	//echo "\n";
	//var_dump($arrdbparams);
	//echo "\n";
	$selectedItemData = query($conn, $getSelectedItemID, $strType, $arrdbparams);
	
	//var_dump($selectedItemData);
	
	header('Content-Type: application/json');
	print ("{");
		if(!empty($selectedItemData))
		{
			$selectedItem = $selectedItemData['stockID'];
			$selectedStockCode = $selectedItemData['stockCode'];
			$ajaxItemImages = fnAjaxItemImages($selectedItem, $stockGroupDetails['name'], $conn);
			$ajaxItemDetails = fnAjaxItemDetails($selectedItem,$groupItemDetails,$stockGroupDetails['retailPrice'],$stockGroupDetails['statusID'],$qty,$conn);
			print ("\"statusCode\": \"1\",");
			print ("\"stockID\": \"".$selectedItem."\",");
			print ("\"stockCode\": \"".$selectedStockCode."\",");
			print ("\"images\": \"".str_replace(array("\"", "/"), array("\\\"", "\/"), $ajaxItemImages)."\",");
			print ("\"content\": \"".str_replace(array("\"", "/"), array("\\\"", "\/"), $ajaxItemDetails)."\"");
		}
		else
		{
			print ("\"statusCode\": \"0\",");
			print ("\"content\": \"Sorry - we do not have an item matching your selected options in stock. Adding to basket has temporarily been disabled until a valid selection is made.\"");
		}
	print ("}");
	
	$conn = null;
?>