<?php
	include("../includes/incsitecommon.php");

	$conn = connect();
	
	if(isset($_REQUEST['q'])) {
		$dataQuery = "SELECT searchTerm FROM site_search_terms ";
		$arrType = "multi";
		$dataParams = null;
		if($_REQUEST['q'] != "") {
			$dataQuery .= " WHERE searchTerm LIKE :itemname ";
			$dataParams = array("itemname" => "%".$_REQUEST['q']."%");
		}
		$dataQuery .= " AND predict = 1 ORDER BY searchTerm ASC LIMIT 10";
		$dataSet = query($conn, $dataQuery, $arrType, $dataParams);

		if(count($dataSet) > 0) {
			echo "<ul id='list'>";
			foreach($dataSet AS $dataSet) {
				echo "<li class='listitem' onclick=\"setitem('".str_replace("'","\'",$dataSet['searchTerm'])."')\">" . $dataSet['searchTerm'] . "</li>";
			}
			echo "</ul>";
		}
	}
	$conn = null;
?>