<?php
include ("../includes/inc_sitecommon.php");
$conn = connect(); // Open Connection to database

	if(isset($_POST['command'])) {
		
		$strcommand = $_POST['command'];
		$introwchange = 0;
		
		$strdbsql = "SELECT * FROM basket_header WHERE recordID = :basketID";
		$arrType = "single";
		$strdbparams = array("basketID"=>$intbasketid);
		$basketDetails = query($conn, $strdbsql, $arrType, $strdbparams);

		$intPostageID = $basketDetails['postageID'];
		$intOfferCodeID = $basketDetails['offerCodeID'];


		if ($intOfferCodeID > 0)
		{
			$strdbsql = "SELECT * FROM site_offer_codes WHERE recordID = :offerCodeID";
			$arrType = "single";
			$strdbparams = array("offerCodeID"=>$intOfferCodeID);
			$offerCodeDetails = query($conn, $strdbsql, $arrType, $strdbparams);
			
			$strtype = $offerCodeDetails['discountType'];
			$decdisvalue = $offerCodeDetails['discountAmount'];
			$decminamt = $offerCodeDetails['minimumSpend'];
			$strdiscountdesc = $offerCodeDetails['description'];
			$arrdiscountcats = explode(" ", $offerCodeDetails['categoryList']);
			$arrdiscountprod = explode(" ", $offerCodeDetails['stockList']);
			$strgetsoffer = '*Item qualifies for discount';
			$discountmark = '*';
		
		} else {
			if ($strdiscountdesc == "") $strdiscountdesc = "Please enter your code";
		}

		switch ($strcommand) {
			case "qty":
				// Update basket item quantity
				$intbasketitem = $_REQUEST['basket'];
				$intnewqty     = $_REQUEST['newqty'];
				if ($intnewqty > 0)
				{
					$strdbsql = "SELECT * FROM basket_items WHERE recordID = :basketItemID";
					$arrType = "single";
					$strdbparams = array("basketItemID"=>$intbasketitem);
					$basketItemDetails = query($conn, $strdbsql, $arrType, $strdbparams);
					$basketItemPrice = $basketItemDetails['price'];

					$strdbsql = "UPDATE basket_items SET quantity=:quantity WHERE recordID = :basketItemID";
					$arrType = "update";
					$strdbparams = array("basketItemID"=>$intbasketitem,"quantity"=>$intnewqty);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
					$introwchange = 1;
					$strrowqty = "<input type='button' name='butm".$intbasketitem."' value='-' title='Remove 1' onclick='jsnewqty(\"$intbasketitem\",\"m\",\"$intnewqty\")'/>&nbsp;$intnewqty&nbsp;<input type='button' name='buta$intbasketitem' value='+' title='Add 1 more' onclick='jsnewqty(\"$intbasketitem\",\"a\",\"$intnewqty\")' />";
					$strrowprice = number_format(($basketItemPrice*$intnewqty),2);
				}
				else
				{
					$strdbsql = "DELETE FROM basket_items WHERE recordID = :basketItemID";
					$arrType = "delete";
					$strdbparams = array("basketItemID"=>$intbasketitem);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
					$introwchange = 2;
				}
				break;
			case "changepostage":
				$intPostageID = $_REQUEST['postage'];
     			$strdbsql = "UPDATE basket_header SET postageID = :postageID WHERE recordID = :basketID";
				$arrType = "update";
				$strdbparams = array("basketID"=>$intbasketid,"postageID"=>$intPostageID);
				$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
				break;
			case "discount":

				$strcode = $_REQUEST['discount'];
				if ($strcode != "")
				{

					$strdbsql = "SELECT * FROM site_offer_codes WHERE code = :strcode";
					$arrType = "single";
					$strdbparams = array("strcode"=>$strcode);
					$offerDetails = query($conn, $strdbsql, $arrType, $strdbparams);
					
					if (count($offerDetails['recordID']) != 0)
					{

						$intused = $offerDetails['totalUses'];
						$intuses = $offerDetails['allowedUses'];
						$intstart = $offerDetails['startDate'];
						$intexpiry = $offerDetails['expiryDate'];

						if (($intexpiry > mktime() || $intexpiry == -1) && ($intuses > $intused || $intuses == 0))
						{
							if ($intstart > mktime()) //echo "code not active yet";
							{
								$strdbsql = "UPDATE basket_header SET offerCodeID = 0, discountAmount = 0 WHERE recordID = :basketID";
								$arrType = "update";
								$strdbparams = array("basketID"=>$intbasketid);
								$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
								
								$strdiscountdesc = "<span style='color:red'>Code is not active until ".date("d M Y", $intstart)."</span>";
								$decdiscount = 0.00;
								$discountmark = ' ';
								$strgetsoffer = '';
								//echo "Code not active, boo";
								
							} else {

								$strdbsql = "UPDATE basket_header SET offerCodeID = :offerCodeID WHERE recordID = :basketID";
								$arrType = "update";
								$strdbparams = array("basketID"=>$intbasketid, "offerCodeID"=>$offerDetails['recordID']);
								$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

								$codevalid = true;
								$discountmark = '*';
								$decdiscount = $offerDetails['discountAmount'];
								$strtype = $offerDetails['discountType'];
								$decdisvalue = $offerDetails['discountAmount'];
								$decminamt = $offerDetails['minimumSpend'];
								$strdiscountdesc = $offerDetails['description'];
								$arrdiscountcats = explode(",", $offerDetails['categoryList']);
								$arrdiscountprod = explode(",", $offerDetails['stockList']);
								$strgetsoffer = '*Item qualifies for discount';
								//echo "Discount applied, yay";
							}
							
						} else { //echo "code expired";
							
							$strdiscountdesc = "<span style='color:red'>Code has expired"."</span>";
							$decdiscount = 0.00;
							$discountmark = ' ';
							$strgetsoffer = '';
							$strdiscountdesc = '';
							//echo "Code expired, boo";
						}
						
					} else { //echo "code not found";
						
						$strdbsql = "UPDATE basket_header SET offerCodeID = 0, discountAmount = 0 WHERE recordID = :basketID";
						$arrType = "update";
						$strdbparams = array("basketID"=>$intbasketid);
						$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
						
						$strdiscountdesc = "Code not Recognised";
						$discountmark = ' ';
						$strcode = '';
						$strgetsoffer = '';
						$decdiscount = 0.00;
						//echo "Code not found, boo";
					}

				} else {
	     			 //clear unwanted code
					$strdbsql = "UPDATE basket_header SET offerCodeID = 0, discountAmount = 0 WHERE recordID = :basketID";
					$arrType = "update";
					$strdbparams = array("basketID"=>$intbasketid);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
					
					$decdiscount = 0.00;
					$discountmark = ' ';
					$strgetsoffer = '';
					$strdiscountdesc = '';
    			}
    			break;
		}
	}
	
	
	
	
	$intnoitems = 0;
	$dectotcost = 0;
	header ("Content-type: text/xml; charset=utf-8");
	print(utf8_encode ("<?xml version='1.0' encoding='UTF-8' ?>\n"));
	print(utf8_encode ("<basket>\n"));

	if($introwchange == 2) print(utf8_encode ("\t<deleterow>$intbasketitem</deleterow>\n"));
	if($introwchange == 1) {
		print(utf8_encode ("\t<updaterow>"));
			print(utf8_encode ("<rowID>$intbasketitem</rowID>"));
			print(utf8_encode ("<rowQty><![CDATA[$strrowqty]]></rowQty>"));
			print(utf8_encode ("<rowTotal><![CDATA[$strrowprice $discountmark]]></rowTotal>"));
		print(utf8_encode ("</updaterow>\n"));

	}
	
	//need to check all items
	$strdbsql = "SELECT tbl_basket.fld_product, tbl_stock.fld_itemsize, tbl_basket.fld_counter, tbl_basket.fld_qty, tbl_basket.fld_price";
	
	$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basketID";
	$strType = "multi";
	$arrdbparams = array("basketID"=>$intbasketid);
	$basketItems = query($conn,$strdbsql,$strType,$arrdbparams);

	foreach ($basketItems AS $basketItem) {
		
		$discountmark = '';
		$stockDetails = unserialize($basketItem['stockDetailsArray']);
		for ($i = 0; $i < $basketItem['quantity']; $i = $i + 1)
		{
			$strordersize .= $stockDetails['deliverySize'];
		}

		if($strcode != "") {

			$discountmark = '';
			if (!in_array('all', $arrdiscountcats)) //if offer doesn't apply to everything
			{
				//test if this product is in an offer
				if (in_array($basketItem['stockID'], $arrdiscountprod))
				{
					$booldiscountapplies = true;
					$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
					$discountmark = '*';
				}
				else
				{
					$strdbsql = "SELECT stock_category_relations.* FROM stock_category_relations INNER JOIN stock ON stock_category_relations.stockID = stock.groupID WHERE stock.recordID = :stockID";
					$strType = "multi";
					$arrdbparams = array("stockID"=>$basketItem['stockID']);
					$stockRelations = query($conn,$strdbsql,$strType,$arrdbparams);
					
					foreach ($stockRelations AS $stockRelation) {
						if (in_array($stockRelation['categoryID'], $arrdiscountcats) && $discountmark == '')
						{
							$booldiscountapplies = true;
							$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
							$discountmark = '*';
						}
					}
				}
			}
			else
			{
				$booldiscountapplies = true;
				$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
				$discountmark = '*';
			}
		}
		$rowprice = number_format(($basketItem['quantity'] * $basketItem['price']),2);
		$strrowqty = "<input type='button' name='butm".$basketItem['recordID']."' value='-' title='Remove 1' onclick='jsnewqty(\"".$basketItem['recordID']."\",\"m\",\"".$basketItem['quantity']."\")'/>&nbsp;".number_format($basketItem['quantity'], 0)."&nbsp;<input type='button' name='buta".$basketItem['recordID']."' value='+' title='Add 1 more' onclick='jsnewqty(\"".$basketItem['recordID']."\",\"a\",\"".$basketItem['quantity']."\")' />";
		print(utf8_encode ("\t<updaterow>"));
			print(utf8_encode ("<rowID>".$basketItem['recordID']."</rowID>"));
			print(utf8_encode ("<rowQty><![CDATA[$strrowqty]]></rowQty>"));
			print(utf8_encode ("<rowTotal><![CDATA[$rowprice $discountmark]]></rowTotal>"));
		print(utf8_encode ("</updaterow>\n"));
		
		$dectotcost += $basketItem['quantity'] * $basketItem['price'];
		$intnoitems += $basketItem['quantity'];
	}

	if ($decqualifytot < $decminamt && $decminamt > 0) {
		$strdiscountdesc = "<span  style='color:red'>You need to spent a minimum of &pound;".number_format($decminamt,2)." on qualifying products to receive this discount. (You are only spending &pound;".number_format($decqualifytot,2)."  on qualifying products.)</span>";
		$decdiscount = 0.00;
	} else if($booldiscountapplies) {
		switch($strtype) {
			case 'postage': $decdiscount = $decpostageamt; break;
			case 'percent': $decdiscount = $decqualifytot*($decdisvalue/100); break;
			case 'pound': $decdiscount = $decdisvalue; break;
		}
		$strdbsql = "UPDATE basket_header SET discountAmount = :discountAmount WHERE recordID = :basketHeaderID";
		$strType = "update";
		$arrdbparams = array("basketHeaderID"=>$intbasketid,"discountAmount"=>$decdiscount);
		$updateResult = query($conn,$strdbsql,$strType,$arrdbparams);
		$dectotcost = $dectotcost - $decdiscount;
	
	} else {
		$strdbsql = "UPDATE basket_header SET discountAmount = '', offerCodeID = '' WHERE recordID = :basketHeaderID";
		$strType = "update";
		$arrdbparams = array("basketHeaderID"=>$intbasketid);
		$updateResult = query($conn,$strdbsql,$strType,$arrdbparams);
		$strcode = '';
		$strgetsoffer = '';
		$decdiscount = 0.00;
		if ($codevalid == true) $strdiscountdesc = '';
	}

	if ($strcode == '') $strcode = '&#xA0;';
	if ($decdiscount == '') $decdiscount = '&#xA0;';
	else $decdiscount = " - £ ".number_format($decdiscount, 2);
	if ($strdiscountdesc == '') $strdiscountdesc = 'Please enter your code.';
	if ($strgetsoffer == '') $strgetsoffer = '&#xA0;';
	print(utf8_encode ("\t<offer>"));
		print(utf8_encode ("<offerdesc>$strdiscountdesc</offerdesc>"));
		print(utf8_encode ("<offercode>$strcode</offercode>"));
		print(utf8_encode ("<offeramount>$decdiscount</offeramount>"));
		print(utf8_encode ("<getsoffer>$strgetsoffer</getsoffer>"));
	print(utf8_encode ("</offer>\n"));


	print(utf8_encode ("\t<header><![CDATA[$intnoitems items = &pound; ".number_format($dectotcost,2)."]]></header>\n"));
	
	// if all items free postage
	if ((stristr($strordersize, "S") == FALSE) AND (stristr($strordersize, "L") == FALSE) AND (stristr($strordersize, "F") !== FALSE)) {
		$decpostage2 = 0;
		$decpostageamt = 0.00;
	}
	// else if order over free postage threshold
	else if (($dectotcost > $decfreepamt))
	{
		$decpostage2 = 0;
		$decpostageamt = 0.00;
	}
	// otherwise
	else
	{
		$decpostage2 = 25;
		$decpostageamt = 25.00;
	}
	
	print(utf8_encode ("\t<goodsitems>$intnoitems items</goodsitems>\n"));
	print(utf8_encode ("\t<goodstotal><![CDATA[".number_format($dectotcost,2)."]]></goodstotal>\n"));

	print(utf8_encode ("\t<standardpost><![CDATA[".number_format($decpostage2,2)."]]></standardpost>\n"));
	$dectotcost = $dectotcost + $decpostageamt;
	print(utf8_encode ("\t<subtotal><![CDATA[".number_format($dectotcost,2)."]]></subtotal>\n"));

	$decvatamt = $dectotcost * ($decvatrate/100);
	$dectotcost = $dectotcost + $decvatamt;

	print(utf8_encode ("\t<vat><![CDATA[".number_format($decvatamt,2)."]]></vat>\n"));
	print(utf8_encode ("\t<vatrate><![CDATA[".number_format($decvatrate,2)."]]></vatrate>\n"));
	print(utf8_encode ("\t<total><![CDATA[".number_format($dectotcost,2)."]]></total>\n"));
	print(utf8_encode ("</basket>"));

$conn = null; // close the database connection after all processing
?>