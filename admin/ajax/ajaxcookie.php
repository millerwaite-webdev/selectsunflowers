<?php
include ("../includes/inc_sitecommon.php");
$conn = connect(); // Open Connection to database

?>
<div id="popupCookie">
<div id='framebox' style='height:400px;overflow-y:auto;'>
<?php
$strdbsql = "SELECT * FROM site_pages WHERE pageName = 'cookies'";
$strType = "single";
$arrdbparams = array();
$pageDetails = query($conn,$strdbsql,$strType,$arrdbparams);

if (count($pageDetails['recordID']) != 0)
{
	$strcookiecontent = $pageDetails['pageContent'];
	$strcookietitle = $pageDetails['pageTitle'];
}
?>
<h2><?php echo $strcookietitle; ?></h2>
<div id="cookietext">
<?php echo $strcookiecontent; ?>
</div>
</div>
<a id="popupCookieClose">OK</a>
</div>
<div id="backgroundCookie"></div>

<?php
$conn = null; // close the database connection after all processing
?>