<?php
	header('Content-type: text/xml; charset=utf-8');
	//include($strrootpath."includes/incsitecommon.php");
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	switch($strcmd)
	{
		case "":
			
		break;
	}
	
	include("../includes/inc_sitecommon.php"); // Standard include used throughout site

	if(isset($_POST['page'])) {
		/*$strpage = $_POST['page'];
		echo $strpage."<br/>";
		$strpagetype = substr($strpage, 0, strpos($strpage, '_'));
		echo $strpagetype."<br/>";
		$strcat = str_replace ($strpagetype."_", "", $strpage);
		echo $strcat."<br/>";
		$strpagemore = substr($strpage, 1, strpos($strpage, '_'));
		echo $strpagemore."<br/>";
		$strcat = str_replace ($strpagetype."_", "", $strpagemore);
		echo $strcat."<br/>";*/
		$strpagearr = explode("_", $_POST['page']);
		$strcat = "";
		for($i = 2; $i < count($strpagearr); $i++)
		{
			$strcat .= $strpagearr[$i]."_";
		}
		$strcat = rtrim($strcat, "_");
		$strview = $strpagearr[0];
		//echo $strview."<br/>";
		//echo $strcat;
		$more = $strpagearr[1];
		$conn = connect();
		
		
		//include($strrootpath."includes/inc_shoplist.php");
		include("../includes/inc_shoplist.php");


		if(isset($intcats)) {
			print ("<input type='hidden' value='$intend' id='totalProducts'/>");
			//echo $strshopsqllimit."<br/>";
			//var_dump($arrdbparams);
			$stockItems = query($conn, $strshopsqllimit, $strType, $arrdbparams);
			//var_dump($stockItems);
			//echo "<br/><br/>";
			//echo $strshopsqllimit;
			//echo "<br/><br/>";
			//var_dump($arrdbparams);
			
			/********************************************** Filters ****************************************/
				print ("\n\n\n<div class='h2' style='height: 36px; padding: 5px 0; line-height: 0px;' >\n");
					
					$priceOptions = array();
					$priceOptions[] = "50";
					$priceOptions[] = "100";
					$priceOptions[] = "200";
					$priceOptions[] = "300";
					$priceOptions[] = "400";
					$priceOptions[] = "500";
					$priceOptions[] = "800";
					$priceOptions[] = "1000";

					//price low
					print ("<span style='margin-left: 10px;' >Lowest Price: <select name='priceLow' style='width:80px; margin-left: 10px;' id='priceLow' onchange='applyFilter()'><option value='' >Show All</option>");
					foreach ($priceOptions AS $price) { 
						if ($_POST['priceLow'] == $price) $selected = "selected"; else $selected = "";
						print ("<option value='$price' $selected>&pound;".number_format($price,2)."</option>");
					}
					print ("</select></span>");
					
					//price high
					print ("<span style='margin-left: 10px;' >Highest Price: <select name='priceHigh' style='width:80px; margin-left: 10px;' id='priceHigh' onchange='applyFilter()'><option value='' >Show All</option>");
					foreach ($priceOptions AS $price) { 
						if ($_POST['priceHigh'] == $price) $selected = "selected"; else $selected = "";
						print ("<option value='$price' $selected>&pound;".number_format($price,2)."</option>");
					}
					print ("</select></span>");
					//brand
					print ("<span style='margin-left: 10px;' >Brand: <select name='brand' style='width:200px; margin-left: 10px;' id='brand' onchange='applyFilter()'><option value='' >Show All</option>");
					$getBrandsQuery = "SELECT DISTINCT stock_brands.* FROM stock_brands INNER JOIN stock_group_information ON stock_group_information.brandID = stock_brands.recordID WHERE stock_group_information.recordID IN ($strstockgroupids)";
					$strType = "multi";
					$brands = query($conn, $getBrandsQuery, $strType);
					foreach($brands AS $brand)
					{
						if ($_POST['brand'] == $brand['recordID']) $selected = "selected"; else $selected = "";
						print("<option value='".$brand['recordID']."' $selected>".$brand['brandName']."</option>\n");
					}
					print ("</select></span>");
					
					//finish
					print ("<span style='margin-left: 10px;' >Finish: <select name='finish' style='width:220px; margin-left: 10px;' id='finish' onchange='applyFilter()'><option value='' >Show All</option>");
					$getFinishesQuery = "SELECT DISTINCT stock_attributes.* FROM stock_attributes INNER JOIN stock_attribute_relations ON stock_attributes.recordID = stock_attribute_relations.stockAttributeID WHERE stock_attribute_relations.stockID IN ($strstockids) AND stock_attributes.attributeTypeID = 2";
					$strType = "multi";
					$stockFinishes = query($conn, $getFinishesQuery, $strType);
					foreach($stockFinishes AS $stockFinish)
					{
						if ($_POST['finish'] == $stockFinish['recordID']) $selected = "selected"; else $selected = "";
						print("<option value='".$stockFinish['recordID']."' $selected>".$stockFinish['description']."</option>\n");
					}
					print ("</select></span>");
					//echo $getFinishesQuery;
					
					
				print ("</div>\n\n\n\n");

				/********************************************** Item List ****************************************/
				$itemList = fnItemList($stockItems, $conn);
				print $itemList;
		} else {
			header ("HTTP/1.0 500 Page Not Found $strpagetype $strcat");
		}
		$conn = null;
	} else {
		header ("HTTP/1.0 500 Request Data Missing");
	}
?>