<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "config-deliveryOptionRates"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	$configPageDetails = array();
	$configPageDetails['pageTitle'] = "Delivery Option Rates";
	$configPageDetails['allowUpdate'] = true;
	$configPageDetails['allowInsert'] = true;
	$configPageDetails['dbtable'] = "deliveryOptionRates"; //this is always required even if using a separate dbSelect query (below) - enter the primary table
	$configPageDetails['dbSelect'] = "SELECT deliveryOptions.description, deliveryOptionRates.* FROM deliveryOptionRates INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID"; //SELECT * FROM dbtable - default, enter value here if join is required
	$configPageDetails['dbSelectPrimaryKey'] = "recordID";
	
	$configPageDetails['displayTable']['rowColourDBField'] = ""; //which db field do we use to decide what colour to show the rows, leave blank to ignore row colours
	$configPageDetails['displayTable']['rowColourDangerValue'] = ""; //what value to check for to set the row as red (danger)
	$configPageDetails['displayTable']['rowColourSuccessValue'] = ""; //what value to check for to set the row as green (success)
	
	// replicate below for each required field
	$configPageDetails['columns']['description']['name'] = "Description";
	$configPageDetails['columns']['description']['inputType'] = "select"; //if select then need to define sql code for select lookup or possibly select options
	$configPageDetails['columns']['description']['inputSelect']['query'] = "SELECT * FROM deliveryOptions"; //enter query if getting options from DB
	$configPageDetails['columns']['description']['inputSelect']['options'] = ""; //comma separated list of options - will be used if query is blank
	$configPageDetails['columns']['description']['inputSelect']['default'] = ""; //default option if non set
	$configPageDetails['columns']['description']['inputSelect']['valuedbfield'] = "recordID"; //value (not visible)
	$configPageDetails['columns']['description']['inputSelect']['valuedbfieldDisplay'] = "description"; //value (visible)
	$configPageDetails['columns']['description']['displayOnTable'] = true;
	$configPageDetails['columns']['description']['displayOnInsert'] = false;
	$configPageDetails['columns']['description']['displayOnUpdate'] = false;
	$configPageDetails['columns']['description']['displayFormat'] = ""; //price
	$configPageDetails['columns']['description']['allowInsert'] = false;
	$configPageDetails['columns']['description']['allowUpdate'] = false;
	
	$configPageDetails['columns']['deliveryOptionID']['name'] = "Description";
	$configPageDetails['columns']['deliveryOptionID']['inputType'] = "select"; //if select then need to define sql code for select lookup or possibly select options
	$configPageDetails['columns']['deliveryOptionID']['inputSelect']['query'] = "SELECT * FROM deliveryOptions"; //enter query if getting options from DB
	$configPageDetails['columns']['deliveryOptionID']['inputSelect']['options'] = ""; //comma separated list of options - will be used if query is blank
	$configPageDetails['columns']['deliveryOptionID']['inputSelect']['default'] = ""; //default option if non set
	$configPageDetails['columns']['deliveryOptionID']['inputSelect']['valuedbfield'] = "recordID"; //value (not visible)
	$configPageDetails['columns']['deliveryOptionID']['inputSelect']['valuedbfieldDisplay'] = "description"; //value (visible)
	$configPageDetails['columns']['deliveryOptionID']['displayOnTable'] = false;
	$configPageDetails['columns']['deliveryOptionID']['displayOnInsert'] = true;
	$configPageDetails['columns']['deliveryOptionID']['displayOnUpdate'] = true;
	$configPageDetails['columns']['deliveryOptionID']['displayFormat'] = ""; //price
	$configPageDetails['columns']['deliveryOptionID']['allowInsert'] = true;
	$configPageDetails['columns']['deliveryOptionID']['allowUpdate'] = false;
	
	$configPageDetails['columns']['maxWeight']['name'] = "Max Weight";
	$configPageDetails['columns']['maxWeight']['inputType'] = "text"; //if select then need to define sql code for select lookup or possibly select options
	$configPageDetails['columns']['maxWeight']['inputSelect']['query'] = ""; //enter query if getting options from DB
	$configPageDetails['columns']['maxWeight']['inputSelect']['options'] = ""; //comma separated list of options - will be used if query is blank
	$configPageDetails['columns']['maxWeight']['inputSelect']['default'] = ""; //default option if non set
	$configPageDetails['columns']['maxWeight']['inputSelect']['valuedbfield'] = ""; //value (not visible)
	$configPageDetails['columns']['maxWeight']['inputSelect']['valuedbfieldDisplay'] = ""; //value (visible)
	$configPageDetails['columns']['maxWeight']['displayOnTable'] = true;
	$configPageDetails['columns']['maxWeight']['displayOnInsert'] = true;
	$configPageDetails['columns']['maxWeight']['displayOnUpdate'] = true;
	$configPageDetails['columns']['maxWeight']['displayFormat'] = ""; //price
	$configPageDetails['columns']['maxWeight']['allowInsert'] = true;
	$configPageDetails['columns']['maxWeight']['allowUpdate'] = true;
	
	$configPageDetails['columns']['cost']['name'] = "Cost";
	$configPageDetails['columns']['cost']['inputType'] = "text"; //if select then need to define sql code for select lookup or possibly select options
	$configPageDetails['columns']['cost']['inputSelect']['query'] = ""; //enter query if getting options from DB
	$configPageDetails['columns']['cost']['inputSelect']['options'] = ""; //comma separated list of options - will be used if query is blank
	$configPageDetails['columns']['cost']['inputSelect']['default'] = ""; //default option if non set
	$configPageDetails['columns']['cost']['inputSelect']['valuedbfield'] = ""; //value (not visible)
	$configPageDetails['columns']['cost']['inputSelect']['valuedbfieldDisplay'] = ""; //value (visible)
	$configPageDetails['columns']['cost']['displayOnTable'] = true;
	$configPageDetails['columns']['cost']['displayOnInsert'] = true;
	$configPageDetails['columns']['cost']['displayOnUpdate'] = true;
	$configPageDetails['columns']['cost']['displayFormat'] = ""; //price
	$configPageDetails['columns']['cost']['allowInsert'] = true;
	$configPageDetails['columns']['cost']['allowUpdate'] = true;
	// end of required replication fields
								

	
	
	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['primaryKey'])) $primaryKey = $_REQUEST['primaryKey']; else $primaryKey = "";
	
	switch($strcmd){
		case "recordSave":
			
			//get $_REQUEST fields
			foreach($configPageDetails['columns'] AS $key=>$value){
				$columnValues[$key] = (isset($_REQUEST['frm_'.$key])) ? $_REQUEST['frm_'.$key] : "";
			}
			
			if (!empty($primaryKey)){ //update existing
			
				//check we can find details using primary key
				$sqlQuery = "SELECT * FROM ".$configPageDetails['dbtable']." WHERE ".$configPageDetails['dbSelectPrimaryKey']." = :primaryKey";
				$strType = "single";
				$arrdbparam = array("primaryKey"=>$primaryKey);
				$resultdata = query($conn, $sqlQuery, $strType, $arrdbparam);
				
				if(!empty($resultdata[$configPageDetails['dbSelectPrimaryKey']])){
				
					$sqlquery = "UPDATE ".$configPageDetails['dbtable']." SET ";
					$arrdbparams = array("primaryKey"=>$primaryKey);
					foreach($configPageDetails['columns'] AS $key=>$value){
						
						if ($value['allowUpdate']) {
							$sqlquery .= $key." = :".$key.", "; 
							$arrdbparams[$key] = $columnValues[$key];
						}
					}
					$sqlquery = rtrim($sqlquery,", ");
					$sqlquery .= " WHERE ".$configPageDetails['dbSelectPrimaryKey']." = :primaryKey";
					$strType = "update";
					$updateResult = query($conn, $sqlquery, $strType, $arrdbparams);
					$strsuccess = "Successfully updated record. ";
					$strcmd = "";
					$primaryKey = "";
				
				} else {
					$strerror = "Record details could not be found. Please try again.";
					$primaryKey = "";
					$strcmd = "recordEdit";
				}
			
			
			} else { //insert new
					
				$sqlquery = "INSERT INTO ".$configPageDetails['dbtable']."";
				$arrdbparams = array();
				$sqlqueryFields = "";
				$sqlqueryValues = "";
				foreach($configPageDetails['columns'] AS $key=>$value){
					
					if ($value['allowInsert']) {
						$sqlqueryFields .= $key.","; 
						$sqlqueryValues .= ":".$key.","; 
						$arrdbparams[$key] = $columnValues[$key];
					}
				}
				$sqlquery .= "(".rtrim($sqlqueryFields,",").") VALUES (".rtrim($sqlqueryValues,",").")";
				$strType = "insert";
				$insertResult = query($conn, $sqlquery, $strType, $arrdbparams);
				$strsuccess = "Successfully created record. ";
				
				if($insertResult){
					$strsuccess = "Successfully created record.";
					$strcmd = "";
				} else {
					$strerror = "There was a problem creating the record please try again.";
					$strcmd = "recordAdd";
				}
			}
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>".$configPageDetails['pageTitle']."</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function fnCancel() {
					document.form.cmd.value="";
					document.form.submit();
				}
				function fnAddRecord(){
					document.form.cmd.value="recordAdd";
					document.form.primaryKey.value="";
					document.form.submit();
				}
				function fnEditRecord(primaryKey){
					document.form.cmd.value="recordEdit";
					document.form.primaryKey.value=primaryKey;
					document.form.submit();
				}
				function fnSave(){
					document.form.cmd.value="recordSave";
					document.form.submit();
				}
			</script>
			<?php
		
			print("<form action='$strpage.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='primaryKey' id='primaryKey' value='$primaryKey'/>");

				
				switch($strcmd) {

					case "recordAdd":
					case "recordEdit":
							
							$columnValues = array();
							if (!empty($primaryKey) AND $strcmd == "recordEdit") {

								if (!empty($configPageDetails['dbSelect'])) {
									$sqlQuery = $configPageDetails['dbSelect']." WHERE ".$configPageDetails['dbtable'].".".$configPageDetails['dbSelectPrimaryKey']." = :primaryKey"; 
								} else {
									$sqlQuery = "SELECT * FROM ".$configPageDetails['dbtable']." WHERE ".$configPageDetails['dbSelectPrimaryKey']." = :primaryKey";
								}
								
								$strType = "single";
								$arrdbparam = array("primaryKey"=>$primaryKey);
								$resultdata = query($conn, $sqlQuery, $strType, $arrdbparam);
								
								if (!empty($resultdata[$configPageDetails['dbSelectPrimaryKey']])) {
									foreach($configPageDetails['columns'] AS $key=>$value){
										$columnValues[$key] = $resultdata[$key];
									}
								} else {
									$strerror = "Delivery location could not be loaded";
									foreach($configPageDetails['columns'] AS $key=>$value){
										$columnValues[$key] = (isset($_REQUEST['frm_'.$key])) ? $_REQUEST['frm_'.$key] : "";
									}
								}
								
							} else {
								foreach($configPageDetails['columns'] AS $key=>$value){
									$columnValues[$key] = (isset($_REQUEST['frm_'.$key])) ? $_REQUEST['frm_'.$key] : "";
								}
							}
							
							print("<div class='row'>");
								print("<div class='col-md-6 split even-left'>");

									print("<div class='box-light' id='deltable".$strclass."'>");
										print("<div class='section'>");
											print("<fieldset>");
												print("<legend id='addLegend'></legend>");

												foreach($configPageDetails['columns'] AS $key=>$value){
													
													if ($value['allowUpdate'] AND $strcmd == "recordEdit") {
														$disabled = "";
													} else if ($value['allowInsert'] AND $strcmd == "recordAdd") {
														$disabled = "";
													} else {
														$disabled = "disabled";
													}
													
													if (($value['displayOnUpdate'] AND $strcmd == "recordEdit") OR ($value['displayOnInsert'] AND $strcmd == "recordAdd")) {
														switch ($value['inputType']) {
															case "text":
																print("<div class='row'>");
																	print("<div class='form-group col-sm-6 even-left'>");
																		print("<label for='frm_".$key."' class='control-label'>".$value['name'].":</label>");
																		print("<input $disabled class='form-control' id='frm_".$key."' name='frm_".$key."' type='text' placeholder='".$value['name']."' value='".$columnValues[$key]."' />");
																	print("</div>");
																print("</div>");
															break;
															case "select":
																print("<div class='row'>");
																	print("<div class='form-group col-sm-6 even-left'>");
																		print("<label for='frm_".$key."' class='control-label'>".$value['name'].":</label>");

																		if ($value['inputSelect']['query'] != "") {
																			$strdbsql = $value['inputSelect']['query'];
																			$arrdbparams = array();
																			$strType = "multi";
																			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
																		} else {
																			$resultdata = explode(",",$value['inputSelect']['options']);
																		}
																		
																		if ($columnValues[$key] == "") $columnValues[$key] = $value['inputSelect']['default'];
																		print ("<select $disabled name='frm_".$key."' id='frm_".$key."' class='form-control' placeholder='".$value['name']."' >");
																		foreach($resultdata AS $row){	
																			if ($columnValues[$key] == $row[$value['inputSelect']['valuedbfield']]) $selected = "selected"; else $selected = "";
																			print ("<option $selected value='".$row[$value['inputSelect']['valuedbfield']]."'>".$row[$value['inputSelect']['valuedbfieldDisplay']]."</option>");
																		}
																		print ("</select>");
																	print("</div>");
																print("</div>");
															break;
														}
													}
												}
											print("</fieldset>");
										print("</div>");
									print("</div>");
	
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-3' style='text-align:left;'>");
									print("<button onclick='return fnCancel();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-3' style='text-align:right;'>");
									print("<button onclick='return fnSave(this);' type='submit' class='btn btn-success right' style='display:inline-block;'>Save</button> ");
								print("</div>");
							print("</div>");
					break;
					default:				
						if ($configPageDetails['allowUpdate']) print("<button style='margin-bottom: 15px;' onclick='return fnAddRecord();' type='submit' class='btn btn-success'>Add New</button>");
						print("<div class='section'>");
							print("<table id='' class='table table-striped table-bordered table-hover table-condensed' >");
							
								//head display
								print("<thead><tr>");
									foreach($configPageDetails['columns'] AS $column){
										if ($column['displayOnTable']) {
											print("<th>".$column['name']."</th>");
										}
									}
									if($configPageDetails['allowUpdate']) print("<th style='text-align:center;'>Edit</th>");
								print ("</tr></thead><tbody>");
								
								//database query
								if (!empty($configPageDetails['dbSelect'])) $strdbsql = $configPageDetails['dbSelect']; else  $strdbsql = "SELECT * FROM ".$configPageDetails['dbtable'];
								$strType = "multi";
								$arrdbparams = array();
								$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

								if (count($resultdata) > 0) {
									foreach ($resultdata AS $row) {
										print ("<tr>");

											//check to see if we need to set a colour for each row
											if (!empty($configPageDetails['displayTable']['rowColourDBField'])){
												if ($row[$configPageDetails['displayTable']['rowColourDBField']] == $configPageDetails['displayTable']['rowColourDangerValue']) $tablecolour = "danger";
												else if ($row[$configPageDetails['displayTable']['rowColourDBField']] == $configPageDetails['displayTable']['rowColourSuccessValue']) $tablecolour = "success";
											} else {
												$tablecolour = "";
											}
											
											foreach ($row AS $key => $value){
											
												//get the column details based on the database column name
												$columnDetails = $configPageDetails['columns'][$key];
												if ($columnDetails['displayOnTable']){
													switch ($columnDetails['displayFormat']) {
														case "price":
															print ("<td class='$tablecolour'>".number_format($value,2)."</td>");
														break;
														default:
															print ("<td class='$tablecolour'>".$value."</td>");
														break;
													}
												}
											}									
											if ($configPageDetails['allowUpdate']) print("<td class='$tablecolour'><button onclick='fnEditRecord(".$row[$configPageDetails['dbSelectPrimaryKey']]."); return false;' class='btn btn-primary circle' type='submit' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");									
										print ("</tr>");
									}
								}
							print ("</tbody></table>");
						print("</div>");
						if ($configPageDetails['allowInsert']) print("<button onclick='return fnAddRecord();' type='submit' class='btn btn-success'>Add New</button>");
					break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
	?>
	<script language='Javascript'>
		$().ready(function() {
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
		});
	</script>
	<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>