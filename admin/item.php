<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * item.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "item"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
		
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	//get the main product link
	if (isset($_REQUEST['stockGroupLink'])) $stockGroupLink = $_REQUEST['stockGroupLink']; else $stockGroupLink = "";
	
	if($stockGroupLink != "")
	{
		
		$stockGroupLinkArr = explode("_", $stockGroupLink);
		$stockCode = $stockGroupLinkArr[0];
		
		//check the stockGroupID does exist and is available to purchase
		$strdbsql2 = "SELECT stock_group_information.*, stock.recordID AS stockID FROM stock_group_information INNER JOIN stock ON stock_group_information.recordID = stock.groupID WHERE stock.stockCode = :stockCode";
		$returnType = "single";
		$dataParam = array("stockCode" => $stockCode);
		//echo $strdbsql2;
		//var_dump($dataParam);
		$stockGroupDetails = query ($conn, $strdbsql2, $returnType, $dataParam);
		$stockGroupID = $stockGroupDetails['recordID'];
		$meta = array();
		
		if (!empty($stockGroupDetails)) {
			
			if(!isset($groupItemDetails)) $groupItemDetails = getStockGroupItems($stockGroupDetails['recordID'], $conn);
			
			
			if(empty($stockGroupDetails['stockID'])){
				reset($groupItemDetails);
				$selectedItem = key($groupItemDetails);
			}
			else
			{
				$selectedItem = $stockGroupDetails['stockID'];
			}
			
			//check the stocks status and display an error message if not found
			switch ($stockGroupDetails['statusID']) {
				case 1: //deleted
				case 2: //do not show on site
					$strerror = "Sorry this stock item has been removed from our online store.";
				break;
				case 3: //Standard
				case 4: //Web Exclusive
				case 5: //Catalogue Only
					$strerror = "";
					$meta['metaTitle'] = $stockGroupDetails['metaDoctitle']." - Stollers Furniture & Interiors";
					$meta['description'] = $stockGroupDetails['metaDescription'];
					$meta['keywords'] = $stockGroupDetails['metaKeywords'];
					$canonicalLinkInfo = array(
						"recordID" => $stockGroupDetails['recordID'],
						"name" => $stockGroupDetails['name'],
						"stockCode" => $stockCode
					);
					$meta['metaPageLink'] = getItemHref($canonicalLinkInfo);
					
				break;
			}
			
			$recentlyViewed = array(
								"recordID" => $stockGroupDetails['recordID'],
								"name" => $stockGroupDetails['name'],
								"stockCode" => $stockCode
							);
			if(!empty($_SESSION['recently_viewed']))
			{
				$unsetKey = -1;
				foreach($_SESSION['recently_viewed'] AS $key => $recentlyViewedArray)
				{
					if(array_values($recentlyViewed) == array_values($recentlyViewedArray))
					{
						$unsetKey = $key;
					}
				}
				
				if ($unsetKey != -1)
				{
					unset($_SESSION['recently_viewed'][$unsetKey]);
					$_SESSION['recently_viewed'] = array_values($_SESSION['recently_viewed']);
				}
				$intViewed = count($_SESSION['recently_viewed']);
				
				$i = intval($intViewed - 1);
				$y = 0;
				$recentlyViewedItems = array();
				while($i > -1 && $y < 3)
				{
					$recentlyViewedItems[$y] = $_SESSION['recently_viewed'][$i];
					$i = $i - 1;
					$y++;
				}
				ksort($recentlyViewedItems);
			}
			
			$_SESSION['recently_viewed'][] = $recentlyViewed;
			
		} else {
			$strerror = "Stock code not found, please check the code is correct";
		}
	} else {
		$strerror = "No stock code specified";
	}

	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	print ("<link rel='stylesheet' href='/css/jquery.fancybox.css?v=2.1.5' type='text/css' media='screen' property='stylesheet' />");
	print ("<link rel='stylesheet' href='/css/jquery.jqzoom.css' type='text/css' property='stylesheet' />");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	
	?>	
	<script type='text/javascript'>
		function fnLoadReviewEntry() {
			centerPopup2();
			loadPopup2();
		}
		function fnCloseReviewEntry() {
			disablePopup2();
		}
	</script>
	<?php

	print("<div id='mainContent'>");

		//Print out debug and error messages
		if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
		if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
		if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
		
		print ("<form action='item.php' class='uniForm' method='post' name='form' id='form' style='margin-top: 20px;'>");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' />");
			print ("<input type='hidden' class='nodisable' name='stockID' id='stockID' value='$selectedItem' />");
			print ("<input type='hidden' class='nodisable' name='stockGroupID' id='stockGroupID' value='$stockGroupID' />");
		
		if ($strerror == "") {

			//breadcrumbs
		
			//get brand information if available
			if ($stockGroupDetails['name'] != "") {
				$stockBrandDetails = getBrandInfo($stockGroupDetails['brandID'], "product", $conn);
				if ($stockBrandDetails['brandName'] != "") $stockBrandDetails['brandName'] .= " ";
			}
			
			print ("<div style='display: inline-block; width: 82%;'>");
				//stock group name
				print ("<h1>");
					if(strpos($stockGroupDetails['name'], $stockBrandDetails['brandName']))
					{
						print ($stockBrandDetails['brandName']);
					}
					print ($stockGroupDetails['name']);
				print ("</h1>");
				
				//reviews
				$stockAvgReviewDetails = getReviewInfo($stockGroupID, "avg", $conn);
				$ratingImage = "<img style='height:13px; margin-right: 10px;' src='/images/reviews/".$stockAvgReviewDetails['average_rating']."_full.png' alt='".($stockAvgReviewDetails['average_rating'] / 2)." Stars' />";
				if($stockAvgReviewDetails['count'] > 0)
				{
					$numReviews = $stockAvgReviewDetails['count']." Review";
					$writeReview = "Write a review";
					if($stockAvgReviewDetails['count'] > 1)
					{
						$numReviews .= "s";
					}
				}
				else
				{
					$numReviews = "No Reviews Yet";
					$writeReview = "Be the first to write a review";
				}
				print ("<p class='reviewTop'>".$ratingImage." <a href='#tabs-reviews' class='open-tab' style=''>".$numReviews."</a><span style='display: inline-block; height:13px; width: 1px;margin: 0 10px; background-color: silver;'></span><a href='#' onclick=\"fnLoadReviewEntry(); return false\">".$writeReview."</a></p>");
			print ("</div>");
			
			print ("<div style='display: inline-block; width: 18%;'>");
				if(!empty($stockBrandDetails['brandImage']) && file_exists($_SERVER['DOCUMENT_ROOT']."/images/brands/".$stockBrandDetails['brandImage']))
				{
					print ("<img style='float:right;' src='/images/brands/".$stockBrandDetails['brandImage']."' alt='".$stockBrandDetails['brandName']."' />");
				}
			print ("</div>");

			print ("<div id='itemleftcont' >");
				
				$ajaxItemImages = fnAjaxItemImages($selectedItem, $stockGroupDetails['name'], $conn);
				print ("<div id='imgbox'>\n");
					print ($ajaxItemImages);
				print ("</div>");
				
				//recently viewed / you may like
				print ("<div id='tabs1'>");
					print ("<ul>");
						print ("<li><a href='#tabs-recentlyviewed'>You Recently Viewed</a></li>");
						print ("<li><a href='#tabs-youmayalsolike'>You May Also Like</a></li>");
					print ("</ul>");
					print ("<div id='tabs-recentlyviewed'>");
						$boolTableDrawn = false;
						$totalRecentlyViewed = count($recentlyViewedItems);
						$i = 1;
						if(isset($recentlyViewedItems) && $totalRecentlyViewed > 0)
						{
							foreach($recentlyViewedItems AS $viewedItem)
							{
								if($viewedItem['recordID'] != $stockGroupID)
								{
									if(!$boolTableDrawn)
									{
										$boolTableDrawn = true;
										print ("<table>");
											print ("<tr>");
									}
									//print ("<div>");
									print ("<td>");
										$relatedStockAvgReviewDetails = getReviewInfo($viewedItem['recordID'], "avg", $conn);
										$viewedStockItemInfo = getStockGroupItemInfo($viewedItem['recordID'], $conn);
										$relatedStockItemImage = getThumbnailImage($viewedStockItemInfo['recordID'], $conn);
										
										print ("<a href='/".getItemHref($viewedItem)."'>");
										if(!empty($relatedStockItemImage['imageLink']) && file_exists($_SERVER['DOCUMENT_ROOT']."/images/products/".$viewedStockItemInfo['recordID']."/list/".$relatedStockItemImage['imageLink']))
										{
											print ("<img style='float:right;' class='relatedItemImage' src='/images/products/".$viewedStockItemInfo['recordID']."/list/".$relatedStockItemImage['imageLink']."' title='".$viewedItem['name']."' alt='".$viewedItem['name']." ".$relatedStockItemImage['description']."' />");
										}
										else
										{
											print ("<img src='http://www.jhbenson.co.uk/images/no_image.gif' class='relatedItemImage' alt='Image Not Available'>");
										}
										print ("</a>");
										
										$relatedRatingImage = "<img style='height:13px; margin-right: 10px;' src='/images/reviews/".$relatedStockAvgReviewDetails['average_rating']."_full.png' alt='".($relatedStockAvgReviewDetails['average_rating'] / 2)." Stars' />";
										$numRelatedReviews = $relatedStockAvgReviewDetails['count'];
										print ("<p class='reviewTop'>");
											print ($relatedRatingImage." <a href='#'>".$numRelatedReviews."<br/>");
											print ("<a href='/".getItemHref($viewedItem)."'>".$viewedItem['name']."</a><br/>");
											if($viewedStockItemInfo['price'] > 0)
											{
												print ("<span class='related-cost'>&pound;".$viewedStockItemInfo['price']."</span></a>");
											}
										print ("</p>");
									print ("</td>");
									//print ("</div>");
								}
								if($i == $totalRecentlyViewed && $boolTableDrawn)
								{
										print ("</tr>");
									print ("</table>");
								}
								$i++;
							}
						}
						if(!$boolTableDrawn)
						{
							print ("<p>No products viewed recently</p>");
						}
					print ("</div>");
					print ("<div id='tabs-youmayalsolike'>");
						$relatedItems = getRelatedItems($stockGroupDetails['recordID'], $conn);
						if(count($relatedItems) > 0)
						{
							print ("<table>");
								print ("<tr>");
									foreach($relatedItems AS $relatedItem)
									{
										//print ("<div>");
										print ("<td>");
											$relatedStockAvgReviewDetails = getReviewInfo($relatedItem['recordID'], "avg", $conn);
											$relatedStockItemInfo = getStockGroupItemInfo($relatedItem['recordID'], $conn);
											$relatedStockItemImage = getThumbnailImage($relatedStockItemInfo['recordID'], $conn);
											
											print ("<a href='/".getItemHref($relatedItem)."'>");
											if(!empty($relatedStockItemImage['imageLink']) && file_exists($_SERVER['DOCUMENT_ROOT']."/images/products/".$relatedStockItemInfo['recordID']."/list/".$relatedStockItemImage['imageLink']))
											{
												print ("<img style='float:right;' class='relatedItemImage' src='/images/products/".$relatedStockItemInfo['recordID']."/list/".$relatedStockItemImage['imageLink']."' title='".$relatedItem['name']."' alt='".$relatedItem['name']." ".$relatedStockItemImage['description']."' />");
											}
											else
											{
												print ("<img src='http://www.jhbenson.co.uk/images/no_image.gif' class='relatedItemImage' alt='Image Not Available'>");
											}
											print ("</a>");
											
											$relatedRatingImage = "<img style='height:13px; margin-right: 10px;' src='/images/reviews/".$relatedStockAvgReviewDetails['average_rating']."_full.png' alt='".($relatedStockAvgReviewDetails['average_rating'] / 2)." Stars' />";
											$numRelatedReviews = $relatedStockAvgReviewDetails['count'];
											print ("<p class='reviewTop'>");
												print ($relatedRatingImage." <a href='#'>".$numRelatedReviews."<br/>");
												print ("<a href='/".getItemHref($relatedItem)."'>".$relatedItem['name']."</a><br/>");
												if($viewedStockItemInfo['price'] > 0)
												{
													print ("<span class='related-cost'>&pound;".$relatedStockItemInfo['price']."</span></a>");
												}
											print ("</p>");
										print ("</td>");
										//print ("</div>");
									}
								print ("</tr>");
							print ("</table>");
						}
						else
						{
							print ("<p>No related items for this product</p>");
						}
					print ("</div>");
				print ("</div>");
			
			print ("</div>");
			print ("<div id='itemrightcont' >");
			
				//the information below changes depending on the options selected
				print ("<div id='ajaxContent'>");
					//var_dump($groupItemDetails[$selectedItem]);
					$qty = 1;
					$ajaxItemDetails = fnAjaxItemDetails($selectedItem,$groupItemDetails,$stockGroupDetails['retailPrice'],$stockGroupDetails['statusID'],$qty,$conn);
					print ($ajaxItemDetails);
				print ("</div>");
				
				//description / delivery / returns / reviews
				print ("<div id='tabs2'>");
					print ("<ul>");
						print ("<li><a href='#tabs-description'>Description</a></li>");
						print ("<li><a href='#tabs-delivery'>Delivery</a></li>");
						print ("<li><a href='#tabs-returns'>Returns</a></li>");
						print ("<li><a href='#tabs-reviews'>Reviews</a></li>");
					print ("</ul>");
					print ("<div id='tabs-description'>");
						
						print ("<dl>");
						print ("<dt>".$stockGroupDetails['name']."</dt>");
						print ("<dd>".$stockGroupDetails['description']."</dd>");
						print ("</dl>");
						
						/*print ("<dl>");
						print ("<dt>".$stockBrandDetails['brandName']."</dt>");
						print ("<p>".$stockBrandDetails['brandDescription']."</p>");
						print ("</dl>");*/
						
					print ("</div>");
					print ("<div id='tabs-delivery'>");
						print ("<p>Delivery</p>");
					print ("</div>");
					print ("<div id='tabs-returns'>");
						print ("<p>Returns</p>");
					print ("</div>");
					print ("<div id='tabs-reviews'>");
						print ("<p class='reviewTop'>".$ratingImage." <a href='#' style=''>".$numReviews."</a><span style='display: inline-block; height:13px; width: 1px;margin: 0 10px; background-color: silver;'></span><a href='#' onclick='fnLoadReviewEntry(); return false' style=''>".$writeReview."</a></p>");
						print("<hr/>");
						$stockReviewAllDetails = getReviewInfo($stockGroupDetails['recordID'], "all", $conn);
						print("<table style='width: 100%;'>");
						$i = 1;
						foreach($stockReviewAllDetails AS $stockReviewDetails)
						{
							print("<tr>");
								print("<td>");
									$ratingImage = "<img style='height:13px; margin-right: 10px;' src='/images/reviews/".$stockReviewDetails['numStars']."_full.png' alt='".($stockReviewDetails['average_rating'] / 2)." Stars' />";
									print ("<p class='reviewTop'>".$ratingImage."<br/>".date("d M Y", $stockReviewDetails['dateReview'])."<br/>".$stockReviewDetails['reviewerName']."</p>");
								print("</td>");
								print("<td>");
									print ("<h4>".$stockReviewDetails['title']."</h4>");
									print ("<p>".$stockReviewDetails['description']."</p>");
									if(!empty($stockReviewDetails['dateResponse']) && !empty($stockReviewDetails['response']))
									{
										print ("<hr/>");
										print ("<h4>Stollers response (".date("d M Y", $stockReviewDetails['dateReview']).")</h4>");
										print ("<p>".$stockReviewDetails['response']."</p>");
									}
								print("</td>");
							print("</tr>");
							if($i < $numReviews)
							{
								print ("<tr><td colspan='2'><hr/></td></tr>");
							}
							$i++;
						}
						print("</table>");
					print ("</div>");
				print ("</div>");
			print ("</div>");

			
			//product review pop up
			print ("<div id='popupReview' style='display: none; padding:0;'>");
				print ("<h2 class='itemreviewheader' style='margin:0;'>Customer Review - <span style='color:#183D6A; font-size: 13px;'>".$stockGroupDetails['name']."</span></h2>\n");
				print ("<a href='#' id='popupReviewClose' onclick='fnCloseReviewEntry(); return false'>x</a>");
				print ("<table style='margin: 10px; border: none; cell-padding: 3px; cell-spacing: 3px;'>");
				print ("<tr><td>Review Title : </td><td><input type='text' name='revtitle' size='50' maxlength='30'/><input type='hidden' name='revstock' value='$stockGroupID'/></td></tr>");
				print ("<tr><td>Comments : </td><td><textarea name='revdetail' rows='3' cols='50'></textarea></td></tr>");
				print ("<tr><td>Rating : </td><td><input type='hidden' name='revscore' value='0'/>");
				print ("<img src='".$strimageurl."ratings/0_full.png' id='ratingimg' alt='My Rating' width='90' height='17'/>");
				print ("</td></tr>");
				print ("<tr><td>Your Name : </td><td><input type='text' name='revname' size='40' maxlength='50' /></td></tr>");
				print ("<tr><td style='vertical-align: top;'>Email : </td><td><input type='text' name='revemail' size='40' maxlength='50' /><br/><p class='reviewcredit'>(Optional: We may use this to contact you to discuss your experience with our products)</p></td></tr>");
				print ("<tr><td colspan='2'><input type='button' value='Submit Review' onclick='jsaddreview(); return false;' class='short_button' /></td></tr>");
				print ("</table>");
				print ("<div id='reviewerror'></div>");
			print ("</div>");
			print ("<div id='basketpopup'></div>");
		} else {
			print ("<h2>Product Not Found</h2>");
			print ("This product may have been removed, please use the search feature above to find similar products");
		}
		print ("</form>");
	print("</div>");
	
	?>
	<script src="/js/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
	<script src="/js/jqzoom.pack.2.3.js" type="text/javascript"></script>
	<script type='text/javascript'>
		
		var prevDimensionSelection;
		
		$().ready(function() {
			if(document.getElementById("dimensions")){
				prevDimensionSelection = $("#dimensions").val();
			}
			init_jqzoom();
			
			setTimeout(function() {
				moveOverlays()
			}, 1000);
			
			$(function() {
				$( "#tabs1" ).tabs(); //you recently viewed / You may also like
				$( "#tabs2" ).tabs(); //description / delivery / returns / reviews
			});
			$(".fancybox").fancybox({
				'transitionIn': 'elastic',
				'transitionOut': 'fade'
			});
			$('#ratingimg').mousemove(function(e){
				var img = $(this);
				var pagePos = e.pageX;
				var imgOffset = img.offset().left;
				var imgWidth = img.width();
				var imgPos = pagePos - imgOffset;
				var ranking = parseInt((imgPos/imgWidth)*6)*2;
				document.form.revscore.value = parseInt(ranking / 2);
				img.attr("src","http://jhbenson.co.uk/images/ratings/" +ranking + "_full.png");
			});
			$('#ratingimg').mouseout(function(e){
				var img = $(this);
				var ranking = parseInt(document.form.revscore.value * 2);
				img.attr("src","http://jhbenson.co.uk/images/ratings/" +ranking + "_full.png");
			});
			
			$('#zoomlink').bind('click',function(){
				var fancyImage = jQuery('.zoomWrapperImage img').attr('src');      
				jQuery.fancybox.open([{href : fancyImage}], {closeClick : true}); 
			});
			
			$('.thumbnail').click(function(e){
				e.preventDefault();
				clickThumbnail(this);
			});
			
			$('.open-tab').click(function(event) {
				var tab = $(this).attr('href');
				$('#tabs2').tabs('select', tab);
				event.preventDefault();
			});
			
			if (Modernizr.history) {
				var data = {
					"stockID": "<?php print($stockCode); ?>",
					"images": "<?php print($ajaxItemImages); ?>",
					"content": "<?php print(addslashes($ajaxItemDetails)); ?>"
				};
				//console.log(data);
				window.history.replaceState(data, null, null);
			}
			
			window.addEventListener("popstate", function(e) {
				
				// state
				var state = e.state;
				//console.log(state);
				if(state == null) {
					return;
				}
				// return to last state
				if (state.stockCode != $("#stock_code").text()) {
					updateItemContent(state);
				}
			});
		});

	</script>
	
	<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>