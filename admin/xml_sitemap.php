<?php

//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * xml_sitemap.php                                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '
//	XML sitemap for search engines auto updating from db.


	// ************* Common page setup ******************** //
	//=====================================================//
	
	$strpage = "xml_sitemap"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	header("Content-type: text/xml");

	print("<?xml version='1.0'?>\n");
	print("<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>");

		print("<url>\n");
			print("<loc>".$strsiteurl."</loc>\n");
			print("<changefreq>weekly</changefreq>\n");
			print("<priority>1.0</priority>");
		print("</url>\n");
		print("\n");

		print("<url>\n");
			print("<loc>".$strsiteurl."/news.php</loc>\n");
			print("<changefreq>weekly</changefreq>\n");
			print("<priority>0.5</priority>");
		print("</url>\n");
		print("\n");

		print("<url>\n");
			print("<loc>".$strsiteurl."/rss_news.php</loc>\n");
			print("<changefreq>weekly</changefreq>\n");
			print("<priority>0.5</priority>");
		print("</url>\n");
		print("\n");
		
		$getNewsArticlesQuery = "SELECT * FROM site_news_events WHERE title != 'New news item' ORDER BY date DESC";
		$strType = "multi";
		$newsArticles = query($conn, $getNewsArticlesQuery, $strType);

		if (empty($newsArticles))
		{
			$strmessage = "Error";
		}
		else
		{
			foreach($newsArticles AS $newsArticle)
			{
				
				if ($newsArticle['title'] != "" && $newsArticle['title'] != "New News Item") {
					print("<url>\n");
						print("<loc>".$strsiteurl."/news.php?story=".$newsArticle['recordID']."</loc>\n");
						print("<changefreq>monthly</changefreq>\n");
						print("<priority>0.3</priority>");
					print("</url>\n");
					print("\n");
				}

				
			}
		}

		/*print("<url>\n");
			print("<loc>".$strsiteurl."/jobs.php</loc>\n");
			print("<changefreq>weekly</changefreq>\n");
			print("<priority>0.5</priority>");
		print("</url>\n");
		print("\n");
		

		$strdbsql = "SELECT * FROM tbl_jobs";
		$result = mysql_query ($strdbsql,$strdb);

		if (mysql_num_rows($result) == 0)
		{
			$strmessage = "Error";
		}
		else
		{
			while($resultdata = mysql_fetch_object($result)) {
				
					print("<url>\n");
						print("<loc>".$strsiteurl."/jobs.php?ref=".$resultdata->fld_counter."</loc>\n");
						print("<changefreq>monthly</changefreq>\n");
						print("<priority>0.3</priority>");
					print("</url>\n");
					print("\n");
				
			}
		}
		mysql_free_result ($result);*/
		
		$getSitePages = "SELECT * FROM site_pages ORDER BY recordID";
		$strType = "multi";
		$sitePages = query($conn, $getSitePages, $strType);

		if (empty($result))
		{
			$strmessage = "Error";
		}
		else
		{
			foreach($sitePages AS $sitePage)
			{
				print("<url>\n");
					print("<loc>".$strsiteurl."/".$sitePage['pageName'].".php</loc>\n");
					print("<changefreq>weekly</changefreq>\n");
					print("<priority>0.9</priority>");
				print("</url>\n");
				print("\n");
			}
		}

		$getCategories = "SELECT * FROM category ORDER BY recordID";
		$strType = "multi";
		$categories = query($conn, $getCategories, $strType);

		if (empty($categories))
		{
			$strmessage = "Error";
		}
		else
		{
			foreach($categories AS $category)
			{
				print("<url>\n");
					print("<loc>".$strsiteurl."/category_".$category['metaPageLink']."</loc>\n");
					print("<changefreq>weekly</changefreq>\n");
					print("<priority>0.9</priority>");
				print("</url>\n");
				print("\n");
			}
		}

		$getBrands = "SELECT * FROM stock_brands ORDER BY recordID";
		$strType = "multi";
		$brands = query($conn, $getBrands, $strType);

		if (empty($brands))
		{
			$strmessage = "Error";
		}
		else
		{
			foreach($brands AS $brand)
			{
				print("<url>\n");
					print("<loc>".$strsiteurl."/brand_".$brand['metaPageLink']."</loc>\n");
					print("<changefreq>weekly</changefreq>\n");
					print("<priority>0.9</priority>");
				print("</url>\n");
				print("\n");
			}
		}

		$getRanges = "SELECT * FROM stock_ranges ORDER BY recordID";
		$strType = "multi";
		$ranges = query($conn, $getRanges, $strType);

		if (empty($ranges))
		{
			$strmessage = "Error";
		}
		else
		{
			foreach($ranges AS $range)
			{
				print("<url>\n");
					print("<loc>".$strsiteurl."/range_".$range['metaPageLink']."</loc>\n");
					print("<changefreq>weekly</changefreq>\n");
					print("<priority>0.9</priority>");
				print("</url>\n");
				print("\n");
			}
		}

		$getStockGroupItems = "SELECT stock.stockCode, stock_group_information.name FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock_group_information.statusID NOT IN (1, 2) ORDER BY stock_group_information.metaLink";
		$strType = "multi";
		$stockItems = query($conn, $getStockGroupItems, $strType);

		if (empty($stockItems))
		{
			$strmessage = "Error";
		}
		else
		{
			foreach($stockItems AS $stockItem)
			{
				$strcanonical = "/".getItemHref($stockItem);
				print("<url>\n");
					print("<loc>".$strsiteurl.$strcanonical."</loc>\n");
					print("<changefreq>weekly</changefreq>\n");
					print("<priority>0.5</priority>");
				print("</url>\n");
				print("\n");

				
			}
		}
		
		// ************* Common page setup ******************** //
		//=====================================================//
		$conn = null; // close the database connection after all processing
?>
</urlset>
