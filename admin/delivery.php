<?php


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "delivery"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['deliveryID'])) $deliveryID = $_REQUEST['deliveryID']; else $deliveryID = "";
	
	switch($strcmd){
		case "saveDelivery":
		
			//check date is valid
			$intDeliveryDate = strtotime(str_replace("/", "-", $_REQUEST['frm_deliveryDate']));
			$weekday = date('N', $intDeliveryDate);
			$allowedWeekdays = array("1","2","3","4");
			if (!in_array($weekday,$allowedWeekdays)) {
				while(!in_array($weekday,$allowedWeekdays)){
					$intDeliveryDate = strtotime('+1 day', $intDeliveryDate);
					$weekday = date('N', $intDeliveryDate);
				}
				$strwarning = "Selected despatch date is not valid, it has been set to the first valid despatch day.";
			}
			
			//set initial parameters
			$arrParams = array("deliveryDate" => $intDeliveryDate, "deliveryOptionRateID" => $_REQUEST['frm_deliveryOption'], "packageTypeID" => $_REQUEST['frm_packageType'], "notes" => $_REQUEST['frm_deliveryNotes'], "serviceCode"=> $_REQUEST['frm_serviceCode']);

			//get header details of submitted order
			$strdbsql = "SELECT * FROM order_header WHERE recordID = :orderID";
			$strType = "single";
			$arrParam = array("orderID"=>$_REQUEST['frm_orders']);
			$orderHeaderDetails = query($conn, $strdbsql, $strType, $arrParam);
			
			if (!empty($orderHeaderDetails['recordID'])) {
				
				//get order item details
				$strdbsql = "SELECT * FROM order_items WHERE orderHeaderID = :orderID";
				$strType = "multi";
				$arrParam = array("orderID"=>$orderHeaderDetails['recordID']);
				$orderItemDetails = query($conn, $strdbsql, $strType, $arrParam);
				
				//set customer from order header
				$arrParams['customerID'] = $orderHeaderDetails['customerID'];
				$arrParams['orderHeaderID'] = $orderHeaderDetails['recordID'];
				
				//check if we already have a deliveryID (editing existing)
				if($deliveryID != "")
				{
					//update
					$strdbsql = "UPDATE delivery SET orderHeaderID=:orderHeaderID, deliveryDate = :deliveryDate, deliveryOptionRateID = :deliveryOptionRateID, customerID = :customerID, packageTypeID = :packageTypeID, notes = :notes, serviceCode = :serviceCode WHERE recordID = :deliveryID";
					$strType = "update";
					$arrParams['deliveryID'] = $deliveryID;
					$updateDelivery = query($conn, $strdbsql, $strType, $arrParams);
				}
				else
				{
					//insert
					$strdbsql = "INSERT INTO delivery (deliveryDate, deliveryOptionRateID, customerID, packageTypeID, notes, orderHeaderID, serviceCode) VALUES (:deliveryDate, :deliveryOptionRateID, :customerID, :packageTypeID, :notes, :orderHeaderID, :serviceCode)";
					$strType = "insert";
					$deliveryID = query($conn, $strdbsql, $strType, $arrParams);
				}
				
				if($deliveryID != "")
				{
					//delete order items from delivery table as we are going to re-create
					$strdbsql = "DELETE FROM deliveryOrders WHERE deliveryItemID = :deliveryItemID";
					$strType = "delete";
					$arrParams = array("deliveryItemID" => $deliveryID);
					$deleteResult = query($conn, $strdbsql, $strType,$arrParams);
					
					//insert each order item into the delivery table
					foreach($orderItemDetails AS $orderItem)
					{
						$strdbsql = "INSERT INTO deliveryOrders (deliveryItemID, orderItemID) VALUES (:deliveryItemID,:orderItemID)";
						$strType = "insert";
						$arrParams = array("deliveryItemID"=>$deliveryID,"orderItemID"=>$orderItem['recordID']);
						$deleteResult = query($conn, $strdbsql, $strType,$arrParams);
					}
					
					//check weights
					$strdbsql = "SELECT maxWeight FROM deliveryOptionRates WHERE recordID = :deliveryOptionRateID";
					$strType = "single";
					$arrParam = array("deliveryOptionRateID" => $_REQUEST['frm_deliveryOption']);
					$maxWeightArr = query($conn, $strdbsql, $strType, $arrParam);
					$maxWeight = $maxWeightArr['maxWeight'];
					
					$totalOrderWeight = 0.00;
					
					//get package type weight
					$strdbsql = "SELECT weight FROM deliveryPackageTypes WHERE recordID = :packageTypeID";
					$strType = "single";
					$arrParam = array("packageTypeID" => $_REQUEST['frm_packageType']);
					$packageWeightArr = query($conn, $strdbsql, $strType, $arrParam);
					
					$totalOrderWeight += $packageWeightArr['weight'];
					
					$itemsNotAdded = "";
					$itemsAdded = "";
					
					//stock items weight + package weight < delivery option rate max weight
					foreach($orderItemDetails AS $orderItem)
					{
						$strdbsql = "SELECT (stock.weight * order_items.quantity) AS orderItemWeight, stock_group_information.name FROM stock INNER JOIN order_items ON stock.recordID = order_items.stockID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE order_items.recordID = :recordID";
						$strType = "single";
						$arrParam = array("recordID"=>$orderItem['recordID']);
						$stockWeightArr = query($conn, $strdbsql, $strType, $arrParam);
						$orderItemWeight = $stockWeightArr['orderItemWeight'];
						$totalOrderWeight += $orderItemWeight;
						
						if(($totalOrderWeight > $maxWeight) && $maxWeight > 0.0000)
						{
							$strwarning = "Total order weight is greater than allowed, pleased review the selected delivery method.<br/><br/>Total Order Weight: $totalOrderWeight<br/>Maximum Delivery Weight: $maxWeight";
						}
					}
				
					$strdbsql = "UPDATE delivery SET weight = :weight WHERE recordID = :recordID";
					$strType = "update";
					$arrParam = array("weight" => $totalOrderWeight, "recordID" => $deliveryID);
					query($conn, $strdbsql, $strType, $arrParam);
					
					if ($strwarning == "" AND $strerror == "") {
						$strsuccess = "Order successfully updated for delivery.";
					
						$strdbsql = "UPDATE order_header SET orderStatus = 4 WHERE recordID = :orderID";
						$strType = "single";
						$arrParam = array("orderID"=>$orderHeaderDetails['recordID']);
						$resultdataUpdate = query($conn, $strdbsql, $strType, $arrParam);
					}
					
				} else {
					$strerror = "Problem creating delivery, please try again.";
				}

			} else {
				$strerror = "Order not found in the system, please try again.";
			}
			$strcmd = "editDelivery";
		break;
	}
	
	
	

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Delivery</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

			?>	
			<script type='text/javascript'>	

				function fnEditDelivery(deliveryID){
					document.form.cmd.value='editDelivery';
					document.form.deliveryID.value=deliveryID;
					document.form.submit();
				}
				function fnAddDelivery(){
					document.form.cmd.value='addDelivery';
					document.form.deliveryID.value='';
					document.form.submit();
				}
				function fnSaveDelivery(){
					document.form.cmd.value='saveDelivery';
					document.form.submit();
				}
				function fnCancel(){
					document.form.cmd.value='';
					document.form.deliveryID.value='';
					document.form.submit();
				}

			</script>
			<?php
			
			
			print("<form action='delivery.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' class='nodisable' name='cmd' id='cmd' />");
				print("<input type='hidden' class='nodisable' name='deliveryID' id='deliveryID' value='$deliveryID' />");

				
				switch ($strcmd){
					case "addDelivery":
					case "editDelivery":
						print("<input type='hidden' class='nodisable' name='orderItemIDs' id='orderItemIDs' class='form-control' value='' />");
						
						print("<div class='row'>");
							print("<div class='col-md-6'>");
								print("<div class='section'>");
									print("<fieldset>");
										print("<legend>Delivery Details</legend>");
						
									if (!empty($deliveryID)) {
										
										$strdbsql = "SELECT delivery.*, deliveryOptionRates.deliveryOptionID FROM delivery INNER JOIN deliveryOptionRates ON delivery.deliveryOptionRateID = deliveryOptionRates.recordID WHERE delivery.recordID = :recordID";
										$strType = "single";
										$arrdbparam = array("recordID"=>$deliveryID);
										$delivery = query($conn, $strdbsql, $strType, $arrdbparam);

									} else {
										$delivery['deliveryDate'] = $datnow;
										$delivery['deliveryOptionRateID'] = "";
										$delivery['packageTypeID'] = "";
										$delivery['weight'] = 0;
										$delivery['notes'] = "";
										$delivery['deliveryStatusID'] = 1;
										$delivery['customerID'] = null;
									}
									
									print("<div class='row'>");
									
										//select delivery option
										print("<div class='form-group col-md-12' >");
											print("<label for='frm_orders' class='control-label'>Orders:</label>");
											print("<select name='frm_orders' id='frm_orders' class='form-control'>");
												$strdbsql = "SELECT order_header.* FROM order_items INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID LEFT JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID WHERE (deliveryOrders.recordID IS NULL OR deliveryOrders.deliveryItemID = :deliveryItemID) AND order_header.orderStatus < 3 GROUP BY order_items.orderHeaderID ORDER BY order_header.dispatchDate ASC ";
												$strType = "multi";
												$arrdbparam = array("deliveryItemID"=>$deliveryID);
												$resultdata = query($conn, $strdbsql, $strType, $arrdbparam);							  
												foreach ($resultdata AS $row)
												{
													$strSelected = "";
													if($row['recordID'] == $delivery['orderHeaderID']) $strSelected = " selected";
													print("<option value='".$row['recordID']."'".$strSelected.">Order: ".$row['recordID']." for ".$row['addDeliveryTitle']." ".$row['addDeliveryFirstname']." ".$row['addDeliverySurname'].". Due for delivery ".fnconvertunixtime($row['dispatchDate'])."</option>");
												}
											print("</select>");
										print("</div>");
									print("</div>");
								
									
									print("<div class='row'>");
									
										//delivery day / timestamp
										print("<div class='form-group col-md-6' style='padding-right:15px;'>");
											print("<label for='frm_deliveryDate' class='control-label'>Delivery Date:</label>");
											print("<input type='text' class='form-control' id='frm_deliveryDate' name='frm_deliveryDate' value='".date("d/m/Y", $delivery['deliveryDate'])."' />");
										print("</div>");
					
										//select delivery option
										print("<div class='form-group col-md-6' style='padding-left:15px;'>");
											print("<label for='frm_deliveryOption' class='control-label'>Delivery Option:</label>");
											print("<select name='frm_deliveryOption' id='frm_deliveryOption' class='form-control'>");
												$strdbsql = "SELECT deliveryOptionRates.*, deliveryOptions.description FROM deliveryOptionRates INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID ORDER BY description";
												$strType = "multi";
												$resultdata = query($conn, $strdbsql, $strType);								  
												foreach ($resultdata AS $row)
												{
													$strSelected = "";
													if($row['recordID'] == $delivery['deliveryOptionRateID']) $strSelected = " selected";
													print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']." - ".$row['maxWeight']."</option>");
												}
											print("</select>");
										print("</div>");
									print("</div>");
									
									
									print("<div class='row'>");
										print("<div class='form-group col-md-6'>");
											
											//select package type
											print("<label for='frm_packageType' class='control-label'>Package Type:</label><select name='frm_packageType' id='frm_packageType' class='form-control'>");
											$strdbsql = "SELECT * FROM deliveryPackageTypes";
											$strType = "multi";
											$resultdata = query($conn, $strdbsql, $strType);
											foreach ($resultdata AS $row)
											{
												$strSelected = "";
												if($row['recordID'] == $delivery['packageTypeID']) $strSelected = " selected";
												print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']." - ".$row['weight']."</option>");
											}
											print("</select>");
										print("</div>");
										
										//select delivery option
										print("<div class='form-group col-md-6' style='padding-left:15px;'>");
											print("<label for='frm_serviceCode' class='control-label'>Service Code:</label>");
											print("<select name='frm_serviceCode' id='frm_serviceCode' class='form-control'>");
												//$strdbsql = "SELECT deliveryOptionRates.*, deliveryOptions.description FROM deliveryOptionRates INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID";
												//$strType = "multi";
												//$resultdata = query($conn, $strdbsql, $strType);
												$serviceCodes[0]['value'] = "A";
												$serviceCodes[0]['description'] = "Next Day";
												$serviceCodes[1]['value'] = "B";
												$serviceCodes[1]['description'] = "Scotland";
												foreach ($serviceCodes AS $row)
												{
													$strSelected = "";
													if($row['value'] == $delivery['serviceCode']) $strSelected = " selected";
													print("<option value='".$row['value']."'".$strSelected.">".$row['value']." - ".$row['description']."</option>");
												}
											print("</select>");
										print("</div>");
										
										
									print("</div>");
									print("<div class='row'>");
										print("<div class='form-group col-md-12'>");
											
											//delivery notes
											print("<label for='frm_deliveryNotes' class='control-label'>Notes:</label><textarea class='form-control' id='frm_deliveryNotes' name='frm_deliveryNotes' rows='5'>".$delivery['notes']."</textarea>");
										print("</div>");
									print("</div>");
									print("<div class='row'>");
										print("<div class='form-group col-md-12'>");
											
											//buttons
											print("<button onclick='return fnSaveDelivery();' type='button' class='btn btn-success right'>Save</button> ");
											print("<button onclick='return fnCancel();' class='btn btn-danger left'>Cancel</button>"); //need rules on this e.g. order status and payment status
										print("</div>");
									print("</div>");
						print("</fieldset>");
						
						
					break;				
					default:
						//show list of deliverys
						$strdbsql = "SELECT delivery.*,deliveryStatus.description AS statusDesc, deliveryOptions.description AS optionDesc, deliveryOptionRates.cost, deliveryPackageTypes.description AS packageDesc FROM delivery INNER JOIN deliveryOptionRates ON delivery.deliveryOptionRateID = deliveryOptionRates.recordID INNER JOIN deliveryPackageTypes ON delivery.packageTypeID = deliveryPackageTypes.recordID INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID LEFT JOIN deliveryStatus ON delivery.deliveryStatusID = deliveryStatus.recordID INNER JOIN order_header ON delivery.orderHeaderID = order_header.recordID WHERE order_header.orderStatus < 5 ORDER BY delivery.deliveryDate DESC";
						$strType = "multi";
						$arrdbparam = array();
						$deliveryDetails = query($conn, $strdbsql, $strType, $arrdbparam);
						
						print("<div class='section'>");
						
							print("<table class='order-list table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th>Despatch Date</th>");
									print("<th>Despatch Date</th>");
									print("<th>Despatch Option</th>");
									print("<th>Cost</th>");
									print("<th>Notes</th>");
									print("<th>Packaging</th>");
									print("<th>Order Details</th>");
									print("<th>Stock Details</th>");
									print("<th>Current Status</th>");
									print("<th></th>");
								print("</tr></thead><tbody>");
								
								if (count($deliveryDetails) > 0) {
									foreach($deliveryDetails AS $delivery)
									{
										print("<tr>");
										//delivery date
										print("<td>".fnconvertunixtime($delivery['deliveryDate'],"Ymd")."</td>");
										print("<td>".fnconvertunixtime($delivery['deliveryDate'])."</td>");
									
										//delivery option
										print("<td>".$delivery['optionDesc']."</td>");
										
										//delivery rate / cost
										print("<td>&pound;".number_format($delivery['cost'],2)."</td>");
										
										//notes
										print("<td>".$delivery['notes']."</td>");
										
										//package desc
										print("<td>".$delivery['packageDesc']."</td>");
										
										//order lines
										$strdbsql = "SELECT order_items.*, order_dispatch.description AS dispatchDesc, order_header.timestampOrder, order_header.addDeliveryPostcode, order_header.addDelivery1, order_status.description AS orderStatusDesc, order_payment_status.description AS payDesc FROM deliveryOrders INNER JOIN order_items ON deliveryOrders.orderItemID = order_items.recordID INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID INNER JOIN order_payment_status ON order_header.paymentStatus = order_payment_status.recordID INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID WHERE deliveryOrders.deliveryItemID = :deliveryItemID";
										$strType = "multi";
										$arrdbparam = array("deliveryItemID"=>$delivery['recordID']);
										$deliveryOrderItems = query($conn, $strdbsql, $strType, $arrdbparam);
										
										if (count($deliveryOrderItems) > 0) {
											print("<td>");
												print("Order Date: ".fnconvertunixtime($deliveryOrderItems[0]['timestampOrder'])."<br/>");
												print("Address: ".$deliveryOrderItems[0]['addDelivery1']." ".$deliveryOrderItems[0]['addDeliveryPostcode']."<br/>");
												print("Status: ".$deliveryOrderItems[0]['orderStatusDesc']."<br/>Payment: ".$deliveryOrderItems[0]['payDesc']);
											print("</td>");
											print("<td>");
											foreach ($deliveryOrderItems AS $orderItems) {
												
												$stockDetailsArray = unserialize($orderItems['stockDetailsArray']);
												switch ($orderItems['dispatchDesc']) {
													case "On":
													case "Before":
													case "After":
													case "Xmas":
														$dispatch = $orderItems['dispatchDesc']." ".fnconvertunixtime($orderItems['dispatchDate'])."";
													break;
													default:
														$dispatch = $orderItems['dispatchDesc'];
													break;
												}
												print ($orderItems['quantity']." x ".$stockDetailsArray['stockCode']." - Deliver: ".$dispatch."<br/>");
											}
											print("</td>");
										} else {
											print("<td></td><td></td>");
										}
										
										//delivery status
										print("<td>".$delivery['statusDesc']."</td>");
										print("<td><button type='button' onclick='fnEditDelivery(".$delivery['recordID'].");' class='center btn btn-primary circle'><i class='fa fa-pencil'></i></button></td>");
										print("</tr>");
									}
								}
								print("</tbody>");
							print("</table>");
							
						print("</div>");
						
						print("<div class='buttons'>");
							print("<button onclick='return fnAddDelivery();' type='submit' class='btn btn-success right'>Add</button> ");
						print("</div>");
							
					break;
				}
			print("</form>");
		print("</div>");
	print("</div>");
	
?>
	<!--<script type="text/javascript" src="/js/jquery-ui.js" ></script>-->
	<script type='text/javascript'>
		$().ready(function() {
			$('.order-list').DataTable({
				"order": [[ 0, "desc" ]],
				"bStateSave": true,
				"columnDefs": [
				{
					"targets": [ 0 ],
					"visible": false,
					"searchable": false
				}]
			
			});
			
			$('#frm_deliveryDate').datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true, 
				changeYear: true,
				beforeShowDay: function(date) {
					var day = date.getDay();
					return [(day != 5 && day != 6 && day != 0)];
				}
			});
		});
		
	</script>
	
<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>