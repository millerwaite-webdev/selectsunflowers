<?php
	include("includes/inc_sitecommon.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-gb" xml:lang="en-gb" xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<title>Stollers Furniture World - Voucher</title>
	<meta name="Description" id="description" content="Stollers Furniture World manufacture and supply a range of quality building and home care products. Sign up for an account with our online store or sign in with an existing account."/>
	<meta name="keywords" id="keywords" content="Stollers Building Products, Building Products, UK"/> <!--Include category list-->
</head>
<body style='background:none;'>
	<div style='border-width: 2px; border-style:dashed; width: 540px;' >
		<div id='vouchercode' style='background: url("/images/coffee_voucher.jpg") no-repeat; margin:20px; position:relative;'>
			<span style='position:absolute; top:15px; right:40px;'>
				<?php print $intbasketid; ?>
			</span>
			<img src='/images/coffee_voucher.jpg' alt='Coffee Voucher' />
		</div>
	</div>
	<div style='margin:20px;'>
		<input type="button" onClick="window.print()" value="Print This Page"/>
	</div>
</body>
</html>
