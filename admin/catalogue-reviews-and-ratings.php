<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-reviews-and-ratings.php                                  * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-reviews-and-ratings"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['reviewID'])) $reviewID = $_REQUEST['reviewID']; else $reviewID = "";
	
	switch($strcmd)
	{
		case "disable":
		case "enable":
			
			if ($strcmd == "enable")
			{
				$shownOnSite = 1;
			}
			elseif ($strcmd == "disable")
			{
				$shownOnSite = 0;
			}
			
			$toggleReviewQuery = "UPDATE stock_reviews SET shownOnSite = :shownOnSite WHERE recordID = :recordID";
			$strType = "update";
			$arrdbparams = array(
							"shownOnSite" => $shownOnSite,
							"recordID" => $reviewID
						);
			$toggleReview = query($conn, $toggleReviewQuery, $strType, $arrdbparams);
			
			if ($toggleReview <= 1)
			{
				$strsuccess = "Review toggled";
			}
			elseif ($toggleReview > 1)
			{
				$strwarning = "Review may not have been toggled";
			}
			
			
			$strcmd = "";
			$reviewID = "";
		
		break;
		
		case "saveResponse":
			
			if($_POST['frm_reviewresponse'] == "")
			{
				$dateResponse = 0;
			}
			else
			{
				$dateResponse = time();
			}
			
			$saveResponseQuery = "UPDATE stock_reviews SET response = :response, dateResponse = '".$dateResponse."' WHERE recordID = :recordID";
			$strType = "update";
			$arrdbparams = array(
							"response" => $_POST['frm_reviewresponse'],
							"recordID" => $reviewID
						);
			$saveResponse = query($conn, $saveResponseQuery, $strType, $arrdbparams);
			
			if ($saveResponse <= 1)
			{
				$strsuccess = "Response saved";
			}
			elseif ($saveResponse > 1)
			{
				$strwarning = "Response may not have been saved";
			}
			
			
			$strcmd = "manageResponse";
		
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Reviews and Ratings</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jstoggleReview(reviewID, cmd) {
					document.form.cmd.value=cmd;
					document.form.reviewID.value=reviewID;
					document.form.submit();
				}
				function jsrespondReview(reviewID) {
					document.form.cmd.value="manageResponse";
					document.form.reviewID.value=reviewID;
					document.form.submit();
				}
				function jssaveResponse(reviewID) {
					document.form.cmd.value="saveResponse";
					document.form.reviewID.value=reviewID;
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
			</script>
			<?php
		
			print ("<form action='catalogue-reviews-and-ratings.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='reviewID' id='reviewID' value='$reviewID' />");
				
				switch($strcmd)
				{
					case "manageResponse":
						
						$getReviewQuery = "SELECT stock_reviews.*, customer.title AS custTitle, customer.firstname, customer.surname, stock_group_information.metaDocTitle AS prodName FROM stock_reviews INNER JOIN stock_group_information ON stock_reviews.stockGroupID = stock_group_information.recordID LEFT JOIN customer ON stock_reviews.memberID = customer.recordID WHERE stock_reviews.recordID = :recordID";
						$strType = "single";
						$arrdbparam = array(
							"recordID" => $reviewID
						);
						$review = query($conn, $getReviewQuery, $strType, $arrdbparam);
						
						print ("<table id='sortableStockTable' class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
								print ("<th>Date</th>");
								print ("<th>Stock Title</th>");
								print ("<th>Review Title</th>");
								print ("<th>Review Text</th>");
								print ("<th>Score</th>");
								print ("<th>Name</th>");
							print ("</tr></thead><tbody>");
							print ("<tr>");
								print ("<td>".date("d/m/Y", $review['dateReview'])."</td>");
								print ("<td>".$review['prodName']."</td>");
								print ("<td>".$review['title']."</td>");
								print ("<td>".$review['description']."</td>");
								print ("<td>".$review['numStars']."</td>");
								if(!empty($review['firstname']) && !empty($review['surname']))
								{
									$strName = $review['firstname']." ".$review['surname'];
									if(!empty($review['custTitle']))
									{
										$strName = $review['custTitle']." ".$strName;
									}
								}
								else
								{
									$strName = "Anonymous";
								}
								print ("<td>".$strName."</td>");
							print ("</tr>");
							print ("</tbody>");
						print ("</table>");
						
						print ("<div class='form-group'>
								<label for='frm_reviewresponse' class='col-sm-2 control-label'>Reponse</label>
								<div class='col-sm-5'>
								  <textarea class='form-control' id='frm_reviewresponse' name='frm_reviewresponse' rows='5'>".$review['response']."</textarea>
								</div>
							  </div>");
						print ("<br/>");
						print ("<br/>");
						print ("<button onclick='jssaveResponse(".$review['recordID']."); return false;' class='center btn btn-success' type='submit'>Save Response</button> ");
						print ("<button onclick='jscancel(\"\"); return false;' class='center btn btn-danger' type='submit'>Cancel</button>");
							
					
						break;
						
					default:
						
						print ("<table id='reviews-table' class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
								print ("<th>Date</th>");
								print ("<th>Stock Title</th>");
								print ("<th>Review Title</th>");
								print ("<th>Review Text</th>");
								print ("<th>Score</th>");
								print ("<th>Name</th>");
								print ("<th>Responded On</th>");
								print ("<th>Enable/Disable</th>");
								print ("<th>Respond</th>");
								print ("<th>Delete</th>");
							print ("</tr></thead><tbody>");
							$getReviewsQuery = "SELECT stock_reviews.*, customer.title AS custTitle, customer.firstname, customer.surname, stock_group_information.metaDocTitle AS prodName FROM stock_reviews INNER JOIN stock_group_information ON stock_reviews.stockGroupID = stock_group_information.recordID LEFT JOIN customer ON stock_reviews.memberID = customer.recordID";
							$strType = "multi";
							$reviews = query($conn, $getReviewsQuery, $strType);
							
							if (count($reviews) != 0) {
								foreach ($reviews as $review) {
									print ("<tr>");
										print ("<td>".date("d/m/Y", $review['dateReview'])."</td>");
										print ("<td>".$review['prodName']."</td>");
										print ("<td>".$review['title']."</td>");
										print ("<td>".$review['description']."</td>");
										print ("<td>".$review['numStars']."</td>");
										if(!empty($review['firstname']) && !empty($review['surname']))
										{
											$strName = $review['firstname']." ".$review['surname'];
											if(!empty($review['custTitle']))
											{
												$strName = $review['custTitle']." ".$strName;
											}
										}
										else
										{
											$strName = "Anonymous";
										}
										print ("<td>".$strName."</td>");
										if ($review['dateResponse'] > 0)
										{
											$strDate = date("d/m/Y", $review['dateResponse']);
										}
										else
										{
											$strDate = "N/A";
										}
										print ("<td>".$strDate."</td>");
										if($review['shownOnSite'])
										{
											print ("<td><button onclick='jstoggleReview(".$review['recordID'].", \"disable\"); return false;' class='center btn btn-danger' type='submit'>Disable</button></td>");
										}
										else
										{
											print ("<td><button onclick='jstoggleReview(".$review['recordID'].", \"enable\"); return false;' class='center btn btn-success' type='submit'>Enable</button></td>");
										}
										if($review['response'] == "")
										{
											print ("<td><button onclick='jsrespondReview(".$review['recordID']."); return false;' class='center btn btn-primary' type='submit' style='width: 127px;'>Write Response</button></td>");				
										}
										else
										{
											print ("<td><button onclick='jsrespondReview(".$review['recordID']."); return false;' class='center btn btn-primary' type='submit' style='width: 127px;'>Edit Response</button></td>");				
										}
										print ("<td><button onclick='jsdeleteReview(".$review['recordID']."); return false;' class='center btn btn-danger' type='submit'>Delete</button></td>");				
									print ("</tr>");
								}
							}/* else {
								print ("<tr><td colspan='7'>Currently No Reviews</td></tr>");
							}*/
							print ("</tbody>");
						print ("</table>");
						
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
				
			$('#reviews-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>