<?php
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "import-website-orders"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	function fnAddStock($orderID,$itemSKU,$itemName,$itemOption,$itemQty,$itemPrice,$promocode,$conn){
	
		$strdbsql = "SELECT stock_group_information.*, stock.vatID, stock.recordID AS stockID FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock.stockCode = :stockCode";
		$strType = "single";
		$arrdbparams = array("stockCode"=>$itemSKU);
		$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
		
		if (empty($resultdata['recordID'])) {
			
			//insert into stock tables stock_group_information and stock
			$strdbsql = "INSERT INTO stock_group_information (name, description, dateCreated, statusID, productType) VALUES (:name, :description, UNIX_TIMESTAMP(), 3, 0)";
			$strType = "insert";
			$arrdbparams = array("name"=>$itemSKU,"description"=>$itemName);
			$stockGroupInfoID = query($conn,$strdbsql,$strType,$arrdbparams);

			$strdbsql = "INSERT INTO stock (price, stockCode, groupID, vatID) VALUES (:price, :stockCode, :groupID, 1)";
			$strType = "insert";
			$arrdbparams = array("price"=>$itemPrice,"stockCode"=>$itemSKU,"groupID"=>$stockGroupInfoID);
			$stockID = query($conn,$strdbsql,$strType,$arrdbparams);
			
			//pull out information from insert
			$strdbsql = "SELECT stock_group_information.*, stock.vatID, stock.recordID AS stockID FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock_group_information.recordID = :recordID";
			$strType = "single";
			$arrdbparams = array("recordID"=>$stockGroupInfoID);
			$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
		}
		
			
		$stockDetailsArray = array();
		//check to see if this is a hamper product				
		if ($resultdata['productType'] == 1) {
			$stockDetailsArray['stockCode'] = $itemSKU;
			if (empty($itemOption)) $stockDetailsArray['name'] = $itemName." (Hamper)";
			else $stockDetailsArray['name'] = $itemName." (Hamper) (".$itemOption.")";
			if (empty($itemOption)) $stockDetailsArray['description'] = $resultdata['description'];
			else $stockDetailsArray['description'] = $resultdata['description']." (".$itemOption.")";
		} else {
			$stockDetailsArray['stockCode'] = $itemSKU;
			if (empty($itemOption)) $stockDetailsArray['name'] = $itemName;
			else $stockDetailsArray['name'] = $itemName." (".$itemOption.")";
			if (empty($itemOption)) $stockDetailsArray['description'] = $resultdata['description'];
			else $stockDetailsArray['description'] = $resultdata['description']." (".$itemOption.")";
		}
		$quantityAdd = ($itemQty > 0) ? $itemQty : 1;
		$discountPerc = 0;
		$priceAdd = $itemPrice;
		$vatSelectAdd = (!empty($resultdata['vatID'])) ? $resultdata['vatID'] : 1;
		
		//insert record into details table
		$strdbsql = "INSERT INTO order_items(orderHeaderID, stockID, stockDetailsArray, quantity, price, discountPerc, vatID, display, hamperOrderID) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price, :discountPerc, :vatID, 1, :hamperOrderID);";
		$strType = "insert";
		$arrdbparams = array(
			"orderHeaderID"=>$orderID, 
			"stockID"=>$resultdata['stockID'], 
			"stockDetailsArray"=>serialize($stockDetailsArray), 
			"quantity"=>$quantityAdd, 
			"price"=>$priceAdd, 
			"discountPerc"=>$discountPerc, 
			"vatID"=>$vatSelectAdd,
			"hamperOrderID"=>0
		);
		$orderInsert = query($conn,$strdbsql,$strType,$arrdbparams);
		
		//if hamper then insert the individual products into our order details table
		if ($resultdata['productType'] == 1) {
		
			//update original hamper detail with its own ID
			$strdbsql = "UPDATE order_items SET hamperOrderID = :hamperOrderID WHERE recordID = :recordID";
			$strType = "update";
			$arrdbparams = array(
				"recordID"=>$orderInsert,
				"hamperOrderID"=>$orderInsert
			);
			$orderDetailUpdate = query($conn,$strdbsql,$strType,$arrdbparams);
		
			$strdbsql = "SELECT count(*) AS qtyToAdd, stock_group_information.name, stock_group_information.description, stock.*, stock_status.description AS statusDesc FROM stock_hamper INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_hamper.stockGroupID = :hamperID GROUP BY stockCode";
			$arrType = "multi";
			$strdbparams = array("hamperID"=>$resultdata['recordID']);
			$resultdata2 = query($conn, $strdbsql, $arrType, $strdbparams);
		
			foreach($resultdata2 AS $row2){
			
				$stockDetailsArray = array();
				$stockDetailsArray['stockCode'] = $row2['stockCode'];
				$stockDetailsArray['name'] = $row2['name'];
				$stockDetailsArray['description'] = $row2['description'];
				$vatSelectAdd = (!empty($row2['vatID'])) ? $row2['vatID'] : 1;
				
				//insert record into details table
				$strdbsql = "INSERT INTO order_items(orderHeaderID, stockID, stockDetailsArray, quantity, price, discountPerc, vatID, display, hamperOrderID) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price, :discountPerc, :vatID, 0, :hamperOrderID);";
				$strType = "insert";
				$arrdbparams = array(
					"orderHeaderID"=>$orderID, 
					"stockID"=>$row2['recordID'], 
					"stockDetailsArray"=>serialize($stockDetailsArray), 
					"quantity"=>$row2['qtyToAdd'] * $quantityAdd, 
					"price"=>0, 
					"discountPerc"=>0, 
					"vatID"=>$vatSelectAdd,
					"hamperOrderID"=>$orderInsert
				);
				$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
			}
		}
	}
	function fnMemberAddReturn($strtitle,$strfirstname,$strsurname,$strcompany,$stremail,$strphone,$strmobile,$deliverName,$strbilltitle,$strbillfirstname,$strbillsurname,$strbilladd1,$strbilladd2,$strbilladd3,$strbillTown,$strbillCnty,$strbillCntry,$strbillPstCde,$strdeltitle,$strdelfirstname,$strdelsurname,$strdeladd1,$strdeladd2,$strdeladd3,$strdelTown,$strdelCnty,$strdelCntry,$strdelPstCde,$conn){
		
		//check if member already exists, if yes get id otherwise create new 
		$strsql = "SELECT customer.* FROM customer INNER JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE LOWER(REPLACE(customer_address.add1, ' ', '')) = :billadd1 AND LOWER(REPLACE(customer_address.postcode, ' ', '')) = :billaddpostcode AND LOWER(REPLACE(customer.firstname, ' ', '')) = :firstname AND LOWER(REPLACE(customer.surname, ' ', '')) = :surname";
		$strType = "single";
		$arrdbparams = array(
			"firstname" => strtolower(str_replace(' ', '',$strfirstname)),
			"surname" => strtolower(str_replace(' ', '',$strsurname)),
			"billadd1" => strtolower(str_replace(' ', '',$strbilladd1)),
			"billaddpostcode" => strtolower(str_replace(' ', '',$strbillPstCde))
		);
		$resultdata = query($conn, $strsql, $strType, $arrdbparams);
		
		if (!empty($resultdata['recordID'])) { //use existing details
			
			$customerAccountID = $resultdata['recordID'];
		
		} else { //create new member

			//create account
			$createAccountQuery = "INSERT INTO customer (groupID, username, password, title, firstname, surname, company, telephone, mobile, email) VALUES ('1', :username, :password, :title, :firstname, :surname, :company, :telephone, :mobile, :email)";
			$strType = "insert";
			$arrdbparams = array(
				"username" =>"",
				"password" =>"",
				"title" => $strtitle,
				"firstname" => $strfirstname,
				"surname" => $strsurname,
				"company" => $strcompany,
				"telephone" => $strphone,
				"mobile" => $strmobile,
				"email" => $stremail
			);
			$customerAccountID = query($conn, $createAccountQuery, $strType, $arrdbparams);
			
			if($customerAccountID)
			{	
				//create account with two addresses
				$insertAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Delivery Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
				$strType = "insert";
				$arrdbparams = array(
					"customerID" => $customerAccountID,
					"title" => $strdeltitle,
					"firstname" => $strdelfirstname,
					"surname" => $strdelsurname,
					"add1" => $strdeladd1,
					"add2" => $strdeladd2,
					"add3" => $strdeladd3,
					"town" => $strdelTown,
					"county" => $strdelCnty,
					"postcode" => $strdelPstCde
				);
				$delAddID = query($conn, $insertAddressQuery, $strType, $arrdbparams);
				
				$billAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Billing Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
				$strType = "insert";
				$arrdbparams = array(
					"customerID" => $customerAccountID,
					"title" => $strbilltitle,
					"firstname" => $strbillfirstname,
					"surname" => $strbillsurname,
					"add1" => $strbilladd1,
					"add2" => $strbilladd2,
					"add3" => $strbilladd3,
					"town" => $strbillTown,
					"county" => $strbillCnty,
					"postcode" => $strbillPstCde
				);
				$billAddID = query($conn, $billAddressQuery, $strType, $arrdbparams);
				$values = array(
					"billingadd" => $billAddID,
					"deliveryadd" => $delAddID,
					"custid" => $customerAccountID
				);
				$assignCustomerAddresses = updateUserBillDelAddresses($values, "billanddel", $conn);
			}
		}
		
		return $customerAccountID;
	}
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['importID'])) $importID = $_REQUEST['importID']; else $importID = "";
	if (isset($_REQUEST['importConfigID'])) $importConfigID = $_REQUEST['importConfigID']; else $importConfigID = "";
	
	switch($strcmd){
		case "insertOrders":
				
			//file information
			$strdbsql = "SELECT * FROM fileImport WHERE importID = :importID AND status != 3";
			$arrdbparams = array("importID"=>$importID);
			$strType = "single";
			$fileInformation = query($conn, $strdbsql, $strType, $arrdbparams);
						
			//import format information
			$strdbsql = "SELECT configFileImportFormats.*, configFileImportWorksheets.* FROM configFileImportFormats INNER JOIN configFileImportWorksheets ON configFileImportFormats.importID = configFileImportWorksheets.importID WHERE configFileImportFormats.importID = :importID AND configFileImportWorksheets.importIndex = :worksheetID";
			$arrdbparams = array("importID"=>$fileInformation['configID'],"worksheetID"=>$fileInformation['worksheetID']);
			$strType = "single";
			$formatInformation = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if (!empty($fileInformation)) {

				$arrconcat = unserialize($formatInformation['columnConcatenation']);
				$columnDataMatches = unserialize($formatInformation['columnDataMatches']);
				$columnConversion = unserialize($formatInformation['columnConversion']);
				$columnExplode = unserialize($formatInformation['columnExplode']);

				$importReference = $fileInformation['reference'];
				$strdbsql = "SELECT fileImportOrders.recordID AS orderRecordID, fileImportOrders.*, fileImportData.* FROM fileImportOrders INNER JOIN fileImportData ON fileImportOrders.dataID = fileImportData.recordID WHERE fileImportOrders.importID=:importID";		
				$strType = "multi";
				$arrdbparams = array("importID"=>$importID);
				$resultdatamulti = query($conn, $strdbsql, $strType, $arrdbparams);
				
				foreach ($resultdatamulti as $row) {
				
					//get the data match fields
					$dataRow = unserialize($row['dataArray']);
					$externalOrderID = getValueUsingKey ("frm_orderID", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$timestamp = getValueUsingKey ("frm_timestamp", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strtitle = "";
					$strfirstname = getValueUsingKey ("frm_firstname", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strsurname = getValueUsingKey ("frm_surname", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strcompany = "";
					$voucherCode = getValueUsingKey ("frm_voucherCode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$price = getValueUsingKey ("frm_price", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$stremail = getValueUsingKey ("frm_email", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strphone = getValueUsingKey ("frm_telephone", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strmobile = "";					
					$strfax = "";
					$paymentType = getValueUsingKey ("frm_paymenttype", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$orderStatusDesc = getValueUsingKey ("frm_orderstatus", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$deliveryPrice = getValueUsingKey ("frm_deliveryprice", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$tax = getValueUsingKey ("frm_tax", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$deliverName = getValueUsingKey ("frm_deliverName", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$deliverPrice = getValueUsingKey ("frm_deliverPrice", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$instructions = getValueUsingKey ("frm_instructions", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$giftMessage = getValueUsingKey ("frm_message", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$deliveryDate = getValueUsingKey ("frm_deliveryDate", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbilltitle = "";
					$strbillfirstname = getValueUsingKey ("frm_firstname", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbillsurname = getValueUsingKey ("frm_surname", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbilladd1 = getValueUsingKey ("frm_billAdd1", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbilladd2 = getValueUsingKey ("frm_billAdd2", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbilladd3 = "";
					$strbillTown = getValueUsingKey ("frm_billTown", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbillCnty = getValueUsingKey ("frm_billCounty", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbillCntry = getValueUsingKey ("frm_billCountry", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strbillPstCde = getValueUsingKey ("frm_billPostcode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strdeltitle = "";
					$strdelfirstname = getValueUsingKey ("frm_deliverName", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strdelsurname = "";
					$strdeladd1 = getValueUsingKey ("frm_delAdd1", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strdeladd2 = getValueUsingKey ("frm_delAdd2", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strdeladd3 = "";
					$strdelTown = getValueUsingKey ("frm_delTown", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strdelCnty = getValueUsingKey ("frm_delCounty", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strdelCntry = getValueUsingKey ("frm_delCountry", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$strdelPstCde = getValueUsingKey ("frm_delPostcode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$itemSKU = getValueUsingKey ("frm_itemSKU", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$itemName = getValueUsingKey ("frm_itemName", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$itemOption = getValueUsingKey ("frm_itemOption", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$itemQty = getValueUsingKey ("frm_itemQty", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$itemPrice = getValueUsingKey ("frm_itemPrice", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$promocode = getValueUsingKey ("frm_promocode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
					$orderSource = 7; //7 is web
					$orderDate = strtotime(str_replace("/","-",$timestamp));
					$dispatchAdd = 1; //1 = on
					if (empty($deliveryDate)) $deliveryDate = date("d-m-Y");
					$dispatchAddDate = strtotime(str_replace("/","-",$deliveryDate));
					$dispatchAddDate = strtotime('-1 day', $dispatchAddDate); //remove a day as the customer selects delivery date not dispatch date
					$weekday = date('N', $dispatchAddDate);
					$allowedWeekdays = array("1","2","3","4");
					if (!in_array($weekday,$allowedWeekdays)) {
						while(!in_array($weekday,$allowedWeekdays)){
							$dispatchAddDate = strtotime('+1 day', $dispatchAddDate);
							$weekday = date('N', $dispatchAddDate);
						}
					}
					
					if ($orderStatusDesc != "Payment Declined"){
					
						//check to see if we have already imported this order (just adding extra detail lines)
						$strdbsql = "SELECT * FROM order_header WHERE externalOrderID = :externalOrderID";
						$strType = "single";
						$arrdbparams = array("externalOrderID"=>$externalOrderID);
						$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
						
						if (empty($resultdata['recordID'])) { //order hasnt already been imported
							
							//check if member already exists, if yes get id otherwise create new 
							$customerID = fnMemberAddReturn($strtitle,$strfirstname,$strsurname,$strcompany,$stremail,$strphone,$strmobile,$deliverName,$strbilltitle,$strbillfirstname,$strbillsurname,$strbilladd1,$strbilladd2,$strbilladd3,$strbillTown,$strbillCnty,$strbillCntry,$strbillPstCde,$strdeltitle,$strdelfirstname,$strdelsurname,$strdeladd1,$strdeladd2,$strdeladd3,$strdelTown,$strdelCnty,$strdelCntry,$strdelPstCde,$conn);

							$addDeliveryCustomerID = fnMemberAddReturn($strdeltitle,$strdelfirstname,$strdelsurname,"","","","",$deliverName,$strdeltitle,$strdelfirstname,$strdelsurname,$strdeladd1,$strdeladd2,$strdeladd3,$strdelTown,$strdelCnty,$strdelCntry,$strdelPstCde,$strdeltitle,$strdelfirstname,$strdelsurname,$strdeladd1,$strdeladd2,$strdeladd3,$strdelTown,$strdelCnty,$strdelCntry,$strdelPstCde,$conn);
							
							//work out order status based on the 
							switch($orderStatusDesc){
								case "Payment Declined":
									$orderStatus = 1; //1 = order placed, 2 = processing, 3 = cancelled, 4 = despatched
									$paymentStatusAdd = 3; //3 = refused
								break;
								case "Paid: awaiting despatch":
									$orderStatus = 1;
									$paymentStatusAdd = 2; //2 = authorised
								break;
								case "Order Completed":
									$orderStatus = 1;
									$paymentStatusAdd = 2; //2 = authorised
								break;
								default:
									$orderStatus = 1; //1 = order placed, 2 = processing, 3 = cancelled, 4 = despatched
									$paymentStatusAdd = 1; //1 = unknown
								break;
							}
							
							$strdbsql = "INSERT INTO order_header (customerID, timestampOrder, addDeliveryTitle, addDeliveryFirstname, addDeliverySurname, addDelivery1, addDelivery2, addDelivery3, addDelivery4, addDelivery5, addDelivery6, addDeliveryPostcode, addDeliveryMessage, addBillingTitle, addBillingFirstname, addBillingSurname, addBilling1, addBilling2, addBilling3, addBilling4, addBilling5, addBilling6, addBillingPostcode, telephone, email, notes, orderStatus, orderSource, giftMessage, dispatch, dispatchDate, externalOrderID,addDeliveryCustomerID) VALUES (:customerID, :timestampOrder, :addDeliveryTitle, :addDeliveryFirstname, :addDeliverySurname, :addDelivery1, :addDelivery2, :addDelivery3, :addDelivery4, :addDelivery5, :addDelivery6, :addDeliveryPostcode, :addDeliveryMessage, :addBillingTitle, :addBillingFirstname, :addBillingSurname, :addBilling1, :addBilling2, :addBilling3, :addBilling4, :addBilling5, :addBilling6, :addBillingPostcode, :telephone, :email, :notes, :orderStatus, :orderSource, :giftMessage, :dispatch, :dispatchDate, :externalOrderID,:addDeliveryCustomerID);";
							$strType = "insert";
							$arrdbparams = array(
								"customerID"=>$customerID, 
								"timestampOrder"=>$orderDate, 
								"addDeliveryCustomerID"=>$addDeliveryCustomerID, 
								"addDeliveryTitle"=>$strdeltitle, 
								"addDeliveryFirstname"=>$strdelfirstname, 
								"addDeliverySurname"=>$strdelsurname, 
								"addDelivery1"=>$strdeladd1, 
								"addDelivery2"=>$strdeladd2, 
								"addDelivery3"=>$strdeladd3, 
								"addDelivery4"=>$strdelTown, 
								"addDelivery5"=>$strdelCnty, 
								"addDelivery6"=>$strdelCntry, 
								"addDeliveryPostcode"=>$strdelPstCde, 
								"addDeliveryMessage"=>"", 
								"addBillingTitle"=>$strbilltitle, 
								"addBillingFirstname"=>$strbillfirstname, 
								"addBillingSurname"=>$strbillsurname, 
								"addBilling1"=>$strbilladd1, 
								"addBilling2"=>$strbilladd2, 
								"addBilling3"=>$strbilladd3, 
								"addBilling4"=>$strbillTown, 
								"addBilling5"=>$strbillCnty, 
								"addBilling6"=>$strbillCntry, 
								"addBillingPostcode"=>$strbillPstCde, 
								"telephone"=>$strphone, 
								"email"=>$stremail, 
								"notes"=>$instructions, 
								"orderStatus"=>$orderStatus, 
								"orderSource"=>$orderSource,
								"giftMessage"=>$giftMessage,
								"dispatch"=>$dispatchAdd, 
								"dispatchDate"=>$dispatchAddDate,
								"externalOrderID"=>$externalOrderID
							);
							$orderID = query($conn,$strdbsql,$strType,$arrdbparams);

							if (!empty($orderID)){
								
								//add payment 
								$paymentDateAdd = strtotime(str_replace("/","-",$timestamp));
								$paymentSourceAdd = $paymentType;
								$paymentAmountAdd = $price;

								//insert record into details table
								$strdbsql = "INSERT INTO order_payment_details(orderHeaderID, transactionSource, transactionTimestamp, transactionAmount, paymentStatus) VALUES (:orderHeaderID, :transactionSource, :transactionTimestamp, :transactionAmount, :paymentStatus);";
								$strType = "insert";
								$arrdbparams = array(
									"orderHeaderID"=>$orderID, 
									"transactionSource"=>$paymentSourceAdd, 
									"transactionTimestamp"=>$paymentDateAdd, 
									"transactionAmount"=>number_format($paymentAmountAdd,2), 
									"paymentStatus"=>$paymentStatusAdd
								);
								$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);

								//add stock
								fnAddStock($orderID,$itemSKU,$itemName,$itemOption,$itemQty,$itemPrice,$promocode,$conn);
								
								//add delivery line
								$deliverPriceVAT = abs(number_format(($deliverPrice / (1+(20 / 100)) - $deliverPrice),5));
								$deliverPrice -= $deliverPriceVAT;
								fnAddStock($orderID,"AM-A","Delivery","",1,number_format($deliverPrice,2),"",$conn);

							}
							
						} else { //order has been imported so add the extra item
							
							//add stock
							$orderID = $resultdata['recordID'];
							fnAddStock($orderID,$itemSKU,$itemName,$itemOption,$itemQty,$itemPrice,$promocode,$conn);
						}
						
						
						
						//update / add delivery
						$deliveryOption = 49; //not sure what this should be, cant pick it up from import
						$packageType = 12; //not sure what this should be, cant pick it up from import
						$serviceCode = "A";
						$arrParams = array("deliveryDate" => $dispatchAddDate, "deliveryOptionRateID" => $deliveryOption, "packageTypeID" => $packageType, "notes" =>"", "serviceCode"=> $serviceCode);
						
						
						//get header details of submitted order
						$strdbsql = "SELECT * FROM order_header WHERE recordID = :orderID";
						$strType = "single";
						$arrParam = array("orderID"=>$orderID);
						$orderHeaderDetails = query($conn, $strdbsql, $strType, $arrParam);
					
						if (!empty($orderHeaderDetails['recordID'])) {
												
							//get order item details
							$strdbsql = "SELECT * FROM order_items WHERE orderHeaderID = :orderID";
							$strType = "multi";
							$arrParam = array("orderID"=>$orderHeaderDetails['recordID']);
							$orderItemDetails = query($conn, $strdbsql, $strType, $arrParam);
							
							//set customer from order header
							$arrParams['customerID'] = $orderHeaderDetails['customerID'];
							$arrParams['orderHeaderID'] = $orderHeaderDetails['recordID'];
							
							//check if we already have a deliveryID (editing existing)
							$strdbsql = "SELECT * FROM delivery WHERE orderHeaderID = :orderID";
							$strType = "single";
							$arrParam = array("orderID"=>$orderID);
							$deliveryDetails = query($conn, $strdbsql, $strType, $arrParam);
							
							if(!empty($deliveryDetails['recordID']))
							{
								$deliveryID = $deliveryDetails['recordID'];
								
								//update
								$strdbsql = "UPDATE delivery SET orderHeaderID=:orderHeaderID, deliveryDate = :deliveryDate, deliveryOptionRateID = :deliveryOptionRateID, customerID = :customerID, packageTypeID = :packageTypeID, notes = :notes, serviceCode = :serviceCode WHERE recordID = :deliveryID";
								$strType = "update";
								$arrParams['deliveryID'] = $deliveryID;
								$updateDelivery = query($conn, $strdbsql, $strType, $arrParams);
							}
							else
							{
								//insert
								$strdbsql = "INSERT INTO delivery (deliveryDate, deliveryOptionRateID, customerID, packageTypeID, notes, orderHeaderID, serviceCode) VALUES (:deliveryDate, :deliveryOptionRateID, :customerID, :packageTypeID, :notes, :orderHeaderID, :serviceCode)";
								$strType = "insert";
								$deliveryID = query($conn, $strdbsql, $strType, $arrParams);
							}
							
							if($deliveryID != "")
							{
								//delete order items from delivery table as we are going to re-create
								$strdbsql = "DELETE FROM deliveryOrders WHERE deliveryItemID = :deliveryItemID";
								$strType = "delete";
								$arrParams = array("deliveryItemID" => $deliveryID);
								$deleteResult = query($conn, $strdbsql, $strType,$arrParams);
								
								//insert each order item into the delivery table
								foreach($orderItemDetails AS $orderItem)
								{
									$strdbsql = "INSERT INTO deliveryOrders (deliveryItemID, orderItemID) VALUES (:deliveryItemID,:orderItemID)";
									$strType = "insert";
									$arrParams = array("deliveryItemID"=>$deliveryID,"orderItemID"=>$orderItem['recordID']);
									$deleteResult = query($conn, $strdbsql, $strType,$arrParams);
								}
								
								//check weights
								$strdbsql = "SELECT maxWeight FROM deliveryOptionRates WHERE recordID = :deliveryOptionRateID";
								$strType = "single";
								$arrParam = array("deliveryOptionRateID" => $deliveryOption);
								$maxWeightArr = query($conn, $strdbsql, $strType, $arrParam);
								$maxWeight = $maxWeightArr['maxWeight'];
								$totalOrderWeight = 0.00;
								
								//get package type weight
								$strdbsql = "SELECT weight FROM deliveryPackageTypes WHERE recordID = :packageTypeID";
								$strType = "single";
								$arrParam = array("packageTypeID" => $packageType);
								$packageWeightArr = query($conn, $strdbsql, $strType, $arrParam);
								
								$totalOrderWeight += $packageWeightArr['weight'];
								
								$itemsNotAdded = "";
								$itemsAdded = "";
								
								//stock items weight + package weight < delivery option rate max weight
								foreach($orderItemDetails AS $orderItem)
								{
									$strdbsql = "SELECT (stock.weight * order_items.quantity) AS orderItemWeight, stock_group_information.name FROM stock INNER JOIN order_items ON stock.recordID = order_items.stockID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE order_items.recordID = :recordID";
									$strType = "single";
									$arrParam = array("recordID"=>$orderItem['recordID']);
									$stockWeightArr = query($conn, $strdbsql, $strType, $arrParam);
									$orderItemWeight = $stockWeightArr['orderItemWeight'];
									$totalOrderWeight += $orderItemWeight;
								}
							
								$strdbsql = "UPDATE delivery SET weight = :weight WHERE recordID = :recordID";
								$strType = "update";
								$arrParam = array("weight" => $totalOrderWeight, "recordID" => $deliveryID);
								query($conn, $strdbsql, $strType, $arrParam);
							}
						}
						
						
						
					}
				}
				$strcmd = "";
				$importConfigID = "";
				error_log("Marking fileImport and fileImportData with ID - $importID - as complete");
				
				//update fileImport
				$strdbsql = "UPDATE fileImport SET status = 3 WHERE importID = :importID";
				$arrdbparams = array("importID"=>$importID);
				$strType = "update";
				$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
				
				$strsuccess = "Successfully imported records";
				
			} else {
				$strerror = "Import already processed or not found";
			}
		
		break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>".$configPageDetails['pageTitle']."</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

?>	
<script language='Javascript'>
	function fnSelectImport() {
		document.getElementById("importID").value = document.getElementById("frm_importID").value;
		document.getElementById("importConfigID").value = document.getElementById("frm_importConfigID").value;
		document.getElementById("cmd").value = "processImport";
		document.getElementById("form").submit();
	}
	function fnCheckOrders(){
		document.getElementById("cmd").value = "checkOrders";
		document.getElementById("form").submit();
	}
	function fnInsertOrders() {
		document.getElementById("cmd").value = "insertOrders";
		document.getElementById("form").submit();
	}
	function fnReProcess(){
		document.getElementById("cmd").value = "checkOrders";
		document.getElementById("form").submit();
	}
	function fnCancel() {
		document.getElementById("importID").value = "";
		document.getElementById("importConfigID").value = "";
		document.getElementById("cmd").value = "";
		document.getElementById("form").submit();
	}
	
</script>
<?php
			print ("<div class='section' style='overflow:auto;'>");
			print ("<form action='import-website-orders.php' class='uniForm' method='post' name='form' id='form'>");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' value='$strcmd'/>");
			print ("<input type='hidden' class='nodisable' name='importID' id='importID' value='$importID'/>");
			print ("<input type='hidden' class='nodisable' name='importConfigID' id='importConfigID' value='$importConfigID'/>");

			switch ($strcmd) {
				case "checkOrders":

					if ($importID != "") {
					
						//file information
						$strdbsql = "SELECT * FROM fileImport WHERE importID = :importID";
						$arrdbparams = array("importID"=>$importID);
						$strType = "single";
						$fileInformation = query($conn, $strdbsql, $strType, $arrdbparams);
											
						//import format information
						$strdbsql = "SELECT configFileImportFormats.*, configFileImportWorksheets.* FROM configFileImportFormats INNER JOIN configFileImportWorksheets ON configFileImportFormats.importID = configFileImportWorksheets.importID WHERE configFileImportFormats.importID = :importID AND configFileImportWorksheets.importIndex = :worksheetID";
						$arrdbparams = array("importID"=>$importConfigID,"worksheetID"=>$fileInformation['worksheetID']);
						$strType = "single";
						$formatInformation = query($conn, $strdbsql, $strType, $arrdbparams);
						
						//get data rows
						$strdbsql = "SELECT * FROM fileImportData WHERE importID = :importID";
						$arrdbparams = array("importID"=>$importID);
						$strType = "multi";
						$datarows = query($conn, $strdbsql, $strType, $arrdbparams);
						
						//variables for insert into payments						
						$importReference = $fileInformation['reference'];
						$errorResults = "";
						$warningResults = "";
						$infoResults = "";
						$successResults = "";
						$goodResults = "";

						//get highest row of data and then loop around all rows
						$noofdatarows = count($datarows);
						$paymentAmountErrors = 0;
						$paymentAmountWarnings = 0;
						$paymentAmountInfo = 0;
						$paymentAmountSuccess = 0;
						for ($j = 0; $j < $noofdatarows; $j++) {
							if (($formatInformation['noIgnoreTop'] <= $j) AND (($noofdatarows - $formatInformation['noIgnoreBottom']) > $j)) {
								
								$paymentAmount = 0;
								//check to see if the row has already been matched 
								$strdbsql = "SELECT * FROM fileImportOrders WHERE importID = :importID AND dataID = :rowID";
								$arrdbparams = array("importID"=>$importID, "rowID"=>$datarows[$j]['recordID']);
								$strType = "multi";
								$rowmatched = query($conn, $strdbsql, $strType, $arrdbparams);
								$dataRow = unserialize($datarows[$j]['dataArray']);
								
								if (count($rowmatched) == 0) { //hasnt been added as a successful member import yet
								
									//reset values
									$errors = array();
									$warnings = array();
									$info = array();
								
									//get the data match fields
									$columnDataMatches = unserialize($formatInformation['columnDataMatches']);
									$arrconcat = unserialize($formatInformation['columnConcatenation']);
									$columnConversion = unserialize($formatInformation['columnConversion']);
									$columnExplode = unserialize($formatInformation['columnExplode']);
									
									//check the unique reference hasnt already been imported
									$uniqueReference = null;
									if ($formatInformation['columnUniqueRef'] > 0) {
									
										//get the unique value using the column ID
										$uniqueReference = $dataRow[$formatInformation['columnUniqueRef']-1];
										
										$strdbsql = "SELECT fileImportOrders.* FROM fileImportOrders INNER JOIN fileImport ON fileImportOrders.importID = fileImport.importID WHERE fileImport.configID = :configID AND fileImport.worksheetID = :worksheetID AND fileImportOrders.uniqueReference = :uniqueReference";
										$arrdbparams = array("configID"=>$importConfigID, "worksheetID"=>$fileInformation['worksheetID'], "uniqueReference"=>$uniqueReference);
										$strType = "multi";
										$alreadyImported = query($conn, $strdbsql, $strType, $arrdbparams);
										
										if (count($alreadyImported) > 0) {
											$errors[] = "The unique reference ($uniqueReference) for this import line has already been processed. Please check the file doesnt contain duplicate unique references.";
										}
									}
									
									$paymentCheckColumnValue = getValueUsingKey ("frm_paymentCheckColumn", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$paymentCheckValue = getValueUsingKey ("paymentCheckAcceptValue", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									if ($paymentCheckColumnValue != "" AND $paymentCheckValue != "") {
										if ($paymentCheckColumnValue != $paymentCheckValue ) {
											$errors[] = "The payment for this row has not been successful, this row will not be imported.";
										}										
									}
								
									//check the required data fields are available
									foreach ($columnDataMatches AS $key => $column) {
										//if column is required make sure it has a value
										if (!empty($column['required'])) {
											if ($column['required'] == 1) {
												$check = checkRequiredDataColumns($dataRow,$key,$column,$columnDataMatches, $arrconcat, $columnConversion, $columnExplode);
												if ($check != "") $errors[] = $check;
											}
										}
									}
									

									$orderID = getValueUsingKey ("frm_orderID", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$timestamp = getValueUsingKey ("frm_timestamp", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$firstname = getValueUsingKey ("frm_firstname", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$surname = getValueUsingKey ("frm_surname", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$voucherCode = getValueUsingKey ("frm_voucherCode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$price = getValueUsingKey ("frm_price", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$email = getValueUsingKey ("frm_email", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$telephone = getValueUsingKey ("frm_telephone", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$paymentType = getValueUsingKey ("frm_paymenttype", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$orderStatus = getValueUsingKey ("frm_orderstatus", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$deliveryPrice = getValueUsingKey ("frm_deliveryprice", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$tax = getValueUsingKey ("frm_tax", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$deliverName = getValueUsingKey ("frm_deliverName", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$instructions = getValueUsingKey ("frm_instructions", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$message = getValueUsingKey ("frm_message", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$deliveryDate = getValueUsingKey ("frm_deliveryDate", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$billName = getValueUsingKey ("frm_billName", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$billAdd1 = getValueUsingKey ("frm_billAdd1", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$billAdd2 = getValueUsingKey ("frm_billAdd2", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$billTown = getValueUsingKey ("frm_billTown", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$billCounty = getValueUsingKey ("frm_billCounty", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$billCountry = getValueUsingKey ("frm_billCountry", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$billPostcode = getValueUsingKey ("frm_billPostcode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$delName = getValueUsingKey ("frm_delName", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$delAdd1 = getValueUsingKey ("frm_delAdd1", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$delAdd2 = getValueUsingKey ("frm_delAdd2", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$delTown = getValueUsingKey ("frm_delTown", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$delCounty = getValueUsingKey ("frm_delCounty", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$delCountry = getValueUsingKey ("frm_delCountry", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$delPostcode = getValueUsingKey ("frm_delPostcode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$itemSKU = getValueUsingKey ("frm_itemSKU", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$itemName = getValueUsingKey ("frm_itemName", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$itemOption = getValueUsingKey ("frm_itemOption", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$itemQty = getValueUsingKey ("frm_itemQty", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$itemPrice = getValueUsingKey ("frm_itemPrice", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									$promocode = getValueUsingKey ("frm_promocode", $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
									
									
									//may need to do some tests here to ensure data is correct
									
									
									//check to see if any options have been selected for this import row
									if (isset($_REQUEST['row'])){
										if ($_REQUEST['row'][$j]['import'] != "") {
											if ($_REQUEST['row'][$j]['import'] == 0) { //do not import
												$warnings = array();
												$info[] = "This line has been selected not to be imported.";
											}
										}
									}

									if (count($errors) != 0 AND count($info) == 0) {
									
										$errorlist = "";
										foreach ($errors AS $error) {
											$errorlist .= $error."<br/>";
										}
										$errorResults .= "<tr>";
										$errorResults .= ("<td class='danger'>$errorlist</td>");
										for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
											$errorResults .= ("<td class='danger'>".$dataRow[$i]."</td>");
										}
										$errorResults .= "</tr>";
									
									} else if (count($warnings) != 0 AND count($info) == 0) {
									
										$warninglist = "";
										foreach ($warnings AS $warning) {
											$warninglist .= $warning."<br/>";
										}
										$warningResults .= "<tr>";
										$warningResults .= ("<td class='warning'>$warninglist</td>");
										$warningResults .= ("<td class='warning'></td>");
										for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
											$warningResults .= ("<td class='warning'>".$dataRow[$i]."</td>");
										}
										$warningResults .= "</tr>";
									
									} else if (count($info) != 0) {
									
										$infolist = "";
										foreach ($info AS $row) {
											$infolist .= $row."<br/>";
										}
										$infoResults .= "<tr>";
										$infoResults .= ("<td class='info'>$infolist<input type='hidden' name='row[$j][import]' id='row[$j][import]' value='0'/></td>");
										for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
											$infoResults .= ("<td class='info'>".$dataRow[$i]."</td>");
										}
										$infoResults .= "</tr>";

									} else { //no errors for this data row
										
										$goodResults .= "<tr>";
										for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
											$goodResults .= ("<td class='success'>".$dataRow[$i]."</td>");
										}
										$goodResults .= "</tr>";
									
										//insert the rowID into the fileImportOrders table ready to be processed
										$strdbsql = "INSERT INTO fileImportOrders (importID, dataID, status, uniqueReference, orderID) VALUES (:importID, :dataID, 0, :uniqueReference, :orderID);";
										$arrdbparams = array("importID"=>$importID,"dataID"=>$datarows[$j]['recordID'],"uniqueReference"=>$uniqueReference,"orderID"=>$orderID);
										$strType = "insert";
										$insertResult = query($conn, $strdbsql, $strType, $arrdbparams);
									}

								} else {
									$goodResults .= "<tr>";
									for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
										$goodResults .= ("<td class='success'>".$dataRow[$i]."</td>");
									}
									$goodResults .= "</tr>";
								}
							}
						}
						
						
						//print list of members with errors / warnings
						if ($warningResults != ""){
							print ("<div class='col-sm-12' style='overflow:auto;'>");
							print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
							print ("<th>Warning</th>");
							print ("<th>Action</th>");
							$columnNames = explode(",", $formatInformation['columnNames']);
							for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
								print ("<th>".$columnNames[$i]."</th>");
							}
							print ("</tr></thead><tbody>");
							print $warningResults;
							print ("</tbody>");
							print ("</table>");
							print ("</div>");
							
							print ("<div class='col-sm-12'>");
								print ("<div class='form-group'>");						
									print ("<button onclick='fnReProcess(); return false;' class='btn btn-primary right'>Update</button>");
								print ("</div>");
							print ("</div>");
						}
						
						//print list of members with errors that wont import
						if ($errorResults != ""){
						
							print ("<div class='col-sm-12' style='overflow:auto;'>");
							print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
							print ("<th>Error</th>");
							$columnNames = explode(",", $formatInformation['columnNames']);
							for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
								print ("<th>".$columnNames[$i]."</th>");
							}
							print ("</tr></thead><tbody>");
							print $errorResults;
							print ("</tbody>");
							print ("</table>");
							print ("</div>");
							
							print ("<div class='col-sm-12'>");
								print ("<div class='form-group'>");						
									print ("<button onclick='fnReProcess(); return false;' class='btn btn-primary right'>Update</button>");
								print ("</div>");
							print ("</div>");
						}
						
						//print list of members that will import successfully
						if ($infoResults != ""){
							print ("<div class='col-sm-12' style='overflow:auto;'>");
							print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
							print ("<th>Info</th>");
							$columnNames = explode(",", $formatInformation['columnNames']);
							for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
								print ("<th>".$columnNames[$i]."</th>");
							}
							print ("</tr></thead><tbody>");
							print $infoResults;
							print ("</tbody>");
							print ("</table>");
							print ("</div>");
						}			
						
						//print list of members that will import successfully
						if ($goodResults != ""){
							print ("<div class='col-sm-12' style='overflow:auto;'>");
							print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
							$columnNames = explode(",", $formatInformation['columnNames']);
							for ($i = 0; $i < $formatInformation['noColumns']; $i++) {
								print ("<th>".$columnNames[$i]."</th>");
							}
							print ("</tr></thead><tbody>");
							print $goodResults;
							print ("</tbody>");
							print ("</table>");
							print ("</div>");
						}
						
						if ($warningResults == "") {
							print ("<div class='col-sm-12'>");
								print ("<button onclick='fnInsertOrders(); return false;' class='btn btn-success right'>Continue</button>");
							print ("</div>");
						}
						
					} else {
						print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>No file details found</p></div>");
					}
					break;
					
				case "processImport":

					if ($importID != "") {
						
						//file information
						$strdbsql = "SELECT fileImport.*, configFileImportTypes.description AS importDescription, admin_operator.username FROM fileImport 
						INNER JOIN admin_operator ON fileImport.operator = admin_operator.operatorID
						INNER JOIN configFileImportTypes ON fileImport.importType = configFileImportTypes.recordID
						WHERE fileImport.importID = :importID";
						$strType = "single";
						$arrdbparams = array("importID"=>$importID);
						$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
					
						//file information
						print ("<div class='col-sm-8'>");
						print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
						print ("<thead><tr>");
							print ("<th colspan='2'>File Details</th>");
						print ("</tr></thead><tbody>");
							print ("<tr>");
								print ("<td>Import Reference:</td>");
								print ("<td>".$resultdata['reference']."</td>");
							print ("</tr>");
							print ("<tr>");
								print ("<td>File Path:</td>");
								print ("<td><a href='".$resultdata['filePath']."' >".$resultdata['filePath']."</a></td>");
							print ("</tr>");
							print ("<tr>");
								print ("<td>Import Type:</td>");
								print ("<td>".$resultdata['importDescription']."</td>");
							print ("</tr>");
						print ("</tbody>");
						print ("</table>");
						print ("</div>");
								
						//import format information
						$strdbsql = "SELECT configFileImportFormats.description AS formatDescription, configFileImportFormats.fileFormat, configFileImportWorksheets.*,  configFileImportMatchTypes.description AS accountMatchTypeName
						FROM configFileImportWorksheets
						INNER JOIN configFileImportFormats ON configFileImportWorksheets.importID = configFileImportFormats.importID
						INNER JOIN configFileImportMatchTypes ON configFileImportWorksheets.accountMatchType = configFileImportMatchTypes.recordID
						WHERE configFileImportWorksheets.importID = :importID AND configFileImportWorksheets.importIndex = :importIndex";
						
						$arrdbparams = array("importID"=>$importConfigID,"importIndex"=>$resultdata['worksheetID']);
						$strType = "single";
						$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);


						//file information
						print ("<div class='col-sm-4'>");
						print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
						print ("<thead><tr>");
							print ("<th colspan='2'>Format Details</th>");
						print ("</tr></thead><tbody>");
							print ("<tr>");
								print ("<td>Format Name:</td>");
								print ("<td>".$resultdata['formatDescription']."</td>");
							print ("</tr>");
							print ("<tr>");
								print ("<td>File Format:</td>");
								print ("<td>".$resultdata['fileFormat']."</td>");
							print ("</tr>");
						print ("</tbody>");
						print ("</table>");
						print ("</div>");

						// Read the import file into memory - to be search through
						$strdbsql = "SELECT * FROM fileImportData WHERE importID = :importID ORDER BY rowID";
						$strType = "multi";
						$arrdbparams = array("importID"=>$importID);
						$datarows = query($conn, $strdbsql, $strType, $arrdbparams);

						//setup table
						$fileImportData = ("<div class='col-sm-12'>");
						$fileImportData .= ("<table class='table table-striped table-bordered table-hover table-condensed' >");
						$fileImportData .= ("<thead><tr>");

						$columnNames = explode(",", $resultdata['columnNames']);
						for ($i = 0; $i < $resultdata['noColumns']; $i++) {
							$fileImportData .= ("<th>".$columnNames[$i]."</th>");
						}
						$fileImportData .= ("</tr></thead><tbody>");
						
						//get highest row of data and then loop around all displaying each data row
						$noofdatarows = count($datarows);
						for ($j = 0; $j < $noofdatarows; $j++) {
							
							if (($resultdata['noIgnoreTop'] <= $j) AND (($noofdatarows - $resultdata['noIgnoreBottom']) > $j)) {
							
								$fileImportData .= ("<tr>");
								$rowData = unserialize($datarows[$j]['dataArray']);
								if (count($rowData) != $resultdata['noColumns']) {
									if (count($rowData) == $resultdata['noColumns']+1) { //extra column
							
										//check to see if the last column is blank
										if ($rowData[count($rowData)-1] == "") {
											$strwarning = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'].".<br/><b> The extra column looks to only contain spaces, please check the import process carefully.</b>";
										} else {
											$strwarning = "";
											$strerror = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'].".<br/><b> The last column has at least one value, please review the file.</b>";
										}
										
									} else if (count($rowData) == $resultdata['noColumns']-1) { //missing 1 column
										
										$strwarning = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'].".<br/><b>If the last column is not used you can ignore this warning, however if you are unsure please check the file.</b>";

									} else {
										$strerror = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'];
									}
								}
								for ($i = 0; $i < $resultdata['noColumns']; $i++) {
									$tablecolour = "class=''";
									$fileImportData .= ("<td $tablecolour>".$rowData[$i]."</td>");
								}
								$fileImportData .= ("</tr>");
							}
						}
						$fileImportData .= ("</tbody>");
						$fileImportData .= ("</table>");
						$fileImportData .= ("</div>");
						
						$buttons = ("<div class='col-sm-12'>");
						if ($strerror == "") {
							if ($strwarning != "") $buttons .= ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>");
							$buttons .= ("<button style='margin-bottom: 10px;' onclick='fnCancel(); return false;' class='btn btn-danger left'>Cancel</button>");
							$buttons .= ("<button style='margin-bottom: 10px;' onclick='fnCheckOrders(); return false;' class='btn btn-success right'>Continue</button>");
						} else {
							$buttons .= ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>");
							$buttons .= ("</div>");
							$buttons .= ("<div class='col-sm-12'>");
							$buttons .= ("<button style='margin-bottom: 10px;' onclick='fnCancel(); return false;' class='btn btn-danger left'>Cancel</button>");
						}
						$buttons .= ("</div>");

						print $buttons;
						print $fileImportData;
						print $buttons;
						

					} else {
						print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>No file uploaded</p></div>");
					}
					break;

				default:
				
						$strdbsql = "SELECT configFileImportFormats.*, configFileImportTypes.description AS fileType FROM configFileImportFormats INNER JOIN configFileImportTypes ON configFileImportFormats.importType = configFileImportTypes.recordID WHERE configFileImportFormats.status = 1 AND configFileImportFormats.importType = 3 ORDER BY configFileImportTypes.description";
						$arrdbvalues = array();
						$strType = "multi";
						$resultdata = query($conn, $strdbsql, $strType, $arrdbvalues);

						print ("<label for='frm_importConfigID' class='col-sm-2 control-label'>Import Format: </label><div class='col-sm-10'>");
						print ("<select name='frm_importConfigID' id='frm_importConfigID' class='form-control' style='width:400px;'>");
						print ("<option value=''>-- Please Select --</option>");
						foreach ($resultdata as $row) {
							$strselected = "";
							if(isset($importFormatID)) if ($importFormatID == $row['importID']) { $strselected = "selected"; } else { $strselected = ""; }
							print ("<option value='".$row['importID']."' $strselected>".$row['fileType']." Import: ".$row['description']." </option>");
						}
						print ("</select></div>");
						
						$strdbsql = "SELECT * FROM fileImport WHERE status != 3 AND importType = 3";
						$arrdbparams = array();
						$strType = "multi";
						$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
						
						print ("<label for='frm' class='col-sm-2 control-label'>File Import: </label><div class='col-sm-10'>");
						print ("<select name='frm_importID' id='frm_importID' class='form-control' style='width:300px;'>");
						if ($importID == "") { $strselected = "selected"; } else { $strselected = ""; }
						print ("<option value='' $strselected>-- Please Select --</option>");
						foreach ($resultdata as $row) {
							if ($importID == $row['importID']) { $strselected = "selected"; } else { $strselected = ""; }
							print ("<option value='".$row['importID']."' $strselected>".$row['reference']." ".fnconvertunixtime($row['timestamp'])."</option>");
						}
						print ("</select></div>");
						

						print ("<label for='frm' class='col-sm-2 control-label'></label><div class='col-sm-10'>");
						print ("<button onclick='fnSelectImport(); return false;' class='btn btn-success pull-left'>Continue</button></div>");

					
					break;
			}
			print ("</form>");
			print("</div>");
		print("</div>");
	print("</div>");

	?>	
	<script language='Javascript' >
		$().ready(function() {
			// validate signup form on keyup and submit
			$("#form").validate({
				errorPlacement: function(error,element) {
					error.insertAfter(element);
				},
				rules: {
				}
			});
		});
	</script>
	<?php
	
// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>