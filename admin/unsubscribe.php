<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * unsubscribe.php                                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details

	
	$strpage = "unsubscribe"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	if(isset($_REQUEST['email']))$stremail = $_REQUEST['email']; else $stremail = "";
	if(isset($_REQUEST['cmd'])) {
		$strcmd = $_REQUEST['cmd'];

		switch ($strcmd) {
			case "unsubscribe":
				if ($stremail != "")
				{
					$newsletterSub = getNewsletterSubscription($stremail, "byemail", $conn);
					if (empty($newsletterSub))
					{
						$strerror = "Your email address ($stremail) does not appear to be on our mailing list.";
					}
					else
					{
						if (removeNewsletterSubscription($stremail, $conn)) $strinfo = "Your email address (".$stremail.") has been removed from the mailing list.";
						else $strinfo = "An unknown error has occured. Please try again. If you still have problems please contact ".$companyName." directly to have your newsletter subscription stopped.";
					}
				}
				else
				{
					$strerror = "Please enter an Email Address";
				}
				break;
		}
	}

	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	print("<div id='mainContent'>");
	
	if($strerror != "") {
		print("<div class='notification-error'>");
			print("<h3>Error</h3>");
			print("<p>".$strerror."</p>");
		print("</div>");
	}
	if($strinfo != "") {
		print("<div class='notification-info'>");
			print("<h3>Info</h3>");
			print("<p>".$strinfo."</p>");
		print("</div>");
	}
?>
	<h1>Newsletter Subscriptions</h1>
	<p>Sorry to hear you no longer wish to receive our newsletter. Should you change your mind you can resubscribe at any time.</p>
	<p>Click the button below to unsubscribe.</p>
	<form name='frm_newsletter' action='unsubscribe.php' method='post' style='display:block;margin:0px auto; width:260px;border:solid 2px #2A5F95;background-color:#BEd3E6; padding:20px;'>
		<input type='hidden' name='cmd' value='unsubscribe' />
		<input type='text' name='email' value='<?php print($stremail); ?>' />
		<input type='submit' value='Unsubscribe'/>
	</form>
	<p style='font-size:small;text-align:center;'> Do you want to subscribe with a different address? <a href='/subscribe.php' >Click Here</a>
		<?php if($intuserid > 1) print("<br/><br/><input type='button' onclick='window.location = \"account.php\"' value='Return to Account' />"); ?>
	</p>
<?php 	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>