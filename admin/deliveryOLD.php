<?php


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "delivery"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['deliveryID'])) $deliveryID = $_REQUEST['deliveryID']; else $deliveryID = "";
	
	switch($strcmd){
		case "saveDelivery":
		
			$intDeliveryDate = strtotime(str_replace("/", "-", $_REQUEST['frm_deliveryDate']));
			$weekday = date('N', $intDeliveryDate);
			$allowedWeekdays = array("1","2","3","4");
			if (!in_array($weekday,$allowedWeekdays)) {
				while(!in_array($weekday,$allowedWeekdays)){
					$intDeliveryDate = strtotime('+1 day', $intDeliveryDate);
					$weekday = date('N', $intDeliveryDate);
				}
				$strwarning = "Selected delivery date is not valid, it has been set to the first valid delivery day.";
			}
			
			$arrParams = array("deliveryDate" => $intDeliveryDate, "deliveryOptionRateID" => $_REQUEST['frm_deliveryOption'], "packageTypeID" => $_REQUEST['frm_packageType'], "notes" => $_REQUEST['frm_deliveryNotes']);
			if($_REQUEST['frm_customer'] != "null")
			{
				$arrParams['customerID'] = $_REQUEST['frm_customer'];
			}
			else
			{
				$arrParams['customerID'] = null;
			}
			$boolItemsDeleted = false;
			
			if($deliveryID != "")
			{
				$strdbsql = "SELECT customerID FROM delivery WHERE recordID = :deliveryID";
				$strType = "single";
				$arrParam = array("deliveryID" => $deliveryID);
				$prevCustomerID = query($conn, $strdbsql, $strType, $arrParam);
				
				if($_REQUEST['frm_customer'] != $prevCustomerID['customerID'])
				{
					$strdbsql = "DELETE FROM deliveryOrders WHERE deliveryItemID = :deliveryID";
					$strType = "delete";
					$arrParam = array("deliveryID" => $deliveryID);
					$removePrevItems = query($conn, $strdbsql, $strType, $arrParam);
					$boolItemsDeleted = true;
				}
			
				$strdbsql = "UPDATE delivery SET deliveryDate = :deliveryDate, deliveryOptionRateID = :deliveryOptionRateID, customerID = :customerID, packageTypeID = :packageTypeID, notes = :notes WHERE recordID = :deliveryID";
				$strType = "update";
				$arrParams['deliveryID'] = $deliveryID;
				$updateDelivery = query($conn, $strdbsql, $strType, $arrParams);
			}
			else
			{
				$strdbsql = "INSERT INTO delivery (deliveryDate, deliveryOptionRateID, customerID, packageTypeID, notes) VALUES (:deliveryDate, :deliveryOptionRateID, :customerID, :packageTypeID, :notes)";
				$strType = "insert";
				$deliveryID = query($conn, $strdbsql, $strType, $arrParams);
			}
			
			$deliveryOrderItemsSplit = explode(",", $_REQUEST['orderItemIDs']);
			
			$deliveryOrderItems = array();
			foreach($deliveryOrderItemsSplit AS $deliveryOrderItem)
			{
				if($deliveryOrderItem != "")
				{
					$deliveryOrderItems[] = $deliveryOrderItem;
				}
			}
			
			if(empty($deliveryOrderItems) && !$boolItemsDeleted)
			{
				$strdbsql = "DELETE FROM deliveryOrders WHERE deliveryItemID = :deliveryID";
				$strType = "delete";
				$arrParam = array("deliveryID" => $deliveryID);
				$removePrevItems = query($conn, $strdbsql, $strType, $arrParam);
			}
			else
			{
				if($deliveryID != "")
				{
					$strdbsql = "SELECT * FROM deliveryOrders WHERE deliveryItemID = :deliveryItemID";
					$strType = "multi";
					$arrParams = array("deliveryItemID" => $deliveryID);
					$currOrderItems = query($conn, $strdbsql, $strType,$arrParams);
					
					$currOrderItemIDs = array();
					foreach($currOrderItems AS $currOrderItem)
					{
						if(!in_array($currOrderItem['orderItemID'], $deliveryOrderItems))
						{
							$strdbsql = "DELETE FROM deliveryOrders WHERE recordID = :recordID";
							$strType = "delete";
							$arrParam = array("recordID" => $currOrderItem['recordID']);
							query($conn, $strdbsql, $strType, $arrParam);
						}
						else
						{
							$currOrderItemIDs[] = $currOrderItem['orderItemID'];
						}
					}
				}
				
				//check weights
				$strdbsql = "SELECT maxWeight FROM deliveryOptionRates WHERE recordID = :deliveryOptionRateID";
				$strType = "single";
				$arrParam = array("deliveryOptionRateID" => $_REQUEST['frm_deliveryOption']);
				$maxWeightArr = query($conn, $strdbsql, $strType, $arrParam);
				$maxWeight = $maxWeightArr['maxWeight'];
				
				$totalOrderWeight = 0.00;
				
				//get package type weight
				$strdbsql = "SELECT weight FROM deliveryPackageTypes WHERE recordID = :packageTypeID";
				$strType = "single";
				$arrParam = array("packageTypeID" => $_REQUEST['frm_packageType']);
				$packageWeightArr = query($conn, $strdbsql, $strType, $arrParam);
				
				$totalOrderWeight += $packageWeightArr['weight'];
				
				$itemsNotAdded = "";
				$itemsAdded = "";
				
				//stock items weight + package weight < delivery option rate max weight
				foreach($deliveryOrderItems AS $deliveryOrderItem)
				{
					$strdbsql = "SELECT (stock.weight * order_items.quantity) AS orderItemWeight, stock_group_information.name FROM stock INNER JOIN order_items ON stock.recordID = order_items.stockID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE order_items.recordID = :recordID";
					$strType = "single";
					$arrParam = array("recordID" => $deliveryOrderItem);
					$stockWeightArr = query($conn, $strdbsql, $strType, $arrParam);
					$orderItemWeight = $stockWeightArr['orderItemWeight'];
					
					if($deliveryID != "" && ((($totalOrderWeight + $orderItemWeight) > $maxWeight) && $maxWeight > 0.0000))
					{
						$strdbsql = "DELETE FROM deliveryOrders WHERE deliveryItemID = :deliveryItemID AND orderItemID = :orderItemID";
						$strType = "delete";
						$arrParams = array("orderItemID" => $deliveryOrderItem, "deliveryItemID" => $deliveryID);
						query($conn, $strdbsql, $strType,$arrParams);
						
						if($itemsNotAdded == "")
						{
							$itemsNotAdded .= "The following item(s) were not added to the delivery as they are too heavy:";
						}
						$itemsNotAdded .= "<br/>- ".$stockWeightArr['name'];
					}
					else
					{
						if(!in_array($deliveryOrderItem, $currOrderItemIDs))
						{
							$strdbsql = "INSERT INTO deliveryOrders (deliveryItemID, orderItemID) VALUES (:deliveryItemID, :orderItemID)";
							$strType = "insert";
							$arrParams = array("orderItemID" => $deliveryOrderItem, "deliveryItemID" => $deliveryID);
							query($conn, $strdbsql, $strType, $arrParams);
							
							if($itemsAdded == "")
							{
								$itemsAdded .= "The following item(s) were added to the delivery:";
							}
							$itemsAdded .= "<br/>- ".$stockWeightArr['name'];
						}
						$totalOrderWeight += $orderItemWeight;
					}
				}
				$strwarning .= $itemsNotAdded;
				$strsuccess = $itemsAdded;
			}
			$strdbsql = "UPDATE delivery SET weight = :weight WHERE recordID = :recordID";
			$strType = "update";
			$arrParam = array("weight" => $totalOrderWeight, "recordID" => $deliveryID);
			query($conn, $strdbsql, $strType, $arrParam);
			
			$strcmd = "editDelivery";
		
		break;
	}
	
	
	

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Delivery</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

			?>	
			<script type='text/javascript'>	
				function fnBuildOrderItems(){
					var orderItems = "";
					$("#deliveryOrderItems li").each(function (index, item) {
						orderItems += $(item).attr("id")+",";
					});
					orderItems = orderItems.replace(/,\s*$/, "");
					
					return orderItems;
				}
				function fnEditDelivery(deliveryID){
					document.form.cmd.value='editDelivery';
					document.form.deliveryID.value=deliveryID;
					document.form.submit();
				}
				function fnAddDelivery(){
					document.form.cmd.value='addDelivery';
					document.form.deliveryID.value='';
					document.form.submit();
				}
				function fnSaveDelivery(){
					document.form.cmd.value='saveDelivery';
					document.form.orderItemIDs.value=fnBuildOrderItems();
					document.form.submit();
				}
				function fnCancel(){
					document.form.cmd.value='';
					document.form.deliveryID.value='';
					document.form.submit();
				}
				function updateCustomerID() {
					var callbacks = $.Callbacks();
					callbacks.add(dragDropOrderItems);
					
					var request = $.ajax({
						type: "POST",
						url: "includes/ajax_updatedeliveryorder.php",
						data:{ customerID: $("#frm_customer").val(), deliveryID: $("#deliveryID").val() }
					});
					
					request.done(function(response) {
						$("#orderItemsContainer").html(response);
						callbacks.fire();
					});
					
					request.fail(function(jqXHR, textStatus) {
					  alert( "Request failed: " + textStatus );
					});
				}
				function dragDropOrderItems() {
					$("#unassignedOrderItems").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#deliveryOrderItems").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#deliveryOrderItems'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
						}
					}).disableSelection();
					
					$("#deliveryOrderItems").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedOrderItems").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedOrderItems'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
						}
					}).disableSelection();
				}
				function selectAllOrderItems(listType) {
					if(listType == "unassigned")
					{
						$("#deliveryOrderItems li").removeClass("selected");
						$("#unassignedOrderItems li").not(".selected").addClass("selected");
					}
					else if(listType == "assigned")
					{
						$("#unassignedOrderItems li").removeClass("selected");
						$("#deliveryOrderItems li").not(".selected").addClass("selected");
					}
				}
			</script>
			<?php
			
			
			print("<form action='delivery.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' class='nodisable' name='cmd' id='cmd' />");
				print("<input type='hidden' class='nodisable' name='deliveryID' id='deliveryID' value='$deliveryID' />");

				
				switch ($strcmd){
					case "addDelivery":
					case "editDelivery":
						print("<input type='hidden' class='nodisable' name='orderItemIDs' id='orderItemIDs' class='form-control' value='' />");
						
						print("<div class='row'>");
							print("<div class='col-md-6'>");
								print("<div class='section'>");
									print("<fieldset>");
										print("<legend>Delivery Details</legend>");
						
									if (!empty($deliveryID)) {
										
										$strdbsql = "SELECT delivery.*, deliveryOptionRates.deliveryOptionID FROM delivery INNER JOIN deliveryOptionRates ON delivery.deliveryOptionRateID = deliveryOptionRates.recordID WHERE delivery.recordID = :recordID";
										$strType = "single";
										$arrdbparam = array("recordID"=>$deliveryID);
										$delivery = query($conn, $strdbsql, $strType, $arrdbparam);

									} else {
										$delivery['deliveryDate'] = $datnow;
										$delivery['deliveryOptionRateID'] = "";
										$delivery['packageTypeID'] = "";
										$delivery['weight'] = 0;
										$delivery['notes'] = "";
										$delivery['deliveryStatusID'] = 1;
										$delivery['customerID'] = null;
									}
								
									//delivery day / timestamp
									print("<div class='row'>");
										print("<div class='form-group col-md-6' style='padding-right:15px;'>");
											print("<label for='frm_deliveryDate' class='control-label'>Delivery Date:</label>");
											print("<input type='text' class='form-control' id='frm_deliveryDate' name='frm_deliveryDate' value='".date("d/m/Y", $delivery['deliveryDate'])."' />");
										print("</div>");
					
									//select delivery option
										print("<div class='form-group col-md-6' style='padding-left:15px;'>");
											print("<label for='frm_deliveryOption' class='control-label'>Delivery Option:</label>");
											print("<select name='frm_deliveryOption' id='frm_deliveryOption' class='form-control'>");
												$strdbsql = "SELECT deliveryOptionRates.*, deliveryOptions.description FROM deliveryOptionRates INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID";
												$strType = "multi";
												$resultdata = query($conn, $strdbsql, $strType);								  
												foreach ($resultdata AS $row)
												{
													$strSelected = "";
													if($row['recordID'] == $delivery['deliveryOptionRateID']) $strSelected = " selected";
													print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']." - ".$row['maxWeight']."</option>");
												}
											print("</select>");
										print("</div>");
									print("</div>");
									print("<div class='row'>");
										print("<div class='form-group col-md-6'>");
											
											//select delivery rate
											/*print("<div class='form-group'><label for='frm_deliveryOptionRate' class='col-sm-2 control-label'>Delivery Rate:</label><div class='col-sm-10'><select name='frm_deliveryOptionRate' id='frm_deliveryOptionRate' class='form-control' style='width: 200px;'>");
											$strdbsql = "SELECT * FROM deliveryOptionRates";
											$strType = "multi";
											$resultdata = query($conn, $strdbsql, $strType);								  
											foreach ($resultdata AS $row)
											{
												$strSelected = "";
												if($row['recordID'] == $delivery['deliveryOptionRateID']) $strSelected = " selected";
												print("<option value='".$row['recordID']."'".$strSelected.">&pound;".number_format($row['cost'],2)." - ".$row['maxWeight']."</option>");
											}
											print("</select></div></div>");*/
											
											//select customer
											print("<label for='frm_order' class='control-label'>Customer:</label><select name='frm_customer' id='frm_customer' class='form-control' style='width: 200px;' onchange='updateCustomerID()'>");
												print("<option value='null'>None</option>");
												$strdbsql = "SELECT customer.recordID, customer.surname, COUNT(order_items.recordID) AS intOrderedItems, COUNT(deliveryOrders.recordID) AS intAssignedItems FROM customer INNER JOIN order_header ON customer.recordID = order_header.customerID INNER JOIN order_items ON order_header.recordID = order_items.orderHeaderID LEFT JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID GROUP BY order_header.customerID, order_header.addDeliverySurname HAVING intAssignedItems < intOrderedItems";
												$strType = "multi";
												$arrParam = array();
												if($delivery['customerID'] != null)
												{
													$strdbsql .= " OR order_header.customerID = :customerID";
													$arrParam['customerID'] = $delivery['customerID'];
												}
												$resultdata = query($conn, $strdbsql, $strType, $arrParam);								  
												foreach ($resultdata AS $row)
												{
													$strSelected = "";
													if($row['recordID'] == $delivery['customerID']) $strSelected = " selected";
													print("<option value='".$row['recordID']."'".$strSelected.">".$row['recordID']." - ".$row['surname']."</option>");
												}
												print("</select>");
										print("</div>");
									print("</div>");
									print("<div class='row'>");
										print("<div class='form-group col-md-12'>");
											
											//select order items
												//allow all to be selected
												//allow individual items to be selected
											print("<label for='frm_order' class='control-label'>Order Items:</label><div class='col-sm-12' id='orderItemsContainer' style='padding-left: 0; padding-right: 0;'>");
											
												include("includes/ajax_updatedeliveryorder.php");
											
											print("</div>");
										print("</div>");
									print("</div>");
									print("<div class='row'>");
										print("<div class='form-group col-md-6'>");
											
											//select package type
											print("<label for='frm_packageType' class='control-label'>Package Type:</label><select name='frm_packageType' id='frm_packageType' class='form-control'>");
											$strdbsql = "SELECT * FROM deliveryPackageTypes";
											$strType = "multi";
											$resultdata = query($conn, $strdbsql, $strType);
											foreach ($resultdata AS $row)
											{
												$strSelected = "";
												if($row['recordID'] == $delivery['packageTypeID']) $strSelected = " selected";
												print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']." - ".$row['weight']."</option>");
											}
											print("</select>");
										print("</div>");
									print("</div>");
									print("<div class='row'>");
										print("<div class='form-group col-md-12'>");
											
											//delivery notes
											print("<label for='frm_deliveryNotes' class='control-label'>Notes:</label><textarea class='form-control' id='frm_deliveryNotes' name='frm_deliveryNotes' rows='5'>".$delivery['notes']."</textarea>");
										print("</div>");
									print("</div>");
									print("<div class='row'>");
										print("<div class='form-group col-md-12'>");
											
											//buttons
											print("<button onclick='return fnSaveDelivery();' type='button' class='btn btn-success right'>Save</button> ");
											print("<button onclick='return fnCancel();' class='btn btn-danger left'>Cancel</button>"); //need rules on this e.g. order status and payment status
										print("</div>");
									print("</div>");
						print("</fieldset>");
						
						
					break;				
					default:
						//show list of deliverys
						$strdbsql = "SELECT delivery.*,deliveryStatus.description AS statusDesc, deliveryOptions.description AS optionDesc, deliveryOptionRates.cost, deliveryPackageTypes.description AS packageDesc FROM delivery INNER JOIN deliveryOptionRates ON delivery.deliveryOptionRateID = deliveryOptionRates.recordID INNER JOIN deliveryPackageTypes ON delivery.packageTypeID = deliveryPackageTypes.recordID INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID LEFT JOIN deliveryStatus ON delivery.deliveryStatusID = deliveryStatus.recordID ORDER BY delivery.deliveryDate DESC";
						$strType = "multi";
						$arrdbparam = array();
						$deliveryDetails = query($conn, $strdbsql, $strType, $arrdbparam);
						
						print("<div class='section'>");
						
							print("<table class='order-list table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th>Delivery Date</th>");
									print("<th>Delivery Option</th>");
									print("<th>Cost</th>");
									print("<th>Notes</th>");
									print("<th>Packaging</th>");
									print("<th>Order Details</th>");
									print("<th>Stock Details</th>");
									print("<th>Current Status</th>");
									print("<th></th>");
								print("</tr></thead><tbody>");
								
								if (count($deliveryDetails) > 0) {
									foreach($deliveryDetails AS $delivery)
									{
										print("<tr>");
										//delivery date
										print("<td>".fnconvertunixtime($delivery['deliveryDate'])."</td>");
									
										//delivery option
										print("<td>".$delivery['optionDesc']."</td>");
										
										//delivery rate / cost
										print("<td>&pound;".number_format($delivery['cost'],2)."</td>");
										
										//notes
										print("<td>".$delivery['notes']."</td>");
										
										//package desc
										print("<td>".$delivery['packageDesc']."</td>");
										
										//order lines
										$strdbsql = "SELECT order_items.*, order_dispatch.description AS dispatchDesc, order_header.timestampOrder, order_header.addDeliveryPostcode, order_header.addDelivery1, order_status.description AS orderStatusDesc, order_payment_status.description AS payDesc FROM deliveryOrders INNER JOIN order_items ON deliveryOrders.orderItemID = order_items.recordID INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID INNER JOIN order_payment_status ON order_header.paymentStatus = order_payment_status.recordID INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID WHERE deliveryOrders.deliveryItemID = :deliveryItemID";
										$strType = "multi";
										$arrdbparam = array("deliveryItemID"=>$delivery['recordID']);
										$deliveryOrderItems = query($conn, $strdbsql, $strType, $arrdbparam);
										
										if (count($deliveryOrderItems) > 0) {
											print("<td>");
												print("Order Date: ".fnconvertunixtime($deliveryOrderItems[0]['timestampOrder'])."<br/>");
												print("Address: ".$deliveryOrderItems[0]['addDelivery1']." ".$deliveryOrderItems[0]['addDeliveryPostcode']."<br/>");
												print("Status: ".$deliveryOrderItems[0]['orderStatusDesc']."<br/>Payment: ".$deliveryOrderItems[0]['payDesc']);
											print("</td>");
											print("<td>");
											foreach ($deliveryOrderItems AS $orderItems) {
												
												$stockDetailsArray = unserialize($orderItems['stockDetailsArray']);
												switch ($orderItems['dispatchDesc']) {
													case "On":
													case "Before":
													case "After":
													case "Xmas":
														$dispatch = $orderItems['dispatchDesc']." ".fnconvertunixtime($orderItems['dispatchDate'])."";
													break;
													default:
														$dispatch = $orderItems['dispatchDesc'];
													break;
												}
												print ($orderItems['quantity']." x ".$stockDetailsArray['stockCode']." - Deliver: ".$dispatch."<br/>");
											}
											print("</td>");
										} else {
											print("<td></td><td></td>");
										}
										
										//delivery status
										print("<td>".$delivery['statusDesc']."</td>");
										print("<td><button type='button' onclick='fnEditDelivery(".$delivery['recordID'].");' class='center btn btn-primary circle'><i class='fa fa-pencil'></i></button></td>");
										print("</tr>");
									}
								}
								print("</tbody>");
							print("</table>");
							
						print("</div>");
						
						print("<div class='buttons'>");
							print("<button onclick='return fnAddDelivery();' type='submit' class='btn btn-success right'>Add</button> ");
						print("</div>");
							
					break;
				}
			print("</form>");
		print("</div>");
	print("</div>");
	
?>
	<!--<script type="text/javascript" src="/js/jquery-ui.js" ></script>-->
	<script type='text/javascript'>
		$().ready(function() {
			$('.order-list').DataTable();
			
			$('#frm_deliveryDate').datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true, 
				changeYear: true,
				beforeShowDay: function(date) {
					var day = date.getDay();
					return [(day != 5 && day != 6 && day != 0)];
				}
			});
			
			dragDropOrderItems();
		});
		
	</script>
	
<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>