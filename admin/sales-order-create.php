<?php


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "sales-order-create"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site (Admin only version???)
	$conn = connect(); // Open Connection to database
	

	/*function fnUpdateBasketHeader ($intbasketid, $intuserid, $conn) {
		
		$billingDetails = array();
		$deliveryDetails = array();
		$contactDetails = array();
		if ($intuserid == 1) { //guest so use the $_REQUEST info
			
			$contactDetails['company'] = $_REQUEST['company'];
			$contactDetails['telephone'] = $_REQUEST['tel'];
			$contactDetails['mobile'] = "";
			$contactDetails['email'] = $_REQUEST['email'];
			$contactDetails['fax'] = "";
			$contactDetails['customerID'] = 1;
			$contactDetails['customerName'] = "Guest";
			
			$deliveryDetails['recordID'] = 0;
			$deliveryDetails['customerID'] = 1;
			$deliveryDetails['addDescription'] = "";
			$deliveryDetails['title'] = $_REQUEST['title'];
			$deliveryDetails['firstname'] = $_REQUEST['firstname'];
			$deliveryDetails['surname'] = $_REQUEST['surname'];
			$deliveryDetails['add1'] = $_REQUEST['address'];
			$deliveryDetails['add2'] = $_REQUEST['address2'];
			$deliveryDetails['add3'] = $_REQUEST['address3'];
			$deliveryDetails['town'] = $_REQUEST['town'];
			$deliveryDetails['county'] = $_REQUEST['county'];
			$deliveryDetails['country'] = "";
			$deliveryDetails['postcode'] = $_REQUEST['postcode'];
			$deliveryDetails['notes'] = "";
			
			if(isset($_REQUEST['addcheck']))
			{
				$billingDetails['recordID'] = 0;
				$billingDetails['customerID'] = 1;
				$billingDetails['addDescription'] = "";
				$billingDetails['title'] = $_REQUEST['title'];
				$billingDetails['firstname'] = $_REQUEST['firstname'];
				$billingDetails['surname'] = $_REQUEST['surname'];
				$billingDetails['add1'] = $_REQUEST['address'];
				$billingDetails['add2'] = $_REQUEST['address2'];
				$billingDetails['add3'] = $_REQUEST['address3'];
				$billingDetails['town'] = $_REQUEST['town'];
				$billingDetails['county'] = $_REQUEST['county'];
				$billingDetails['country'] = "";
				$billingDetails['postcode'] = $_REQUEST['postcode'];
				$billingDetails['notes'] = "";
			}
			else
			{
				$billingDetails['recordID'] = 0;
				$billingDetails['customerID'] = 1;
				$billingDetails['addDescription'] = "";
				$billingDetails['title'] = $_REQUEST['btitle'];
				$billingDetails['firstname'] = $_REQUEST['bfirstname'];
				$billingDetails['surname'] = $_REQUEST['bsurname'];
				$billingDetails['add1'] = $_REQUEST['baddress'];
				$billingDetails['add2'] = $_REQUEST['baddress2'];
				$billingDetails['add3'] = $_REQUEST['baddress3'];
				$billingDetails['town'] = $_REQUEST['btown'];
				$billingDetails['county'] = $_REQUEST['bcounty'];
				$billingDetails['country'] = "";
				$billingDetails['postcode'] = $_REQUEST['bpostcode'];
				$billingDetails['notes'] = "";
			}
			
		} else {
			$strdbsql = "SELECT customer_address.* FROM customer INNER JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE customer.recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$billingDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strdbsql = "SELECT customer_address.* FROM customer INNER JOIN customer_address ON customer.defaultDeliveryAdd = customer_address.recordID WHERE customer.recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$deliveryDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strdbsql = "SELECT username AS customerName, recordID AS customerID, company, telephone, mobile, email, fax FROM customer WHERE recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$contactDetails = query($conn,$strdbsql,$strType,$arrdbparams);
		}
		
		//update basket header
		$strdbsql = "UPDATE basket_header SET billingAddressArray = :billingDetails, deliveryAddressArray = :deliveryDetails, contactDetailsArray = :contactDetails, customerID = :customerID, modifiedTimestamp = UNIX_TIMESTAMP() WHERE recordID = :basketID";
		$strType = "update";
		$arrdbparams = array("basketID"=>$intbasketid, "customerID"=>$intuserid, "contactDetails"=>serialize($contactDetails), "deliveryDetails"=>serialize($deliveryDetails), "billingDetails"=>serialize($billingDetails));
		$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
	}*/
	/*function fnGetFinalBasket($basketHeader,$basketItems,$strselectedpostage, $strView, $conn){

		//get offer code details
		if ($basketHeader['offerCodeID'] > 0) {
			
			$strdbsql = "SELECT * FROM site_offer_codes WHERE recordID = :offerCodeID";
			$strType = "single";
			$arrdbparams = array("offerCodeID"=>$basketHeader['offerCodeID']);
			$siteOffers = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strcode = $siteOffers['code'];
			$strtype = $siteOffers['discountType'];
			$decdisvalue = $siteOffers['discountAmount'];
			$decminamt = $siteOffers['minimumSpend'];
			$strdiscountdesc = $siteOffers['description'];
			$arrdiscountcats = explode(" ", $siteOffers['categoryList']);
			$arrdiscountprod = explode(" ", $siteOffers['stockList']);
		
		}
		
		$strdbsql = "SELECT * FROM site_postage WHERE isActive = 1";
		$strType = "multi";
		$arrdbparams = array();
		$postageOffers = query($conn,$strdbsql,$strType,$arrdbparams);
		
		if (count($basketItems) > 0) {
			
			$deliveryAddressDetails = unserialize($basketHeader['deliveryAddressArray']);
			$delpostcode = $deliveryAddressDetails['postcode'];
			
			print ("<div style='clear:both;'>");
				print ("<table cellspacing='0' cellpadding='3' id='baskettable'>");
					print ("<tbody>");
						print ("<tr>");
							print ("<td class='linehead' style='width: 10%;'>Stockcode</td>");
							print ("<td class='linehead' style='width: 30%;'>Name</td>");
							print ("<td class='linehead' style='width: 30%;'>Details</td>");
							print ("<td class='linehead' style='width: 15%;'>Quantity</td>");
							print ("<td class='linehead' align='right' style='width: 15%;'>Price</td>");
						print ("</tr>");
					
						$strordersize = "";
						$blnoddeven = false;
						$booldiscountapplies = false;
						$dectotcost = 0;
						$intnoitems = 0;
						foreach ($basketItems AS $basketItem) {
						
							//styling for changing rows
							if ($blnoddeven) $strtdclass = "class='evenrow line'";
							else $strtdclass = "class='line'";											
							$blnoddeven = !$blnoddeven;
							
							//get stock image
							$strdbsql = "SELECT * FROM stock_images WHERE stockID = :stockID ORDER BY imageOrder ASC LIMIT 1";
							$strType = "single";
							$arrdbparams = array("stockID"=>$basketItem['stockID']);
							$stockImage = query($conn,$strdbsql,$strType,$arrdbparams);
							
							if ($stockImage['imageLink'] != ""){
								$strimgpath = "/images/product_images/".$basketItem['stockID']."/list/".$stockImage['imageLink'];
							} else {
								$strimgpath = "/images/no_image.jpg";
							}
							
							//unserialize stock details
							$stockDetails = unserialize($basketItem['stockDetailsArray']);


							print ("<tr id='row_".$basketItem['recordID']."'>");
							print ("<td $strtdclass>".$stockDetails['stockCode']."</td>");
							print ("<td $strtdclass>".$stockDetails['name']."</td>");
							print ("<td $strtdclass>");

							if ($stockDetails['width'] != "") print ("Width: ".$stockDetails['width']."<br/>");
							if ($stockDetails['height'] != "") print ("Height: ".$stockDetails['height']."<br/>");
							if ($stockDetails['depth'] != "") print ("Depth: ".$stockDetails['depth']."<br/>");
							if ($stockDetails['diameter'] != "") print ("Diameter: ".$stockDetails['diameter']."<br/>");
							if ($stockDetails['description'] != "") print ("Dimensions: ".$stockDetails['description']."<br/>");
							if (isset($stockDetails['Finish']) && $stockDetails['Finish']['description'] != "") print ("Finish: ".$stockDetails['Finish']['description']."<br/>");
							if (isset($stockDetails['Colour']) && $stockDetails['Colour']['description'] != "") print ("Colour: ".$stockDetails['Colour']['description']."<br/>");
							if (isset($stockDetails['Fabric']) && $stockDetails['Fabric']['description'] != "") print ("Fabric: ".$stockDetails['Fabric']['description']."<br/>");
							if (isset($stockDetails['Base']) && $stockDetails['Base']['description'] != "") print ("Base: ".$stockDetails['Base']['description']."<br/>");

							print ("</td>");
							print ("<td $strtdclass id='qty_".$basketItem['recordID']."'>&nbsp;".$basketItem['quantity']."&nbsp;</td>");
							
							for ($i = 0; $i < $basketItem['quantity']; $i = $i + 1)
							{
								$strordersize .= $stockDetails['deliverySize'];
							}

							$discountmark = '';
							if (!in_array('all', $arrdiscountcats)) //if offer doesn't apply to everything
							{
								//test if this product is in an offer
								if (in_array($stockDetails['stockCode'], $arrdiscountprod))
								{
									$booldiscountapplies = true;
									$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
									$discountmark = '*';
								}
								else
								{
									$strdbsql = "SELECT * FROM stock_category_relations WHERE stockID = :stockID";
									$strType = "multi";
									$arrdbparams = array("stockID"=>$basketItem['stockID']);
									$stockRelations = query($conn,$strdbsql,$strType,$arrdbparams);
									
									foreach ($stockRelations AS $stockRelation) {
										if (in_array($stockRelation['categoryID'], $arrdiscountcats) && $discountmark == '')
										{
											$booldiscountapplies = true;
											$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
											$discountmark = '*';
										}
									}
								}
							}
							else
							{
								$booldiscountapplies = true;
								$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
								$discountmark = '*';
							}
							
							
						
						
							// New postage calculator
							// Declare arrays for population later
							//$itemCategories = array();
							$mainCategories = array();
							$arrcantorderitems = array();
							
							//categories
							$strdbsql = "SELECT * FROM stock_category_relations WHERE stockID = :stockID";
							$strType = "multi";
							$arrdbparams = array("stockID"=>$stockDetails['groupID']);
							$stockCategories = query($conn,$strdbsql,$strType,$arrdbparams);
							
							foreach ($stockCategories AS $stockCategory) {
							
								$mainCategories[] = $stockCategory['categoryID'];								
								$strdbsql = "SELECT * FROM category_relations WHERE childID = :categoryID";
								$strType = "multi";
								$arrdbparams = array("categoryID"=>$stockCategory['categoryID']);
								$categoryRelations = query($conn,$strdbsql,$strType,$arrdbparams);
								
								foreach($categoryRelations AS $categoryRelation){
								
									$mainCategories[] = $categoryRelation['parentID'];
									$parentID = $categoryRelation['parentID'];
									$categoryLevel = $categoryRelation['level'];
									while($categoryLevel != 1){
										
										$strdbsql = "SELECT * FROM category_relations WHERE childID = :categoryID";
										$strType = "single";
										$arrdbparams = array("categoryID"=>$parentID);
										$categoryRelation2 = query($conn,$strdbsql,$strType,$arrdbparams);
										
										$mainCategories[] = $categoryRelation2['parentID'];
										$parentID = $categoryRelation2['parentID'];
										$categoryLevel = $categoryRelation2['level'];
									}
								}
							}
							//var_dump($mainCategories);
							
							//ranges
							$strdbsql = "SELECT rangeID FROM stock_range_relations WHERE stockID = :stockID";
							$strType = "single";
							$arrdbparams = array("stockID"=>$stockDetails['groupID']);
							$stockRange = query($conn,$strdbsql,$strType,$arrdbparams);
							
							//brands
							$strdbsql = "SELECT brandID FROM stock_group_information WHERE recordID = :stockID";
							$strType = "single";
							$arrdbparams = array("stockID"=>$stockDetails['groupID']);
							$stockBrand = query($conn,$strdbsql,$strType,$arrdbparams);
							
							// check there are postage rules set
							if(!empty($postageOffers))
							{
								// Loop over postage rules
								foreach($postageOffers AS $postageOffer)
								{
									// set to false as item not yet found to qualify for postage rule
									$boolPostageItemRule = false;
									#echo "<br/>".$postageOffer['fld_desc']."<br/>";
									// set $minspend to that of current offer
									$minspend = $postageOffer['minspend'];
									
									// determine total cost of that item by multiplying quantity by price
									$dectotcost2 = number_format(($basketItem['quantity'] * $basketItem['price']),2,".","");
									// set to false before testing
									$boolpostcodequal = false;
									
									// if there are postcodes specified by the postage rule
									if(!empty($postageOffer['postcodeList']))
									{
										// take fld_postcodes and build array from it based on separation on commas
										$arrofferpostcodes = explode(",", $postageOffer['postcodeList']);
										
										// format $delpostcode for testing - ensure string is capitalised and has no spaces
										$delpostcode = strtoupper(str_replace(" ", "", $delpostcode));
										
										// loop over post codes in postage rule
										foreach($arrofferpostcodes AS $offerpostcode)
										{
											// test for match of each post code in ofer against $delpostcode, matching 2, 3 or 4 characters based on length of $offerpostcode
											switch(strlen($offerpostcode))
											{
												case 2:
													#echo $offerpostcode . " = " . substr($delpostcode, 0, 2) . "<br/>";
													if($offerpostcode == substr($delpostcode, 0, 2)) {
														// post code in offer so $boolpostcodequal set to true
														$boolpostcodequal = true;
														#echo "true!<br/>";
													}
													break;
												case 3:
													#echo $offerpostcode . " = " . substr($delpostcode, 0, 3) . "<br/>";
													if(strlen($delpostcode) == 6 && $offerpostcode == substr($delpostcode, 0, 3)) {
														// post code in offer so $boolpostcodequal set to true
														$boolpostcodequal = true;
														#echo "true!<br/>";
													}
													break;
												case 4:
													#echo $offerpostcode . " = " . substr($delpostcode, 0, 4) . "<br/>";
													if(strlen($delpostcode) == 7 && $offerpostcode == substr($delpostcode, 0, 4)) {
														// post code in offer so $boolpostcodequal set to true
														$boolpostcodequal = true;
														#echo "true!<br/>";
													}
													break;
											}
										}
									}
									else
									{
										// no post codes specified by postage rule, so set $boolpostcodequal to true regardless
										$boolpostcodequal = true;
									}
									// test if post code qualifies
									if($boolpostcodequal) {
										// set default to false
										$boolmaincategory = false;
										$postageCatIDArr = explode(",", $postageOffer['categoryIDList']);
										$postageBrandIDArr = explode(",", $postageOffer['brandIDList']);
										$postageRangeIDArr = explode(",", $postageOffer['rangeIDList']);
										//var_dump($postageCatIDArr);
										//var_dump($mainCategories);
										//echo "<br/>";
										//echo "Boolmaincategory: ".$boolmaincategory."<br/>";
										// loop over item categories/sub categories
										foreach($mainCategories AS $mainCategory)
										{
											//echo $mainCategory."<br/>";
											// if item has a range, brand or type in offer, $boolmaincategory set to true
											if(!empty($postageOffer['categoryIDList']) && in_array($mainCategory, $postageCatIDArr)) {
												$boolmaincategory = true;
											}
										}
										
										if(!empty($postageOffer['brandIDList']) && in_array($stockBrand['brandID'], $postageBrandIDArr)) {
											$boolmaincategory = true;
										}
										
										if(!empty($postageOffer['rangeIDList']) && in_array($stockRange['rangeID'], $postageRangeIDArr)) {
											$boolmaincategory = true;
										}
										//echo "Boolmaincategory: ".$boolmaincategory."<br/>";
										
										// test $boolmaincategory true, or if item ID in postage offer product IDs, or if $itemCategory category/sub category match postage offer brand, type or range
										if($boolmaincategory || in_array($stockDetails['groupID'], explode(",", $postageOffer['stockIDList'])))
										{
											// if $productID not already equal to current product ID or $postageID not already equal to current postage rule ID - basically don't add the same item in the same rule twice
											if ($productID != $basketItem['recordID'] || $postageID != $postageOffer['recordID']) {
												
												// set $productID and $postageID to current iteration equivalents
												$productID = $basketItem['recordID'];
												$postageID = $postageOffer['recordID'];
												
												// add total for current postage rule to $postageArray (this is recorded as a running total of all products in the rule)
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'] += $dectotcost2;
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['cost'] = $postageOffer['cost'];
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['minspend'] = $postageOffer['minspend'];
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['products'][] = $productID;
											}
											
											// item qualifies for this postage rule
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["totalSpend"] = $postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["productSpend"] = $dectotcost2;
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['minspend'] = $postageOffer['minspend'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['cost'] = $postageOffer['cost'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['modifierthreshold'] = $postageOffer['modifierThreshold'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['modifiercost'] = $postageOffer['modifierCost'];
											if ($postageOffer['isGlobal'] == 1) $scope = "Global"; else $scope = "Specific";
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['scope'] = $scope;
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]["ruleValid"] = "N";
											
											//check if rule applies if over minimum spend - may need to change if they base global rules on entire spend rather than just product spend!
											if($dectotcost2 >= $minspend || $minspend == "" || ($postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'] >= $minspend AND $postageOffer['isGlobal'] != 1))
											//if($dectotcost2 >= $minspend || $minspend == "" || $postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'] >= $minspend)
											{
												$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]["ruleValid"] = "Y";
												
												//update all other products that have this rule:
												foreach ($postageItem AS $key => $productPostage) {
													//set valid and totalSpend
													if (isset($productPostage["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']])) {
													
														//the next line may need to be removed depending on if they want to apply the lowest per product or base it on total order value!
														if ($productPostage["Group-".$postageOffer['groupID']]["productSpend"] >= $postageOffer['minspend'] || $postageOffer['isGlobal'] != 1) {
															$postageItem[$key]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]["ruleValid"] = "Y";
															$postageItem[$key]["Group-".$postageOffer['groupID']]["totalSpend"] = $postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'];
														}
													}
												}
											}
										} // End of test if product, range, brand or type in offer
									} // End of test if post code qualifies
									else
									{
										// if post code does not qualify and item can only be delivered to post codes in the rule, add item to $arrcantorderitems
										if($postageOffer['isLocalDeliveryOnly'] == 1 && in_array($basketItem['stockID'], explode(",", $postageOffer['stockIDList'])))
										{
											$arrcantorderitems[] = $stockDetails['name'];
										}
									}
								} // End of loop over postage rules
							}

							
							
							print ("<td $strtdclass align='right' id='price_".$basketItem['recordID']."' style='font-weight:bold'>&pound;&nbsp;".number_format(($basketItem['quantity'] * $basketItem['price']),2).$discountmark."</td>");
							print ("</tr>");
							$dectotcost += $basketItem['quantity'] * $basketItem['price'];
							$intnoitems += $basketItem['quantity'];

						} //end of product loop
						
						print ("<tr>");
							if ($booldiscountapplies) print ("<td class='linehead' id='ajax_getsdiscount' colspan='2'>*Item qualifies for discount</td>");
							else print ("<td class='linehead' colspan='2' id='ajax_getsdiscount' > </td>");
							print ("<td class='linehead'>Sub Total:</td>");
							print ("<td class='linehead' id='ajax_items'>".$intnoitems." Items</td>");
							print ("<td class='linehead' align='right' id='ajax_goodstotal'>&pound;&nbsp;".number_format($dectotcost,2)."</td>");
						print ("</tr>");
					print ("</tbody>");
				print ("</table>");
				print ("<br/>");	
			print ("</div>");	
		}

		
		if($strView == "checkout")
		{
		
			// declare for population in loop over postage rules
			$validPostageRates = array(); //this is an array of prices for delivery of each product
			
			//$postageItem is an array of products with all postage rules that could apply for that product
			if(isset($postageItem) && is_array($postageItem))
			{
				foreach ($postageItem AS $key => $productPostage) { //loop around all products
				
					$validProductPostageRules = array(); //reset products postage rules for each loop
					$validMinSpends = array(); //reset minimum spend for this item
				
					foreach ($productPostage AS $groupID => $productPostageGroups) { //loop around each group of each product
						foreach ($productPostageGroups AS $productPostageRules) { //loop around each rule in each group
							if (is_array($productPostageRules)) { //check the rule is an actual array as we also store some information e.g. total spend for the group
								if ($productPostageGroups['totalSpend'] >= $productPostageRules['minspend'] AND $productPostageRules['ruleValid'] == "Y") { //if the rule is valid and totalspend for this group is greater than the minimum for this rule
									
									if((empty($validProductPostageRules[$productPostageRules['scope']][$groupID]) && empty($validMinSpends[$productPostageRules['scope']][$groupID])) || $productPostageRules['minspend'] > $validMinSpends[$productPostageRules['scope']][$groupID])
									{
										$validMinSpends[$productPostageRules['scope']][$groupID] = $productPostageRules['minspend']; //keep track of the minimum spend for the current selected rule so we dont apply lower minumum spend rules
										if ($productPostageRules['modifierthreshold'] <= $dectotcost AND $productPostageRules['modifierthreshold'] > 0) {
											$validProductPostageRules[$productPostageRules['scope']][$groupID] = $productPostageRules['modifiercost']; //set cost for postage for this group 
										} else {
											$validProductPostageRules[$productPostageRules['scope']][$groupID] = $productPostageRules['cost']; //set cost for postage for this group 
										}
									}
								}
							}
						}
						
						//if this product contains specific rule groups then only add them otherwise add the global rule groups
						if (isset($validProductPostageRules['Specific'])) {
							foreach ($validProductPostageRules['Specific'] AS $validProductPostageRule) {
								$validPostageRates[] = $validProductPostageRule;
							}
						} else {
							foreach ($validProductPostageRules['Global'] AS $validProductPostageRule) {
								$validPostageRates[] = $validProductPostageRule;
							}
						}
					}
				}
			}

			// test if any postage rules qualified for
			if(empty($validPostageRates))
			{
				// if not, set to £25 default
				$postageCost = 25;
			}
			else
			{
				// otherwise get highest value from array of offers qualified for
				$postageCost = max($validPostageRates);
			}
			
			// update basket header with postage cost
			$updatePostageQuery = "UPDATE basket_header SET postageAmount = :postageAmount WHERE recordID = :basketHeaderID";
			$strType = "update";
			$arrdbparams = array(
								"postageAmount" => $postageCost,
								"basketHeaderID" => $basketHeader['recordID']
							);
			$updatePostage = query($conn, $updatePostageQuery, $strType, $arrdbparams);
			

			if(!empty($arrcantorderitems))
			{
				print ("<div class='postage'>");
				print ("<table style='float:right; clear:right; margin: 20px 0;' cellspacing='0' cellpadding='3'>");
				print ("<tbody>");
				print ("<tr>");
				print ("<td class='linehead'>Sorry - we currently only offer local delivery for the following products</td>");
				print ("</tr>");
				foreach($arrcantorderitems AS $cantorderitem)
				{
					print("<tr>");
						print("<td class='line'>" . $cantorderitem . "</td>");
					print("</tr>");
				}
				print ("<tr>");
				print ("<td><br/><p>Please <a href='/contact.php'>contact us</a> if you wish to enquire further.</p></td>");
				print ("</tr>");
				print ("</tbody>");
				print ("</table>");
				print ("</div>");
				print ("<div style='clear:right; margin-top: 20px;' ><div class='leftOption'><a class='short_button' href='http://stollers.co.uk/index.php'>Continue Shopping</a></div>");
				print ("<div class='rightOption'><a class='short_button' href='/checkout.php'>Return to Basket</a></div>");
				print ("</div>");
			} else {
				print ("<div class='postage'>");
				print ("<table style='float:right; clear:right; margin: 20px 0;' cellspacing='0' cellpadding='3'>");
				print ("<tbody>");
				print ("<tr>");
				print ("<td class='linehead'>Item</td>");
				print ("<td class='linehead' align='right'>Value</td>");
				print ("</tr>");
				
				if ($basketHeader['offerCodeID'] > 0) {
					$dectotcost = $dectotcost - $basketHeader['discountAmount'];
					print ("<tr><td class='line'>".$strdiscountdesc.".</td>");
					print ("<td class='line' align='right'> - &pound; ".number_format($basketHeader['discountAmount'], 2)."</td></tr>");
				}

				if ($postageCost != 0)
				{
					print ("<tr><td class='evenrow line'>Delivery</td>");
					print ("<td class='evenrow line' align='right' id='ajax_stanpostage'>&pound;&nbsp;".number_format($postageCost,2)."</td>");
					print ("</tr>");
				}
				else
				{
					print ("<tr><td class='evenrow line'>FREE DELIVERY</td>");
					print ("<td class='evenrow line' align='right' id='ajax_stanpostage'>&pound;&nbsp;".number_format($postageCost,2)."</td>");
					print ("</tr>");
				}
				$dectotcost = $dectotcost + $postageCost;
				
				//get VAT rate
				$strdbsql = "SELECT * FROM site_lookup WHERE description = 'VAT'";
				$strType = "single";
				$arrdbparams = array();
				$vatDetails = query($conn,$strdbsql,$strType,$arrdbparams);
				$decvatrate = $vatDetails['value'];
				$decvatamt = $dectotcost * ($decvatrate/100);
				$dectotcost = $dectotcost + $decvatamt;

				print ("<tr>");
				print ("<td class='linehead'>Total:</td>");
				print ("<td class='linehead' align='right'>&pound;&nbsp;".number_format($dectotcost,2)."</td>");
				print ("</tr>");
				print ("</tbody>");
				print ("</table>");
				print ("</div>");
				print ("<div style='clear:right; margin-top: 20px;' ><div class='basketOptions'><div class='leftOption'><a class='short_button' href='http://stollers.co.uk/index.php'>Continue Shopping</a></div>");
				print ("<div class='rightOption'><a href='pplaunch.php' class='short_button continue'>Pay at Paypal</a></div>");
				print ("<a class='short_button' href='/checkout.php'>Return to Basket</a></div>");
				print ("<br/><br/><br/><div class='basketOptions' style='text-align:right'><a href='pplaunch.php'><img style='border:0px' src='/images/horizontal_solution_PPeCheck.png'></a></div>");
				print ("</div>");
			}
			//print("</div>");
		}
		elseif($strView == "viewBasket")
		{
			print ("<div style='clear:both;overflow:hidden;'>");
				print ("<div class='leftOption'>");
					print ("<a onclick='jsdeletebasket(".$basketHeader['recordID'].")' class='short_button'>");
						print ("Delete");
					print ("</a>");
				print ("</div>");
				print ("<div class='rightOption'>");
					print ("<a onclick='jscopybasket(".$basketHeader['recordID']."); return false;' href='#' class='short_button'>");
						print ("Copy to Current Basket");
					print ("</a>");
				print ("</div>");
			print ("</div>");
		}
	}*/
	/*function fnUpdateBasketAfterPaypal($basketID, $orderHeaderID, $conn){
		$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basketID";
		$strType = "multi";
		$arrdbparams = array("basketID"=>$basketID);
		$basketItems = query($conn,$strdbsql,$strType,$arrdbparams);

		// Move basket contents to complete folder
		foreach ($basketItems AS $basketItem) {
			
			$strdbsql = "INSERT INTO order_items (orderHeaderID, stockID, stockDetailsArray, quantity, price) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price);";
			$strType = "insert";
			$arrdbparams = array("orderHeaderID"=>$orderHeaderID, "stockID"=>$basketItem['stockID'], "stockDetailsArray"=>$basketItem['stockDetailsArray'], "quantity"=>$basketItem['quantity'], "price"=>$basketItem['price']);
			$insertResult = query($conn,$strdbsql,$strType,$arrdbparams);

			$strdbsql = "SELECT * FROM stock WHERE recordID = :stockID";
			$strType = "single";
			$arrdbparams = array("stockID"=>$basketItem['stockID']);
			$stockDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			
			if ($stockDetails['stockLevels'] >= $basketItem['quantity']) {
				$stockLevel = $stockDetails['stockLevels'] - $basketItem['quantity'];
			} else {
				$stockLevel = 0;
			}
			
			$strdbsql = "UPDATE stock SET stockLevels = :stockLevel WHERE recordID = :stockID";
			$strType = "update";
			$arrdbparams = array("stockID"=>$basketItem['stockID'], "stockLevel"=>$stockLevel);
			$updateResult = query($conn,$strdbsql,$strType,$arrdbparams);
		}

		// Delete from basket tables
		$strdbsql = "DELETE FROM basket_items WHERE basketHeaderID = :basketID";
		$strType = "delete";
		$arrdbparams = array("basketID"=>$basketID);
		$deleteResult = query($conn,$strdbsql,$strType,$arrdbparams);
		
		$strdbsql = "DELETE FROM basket_header WHERE recordID = :basketID";
		$strType = "delete";
		$arrdbparams = array("basketID"=>$basketID);
		$deleteResult = query($conn,$strdbsql,$strType,$arrdbparams);
	}*/
	
	function fnGetSOP($orderID, $stockIDEdit, $datnow, $conn){
		
		//check to see existing order in creation
		if (!empty($orderID)){
			$strdbsql = "SELECT * FROM order_header WHERE recordID = :recordID";
			$strType = "single";
			$arrdbparams = array("recordID"=>$orderID);
			$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
			$customerID = $orderHeader['customerID'];
			$orderStatus = $orderHeader['orderStatus'];
			$subTotal = $orderHeader['amountStock'];
			$vatTotal = $orderHeader['amountVat'];
			$paidTotal = $orderHeader['paymentAmount'];
			$orderTotal = $orderHeader['amountTotal'];
			$orderDelivery = $orderHeader['amountDelivery'];
			$orderOffer = $orderHeader['amountOffer'];
			if ($orderStatus > 4 OR $orderStatus == 3) {$disabled = "readonly"; $display = "display: none;";}
			else {$disabled = ""; $display = "";}

			$strdbsql = "SELECT order_items.*, site_vat.amount AS vatRate, order_dispatch.description 
			FROM order_items INNER JOIN site_vat ON order_items.vatID = site_vat.recordID INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID WHERE orderHeaderID = :recordID AND display = 1";
			$strType = "multi";
			$arrdbparams = array("recordID"=>$orderID);
			$orderItems = query($conn,$strdbsql,$strType,$arrdbparams);
			
			/*$strdbsql = "SELECT sum(transactionAmount) AS amountPaid FROM order_payment_details INNER JOIN order_payment_status ON order_payment_details.paymentStatus = order_payment_status.recordID WHERE orderHeaderID = :recordID AND order_payment_status.countTowardsTotal = 1";
			$strType = "single";
			$arrdbparams = array("recordID"=>$orderID);
			$paymentDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			$paid = $paymentDetails['amountPaid'];*/
		
		} else {
			$subTotal = number_format("0", 2);
			$vatTotal = number_format("0", 2);
			$orderTotal = number_format("0", 2);
			$paid = number_format("0", 2);
			$orderStatus = 1;
			$disabled = "";
		}
		
		//customer details
		print ("<div class='row'>");
			print ("<div class='col-md-6 split even-left'>");
				fnGetCustomerDetails($orderID, $customerID, $disabled, $datnow, $conn);
			print ("</div>");
			print ("<div class='col-md-6 split even-right'>");
				fnGetOrderDetails($orderID, $disabled, $datnow, $conn);
			print ("</div>");
		print ("</div>");
		
		
		
		//Stock items
		print("<div id='stockItemsSection' class='section'>");
			print("<fieldset>");
				print("<legend>Items</legend>");
				
				print("<div class='row'>");
					print("<div class='form-group'>");

						print ("<table class='member-list table table-striped table-bordered table-hover table-condensed'>");
							print ("<thead><tr>");
								if ($orderStatus < 3) print ("<th></th>");
								print ("<th>Stock Code</th>");
								print ("<th>Name</th>");
								print ("<th>Description</th>");
								print ("<th>Quantity</th>");
								print ("<th>Discount %</th>");
								print ("<th>Price</th>");
								print ("<th>VAT</th>");
								print ("<th>Total</th>");
								if ($orderStatus < 3) print ("<th>Edit</th>");
							print ("</tr></thead><tbody>");
							
							//ajax to get items
							if (count($orderItems) > 0) {
								foreach($orderItems AS $orderItem){
									
									$strdbsql = "SELECT stock.*, 
									stock_group_information.name AS stockName, 
									stock_group_information.summary, 
									stock_group_information.description, 
									stock_group_information.productType, stock_group_information.productCost, 
									stock_units.description AS unit
									FROM stock 
									INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
									INNER JOIN stock_units ON stock.unit = stock_units.recordID 
									WHERE stock.recordID = :product AND stock_group_information.statusID <> 4";
									$productDetails = query($conn,$strdbsql,"single",array("product"=>$orderItem['stockID']));
									
									$stockDetails = unserialize($orderItem['stockDetailsArray']);
									//for sites with Ex VAT pricing
									/*
									$lineAmount = number_format($orderItem['price']*$orderItem['quantity'],2);
									if ($orderItem['vatRate'] == 0) {
										$lineTax = "0.00";
									} else {
										$lineTax = number_format(($lineAmount / 100) * $orderItem['vatRate'],2);
									}
									$lineTotal = number_format($lineAmount + $lineTax,2);*/
									
									
									//for sites with Inc VAT pricing
									$lineTotal = number_format($orderItem['price']*$orderItem['quantity'],2);
									if ($orderItem['vatRate'] == 0) {
										$lineTax = 0.00;
									} else {
										$lineTax = number_format($lineTotal - ($lineTotal/(($orderItem['vatRate']+100) / 100)),2);
									}
									$lineAmount = number_format($lineTotal - $lineTax,2);

									print ("<tr>");
										if ($orderStatus < 3) print ("<td><a class='btn btn-danger circle' href='#' $disabled onclick='jsDeleteStockItem(".$orderItem['recordID']."); return false;' ><i class='fa fa-trash'></i></a></td>");
										print ("<td>".$productDetails['stockCode']."</td>");
										print ("<td>".$productDetails['stockName']."</td>");
										print ("<td>".$productDetails['unit']."</td>");
										print ("<td>".$orderItem['quantity']."</td>");
										print ("<td>".number_format($orderItem['discountPerc'],2)."%</td>");
										print ("<td>&pound;".$lineAmount."</td>");
										print ("<td>&pound;".$lineTax."</td>");
										print ("<td>&pound;".$lineTotal."</td>");
										if ($orderStatus < 3) print ("<td><a class='btn btn-primary circle' href='#' $disabled onclick='jsEditStockItem(".$orderItem['recordID']."); return false;' ><i class='fa fa-pencil'></i></a></td>");
									print ("</tr>");
									
								}
							
							} else {
								print ("<tr><td colspan='10'>No stock lines</td></tr>");
							}
							print ("</tbody>");
						print ("</table>");
							
							
						//add stock
						print("<div class='row hide' style='$display'>");
						
							//load details of stock to change
							if (!empty($stockIDEdit)){

								$strdbsql = "SELECT order_items.*, site_vat.amount FROM order_items INNER JOIN site_vat ON order_items.vatID = site_vat.recordID WHERE order_items.recordID = :recordID";
								$strType = "single";
								$arrdbparams = array("recordID"=>$stockIDEdit);
								$stockEditDetails = query($conn,$strdbsql,$strType,$arrdbparams);
								$stockDetails = unserialize($stockEditDetails['stockDetailsArray']);
								
								//remove line after getting details
								fnRemoveStockLine($stockIDEdit, $conn);
								
								
				

							} else {
								
								$stockEditDetails = array();
								$stockEditDetails['stockID'] = "";
								$stockEditDetails['quantity'] = "";
								$stockEditDetails['price'] = "";
								$stockEditDetails['discountPerc'] = "";
								$stockEditDetails['vatID'] = "";
								$stockDetails = array();
								$stockDetails['name'] = "";
								$stockDetails['description'] = "";
								$stockDetails['stockCode'] = "";
							}

							//stock code
							print("<div class='form-group col-sm-4 even-left'>");
							
							//onclick=\"setitem('this.value','stock');\"
							
								print ("<select $disabled name='stockCodeAdd' id='stockCodeAdd' class='form-control' >");
								$strdbsql = "SELECT stock.*, stock_group_information.name, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock_group_information.statusID > 2 ORDER BY stockCode ASC";
								$arrType = "multi";
								$strdbparams = array();
								$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
								print ("<option value=''>Please Select</option>");
								foreach ($resultdata AS $row)
								{
									if ($row['recordID'] == $stockEditDetails['stockID']) $selected = "selected"; else $selected = "";
									print("<option $selected value='".$row['recordID']."'>".$row['stockCode']." (".$row['weight'].")</option>");
								}
								print("</select>");
								print ("<input type='hidden' value='".$stockDetails['stockCode']."' name='stockCodeHidden' id='stockCodeHidden'/>");
							print("</div>");
							
							//quantity
							print("<div class='form-group col-sm-2 even-left'>");
								print("<input $disabled class='form-control' style='margin-top:0px;' id='quantityAdd' name='quantityAdd' type='text' placeholder='Quantity' value='".$stockEditDetails['quantity']."' />");
							print("</div>");

							//discount
							print("<div class='form-group col-sm-2 even-left'>");
								print("<input $disabled class='form-control' style='margin-top:0px;' id='discountAdd' name='discountAdd' type='text' placeholder='Discount %' value='".$stockEditDetails['discountPerc']."' />");
							print("</div>");
							
							//price
							print("<div class='form-group col-sm-2 even-left'>");
								print("<input $disabled class='form-control' style='margin-top:0px;' id='priceAdd' name='priceAdd' type='text' placeholder='Price' value='".$stockEditDetails['price']."' />");
							print("</div>");

							//vat
							print("<div class='form-group col-sm-2 even-left'>");
								print ("<select $disabled class='form-control' name='vatSelectAdd' id='vatSelectAdd' >");
									$strdbsql = "SELECT * FROM site_vat";
									$strType = "multi";
									$resultdata = query($conn, $strdbsql, $strType);								  
									foreach ($resultdata AS $row)
									{
										if ($row['recordID'] == $stockEditDetails['vatID']) $selected = "selected"; else $selected = "";
										print("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
									}
								print("</select>");
							print("</div>");

						print("</div>");
						
						print("<div class='row hide' style='$display'>");

							//stock name
							print("<div class='form-group col-sm-4 even-left'>");
								print("<input $disabled class='form-control' readonly id='nameAdd' name='nameAdd' type='text' placeholder='Name' value='".$stockDetails['name']."' />");
							print("</div>");
						
							//stock description
							print("<div class='form-group col-sm-8 even-left'>");
								print("<input $disabled class='form-control' readonly id='descriptionAdd' name='descriptionAdd' type='text' placeholder='Description' value='".$stockDetails['description']."' />");
							print("</div>");
						print("</div>");
						print("<div class='row hide' style='$display'>");
							
							//button
							print("<div class='form-group col-sm-12 even-right'>");
								print("<button $disabled onclick='return jsAddStockItem();' type='submit' class='btn btn-primary right'>Add</button>");
							print("</div>");
						print("</div>");
						

						print ("<script type='text/javascript'>	");
							print ("$('#stockIDAdd').val(".$stockEditDetails['stockID'].");");
						print ("</script>");

					print ("</div>");
				print ("</div>");
				
			print ("</fieldset>");
		print ("</div>");

		//sub totals
		print("<div class='section hide'>");
			print("<fieldset>");
				print("<legend>Payments</legend>");
				
				print("<div class='row'>");
					print("<div class='form-group'>");
						$amountOutstanding = number_format($orderTotal - $paid, 2);
						fnGetPayments($orderID, $disabled, $amountOutstanding, $datnow, $conn);
					print("</div>");
				print("</div>");
				
			print("</fieldset>");
		print("</div>");
		
		print("<div class='row'>");		
			print("<div class='col-md-6'>");		
				print("<div class='section'>");
					print("<fieldset>");
						print("<legend>Order Totals</legend>");
						
						print("<div class='row'>");	
							print ("<div class='form-group'>");
								print("<label class='control-label'>Net Total</label>");
								print("&pound;".number_format($subTotal,2)."");
							print("</div>");
						print("</div>");
						print("<div class='row'>");	
							print ("<div class='form-group'>");
								print("<label class='control-label'>VAT Total</label>");
								print("&pound;".number_format($vatTotal,2)."");
							print("</div>");
						print("</div>");
						print("<div class='row'>");	
							print("<div class='form-group'>");
								print("<label class='control-label'>Delivery</label>");
								print("&pound;".number_format($orderDelivery,2)."");
							print("</div>");
						print("</div>");
						print("<div class='row'>");	
							print("<div class='form-group'>");
								print("<label class='control-label'>Total</label>");
								print("&pound;".number_format($orderTotal,2)."");
							print("</div>");
						print("</div>");
						print("<div class='row'>");	
							print("<div class='form-group'>");
								print("<label class='control-label'>Amount Paid</label>");
								print("&pound;".number_format($paidTotal,2)."");
							print("</div>");
						print("</div>");
						
					print("</fieldset>");
				print ("</div>");
			print ("</div>");
		print ("</div>");

		//buttons
		print("<div class='row'>");
			print("<div class='col-sm-6' style='text-align:left;'>");
			//	if ($orderStatus < 3 AND ($paid == "" OR $paid == 0)) print ("<button onclick='return jsDelete();' class='btn btn-danger left'>Delete</button>"); //need rules on this e.g. order status and payment status
			//	if ($paid == "" OR $paid == 0) print ("<button onclick='return jsDelete();' class='btn btn-danger left'>Delete</button>"); //need rules on this e.g. order status and payment status
				
				//if email address exists, order status is complete and they havent already been sent an email show email confirmation button
			//	if (!empty($orderID)) print("<button onclick='return jsDuplicateOrder()();' type='submit' style='margin-right: 10px;' class='btn btn-primary right'>Duplicate Order</button> ");
				if (!empty($orderID)) print("<button onclick='return jsEmailConfirmation();' type='submit' style='margin-right: 10px;' class='btn btn-primary left'>Email Confirmation</button> ");
				if (!empty($orderID)) print("<button onclick='return jsprintinvoice($orderID);' type='submit' style='margin-right: 10px;' class='btn btn-primary left'>Print Invoice</button> ");
			//	if (!empty($orderID)) print("<button onclick='return jsprintgiftmessage($orderID);' type='submit' style='margin-right: 10px;' class='btn btn-primary right'>Print Gift Message</button> ");
			
			print ("</div>");
			
			
			print("<div class='col-sm-6' style='text-align:right;'>");
				print("<button onclick='return jsSaveOrder();' type='submit' class='btn btn-success right'>Save</button>");
			print("</div>");
		print ("</div>");

		
	}
	function fnGetCustomerDetails($orderID, $customerID, $disabled, $datnow, $conn){
	
		if (!empty($orderID)) {
		
			$strdbsql = "SELECT order_header.*, customer.firstname, customer.surname, customer.title, customer.company, customer.mobile, customer.fax FROM order_header LEFT JOIN customer ON order_header.customerID = customer.recordID WHERE order_header.recordID = :recordID";
			$strType = "single";
			$arrdbparams = array("recordID"=>$orderID);
			$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strfirstname = $orderHeader['firstname'];
			$strsurname = $orderHeader['surname'];
			$strtitle = $orderHeader['title'];
			$strcompany = $orderHeader['company'];
			$strphone = $orderHeader['telephone'];
			$strmobile = $orderHeader['mobile'];
			$stremail = $orderHeader['email'];
			$strfax = $orderHeader['fax'];
			$strnotes = $orderHeader['notes'];
			$strdelfirstname = $orderHeader['addDeliveryFirstname'];
			$strdelsurname = $orderHeader['addDeliverySurname'];
			$strdeltitle = $orderHeader['addDeliveryTitle'];
			$strdeladd1 = $orderHeader['addDelivery1'];
			$strdeladd2 = $orderHeader['addDelivery2'];
			$strdeladd3 = $orderHeader['addDelivery3'];
			$strdelTown = $orderHeader['addDelivery4'];
			$strdelCnty = $orderHeader['addDelivery5'];
			$strdelCntry = $orderHeader['addDelivery6'];
			$strdelPstCde = $orderHeader['addDeliveryPostcode'];
			$strbilfirstname = $orderHeader['addBillingFirstname'];
			$strbilsurname = $orderHeader['addBillingSurname'];
			$strbiltitle = $orderHeader['addBillingTitle'];
			$strbiladd1 = $orderHeader['addBilling1'];
			$strbiladd2 = $orderHeader['addBilling2'];
			$strbiladd3 = $orderHeader['addBilling3'];
			$strbilTown = $orderHeader['addBilling4'];
			$strbilCnty = $orderHeader['addBilling5'];
			$strbilCntry = $orderHeader['addBilling6'];
			$strbilPstCde = $orderHeader['addBillingPostcode'];
			$giftee = $orderHeader['gift'];
		
		} else {
			$strfirstname = $_REQUEST['firstname'];
			$strsurname = $_REQUEST['surname'];
			$strtitle = $_REQUEST['title'];
			$strcompany = $_REQUEST['company'];
			$strphone = $_REQUEST['phone'];
			$strmobile = $_REQUEST['mobile'];
			$stremail = $_REQUEST['email'];
			$strfax = $_REQUEST['fax'];
			$strnotes = $_REQUEST['notes'];
			$strdelfirstname = $_REQUEST['delfirstname'];
			$strdelsurname = $_REQUEST['delsurname'];
			$strdeltitle = $_REQUEST['deltitle'];
			$strdeladd1 = $_REQUEST['deladd1'];
			$strdeladd2 = $_REQUEST['deladd2'];
			$strdeladd3 = $_REQUEST['deladd3'];
			$strdelTown = $_REQUEST['delTown'];
			$strdelCnty = $_REQUEST['delCnty'];
			$strdelCntry = $_REQUEST['delCntry'];
			$strdelPstCde = $_REQUEST['delPstCde'];
			$strbilfirstname = $_REQUEST['bilfirstname'];
			$strbilsurname = $_REQUEST['bilsurname'];
			$strbiltitle = $_REQUEST['biltitle'];
			$strbiladd1 = $_REQUEST['biladd1'];
			$strbiladd2 = $_REQUEST['biladd2'];
			$strbiladd3 = $_REQUEST['biladd3'];
			$strbilTown = $_REQUEST['bilTown'];
			$strbilCnty = $_REQUEST['bilCnty'];
			$strbilCntry = $_REQUEST['bilCntry'];
			$strbilPstCde = $_REQUEST['bilPstCde'];
			$giftee = $_REQUEST['giftee'];
		}
		
		print("<div class='section'>");
			print("<fieldset>");
				print("<legend>Billing Customer Details</legend>");
				
				print("<div class='row'>");
					print("<div class='form-group'>");
						print("<label class='control-label'>Postcode Search:</label>");
						print("<input $disabled class='form-control' type='text' class='form-control' id='frm_customerSearch' name='frm_customerSearch' value='' onkeyup='jsloadSuggestions(\"frm_customerSearch\",\"customer\",\"customerSearchResults\")' />");
					print("</div>");
				print("</div>");
				
			print("</fieldset>");	
		print("</div>");	
		
		print ("<div id='customerSearchResults' style='position:absolute;z-index:10;left:45px;right:45px;top:160px;'></div>");
		
		print("<div class='section'>");
			
			print("<ul id='tabs' class='nav nav-tabs' data-tabs='tabs'>");
				print("<li class='active'>");
					print("<a href='#customer' data-toggle='tab'>Customer Details</a>");
				print("</li>");
				
				print("<li>");
					print("<a href='#delivery' data-toggle='tab'>Delivery Address</a>");
				print("</li>");
				
				print("<li>");
					print("<a href='#billing' data-toggle='tab'>Billing Address</a>");
				print("</li>");
			print("</ul>");
			
			print("<div id='my-tab-content' class='tab-content'>");
				
				print("<div class='tab-pane active' id='customer'>");
					print ("<div id='customer'>");		
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input class='form-control' readonly style='margin-top:0px;' id='title' name='title' type='text' size='10' placeholder='Title' value='$strtitle' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input class='form-control' readonly style='margin-top:0px;' id='firstname' name='firstname' type='text' size='30' placeholder='First Name' value='$strfirstname' />");
							print("</div>");

							print("<div class='form-group col-sm-6 even-right'>");
								print("<input class='form-control' readonly style='margin-top:0px;' id='surname' name='surname' type='text' size='30' placeholder='Surname' value='$strsurname' />");
							print("</div>");
						print("</div>");
						/*print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input class='form-control' readonly style='margin-top:0px;' id='company' type='text' name='company' size='30' placeholder='Company' value='$strcompany' />");
							print("</div>");
						print("</div>");*/
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input class='form-control' readonly style='margin-top:0px;' type='text' id='phone' name='phone' size='30' placeholder='Phone Number' value='$strphone' />");
							print("</div>");
							
							print("<div class='form-group col-sm-6 even-right'>");
								print("<input class='form-control' readonly style='margin-top:0px;' type='text' id='mobile' name='mobile' size='30' placeholder='Mobile' value='$strmobile' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input class='form-control' style='margin-top:0px;' type='text' id='email' name='email' size='30' placeholder='Email' value='$stremail' />");
							print("</div>");
						print("</div>");
						/*print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input class='form-control' disabled style='margin-top:0px;' type='text' id='fax' name='fax' size='30' placeholder='Fax Number' value='$strfax' />");
							print("</div>");
						print("</div>");*/
					print("</div>");
				print("</div>");
				
				print("<div class='tab-pane' id='delivery'>");
					print ("<div id='deltable'>");
						
						//select / search customer for address
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label class='control-label'>Search:</label>");
								print("<input $disabled class='form-control' type='text' class='form-control' id='frm_customerSearchDelivery' name='frm_customerSearchDelivery' value='' onkeyup='jsloadSuggestions(\"frm_customerSearchDelivery\",\"customerDeliveryAdd\",\"customerSearchResultsDelivery\")' />");
								print ("<div id='customerSearchResultsDelivery' style='position:absolute;z-index:10;left:45px;right:45px;top:340px;'></div>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='deltitle' name='deltitle' type='text' size='10' placeholder='Title' value='$strdeltitle' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='delfirstname' name='delfirstname' type='text' size='28' placeholder='First Name' value='$strdelfirstname' />");
							print("</div>");

							print("<div class='form-group col-sm-6 even-right'>");
								print("<input $disabled class='form-control' id='delsurname' name='delsurname' type='text' size='28' placeholder='Surname' value='$strdelsurname' />");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='delPstCde' class='postCode' name='delPstCde' type='text' size='23' placeholder='Post Code' value='$strdelPstCde' />");
							print("</div>");

							print("<div class='form-group col-sm-6 even-right'>");
								print("<a $disabled onclick='jsPostcodeLookup(\"delivery\"); return false;' style='float:left;' class='btn btn-primary'>Lookup</a>");
								print ("<div id='ajax_addressDel' style='float:left; margin: 5px 10px ' name='ajax_addressDel' ></div>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='deladd1' placeholder='Address 1' name='deladd1' type='text' size='28' value='$strdeladd1' />");
							print("</div>");
							print("<div class='form-group col-sm-6 even-right'>");
								print("<input $disabled class='form-control' id='deladd2' placeholder='Address 2' name='deladd2' type='text' size='28' value='$strdeladd2' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");	
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='deladd3' placeholder='Address 3' name='deladd3' type='text' size='28' value='$strdeladd3' />");
							print("</div>");
							print("<div class='form-group col-sm-6 even-right'>");
								print("<input $disabled class='form-control' id='delTown' placeholder='Town' name='delTown' type='text' size='28' value='$strdelTown' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='delCnty' placeholder='County' name='delCnty' type='text' size='28' value='$strdelCnty' />");
							print("</div>");
							print("<div class='form-group col-sm-6 even-right'>");
								if ($strdelCntry == "") $strdelCntry = "GB";
								$strdbsql = "SELECT * FROM configCountries ORDER BY countryName";
								$arrdbparams = array();
								$strType = "multi";
								$countries = query($conn, $strdbsql, $strType, $arrdbparams);
								print ("<select $disabled name='delCntry' id='delCntry' class='form-control' placeholder='Country' >");
								foreach($countries AS $row){	
									if ($strdelCntry == $row['countryCode']) $selected = "selected"; else $selected = "";
									print ("<option $selected value='".$row['countryCode']."'>".$row['countryName']."</option>");
								}
								print ("</select>");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print ("<select name='giftee' id='giftee' class='form-control' placeholder='Giftee' >");
									if ($giftee != 1) $selected = "selected"; else $selected = "";
									print ("<option $selected value='0'>Non Giftee</option>");
									if ($giftee == 1) $selected = "selected"; else $selected = "";
									print ("<option $selected value='1'>Giftee</option>");
								print ("</select>");
							print("</div>");

							print("<div class='form-group col-sm-6 even-right'>");
								print("<a $disabled onclick='jsClearAddress(\"delivery\"); return false;' style='float:left;' class='btn btn-primary'>Clear Details</a>");
							print("</div>");
						print("</div>");
					print ("</div>");
				print("</div>");
				
				print("<div class='tab-pane' id='billing'>");
					print ("<div id='biltable'>");
						
						
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='biltitle' name='biltitle' type='text' size='10' placeholder='Title' value='$strbiltitle' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='bilfirstname' name='bilfirstname' type='text' size='28' placeholder='First Name' value='$strbilfirstname' />");
							print("</div>");

							print("<div class='form-group col-sm-6 even-right'>");
								print("<input $disabled class='form-control' id='bilsurname' name='bilsurname' type='text' size='28' placeholder='Surname' value='$strbilsurname' />");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='bilPstCde' class='postCode' name='bilPstCde' type='text' size='23' placeholder='Post Code' value='$strbilPstCde' />");
							print("</div>");
							
							print("<div class='form-group col-sm-6 even-right'>");
								print("<a $disabled onclick='jsPostcodeLookup(\"billing\"); return false;' style='float:left;' class='btn btn-primary'>Lookup</a>");
								print ("<div id='ajax_addressBill' style='float:left; margin: 5px 10px;' name='ajax_addressBill' ></div>");
							print("</div>");
						print("</div>");
												
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='biladd1' placeholder='Address 1' name='biladd1' type='text' size='28' placeholder='Address 1' value='$strbiladd1' />");
							print("</div>");
							print("<div class='form-group col-sm-6 even-right'>");
								print("<input $disabled class='form-control' id='biladd2' placeholder='Address 2' name='biladd2' type='text' size='28' placeholder='Address 2' value='$strbiladd2' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='biladd3' placeholder='Address 3' name='biladd3' type='text' size='28' placeholder='Address 3' value='$strbiladd3' />");
							print("</div>");
							print("<div class='form-group col-sm-6 even-right'>");
								print("<input $disabled class='form-control' id='bilTown' placeholder='Town' name='bilTown' type='text' size='28' placeholder='Town' value='$strbilTown' />");
							print("</div>");
						print("</div>");
						print("<div class='row'>");
							print("<div class='form-group col-sm-6 even-left'>");
								print("<input $disabled class='form-control' id='bilCnty' placeholder='County' name='bilCnty' type='text' size='28' placeholder='County' value='$strbilCnty' />");
							print("</div>");
							print("<div class='form-group col-sm-6 even-right'>");
								if ($strbilCntry == "") $strbilCntry = "GB";

								print ("<select $disabled name='bilCntry' id='bilCntry' class='form-control' placeholder='Country' >");
								foreach($countries AS $row){	
									if ($strbilCntry == $row['countryCode']) $selected = "selected"; else $selected = "";
									print ("<option $selected value='".$row['countryCode']."'>".$row['countryName']."</option>");
								}
								print ("</select>");
							print("</div>");
						print("</div>");
					print ("</div>");
				print("</div>");
			print("</div>");
				
		print ("</div>");
	}
	function fnGetOrderDetails($orderID, $disabled, $datnow, $conn){
		
		if (!empty($orderID)){
			$strdbsql = "SELECT * FROM order_header WHERE recordID = :recordID";
			$strType = "single";
			$arrdbparams = array("recordID"=>$orderID);
			$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
			$orderNo = $orderID;
			$orderDate = fnconvertunixtime($orderHeader['timestampOrder']);
			$orderSource = $orderHeader['orderSource'];
			$orderStatus = $orderHeader['orderStatus'];
			$orderNote = $orderHeader['notes'];
			$giftMessage = $orderHeader['giftMessage'];
			$estDeliveryDate = $orderHeader['addDeliveryDate'];
			$estDeliveryTime = $orderHeader['addDeliveryTime'];
			$dispatch = $orderHeader['dispatch'];
			$dispatchDate = $orderHeader['dispatchDate'];
			$externalOrderID = $orderHeader['externalOrderID'];
			
		} else {
			$orderNo = "";
			$orderDate = fnconvertunixtime($datnow);
			$orderSource = ""; //select
			$orderStatus = ""; //select - quote / ordered / dispatched (order_status)
			$paymentStatus = ""; //select - No payment made / part payment / full payment (order_payment_status)
			$paymentAmount = ""; //decimal
			$orderNote = "";
			$giftMessage= "";
			$dispatch = "";
			$dispatchDate = "";
			$externalOrderID = "";
		}		
		
		print("<div class='section'>");
			print("<fieldset>");
				print("<legend>Order Details</legend>");
		
				print ("<div id='order'>");
					print("<div class='row'>");
						print("<div class='form-group col-sm-6 even-left'>");
							print("<label class='control-label'>Order No:</label>");
							print("<input class='form-control' disabled type='text' id='frm_orderNo' name='frm_orderNo' value='".$orderNo."' />");
						print("</div>");
						print("<div class='form-group col-sm-6 even-right'>");
							print("<label class='control-label'>Order Date:</label>");
							print("<input $disabled class='form-control' type='text' id='frm_orderDate' name='frm_orderDate' value='".$orderDate."' />");
						print("</div>");
					print("</div>");
					print("<div class='row'>");
						print("<div class='form-group col-sm-6 even-left'>");
							print("<label class='control-label'>Order Source:</label>");
							print("<select $disabled class='form-control' name='frm_orderSource' id='frm_orderSource'>");
								$strdbsql = "SELECT * FROM order_source";
								$strType = "multi";
								$resultdata = query($conn, $strdbsql, $strType);								  
								foreach ($resultdata AS $row)
								{
									$strSelected = "";
									if($row['recordID'] == $orderSource) $strSelected = " selected";
									print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']."</option>");
								}
							print("</select>");
						print("</div>");
						print("<div class='form-group col-sm-6 even-right'>");
								print("<label class='control-label'>Order Status:</label>");
								print("<select $disabled class='form-control' name='frm_orderStatus' id='frm_orderStatus'>");
								$strdbsql = "SELECT * FROM order_status";
								$strType = "multi";
								$resultdata = query($conn, $strdbsql, $strType);								  
								foreach ($resultdata AS $row)
								{
									$strSelected = "";
									if($row['recordID'] == $orderStatus) $strSelected = " selected";
									print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']."</option>");
								}
							print("</select>");
						print("</div>");
					print("</div>");
					print("<div class='row hide'>");
						
						//dispatch select
						print("<div class='form-group col-sm-6 even-left'>");
							print("<label class='control-label'>Dispatch:</label>");
							print ("<select $disabled name='dispatchAdd' id='dispatchAdd' class='form-control'>");
							$strdbsql = "SELECT * FROM order_dispatch";
							$strType = "multi";
							$resultdata = query($conn, $strdbsql, $strType);								  
							foreach ($resultdata AS $row)
							{
								if ($dispatch == $row['recordID']) $selected = "selected"; else $selected = "";
								print("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
							}
							print("</select>");
						print ("</div>");
						print("<div class='form-group col-sm-6 even-right'>");
							print("<label class='control-label'>Dispatch Date:</label>");
							if ($dispatchDate > 0) $dispatchDateDisp = fnconvertunixtime($dispatchDate); else $dispatchDateDisp = "";
							print ("<input $disabled type='text' class='form-control' style='' id='dispatchDateAdd' name='dispatchDateAdd' value='$dispatchDateDisp' />");
						print ("</div>");
					print ("</div>");
					
					
					if (!empty($orderID)) {
										
						$strdbsql = "SELECT delivery.*, deliveryOptionRates.deliveryOptionID FROM delivery INNER JOIN deliveryOptionRates ON delivery.deliveryOptionRateID = deliveryOptionRates.recordID WHERE delivery.orderHeaderID = :orderHeaderID";
						$strType = "single";
						$arrdbparam = array("orderHeaderID"=>$orderID);
						$delivery = query($conn, $strdbsql, $strType, $arrdbparam);
						
						if (empty($delivery['recordID'])) {
							$delivery['deliveryDate'] = $datnow;
							$delivery['deliveryOptionRateID'] = "";
							$delivery['packageTypeID'] = "";
							$delivery['weight'] = 0;
							$delivery['notes'] = "";
							$delivery['deliveryStatusID'] = 1;
							$delivery['pickList'] = "";
						}

					} else {
						$delivery['deliveryDate'] = $datnow;
						$delivery['deliveryOptionRateID'] = "";
						$delivery['packageTypeID'] = "";
						$delivery['weight'] = 0;
						$delivery['notes'] = "";
						$delivery['deliveryStatusID'] = 1;
						$delivery['pickList'] = "";
					}

					
					print("<div class='row hide'>");
					
						//delivery day / timestamp
						print("<div class='form-group col-md-3' style='padding-right:15px;'>");
							print("<label for='frm_deliveryDate' class='control-label'>Dispatch Date:</label>");
							print("<input type='text' class='form-control' id='frm_deliveryDate' name='frm_deliveryDate' value='".date("d/m/Y", $delivery['deliveryDate'])."' />");
						print("</div>");
						
						//delivery batch
						print("<div class='form-group col-md-3' style='padding: 0 15px;'>");
							print("<label for='frm_pickList' class='control-label'>Pick List:</label>");
							print("<select name='frm_pickList' id='frm_pickList' class='form-control'>");
								$strdbsql = "SELECT * FROM deliveryPickList WHERE closed = 0";
								//$strdbsql = "SELECT * FROM deliveryPickList WHERE pickdate = :deliveryDate AND closed = 0";
								$strType = "multi";
								//$arrdbparams = array("deliveryDate"=>strtotime(date("d-m-Y", $delivery['deliveryDate'])." midnight"));
								$arrdbparams = array();
								$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
								
								print("<option value='' >Not Required</option>"); //default
								foreach ($resultdata AS $row)
								{
									$strSelected = "";
									if($row['description'].$row['pickdate'] == $delivery['pickList']) $strSelected = " selected";
									print("<option value='".$row['description'].$row['pickdate']."'".$strSelected.">".date("d/m/Y", $row['pickdate'])." - ".$row['description']."</option>");
								}
							print("</select>");
						print("</div>");
	
						//select delivery option
						print("<div class='form-group col-md-6' style='padding-left:15px;'>");
							print("<label for='frm_deliveryOption' class='control-label'>Delivery Option:</label>");
							print("<select name='frm_deliveryOption' id='frm_deliveryOption' class='form-control'>");
								$strdbsql = "SELECT deliveryOptionRates.*, deliveryOptions.description FROM deliveryOptionRates INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID ORDER BY description";
								$strType = "multi";
								$resultdata = query($conn, $strdbsql, $strType);								  
								foreach ($resultdata AS $row)
								{
									$strSelected = "";
									if($row['recordID'] == $delivery['deliveryOptionRateID']) $strSelected = " selected";
									print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']." - ".$row['maxWeight']."</option>");
								}
							print("</select>");
						print("</div>");
					print("</div>");
					
					
					print("<div class='row hide'>");
						print("<div class='form-group col-md-6' style='padding-right:15px;'>");
							
							//select package type
							print("<label for='frm_packageType' class='control-label'>Package Type:</label><select name='frm_packageType' id='frm_packageType' class='form-control' >");
							$strdbsql = "SELECT * FROM deliveryPackageTypes";
							$strType = "multi";
							$resultdata = query($conn, $strdbsql, $strType);
							foreach ($resultdata AS $row)
							{
								$strSelected = "";
								if($row['recordID'] == $delivery['packageTypeID']) $strSelected = " selected";
								print("<option value='".$row['recordID']."'".$strSelected.">".$row['description']." - ".$row['weight']."</option>");
							}
							print("</select>");
						print("</div>");
						
						//select delivery option
						print("<div class='form-group col-md-6' style='padding-left:15px;'>");
							print("<label for='frm_serviceCode' class='control-label'>Service Code:</label>");
							print("<select name='frm_serviceCode' id='frm_serviceCode' class='form-control'>");
								//$strdbsql = "SELECT deliveryOptionRates.*, deliveryOptions.description FROM deliveryOptionRates INNER JOIN deliveryOptions ON deliveryOptionRates.deliveryOptionID = deliveryOptions.recordID";
								//$strType = "multi";
								//$resultdata = query($conn, $strdbsql, $strType);
								$serviceCodes[0]['value'] = "A";
								$serviceCodes[0]['description'] = "Next Day";
								$serviceCodes[1]['value'] = "B";
								$serviceCodes[1]['description'] = "Scotland";
								foreach ($serviceCodes AS $row)
								{
									$strSelected = "";
									if($row['value'] == $delivery['serviceCode']) $strSelected = " selected";
									print("<option value='".$row['value']."'".$strSelected.">".$row['value']." - ".$row['description']."</option>");
								}
							print("</select>");
						print("</div>");
					print("</div>");
					/*print("<div class='row'>");
						print("<div class='form-group col-md-12'>");
							//delivery notes
							print("<label for='frm_deliveryNotes' class='control-label'>Notes:</label><textarea class='form-control' id='frm_deliveryNotes' name='frm_deliveryNotes' rows='5'>".$delivery['notes']."</textarea>");
						print("</div>");
					print("</div>");*/
				print("</div>");
				
				//external order reference
				print("<div class='row hide'>");
					print("<div class='form-group'>");
						print("<label for='frm_externalRef' class='control-label'>External Reference:</label>");
						print("<input type='text' class='form-control' id='frm_externalRef' name='frm_externalRef' value='".$externalOrderID."' />");
					print("</div>");
				print("</div>");
				
				//description
				print("<div class='row hide'>");
					print("<div class='form-group'>");
						print("<label for='frm_giftMessage' class='control-label'>Gift Message</label>");
						print("<textarea $disabled class='form-control' id='frm_giftMessage' name='frm_giftMessage' rows='2' style='min-width:100%;max-width:100%;height:100px;resize:none;'>".$giftMessage."</textarea>");
					print("</div>");
				print("</div>");
				
				//delivery
			/*	print("<div class='row'>");
					print("<div class='form-group col-md-6 even-left'>");
						print("<label for='frm_estdeliverydate' class='control-label'>Estimated Delivery Date</label>");
						print("<input type='text' class='form-control' id='frm_estdeliverydate' name='frm_estdeliverydate' value='".date("d/m/y", $estDeliveryDate)."' />");
					print("</div>");
					
					$strdbsql = "SELECT * FROM deliveryTimes WHERE enabled = 1 AND recordID = :time";
					$times = query($conn,$strdbsql,"single",["time"=>$estDeliveryTime]);
					
					print("<div class='form-group col-md-6 even-right'>");
						print("<label for='frm_estdeliverytime' class='control-label'>Estimated Delivery Time</label>");
						print("<input type='text' class='form-control' id='frm_estdeliverytime' name='frm_estdeliverytime' value='".substr($times['startTime'], 0, 5)." - ".substr($times['endTime'], 0, 5)."' />");
					print("</div>");
					
				print("</div>");*/
				
				//notes
				print("<div class='row'>");
					print("<div class='form-group'>");
						print("<label for='frm_note' class='control-label'>Note</label>");
						print("<textarea $disabled class='form-control' id='frm_note' name='frm_note' rows='2' style='min-width:100%;max-width:100%;height:100px;resize:none;'>".$orderNote."</textarea>");
					print("</div>");
				print("</div>");
			
			print ("</fieldset>");
		print ("</div>");

	}
	function fnGetPayments($orderID, $disabled, $amountOutstanding, $datnow, $conn){
		
		$disabled = ""; //not sure if we should stop payments or not
		
		if (!empty($orderID)){
			
			$strdbsql = "SELECT order_payment_details.*,order_payment_status.description AS statusDesc FROM order_payment_details INNER JOIN order_payment_status ON order_payment_details.paymentStatus = order_payment_status.recordID WHERE orderHeaderID = :recordID";
			$strType = "multi";
			$arrdbparams = array("recordID"=>$orderID);
			$orderPayments = query($conn,$strdbsql,$strType,$arrdbparams);

		}
			
		//payments
		print ("<table class='member-list table table-striped table-bordered table-hover table-condensed' style='border:none; border-top: 1px solid #ddd;'>");
			print ("<thead><tr>");
				print ("<th>Payment Date</th>");
				//print ("<th>Transaction ID</th>");
				print ("<th>Payment Source</th>");
				print ("<th>Payment Amount</th>");
				print ("<th>Status</th>");
				print ("<th></th>");
			print ("</tr></thead><tbody>");
			
			//ajax to get items
			if (count($orderPayments) > 0) {
				foreach($orderPayments AS $payment){
					
					print ("<tr>");
						print ("<td>".fnconvertunixtime($payment['transactionTimestamp'])."</td>");
						//print ("<td>".$payment['transactionID']."</td>");
						print ("<td>".$payment['transactionSource']."</td>");
						print ("<td>&pound;".number_format($payment['transactionAmount'],2)."</td>");
						print ("<td><select $disabled class='form-control' name='paymentStatusAdd' id='paymentStatusAdd' onchange='fnPaymentStatusChanged(this,".$payment['recordID']."); return false;'>");
						$strdbsql = "SELECT * FROM order_payment_status";
						$strType = "multi";
						$resultdata = query($conn, $strdbsql, $strType);								  
						foreach ($resultdata AS $row)
						{
							if ($payment['paymentStatus'] == $row['recordID']) $selected = "selected"; else $selected = "";
							print("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
						}
						print("</select></td>");
						//print ("<td>".$payment['statusDesc']."</td>");
						print ("<td></td>");
					print ("</tr>");
				}
			
			} else {
				print ("<tr><td colspan='5'>No payment lines</td></tr>");
			}
			print ("</tbody>");
		print ("</table>");

		if ($disabled == "disabled") $display = "display: none;"; else $display = "";
		print("<div class='row' style='$display'>");
		
			//payment date
			print("<div class='form-group col-sm-2 even-left'>");
				print("<input $disabled class='form-control' id='paymentDateAdd' name='paymentDateAdd' value='' placeholder='Payment Date' />");
			print("</div>");

			//payment source
			print("<div class='form-group col-sm-2 even-left'>");
				print ("<select $disabled class='form-control' name='paymentSourceAdd' id='paymentSourceAdd' >");
				$strdbsql = "SELECT * FROM order_payment_source";
				$strType = "multi";
				$resultdata = query($conn, $strdbsql, $strType);								  
				foreach ($resultdata AS $row)
				{
					print("<option value='".$row['description']."'>".$row['description']."</option>");
				}
				print("</select>");
			print("</div>");
			
			//payment amount
			print("<div class='form-group col-sm-2 even-left'>");
				if ($amountOutstanding < 0) $amountOutstanding = 0;
				print ("<input $disabled class='form-control' id='paymentAmountAdd' name='paymentAmountAdd' type='text' placeholder='Amount' value='$amountOutstanding' />");
			print("</div>");

			//payment status
			print("<div class='form-group col-sm-2 even-left'>");
				print ("<select $disabled class='form-control' name='paymentStatusAdd' id='paymentStatusAdd' >");
				$strdbsql = "SELECT * FROM order_payment_status";
				$strType = "multi";
				$resultdata = query($conn, $strdbsql, $strType);								  
				foreach ($resultdata AS $row)
				{
					print("<option value='".$row['recordID']."'>".$row['description']."</option>");
				}
				print("</select>");
			print("</div>");
			
		print("</div>");
		print("<div class='row' style='$display'>");
			//button
			print("<div class='form-group col-sm-12 even-right'>");
				print("<button $disabled onclick='return jsAddPayment();' type='submit' class='btn btn-primary right'>Add</button>");
				print("<button $disabled onclick='return jsAddPaymentSense();' type='submit' class='btn btn-primary right' style='margin-right: 10px;'>Add (Payment Sense)</button>");
			print("</div>");
		print("</div>");

	}
	function fnRemoveStockLine($orderItemID, $conn){
		
		if (!empty($orderItemID)) {
			$strdbsql = "DELETE FROM order_items WHERE recordID = :recordID";
			$strType = "delete";
			$arrdbparams = array("recordID"=>$orderItemID);
			$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strdbsql = "DELETE FROM order_items WHERE hamperOrderID = :hamperOrderID";
			$strType = "delete";
			$arrdbparams = array("hamperOrderID"=>$orderItemID);
			$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strdbsql = "DELETE FROM deliveryOrders WHERE orderItemID = :orderItemID";
			$strType = "delete";
			$arrdbparams = array("orderItemID"=>$orderItemID);
			$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
		}
	}
	$scroll = 0;
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['orderID'])) $orderID = $_REQUEST['orderID']; else $orderID = "";
	if (isset($_REQUEST['orderExternalRef'])) $orderExternalRef = $_REQUEST['orderExternalRef']; else $orderExternalRef = "";
	if (isset($_REQUEST['customerID'])) $customerID = $_REQUEST['customerID']; else $customerID = "";
	if (isset($_REQUEST['delCustomerID'])) $delCustomerID = $_REQUEST['delCustomerID']; else $delCustomerID = "";
	if (isset($_REQUEST['paymentID'])) $paymentID = $_REQUEST['paymentID']; else $paymentID = "";
	if (isset($_REQUEST['stockIDEdit'])) $stockIDEdit = $_REQUEST['stockIDEdit']; else $stockIDEdit = "";

	switch($strcmd){

		case 'duplicateOrder':
		case 'addPaymentSense':
		case 'sendEmailConfirmation':
		case 'updatePayment':
		case 'addPayment':
		case 'deleteStockItem':
		case 'editStockItem':
		case 'addStockItem':
		case 'saveOrder':
		
			if ($strcmd == "duplicateOrder") {
				$oldOrderID = $orderID;
				$orderID = "";
			} else {
				$oldOrderID = "";
			}
			
			$orderDate = time();
			if (!empty($_REQUEST['frm_orderDate'])){
				$orderDate = strtotime(str_replace("/","-",$_REQUEST['frm_orderDate']));
			}
			if($orderDate == 0){
				$orderDate = time();
			}

			if($strcmd == "duplicateOrder"){
				$orderDate = time();
				$orderSource = $_REQUEST['frm_orderSource'];
				$orderStatus = 1;
				$externalOrderRef = "";
				$dispatchAdd = 1; //not used anymore so default to 1
				$dispatchAddDate = time(); //not used anymore
			} else {
				$orderSource = $_REQUEST['frm_orderSource'];
				$orderStatus = (!empty($_REQUEST['frm_orderStatus']) ? $_REQUEST['frm_orderStatus'] : 1);
				$externalOrderRef = (!empty($_REQUEST['frm_externalRef'])) ? $_REQUEST['frm_externalRef'] : "";
				$dispatchAdd = 1; //not used anymore so default to 1
				$dispatchAddDate = strtotime(str_replace("/", "-", $_REQUEST['frm_deliveryDate'])); //not used anymore
			}
			$giftMessage = $_REQUEST['frm_giftMessage'];
			$giftee = (isset($_REQUEST['giftee']) ? $_REQUEST['giftee'] : 1);

			$strtitle = $_REQUEST['title'];
			$strfirstname = $_REQUEST['firstname'];
			$strsurname = $_REQUEST['surname'];
			$strcompany = ""; //$_REQUEST['company'];
			$strphone = $_REQUEST['phone'];
			$strmobile = $_REQUEST['mobile'];
			$stremail = $_REQUEST['email'];
			$strfax = ""; //$_REQUEST['fax'];
			
			$strdeltitle = $_REQUEST['deltitle'];
			$strdelfirstname = $_REQUEST['delfirstname'];
			$strdelsurname = $_REQUEST['delsurname'];
			$strdeladd1 = $_REQUEST['deladd1'];
			$strdeladd2 = $_REQUEST['deladd2'];
			$strdeladd3 = $_REQUEST['deladd3'];
			$strdelTown = $_REQUEST['delTown'];
			$strdelCnty = $_REQUEST['delCnty'];
			$strdelCntry = $_REQUEST['delCntry'];
			$strdelPstCde = $_REQUEST['delPstCde'];

			$strbilltitle = $_REQUEST['biltitle'];
			$strbillfirstname = $_REQUEST['bilfirstname'];
			$strbillsurname = $_REQUEST['bilsurname'];
			$strbilladd1 = $_REQUEST['biladd1'];
			$strbilladd2 = $_REQUEST['biladd2'];
			$strbilladd3 = $_REQUEST['biladd3'];
			$strbillTown = $_REQUEST['bilTown'];
			$strbillCnty = $_REQUEST['bilCnty'];
			$strbillCntry = $_REQUEST['bilCntry'];
			$strbillPstCde = $_REQUEST['bilPstCde'];
			
			//check the delivery postcode and report any problems / modifiers / customs forms etc
			$deliveryLocationDetails = deliveryLocationCheck($strdelPstCde, $strdelCntry, $conn);
			if (!empty($deliveryLocationDetails['strerror'])) $strerror .= $deliveryLocationDetails['strerror'];
			if (!empty($deliveryLocationDetails['strwarning'])) $strwarning .= $deliveryLocationDetails['strwarning'];
			if (!empty($deliveryLocationDetails['strsuccess'])) $strsuccess .= $deliveryLocationDetails['strsuccess'];
			
			
			//check to see if we already have a order id - insert if we dont, update if we do
			if (empty($orderID) OR $orderID == 0) {

				$strdbsql = "INSERT INTO order_header (customerID, timestampOrder, addDeliveryCustomerID, addDeliveryTitle, addDeliveryFirstname, addDeliverySurname, addDelivery1, addDelivery2, addDelivery3, addDelivery4, addDelivery5, addDelivery6, addDeliveryPostcode, addDeliveryMessage, addBillingTitle, addBillingFirstname, addBillingSurname, addBilling1, addBilling2, addBilling3, addBilling4, addBilling5, addBilling6, addBillingPostcode, telephone, email, notes, orderStatus, orderSource, giftMessage, gift, dispatch, dispatchDate,externalOrderID) VALUES (:customerID, :timestampOrder, :addDeliveryCustomerID, :addDeliveryTitle, :addDeliveryFirstname, :addDeliverySurname, :addDelivery1, :addDelivery2, :addDelivery3, :addDelivery4, :addDelivery5, :addDelivery6, :addDeliveryPostcode, :addDeliveryMessage, :addBillingTitle, :addBillingFirstname, :addBillingSurname, :addBilling1, :addBilling2, :addBilling3, :addBilling4, :addBilling5, :addBilling6, :addBillingPostcode, :telephone, :email, :notes, :orderStatus, :orderSource, :giftMessage, :gift, :dispatch, :dispatchDate, :externalOrderRef);";
				$strType = "insert";
				$arrdbparams = array(
					"customerID"=>$customerID, 
					"timestampOrder"=>$orderDate, 
					"addDeliveryCustomerID"=>$delCustomerID, 
					"addDeliveryTitle"=>$strdeltitle, 
					"addDeliveryFirstname"=>$strdelfirstname, 
					"addDeliverySurname"=>$strdelsurname, 
					"addDelivery1"=>$strdeladd1, 
					"addDelivery2"=>$strdeladd2, 
					"addDelivery3"=>$strdeladd3, 
					"addDelivery4"=>$strdelTown, 
					"addDelivery5"=>$strdelCnty, 
					"addDelivery6"=>$strdelCntry, 
					"addDeliveryPostcode"=>$strdelPstCde, 
					"addDeliveryMessage"=>"", 
					"addBillingTitle"=>$strbilltitle, 
					"addBillingFirstname"=>$strbillfirstname, 
					"addBillingSurname"=>$strbillsurname, 
					"addBilling1"=>$strbilladd1, 
					"addBilling2"=>$strbilladd2, 
					"addBilling3"=>$strbilladd3, 
					"addBilling4"=>$strbillTown, 
					"addBilling5"=>$strbillCnty, 
					"addBilling6"=>$strbillCntry, 
					"addBillingPostcode"=>$strbillPstCde, 
					"telephone"=>$strphone, 
					"email"=>$stremail, 
					"notes"=>"", 
					"orderStatus"=>$orderStatus, 
					"orderSource"=>$orderSource,
					"giftMessage"=>$giftMessage,
					"dispatch"=>$dispatchAdd, 
					"dispatchDate"=>$dispatchAddDate,
					"externalOrderRef"=>$externalOrderRef,
					"gift"=>$giftee
				);
				$orderID = query($conn,$strdbsql,$strType,$arrdbparams);
				if ($orderID > 0){
					if ($strcmd == "duplicateOrder") {
						$strsuccess .= "Successfully duplicated order.<br/>";
					} else {
						$strsuccess .= "Successfully updated order.<br/>";
					}
				} else {
					$orderID = "";
					$strerror .= "Error could not create order, please try again.<br/>";
				}
				
			} else {
			
				$strdbsql = "UPDATE order_header SET customerID = :customerID, addDeliveryCustomerID = :addDeliveryCustomerID, timestampOrder = :timestampOrder, addDeliveryTitle = :addDeliveryTitle, addDeliveryFirstname = :addDeliveryFirstname, addDeliverySurname = :addDeliverySurname, addDelivery1 = :addDelivery1, addDelivery2 = :addDelivery2, addDelivery3 = :addDelivery3, addDelivery4 = :addDelivery4, addDelivery5 = :addDelivery5, addDelivery6 = :addDelivery6, addDeliveryPostcode = :addDeliveryPostcode, addDeliveryMessage = :addDeliveryMessage, addBillingTitle = :addBillingTitle, addBillingFirstname = :addBillingFirstname, addBillingSurname = :addBillingSurname, addBilling1 = :addBilling1, addBilling2 = :addBilling2, addBilling3 = :addBilling3, addBilling4 = :addBilling4, addBilling5 = :addBilling5, addBilling6 = :addBilling6, addBillingPostcode = :addBillingPostcode, telephone = :telephone, email = :email, notes = :notes, orderStatus = :orderStatus, orderSource = :orderSource, giftMessage = :giftMessage, gift = :gift, dispatch = :dispatch, dispatchDate = :dispatchDate, externalOrderID = :externalOrderRef  WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparams = array(
					"recordID"=>$orderID, 
					"customerID"=>$customerID, 
					"timestampOrder"=>$orderDate, 
					"addDeliveryCustomerID"=>$delCustomerID, 
					"addDeliveryTitle"=>$strdeltitle, 
					"addDeliveryFirstname"=>$strdelfirstname, 
					"addDeliverySurname"=>$strdelsurname, 
					"addDelivery1"=>$strdeladd1, 
					"addDelivery2"=>$strdeladd2, 
					"addDelivery3"=>$strdeladd3, 
					"addDelivery4"=>$strdelTown, 
					"addDelivery5"=>$strdelCnty, 
					"addDelivery6"=>$strdelCntry, 
					"addDeliveryPostcode"=>$strdelPstCde, 
					"addDeliveryMessage"=>"", 
					"addBillingTitle"=>$strbiltitle, 
					"addBillingFirstname"=>$strbilfirstname, 
					"addBillingSurname"=>$strbillsurname, 
					"addBilling1"=>$strbilladd1, 
					"addBilling2"=>$strbilladd2, 
					"addBilling3"=>$strbilladd3, 
					"addBilling4"=>$strbillTown, 
					"addBilling5"=>$strbillCnty, 
					"addBilling6"=>$strbillCntry, 
					"addBillingPostcode"=>$strbillPstCde, 
					"telephone"=>$strphone, 
					"email"=>$stremail, 
					"notes"=>"", 
					"orderStatus"=>$orderStatus, 
					"orderSource"=>$orderSource,
					"giftMessage"=>$giftMessage,
					"dispatch"=>$dispatchAdd, 
					"dispatchDate"=>$dispatchAddDate,
					"externalOrderRef"=>$externalOrderRef,
					"gift"=>$giftee
				);
				$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
			}
			
			//if duplicate order then also duplicate the order lines
			if($strcmd == "duplicateOrder" AND $orderID > 0 AND $oldOrderID > 0){
				
				//get order item details
				$strdbsql = "SELECT * FROM order_items WHERE orderHeaderID = :orderID";
				$strType = "multi";
				$arrParam = array("orderID"=>$oldOrderID);
				$orderItemDetails = query($conn, $strdbsql, $strType, $arrParam);
				
				//insert each order item into the delivery table
				foreach($orderItemDetails AS $orderItem)
				{
					//insert record into details table
					$strdbsql = "INSERT INTO order_items(orderHeaderID, stockID, stockDetailsArray, quantity, price, discountPerc, vatID, display, hamperOrderID) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price, :discountPerc, :vatID, 1, :hamperOrderID);";
					$strType = "insert";
					$arrdbparams = array(
						"orderHeaderID"=>$orderID, 
						"stockID"=>$orderItem['stockID'], 
						"stockDetailsArray"=>$orderItem['stockDetailsArray'], 
						"quantity"=>$orderItem['quantity'], 
						"price"=>$orderItem['price'], 
						"discountPerc"=>$orderItem['discountPerc'], 
						"vatID"=>$orderItem['vatID'], 
						"hamperOrderID"=>$orderItem['hamperOrderID']
					);
					$orderInsert = query($conn,$strdbsql,$strType,$arrdbparams);
				}
			}
			
			//if we have a new line to add
			if($strcmd == "addStockItem" AND $orderID > 0){
				
				$stockID = $_REQUEST['stockIDAdd'];
				$strdbsql = "SELECT stock_group_information.* FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock.recordID = :stockID";
				$strType = "single";
				$arrdbparams = array("stockID"=>$stockID);
				$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
				
				if (!empty($resultdata['recordID'])) {
					
					$stockDetailsArray = array();
					//check to see if this is a hamper product				
					if ($resultdata['productType'] == 1) {
						$stockDetailsArray['stockCode'] = $_REQUEST['stockCodeHidden'];
						$stockDetailsArray['name'] = $_REQUEST['nameAdd']." (Hamper)";
						$stockDetailsArray['description'] = $_REQUEST['descriptionAdd'];
					} else {
						$stockDetailsArray['stockCode'] = $_REQUEST['stockCodeHidden'];
						$stockDetailsArray['name'] = $_REQUEST['nameAdd'];
						$stockDetailsArray['description'] = $_REQUEST['descriptionAdd'];
					}

					$quantityAdd = $_REQUEST['quantityAdd'];
					$discountPerc = $_REQUEST['discountAdd'];
					$priceAdd = $_REQUEST['priceAdd'] - (($_REQUEST['priceAdd'] / 100) * $discountPerc);
					$vatSelectAdd = $_REQUEST['vatSelectAdd'];
					
					//insert record into details table
					$strdbsql = "INSERT INTO order_items(orderHeaderID, stockID, stockDetailsArray, quantity, price, discountPerc, vatID, display, hamperOrderID) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price, :discountPerc, :vatID, 1, :hamperOrderID);";
					$strType = "insert";
					$arrdbparams = array(
						"orderHeaderID"=>$orderID, 
						"stockID"=>$stockID, 
						"stockDetailsArray"=>serialize($stockDetailsArray), 
						"quantity"=>$quantityAdd, 
						"price"=>$priceAdd, 
						"discountPerc"=>$discountPerc, 
						"vatID"=>$vatSelectAdd,
						"hamperOrderID"=>0
					);
					$orderInsert = query($conn,$strdbsql,$strType,$arrdbparams);
					
					
					//if hamper then insert the individual products into our order details table
					if ($resultdata['productType'] == 1) {
					
						//update original hamper detail with its own ID
						$strdbsql = "UPDATE order_items SET hamperOrderID = :hamperOrderID WHERE recordID = :recordID";
						$strType = "update";
						$arrdbparams = array(
							"recordID"=>$orderInsert,
							"hamperOrderID"=>$orderInsert
						);
						$orderDetailUpdate = query($conn,$strdbsql,$strType,$arrdbparams);
					
						$strdbsql = "SELECT count(*) AS qtyToAdd, stock_group_information.name, stock_group_information.description, stock.*, stock_status.description AS statusDesc FROM stock_hamper INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_hamper.stockGroupID = :hamperID GROUP BY stockCode";
						$arrType = "multi";
						$strdbparams = array("hamperID"=>$resultdata['recordID']);
						$resultdata2 = query($conn, $strdbsql, $arrType, $strdbparams);
					
						foreach($resultdata2 AS $row2){
						
							$stockDetailsArray = array();
							$stockDetailsArray['stockCode'] = $row2['stockCode'];
							$stockDetailsArray['name'] = $row2['name'];
							$stockDetailsArray['description'] = $row2['description'];
							$vatSelectAdd = $_REQUEST['vatSelectAdd'];
							
							//insert record into details table
							$strdbsql = "INSERT INTO order_items(orderHeaderID, stockID, stockDetailsArray, quantity, price, discountPerc, vatID, display, hamperOrderID) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price, :discountPerc, :vatID, 0, :hamperOrderID);";
							$strType = "insert";
							$arrdbparams = array(
								"orderHeaderID"=>$orderID, 
								"stockID"=>$row2['recordID'], 
								"stockDetailsArray"=>serialize($stockDetailsArray), 
								"quantity"=>$row2['qtyToAdd'] * $_REQUEST['quantityAdd'], 
								"price"=>0, 
								"discountPerc"=>0, 
								"vatID"=>$vatSelectAdd,
								"hamperOrderID"=>$orderInsert
							);
							$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
						}
					}
					
					//check to see if we have a delivery setup for this order
					$strdbsql = "SELECT * FROM delivery WHERE orderHeaderID = :orderHeaderID";
					$strType = "single";
					$arrdbparams = array("orderHeaderID"=>$orderID);
					$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
					
					if (!empty($resultdata['recordID'])){
						
						//delete order items from delivery table as we are going to re-create
						$strdbsql = "DELETE FROM deliveryOrders WHERE deliveryItemID = :deliveryItemID";
						$strType = "delete";
						$arrParams = array("deliveryItemID"=>$resultdata['recordID']);
						$deleteResult = query($conn, $strdbsql, $strType,$arrParams);
						
						//get order item details
						$strdbsql = "SELECT * FROM order_items WHERE orderHeaderID = :orderID";
						$strType = "multi";
						$arrParam = array("orderID"=>$orderID);
						$orderItemDetails = query($conn, $strdbsql, $strType, $arrParam);
						
						//insert each order item into the delivery table
						foreach($orderItemDetails AS $orderItem)
						{
							$strdbsql = "INSERT INTO deliveryOrders (deliveryItemID, orderItemID) VALUES (:deliveryItemID,:orderItemID)";
							$strType = "insert";
							$arrParams = array("deliveryItemID"=>$resultdata['recordID'],"orderItemID"=>$orderItem['recordID']);
							$insertResult = query($conn, $strdbsql, $strType,$arrParams);
						}
						
						$strwarning .= "This order had been assigned to a delivery, the delivery has been updated but picklists may need to be re-printed.<br/>";
					}

					$strsuccess .= "Successfully added a new stock line.<br/>";
				} else {
					$strerror .= "Could not find product, please try again.<br/>";
				}
			}

			if($strcmd == "deleteStockItem"){
				$orderItemID = $_REQUEST['stockIDDelete'];
				fnRemoveStockLine($orderItemID, $conn);
			}
			
			if ($strcmd == "addPayment" AND $orderID > 0){
				
				$paymentDateAdd = strtotime(str_replace("/","-",$_REQUEST['paymentDateAdd']));
				$paymentSourceAdd = $_REQUEST['paymentSourceAdd'];
				$paymentAmountAdd = $_REQUEST['paymentAmountAdd'];
				$paymentStatusAdd = $_REQUEST['paymentStatusAdd'];

				//insert record into details table
				$strdbsql = "INSERT INTO order_payment_details(orderHeaderID, transactionSource, transactionTimestamp, transactionAmount, paymentStatus) VALUES (:orderHeaderID, :transactionSource, :transactionTimestamp, :transactionAmount, :paymentStatus);";
				$strType = "insert";
				$arrdbparams = array(
					"orderHeaderID"=>$orderID, 
					"transactionSource"=>$paymentSourceAdd, 
					"transactionTimestamp"=>$paymentDateAdd, 
					"transactionAmount"=>number_format($paymentAmountAdd,2), 
					"paymentStatus"=>$paymentStatusAdd
				);
				$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
				$strsuccess .= "Successfully added a new payment.<br/>";
			}
			
			if ($strcmd == "updatePayment" AND $paymentID > 0){
				
				$paymentStatusUpdate = $_REQUEST['paymentStatusUpdate'];

				//insert record into details table
				$strdbsql = "UPDATE order_payment_details SET paymentStatus = :paymentStatus WHERE recordID = :paymentID";
				$strType = "update";
				$arrdbparams = array(
					"paymentID"=>$paymentID, 
					"paymentStatus"=>$paymentStatusUpdate
				);
				$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
				$strsuccess .= "Successfully updated payment.<br/>";
			}
			
			if ($strcmd == "addPaymentSense" AND $orderID > 0){
				$paymentAmountAdd = $_REQUEST['paymentAmountAdd'];
				header("Location: /Process.php?orderHeaderID=$orderID&amount=$paymentAmountAdd");
			}
			
			if ($strcmd == "sendEmailConfirmation") {
				
				if (!empty($stremail) AND !empty($companyEmail)){
					$strsuccess = "Email confirmation sent to customer.";
					//fnSendOrderConfirmationEmail($orderID, $paymentID, $stremail, $companyEmail, $companyPhone, $companyName, $strsiteurl, $strlogo, $supportemail, $conn);
					$dbsql = "SELECT basketHeaderID FROM order_header WHERE recordID = :order";
					$basket = query($conn,$dbsql, "single",["order"=>$orderID]);
					
					confirmationemail($conn, $basket['basketHeaderID']);
				} else {
					$strerror = "No email address has been entered.";

				}
				//$strerror = "Emails currently disabled due to blacklist issue.";
			}
			
			
			//update / add delivery
			if($orderID > 0){
				
				//check date is valid
				$intDeliveryDate = strtotime(str_replace("/", "-", $_REQUEST['frm_deliveryDate']));
				$weekday = date('N', $intDeliveryDate);
				$allowedWeekdays = array("1","2","3","4");
				if (!in_array($weekday,$allowedWeekdays)) {
					while(!in_array($weekday,$allowedWeekdays)){
						$intDeliveryDate = strtotime('+1 day', $intDeliveryDate);
						$weekday = date('N', $intDeliveryDate);
					}
					$strwarning = "Selected delivery date is not valid, it has been set to the first valid delivery day.";
				}
				
				//set initial parameters
				$arrParams = array("deliveryDate" => $intDeliveryDate, "deliveryOptionRateID" => $_REQUEST['frm_deliveryOption'], "packageTypeID" => $_REQUEST['frm_packageType'], "notes" =>"", "serviceCode"=> $_REQUEST['frm_serviceCode'], "pickList"=>$_REQUEST['frm_pickList']);
				
				//get header details of submitted order
				$strdbsql = "SELECT * FROM order_header WHERE recordID = :orderID";
				$strType = "single";
				$arrParam = array("orderID"=>$orderID);
				$orderHeaderDetails = query($conn, $strdbsql, $strType, $arrParam);
			
				if (!empty($orderHeaderDetails['recordID'])) {
										
					//get order item details
					$strdbsql = "SELECT * FROM order_items WHERE orderHeaderID = :orderID";
					$strType = "multi";
					$arrParam = array("orderID"=>$orderHeaderDetails['recordID']);
					$orderItemDetails = query($conn, $strdbsql, $strType, $arrParam);
					
					//set customer from order header
					$arrParams['customerID'] = $orderHeaderDetails['customerID'];
					$arrParams['orderHeaderID'] = $orderHeaderDetails['recordID'];
					
					//check if we already have a deliveryID (editing existing)
					$strdbsql = "SELECT * FROM delivery WHERE orderHeaderID = :orderID";
					$strType = "single";
					$arrParam = array("orderID"=>$orderID);
					$deliveryDetails = query($conn, $strdbsql, $strType, $arrParam);
					
					
					if(!empty($deliveryDetails['recordID']))
					{
						$deliveryID = $deliveryDetails['recordID'];
						
						//update
						$strdbsql = "UPDATE delivery SET orderHeaderID=:orderHeaderID, deliveryDate = :deliveryDate, deliveryOptionRateID = :deliveryOptionRateID, customerID = :customerID, packageTypeID = :packageTypeID, notes = :notes, serviceCode = :serviceCode, pickList = :pickList WHERE recordID = :deliveryID";
						$strType = "update";
						$arrParams['deliveryID'] = $deliveryID;
						$updateDelivery = query($conn, $strdbsql, $strType, $arrParams);
					}
					else
					{
						//insert
						$strdbsql = "INSERT INTO delivery (deliveryDate, deliveryOptionRateID, customerID, packageTypeID, notes, orderHeaderID, serviceCode, pickList) VALUES (:deliveryDate, :deliveryOptionRateID, :customerID, :packageTypeID, :notes, :orderHeaderID, :serviceCode, :pickList)";
						$strType = "insert";
						$deliveryID = query($conn, $strdbsql, $strType, $arrParams);
					}
					
					if($deliveryID != "")
					{
						//delete order items from delivery table as we are going to re-create
						$strdbsql = "DELETE FROM deliveryOrders WHERE deliveryItemID = :deliveryItemID";
						$strType = "delete";
						$arrParams = array("deliveryItemID" => $deliveryID);
						$deleteResult = query($conn, $strdbsql, $strType,$arrParams);
						
						//insert each order item into the delivery table
						foreach($orderItemDetails AS $orderItem)
						{
							$strdbsql = "INSERT INTO deliveryOrders (deliveryItemID, orderItemID) VALUES (:deliveryItemID,:orderItemID)";
							$strType = "insert";
							$arrParams = array("deliveryItemID"=>$deliveryID,"orderItemID"=>$orderItem['recordID']);
							$deleteResult = query($conn, $strdbsql, $strType,$arrParams);
						}
						
						//check weights
						$strdbsql = "SELECT maxWeight FROM deliveryOptionRates WHERE recordID = :deliveryOptionRateID";
						$strType = "single";
						$arrParam = array("deliveryOptionRateID" => $_REQUEST['frm_deliveryOption']);
						$maxWeightArr = query($conn, $strdbsql, $strType, $arrParam);
						$maxWeight = $maxWeightArr['maxWeight'];
						
						$totalOrderWeight = 0.00;
						
						//get package type weight
						$strdbsql = "SELECT weight FROM deliveryPackageTypes WHERE recordID = :packageTypeID";
						$strType = "single";
						$arrParam = array("packageTypeID" => $_REQUEST['frm_packageType']);
						$packageWeightArr = query($conn, $strdbsql, $strType, $arrParam);
						
						$totalOrderWeight += $packageWeightArr['weight'];
						
						$itemsNotAdded = "";
						$itemsAdded = "";
						
						//stock items weight + package weight < delivery option rate max weight
						foreach($orderItemDetails AS $orderItem)
						{
							$strdbsql = "SELECT (stock.weight * order_items.quantity) AS orderItemWeight, stock_group_information.name FROM stock INNER JOIN order_items ON stock.recordID = order_items.stockID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE order_items.recordID = :recordID";
							$strType = "single";
							$arrParam = array("recordID"=>$orderItem['recordID']);
							$stockWeightArr = query($conn, $strdbsql, $strType, $arrParam);
							$orderItemWeight = $stockWeightArr['orderItemWeight'];
							$totalOrderWeight += $orderItemWeight;
							
							if(($totalOrderWeight > $maxWeight) && $maxWeight > 0.0000)
							{
								//$strwarning = "Total order weight is greater than allowed, pleased review the selected delivery method.<br/><br/>Total Order Weight: $totalOrderWeight<br/>Maximum Delivery Weight: $maxWeight";
							}
						}
					
						$strdbsql = "UPDATE delivery SET weight = :weight WHERE recordID = :recordID";
						$strType = "update";
						$arrParam = array("weight" => $totalOrderWeight, "recordID" => $deliveryID);
						query($conn, $strdbsql, $strType, $arrParam);
						
						/*if ($strwarning == "" AND $strerror == "") {
							$strsuccess = "Order successfully updated for delivery.";
							$strdbsql = "UPDATE order_header SET orderStatus = 4 WHERE recordID = :orderID";
							$strType = "single";
							$arrParam = array("orderID"=>$orderHeaderDetails['recordID']);
							$resultdataUpdate = query($conn, $strdbsql, $strType, $arrParam);
						}*/
						
					} else {
						$strerror = "Problem creating delivery, please try again.";
					}
				}
			}
			
			//if delivery address is set as giftee then check to see if the giftee is in the system and add if not
			$strdbsql = "SELECT customer.recordID, LOWER(REPLACE(customer.surname, ' ', '')) AS trimsurname, LOWER(REPLACE(customer_address.add1, ' ', '')) AS trimadd1, LOWER(REPLACE(customer_address.postcode, ' ', '')) AS trimpostcode FROM customer INNER JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE LOWER(REPLACE(customer.surname, ' ', '')) = :surname AND LOWER(REPLACE(customer_address.add1, ' ', '')) = :add1 AND LOWER(REPLACE(customer_address.postcode, ' ', '')) = :postcode";
			$strType = "single";
			$arrParam = array("surname"=>strtolower(str_replace(" ","",$strdelsurname)),"add1"=>strtolower(str_replace(" ","",$strdeladd1)),"postcode"=>strtolower(str_replace(" ","",$strdelPstCde)));
			$customerLookup = query($conn, $strdbsql, $strType, $arrParam);
			
			if ($giftee == 1 AND empty($customerLookup['recordID']))
			{
				//create account
				$createAccountQuery = "INSERT INTO customer (title, firstname, surname, company, telephone, mobile, email, fax, notes, groupID) VALUES (:title, :firstname, :surname, :company, :telephone, :mobile, :email, :fax, :notes, :groupID)";
				$strType = "insert";
				$arrdbparams = array(
					"title" => $strdeltitle,
					"firstname" => $strdelfirstname,
					"surname" => $strdelsurname,
					"company" => "",
					"telephone" => "",
					"mobile" => "",
					"email" => "",
					"fax" => "",
					"notes" => "",
					"groupID" => 5 //giftee group
				);
				$customerAccountID = query($conn, $createAccountQuery, $strType, $arrdbparams);
				
				if($customerAccountID)
				{
					$insertAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Delivery Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
					$strType = "insert";
					$arrdbparams = array(
						"customerID" => $customerAccountID,
						"title" => $strdeltitle,
						"firstname" => $strdelfirstname,
						"surname" => $strdelsurname,
						"add1" => $strdeladd1,
						"add2" => $strdeladd2,
						"add3" => $strdeladd3,
						"town" => $strdelTown,
						"county" => $strdelCnty,
						"postcode" => $strdelPstCde
					);
					$delAddID = query($conn, $insertAddressQuery, $strType, $arrdbparams);
					$billAddID = $delAddID;

					$values = array(
						"billingadd" => $billAddID,
						"deliveryadd" => $delAddID,
						"custid" => $customerAccountID
					);
					$assignCustomerAddresses = updateUserBillDelAddresses($values, "billanddel", $conn);
				}
			} else if($giftee == 1) {
				$customerAccountID = $customerLookup['recordID'];
			}
			
			
			//if giftee and the customerID is not already set then update order with giftee customer ID for the delivery customerID
			if (!empty($orderID) AND !empty($customerAccountID) AND $giftee == 1) {
				$strdbsql = "UPDATE order_header SET addDeliveryCustomerID = :addDeliveryCustomerID WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparams = array(
					"recordID"=>$orderID, 
					"addDeliveryCustomerID"=>$customerAccountID
				);
				$updateQuery = query($conn, $strdbsql, $strType, $arrdbparams);
			}

			$strcmd = "";
			$scroll = 1;
			
		break;
		case "deleteOrder":

			if ($orderID > 0){
				
				//create a record
				$strdbsql = "SELECT * FROM order_payment_details WHERE orderHeaderID = :recordID";
				$strType = "multi";
				$arrdbparams = array("recordID"=>$orderID);
				$paymentDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			
				$strdbsql = "SELECT * FROM order_items WHERE orderHeaderID = :recordID";
				$strType = "multi";
				$arrdbparams = array("recordID"=>$orderID);
				$items = query($conn,$strdbsql,$strType,$arrdbparams);
			
				$strdbsql = "SELECT * FROM order_header WHERE recordID = :recordID";
				$strType = "single";
				$arrdbparams = array("recordID"=>$orderID);
				$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
				
				$strdbsql = "INSERT INTO deletedOrders (orderHeaderID, headerDetails, orderItemDetails, orderPaymentDetails, operatorID) VALUES (:orderHeaderID, :headerDetails, :orderItemDetails, :orderPaymentDetails, :operatorID)";
				$strType = "insert";
				$arrdbparams = array("orderHeaderID"=>$orderID,"headerDetails"=>serialize($orderHeader),"orderItemDetails"=>serialize($items),"orderPaymentDetails"=>serialize($paymentDetails),"operatorID"=>$_SESSION['operatorID']);
				$deleteInsert = query($conn,$strdbsql,$strType,$arrdbparams);
				
				
				
			
				$strdbsql = "SELECT * FROM order_payment_details INNER JOIN order_payment_status ON order_payment_details.paymentStatus = order_payment_status.recordID WHERE orderHeaderID = :recordID AND countTowardsTotal = 1";
				$strType = "multi";
				$arrdbparams = array("recordID"=>$orderID);
				$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
			
			//	if (count($resultdata) == 0){
				
					$strdbsql = "DELETE FROM order_payment_details WHERE orderHeaderID = :recordID";
					$strType = "delete";
					$arrdbparams = array("recordID"=>$orderID);
					$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
				
					$strdbsql = "DELETE FROM order_items WHERE orderHeaderID = :recordID";
					$strType = "delete";
					$arrdbparams = array("recordID"=>$orderID);
					$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
				
					$strdbsql = "DELETE FROM order_header WHERE recordID = :recordID";
					$strType = "delete";
					$arrdbparams = array("recordID"=>$orderID);
					$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
					
					$strsuccess .= "Successfully deleted order.<br/>";
					$_REQUEST = array();
					$orderID = "";
					$customerID = "";
			//	} else {
			//		$strerror .= "Cannot delete an order with payments.<br/>";
			//	}
			} else {
				$strerror .= "Order No. not found, please try again.<br/>";
			}
			
			$strcmd = "";
		break;
	}
	
	
	

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Sales Order Processing</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

			?>	
			<script type='text/javascript'>	
				
				function jsDuplicateOrder(){
					document.form.cmd.value='duplicateOrder';
					document.form.action='sales-order-create.php';
					document.form.submit();
				}
				
				function jsAddStockItem(){
					document.form.cmd.value='addStockItem';
					document.form.action='sales-order-create.php#stockItemsSection';
					document.form.submit();
				}
				function jsEditStockItem(recordID){
					document.form.cmd.value='editStockItem';
					document.form.stockIDEdit.value=recordID;
					document.form.stockIDDelete.value=recordID;
					document.form.action='sales-order-create.php#stockItemsSection';
					document.form.submit();
				}
				function jsDeleteStockItem(recordID){
					document.form.cmd.value='deleteStockItem';
					document.form.action='sales-order-create.php#stockItemsSection';
					document.form.stockIDDelete.value=recordID;
					document.form.submit();
				}
				function jsSaveOrder(){
					document.form.cmd.value='saveOrder';
					document.form.submit();
				}
				function jsCreateInvoice(){
					document.form.cmd.value='createInvoice';
					document.form.submit();
				}
				function jsDelete(){
					if(confirm("Are you sure you want to delete this order?"))
					{
						document.form.cmd.value='deleteOrder';
						document.form.submit();
					}
				}
				function jsAddPayment(){
					document.form.cmd.value='addPayment';
					document.form.submit();
				}
				function jsAddPaymentSense(){
					document.form.cmd.value='addPaymentSense';
					document.form.submit();
				}
				function fnPaymentStatusChanged(selection,paymentID){
					document.form.cmd.value='updatePayment';
					document.form.paymentID.value=paymentID;
					document.form.paymentStatusUpdate.value=selection.value;
					document.form.submit();
				}
				function jsEmailConfirmation(){
					document.form.cmd.value='sendEmailConfirmation';
					document.form.submit();
				}
				
				function jsClearAddress(addressType){
					
					switch(addressType){
						case "delivery":
							document.getElementById("deltitle").value = "";
							document.getElementById("delfirstname").value = "";
							document.getElementById("delsurname").value = "";
							document.getElementById("delPstCde").value = "";
							document.getElementById("deladd1").value = "";
							document.getElementById("deladd2").value = "";
							document.getElementById("deladd3").value = "";
							document.getElementById("delTown").value = "";
							document.getElementById("delCnty").value = "";
						break;
						case "billing":		
							document.getElementById("biltitle").value = "";
							document.getElementById("bilfirstname").value = "";
							document.getElementById("bilsurname").value = "";
							document.getElementById("bilPstCde").value = "";
							document.getElementById("biladd1").value = "";
							document.getElementById("biladd2").value = "";
							document.getElementById("biladd3").value = "";
							document.getElementById("bilTown").value = "";
							document.getElementById("bilCnty").value = "";
						break;
					}
				}
				
				function jsprintinvoice (orderHeaderID) {
					window.open("/admin/invoiceprint.php?print=pdf&orderHeaderID="+orderHeaderID); 
				}	
				function jsprintgiftmessage (orderHeaderID) {
					window.open("/invoiceprint.php?print=pdf&documentType=giftMessage&orderHeaderID="+orderHeaderID); 
				}	
				function jsPostcodeLookup(postcodeType) {
					switch (postcodeType){
						case "billing":
							postcode = document.getElementById("bilPstCde").value;
							returnLocation = "ajax_addressBill";
						break;
						case "delivery":
							postcode = document.getElementById("delPstCde").value;
							returnLocation = "ajax_addressDel";
						break;
					}
					jsfindaddress(postcode,returnLocation);
				}
				
				function fnSOPAjax(callFunction,recordID,updateDiv){
					
					//post the data to our ajax page
					jQuery.ajax({
						type: "POST",
						url: "includes/ajax_sop.php",
						data: {callFunction: callFunction, recordID: recordID},
						async: true
					}).done(function( data ) {
						$("#"+updateDiv).html(data);
					});
				}
				function jsloadSuggestions(inputID,searchType,outputID){
					//delay(function(){
						inputtext = document.getElementById(inputID).value;
						if (inputtext.length > 3) {
							jQuery.ajax({
								type: "POST",
								url: "includes/ajax_search.php",
								data: {q: inputtext, searchType: searchType},
								async: true
							}).done(function( data ) {
								$("#"+outputID).html(data);
							});
						}
					//},1000);
				}
				function setitem(listitem,searchType,outputID){
					
					switch(searchType){
						case "customer":
							$('#customerSearchResults').empty();
							jQuery.ajax({
								type: "POST",
								url: "includes/ajax_search.php",
								data: {q: listitem, searchType: "customerReturn"},
								async: true,
								dataType: "json" // Set the data type so jQuery can parse it for you
							}).done(function( data ) {
								if(data['recordID']) document.getElementById("customerID").value = data['recordID'];
								if(data['title']) document.getElementById("title").value = data['title'];
								if(data['firstname']) document.getElementById("firstname").value = data['firstname'];
								if(data['surname']) document.getElementById("surname").value = data['surname'];
								if(data['company']) document.getElementById("company").value = data['company'];
								if(data['telephone']) document.getElementById("phone").value = data['telephone'];
								if(data['mobile']) document.getElementById("mobile").value = data['mobile'];
								if(data['email']) document.getElementById("email").value = data['email'];
								//if(data['fax']) document.getElementById("fax").value = data['fax'];
								
								//if(data['recordID']) document.getElementById("delCustomerID").value = data['recordID'];
								if(data['dtitle']) document.getElementById("deltitle").value = data['dtitle'];
								else if(data['title']) document.getElementById("deltitle").value = data['title'];
								else document.getElementById("deltitle").value = "";
								if(data['dfirstname']) document.getElementById("delfirstname").value = data['dfirstname'];
								else if(data['firstname']) document.getElementById("delfirstname").value = data['firstname'];
								else document.getElementById("delfirstname").value = "";
								if(data['dsurname']) document.getElementById("delsurname").value = data['dsurname'];
								else if(data['surname']) document.getElementById("delsurname").value = data['surname'];
								else document.getElementById("delsurname").value = "";
								if(data['dpostcode']) document.getElementById("delPstCde").value = data['dpostcode'];
								else document.getElementById("delPstCde").value = "";
								if(data['dadd1']) document.getElementById("deladd1").value = data['dadd1'];
								else document.getElementById("deladd1").value = "";
								if(data['dadd2']) document.getElementById("deladd2").value = data['dadd2'];
								else document.getElementById("deladd2").value = "";
								if(data['dadd3']) document.getElementById("deladd3").value = data['dadd3'];
								else document.getElementById("deladd3").value = "";
								if(data['dtown']) document.getElementById("delTown").value = data['dtown'];
								else document.getElementById("delTown").value = "";
								if(data['dcounty']) document.getElementById("delCnty").value = data['dcounty'];
								else document.getElementById("delCnty").value = "";
								
								if(data['btitle']) document.getElementById("biltitle").value = data['btitle'];
								else if(data['title']) document.getElementById("biltitle").value = data['title'];
								else document.getElementById("biltitle").value = "";
								if(data['bfirstname']) document.getElementById("bilfirstname").value = data['bfirstname'];
								else if(data['firstname']) document.getElementById("bilfirstname").value = data['firstname'];
								else document.getElementById("bilfirstname").value = "";
								if(data['bsurname']) document.getElementById("bilsurname").value = data['bsurname'];
								else if(data['surname']) document.getElementById("bilsurname").value = data['surname'];
								else document.getElementById("bilsurname").value = "";
								if(data['bpostcode']) document.getElementById("bilPstCde").value = data['bpostcode'];
								else document.getElementById("bilPstCde").value = "";
								if(data['badd1']) document.getElementById("biladd1").value = data['badd1'];
								else document.getElementById("biladd1").value = "";
								if(data['badd2']) document.getElementById("biladd2").value = data['badd2'];
								else document.getElementById("biladd2").value = "";
								if(data['badd3']) document.getElementById("biladd3").value = data['badd3'];
								else document.getElementById("biladd3").value = "";
								if(data['btown']) document.getElementById("bilTown").value = data['btown'];
								else document.getElementById("bilTown").value = "";
								if(data['bcounty']) document.getElementById("bilCnty").value = data['bcounty'];
								else document.getElementById("bilCnty").value = "";
							});
						break;
						case "customerDeliveryAdd":
							$('#customerSearchResultsDelivery').empty();
							jQuery.ajax({
								type: "POST",
								url: "includes/ajax_search.php",
								data: {q: listitem, searchType: "customerReturn"},
								async: true,
								dataType: "json" // Set the data type so jQuery can parse it for you
							}).done(function( data ) {
								if(data['recordID']) document.getElementById("delCustomerID").value = data['recordID'];
								if(data['dtitle']) document.getElementById("deltitle").value = data['dtitle'];
								else if(data['title']) document.getElementById("deltitle").value = data['title'];
								else document.getElementById("deltitle").value = "";
								if(data['dfirstname']) document.getElementById("delfirstname").value = data['dfirstname'];
								else if(data['firstname']) document.getElementById("delfirstname").value = data['firstname'];
								else document.getElementById("delfirstname").value = "";
								if(data['dsurname']) document.getElementById("delsurname").value = data['dsurname'];
								else if(data['surname']) document.getElementById("delsurname").value = data['surname'];
								else document.getElementById("delsurname").value = "";
								if(data['dpostcode']) document.getElementById("delPstCde").value = data['dpostcode'];
								else document.getElementById("delPstCde").value = "";
								if(data['dadd1']) document.getElementById("deladd1").value = data['dadd1'];
								else document.getElementById("deladd1").value = "";
								if(data['dadd2']) document.getElementById("deladd2").value = data['dadd2'];
								else document.getElementById("deladd2").value = "";
								if(data['dadd3']) document.getElementById("deladd3").value = data['dadd3'];
								else document.getElementById("deladd3").value = "";
								if(data['dtown']) document.getElementById("delTown").value = data['dtown'];
								else document.getElementById("delTown").value = "";
								if(data['dcounty']) document.getElementById("delCnty").value = data['dcounty'];
								else document.getElementById("delCnty").value = "";
							});
						break;
						case "stock":
							$('#stockSearchResults').empty();
							jQuery.ajax({
								type: "POST",
								url: "includes/ajax_search.php",
								data: {q: listitem, searchType: "stockReturn"},
								async: true,
								dataType: "json" // Set the data type so jQuery can parse it for you
							}).done(function( data ) {
								if(data['recordID']) document.getElementById("stockIDAdd").value = data['recordID'];
								else document.getElementById("stockIDAdd").value = "";
								if(data['stockCode']) document.getElementById("stockCodeAdd").value = data['stockCode'];
								else document.getElementById("stockCodeAdd").value = "";
								if(data['stockCode']) document.getElementById("stockCodeHidden").value = data['stockCode'];
								else document.getElementById("stockCodeHidden").value = "";
								if(data['name']) document.getElementById("nameAdd").value = data['name'];
								else document.getElementById("nameAdd").value = "";
								if(data['description']) document.getElementById("descriptionAdd").value = data['description'];
								else document.getElementById("descriptionAdd").value = "";
								if(data['price']) document.getElementById("priceAdd").value = data['price'];
								else document.getElementById("priceAdd").value = "";
								if(data['vatID']) document.getElementById("vatSelectAdd").value = data['vatID'];
								else document.getElementById("vatSelectAdd").value = "";
								document.getElementById("quantityAdd").value = 1;
								
								var today = new Date();
								var dd = today.getDate() + 7;
								var mm = today.getMonth();
								var y = today.getFullYear();
								var formattedDate = dd + '/'+ mm + '/'+ y;
								document.getElementById("dispatchDateAdd").value = formattedDate;
							});
						break;
					}
				}
				var delay = (function(){
				  var timer = 0;
				  return function(callback, ms){
					clearTimeout (timer);
					timer = setTimeout(callback, ms);
				  };
				})();
				$().ready(function(){
					$('#frm_orderDate').datepicker({
						dateFormat: 'dd/mm/yy',
						changeMonth: true, 
						changeYear: true
					});
					$('#dispatchDateAdd').datepicker({
						dateFormat: 'dd/mm/yy',
						changeMonth: true, 
						changeYear: true,
						beforeShowDay: function(date) {
							var day = date.getDay();
							return [(day != 5 && day != 6 && day != 0)];
						}
					});
					$('#paymentDateAdd').datepicker({
						dateFormat: 'dd/mm/yy',
						changeMonth: true, 
						changeYear: true
					});
					$('#stockCodeAdd').change(function() {
						setitem(this.value,'stock');
					});
					$('#frm_deliveryDate').datepicker({
						dateFormat: 'dd/mm/yy',
						changeMonth: true, 
						changeYear: true,
						beforeShowDay: function(date) {
							var day = date.getDay();
							return [(day != 5 && day != 6 && day != 0)];
						}
					});
					
					
					<?php
					
					if($scroll == 1) print("$('.whitePage').scrollTop($('.whitePage')[0].scrollHeight);");
					
					?>

					//$('#stockCodeAdd').focus();
					//$('#stockCodeAdd').show().focus().click();
					
					/*$("#tabs").tabs({ beforeActivate: function(event ,ui){

							alert( ui.newTab.attr('li',"innerHTML")[0].getElementsByTagName("a")[0].innerHTML);
							$("select").searchable({
								maxListSize: 100,						// if list size are less than maxListSize, show them all
								maxMultiMatch: 50,						// how many matching entries should be displayed
								exactMatch: false,						// Exact matching on search
								wildcards: true,						// Support for wildcard characters (*, ?)
								ignoreCase: true,						// Ignore case sensitivity
								latency: 200,							// how many millis to wait until starting search
								warnMultiMatch: 'top {0} matches ...',	// string to append to a list of entries cut short by maxMultiMatch 
								warnNoMatch: 'no matches ...',			// string to show in the list when no entries match
								zIndex: 'auto'							// zIndex for elements generated by this plugin
							});
						}
					});*/
					
					<?php
					
					if(!empty($_REQUEST['customerIDPassed'])) print ("setitem(".$_REQUEST['customerIDPassed'].",'customer');");
					
					?>

				});
			</script>
			<?php
			if (!empty($orderID)){
				$strdbsql = "SELECT * FROM order_header WHERE recordID = :recordID";
				$strType = "single";
				$arrdbparams = array("recordID"=>$orderID);
				$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
				$customerID = $orderHeader['customerID'];
				
				if (empty($orderHeader['customerID'])) {
					print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>Order number not found</p></div>");
					$orderID = "";
				}
			} 
			
			if (!empty($orderExternalRef)){
				$strdbsql = "SELECT * FROM order_header WHERE externalOrderID = :externalOrderID";
				$strType = "single";
				$arrdbparams = array("externalOrderID"=>$orderExternalRef);
				$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
				$customerID = $orderHeader['customerID'];
				
				if (empty($orderHeader['customerID'])) {
					print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>Order external reference not found</p></div>");
					$orderExternalRef = "";
				} else {
					$orderID = $orderHeader['recordID'];
				}
			}
			
			print ("<form action='sales-order-create.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' />");
				print ("<input type='hidden' class='nodisable' name='orderID' id='orderID' value='$orderID' />");
				print ("<input type='hidden' class='nodisable' name='customerID' id='customerID' value='$customerID' />");
				print ("<input type='hidden' class='nodisable' name='delCustomerID' id='delCustomerID' value='$delCustomerID' />");
				print ("<input type='hidden' class='nodisable' name='stockIDAdd' id='stockIDAdd' value='' />");
				print ("<input type='hidden' class='nodisable' name='stockIDDelete' id='stockIDDelete' value='' />");
				print ("<input type='hidden' class='nodisable' name='stockIDEdit' id='stockIDEdit' value='' />");
				print ("<input type='hidden' class='nodisable' name='paymentID' id='paymentID' value='' />");
				print ("<input type='hidden' class='nodisable' name='paymentStatusUpdate' id='paymentStatusUpdate' value='' />");
				
				switch ($strcmd){
					default:
						fnGetSOP ($orderID, $stockIDEdit, $datnow, $conn);
					break;
				}
			print ("</form>");
		print("</div>");
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>