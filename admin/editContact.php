<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * editContact.php                                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	
	// Ensure user is logged on
	// --------------------
	if (empty($_SESSION['userID'])) { header ("Location: account.php"); }

	
	$strpage = "editContact"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// ************* Requests processing ****************** //
	//=====================================================//
	if(isset($_REQUEST['command'])) $strcmd = $_REQUEST['command'];
	else $strcmd = "";
	$strerror = "";
	
	switch ($strcmd)
	{

		case "updateac":
			
			$strfirstName = $_REQUEST['firstname'];
			$strlsurname = $_REQUEST['surname'];
			$strTitle = $_REQUEST['title'];
			$strphone = $_REQUEST['phone'];
			$stremail = $_REQUEST['email'];
			$strmob = $_REQUEST['mob'];
			$strfax = $_REQUEST['fax'];
			$strcompany = $_REQUEST['company'];
			
			$params = array(
				"email" => $_SESSION['email'],
				"custID" => $intuserid
			);
			
			$accountWithEmail = getUserDetails($params, "byemailandnotid", $conn);

			if (!empty($accountWithEmail))
			{
				$strerror = "Sorry - This email is already in use with another account.";
				$strcmd = "";
			}
			else
			{
				include("includes/inc_emailtest.php");
				if(is_valid_email_address($stremail)) {

					$values = array(
						"firstname" => $strfirstName,
						"surname" => $strlsurname,
						"title" => $strTitle,
						"telephone" => $strphone,
						"email" => $stremail,
						"mobile" => $strmob,
						"fax" => $strfax,
						"company" => $strcompany,
						"custid" =>  $_SESSION['userID']
					);
					if(updateUserDetails($values, "byid", $conn)) header ("Location: account.php");
					else {
						$strerror = "There was an error saving your changes please try again.";
						$strcmd = "";						
					}
				}
				else {
					$strerror = "Your email address does not appear to be valid, please try again.";
					$strcmd = "";
				}
			}

			break;
		default:
			
			$userDetails = getUserDetails($_SESSION['userID'], "byid", $conn);

			$strfirstName = $userDetails['firstname'];
			$strlsurname = $userDetails['surname'];
			$strTitle = $userDetails['title'];
			$strphone = $userDetails['telephone'];
			$stremail = $userDetails['email'];
			$strmob = $userDetails['mobile'];
			$strcomp = $userDetails['company'];
			$strfax = $userDetails['fax'];
			
			break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	print("<div id='mainContent'>");
	
		if($strerror != "") {
			print("<div class='notification-error'>");
				print("<h3>Error</h3>");
				print("<p>".$strerror."</p>");
			print("</div>");
		}
		if($strinfo != "") {
			print("<div class='notification-info'>");
				print("<h3>Info</h3>");
				print("<p>".$strinfo."</p>");
			print("</div>");
		}
?>
<script type="Text/JavaScript">
<!--

function jscontentval()
{
	if (document.frm_form.firstname.value == "")
	{
		alert("Please enter your First Name.");
		return false;
	}
	else
	{
		if (document.frm_form.surname.value == "")
		{
			alert("Please enter your Surname.");
			return false;
		}
		else
		{
			if (document.frm_form.phone.value == "")
			{
				alert("Please enter a Phone Number.");
				return false;
			}
			else
			{
				if (document.frm_form.email.value == "")
				{
					alert("Please enter a valid Email Address.");
					return false;
				}
				else
				{
					document.frm_form.command.value = "updateac";
					return true;
				}
			}
		}
	}
}

function jsupdate()
{
  if(jscontentval()) document.frm_form.submit();
}
function jscancel() {
	window.location = "account.php";
}
-->
</script>
<?php

	print ("<form name='frm_form' action='editContact.php' method='post' onSubmit='return jsaddressval()' id='form'>");
	print ("<input type='hidden' name='command' />");


	if ($intuserid > 0)
	{
		print ("<div class='textbox'>");
			print ("<h2>Edit Account &amp; Contact Details</h2>");
			print ("<div class='bodytext' style='clear:both; padding: 5px;'>");
			print ("<table style='width:700px'>");
			print ("<tr><td colspan='2'></td></tr>");
			print ("<tr><td><label for='email'>Email/Login : </label></td><td align='left'><input type='text' name='email' size='30' value=\"$stremail\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
			print ("<tr><td><label for='title'>Title : </label></td><td align='left'><input id='title' name='title' type='text' size='10' value=\"$strTitle\" /></td></tr>");
			print ("<tr><td><label for='firstname'>First Name : </label></td><td align='left'><input id='firstname' name='firstname' type='text' size='30' value=\"$strfirstName\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
			print ("<tr><td><label for='surname'>Surname : </label></td><td align='left'><input id='surname' name='surname' type='text' size='30' value=\"$strlsurname\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
			print ("<tr><td><label for='phone'>Phone Number : </label></td><td align='left'><input type='text' id='phone' name='phone' size='30' value=\"$strphone\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
			print ("<tr><td><label for='mob'>Mobile : </label></td><td align='left'><input type='text' id='mob' name='mob' size='30' value=\"$strmob\" /></td></tr>");
			print ("<tr><td><label for='fax'>Fax : </label></td><td align='left'><input type='text' id='fax' name='fax' size='30' value=\"$strfax\" /></td></tr>");
			print ("<tr><td><label for='company'>Company : </label></td><td align='left'><input type='text' id='company' name='company' size='30' value=\"$strcomp\" /></td></tr>");
			print ("</table><br>");
			print ("</div>");
			print ("<div class='buttons'><input type='button' class='short_button' id='updateDelBill' onclick='jsupdate()' value='Update My Details' /><input type='button' id='updateDelBill' class='short_button' onclick='jscancel()' value='Cancel Changes' /></div>");
		print ("</div><!--end of signed in section-->");
	}

	print ("</form>");
	print ("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>