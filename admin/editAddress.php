<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * editAddress.php                                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	
	// Ensure user is logged on
	// --------------------
	if (empty($_SESSION['userID'])) { header ("Location: account.php"); }

	
	$strpage = "editAddress"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	$strerror = "";

	// ************* Requests processing ****************** //
	//=====================================================//

	if(isset($_POST['address'])) $intaddressid = $_POST['address'];
	else $intaddressid =  0;

	if(isset($_REQUEST['command'])) $strcmd = $_REQUEST['command'];
	else $strcmd = "";
	$strerror = "";


	switch ($strcmd)
	{

		case "update":
			/*$strfirstname = $_REQUEST['firstname'];
			$strsurname = $_REQUEST['surname'];
			$strTitle = $_REQUEST['title'];
			$stradd1 = $_REQUEST['add1'];
			$stradd2 = $_REQUEST['add2'];
			$stradd3 = $_REQUEST['add3'];
			$strTown = $_REQUEST['town'];
			if($strcountry == "US") $strCnty = $_REQUEST['stateUS'];
			else if($strcountry == "CA") $strCnty = $_REQUEST['stateCA'];
			else $strCnty = $_REQUEST['cnty'];
			$strPstCde = $_REQUEST['pstCde'];*/
			
			$values = array(
				"firstname" => $_REQUEST['firstname'],
				"surname" => $_REQUEST['surname'],
				"title" => $_REQUEST['title'],
				"add1" => $_REQUEST['add1'],
				"add2" => $_REQUEST['add2'],
				"add3" => $_REQUEST['add3'],
				"town" => $_REQUEST['town'],
				"county" => $_REQUEST['cnty'],
				"country" => $_REQUEST['country'],
				"postcode" => $_REQUEST['pstCde'],
				"custid" => $intaddressid
			);
			//echo updateUserBillDelAddresses($values, "byid", $conn);
			if(updateUserBillDelAddresses($values, "byid", $conn)) header ("Location: manageAddresses.php");
			break;
		case "saveaddress":
			/*$strfirstname = $_REQUEST['firstname'];
			$strsurname = $_REQUEST['surname'];
			$strTitle = $_REQUEST['title'];
			$stradd1 = $_REQUEST['add1'];
			$stradd2 = $_REQUEST['add2'];
			$stradd3 = $_REQUEST['add3'];
			$strTown = $_REQUEST['town'];
			$strcountry = $_REQUEST['country'];
			if($strcountry == "US") $strCnty = $_REQUEST['stateUS'];
			else if($strcountry == "CA") $strCnty = $_REQUEST['stateCA'];
			else $strCnty = $_REQUEST['cnty'];
			$strPstCde = $_REQUEST['pstCde'];*/
			
			$values = array(
				"firstname" => $_REQUEST['firstname'],
				"surname" => $_REQUEST['surname'],
				"title" => $_REQUEST['title'],
				"add1" => $_REQUEST['add1'],
				"add2" => $_REQUEST['add2'],
				"add3" => $_REQUEST['add3'],
				"town" => $_REQUEST['town'],
				"county" => $_REQUEST['cnty'],
				"country" => $_REQUEST['country'],
				"postcode" => $_REQUEST['pstCde'],
				"custid" => $_SESSION['userID']
			);
			
			if(insertUserAddress($values, $conn)) header ("Location: manageAddresses.php");
			break;
		case "new":
			$strfirstname = "";
			$strsurname = "";
			$strtitle = "";
			$stradd1 = "";
			$stradd2 = "";
			$stradd3 = "";
			$strtown = "";
			$strCnty = "";
			$strcountry = "";
			$strpstCde = "";
			break;
		default: 
			$address = getUserAddresses($intaddressid, "byid", $conn);
			
			$strfirstname = $address['firstname'];
			$strsurname = $address['surname'];
			$strtitle = $address['title'];
			$stradd1 = $address['add1'];
			$stradd2 = $address['add2'];
			$stradd3 = $address['add3'];
			$strtown = $address['town'];
			$strCnty = $address['county'];
			$strcountry = $address['country'];
			$strpstCde = $address['postcode'];
			break;
	}
	if($intaddressid != 0)
	{
		$address = getUserAddresses($intaddressid, "byid", $conn);
				
		$strfirstname = $address['firstname'];
		$strsurname = $address['surname'];
		$strtitle = $address['title'];
		$stradd1 = $address['add1'];
		$stradd2 = $address['add2'];
		$stradd3 = $address['add3'];
		$strtown = $address['town'];
		$strCnty = $address['county'];
		$strcountry = $address['country'];
		$strpstCde = $address['postcode'];
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	print("<div id='mainContent'>");
	
		if($strerror != "") {
			print("<div class='notification-error'>");
				print("<h3>Error</h3>");
				print("<p>".$strerror."</p>");
			print("</div>");
		}
		if($strinfo != "") {
			print("<div class='notification-info'>");
				print("<h3>Info</h3>");
				print("<p>".$strinfo."</p>");
			print("</div>");
		}
?>
<script type="Text/JavaScript">
	<!--
	function jsaddressval()
	{
		/*var selectedcntry = document.getElementById("country").options[document.getElementById("country").selectedIndex].value;
		if((selectedcntry == "IE"  || selectedcntry == "AO" || selectedcntry == "BZ" || selectedcntry == "BJ" || selectedcntry == "HK" || selectedcntry == "MO") && document.frm_form.pstCde.value == "") {
			document.frm_form.pstCde.value = "NA"
		}*/

		if (document.frm_form.firstname.value == "") {
			alert("Please enter your First Name.");
			return false;
		} else if (document.frm_form.surname.value == "") {
			alert("Please enter your Surname.");
			return false;
		} else if (document.frm_form.add1.value == "") {
			alert("Please enter the Building Name or House number and Street Address.");
			return false;
		} else if (document.frm_form.town.value == "") {
			alert("Please enter the Town/City.");
			return false;
		} else if (document.frm_form.pstCde.value == "") {
				alert("Please enter the Postal/Zip Code.");
				return false;
		} else if (document.frm_form.country.value == "") {
				alert("Please enter a country.");
				return false;
		} else {
			return true;
		}
	}

	function jsnewaddress(mytype)
	{
		if(jsaddressval()) {
			document.frm_form.command.value = mytype;
			document.frm_form.submit();
		}
	}

	function jsback() {
		window.location = "manageAddresses.php";
	}
	function formupdate() {
		var selectedcntry = document.getElementById("country").options[document.getElementById("country").selectedIndex].value;
		if (selectedcntry == "IE" || selectedcntry == "AO" || selectedcntry == "BZ" || selectedcntry == "BJ" || selectedcntry == "HK" || selectedcntry == "MO") {
			document.getElementById("county").style.display = "table-row";
			document.getElementById("state_us").style.display = "none";
			document.getElementById("state_ca").style.display = "none";
			document.getElementById("postcode").style.display = "none";

		} else if (selectedcntry == "US") {
			document.getElementById("county").style.display = "none";
			document.getElementById("state_us").style.display = "table-row";
			document.getElementById("state_ca").style.display = "none";
			document.getElementById("postcode").style.display = "inline-block";

		} else if (selectedcntry == "CA") {
			document.getElementById("county").style.display = "none";
			document.getElementById("state_us").style.display = "none";
			document.getElementById("state_ca").style.display = "table-row";
			document.getElementById("postcode").style.display = "inline-block";
		} else {
			document.getElementById("county").style.display = "table-row";
			document.getElementById("state_us").style.display = "none";
			document.getElementById("state_ca").style.display = "none";
			document.getElementById("postcode").style.display = "inline-block";
		}
	}
	-->
</script>
<?php
		print ("<form name='frm_form' action='editAddress.php' method='post' onSubmit='return jsaddressval()'>");
			print ("<input type='hidden' name='command' />");
			print ("<input type='hidden' name='address' value='$intaddressid'/>");

			if ($intaddressid > 0 || $strcmd == 'new')
			{
				if ($strcountry == "IE"  || $strcountry == "AO" || $strcountry == "BZ" || $strcountry == "BJ" || $strcountry == "HK" || $strcountry == "MO") {
					$strselected1 = "";
					$strselected2 = "style='display:none'";
					$strselected3 = "style='display:none'";
					$strselected4 = "style='display:none'";
				}
				else if ($strcountry == "US") {
					$strselected1 = "style='display:none'";
					$strselected2 = "";
					$strselected3 = "style='display:none'";
					$strselected4 = "";
				}
				else if ($strcountry == "CA") {
					$strselected1 = "style='display:none'";
					$strselected2 = "style='display:none'";
					$strselected3 = "";
					$strselected4 = "";
				}
				else {
					$strselected1 = "";
					$strselected2 = "style='display:none'";
					$strselected3 = "style='display:none'";
					$strselected4 = "";
				}
				print ("<div class='textbox'>");
					print ("<h2>Address Editor</h2>");
					print ("<table style='padding: 5px;'>");
						print ("<tr><td><label for='firstname'>First Name : </label></td><td align='left'><input name='firstname' type='text' size='30' value=\"$strfirstname\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='surname'>Surname : </label></td><td align='left'><input name='surname' type='text' size='30' value=\"$strsurname\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='custTitle'>Title : </label></td><td align='left'><input name='title' type='text' size='10' value=\"$strtitle\" /></td></tr>");
						print ("<tr><td><label for='add1'>Address 1 : </label></td><td align='left'><input name='add1' size='30' value=\"$stradd1\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='add2'>Address 2 : </label></td><td align='left'><input name='add2' size='30' value=\"$stradd2\" /></td></tr>");
						print ("<tr><td><label for='add3'>Address 3 : </label></td><td align='left'><input name='add3' size='30' value=\"$stradd3\" /></td></tr>");
						print ("<tr><td><label for='town'>Town/City : </label></td><td align='left'><input name='town' size='30' value=\"$strtown\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='country'>Country : </label></td><td align='left'><input name='country' size='30' value=\"$strcountry\" />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr id='county' $strselected1><td><label for='cnty'>County/State : </label></td><td align='left'><input name='cnty' size='30' value=\"$strCnty\" /></td></tr>");
						print ("<tr><td><label for='pstCde'>Post/Zip Code : </label></td><td align='left'><input name='pstCde' size='30' value=\"$strpstCde\" />&nbsp;<font size='1' color='red'  id='postcode' $strselected4>*</font></td></tr>");
						print ("<tr><td colspan='2' style='text-align: center; padding-top: 10px;'>");
							if ($strcmd == 'new') {
								print ("<input type='button' id='updateDelBill' class='short_button' onclick='jsnewaddress(\"saveaddress\")' value='Save Address' />");
							}
							else
							{
								print ("<input type='button' id='updateDelBill' class='short_button' onclick='jsnewaddress(\"update\")' value='Update Address' />");
							}
							print ("<input type='button' id='updateDelBill' class='short_button' onclick='jsback()' value='Return to Address Book' />");
						print ("</td></tr>");
					print ("</table>");
				print ("</div>");
			}
			else
			{
				print("$strerror");

			}
		print ("</form>");
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>