<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * cms-promo.php						                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-promo"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['newsCategoryID'])) $newsCategoryID = $_REQUEST['newsCategoryID']; else $newsCategoryID = "";
	if (isset($_REQUEST['newsItemID'])) $newsItemID = $_REQUEST['newsItemID']; else $newsItemID = "";
	$strItemType = 1;
	
	switch($strcmd)
	{		
		case "deleteNewsCategory":
			
			$strdbsql = "DELETE FROM site_news_categories WHERE recordID = :newsCategoryID";
			$strType = "delete";
			$arrdbparams = array( "newsCategoryID" => $newsCategoryID );
			$deleteNewsItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
		
		case "upNewsItem":
			$strdbsql = "SELECT displayOrder FROM site_promotions WHERE recordID = :item ";
			$test = query($conn,$strdbsql,"single",["item"=> $newsItemID ]);
			
			if($test['displayOrder'] > 1) {
				
				$strdbsql = "UPDATE site_promotions SET displayOrder = displayOrder + 1 WHERE displayOrder = :newPos";
				$update1 = query($conn,$strdbsql,"update",["newPos"=> $test['displayOrder']-1 ]);
				
				$strdbsql = "UPDATE site_promotions SET displayOrder = displayOrder - 1 WHERE recordID = :item";
				$update2 = query($conn,$strdbsql,"update",["item"=> $newsItemID ]);
				
			}
			
			$strcmd = "";
			break;
		case "downNewsItem":
			$strdbsql = "SELECT displayOrder FROM site_promotions WHERE recordID = :item ";
			$test1 = query($conn,$strdbsql,"single",["item"=> $newsItemID ]);
			
			$strdbsql = "SELECT displayOrder FROM site_promotions ";
			$test2 = query($conn,$strdbsql,"multi");
			
			if($test1['displayOrder'] < count($test2)) {
				
				$strdbsql = "UPDATE site_promotions SET displayOrder = displayOrder - 1 WHERE displayOrder = :newPos";
				$update1 = query($conn,$strdbsql,"update",["newPos"=> $test1['displayOrder']+1 ]);
				
				$strdbsql = "UPDATE site_promotions SET displayOrder = displayOrder + 1 WHERE recordID = :item";
				$update2 = query($conn,$strdbsql,"update",["item"=> $newsItemID ]);
				
			}
			
			$strcmd = "";
			break;
		case "insertNewsItem":
		case "updateNewsItem":
			
			if($_POST['frm_category'] == 0)
			{
				$_POST['frm_category'] = null;
			}
			
			$arrdbparams = array(
				"title" => $_POST['frm_title'],
				"subtitle" => $_POST['frm_subtitle'],
				"content" => $_POST['frm_content'],
				"label1" => $_POST['frm_label1'],
				"link1" => $_POST['frm_link1'],
				"label2" => $_POST['frm_label2'],
				"link2" => $_POST['frm_link2'],
				"image" => $_POST['frm_image']
			);
			
			
			
				$arrparams['type'] = $_POST['frm_category'];
			
			
			if ($strcmd == "insertNewsItem")
			{	
				$strdbsql = "INSERT INTO site_promotions (title, description, type, label, link, image) 
							VALUES (:title, :content, :type, :label, :link, :image)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateNewsItem")
			{
				$strdbsql = "UPDATE site_promotions SET title = :title,  subtitle = :subtitle, description = :content, type = :type, label1 = :label1, link1 = :link1,  label2 = :label2, link2 = :link2, image = :image WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $newsItemID;
				$strType = "update";
			}
			
			$updateNewsItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertNewsItem")
			{
				$newsItemID = $updateNewsItem;
			}
			
			if ($strcmd == "insertNewsItem")
			{
				if ($updateNewsItem > 0)
				{
					$strsuccess = "Item successfully added";
				}
				elseif ($updateNewsItem == 0)
				{
					$strerror = "An error occurred while adding the news item";
				}
			}
			elseif ($strcmd == "updateNewsItem")
			{
				if ($updateNewsItem <= 1)
				{
					$strsuccess = "Item successfully updated";
				}
				elseif ($updateNewsItem > 1)
				{
					$strwarning = "An error may have occurred while updating this item";
				}
			}
			
			$strcmd = "viewNewsItem";
			
		break;
		
		case "deleteNewsItem":
			
			$strdbsql = "DELETE FROM site_promotions WHERE recordID = :newsItemID";
			$strType = "delete";
			$arrdbparams = array( "newsItemID" => $newsItemID );
			$deleteNewsItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_newimage']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print("$upfile - saving image<br>");

			if ($_FILES['frm_newimage']['error'] > 0)
			{
				switch ($_FILES['frm_newimage']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_newimage']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_newimage']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_newimage']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_newimage']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strnewspath = $strimguploadpath."promotions/";
				
				$strbranddir = $strrootpath.$strnewspath;
				
				$newfile = $strbranddir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				unlink($upfile);
				
				if (isset($_REQUEST['frm_replacecurrent'])) $replaceCurrent = $_REQUEST['frm_replacecurrent']; else $replaceCurrent = 0;
				
				if($replaceCurrent)
				{
					$updateNewsImageQuery = "UPDATE site_promotions SET image = :image WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array (
						"image" => $newfilename,
						"recordID" => $newsItemID
					);
					$updateBrandImage = query($conn, $updateNewsImageQuery, $strType, $arrdbparams);
				}
			}
			
			$strsuccess = "New image added";
			
			$strcmd = "viewNewsItem";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Promotions</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsinsertNewsCategory() {
					document.form.cmd.value='insertNewsCategory';
					document.form.submit();
				}
				function jsupdateNewsCategory() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateNewsCategory';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddNewsItem() {
					document.form.cmd.value="addNewsItem";
					document.form.submit();
				}
				function jsviewNewsItem(newsItemID) {
					document.form.cmd.value="viewNewsItem";
					document.form.newsItemID.value=newsItemID;
					document.form.submit();
				}
				function jsdeleteNewsItem(newsItemID) {
					if(confirm("Are you sure you want to delete this news item?"))
					{
						document.form.cmd.value="deleteNewsItem";
						document.form.newsItemID.value=newsItemID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertNewsItem() {
					document.form.cmd.value='insertNewsItem';
					document.form.submit();
				}
				function jsupdateNewsItem() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateNewsItem';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsNewsMoveUp(newsItemID) {
					document.form.cmd.value="upNewsItem";
					document.form.newsItemID.value=newsItemID;
					document.form.submit();
				}
				function jsNewsMoveDown(newsItemID) {
					document.form.cmd.value="downNewsItem";
					document.form.newsItemID.value=newsItemID;
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
			</script>
			<?php
		
			print("<form action='cms-promo.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='newsCategoryID' id='newsCategoryID' value='".$newsCategoryID."' />");
				print("<input type='hidden' name='newsItemID' id='newsItemID' value='".$newsItemID."' />");
				print("<input type='hidden' name='itemType' id='itemType' value='".$strItemType."' />");
				
				switch($strcmd)
				{
					case "viewNewsItem":
					case "addNewsItem":
						
						print("<div class='row'>");
							print("<div class='col-md-6'>");
								print("<div class='section'>");
						
									if ($strcmd == "viewNewsItem")
									{
										$strdbsql = "SELECT * FROM site_promotions WHERE recordID = :recordID";
										$strType = "single";
										$arrdbparams = array("recordID" => $newsItemID);
										$newsItemDetails = query($conn, $strdbsql, $strType, $arrdbparams);
										$newsItemCategory = $newsItemDetails['categoryID'];
										$newsImage = $newsItemDetails['image'];
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Promotion Details</legend>");
									}
									elseif ($strcmd == "addNewsItem")
									{
										$pageDetails = array(
											"title" => "",
											"content" => ""
										);
										$newsItemCategory = 0;
										$newsImage = "";
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Promotion Details</legend>");
									}
										print("<div class='row'>");
											print("<div class='col-md-6 form-group'>");
												print("<label for='frm_category' class='control-label'>Type</label>");
												print("<select id='frm_category' name='frm_category' class='form-control' >");
													print("<option value='1' ".(($newsItemDetails['type'] == 1) ? "selected" : "").">Large Banner (4 x 2)</option>");
													print("<option value='5' ".(($newsItemDetails['type'] == 5) ? "selected" : "").">Medium Banner (4 x 1)</option>");
													print("<option value='6' ".(($newsItemDetails['type'] == 6) ? "selected" : "").">Medium Banner (3 x 1)</option>");
													print("<option value='2' ".(($newsItemDetails['type'] == 2) ? "selected" : "").">Medium Square (2 x 2)</option>");
													print("<option value='3' ".(($newsItemDetails['type'] == 3) ? "selected" : "").">Small Banner (2 x 1)</option>");
													print("<option value='4' ".(($newsItemDetails['type'] == 4) ? "selected" : "").">Small Square (1 x 1)</option>");
												print("</select>");
											print("</div>");
											
											print("<div class='col-md-6 form-group'>");
												print("<label for='frm_image' class='control-label'>Image</label>");
												print("<select id='frm_image' name='frm_image' class='form-control' >");
													print("<option value=''>None</option>");
													$dh = opendir("../images/promotions/");
													while (false !== ($image = readdir($dh))) {
														if ($image != "." && $image != "..") {
															if($image == $newsImage)
															{
																$strselected = " selected";
															}
															else
															{
																$strselected = "";
															}
															print("<option value='".$image."'".$strselected.">".$image."</option>");
														}
													}
												print("</select>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_subtitle' class='control-label'>Small Title</label>");
												print("<input type='text' class='form-control' id='frm_subtitle' name='frm_subtitle' value=\"".str_replace('"', '\"',$newsItemDetails['subtitle'])."\">");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_title' class='control-label'>Large Title</label>");
												print("<input type='text' class='form-control' id='frm_title' name='frm_title' value=\"".str_replace('"', '\"',$newsItemDetails['title'])."\">");
											print("</div>");
										print("</div>");
										
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_content' class='control-label'>Description</label>");
												print("<textarea class='form-control tinymce' id='frm_content' name='frm_content' rows='12'>".$newsItemDetails['description']."</textarea>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='col-md-6 form-group'>");
												print("<label for='frm_label1' class='control-label'>Button 1 Label</label>");
												print("<input id='frm_label1' name='frm_label1' class='form-control' value=\"".str_replace('"', '\"',$newsItemDetails['label1'])."\" />");
											print("</div>");
											print("<div class='col-md-6 form-group'>");
												print("<label for='frm_link1' class='control-label'>Button 1 Link (".$strsiteurl."*)</label>");
												print("<input type='text' class='form-control' id='frm_link1' name='frm_link1' value=\"".str_replace('"', '\"',$newsItemDetails['link1'])."\">");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='col-md-6 form-group'>");
												print("<label for='frm_label2' class='control-label'>Button 2 Label</label>");
												print("<input id='frm_label2' name='frm_label2' class='form-control' value=\"".str_replace('"', '\"',$newsItemDetails['label2'])."\" />");
											print("</div>");
											print("<div class='col-md-6 form-group'>");
												print("<label for='frm_link2' class='control-label'>Button 2 Link (".$strsiteurl."*)</label>");
												print("<input type='text' class='form-control' id='frm_link2' name='frm_link2' value=\"".str_replace('"', '\"',$newsItemDetails['link2'])."\">");
											print("</div>");
										print("</div>");
									print("</fieldset>");
								
								print("</div>");
							print("</div>");
									
							if($strcmd == "viewNewsItem")
							{
								print("<div class='col-md-6'>");
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
											print("<legend>Add New Image</legend>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_newimage' class='control-label'>Image</label>");
													print("<input type='file' class='form-control' id='frm_newimage' name='frm_newimage' />");
												print("</div>");
											print("</div>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_replacecurrent' class='control-label'>Replace current blog image</label>");
													print("<input type='checkbox' id='frm_replacecurrent' name='frm_replacecurrent' value='1' />");
												print("</div>");
											print("</div>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success'>Insert Image</button>");
												print("</div>");
											print("</div>");
										print("</fieldset>");
									print("</div>");
								print("</div>");
							}
							
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-sm-12'>");
									if($strcmd == "addNewsItem") print("<button onclick='return jsinsertNewsItem();' type='submit' class='btn btn-success right'>Save</button>");
									elseif($strcmd == "viewNewsItem") print("<button onclick='return jsupdateNewsItem();' type='submit' class='btn btn-success right'>Save</button>");
									print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
								print("</div>");
							print("</div>");
					
						break;
						
					default:
						
						$strdbsql = "SELECT * FROM site_promotions ORDER BY displayOrder";
						$newsItems = query($conn, $strdbsql, "multi");
						
						print("<div class='section'>");
							print("<fieldset class='inlineLabels'>");
							//	print("<p>Note: Only one promotion can be activated at a time.</p>");
								print("<table class='table table-striped table-bordered table-hover table-condensed datatable' >");
									print("<thead>");
										print("<tr>");
											print("<th>Position</th>");
											print("<th></th>");
											print("<th></th>");
											print("<th>Small Title</th>");
											print("<th>Big Title</th>");
											print("<th width='50%'>Description</th>");
											print("<th>Type</th>");
											print("<th>Edit</th>");
											print("<th>Delete</th>");
										print("</tr>");
									print("</thead>");
									print("<tbody>");
										foreach($newsItems AS $newsItem)
										{
											print("<tr>");
												print("<td>".$newsItem['displayOrder']."</td>");
												print("<td>");
													if($newsItem['displayOrder'] > 1) print("<button onclick='jsNewsMoveUp(\"".$newsItem['recordID']."\");' type='button' class='btn btn-primary circle'><i class='fa fa-angle-up'></i></button>");
												print("</td><td>");
													if($newsItem['displayOrder'] < count($newsItems)) print("<button onclick='jsNewsMoveDown(\"".$newsItem['recordID']."\");' type='button' class='btn btn-primary circle'><i class='fa fa-angle-down'></i></button>");
												print("</td>");
												print("<td>".$newsItem['subtitle']."</td>");
												print("<td>".$newsItem['title']."</td>");
												print("<td>".$newsItem['description']."</td>");
												switch($newsItem['type']) {
													case 1:
														print("<td>Large Banner (4 x 2)</td>");
														break;
													case 5:
														print("<td>Medium Banner (4 x 1)</td>");
														break;
													case 6:
														print("<td>Medium Banner (3 x 1)</td>");
														break;
													case 2:
														print("<td>Medium Square (2 x 2)</td>");
														break;
													case 3:
														print("<td>Small Banner (2 x 1)</td>");
														break;
													case 4:
														print("<td>Small Square (1 x 1)</td>");
														break;
												
												}
												print("<td><button onclick='return jsviewNewsItem(\"".$newsItem['recordID']."\");' type='submit' class='btn btn-primary circle'><i class='fa fa-pencil'></i></button></td>");
												print("<td><button onclick='return jsdeleteNewsItem(\"".$newsItem['recordID']."\");' type='submit' class='btn btn-danger circle'><i class='fa fa-trash'></i></button></td>");
											print("</tr>");
										}
									print("</tbody>");
								print("</table>");
							print("</fieldset>");
						print("</div>");
						
						print("<button onclick='return jsaddNewsItem();' type='submit' class='btn btn-success'>Add</button>");
						
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('.datatable').DataTable({"paging":false,"ordering": false});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>