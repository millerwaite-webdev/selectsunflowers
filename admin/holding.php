<?php

//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * holding.php                                                        * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_sitecommon.php");
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	if(isset($_GET['frame'])) $boolframe = true;
	else $boolframe = false;

	$strpage = 'holding';

	$strpgtitle = "Server Migration";
	$meta['metaTitle'] = "Server Migration - Stollers Building Products";
	$strpgcontent = "<br/><br/>We are currently moving our site to a new server. You will not be able to buy from the site during the move but please feel free to browse. Check back later today when the site should be fully up and running again.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

	/*$strbreadcrumbs = "<div class='pro_breadcrumbs-indent'><ul class='pro_breadcrumbs-one'>";
	$strbreadcrumbs .= "<li><a href='index.php'>Home</a></li>";
	$strbreadcrumbs .= "<li><a href='#' class='current'>Server Migration</a></li></ul></div>";*/
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	
	if($boolframe) {
		print("<style type='text/css'>body {background:none;}</style>");
	}
?>
<?php
	if(!$boolframe) {
?>
<div id="pagemiddle">
	<div id="content">
<?php
		include("includes/inc_sidebar.php");
	}
?>
	<!--Image map and inaccessible content goes here so users can skip over it -->
	<h2><?php echo $strpgtitle; ?></h2>
	<div id="pagetext">
		<?php echo $strpgcontent; ?>
	</div><!-- END OF PAGETEXT-->
<?php
	if(!$boolframe) {
		//breadcrumb links paste from above
		//	print("<hr class='clearing' /><br/>".$strbreadcrumbs);
?>
	</div><!--End of page contents-->
</div><!--End of 'pagemiddle'-->

<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
?>
<?php } ?>

<script type="text/javascript">
/*<![CDATA[*/
OAS_rn = new String (Math.random());
OAS_rns = OAS_rn.substring (2, 11);
var tfsm_protocol = window.location.protocol;
if (tfsm_protocol == "https:") {
 DataColl="https://";
} else { DataColl="http://"; 
 } 
document.write('<img alt=""  SRC="'+DataColl+'oas.newsquestdigital.co.uk/RealMedia/ads/adstream.track/1'+OAS_rns+'?XE&epmAccountKey=2609&epmXTransKey=1381&epmXtransStep=0&ProductCategory=&ItemDescription=&XE" style="width:0px;height:0px;border:none" />');
$(document).ready(function() {
	$('body').prepend("<div style='width:100%;z-index:500;border-bottom:solid 1px black;text-align:center; background-color:#FFFFDD;/*position:absolute;top:0px;left:0px;*/height:auto;padding:10px;'><p><b>Server Migration In Progress</b> <br/>The site is currently in the process of moving servers. You can browse our products during the migration but checkout and account processes are currently disabled.</p></div>");
});

/*]]>*/
</script>
</body>
</html>
