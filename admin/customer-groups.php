<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * customer-groups.php			                                       * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

session_start(); //stores session variables such as access levels and logon details
	$strpage = "customer-groups"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['groupID'])) $groupID = $_REQUEST['groupID']; else $groupID = "";
	
	switch($strcmd)
	{
		case "insertGroup":
		case "updateGroup":
		
			$arrdbparam = array( "customerdescription" => $_POST['frm_groupdescription'] );
			
			if ($strcmd == "insertGroup")
			{	
				$strdbsql = "INSERT INTO customer_group (description) VALUES (:customerdescription)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateGroup")
			{
				$strdbsql = "UPDATE customer_group SET description = :customerdescription WHERE recordID = :recordID";
				$arrdbparam['recordID'] = $groupID;
				$strType = "update";
			}
			
			$updateGroup = query($conn, $strdbsql, $strType, $arrdbparam);
			
			if ($strcmd == "insertGroup")
			{
				$groupID = $updateGroup;
			}
			
			if ($strcmd == "insertGroup")
			{
				if ($updateGroup > 0)
				{
					$strsuccess = "Group successfully added";
				}
				elseif ($updateGroup == 0)
				{
					$strerror = "An error occurred while adding the group";
				}
			}
			elseif ($strcmd == "updateGroup")
			{
				if ($updateGroup <= 1)
				{
					$strsuccess = "Group successfully updated";
				}
				elseif ($updateGroup > 1)
				{
					$strwarning = "An error may have occurred while updating this group";
				}
			}
			
			$strcmd = "viewGroup";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Customer Group Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				
				function jsviewGroup(groupID) {
					document.form.groupID.value=groupID;
					document.form.cmd.value="viewGroup";
					document.form.submit();
				}
				function jsinsertGroup() {
					document.form.cmd.value="insertGroup";
					document.form.submit();
				}
				function jseditGroup() {
					document.form.cmd.value="updateGroup";
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				var lastClicked = 0;
				$().ready(function() {
				
					$("#groupedCustomers").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#ungroupedCustomers").children().removeClass('selected');
						}
						if (e.shiftKey) {
							var startPos;
							var endPos;
							if ($(this).index() < lastClicked)
							{
								startPos = $(this).index();
								endPos = lastClicked;
							}
							else
							{
								startPos = lastClicked;
								endPos = $(this).index();
							}
							$(this).siblings().slice(startPos, endPos).addClass("selected");
						}
						lastClicked = $(this).index();
					}).sortable({
						connectWith: ['#ungroupedCustomers'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#ungroupedCustomers")) {
									dataArray[i] = {};
									dataArray[i]["customerID"] = $(item).attr('id');
									dataArray[i]["groupID"] = $("#groupID").attr("value");
									dataArray[i]["cmd"] = "delete";
									console.log(dataArray);
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_customergroupmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$("#ungroupedCustomers").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#groupedCustomers").children().removeClass('selected');
						}
						if (e.shiftKey) {
							var startPos;
							var endPos;
							if ($(this).index() < lastClicked)
							{
								startPos = $(this).index();
								endPos = lastClicked;
							}
							else
							{
								startPos = lastClicked;
								endPos = $(this).index();
							}
							$(this).siblings().slice(startPos, endPos).addClass("selected");
						}
						lastClicked = $(this).index();
					}).sortable({
						connectWith: ['#groupedCustomers'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#groupedCustomers")) {
									dataArray[i] = {};
									dataArray[i]["customerID"] = $(item).attr('id');
									dataArray[i]["groupID"] = $("#groupID").attr("value");
									dataArray[i]["cmd"] = "add";
									console.log(dataArray);
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_customergroupmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
					
			</script>
			<?php
		
			print("<form action='customer-groups.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='groupID' id='groupID' value='".$groupID."'/>");
				
				switch($strcmd)
				{
					case "viewGroup":
					
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
								print("<div class='section'>");
					
									$getGroupDetailsQuery = "SELECT * FROM customer_group WHERE recordID = :groupID";
									$strType = "single";
									$arrdbparam = array( "groupID" => $groupID );
									$groupDetails = query($conn, $getGroupDetailsQuery, $strType, $arrdbparam);

									print("<fieldset>");
										print("<legend>Edit Group ".$groupDetails['description']."</legend>");
						
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_groupdescription' class='control-label'>Group Name</label>");
												print("<input type='text' class='form-control' id='frm_groupdescription' name='frm_groupdescription' value='".$groupDetails['description']."' />");
											print("</div>");
										print("</div>");
									//	print("<div class='row'>");
									//		print("<div class='form-group'>");
									//			print("<div class='col-sm-8'>");
									//				print("<button onclick='return jseditGroup()' class='btn btn-success' type='submit'>Edit Group</button> ");
									//				print("<button onclick='return jscancel()' class='btn btn-primary' type='submit'>Return to All Groups</button>");
									//			print("</div>");
									//		print("</div>");
									//	print("</div>");
									
									print("</fieldset>");
								print("</div>");
								
								print("<div class='section'>");
									print("<fieldset>");
										print("<legend>Manage Customers in Group</legend>");
						
										print("<div class='row'>");
											print("<div class='form-group col-md-6 even-left'>");

												$ungroupedCustomersQuery = "SELECT customer.*, customer_address.postcode FROM customer INNER JOIN customer_address ON customer.defaultDeliveryAdd = customer_address.recordID WHERE groupID IS NULL ORDER BY customer.surname ASC";
												$strType = "multi";
												$ungroupedCustomers = query($conn, $ungroupedCustomersQuery, $strType);
											
												print("<label for='frm_ungroupedcustomers' class='control-label'>Ungrouped Customers</label>");

												print("<ul id='ungroupedCustomers' class='ui-sortable' style='overflow-x:hidden;overflow-y:scroll; height: 500px;'>");
													
													foreach($ungroupedCustomers AS $ungroupedCustomer)
													{
														//if(!empty($ungroupedCustomer['firstname']) && !empty($ungroupedCustomer['surname']))
														//{
															$strName = $ungroupedCustomer['firstname']." ".$ungroupedCustomer['surname'];
															if(!empty($ungroupedCustomer['title']))
															{
																$strName = $ungroupedCustomer['title']." ".$strName;
															}
															print("<li id='".$ungroupedCustomer['recordID']."'><span>".$strName." ".$ungroupedCustomer['postcode']."</span></li>");
														//}
													}
													
												print("</ul>");
												
											print("</div>");
											print("<div class='form-group col-md-6 even-right'>");
											
												$groupedCustomersQuery = "SELECT customer.*, customer_address.postcode FROM customer INNER JOIN customer_address ON customer.defaultDeliveryAdd = customer_address.recordID WHERE groupID = :groupID ORDER BY customer.surname ASC";
												$strType = "multi";
												$arrdbparam = array( "groupID" => $groupID );
												$groupedCustomers = query($conn, $groupedCustomersQuery, $strType, $arrdbparam);
								
												print("<label for='frm_groupedcustomers' class='control-label'>Grouped Customers</label>");
									
												print("<ul id='groupedCustomers' class='ui-sortable' style='overflow-x:hidden;overflow-y:scroll; height: 500px;'>");
													
													foreach($groupedCustomers AS $groupedCustomer)
													{
														//if(!empty($groupedCustomer['firstname']) && !empty($groupedCustomer['surname']))
														//{
															$strName = $groupedCustomer['firstname']." ".$groupedCustomer['surname'];
															if(!empty($groupedCustomer['title']))
															{
																$strName = $groupedCustomer['title']." ".$strName;
															}
															print("<li id='".$groupedCustomer['recordID']."'><span>".$strName." ".$groupedCustomer['postcode']."</span></li>");
														//}
													}
													
												print("</ul>");
								
											print("</div>");
										print("</div>");
							
									print("</fieldset>");
								print("</div>");
							
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
						
								print("<div class='row'>");
									print("<div class='form-group col-xs-6' style='text-align:left;padding-left:15px;padding-right:15px;'>");
									
										print("<button onclick='return jscancel()' class='btn btn-primary circle' type='submit' style='display:inline-block;'><i class='fa fa-undo'></i></button>");
									
									print("</div>");
									print("<div class='form-group col-xs-6' style='text-align:right;padding-right:15px;padding-left:15px;'>");
									
										print("<button onclick='return jseditGroup()' class='btn btn-success' type='submit' style='display:inline-block;'>Edit</button> ");
									
									print("</div>");
								print("</div>");
								
							print("</div>");
						print("</div>");
						
					break;
						
					default:
						
						$getCustomerGroupsQuery = "SELECT * FROM customer_group";
						$strType = "multi";
						$customerGroups = query($conn, $getCustomerGroupsQuery, $strType);
						
						print("<div class='row'>");
							print("<div class='col-md-6' style='padding-right:7.5px;'>");
								
								print("<div class='section'>");
									print("<table class='table table-striped table-bordered table-hover table-condensed' id='groups-table'>");
										print("<thead>");
											print("<th width='85%'>Group Name</th>");
											print("<th width='15%' style='text-align:center;'>Update</th>");
										print("</thead>");
										print("<tbody>");
											foreach($customerGroups AS $customerGroup)
											{
												print("<tr>");
													print("<td>".$customerGroup['description']."</td>");
													print("<td style='text-align:center;'><button onclick='return jsviewGroup(\"".$customerGroup['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");
												print("</tr>");
											}
										print("</tbody>");
									print("</table>");
								print("</div>");
						
								print("<div class='section'>");
									print("<fieldset>");
										print("<legend style='position:relative;'>Add New Group <button onclick='return jsinsertGroup()' class='btn btn-success circle' type='submit' style='position:absolute;top:8px;right:16px;'><i style='color:#FFFFFF;' class='fa fa-plus'></i></button></legend>");
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_groupdescription' class='control-label'>Group Name</label>");
												print("<input type='text' class='form-control' id='frm_groupdescription' name='frm_groupdescription' />");
											print("</div>");
										print("</div>");
									print("</fieldset>");
								print("</div>");
								
							print("</div>");
						print("</div>");
								
					break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#groups-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>
