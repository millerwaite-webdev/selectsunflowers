<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * sales-orders.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
	//=====================================================//
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "sales-orders"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	function fnSendOrderFeedbackEmail($orderHeaderID, $companyEmail, $companyPhone, $companyName, $strsiteurl, $strlogo, $conn){
		
		$getOrderHeaderQuery = "SELECT * FROM order_header WHERE recordID = :orderHeaderID";
		$strType = "single";
		$arrdbparam = array("orderHeaderID" => $orderHeaderID);
		$orderHeader = query($conn, $getOrderHeaderQuery, $strType, $arrdbparam);
		
		
		$orderTimestamp = $orderHeader['timestampOrder'];
		$strbname = $orderHeader['addBillingTitle']." ".$orderHeader['addBillingFirstname']." ".$orderHeader['addBillingSurname'];
		$strbadd1 = $orderHeader['addBilling1'];
		$strbadd2 = $orderHeader['addBilling2'];
		$strbadd3 = $orderHeader['addBilling3'];
		$strbadd4 = $orderHeader['addBilling4'];
		$strbadd5 = $orderHeader['addBilling5'];
		$strbpostcode = $orderHeader['addBillingPostcode'];
		$strname = $orderHeader['addDeliveryTitle']." ".$orderHeader['addDeliveryFirstname']." ".$orderHeader['addDeliverySurname'];
		$stradd1 = $orderHeader['addDelivery1'];
		$stradd2 = $orderHeader['addDelivery2'];
		$stradd3 = $orderHeader['addDelivery3'];
		$stradd4 = $orderHeader['addDelivery4'];
		$stradd5 = $orderHeader['addDelivery5'];
		$strpostcode = $orderHeader['addDeliveryPostcode'];
		$stremail = $orderHeader['email'];
		$strtel = $orderHeader['telephone'];
		
		$strcusttitle = 'Order Feedback Request';
		$stradmintitle = 'Order Feedback Request';
		$strmessagehead = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>";
		$strmessagehead = $strmessagehead."<html><head><title></title></head><body><a href=''><img border=0 src='https://".$strsiteurl.$strlogo."' /></a><br/><br/><br/>";

		$strmessageheadcust = $strmessagehead."<p><b>Feeback</b></p>";
		$strmessageheadcust = $strmessageheadcust."<p>You recently placed on order on our system, if you could take a minute to provide some feedback it would be greatly appreciated. You can do this by emailing $companyEmail, or if you would prefer to speak to someone on the phone you can call $companyPhone.</p>";
		$strmessageheadcust = $strmessageheadcust."<p>Your order details are shown below.</p>";

		// send notification email to Stollers
		$strmessageheadadmin = $strmessagehead."<p><b>Feedback Request</b></p>";
		$strmessageheadadmin = $strmessageheadadmin."<p>Email sent requesting feedback.</p>";

		//confirm basic details
		$strmessage = "<table width='800px'>";
		$strmessage = $strmessage."<tr><td>Order Number</td><td>$orderHeaderID</td></tr>";
		$strmessage = $strmessage."<tr><td>Order Date</td><td>".date("d/m/Y",$orderTimestamp)."</td></tr>";
		$strmessage = $strmessage."<tr><td>Expected Despatch Date</td><td>".date("d/m/Y",$orderHeader['dispatchDate'])."</td></tr>";
		$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";

		//Confirm delivery details
		$strmessage = $strmessage."<tr><td>Billing Address</td><td>$strbname</td></tr>";
		if (!empty($strbadd1)) $strmessage = $strmessage."<tr><td></td><td>$strbadd1</td></tr>";
		if (!empty($strbadd2)) $strmessage = $strmessage."<tr><td></td><td>$strbadd2</td></tr>";
		if (!empty($strbadd3)) $strmessage = $strmessage."<tr><td></td><td>$strbadd3</td></tr>";
		if (!empty($strbadd4)) $strmessage = $strmessage."<tr><td></td><td>$strbadd4</td></tr>";
		if (!empty($strbadd5)) $strmessage = $strmessage."<tr><td></td><td>$strbadd5</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strbpostcode</td></tr>";
		$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
		$strmessage = $strmessage."<tr><td>Delivery Address</td><td>$strname</td></tr>";
		if (!empty($stradd1)) $strmessage = $strmessage."<tr><td></td><td>$stradd1</td></tr>";
		if (!empty($stradd2)) $strmessage = $strmessage."<tr><td></td><td>$stradd2</td></tr>";
		if (!empty($stradd3)) $strmessage = $strmessage."<tr><td></td><td>$stradd3</td></tr>";
		if (!empty($stradd4)) $strmessage = $strmessage."<tr><td></td><td>$stradd4</td></tr>";
		if (!empty($stradd5)) $strmessage = $strmessage."<tr><td></td><td>$stradd5</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strpostcode</td></tr>";
		$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
		$strmessage = $strmessage."</table><br/><br/>";

		//order break down
		$strmessage = $strmessage."<table width='800px' style='text-align:left'>";
		$strmessage = $strmessage."<thead><tr><th>Stock Code</th><th>Product Name</th><th>Quantity</th><th>Price</th></tr></thead>";
		$strmessage = $strmessage."<tbody>";
		
		//$getOrderItemsQuery = "SELECT recordID, orderHeaderID, stockID, stockDetailsArray, quantity, price FROM order_items".$strtable." WHERE orderHeaderID = :orderHeaderID ORDER BY recordID";
		$strdbsql = "SELECT order_items.*, site_vat.amount, order_dispatch.description FROM order_items INNER JOIN site_vat ON order_items.vatID = site_vat.recordID INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID WHERE orderHeaderID = :recordID AND display = 1";
		$strType = "multi";
		$arrdbparams = array("recordID"=>$orderHeaderID);
		$orderItems = query($conn,$strdbsql,$strType,$arrdbparams);

		foreach ($orderItems AS $orderItem)
		{
			$stockItemDetails = unserialize($orderItem['stockDetailsArray']);
			$intPrice = $orderItem['quantity'] * $orderItem['price'];
			$strmessage = $strmessage."<tr>";
			$strmessage = $strmessage."<td>".$stockItemDetails['stockCode']."</td>";
			$strmessage = $strmessage."<td>".$stockItemDetails['name']."</td>";
			$strmessage = $strmessage."<td>".$orderItem['quantity']."</td>";
			$strmessage = $strmessage."<td>&pound;&nbsp;".number_format($intPrice,2)."</td>";
			$strmessage = $strmessage."</tr>";
			$intitems = $intitems + $orderItem['quantity'];
		}
		$strmessage = $strmessage."</tbody>";
		$strmessage = $strmessage."</table><br/><br/>";
		$strmessage = $strmessage."</div><!--end of order-->";
		$strmessage = $strmessage."</div>";

		//what to do if there is an error
		$strmessagefootcust = "<p>Thank you</p>";
		$strmessagefootcust = $strmessagefootcust."</body></html>";

		$strmessagefootadmin = "<p>If there is a problem with this order the customers contact details are:</p>";
		$strmessagefootadmin = $strmessagefootadmin."<center><h2>$strtel</h2>";
		$strmessagefootadmin = $strmessagefootadmin."<h2>$stremail</h2></center>";
		$strmessagefootadmin = $strmessagefootadmin."</body></html>";

		//customer email $stremail
		if(mail("$stremail", $strcusttitle, $strmessageheadcust.$strmessage.$strmessagefootcust, "MIME-Version: 1.0".PHP_EOL."X-Mailer: PHP/" . phpversion().PHP_EOL."Sensitivity: Personal".PHP_EOL."Return-Path:$strsiteurl".PHP_EOL."From: $companyName <$companyEmail>".PHP_EOL."Content-Type: text/html; charset=iso-8859-1"))
		{
			$recordOrderEmailConfirmationQuery = "UPDATE order_header SET sentContactFeedback = 1 WHERE recordID = :orderHeaderID";
			$strType = "update";
			$arrdbparam = array("orderHeaderID" => $orderHeaderID);
			$recordOrderEmailConfirmation = query($conn, $recordOrderEmailConfirmationQuery, $strType, $arrdbparam);
		}
		mail($companyEmail, $stradmintitle, $strmessageheadadmin.$strmessage.$strmessagefootadmin, "MIME-Version: 1.0".PHP_EOL."X-Mailer: PHP/" . phpversion().PHP_EOL."Sensitivity: Personal".PHP_EOL."Return-Path:$strsiteurl".PHP_EOL."From: $companyName <$companyEmail>".PHP_EOL."Content-Type: text/html; charset=iso-8859-1");

	}
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['customerID'])) $customerID = $_REQUEST['customerID']; else $customerID = "";
	if (isset($_REQUEST['userID'])) $customerID = $_REQUEST['userID'];
	if (isset($_REQUEST['orderID'])) $orderID = $_REQUEST['orderID']; else $orderID = "";
	if (isset($_REQUEST['frm_fromdate'])) {
		$fromdate = strtotime(str_replace("/", "-", $_REQUEST['frm_fromdate']));
	} else {
		$fromdate = strtotime( '-1 month', $datnow );
	}
	if (isset($_REQUEST['frm_todate'])) {
		$todate = strtotime(str_replace("/", "-", $_REQUEST['frm_todate']));
		$todate = strtotime("tomorrow", $todate) - 1;
	} else {
		$todate = $datnow;
	}
	
	error_log($orderID);
	error_log($strcmd);
	
	switch($strcmd)
	{	
		case "feedbackEmail":
		
			if(!empty($orderID)) {
				
				$strsuccess = "Email sent to customer asking for feedback.";
				fnSendOrderFeedbackEmail($orderID, $companyEmail, $companyPhone, $companyName, $strsiteurl, $strlogo, $conn);
				
			} else {
				$strerror = "Order ID not found please try again";
				$strcmd = "";
			}
		
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Sale Orders</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script type='text/javascript'>
				function fnLetterForFeedback(orderID){
					if ($('#form').valid()) {
						window.open("/admin/invoiceprint.php?print=pdf&documentType=feedbackRequest&orderHeaderID="+orderID); 
						$('#feedbackButtonLetter'+orderID).hide();
					}
				}
				
				function fnLoadOrders(){
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "";
						document.getElementById("orderID").value = "";
						document.getElementById("form").submit();
					}
				}
				
				function fnEmailForFeedback(orderID){
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "feedbackEmail";
						document.getElementById("orderID").value = orderID;
						$('#feedbackButtonEmail'+orderID).hide();
						document.getElementById("form").submit();
					}
				}
				
				function jsprintinvoice (orderHeaderID) {
					window.open("/admin/invoiceprint.php?print=pdf&orderHeaderID="+orderHeaderID); 
				}	
			</script>
			<?php
		
			print ("<form action='sales-orders.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='customerID' id='customerID' value='$customerID'/>");
				print ("<input type='hidden' name='orderID' id='orderID' value=''/>");

				switch($strcmd)
				{
					default:
					
						print ("<div class='cwcontainer' style=''>");
								print ("<div class='form-group'>");
									print ("<div class='left'>");
										print ("<label style='width:100px;' for='frm_fromdate'>Orders From: </label>");
										print ("<input tabindex='1' type='text' value='".fnconvertunixtime($fromdate)."' name='frm_fromdate' id='frm_fromdate' maxlength='80' style='width:150px;' size='35' maxlength='50' class='form-control' />");
									print ("</div>");
									print ("<div class='left' style='margin-left: 10px;'>");
										print ("<label style='width:100px;' for='frm_todate' class=''>Orders To: </label>");
										print ("<input tabindex='2' type='text' value='".fnconvertunixtime($todate)."' name='frm_todate' id='frm_todate' maxlength='80' style='width:150px;' size='35' maxlength='50' class='form-control' />");
									print ("</div>");
									print ("<div class='left' style='margin-left: 10px;'>");
										print ("<button style='margin-top: 25px;' onclick='return fnLoadOrders();' type='submit' class='btn btn-success '>Load</button>");
									print ("</div>");
								print ("</div>");
						print ("</div>");
					
						
						if (!empty($customerID)){
							$orderDetailsQuery = "SELECT order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_status.description AS orderStatus FROM order_header INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID WHERE order_header.timestampOrder >= :fromdate AND order_header.timestampOrder < :todate AND (customer.recordID = :customerID OR addDeliveryCustomerID = :customerID2) ORDER BY order_header.timestampOrder, order_header.recordID DESC";
							$arrdbparam = array("customerID"=>$customerID,"customerID2"=>$customerID,"fromdate"=>$fromdate,"todate"=>$todate);
						} else {
							$orderDetailsQuery = "SELECT order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_status.description AS orderStatus FROM order_header INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID WHERE order_header.timestampOrder >= :fromdate AND order_header.timestampOrder < :todate ORDER BY order_header.timestampOrder, order_header.recordID DESC";
							$arrdbparam = array("fromdate"=>$fromdate,"todate"=>$todate);
						}
						$strType = "multi";
						$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparam);

						print("<div class='section'>");
							print ("<table class='order-list table table-striped table-bordered table-hover table-condensed' >");
								print ("<thead><tr>");
									print ("<th>Order Reference</th>");
									print ("<th>Order Date</th>");
								//	print ("<th>Sub-Total</th>");
								//	print ("<th>VAT</th>");
									print ("<th>Total</th>");
								//	print ("<th>Amount Paid</th>");
									print ("<th>Order Status</th>");
									print ("<th>Despatch Details</th>");
									print ("<th>Billing Details</th>");
									print ("<th style='text-align:center;'>View Order</th>");
								//	print ("<th style='text-align:center;'>Feedback</th>");
									print ("<th style='text-align:center;'>Print</th>");
								print ("</tr></thead><tbody>");
								foreach($ordersDetails AS $orderDetails)
								{
									//reset values
									$subTotalAmount = 0;
									$vatAmount = 0;
									$totalAmount = 0;
										
									//get item details
									$strdbsql = "SELECT order_items.*, site_vat.amount FROM order_items INNER JOIN site_vat ON order_items.vatID = site_vat.recordID WHERE orderHeaderID = :recordID";
									$strType = "multi";
									$arrdbparam = array("recordID"=>$orderDetails['recordID']);
									$orderItems = query($conn, $strdbsql, $strType, $arrdbparam);
									
									foreach($orderItems AS $orderItem){
										$lineAmount = number_format($orderItem['price']*$orderItem['quantity'],2);
										if ($orderItem['amount'] == 0) {
											$lineTax = "0.00";
										} else {
											$lineTax = number_format(($lineAmount / 100) * $orderItem['amount'],2);
										}
										$lineTotal = number_format($lineAmount + $lineTax,2);
										$subTotalAmount += $lineAmount;
										$vatAmount += $lineTax;
										$totalAmount += $lineTotal;
									}
									
									$deliveryDetails = $orderDetails['addDeliveryTitle']." ".$orderDetails['addDeliveryFirstname']." ".$orderDetails['addDeliverySurname']." - ".$orderDetails['addDelivery1']." ".$orderDetails['addDeliveryPostcode'];
									$billingDetails = $orderDetails['addBillingTitle']." ".$orderDetails['addBillingFirstname']." ".$orderDetails['addBillingSurname']." - ".$orderDetails['addBilling1']." ".$orderDetails['addBillingPostcode'];
									
									print ("<tr>");
									print ("<td>".$orderDetails['recordID']."</td>");
									print ("<td>".fnconvertunixtime($orderDetails['timestampOrder'])."</td>");
								//	print ("<td>&pound;".number_format($subTotalAmount,2)."</td>");
								//	print ("<td>&pound;".number_format($vatAmount,2)."</td>");
									print ("<td>&pound;".$orderDetails['amountTotal']."</td>");
								//	print ("<td>&pound;".number_format($orderDetails['paymentAmount'],2)."</td>");
									print ("<td>".$orderDetails['orderStatus']."</td>");
									print ("<td>".$deliveryDetails."</td>");
									print ("<td>".$billingDetails."</td>");
									print ("<td style='text-align:center;'><a style='display:inline-block;' href='/admin/sales-order-create.php?orderID=".$orderDetails['recordID']."' class='btn btn-primary circle'><i class='fa fa-eye'></i></a></td>");
								/*	if ($orderDetails['timestampOrder'] < strtotime("-4 weeks") AND $orderDetails['sentContactFeedback'] == 0){
										if (!empty($orderDetails['email'])) print ("<td style='text-align:center;'><a id='feedbackButtonEmail".$orderDetails['recordID']."' style='display:inline-block;' href='#' onclick='fnEmailForFeedback(".$orderDetails['recordID']."); return false;' class='btn btn-primary circle'><i class='fa fa-comment'></i></a></td>");
										else print ("<td style='text-align:center;'><a id='feedbackButtonLetter".$orderDetails['recordID']."' style='display:inline-block;' href='#' onclick='fnLetterForFeedback(".$orderDetails['recordID']."); return false;' class='btn btn-default circle'><i class='fa fa-comment'></i></a></td>");
									} else {
										print ("<td style='text-align:center;'></td>");
									}*/
									print ("<td style='text-align:center;'><a style='display:inline-block;' onclick='return jsprintinvoice(".$orderDetails['recordID'].");' class='btn btn-primary circle'><i class='fa fa-print'></i></a></td>");
									print ("</tr>");
								}
								print ("</tbody>");
							print ("</table>");
						print ("</div>");
						
						if (!empty($customerID)){

							print ("<a style='float:left' href='/customer-manage.php?cmd=viewDetails&userID=$customerID' class='btn btn-success '>View Customer</button>");

							print ("<a style='margin-left: 10px; float:left' href='/customer-manage.php' class='btn btn-success '>Customer Search</button>");
							print ("<a style='margin-left: 10px; float:right' href='/admin/sales-order-create.php?customerIDPassed=$customerID' class='btn btn-success '>Add New Order</button>");

						}
					break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script type='text/javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('.order-list').DataTable({"bStateSave": true,"order": [[ 0, "desc" ],[1,"desc"]]});
			
			$('#frm_fromdate').datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true, 
				changeYear: true
			});
			$('#frm_todate').datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true, 
				changeYear: true
			});
			
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>