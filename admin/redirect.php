<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * index.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	/*$sectionArr = explode("_", $_GET['page']);
	unset($sectionArr[0]);
	$sectionStr = implode("_", $sectionArr);*/
	
	$sectionStr = $_GET['page'];
	
	$strType = "single";
	$arrdbparam = array( "metaPageLink" => $sectionStr );
	

	$conn = null; // close the database connection after all processing
?>