<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * config-letters.php                                      * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "admin-operators"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['operatorID'])) $operatorID = $_REQUEST['operatorID']; else $operatorID = "";
	switch($strcmd)
	{
		case "saveNewOperator":
		
			//all users now go into the global account - usernames have to be unique
			$strdbsql = "SELECT * FROM admin_operator WHERE username = :username";
			$arrType = "count";
			$strdbparams = array("username" => $_REQUEST['frm_username']);
			$operatorCheck = query($conn, $strdbsql, $arrType, $strdbparams);
			
			if ($operatorCheck == 0) {
			
				$strdbsql = "INSERT INTO admin_operator (username, password, firstname, surname, phonenumber, email, permissionGroup, enabled, memorableInformation) VALUES (:username, :password, :firstname, :surname, :phonenumber, :email, :permissionGroup, :enabled, :memorableInformation);";
				$arrType = "insert";
				$strdbparams = array("username" => $_REQUEST['frm_username'], "password" => md5($_REQUEST['frm_password']), "firstname" => $_REQUEST['frm_firstname'], "surname" => $_REQUEST['frm_surname'], "phonenumber" => $_REQUEST['frm_ddi'], "email" => $_REQUEST['frm_email'], "permissionGroup" => $_REQUEST['frm_permissionGroup'], "enabled" => $_REQUEST['frm_enabled'], "memorableInformation" => $_REQUEST['frm_meminfo']);
				$insertlocal = query($conn, $strdbsql, $arrType, $strdbparams);
				
				$strsuccess = "New operator created";
				$strcmd = "";

			} else {
				$strerror = "Username already exists please change";
				$strcmd = "createOperator";
			}

		
		break;	
		case "saveDetails":
		
			//all users now go into the global account - usernames have to be unique
			$strdbsql = "SELECT * FROM admin_operator WHERE username = :username";
			$arrType = "count";
			$strdbparams = array("username" => $_REQUEST['frm_username']);
			$operatorCheck = query($conn, $strdbsql, $arrType, $strdbparams);
			
			if ($operatorCheck == 0 || $_REQUEST['frm_username'] == $_REQUEST['operatorUsername']) {
		
				$strdbsql = "UPDATE admin_operator SET username = :newUsername, permissionGroup = :permissionGroup, enabled = :enabled, firstname = :firstname, surname = :surname, phonenumber = :phonenumber, email = :email, memorableInformation = :meminfo WHERE username = :username";
				$arrType = "update";
				$strdbparams = array("username" => $_REQUEST['operatorUsername'], "newUsername" => $_REQUEST['frm_username'], "permissionGroup" => $_REQUEST['frm_permissionGroup'], "enabled" => $_REQUEST['frm_enabled'], "firstname" => $_REQUEST['frm_firstname'], "surname" => $_REQUEST['frm_surname'], "phonenumber" => $_REQUEST['frm_ddi'], "email" => $_REQUEST['frm_email'], "meminfo" => $_REQUEST['frm_meminfo']);
				$resultupdate = query($conn, $strdbsql, $arrType, $strdbparams);
				$strsuccess = "Operator details successfully updated<br/>";
				$strcmd = "";
				
			} else {
				$strerror = "Username already exists please change";
				$strcmd = "editOperator";
			}
		
		break;
		case "savePassword":
		
			//update both global and local if applicable
			$strdbsql = "UPDATE admin_operator SET password = :password WHERE operatorID = :operatorID";
			$arrType = "update";
			$strdbparams = array("operatorID" => $_REQUEST['operatorID'], "password" => md5($_REQUEST["frm_password"]));
			$resultupdate = query($conn, $strdbsql, $arrType, $strdbparams);
			

			$strsuccess = "Successfully updated password<br/>";
			$strcmd = "";

		break;
	}
	
	
	// ************* Common page setup ******************* //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Operators</h1>");
		
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript' >

				function fnCreateOperator() {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "createOperator";
						document.getElementById("form").submit();
					}
				}
				
				function fnSaveNewOperator() {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "saveNewOperator";
						document.getElementById("form").submit();
					}
				}
				
				function fnEditOperator(operatorID) {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "editOperator";
						document.getElementById("operatorID").value = operatorID;
						document.getElementById("form").submit();
					}
				}
				
				function fnOperatorHistory(operatorID) {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "operatorHistory";
						document.getElementById("operatorID").value = operatorID;
						document.getElementById("form").submit();
					}
				}
				
				function fnSaveDetails(operatorID) {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "saveDetails";
						document.getElementById("operatorID").value = operatorID;
						document.getElementById("form").submit();
					}
				}
				
				function fnSavePass(operatorID) {
					if ($('#form').valid()) {
						document.getElementById("cmd").value = "savePassword";
						document.getElementById("operatorID").value = operatorID;
						document.getElementById("form").submit();
					}
				}
				
				function fnCancel(){
					document.getElementById("cmd").value = "";
					document.getElementById("operatorID").value = "";
					document.getElementById("form").submit();
				}
			</script>
			<?php


			print ("<form action='admin-operators.php' class='uniForm' method='post' name='form' id='form'>");

			// Hidden form objects to post data back to form for next step
			// ===========================================================
			print ("<input type='hidden' class='nodisable' name='operatorID' id='operatorID' value='$operatorID'/>");
			print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' />");

				switch ($strcmd) {
					
					
					case "createOperator":
						print ("<div class='section'>");
							
							
							print ("<h3>Add Operator</h3>");
							
							print ("<div class='row'><label for='frm_username' class='col-sm-2 control-label'>Username</label><div class='col-sm-2'>
							<input name='frm_username' id='frm_username' value='".$_REQUEST['frm_username']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
																
							print ("<div class='row'><label for='frm_firstname' class='col-sm-2 control-label'>Firstname</label><div class='col-sm-2'>
							<input name='frm_firstname' id='frm_firstname' value='".$_REQUEST['frm_firstname']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_surname' class='col-sm-2 control-label'>Surname</label><div class='col-sm-2'>
							<input name='frm_surname' id='frm_surname' value='".$_REQUEST['frm_surname']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_ddi' class='col-sm-2 control-label'>Direct Dial No.</label><div class='col-sm-2'>
							<input name='frm_ddi' id='frm_ddi' value='".$_REQUEST['frm_ddi']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_email' class='col-sm-2 control-label'>Email Address</label><div class='col-sm-2'>
							<input name='frm_email' id='frm_email' value='".$_REQUEST['frm_email']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
																
							print ("<div class='row'>
							<label for='frm_password' class='col-sm-2 control-label'>Password</label>
							<div class='col-sm-10'>
							  <input onkeyup='passwordStrength(this.value); return false;' style='width:200px;' type='password' class='form-control' id='frm_password' name='frm_password' placeholder='Password'>
							  <span class='help-block'><span id='strength' name='strength'></span>
							</div>
						  </div>
						  <div class='row'>
							<label for='frm_passrepeat' class='col-sm-2 control-label'>Repeat Password</label>
							<div class='col-sm-10'>
							  <input onkeyup='passMatch(); return false;' style='width:200px;' type='password' class='form-control' id='frm_passrepeat' name='frm_passrepeat' placeholder='Repeat Password'>
							  <span class='help-block'><span id='match' name='match'></span></span>
							</div>
						  </div>");
							
							print ("<div class='row'><label for='frm_meminfo' class='col-sm-2 control-label'>Memorable Information</label><div class='col-sm-2'>
							<input name='frm_meminfo' id='frm_meminfo' value='".$_REQUEST['frm_meminfo']."' style='width:120px;' size='35' maxlength='8' type='text' class='form-control' /></div></div>");
							
							//permission group
							print ("<div class='row'>");				
								print ("<label for='frm_permissionGroup' class='col-sm-2 control-label'>Permission Group:</label>");
								print ("<div class='col-sm-2'><select name='frm_permissionGroup' id='frm_permissionGroup' class='form-control' >");
									$strdbsql = "SELECT * FROM admin_permission_groups ORDER BY level ASC";
									$strType = "multi";
									$arrdbparams = array();
									$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
									foreach ($resultdata as $row) {
										if ($row['groupID'] == $_REQUEST['frm_permissionGroup']) $selected = "selected"; else $selected = "";
										print ("<option $selected value='".$row['groupID']."'>".$row['name']." (lvl ".$row['level'].")</option>");
									}
								print ("</select>");
								print ("</div>");
							print ("</div>");
							
							//enabled
							print ("<div class='row'>");				
								print ("<label for='frm_enabled' class='col-sm-2 control-label'>Enabled:</label>");
								print ("<div class='col-sm-2'><select name='frm_enabled' id='frm_enabled' class='form-control' >");
									if ($_REQUEST['frm_enabled'] == 1) $selected = "selected"; else $selected = "";
									print ("<option $selected value='1'>Enabled</option>");
									if ($_REQUEST['frm_enabled'] == 0) $selected = "selected"; else $selected = "";
									print ("<option $selected value='0'>Disabled</option>");
								print ("</select>");
								print ("</div>");
							print ("</div>");
							print("<br/>");
							print("<div class='row'>");
								print("<div class='col-xs-6' style='text-align:left;'>");
									print("<button onclick='return fnCancel();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-6 style='text-align:right;'>");
									print("<button onclick='return fnSaveNewOperator();' type='submit' class='btn btn-success right' style='display:inline-block;'>Add Operator</button> ");
								print("</div>");
							print("</div>");
								  
						print ("</div>");	
					break;		
					
					
					case "editOperator":
						print ("<div class='section'>");
						
							$strdbsql = "SELECT * FROM admin_operator WHERE operatorID = :operatorID";
							$arrType = "single";
							$strdbparams = array("operatorID" => $operatorID);
							$staffdetails = query($conn, $strdbsql, $arrType, $strdbparams);
							
							print ("<input type='hidden' class='nodisable' name='operatorUsername' id='operatorUsername' value='".$staffdetails['username']."'/>");
														
							print ("<h3>Account Details</h3>");
							
							print ("<div class='row'><label for='frm_username' class='col-sm-2 control-label'>Username</label><div class='col-sm-3'>
							<input name='frm_username' id='frm_username' value='".$staffdetails['username']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_firstname' class='col-sm-2 control-label'>Firstname</label><div class='col-sm-3'>
							<input name='frm_firstname' id='frm_firstname' value='".$staffdetails['firstname']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_surname' class='col-sm-2 control-label'>Surname</label><div class='col-sm-3'>
							<input name='frm_surname' id='frm_surname' value='".$staffdetails['surname']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_ddi' class='col-sm-2 control-label'>Direct Dial No.</label><div class='col-sm-3'>
							<input name='frm_ddi' id='frm_ddi' value='".$staffdetails['phonenumber']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_email' class='col-sm-2 control-label'>Email Address</label><div class='col-sm-3'>
							<input name='frm_email' id='frm_email' value='".$staffdetails['email']."' size='35' maxlength='50' type='text' class='form-control' /></div></div>");
							
							print ("<div class='row'><label for='frm_meminfo' class='col-sm-2 control-label'>Memorable Information</label><div class='col-sm-3'>
							<input name='frm_meminfo' id='frm_meminfo' value='".$staffdetails['memorableInformation']."' size='35' maxlength='8' type='text' class='form-control' /></div></div>");
							
							//permission group
							print ("<div class='row'>");				
								print ("<label for='frm_permissionGroup' class='col-sm-2 control-label'>Permission Group:</label>");
								print ("<div class='col-sm-4'><select name='frm_permissionGroup' id='frm_permissionGroup' class='form-control' >");
									$strdbsql = "SELECT * FROM admin_permission_groups ORDER BY level ASC";
									$strType = "multi";
									$arrdbparams = array();
									$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
									foreach ($resultdata as $row) {
										if ($row['groupID'] == $staffdetails['permissionGroup']) $selected = "selected"; else $selected = "";
										print ("<option $selected value='".$row['groupID']."'>".$row['name']." (lvl ".$row['level'].")</option>");
									}
								print ("</select>");
								print ("</div>");
							print ("</div>");
							
							//enabled
							print ("<div class='row'>");				
								print ("<label for='frm_enabled' class='col-sm-2 control-label'>Enabled:</label>");
								print ("<div class='col-sm-4'><select name='frm_enabled' id='frm_enabled' class='form-control' >");
									if ($staffdetails['enabled'] == 1) $selected = "selected"; else $selected = "";
									print ("<option $selected value='1'>Enabled</option>");
									if ($staffdetails['enabled'] == 0) $selected = "selected"; else $selected = "";
									print ("<option $selected value='0'>Disabled</option>");
								print ("</select>");
								print ("</div>");
							print ("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-6' style='text-align:left;'>");
									print("<button onclick='return fnCancel();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-6 style='text-align:right;'>");
									print("<button onclick='return fnSaveDetails($operatorID);' type='submit' class='btn btn-success right' style='display:inline-block;'>Save Details</button> ");
								print("</div>");
							print("</div>");
						print("</div>");
						print ("<div class='section'>");	
							print ("<h3>Change Password</h3>");
							print ("<div class='row'>
								<label for='frm_password' class='col-sm-2 control-label'>Password</label>
								<div class='col-sm-3'>
								  <input onkeyup='passwordStrength(this.value); return false;' type='password' class='form-control' id='frm_password' name='frm_password' placeholder='Password'>
								  <span class='help-block'><span id='strength' name='strength'></span>
								</div>
							  </div>
							  <div class='row'>
								<label for='frm_passrepeat' class='col-sm-2 control-label'>Repeat Password</label>
								<div class='col-sm-3'>
								  <input onkeyup='passMatch(); return false;' type='password' class='form-control' id='frm_passrepeat' name='frm_passrepeat' placeholder='Repeat Password'>
								  <span class='help-block'><span id='match' name='match'></span></span>
								</div>
							  </div>");
						
							print("<div class='row'>");
								print("<div class='col-xs-6' style='text-align:left;'>");
									print("<button onclick='return fnCancel();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-6 style='text-align:right;'>");
									print("<button onclick='return fnSavePass($operatorID);' type='submit' class='btn btn-success right' style='display:inline-block;'>Update</button> ");
								print("</div>");
							print("</div>");	
						print ("</div>");		  
					break;		

					case "operatorHistory":

					break;
					
					default:
						print ("<div class='section'>");
							print("<div class='row'>");
							print ("<div class='col-sm-12'>");
							print ("<table class='table table-striped table-bordered table-hover table-condensed' ><thead>");
							print ("<tr>");
								print ("<th>Username</th>");
								print ("<th>Name</th>");
								print ("<th>Phone Number</th>");
								print ("<th>Email</th>");
								print ("<th>Permission Group</th>");
								print ("<th>&nbsp;</th>");
								//print ("<th>&nbsp;</th>");
							print ("</tr></thead><tbody>");
							
							$strdbsql = "SELECT * FROM admin_operator ORDER BY enabled DESC";
							$strType = "multi";
							$arrdbparams = array();
							$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

							if (count($resultdata) > 0) {
								foreach ($resultdata as $row) {
									print ("<tr>");
									
										$strdbsql = "SELECT * FROM admin_permission_groups WHERE groupID = :permissionGroup";
										$strType = "single";
										$arrdbparams = array("permissionGroup"=>$row['permissionGroup']);
										$configPermissionGroups = query($conn, $strdbsql, $strType, $arrdbparams);
										
										if ($row['enabled'] == 0) $tablecolour = "danger";
										else $tablecolour = "success";
										print ("<td class='$tablecolour'>".$row['username']."</td>");
										print ("<td class='$tablecolour'>".$row['firstname']." ".$row['surname']."</td>");
										print ("<td class='$tablecolour'>".$row['phonenumber']."</td>");
										print ("<td class='$tablecolour'>".$row['email']."</td>");
										print ("<td class='$tablecolour'>".$configPermissionGroups['name']." (lvl ".$configPermissionGroups['level'].")</td>");
										print("<td class='$tablecolour'><button onclick='fnEditOperator(".$row['operatorID']."); return false;' class='btn btn-primary circle' type='submit' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");									
									print ("</tr>");
								}
							} else {
								print ("<tr><td colspan='7'>There are no existing operators</td></tr>");
							}
							print ("</tbody></table>");
							print ("</div>");
							print ("</div>");
							print("<div class='row'>");
								print ("<div class='col-sm-12'>");
									print ("<button class='btn btn-success right' style='margin-left:10px;' onclick='fnCreateOperator(); return false;'>New</button>");
								print ("</div>");
							print ("</div>");

						print ("</div><br/>");	
					break;
				}

			print ("</form>");
		print("</div>");
	print("</div>");
	
	?>
	<script language='Javascript'>
	$().ready(function() {
		// validate signup form on keyup and submit
		$("#form").validate({
			errorPlacement: function(error,element) {
				error.insertAfter(element);
			},
			rules: {
				frm_firstname: {
					required: true
				},
				frm_surname: {
					required: true
				},
				frm_meminfo: {
					required: true,
					minlength: 8
				}
			}
		});
	});
	
	function passMatch()
	{
		var status = "<span style='color: #ff0000'>Passwords Don't Match</span>";
		if (document.getElementById("frm_password").value == document.getElementById("frm_passrepeat").value ) status = "<span style='color: #228822'>Passwords Match</span>";
		document.getElementById("match").innerHTML = status;
	}
	</script>
	<?php
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null;

?>