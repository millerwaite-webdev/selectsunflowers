<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * promotions-catalogue-postage-rules.php	                           * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "promotions-catalogue-postage-rules"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['ruleID'])) $ruleID = $_REQUEST['ruleID']; else $ruleID = "";
	if (isset($_REQUEST['groupID'])) $groupID = $_REQUEST['groupID']; else $groupID = "";
	
	switch($strcmd) {
		case "save_new_rule":
			if(!isset($_POST['frm_localOnly'])) {
				$_POST['frm_localOnly'] = 0;
			}
			$addPostageRuleQuery = "INSERT INTO site_postage (description, cost, minspend, isActive, isLocalDeliveryOnly, isGlobal, costModifier, modifierThreshold, modifierCost) 
			VALUES (:description, :cost, :minSpend, :isActive, :localOnly, :isGlobal, :costModifier, :modifierThreshold, :modifierCost)";
			$strType = "insert";
			$arrdbparams = array(
								"description" => $_POST['frm_description'],
								"cost" => $_POST['frm_postageCost'],
								"minSpend" => $_POST['frm_minSpend'],
								"isActive" => 0,
								"localOnly" => $_POST['frm_localOnly'],
								"isGlobal" => $_POST['frm_global'],
								"costModifier" => $_POST['frm_costModifier'],
								"modifierThreshold" => $_POST['frm_modifierThreshold'],
								"modifierCost" => $_POST['frm_modifierCost']
							);
			query($conn, $addPostageRuleQuery, $strType, $arrdbparams);
			$strcmd = "";
			break;
		case "add_new_group":
			$addPostageGroupQuery = "INSERT INTO site_postage_group (description) VALUES (:groupName)";
			$strType = "insert";
			$arrdbparam = array("groupName" => $_POST['frm_groupname']);
			$groupID = query($conn, $addPostageGroupQuery, $strType, $arrdbparam);
			$strcmd = "";
			break;
		case "save_updated_rule":
			if(!isset($_POST['frm_enabled'])) {
				$_POST['frm_enabled'] = 0;
			}
			if(!isset($_POST['frm_localOnly'])) {
				$_POST['frm_localOnly'] = 0;
			}
			$updatePostageRuleQuery = "UPDATE site_postage SET description = :description, cost = :cost, minspend = :minspend, isActive = :isActive, isLocalDeliveryOnly = :localOnly, isGlobal = :isGlobal, costModifier = :costModifier, modifierThreshold = :modifierThreshold, modifierCost = :modifierCost WHERE recordID = :ruleID";
			$strType = "update";
			$arrdbparams = array(
								"description" => $_POST['frm_description'],
								"cost" => $_POST['frm_postageCost'],
								"minspend" => $_POST['frm_minSpend'],
								"isActive" => $_POST['frm_enabled'],
								"localOnly" => $_POST['frm_localOnly'],
								"isGlobal" => $_POST['frm_global'],
								"costModifier" => $_POST['frm_costModifier'],
								"modifierThreshold" => $_POST['frm_modifierThreshold'],
								"modifierCost" => $_POST['frm_modifierCost'],
								"ruleID" => $ruleID
							);
			query($conn, $updatePostageRuleQuery, $strType, $arrdbparams);
			$strcmd = "update_rule";
			break;
		case "save_postage_group":
			$updatePostageRuleGroupQuery = "UPDATE site_postage_group SET description = :groupDesc WHERE recordID = :groupID";
			$strType = "update";
			$arrdbparams = array(
								"groupDesc" => $_POST['frm_groupname'],
								"groupID" => $groupID
							);
			query($conn, $updatePostageRuleGroupQuery, $strType, $arrdbparams);
			$strcmd = "update_group";
			break;
		case "delete_rule":
			$deletePostageRuleQuery = "DELETE FROM site_postage WHERE recordID = :postageID";
			$strType = "delete";
			$arrdbparam = array("postageID" => $ruleID);
			query($conn, $deletePostageRuleQuery, $strType, $arrdbparam);
			$strcmd = "";
			break;
		case "delete_group":
			$deletePostageRuleGroupQuery = "DELETE FROM site_postage_group WHERE recordID = :groupID";
			$strType = "delete";
			$arrdbparam = array("groupID" => $groupID);
			query($conn, $deletePostageRuleGroupQuery, $strType, $arrdbparam);
			$strcmd = "";
			break;
		case "add_rules":
			foreach($_POST['frm_ungroupedrules'] AS $ungroupedrule)
			{
				$updatePostageRuleGroupQuery = "UPDATE site_postage SET groupID = :groupID WHERE recordID = :ruleID";
				$strType = "update";
				$arrdbparams = array(
									"groupID" => $groupID,
									"ruleID" => $ungroupedrule
								);
				query($conn, $updatePostageRuleGroupQuery, $strType, $arrdbparams);
			}
			$strcmd = "update_group";
			break;
		case "remove_rules":
			foreach($_POST['frm_groupedrules'] AS $groupedrule)
			{
				$updatePostageRuleGroupQuery = "UPDATE site_postage SET groupID = NULL WHERE recordID = :ruleID";
				$strType = "update";
				$arrdbparams = array(
									"ruleID" => $groupedrule
								);
				query($conn, $updatePostageRuleGroupQuery, $strType, $arrdbparams);
			}
			$strcmd = "update_group";
			break;
		case "add_ranges":
		case "remove_ranges":
			$getPostageRuleRangesQuery = "SELECT rangeIDList FROM site_postage WHERE recordID = :ruleID";
			$strType = "single";
			$arrdbparams = array(
								"ruleID" => $ruleID
							);
			$postageRuleRanges = query($conn, $getPostageRuleRangesQuery, $strType, $arrdbparams);
			switch($strcmd) {
				case "add_ranges":
					if(!empty($_POST['frm_ranges'])) {
						if(strlen($postageRuleRanges['rangeIDList']) > 0) {
							$newranges = $postageRuleRanges['rangeIDList'] . "," . implode(",", $_POST['frm_ranges']);
						} else {
							$newranges = implode(",", $_POST['frm_ranges']);
						}
					}
					break;
				case "remove_ranges":
					if(!empty($_POST['frm_rangesInOffer'])) {
						$newranges = implode(",", array_diff(explode(",", $postageRuleRanges['rangeIDList']), $_POST['frm_rangesInOffer']));
					}
					break;
			}
			$updatePostageRuleQuery = "UPDATE site_postage SET rangeIDList = :rangeList WHERE recordID = :ruleID";
			$strType = "update";
			$arrdbparams = array(
								"rangeList" => $newranges,
								"ruleID" => $ruleID
							);
			query($conn, $updatePostageRuleQuery, $strType, $arrdbparams);
			$strcmd = "update_rule";
			break;
		case "add_brands":
		case "remove_brands":
			$getPostageRuleBrandsQuery = "SELECT brandIDList FROM site_postage WHERE recordID = :ruleID";
			$strType = "single";
			$arrdbparams = array(
								"ruleID" => $ruleID
							);
			$postageRuleBrands = query($conn, $getPostageRuleBrandsQuery, $strType, $arrdbparams);
			switch($strcmd) {
				case "add_brands":
					if(!empty($_POST['frm_brands'])) {
						if(strlen($postageRuleBrands['brandIDList']) > 0) {
							$newbrands = $postageRuleBrands['brandIDList'] . "," . implode(",", $_POST['frm_brands']);
						} else {
							$newbrands = implode(",", $_POST['frm_brands']);
						}
					}
					break;
				case "remove_brands":
					if(!empty($_POST['frm_brandsInOffer'])) {
						$newbrands = implode(",", array_diff(explode(",", $postageRuleBrands['brandIDList']), $_POST['frm_brandsInOffer']));
					}
					break;
			}
			$updatePostageRuleQuery = "UPDATE site_postage SET brandIDList = :brandList WHERE recordID = :ruleID";
			$strType = "update";
			$arrdbparams = array(
								"brandList" => $newbrands,
								"ruleID" => $ruleID
							);
			query($conn, $updatePostageRuleQuery, $strType, $arrdbparams);
			$strcmd = "update_rule";
			break;
		case "add_cats":
		case "remove_cats":
			$getPostageRuleCatsQuery = "SELECT categoryIDList FROM site_postage WHERE recordID = :ruleID";
			$strType = "single";
			$arrdbparams = array(
								"ruleID" => $ruleID
							);
			$postageRuleCats = query($conn, $getPostageRuleCatsQuery, $strType, $arrdbparams);
			switch($strcmd) {
				case "add_cats":
					if(!empty($_POST['frm_cats'])) {
						if(strlen($postageRuleCats['categoryIDList']) > 0) {
							$newcats = $postageRuleCats['categoryIDList'] . "," . implode(",", $_POST['frm_cats']);
						} else {
							$newcats = implode(",", $_POST['frm_cats']);
						}
					}
					break;
				case "remove_cats":
					if(!empty($_POST['frm_catsInOffer'])) {
						$newcats = implode(",", array_diff(explode(",", $postageRuleCats['categoryIDList']), $_POST['frm_catsInOffer']));
					}
					break;
			}
			$updatePostageRuleQuery = "UPDATE site_postage SET categoryIDList = :catIDList WHERE recordID = :ruleID";
			$strType = "update";
			$arrdbparams = array(
								"catIDList" => $newcats,
								"ruleID" => $ruleID
							);
			query($conn, $updatePostageRuleQuery, $strType, $arrdbparams);
			$strcmd = "update_rule";
			break;
		case "add_products":
		case "remove_products":
			$getPostageRuleStockQuery = "SELECT stockIDList FROM site_postage WHERE recordID = :ruleID";
			$strType = "single";
			$arrdbparams = array(
								"ruleID" => $ruleID
							);
			$postageRuleProducts = query($conn, $getPostageRuleStockQuery, $strType, $arrdbparams);
			switch($strcmd) {
				case "add_products":
					if(!empty($_POST['frm_products'])) {
						if(strlen($postageRuleProducts['stockIDList']) > 0) {
							$newproducts = $postageRuleProducts['stockIDList'] . "," . implode(",", $_POST['frm_products']);
						} else {
							$newproducts = implode(",", $_POST['frm_products']);
						}
					}
					break;
				case "remove_products":
					if(!empty($_POST['frm_productsInOffer'])) {
						$newproducts = implode(",", array_diff(explode(",", $postageRuleProducts['stockIDList']), $_POST['frm_productsInOffer']));
					}
					break;
			}
			$updatePostageRuleQuery = "UPDATE site_postage SET stockIDList = :stockIDList WHERE recordID = :ruleID";
			$strType = "update";
			$arrdbparams = array(
								"stockIDList" => $newproducts,
								"ruleID" => $ruleID
							);
			query($conn, $updatePostageRuleQuery, $strType, $arrdbparams);
			$strcmd = "update_rule";
			break;
		case "add_postcodes":
		case "remove_postcodes":
			$getPostageRulePostcodesQuery = "SELECT postcodeList FROM site_postage WHERE recordID = :ruleID";
			$strType = "single";
			$arrdbparams = array(
								"ruleID" => $ruleID
							);
			$postageRulePostcodes = query($conn, $getPostageRulePostcodesQuery, $strType, $arrdbparams);
			
			switch($strcmd) {
				case "add_postcodes":
					if(!empty($_POST['frm_postcodes'])) {
						if(strlen($postageRulePostcodes['postcodeList']) > 0) {
							$newpostcodes = $postageRulePostcodes['postcodeList'] . "," . implode(",", $_POST['frm_postcodes']);
						} else {
							$newpostcodes = implode(",", $_POST['frm_postcodes']);
						}
					}
					break;
				case "remove_postcodes":
					if(!empty($_POST['frm_postcodesInOffer'])) {
						$newpostcodes = implode(",", array_diff(explode(",", $postageRulePostcodes['postcodeList']), $_POST['frm_postcodesInOffer']));
					}
					break;
			}
			$updatePostageRuleQuery = "UPDATE site_postage SET postcodeList = :postcodeList WHERE recordID = :ruleID";
			$strType = "update";
			$arrdbparams = array(
								"postcodeList" => $newpostcodes,
								"ruleID" => $ruleID
							);
			query($conn, $updatePostageRuleQuery, $strType, $arrdbparams);
			$strcmd = "update_rule";
			break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Postage Rules Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				<!--
				function jsaddpostagerule()
				{
					document.getElementById("cmd").value = "add_new_rule";
					document.getElementById("form").submit();
				}
				
				function jssavepostagerule(operation)
				{
					if(operation == "insert")
					{
						document.getElementById("cmd").value = "save_new_rule";
						document.getElementById("form").submit();
					}
					else if(operation == "update")
					{
						document.getElementById("cmd").value = "save_updated_rule";
						document.getElementById("form").submit();
					}
				}
				
				function jsaddpostagerulegroup()
				{
					document.getElementById("cmd").value = "add_new_group";
					document.getElementById("form").submit();
				}
				
				function jseditdeletepostagerule(operation, ruleID)
				{
					if(operation == "edit")
					{
						document.getElementById("ruleID").value = ruleID;
						document.getElementById("cmd").value = "update_rule";
						document.getElementById("form").submit();
					}
					else if(operation == "delete")
					{
						var confirmDelete = confirm("Are you SURE you want to delete this postage rule?");
						if(confirmDelete) {
							document.getElementById("ruleID").value = ruleID;
							document.getElementById("cmd").value = "delete_rule";
							document.getElementById("form").submit();
						}
					}
				}
				
				function jseditdeletepostagerulegroup(operation, groupID)
				{
					if(operation == "edit")
					{
						document.getElementById("groupID").value = groupID;
						document.getElementById("cmd").value = "update_group";
						document.getElementById("form").submit();
					}
					else if(operation == "delete")
					{
						var confirmDelete = confirm("Are you SURE you want to delete this postage rule group?");
						if(confirmDelete) {
							document.getElementById("groupID").value = groupID;
							document.getElementById("cmd").value = "delete_group";
							document.getElementById("form").submit();
						}
					}
				}
				
				function jssavepostagerulegroup()
				{
					document.getElementById("cmd").value = "save_postage_group";
					document.getElementById("form").submit();
				}
				
				function jsaddremoverules(operation)
				{
					if(operation == "add")
					{
						document.getElementById("cmd").value = "add_rules";
					}
					else if(operation == "remove")
					{
						document.getElementById("cmd").value = "remove_rules";
					}
					document.getElementById("form").submit();
				}
				
				function jsaddremoveranges(operation)
				{
					if(operation == "add")
					{
						document.getElementById("cmd").value = "add_ranges";
					}
					else if(operation == "remove")
					{
						document.getElementById("cmd").value = "remove_ranges";
					}
					document.getElementById("form").submit();
				}
				
				function jsaddremovebrands(operation)
				{
					if(operation == "add")
					{
						document.getElementById("cmd").value = "add_brands";
					}
					else if(operation == "remove")
					{
						document.getElementById("cmd").value = "remove_brands";
					}
					document.getElementById("form").submit();
				}
				
				function jsaddremovecats(operation)
				{
					if(operation == "add")
					{
						document.getElementById("cmd").value = "add_cats";
					}
					else if(operation == "remove")
					{
						document.getElementById("cmd").value = "remove_cats";
					}
					document.getElementById("form").submit();
				}
				
				function jsaddremoveproducts(operation)
				{
					if(operation == "add")
					{
						document.getElementById("cmd").value = "add_products";
					}
					else if(operation == "remove")
					{
						document.getElementById("cmd").value = "remove_products";
					}
					document.getElementById("form").submit();
				}
				
				function jsaddremovepostcodes(operation)
				{
					if(operation == "add")
					{
						document.getElementById("cmd").value = "add_postcodes";
					}
					else if(operation == "remove")
					{
						document.getElementById("cmd").value = "remove_postcodes";
					}
					document.getElementById("form").submit();
				}
				-->
			</script>
			<?php
		
			print ("<form action='promotions-catalogue-postage-rules.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='ruleID' id='ruleID' value='".$ruleID."'/>");
				print ("<input type='hidden' name='groupID' id='groupID' value='".$groupID."'/>");
				
				
				switch($strcmd)
				{
					case "":
						print ("<legend>Postage Rules</legend>");
						print ("<button onclick='return jsaddpostagerule();' class='btn btn-success'>Add New Rule</button>");
						print ("<br/>");
						print ("<br/>");
			?>
						<table id='rules-table' class='table table-striped table-bordered table-hover table-condensed' >
							<thead>
								<tr>
									<th>
									Description
									</th>
									<th>
									Postage Cost (&pound;)
									</th>
									<th>
									Min Spend (&pound;)
									</th>
									<th>
									Local Only
									</th>
									<th>
									Active
									</th>
									<th>
									Global
									</th>
									<th>
									Cost Modifier
									</th>
									<th>
									Modifier Threshold (&pound;)
									</th>
									<th>
									Modifier Price (&pound;)
									</th>
									<th>
									View
									</th>
									<th>
									Delete
									</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$getRulesQuery = "SELECT * FROM site_postage ORDER BY recordID";
								$strType = "multi";
								$postageRules = query($conn, $getRulesQuery, $strType);

								/*if (empty($postageRules)) {
									print ("\t\t\t\t<tr><td colspan='11'>No postage rules found</td></tr>");
								}
								else
								{*/
									foreach($postageRules AS $postageRule)
									{
										print ("<tr>
													<td>".$postageRule['description']."</td>
													<td>&pound;".$postageRule['cost']."</td>
													<td>&pound;".$postageRule['minspend']."</td>
													<td><input type='checkbox' name='frm_localOnly' value='1' ");
													if($postageRule['isLocalDeliveryOnly']) {
														print("checked ");
													}
													print(" disabled /></td>
													<td><input type='checkbox' name='frm_enabled' value='1' ");
													if($postageRule['isActive']) {
														print("checked ");
													}
													print(" disabled /></td>
													<td>");
													if($postageRule['isGlobal']) {
														print("Yes");
													}
													else
													{
														print("No");
													}
													print("</td>
													<td>".$postageRule['costModifier']."</td>
													<td>&pound;".$postageRule['modifierThreshold']."</td>
													<td>&pound;".$postageRule['modifierCost']."</td>
													<td><button type='button' onclick='jseditdeletepostagerule(\"edit\", \"".$postageRule['recordID']."\")' class='btn btn-primary'>Edit</button></td>
													<td><button type='button' onclick='jseditdeletepostagerule(\"delete\", \"".$postageRule['recordID']."\")' class='btn btn-danger'>Delete</button></td>
												</tr>");
									}
								//}
			?>
							</tbody>
						</table>
			<?php
						print ("<br/>");
						print ("<button onclick='return jsaddpostagerule();' class='btn btn-success'>Add New Rule</button>");
						print ("<br/>");
						print ("<br/>");
						print ("<br/>");
						print ("<legend>Postage Groups</legend>");
			?>
						<table id='rule-groups-table' class='table table-striped table-bordered table-hover table-condensed' >
							<thead>		
								<tr>
									<th style='width: 90%;'>
									Group Name
									</th>
									<th>
									View
									</th>
									<th>
									Delete
									</th>
								</tr>
							</thead>
							<tbody>
			<?php
								
								$getPostageGroups = "SELECT * FROM site_postage_group ORDER BY recordID";
								$strType = "multi";
								$postageGroups = query($conn, $getPostageGroups, $strType);

								/*if (empty($postageGroups)) {
									print ("\t\t\t\t<tr><td colspan='3'>No postage rule groups found</td></tr>");
								}
								else
								{*/
									foreach($postageGroups AS $postageGroup)
									{
										print ("<tr>
													<td>".$postageGroup['description']."</td>
													<td><button type='button' onclick='jseditdeletepostagerulegroup(\"edit\", \"".$postageGroup['recordID']."\")' class='btn btn-primary'>Edit</button></td>
													<td><button type='button' onclick='jseditdeletepostagerulegroup(\"delete\", \"".$postageGroup['recordID']."\")' class='btn btn-danger'>Delete</button></td>
												</tr>");
									}
								//}
							?>
							</tbody>
						</table>
					<?php
						print ("<br/>");
						print ("<div class='form-group'>");
							print ("<input style='width:200px;' type='text' class='form-control' id='frm_groupname' name='frm_groupname' /><br/>");
							print ("<button onclick='return jsaddpostagerulegroup();' class='btn btn-success'>Add New Group</button>");
						print ("</div>");
						break;
						
					case "add_new_rule":
						print ("<legend>Add New Postage Rule</legend>");
						print("<table id='rule-table' class='table table-striped table-bordered table-hover table-condensed' >
								<thead>
									<tr>
										<th>
											Description
										</th>
										<th>
											Postage Cost (&pound;)
										</th>
										<th>
											Min Spend (&pound;)
										</th>
										<th>
											Local Only
										</th>
										<th>
											Scope
										</th>
										<th>
											Modifier
										</th>
										<th>
											Modifier Threshold (&pound;)
										</th>
										<th>
											Modifier Price (&pound;)
										</th>
										<th>
											Update
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type='text' name='frm_description' /></td>
										<td><input type='text' name='frm_postageCost' /></td>
										<td><input type='text' name='frm_minSpend' /></td>
										<td><input type='checkbox' name='frm_localOnly' value='1' /></td>
										<td>
											<select name='frm_global'>
												<option value='0'>Standard</option>
												<option value='1'>Global</option>
											</select>
										</td>
										<td><input type='text' name='frm_costModifier' /></td>
										<td><input type='text' name='frm_modifierThreshold' /></td>
										<td><input type='text' name='frm_modifierCost' /></td>
										<td><button class='btn btn-success' onclick='jssavepostagerule(\"insert\")'>Save New Rule</button></td>
									</tr>
								</tbody>
							</table>");
						break;
						
					case "update_group":
						$getPostageGroup = "SELECT * FROM site_postage_group WHERE recordID = :groupID";
						$strType = "single";
						$arrdbparam = array("groupID" => $groupID);
						$postageGroup = query($conn, $getPostageGroup, $strType, $arrdbparam);
						print ("<legend>Update Postage Rule Group</legend>");
						print("<table id='rule-table' class='table table-striped table-bordered table-hover table-condensed' >
									<thead>
										<tr>
											<th>
												Group Name
											</th>
											<th>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><input type='text' name='frm_groupname' value='".$postageGroup['description']."' /></td>
											<td><button class='btn btn-success' onclick='jssavepostagerulegroup()'>Save Group</button></td>
										</tr>
									</tbody>
								</table>
								<legend>Rules in group</legend>
								<table>
									<tr>
										<td>");
											$getUnassignedRules = "SELECT * FROM site_postage WHERE groupID IS NULL ORDER BY description ASC";
											$strType = "multi";
											$unassignedRules = query($conn, $getUnassignedRules, $strType);
											if(!empty($unassignedRules))
											{
												print("<select multiple name='frm_ungroupedrules[]' style='height: 400px; width: 100%'>");															
													foreach($unassignedRules AS $unassignedRule)
													{
														print("<option value='".$unassignedRule['recordID']."'>".$unassignedRule['description']."</option>");																
													}
												print("</select>");
											}
											else
											{
												print("No rules to display");
											}
								print("</td>
										<td style='padding: 5px;'><button type='button' class='btn' onclick='jsaddremoverules(\"remove\")'><<</button>
											<br/>
											<br/>
											<button type='button' class='btn' onclick='jsaddremoverules(\"add\")'>>></button>
										</td>
										<td>");
											$getAssignedRules = "SELECT * FROM site_postage WHERE groupID = :groupID ORDER BY description ASC";
											$strType = "multi";
											$arrdbparam = array("groupID" => $groupID);
											$assignedRules = query($conn, $getAssignedRules, $strType, $arrdbparam);
											if(!empty($assignedRules))
											{
												print("<select multiple name='frm_groupedrules[]' style='height: 400px; width: 100%'>");
													foreach($assignedRules AS $assignedRule)
													{
														print("<option value='".$assignedRule['recordID']."'>".$assignedRule['description']."</option>");																
													}														
												print("</select>");
											}
											else
											{
												print("No rules in group");
											}
								print("</td>
									</tr>
								</table>");
						break;
						
					case "update_rule":
							$getPostageRule = "SELECT * FROM site_postage WHERE recordID = :ruleID";
							$strType = "single";
							$arrdbparam = array("ruleID" => $ruleID);
							$postageRule = query($conn, $getPostageRule, $strType, $arrdbparam);
							
							print("<a href='/admin/promotions-catalogue-postage-rules.php' class='btn btn-primary'>View other rules</a><br/><br/>");
						
							print("<table id='rule-table' class='table table-striped table-bordered table-hover table-condensed'>
									<tr>
										<th>
											Description
										</th>
										<th>
											Postage Cost (&pound;)
										</th>
										<th>
											Min Spend (&pound;)
										</th>
										<th>
											Local Only
										</th>
										<th>
											Scope
										</th>
										<th>
											Modifier
										</th>
										<th>
											Modifier Threshold (&pound;)
										</th>
										<th>
											Modifier Price (&pound;)
										</th>
										<th>
											Active
										</th>
										<th>
											Update
										</th>
									</tr>
									<tr>
										<td><input type='text' value='".$postageRule['description']."' name='frm_description' /></td>
										<td><input type='text' value='".$postageRule['cost']."' name='frm_postageCost' /></td>
										<td><input type='text' value='".$postageRule['minspend']."' name='frm_minSpend' /></td>
										<td><input type='checkbox' name='frm_localOnly' value='1' ");
										if($postageRule['isLocalDeliveryOnly']) {
											print("checked ");
										}
										print("/></td>
										<td>
											<select name='frm_global'>");
												$selected = "";
												if($postageRule['isGlobal'] == 0)
												{
													$selected = "selected";
												}
												print("<option value='0' $selected>Standard</option>");
											
											$selected = "";
											if($postageRule['isGlobal'] == 1)
											{
												$selected = " selected";
											}
												print("<option value='1' $selected>Global</option>
											</select>
										</td>
										<td><input type='text' name='frm_costModifier' value='".$postageRule['costModifier']."' /></td>
										<td><input type='text' name='frm_modifierThreshold' value='".$postageRule['modifierThreshold']."' /></td>
										<td><input type='text' name='frm_modifierCost' value='".$postageRule['modifierCost']."' /></td>
										<td><input type='checkbox' name='frm_enabled' value='1' ");
										if($postageRule['isActive']) {
											print("checked ");
										}
										print("/></td>
										<td colspan='4'><button class='btn btn-primary' onclick='jssavepostagerule(\"update\")'>Update</button></td>
									</tr>
								</table>
								<legend>Ranges</legend>
								<table>
									<tr>
										<td>
											<select multiple name='frm_ranges[]' style='height: 400px; width: 100%'>");
												$rangesInOffer = array();
												$rangesArray = explode(",", $postageRule['rangeIDList']);
												$getRanges = "SELECT * FROM stock_ranges ORDER BY rangeName";
												$strType = "multi";
												$rangesData = query($conn, $getRanges, $strType);
												foreach($rangesData AS $rangeData)
												{
													if(!in_array($rangeData['recordID'], $rangesArray)) {
														print("<option value='".$rangeData['recordID']."'>".$rangeData['rangeName']."</option>");
													} else {
														array_push($rangesInOffer, $rangeData);																
													}															
												}
											print("</select>
										</td>
										<td>
											<button type='button' class='btn' onclick='jsaddremoveranges(\"remove\")'><<</button>
											<br/>
											<br/>
											<button type='button' class='btn' onclick='jsaddremoveranges(\"add\")'>>></button>
										</td>
										<td>");
										if(!empty($rangesInOffer)) {
											print("<select multiple name='frm_rangesInOffer[]' style='height: 400px; width: 100%;'>");
												foreach ($rangesInOffer AS $rangeInOffer)
												{
													print("<option value='".$rangeInOffer['recordID']."'>".$rangeInOffer['rangeName']."</option>");
												}
											print("</select>");
										} else {
											print("No ranges currently in offer.");
										}
										print("</td>
									</tr>
								</table>
								<br/>
								<legend>Brands</legend>
								<table>
									<tr>
										<td>
											<select multiple name='frm_brands[]' style='height: 400px; width: 100%;'>");
												$brandsInOffer = array();
												$brandsArray = explode(",", $postageRule['brandIDList']);
												$getBrands = "SELECT * FROM  stock_brands ORDER BY brandName";
												$strType = "multi";
												$brandsData = query($conn, $getBrands, $strType);
												foreach($brandsData AS $brandData)
												{
													if(!in_array($brandData['recordID'], $brandsArray)) {
														print("<option value='".$brandData['recordID']."'>".$brandData['brandName']."</option>");
													} else {
														array_push($brandsInOffer, $brandData);																
													}															
												}
										print("</select>
										</td>
										<td>
											<button type='button' class='btn' onclick='jsaddremovebrands(\"remove\")'><<</button>
											<br/>
											<br/>
											<button type='button' class='btn' onclick='jsaddremovebrands(\"add\")'>>></button>
										</td>
										<td>");
										if(!empty($brandsInOffer)) {
											print("<select multiple name='frm_brandsInOffer[]' style='height: 400px; width: 100%;'>");
												foreach ($brandsInOffer AS $brandInOffer)
												{
													print("<option value='".$brandInOffer['recordID']."'>".$brandInOffer['brandName']."</option>");
												}
											print("</select>");
										} else {
											print("No brands currently in offer.");
										}
										print("</td>
									</tr>
								</table>
								<br/>
								<legend>Categories</legend>
								<table>
									<tr>
										<td>
											<select multiple name='frm_cats[]' style='height: 400px; width: 100%;'>");
												$categoriesInOffer = array();
												$categoriesArray = explode(",", $postageRule['categoryIDList']);
												$getCategories = "SELECT * FROM  category ORDER BY contentName";
												$strType = "multi";
												$categoriesData = query($conn, $getCategories, $strType);
												foreach($categoriesData AS $categoryData)
												{
													if(!in_array($categoryData['recordID'], $categoriesArray)) {
														print("<option value='".$categoryData['recordID']."'>".$categoryData['contentName']."</option>");
													} else {
														array_push($categoriesInOffer, $categoryData);																
													}															
												}
											print("</select>
										</td>
										<td>
											<button type='button' class='btn' onclick='jsaddremovecats(\"remove\")'><<</button>
											<br/>
											<br/>
											<button type='button' class='btn' onclick='jsaddremovecats(\"add\")'>>></button>
										</td>
										<td>");
										if(!empty($categoriesInOffer)) {
											print("<select multiple name='frm_catsInOffer[]' style='height: 400px; width: 100%;'>");
												foreach ($categoriesInOffer AS $categoryInOffer)
												{
													print("<option value='".$categoryInOffer['recordID']."'>".$categoryInOffer['contentName']."</option>");
												}
											print("</select>");
										} else {
											print("No categories currently in offer.");
										}													
										print("</td>
									</tr>
								</table>
								<br/>
								<legend>Products</legend>
								<table>
									<tr>
										<td>
											<select multiple name='frm_products[]' style='height: 400px; width: 100%;'>");
												$productsInOffer = array();
												$productsArray = explode(",", $postageRule['stockIDList']);
												$getProducts = "SELECT * FROM stock_group_information WHERE name != '' ORDER BY name";
												$strType = "multi";
												$productsData = query($conn, $getProducts, $strType);
												foreach($productsData AS $productData)
												{
													if(!in_array($productData['recordID'], $productsArray)) {
														print("<option value='".$productData['recordID']."'>".$productData['name']."</option>");
													} else {
														array_push($productsInOffer, $productData);																
													}															
												}
											print("</select>
										</td>
										<td>
											<button type='button' class='btn' onclick='jsaddremoveproducts(\"remove\")'><<</button>
											<br/>
											<br/>
											<button type='button' class='btn' onclick='jsaddremoveproducts(\"add\")'>>></button>
										</td>
										<td>");
										if(!empty($productsInOffer)) {
											print("<select multiple name='frm_productsInOffer[]' style='height: 400px; width: 100%;'>");
												foreach ($productsInOffer AS $productInOffer)
												{
													print("<option value='".$productInOffer['recordID']."'>".$productInOffer['name']."</option>");
												}
											print("</select>");
										} else {
											print("No products currently in offer.");
										}													
										print("</td>
									</tr>
								</table>
								<br/>
								<legend>Post Codes</legend>
								<table>
									<tr>
										<form name='postcodesform' action='postage_rules.php' method='post' accept-charset='UTF-8'>
											<td>
												<select multiple name='frm_postcodes[]' style='height: 400px; width: 100%;'>");
													$postcodesInOffer = array();
													$postcodesArray = explode(",", $postageRule['postcodeList']);
													$getPostcodes = "SELECT * FROM site_postcode_districts WHERE postcode LIKE 'LA%' OR postcode LIKE 'CA%' ORDER BY postcode";
													$strType = "multi";
													$postcodesData = query($conn, $getPostcodes, $strType);
													foreach($postcodesData AS $postcodeData)
													{
														if(!in_array($postcodeData['postcode'], $postcodesArray)) {
															print("<option value='".$postcodeData['postcode']."'>".$postcodeData['postcode']."</option>");
														} else {
															array_push($postcodesInOffer, $postcodeData);																
														}															
													}
												print("</select>
											</td>
											<td>
												<button type='button' class='btn' onclick='jsaddremovepostcodes(\"remove\")'><<</button>
												<br/>
												<br/>
												<button type='button' class='btn' onclick='jsaddremovepostcodes(\"add\")'>>></button>
											</td>
											<td>");
											if(!empty($postcodesInOffer)) {
												print("<select multiple name='frm_postcodesInOffer[]' style='height: 400px; width: 100%;'>");
													foreach ($postcodesInOffer AS $postcodeInOffer)
													{
														print("<option value='".$postcodeInOffer['postcode']."'>".$postcodeInOffer['postcode']."</option>");
													}
												print("</select>");
											} else {
												print("No post codes currently in offer.");
											}													
											print("</td>
										</form>
									</tr>
								</table>\n");
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#rules-table').DataTable();
			$('#rule-groups-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>