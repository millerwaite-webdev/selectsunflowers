<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * sales-order-create.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "sales-order-create"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	//$_GET['page'] = checkout;

	// *********** Custom Page Processing ***************** //
	//=====================================================//
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['intstep'])) $intstep = $_REQUEST['intstep']; else $intstep = "";
	if (isset($_REQUEST['intnewqty'])) $intnewqty = $_REQUEST['intnewqty']; else $intnewqty = "";
	if (isset($_REQUEST['accountType'])) $accountType = $_REQUEST['accountType']; else $accountType = 0;
	
	switch($strcmd){
		case "login":
		
			$username = $_POST['user'];
			$password = $_POST['password'];
			
			//error_log("User: ".$username." - Pass: ".$password);
			
			$basicCustomerDetails = checkUserLogin($username, $password, $conn);
			//var_dump($basicCustomerDetails);
			
			if ($basicCustomerDetails['num_valid'] == 1)
			{
				$intuserhash = $basicCustomerDetails['recordID'];
				setcookie ("stoid", "$intuserhash", time() + 60*60*6, "/", $strcookieurl);
				
				if(is_numeric($intuserhash) && $intuserhash == 1) {
					$_SESSION['userID'] = 1;
				} else {
					if(!empty($basicCustomerDetails['recordID'])) {
						$_SESSION['userID'] = $basicCustomerDetails['recordID'];
						
						if(!empty($basicCustomerDetails['email'])) {
							$_SESSION['email'] = $basicCustomerDetails['email'];
						}
					} else {
						$_SESSION['userID'] = 0;
					}
				}
				$intuserid = $_SESSION['userID'];
				fnUpdateBasketHeader($intbasketid, $intuserid, $_REQUEST, $conn);
				$strcmd = "confirmDetails";
				
				$strloginmessage = "<div class='notification-success'><p>Login successful</p></div>";
				
			} else {
				$strloginmessage = "<div class='notification-error'><p>The user name and password you supplied do not match our records. Please try again.</p></div>";
				$strcmd = "accountType";
				//if ($strreferer != "") { header("Location: ".$strsecurehref.$strreferer."?error"); exit; }
			}

		break;
		case 'saveAccountDetails':
			
			$strfirstname = $_REQUEST['firstname'];
			$strsurname = $_REQUEST['surname'];
			$strtitle = $_REQUEST['title'];
			$strcompany = $_REQUEST['company'];
			$strphone = $_REQUEST['tel'];
			$stremail = $_REQUEST['email'];
			if(isset($_REQUEST['addtype'])) $intaddtype = $_REQUEST['addtype'];
			else $intaddtype = 0;

			$strusername = $_REQUEST['username'];
			$strpassword = md5($_REQUEST['password']);
			if (isset($_REQUEST['newsletter'])) $intnewsletter = $_REQUEST['newsletter'];
			else $intnewsletter = 0;

			$strdelfirstname = $_REQUEST['firstname'];
			$strdelsurname = $_REQUEST['surname'];
			$strdeltitle = $_REQUEST['title'];
			$strdeladd1 = $_REQUEST['address'];
			$strdeladd2 = $_REQUEST['address2'];
			$strdeladd3 = $_REQUEST['address3'];
			$strdelTown = $_REQUEST['town'];
			$strdelCnty = $_REQUEST['county'];
			$strdelPstCde = $_REQUEST['postcode'];

			$strbillfirstname = $_REQUEST['bfirstname'];
			$strbillsurname = $_REQUEST['bsurname'];
			$strbilltitle = $_REQUEST['btitle'];
			$strbilladd1 = $_REQUEST['baddress'];
			$strbilladd2 = $_REQUEST['baddress2'];
			$strbilladd3 = $_REQUEST['baddress3'];
			$strbillTown = $_REQUEST['btown'];
			$strbillCnty = $_REQUEST['bcounty'];
			$strbillPstCde = $_REQUEST['bpostcode'];

			if (check_email_address($stremail))
			{
				if ($accountType == 1) {
					
					// not guest user create account
					$strdbsql = "SELECT * FROM customer WHERE username = :username";
					$strType = "single";
					$arrdbparams = array("username"=>$strusername);
					$customer = query($conn,$strdbsql,$strType,$arrdbparams);

					if (count($customer['recordID']) == 0){
												
						//create customer account
						$strdbsql = "INSERT INTO customer (groupID, username, password, title, firstname, surname, company, telephone, email) VALUES (1, :username, :password, :title, :firstname, :surname, :company, :telephone, :email);";
						$strType = "insert";
						$arrdbparams = array("username"=>$strusername, "password"=>$strpassword, "title"=>$strtitle, "firstname"=>$strfirstname, "surname"=>$strsurname, "company"=>$strcompany, "telephone"=>$strphone, "email"=>$stremail);
						$customerID = query($conn,$strdbsql,$strType,$arrdbparams);
						
						//insert a new address record
						$strdbsql = "INSERT INTO customer_address (customerID, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode);";
						$strType = "insert";
						$arrdbparams = array("customerID"=>$customerID, "title"=>$strdeltitle, "firstname"=>$strdelfirstname, "surname"=>$strdelsurname, "add1"=>$strdeladd1, "add2"=>$strdeladd2, "add3"=>$strdeladd3, "town"=>$strdelTown, "county"=>$strdelCnty, "postcode"=>$strdelPstCde);
						$deliveryAddress = query($conn,$strdbsql,$strType,$arrdbparams);
						
						//if delivery and billing are different then add both
						if ($intaddtype != 0) {
						
							$strdbsql = "INSERT INTO customer_address (customerID, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode);";
							$strType = "insert";
							$arrdbparams = array("customerID"=>$customerID, "title"=>$strbilltitle, "firstname"=>$strbillfirstname, "surname"=>$strbillsurname, "add1"=>$strbilladd1, "add2"=>$strbilladd2, "add3"=>$strbilladd3, "town"=>$strbillTown, "county"=>$strbillCnty, "postcode"=>$strbillPstCde);
							$billingAddress = query($conn,$strdbsql,$strType,$arrdbparams);
						
						} else {
							$billingAddress = $deliveryAddress;
						}
						
						//update default addresses for customer
						$strdbsql = "UPDATE customer SET defaultBillingAdd = :billingAddress, defaultDeliveryAdd = :deliveryAddress WHERE recordID = :customerID";
						$strType = "insert";
						$arrdbparams = array("customerID"=>$customerID,"billingAddress"=>$billingAddress,"deliveryAddress"=>$deliveryAddress);
						$updateResult = query($conn,$strdbsql,$strType,$arrdbparams);
						
						//add to newsletter
						if ($intnewsletter == 1) {
							$strdbsql = "INSERT INTO customer_newsletter (customerID, name, emailAddress) VALUES (:customerID, :name, :emailAddress);";
							$strType = "insert";
							$name = $strtitle." ".$strfirstname." ".$strsurname;
							$arrdbparams = array("name"=>$name, "customerID"=>$customerID, "emailAddress"=>$stremail);
							$newsletterInsert = query($conn,$strdbsql,$strType,$arrdbparams);
						}

						//send email confirmation
						$strmessage = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>";
						$strmessage = $strmessage."<html><head><title>Welcome to stollers.co.uk</title></head><body><img src='http://stollers.co.uk/images/logo.png' /><br/><br/><br/>";
						$strmessage = $strmessage."<p><b>Thank you for Registering for an account with <a href='http://stollers.co.uk'>stollers.co.uk</a>.</b></p>";
						$strmessage = $strmessage."<p>Your account details are given below. Please keep a record of your username and password as you will require these to log onto the site.</p>";

						$strmessage = $strmessage."<table width='500px'>";
						$strmessage = $strmessage."<tr><td width='40%'>User Name</td><td width='60%'>$strusername</td></tr>";
						$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";

						$strmessage = $strmessage."<tr><td>First Name</td><td>$strfirstname</td></tr>";
						$strmessage = $strmessage."<tr><td>Last Name</td><td>$strsurname</td></tr>";
						$strmessage = $strmessage."<tr><td>Title</td><td>$strtitle</td></tr>";
						$strmessage = $strmessage."<tr><td>Company</td><td>$strcompany</td></tr>";
						$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";

						$strmessage = $strmessage."<tr><td>Phone Number</td><td>$strphone</td></tr>";
						$strmessage = $strmessage."<tr><td>Email</td><td>$stremail</td></tr>";
						$strmessage = $strmessage."<tr><td>Fax</td><td>$strfax</td></tr>";
						$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";

						if ($intaddtype ==0 ) {
							$strmessage = $strmessage."<tr><td>Address</td><td>$strdeladd1</td></tr>";
							if ($strdeladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd2</td></tr>"; }
							if ($strdeladd3 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd3</td></tr>"; }
							if ($strdelTown != "") {$strmessage = $strmessage."<tr><td></td><td>$strdelTown</td></tr>"; }
							$strmessage = $strmessage."<tr><td></td><td>$strdelCnty</td></tr>";
							$strmessage = $strmessage."<tr><td></td><td>$strdelPstCde</td></tr>";
							$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
						} else {
							$strmessage = $strmessage."<tr><td>Delivery Address</td><td>$strdelname</td>";
							$strmessage = $strmessage."<tr><td></td><td>$strdeladd1</td></tr>";
							if ($strdeladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd2</td></tr>"; }
							if ($strdeladd3 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd3</td></tr>"; }
							if ($strdelTown != "") {$strmessage = $strmessage."<tr><td></td><td>$strdelTown</td></tr>"; }
							$strmessage = $strmessage."<tr><td></td><td>$strdelCnty</td></tr>";
							$strmessage = $strmessage."<tr><td></td><td>$strdelPstCde</td></tr>";

							$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
							$strmessage = $strmessage."<tr><td>Billing Address</td><td>$strbillname</td>";
							$strmessage = $strmessage."<tr><td></td><td>$strbilladd1</td></tr>";
							if ($strbilladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strbilladd2</td></tr>"; }
							if ($strbilladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strbilladd2</td></tr>"; }
							if ($strbillTown != "") {$strmessage = $strmessage."<tr><td></td><td>$strbillTown</td></tr>"; }
							$strmessage = $strmessage."<tr><td></td><td>$strbillCnty</td></tr>";
							$strmessage = $strmessage."<tr><td></td><td>$strbillPstCde</td></tr>";
							$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";

						}
						$strmessage = $strmessage."</table></body></html>";
						mail($stremail, "Welcome to stollers.co.uk",$strmessage, "MIME-Version: 1.0".PHP_EOL."X-Mailer: PHP/" . phpversion().PHP_EOL."Sensitivity: Personal".PHP_EOL."Return-Path:stollers.co.uk".PHP_EOL."From: Stollers UK <no-reply@stollers.co.uk>".PHP_EOL."Content-Type: text/html; charset=iso-8859-1");

						fnUpdateBasketHeader($intbasketid, $customerID, $_REQUEST, $conn);
						setcookie ("stoid", "$customerID", time() + 60*60*2, "/", $strcookieurl);
						$strcmd = "confirmDetails";
										
					} else {
						$strerror = "<b style='color:red'>Sorry - This username is already in use. Please try a different one.</b>";
						$strcmd = "accountDetails";
					}
					
				} else {
					//set guest in cookie
					$_SESSION['userID'] = 1;
					$intuserid = $_SESSION['userID'];
					fnUpdateBasketHeader($intbasketid, $intuserid, $_REQUEST, $conn);
					setcookie ("stoid", "$intuserid", time() + 60*60*2, "/", $strcookieurl);
					$strcmd = "confirmDetails";
				}

			} else {
				$strerror = "<b style='color:red'>Sorry - Invalid Email Address - please re-enter</b>";
				$strcmd = "accountDetails";
			}
			
		break;

		case 'changeaddress_del':
			
			$strdbsql = "SELECT * FROM basket_header WHERE recordID = :basketID";
			$strType = "single";
			$arrdbparams = array("basketID"=>$intbasketid);
			$basketHeader = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$deliveryDetails = unserialize($basketHeader['deliveryAddressArray']);
			$deliveryDetails['title'] = $_POST['title'];
			$deliveryDetails['firstname'] = $_POST['firstname'];
			$deliveryDetails['surname'] = $_POST['surname'];
			$deliveryDetails['add1'] = $_POST['address1'];
			$deliveryDetails['add2'] = $_POST['address2'];
			$deliveryDetails['add3'] = $_POST['address3'];
			$deliveryDetails['town'] = $_POST['town'];
			$deliveryDetails['county'] = $_POST['county'];
			$deliveryDetails['postcode'] = $_POST['postcode'];
			
			$strdbsql = "UPDATE basket_header SET deliveryAddressArray = :deliveryDetails, modifiedTimestamp = UNIX_TIMESTAMP() WHERE recordID = :basketID";
			$strType = "update";
			$arrdbparams = array("basketID"=>$intbasketid, "deliveryDetails"=>serialize($deliveryDetails));
			$basketUpdate = query($conn,$strdbsql,$strType,$arrdbparams);


			$strsuccess = "Details updated";
			$strcmd = "confirmDetails";
			break;
		case 'changeaddress_bill':
			
			$strdbsql = "SELECT * FROM basket_header WHERE recordID = :basketID";
			$strType = "single";
			$arrdbparams = array("basketID"=>$intbasketid);
			$basketHeader = query($conn,$strdbsql,$strType,$arrdbparams);
			$billingDetails = unserialize($basketHeader['billingAddressArray']);
			$billingDetails['title'] = $_POST['btitle'];
			$billingDetails['firstname'] = $_POST['bfirstname'];
			$billingDetails['surname'] = $_POST['bsurname'];
			$billingDetails['add1'] = $_POST['baddress1'];
			$billingDetails['add2'] = $_POST['baddress2'];
			$billingDetails['add3'] = $_POST['baddress3'];
			$billingDetails['town'] = $_POST['btown'];
			$billingDetails['county'] = $_POST['bcounty'];
			$billingDetails['postcode'] = $_POST['bpostcode'];
		
			$strdbsql = "UPDATE basket_header SET billingAddressArray = :billingDetails, modifiedTimestamp = UNIX_TIMESTAMP() WHERE recordID = :basketID";
			$strType = "update";
			$arrdbparams = array("basketID"=>$intbasketid, "billingDetails"=>serialize($billingDetails));
			$basketUpdate = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strsuccess = "Details updated";
			$strcmd = "confirmDetails";
			break;
	}
	
	//get basket header and item details
	$strdbsql = "SELECT * FROM basket_header WHERE recordID = :basketID";
	$strType = "single";
	$arrdbparams = array("basketID"=>$intbasketid);
	$basketHeader = query($conn,$strdbsql,$strType,$arrdbparams);

	$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basketID";
	$strType = "multi";
	$arrdbparams = array("basketID"=>$intbasketid);
	$basketItems = query($conn,$strdbsql,$strType,$arrdbparams);
	if (count($basketItems) <= 0) $strcmd = "";
	
	$arrdiscountcats = array();
	$arrdiscountprod = array();
	$strselectedpostage = $basketHeader['postageID'];
	
	$meta['metaTitle'] = "Basket/Checkout - Stollers Furniture & Interiors";
	$meta['description'] = "Basket/checkout for Stollers Furniture & Interiors";
	$meta['keywords'] = "basket,checkout,stollers,furniture,interiors";
	$meta['metaPageLink'] = "checkout.php";
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	
	// ************* Custom Page Code ******************** //
	//=====================================================//

	//Print out debug and error messages
	if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
	if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
	if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
	print ("<div class='container'>");
		print ("<div class='page-details'>");
			print ("<h1 class='page-title'>$strPage</h1>");
			print ("<ul class='breadcrumb bread-page'>");
				print ("<li>");
					print ("<a href='/index'>Home</a>");
				print ("</li>");
				print ("<li>");
					print ("<a href='#'>$strPage</a>");
				print ("</li>");
			print ("</ul>");
		print ("</div>");
	print ("</div>");
	
	print ("<div class='container'>");
			print ("<div class='row'>");
				print ("<div class='col-sm-12'>");
					switch ($strcmd) {
						case "": print ("<ul class='breadcrumb bread-check'><li class='current'>Basket</li><li>Account Type</li><li>Delivery Details</li><li>Confirmation &amp; Payment</li><li>Completed</li></ul>"); break;
						case "accountType": print ("<ul class='breadcrumb bread-check'><li class='complete'>Basket</li><li class='current'>Account Type</li><li>Delivery Details</li><li>Confirmation &amp; Payment</li><li>Completed</li></ul>"); break;
						case "accountDetails": print ("<ul class='breadcrumb bread-check'><li class='complete'>Basket</li><li class='complete'>Account Type</li><li class='current'>Delivery Details</li><li>Confirmation &amp; Payment</li><li>Completed</li></ul>"); break;
						case "confirmDetails": print ("<ul class='breadcrumb bread-check'><li class='complete'>Basket</li><li class='complete'>Account Type</li><li class='complete'>Delivery Details</li><li class='current'>Confirmation &amp; Payment</li><li>Completed</li></ul>"); break;
						case "completeSuccess": print ("<ul class='breadcrumb bread-check'><li class='complete'>Basket</li><li class='complete'>Account Type</li><li class='complete'>Delivery Details</li><li class='complete'>Confirmation &amp; Payment</li><li class='current'>Completed</li></ul>"); break;
					}
				print ("</div>");
			print ("</div>");
			print ("<div class='row'>");
				print ("<div class='col-sm-12 main-content' style='padding-right:15px;'>");
			
?>	
					<script type='text/javascript'>	
						function fnLoadBasketPage() {
							document.getElementById("cmd").value = '';
							document.getElementById("intstep").value = 1;
							document.getElementById("form").submit();
						}
						function fnLoadAccountTypePage() {
							document.getElementById("cmd").value = 'accountType';
							document.getElementById("intstep").value = 2;
							document.getElementById("form").submit();
						}
						function fnLogin(){
							document.getElementById("cmd").value = 'login';
							document.getElementById("intstep").value = 2;
							document.getElementById("form").submit();
						}
						function fnForgotPassword(){
						
						}
						function fnLoadAccountDetailsPage(accountType) {
							document.getElementById("cmd").value = 'accountDetails';
							document.getElementById("accountType").value = accountType;
							document.getElementById("intstep").value = 3;
							document.getElementById("form").submit();
						}
						function fnSaveAccountDetails(){
							document.getElementById("cmd").value = 'saveAccountDetails';
							document.getElementById("form").submit();
						}
						function fnLoadConfirmPage() {
							document.getElementById("cmd").value = 'confirmDetails';
							document.getElementById("intstep").value = 4;
							document.getElementById("form").submit();
						}
					
						/* Account details functions */
						if (document.getElementsByClass == undefined) {
							document.getElementsByClass = function(className) {
								var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
								var allElements = document.getElementsByTagName("*");
								var results = [];

								var element;
								for (var i = 0; (element = allElements[i]) != null; i++) {
									var elementClass = element.className;
									if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
										results.push(element);
									}
								return results;
							}
						}
						function fnAccountDetailsCheck(form){
							var highlandsnislnds = new Array("GY", "BT", "IM", "HS", "ZE", "JE");
							var highlandsnislnds2 = new Array("IV41", "IV42", "IV43", "IV44", "IV45", "IV46", "IV47", "IV48", "IV49", "IV50", "IV51", "IV52", "IV53", "IV54", "IV55", "IV56", "KA27", "KA28", "PA20", "PA41", "PA42", "PA43", "PA44", "PA45", "PA46", "PA47", "PA48", "PA49", "PA62", "PA63", "PA64", "PA65", "PA66", "PA67", "PA68", "PA69", "PA70", "PA71", "PA72", "PA73", "PA74", "PA75", "PA76", "PH42", "PH43", "PH44", "PO31", "PO32","PO33","PO34","PO35","PO36", "PO37", "PO38", "PO39","PO40", "PO41");
							var error = 0;

							if(form.regtype.value == 1) {
								if (form.username.value == "") {alert("Please enter a Username.");error = 1;}
								else if (form.password.value == "") {alert("Please enter a Password.");error = 1;}
								else if (form.password.value != form.cpassword.value) {alert("Your Password does not match the confirmation");error = 1;}
							}
							if(error == 0) {
								if (form.addtype.value == 1) {

									if (form.firstname.value == "") {alert("Please enter your First Name. Contact details are required for our carriers to deliver your order.");error = 1;}
									else if (form.surname.value == "" && error == 0) {alert("Please enter your Surname. Contact details are required for our carriers to deliver your order.");error = 1;}
									else if (form.tel.value == "" && error == 0) {alert("Please enter a Phone Number. Contact details are required for our carriers to deliver your order.");error = 1;}
									else if (form.email.value == "" && error == 0) {alert("Please enter a valid email address. Contact details are required for our carriers to deliver your order."); error = 1;}
									else if (form.email.value != form.cemail.value && error == 0) {alert("Your Email Address does not match the confirmation."); error = 1;}

									if (form.address.value == "" && error == 0) {alert("Please enter the Delivery Address. These details are required for our carriers to deliver your order.");error = 1;}
									else if (form.county.value == "" && error == 0) {alert("Please enter the Delivery Address County. These details are required for our carriers to deliver your order.");error = 1;}
									else if (form.postcode.value == "" && error == 0) {alert("Please enter the Delivery Address Post Code. These details are required for our carriers to deliver your order.");error = 1;}
									else if (form.bfirstname.value == "" && error == 0) {alert("Please enter the First Name for the Billing Details.");error = 1;}
									else if (form.bsurname.value == "" && error == 0) {alert("Please enter your Surname for the Billing Details.");error = 1;}
									else if (form.baddress.value == "" && error == 0) {alert("Please enter the Billing Address.");error = 1;}
									else if (form.bcounty.value == "" && error == 0) {alert("Please enter the Billing Address County.");error = 1;}
									else if (form.bpostcode.value == "" && error == 0) {alert("Please enter the Billing Address Post Code.");error = 1;}
									else if (error == 0){
										var dpostcode = form.postcode.value;
										var bpostcode = form.bpostcode.value;


										var pcode = dpostcode.slice(0,2);
										pcode = pcode.toUpperCase();
										var pcode3 = bpostcode.slice(0,2);
										pcode3 = pcode3.toUpperCase();
										for (var i = 0; i <highlandsnislnds.length; i = i+ 1)
										{
											if (pcode == highlandsnislnds[i])
											{
												alert ("Your delivery postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
												error = 1;
											}
											if (pcode3 == highlandsnislnds[i])
											{
												alert ("Your billing postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
												error = 1;
											}
										}

										var pcode2 = dpostcode.slice(0,4);
										pcode2 = pcode2.toUpperCase();
										var pcode4 = bpostcode.slice(0,4);
										pcode4 = pcode4.toUpperCase();
										for (var i = 0; i <highlandsnislnds2.length; i = i+ 1)
										{
											if (pcode2 == highlandsnislnds2[i])
											{
												alert ("Your delivery postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
												error = 1;
											}
											if (pcode4 == highlandsnislnds2[i])
											{
												alert ("Your billing postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
												error = 1;
											}
										}


										dpostcode = dpostcode.replace(" ", "");
										dpostcode = dpostcode.toUpperCase();
										bpostcode = bpostcode.replace(" ", "");
										bpostcode = bpostcode.toUpperCase();
										if (dpostcode.length < 5 || dpostcode > 7) {alert("The delivery postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
										else if (dpostcode.charCodeAt(dpostcode.length-1) < 65 || dpostcode.charCodeAt(dpostcode.length-1) > 90) {alert ("The delivery postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else if (dpostcode.charCodeAt(dpostcode.length-2) < 65 || dpostcode.charCodeAt(dpostcode.length-2) > 90) {alert ("The delivery postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else if (dpostcode.charCodeAt(dpostcode.length-3) < 48 || dpostcode.charCodeAt(dpostcode.length-3) > 57) {alert ("The delivery postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else if (bpostcode.length < 5 || bpostcode > 7) {alert("The billing postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
										else if (bpostcode.charCodeAt(bpostcode.length-1) < 65 || bpostcode.charCodeAt(bpostcode.length-1) > 90) {alert ("The billing postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else if (bpostcode.charCodeAt(bpostcode.length-2) < 65 || bpostcode.charCodeAt(bpostcode.length-2) > 90) {alert ("The billing postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else if (bpostcode.charCodeAt(bpostcode.length-3) < 48 || bpostcode.charCodeAt(bpostcode.length-3) > 57) {alert ("The billing postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else {
											if (error == 0) return true;
										}
									}

								} else {
									if (form.firstname.value == "") {alert("Please enter your First Name. Contact details are required for our carriers to deliver your order.");error = 1;}
									else if (form.surname.value == "" && error == 0) {alert("Please enter your Surname. Contact details are required for our carriers to deliver your order.");error = 1;}
									else if (form.tel.value == "" && error == 0) {alert("Please enter a Phone Number. Contact details are required for our carriers to deliver your order.");error = 1;}
									else if (form.email.value == "" && error == 0) {alert("Please enter a valid email address. Contact details are required for our carriers to deliver your order."); error = 1;}
									else if (form.email.value != form.cemail.value && error == 0) {alert("Your Email Address does not match the confirmation."); error = 1;}

									if (form.address.value == "" && error == 0) {alert("Please enter the Delivery Address. These details are required for our carriers to deliver your order.");error = 1;}
									else if (form.county.value == "" && error == 0) {alert("Please enter the Delivery Address County. These details are required for our carriers to deliver your order.");error = 1;}
									else if (form.postcode.value == "" && error == 0) {alert("Please enter the Delivery Address Post Code. These details are required for our carriers to deliver your order.");error = 1;}
									else if (error == 0) {
										var postcode = form.postcode.value;

										var pcode = postcode.slice(0,2);
										pcode = pcode.toUpperCase();
										var highlandsnislnds = new Array("GY", "BT", "IM", "HS", "ZE", "JE");
										for (var i = 0; i <highlandsnislnds.length; i = i+ 1)
										{
											if (pcode == highlandsnislnds[i])
											{
												alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
												error = 1;
											}
										}

										var pcode2 = postcode.slice(0,4);
										pcode2 = pcode2.toUpperCase();
										var highlandsnislnds2 = new Array("IV41", "IV42", "IV43", "IV44", "IV45", "IV46", "IV47", "IV48", "IV49", "IV50", "IV51", "IV52", "IV53", "IV54", "IV55", "IV56", "KA27", "KA28", "PA20", "PA41", "PA42", "PA43", "PA44", "PA45", "PA46", "PA47", "PA48", "PA49", "PA62", "PA63", "PA64", "PA65", "PA66", "PA67", "PA68", "PA69", "PA70", "PA71", "PA72", "PA73", "PA74", "PA75", "PA76", "PH42", "PH43", "PH44", "PO31", "PO32","PO33","PO34","PO35","PO36", "PO37", "PO38", "PO39","PO40", "PO41");
										for (var i = 0; i <highlandsnislnds2.length; i = i+ 1)
										{
											if (pcode2 == highlandsnislnds2[i])
											{
												alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
												error = 1;
											}
										}

										postcode = postcode.replace(" ", "");
										postcode = postcode.toUpperCase();
										if (postcode.length < 5 || postcode > 7) {alert("The postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
										else if (postcode.charCodeAt(postcode.length-1) < 65 || postcode.charCodeAt(postcode.length-1) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else if (postcode.charCodeAt(postcode.length-2) < 65 || postcode.charCodeAt(postcode.length-2) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else if (postcode.charCodeAt(postcode.length-3) < 48 || postcode.charCodeAt(postcode.length-3) > 57) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
										else {
											if (error == 0) return true;
										}
									}
								}
							}
							return false;
						}
						function jsfindaddress (){
							document.getElementById("ajax_address").style.display = "block";
							var postcode = document.form.postcode.value;
							var request = "/ajax/ajaxaddress.php";
							var vars = "postcode="+postcode;
							var xmlhttp;
							if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
								xmlhttp=new XMLHttpRequest();
							} else {// code for IE6, IE5
								xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							}

							if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

							xmlhttp.open("POST",request,true);
							xmlhttp.onreadystatechange=function() {
								if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									var xml = xmlhttp.responseXML;
									if(xml.getElementsByTagName("ErrorNumber")[0].childNodes[0].nodeValue == 0) {
										if(xml.getElementsByTagName("Address2").length != 0)document.form.address2.value= xml.getElementsByTagName("Address2")[0].childNodes[0].nodeValue;
										if(xml.getElementsByTagName("Address3").length != 0)document.form.address3.value= xml.getElementsByTagName("Address3")[0].childNodes[0].nodeValue;
										document.form.town.value= xml.getElementsByTagName("Town")[0].childNodes[0].nodeValue;
										document.form.county.value= xml.getElementsByTagName("County")[0].childNodes[0].nodeValue;
										var resultlist =  xml.getElementsByTagName("PremiseData")[0].childNodes[0].nodeValue;
										var arrlist = resultlist.split(";");
										var addlist = "<ul >";
										for (var i = 0; i < arrlist.length; i++) {
											if(arrlist[i] != "") {
												arrlist[i] = arrlist[i].replace("||","|");
												arrlist[i] = arrlist[i].replace("|",", ");
												arrlist[i] = arrlist[i].replace("|",", ");
												var straddress =arrlist[i]+" "+xml.getElementsByTagName("Address1")[0].childNodes[0].nodeValue;
												straddress = straddress.replace(/(^,)|(,$)/g, "");
												straddress = straddress.replace(/(^ )|(,$)/g, "");
												addlist += "<li><a href='javascript:jsaddsel(\""+straddress+"\")'>"+straddress+"</a></li>";
											}
										}
										addlist += "</ul>";
										document.getElementById("ajax_address").innerHTML = addlist;

									} else {
										document.getElementById("ajax_address").innerHTML = ""+xml.getElementsByTagName("ErrorMessage")[0].childNodes[0].nodeValue+"";
									}

								} else if (xmlhttp.readyState==4) {
									document.getElementById("ajax_address").innerHTML = "There has been an error updating your address please try again.";

								}
							}
							xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
							xmlhttp.send(vars);
							document.getElementById("ajax_address").innerHTML = " <img src='/images/ajax-loader.gif' /> <span> Search...</span> ";
						}
						function jsaddsel(address){
							document.form.address.value= address;
							document.getElementById("ajax_address").style.display = "none";

						}
						function jsfindbaddress (){
							
							document.getElementById("ajax_baddress").style.display = "block";
							var postcode = document.form.bpostcode.value;
							var request = "/ajax/ajaxaddress.php";
							var vars = "postcode="+postcode;
							var xmlhttp;
							if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
								xmlhttp=new XMLHttpRequest();
							} else {// code for IE6, IE5
								xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
							}

							if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

							xmlhttp.open("POST",request,true);
							xmlhttp.onreadystatechange=function() {
								if (xmlhttp.readyState==4 && xmlhttp.status==200)
								{
									var xml = xmlhttp.responseXML;
									if(xml.getElementsByTagName("ErrorNumber")[0].childNodes[0].nodeValue == 0) {
										if(xml.getElementsByTagName("Address2").length != 0)document.form.baddress2.value= xml.getElementsByTagName("Address2")[0].childNodes[0].nodeValue;
										if(xml.getElementsByTagName("Address3").length != 0)document.form.baddress3.value= xml.getElementsByTagName("Address3")[0].childNodes[0].nodeValue;
										document.form.btown.value= xml.getElementsByTagName("Town")[0].childNodes[0].nodeValue;
										document.form.bcounty.value= xml.getElementsByTagName("County")[0].childNodes[0].nodeValue;
										var resultlist =  xml.getElementsByTagName("PremiseData")[0].childNodes[0].nodeValue;
										var arrlist = resultlist.split(";");
										var addlist = "<ul >";
										for (var i = 0; i < arrlist.length; i++) {
											if(arrlist[i] != "") {
												arrlist[i] = arrlist[i].replace("||","|");
												arrlist[i] = arrlist[i].replace("|",", ");
												arrlist[i] = arrlist[i].replace("|",", ");

												var straddress =arrlist[i]+" "+xml.getElementsByTagName("Address1")[0].childNodes[0].nodeValue;
												straddress = straddress.replace(/(^,)|(,$)/g, "");
												straddress = straddress.replace(/(^ )|(,$)/g, "");
												addlist += "<li><a href='javascript:jsbaddsel(\""+straddress+"\")'>"+straddress+"</a></li>";
											}
										}
										addlist += "</ul>";
										document.getElementById("ajax_baddress").innerHTML = addlist;

									} else {
										document.getElementById("ajax_baddress").innerHTML = xml.getElementsByTagName("ErrorMessage")[0].childNodes[0].nodeValue;
										
									}

								} else if (xmlhttp.readyState==4) {
									document.getElementById("ajax_baddress").innerHTML = " <span>There has been an error updating your basket please try again.</span> ";

								}
							}
							xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
							xmlhttp.send(vars);
							document.getElementById("ajax_baddress").innerHTML = " <img src='/images/ajax-loader.gif' /> <span> Search...</span> ";
						}
						function jsbaddsel(address){
							document.form.baddress.value= address;
							document.getElementById("ajax_baddress").style.display = "none";
						}
						function jsaddtype(el){
							if (el.checked == false) {
								var elements = document.getElementsByClass('combi-hidden');
								for (var i = 0; i < elements.length; i++) {
									if (elements.hasOwnProperty(i)) {
										elements[i].className = 'combi-show';
									}
								}
								document.getElementById('deladdress').style.width = '460px';
								document.form.addtype.value = 1;
							} else {
								var elements = document.getElementsByClass('combi-show');
								for (var i = 0; i < elements.length; i++) {
									if (elements.hasOwnProperty(i)) {
										elements[i].className = 'combi-hidden';
									}
								}

								document.getElementById('deladdress').style.position = '';
								document.getElementById('deladdress').style.width = '';
								document.form.addtype.value = 0;
							}
						}
						function jsregtype(el){
							if (el.checked == false) {
								var elements = document.getElementsByClass('guest-show');
								for (var i = 0; i < elements.length; i++) {
									if (elements.hasOwnProperty(i)) {
										elements[i].className = 'guest-hidden';
									}
								}
								document.form.regtype.value = 0;
							} else {
								var elements = document.getElementsByClass('guest-hidden');
								for (var i = 0; i < elements.length; i++) {
									if (elements.hasOwnProperty(i)) {
										elements[i].className = 'guest-show';
									}
								}
								document.form.regtype.value = 1;
							}
						}
						
						/* Confirmation functions */
						function jsupdate(updateForm){
							if(updateForm == "changebilladdress")
							{
								var firstname = document.form.bfirstname.value;
								var surname = document.form.bsurname.value;
								var address1 = document.form.baddress1.value;
								var town = document.form.btown.value;
								var postcode = document.form.bpostcode.value;
							}
							else
							{
								var firstname = document.form.firstname.value;
								var surname = document.form.surname.value;
								var address1 = document.form.address1.value;
								var town = document.form.town.value;
								var postcode = document.form.postcode.value;
							}
							if (firstname == "") {alert("Please enter your firstname.");}
							else if (surname == "") {alert("Please enter your surname.");}
							else if (address1 == "") {alert("Please enter the first line of your address.");}
							else if (town == "") {alert("Please enter your town or city.");}
							else if (postcode == "") {alert("Please enter your post code.");}
							else {
								var pcode = postcode.slice(0,2);
								pcode = pcode.toUpperCase();
								var highlandsnislnds = new Array("GY", "BT", "IM", "HS", "ZE", "JE");
								for (var i = 0; i <highlandsnislnds.length; i = i+ 1)
								{
									if (pcode == highlandsnislnds[i])
									{
										alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
								}

								var pcode2 = postcode.slice(0,4);
								pcode2 = pcode2.toUpperCase();
								var highlandsnislnds2 = new Array("IV41", "IV42", "IV43", "IV44", "IV45", "IV46", "IV47", "IV48", "IV49", "IV50", "IV51", "IV52", "IV53", "IV54", "IV55", "IV56", "KA27", "KA28", "PA20", "PA41", "PA42", "PA43", "PA44", "PA45", "PA46", "PA47", "PA48", "PA49", "PA62", "PA63", "PA64", "PA65", "PA66", "PA67", "PA68", "PA69", "PA70", "PA71", "PA72", "PA73", "PA74", "PA75", "PA76", "PH42", "PH43", "PH44", "PO31", "PO32","PO33","PO34","PO35","PO36", "PO37", "PO38", "PO39","PO40", "PO41");
								for (var i = 0; i <highlandsnislnds2.length; i = i+ 1)
								{
									if (pcode2 == highlandsnislnds2[i])
									{
										alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
								}

								postcode = postcode.replace(" ", "");
								postcode = postcode.toUpperCase();
								if (postcode.length < 5 || postcode > 7) {alert("The postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
								else if (postcode.charCodeAt(postcode.length-1) < 65 || postcode.charCodeAt(postcode.length-1) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
								else if (postcode.charCodeAt(postcode.length-2) < 65 || postcode.charCodeAt(postcode.length-2) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
								else if (postcode.charCodeAt(postcode.length-3) < 48 || postcode.charCodeAt(postcode.length-3) > 57) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three characters with an option space before the last three characters.");}
								else {
									return true;
								}
							}
							return false;
						}
						function jsupdateadd(type){
							document.getElementById("cmd").value = "changeaddress_"+type;
							document.getElementById("form").submit();
						}
						function jscancel(type){
							document.getElementById('hidden_'+type).style.display ='none';
							document.getElementById('current_'+type).style.display ='block';
						}
						function jsaddress(type){
							document.getElementById('hidden_'+type).style.display ='block';
							document.getElementById('current_'+type).style.display ='none';
						}

					</script>
<?php
				
					//location checks
					if ($strcmd == "accountType" || $strcmd == "accountDetails") { //check to see if we have an active customer ID 
						if ($intuserid > 0) {
							$strcmd = "confirmDetails";
						}
					} else if ($strcmd == "confirmDetails") {
						if ($intuserid == 0) {
							$strcmd = "";
						}
					}

					print ("<form action='checkout.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
						print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' />");
						print ("<input type='hidden' class='nodisable' name='intstep' id='intstep' value='$intstep'/>");
						print ("<input type='hidden' class='nodisable' name='intnewqty' id='intnewqty' />");
						print ("<input type='hidden' class='nodisable' name='offercode' id='offercode' />");
						print ("<input type='hidden' class='nodisable' name='accountType' id='accountType' value='$accountType' />");
						
						switch ($strcmd){
										
							case "accountType":
							
								fnGetAccountType ($strloginmessage);
							
							break;
							
							case "accountDetails":
							
								fnGetAccountDetails($accountType, $_POST, $conn);

							break;
							
							case "confirmDetails":
								fnGetFinalCustomerDetails($basketHeader,$intuserid, $conn);
								
								fnGetFinalBasket($basketHeader,$basketItems,$strselectedpostage, "checkout", $conn);
							
							break;

							default:

								fnGetBasket ($basketHeader, $basketItems, $companyPhone, $companyEmail, $conn);
								
							break;
						}
					print ("</form>");
		
				print ("</div>");
		
				include ("/includes/sidebar.php");
				
			print ("</div>");

	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>