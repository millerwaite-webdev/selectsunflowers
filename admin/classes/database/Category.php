<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 16:00
 */

class Category
{
    /**
     * @param CategoryDetail $categoryDetail
     * @return array|string
     */
    public static function createCategory(CategoryDetail $categoryDetail)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO category (contentName, contentText, contentBanner, contentSidebar, contentMenuImage, metaTitle, metaPageLink, metaDescription, metaKeywords, showProducts, showCategories, showBrands, showRanges, showFilters)
                     VALUES (:contentName, :contentText, :contentBanner, :contentSidebar, :contentMenuImage, :metaTitle, :metaPageLink, :metaDescription, :metaKeywords, :showProducts, :showCategories, :showBrands, :showRanges, :showFilters);";
        $arrdbparams['contentName'] = $categoryDetail->contentName;
        $arrdbparams['contentText'] = $categoryDetail->contentText;
        $arrdbparams['contentBanner'] = $categoryDetail->contentBanner;
        $arrdbparams['contentSidebar'] = $categoryDetail->contentSidebar;
        $arrdbparams['contentMenuImage'] = $categoryDetail->contentMenuImage;
        $arrdbparams['metaTitle'] = $categoryDetail->metaTitle;
        $arrdbparams['metaPageLink'] = $categoryDetail->metaPageLink;
        $arrdbparams['metaDescription'] = $categoryDetail->metaDescription;
        $arrdbparams['metaKeywords'] = $categoryDetail->metaKeywords;
        $arrdbparams['showProducts'] = $categoryDetail->showProducts;
        $arrdbparams['showCategories'] = $categoryDetail->showCategories;
        $arrdbparams['showBrands'] = $categoryDetail->showBrands;
        $arrdbparams['showRanges'] = $categoryDetail->showRanges;
        $arrdbparams['showFilters'] = $categoryDetail->showFilters;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }

    /**
     * @param $name
     * @return CategoryDetail|null
     */
    public static function getCategory($name)
    {
        // get data
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT recordID, contentName, contentText, contentBanner, contentSidebar, contentMenuImage, metaTitle, metaPageLink, metaDescription, metaKeywords, showProducts, showCategories, showBrands, showRanges, showFilters FROM category WHERE contentName = :contentName";
        $arrdbparams['contentName'] = $name;
        $strType = "multi";
        $returnedcategories = query($conn, $strdbsql, $strType, $arrdbparams);
        // build object
        if (count($returnedcategories) == 0)
            return null;
        $createdcategory = $returnedcategories[0];
        $categorydetail = new CategoryDetail(
            $createdcategory["contentName"],
            $createdcategory["contentText"],
            $createdcategory["contentBanner"],
            $createdcategory["contentSidebar"],
            $createdcategory["contentMenuImage"],
            $createdcategory["metaTitle"],
            $createdcategory["metaPageLink"],
            $createdcategory["metaDescription"],
            $createdcategory["metaKeywords"],
            $createdcategory["showProducts"],
            $createdcategory["showCategories"],
            $createdcategory["showBrands"],
            $createdcategory["showRanges"],
            $createdcategory["showFilters"]
        );
        $categorydetail->recordId = $createdcategory["recordID"];
        return $categorydetail;
    }

    public static function checkCategoryExists($name)
    {
        $categories = Category::getCategory($name);
        return ($categories != null);
    }
}

class CategoryDetail
{
    public $recordId;
    public $contentName;
    public $contentText;
    public $contentBanner;
    public $contentSidebar;
    public $contentMenuImage;
    public $metaTitle;
    public $metaPageLink;
    public $metaDescription;
    public $metaKeywords;
    public $showProducts;
    public $showCategories;
    public $showBrands;
    public $showRanges;
    public $showFilters;
    /**
     * @var // extra magento category id
     */
    public $magentoId;

    public static function constructWithOnlyName($contentName)
    {
        $instance = new self();
        $instance->contentName = $contentName;
        return $instance;
    }

    public function __construct($contentName, $contentText, $contentBanner, $contentSidebar, $contentMenuImage, $metaTitle, $metaPageLink, $metaDescription, $metaKeywords, $showProducts, $showCategories, $showBrands, $showRanges, $showFilters)
    {
        $this->contentName = $contentName;
        $this->contentText = $contentText;
        $this->contentBanner = $contentBanner;
        $this->contentSidebar = $contentSidebar;
        $this->contentMenuImage = $contentMenuImage;
        $this->metaTitle = $metaTitle;
        $this->metaPageLink = $metaPageLink;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
        $this->showProducts = $showProducts;
        $this->showCategories = $showCategories;
        $this->showBrands = $showBrands;
        $this->showRanges = $showRanges;
        $this->showFilters = $showFilters;
    }
}