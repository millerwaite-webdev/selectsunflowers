<?php namespace Database;

use Classes\NotImplementedException;

/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 10/07/2015
 * Time: 11:32
 */

class StockAttribute
{
    public static function createAttribute($description, $typeId)
    {
        throw new NotImplementedException();
    }

    /**
     * @param $stockId
     * @param $attributeDescription
     * @param StockAttributeTypeDetail $attribute
     * @return array // created ids
     */
    public static function assignProductAttribute($stockId, $attributeDescription, StockAttributeTypeDetail $attribute)
    {
        // insert attribute
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO stock_attributes (description, swatchImage, attributeTypeID, shortCode, hexcode)
                     VALUES (:description, :swatchImage, :attributeTypeID, :shortCode, :hexCode)";
        $arrdbparams['description'] = $attributeDescription;
        $arrdbparams['swatchImage'] = '';
        $arrdbparams['attributeTypeID'] = $attribute->recordId;
        $arrdbparams['shortCode'] = '';
        $arrdbparams['hexCode'] = '';
        $strType = "insert";
        $createdstockattribute = query($conn, $strdbsql, $strType, $arrdbparams);
        // do association
        unset($arrdbparams);
        $strdbsql = "INSERT INTO stock_attribute_relations (stockAttributeID, stockID) VALUES (:stockAttributeID, :stockID)";
        $arrdbparams['stockAttributeID'] = $createdstockattribute;
        $arrdbparams['stockID'] = $stockId;
        $strType = "insert";
        $stockattributerelation = query($conn, $strdbsql, $strType, $arrdbparams);
        return Array($createdstockattribute, $stockattributerelation);
    }

    /**
     * @param $description
     * @return bool
     */
    public static function checkAttributeExists($description)
    {
        $attribute = StockAttribute::getAttributeType($description);
        return ($attribute != null);
    }

    /**
     * @param $description
     * @return StockAttributeTypeDetail|null
     */
    public static function getAttributeType($description)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT recordID, description, attributeTypeOrder FROM stock_attribute_type WHERE description = :description";
        $arrdbparams['description'] = $description;
        $strType = "multi";
        $categories = query($conn, $strdbsql, $strType, $arrdbparams);
        if (count($categories) == 0)
            return null;
        $category = $categories[0];
        return new StockAttributeTypeDetail(
            $category['recordID'],
            $category['description'],
            $category['attributeTypeOrder']);
    }
}

class StockAttributeDetail
{
    public $recordId;
    public $description;
    public $swatchImage;
    public $attributeTypeId;
    public $shortCode;
    public $hexCode;

    public function __constuct($recordID, $description, $swatchImage, $attributeTypeId, $shortCode, $hexCode)
    {
        $this->recordId = $recordID;
        $this->description = $description;
        $this->swatchImage = $swatchImage;
        $this->attributeTypeId = $attributeTypeId;
        $this->shortCode = $shortCode;
        $this->hexCode = $hexCode;
    }
}

class StockAttributeTypeDetail
{
    public $recordId;
    public $description;
    public $attributeTypeOrder;

    public function __construct($recordId, $description, $attributeTypeOrder)
    {
        $this->recordId = $recordId;
        $this->description = $description;
        $this->attributeTypeOrder = $attributeTypeOrder;
    }
}