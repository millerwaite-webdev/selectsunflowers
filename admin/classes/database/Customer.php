<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 13/07/2015
 * Time: 12:27
 */

class Customer
{
    public static function addCustomer(CustomerDetail $customerDetail)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        // customer
        $strdbsql = "INSERT INTO customer (groupID, username, password, title, firstname, surname, company, telephone, mobile, email, fax, notes)
                     VALUES (:groupID, :username, :password, :title, :firstname, :surname, :company, :telephone, :mobile, :email, :fax, :notes);";
        $arrdbparams['groupID'] = $customerDetail->groupID;
        $arrdbparams['username'] = $customerDetail->username;
        $arrdbparams['password'] = $customerDetail->password;
        $arrdbparams['title'] = $customerDetail->title;
        $arrdbparams['firstname'] = $customerDetail->firstname;
        $arrdbparams['surname'] = $customerDetail->surname;
        $arrdbparams['company'] = $customerDetail->company;
        $arrdbparams['telephone'] = $customerDetail->telephone;
        $arrdbparams['mobile'] = $customerDetail->mobile;
        $arrdbparams['email'] = $customerDetail->email;
        $arrdbparams['fax'] = $customerDetail->fax;
        $arrdbparams['notes'] = $customerDetail->notes;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }

    public static function updateCustomerAddressDefaults($customerId, $defaultBillingAdd, $defaultDeliveryAdd)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "UPDATE customer SET defaultDeliveryAdd = :defaultDeliveryAdd, defaultBillingAdd = :defaultBillingAdd WHERE recordID = :customerId";
        $arrdbparams['customerId'] = $customerId;
        $arrdbparams['defaultBillingAdd'] = $defaultBillingAdd;
        $arrdbparams['defaultDeliveryAdd'] = $defaultDeliveryAdd;
        $strType = "update";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }

    public static function addCustomerAddress($customerId, CustomerAddressDetail $customerAddressDetail)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, country, postcode, notes)
                     VALUES (:customerID, :addDescription, :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :country, :postcode, :notes);";
        $arrdbparams['customerID'] = $customerId;
        $arrdbparams['addDescription'] = $customerAddressDetail->addDescription;
        $arrdbparams['title'] = $customerAddressDetail->title;
        $arrdbparams['firstname'] = $customerAddressDetail->firstname;
        $arrdbparams['surname'] = $customerAddressDetail->surname;
        $arrdbparams['add1'] = $customerAddressDetail->add1;
        $arrdbparams['add2'] = $customerAddressDetail->add2;
        $arrdbparams['add3'] = $customerAddressDetail->add3;
        $arrdbparams['town'] = $customerAddressDetail->town;
        $arrdbparams['county'] = $customerAddressDetail->county;
        $arrdbparams['country'] = $customerAddressDetail->country;
        $arrdbparams['postcode'] = $customerAddressDetail->postcode;
        $arrdbparams['notes'] = $customerAddressDetail->notes;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }
}

class CustomerDetail
{
    public $recordID;
    public $groupID;
    public $username;
    public $password;
    public $title;
    public $firstname;
    public $surname;
    public $company;
    public $telephone;
    public $mobile;
    public $email;
    public $fax;
    public $defaultBillingAdd;
    public $defaultDeliveryAdd;
    public $notes;

    /**
     * @param $groupID
     * @param $username
     * @param $password
     * @param $title
     * @param $firstname
     * @param $surname
     * @param $company
     * @param $telephone
     * @param $mobile
     * @param $email
     * @param $fax
     * @param $notes
     */
    public function __construct(
        $groupID, $username, $password, $title, $firstname, $surname, $company, $telephone, $mobile, $email, $fax, $notes)
    {
        $this->groupID = $groupID;
        $this->username = $username;
        $this->password = $password;
        $this->title = $title;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->company = $company;
        $this->telephone = $telephone;
        $this->mobile = $mobile;
        $this->email = $email;
        $this->fax = $fax;
        $this->notes = $notes;
    }
}

class CustomerAddressDetail
{
    public $recordId;
    public $customerID;
    public $addDescription;
    public $title;
    public $firstname;
    public $surname;
    public $add1;
    public $add2;
    public $add3;
    public $town;
    public $county;
    public $country;
    public $postcode;
    public $notes;

    /**
     * @param $addDescription
     * @param $title
     * @param $firstname
     * @param $surname
     * @param $add1
     * @param $add2
     * @param $add3
     * @param $town
     * @param $county
     * @param $country
     * @param $postcode
     * @param $notes
     */
    public function __construct(
        $addDescription, $title, $firstname, $surname,
        $add1, $add2, $add3, $town, $county, $country, $postcode, $notes)
    {
        $this->addDescription = $addDescription;
        $this->title = $title;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->add1 = $add1;
        $this->add2 = $add2;
        $this->add3 = $add3;
        $this->town = $town;
        $this->county = $county;
        $this->country = $country;
        $this->postcode = $postcode;
        $this->notes = $notes;
    }
}