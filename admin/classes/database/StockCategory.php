<?php
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 14/07/2015
 * Time: 15:00
 */

namespace Database;


class StockCategory
{
    /**
     * @param $stockGroupId
     * @param $categoryId
     * @param $stockOrder
     * @return array|string
     */
    public static function createCategoryStockRelation($stockGroupId, $categoryId, $stockOrder)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO stock_category_relations (stockID, categoryID, stockOrder) VALUES (:stockID, :categoryID, :stockOrder);";
        $arrdbparams['stockID'] = $stockGroupId;
        $arrdbparams['categoryID'] = $categoryId;
        $arrdbparams['stockOrder'] = $stockOrder;
        $strType = "insert";
        $affectedrows = query($conn, $strdbsql, $strType, $arrdbparams);
        return $affectedrows;
    }

    /**
     * @param $parentId
     * @param $childId
     * @param $menuOrder
     * @param $level
     * @return array|string
     */
    public static function createCategoryRelation($parentId, $childId, $menuOrder, $level)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO category_relations (parentID, childID, menuOrder, level)
                     VALUES (:parentID, :childID, :menuOrder, :catlevel)";
        $arrdbparams['parentID'] = $parentId;
        $arrdbparams['childID'] = $childId;
        $arrdbparams['menuOrder'] = $menuOrder;
        $arrdbparams['catlevel'] = $level;
        $strType = "insert";
        $affectedrows = query($conn, $strdbsql, $strType, $arrdbparams);
        return $affectedrows;
    }

    /**
     * Update all category relation values by one
     *
     * @return array|string
     */
    public static function amendCategoryRelationValues()
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "UPDATE category_relations SET level = level + 1";
        $strType = "update";
        $affectedrows = query($conn, $strdbsql, $strType);
        return $affectedrows;
    }
}