<?php namespace Database;
use SplObjectStorage;

/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 13/07/2015
 * Time: 10:51
 */

class StockCustomField
{
    /**
     * @param $stockId
     * @param CustomFieldDetail $customFieldData
     * @param $fieldvalue
     * @return array|string
     */
    public static function assignProductField($stockId, CustomFieldDetail $customFieldData, $fieldvalue)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO stock_custom_field_values (stockId, customFieldID, value) VALUES (:stockId, :customFieldId, :value)";
        $arrdbparams['stockId'] = $stockId;
        $arrdbparams['customFieldId'] = $customFieldData->recordId;
        $arrdbparams['value'] = $fieldvalue;
        $strType = "insert";
        $result = query($conn, $strdbsql, $strType, $arrdbparams);
        return $result;
    }

    /**
     * @param $description
     * @return CustomFieldDetail|null
     */
    public static function getField($description)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT recordID, description, defaultValue FROM stock_custom_fields WHERE description = :description;";
        $arrdbparams['description'] = $description;
        $strType = "multi";
        $fieldrows = query($conn, $strdbsql, $strType, $arrdbparams);
        if (count($fieldrows) == 0)
            return null;
        $fieldrow = $fieldrows[0];
        return new CustomFieldDetail(
            $fieldrow['recordID'],
            $fieldrow['description'],
            $fieldrow['defaultValue']
        );
    }
}

class CustomFieldDetail
{
    public $recordId;
    public $description;
    public $defaultValue;

    public function __construct($recordId, $description, $defaultValue)
    {
        $this->recordId = $recordId;
        $this->description = $description;
        $this->defaultValue = $defaultValue;
    }
}