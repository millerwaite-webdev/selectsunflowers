<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * register.php                                                       * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "register"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
		
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	//	Requests

	$strcmd = $_REQUEST['command'];
	if ($_REQUEST['referer'] == "")
	{
	    $strreferer = "account.php";
	}
	else
	{
	    $strreferer = $_REQUEST['referer'];
	}


	$strerror = "";

	switch ($strcmd)
	{
		case "createac":
			$strusername = $_REQUEST['username'];
			$strfirstname = $_REQUEST['firstname'];
			$strsurname = $_REQUEST['surname'];
			$strtitle = $_REQUEST['title'];
			$strcompany = $_REQUEST['company'];
			$strphone = $_REQUEST['phone'];
			$stremail = $_REQUEST['email'];
			$strfax = $_REQUEST['fax'];
			$strdelfirstname = $_REQUEST['delfirstname'];
			$strdelsurname = $_REQUEST['delsurname'];
			$strdeltitle = $_REQUEST['deltitle'];
			$strdeladd1 = $_REQUEST['deladd1'];
			$strdeladd2 = $_REQUEST['deladd2'];
			$strdeladd3 = $_REQUEST['deladd3'];
			$strdelTown = $_REQUEST['delTown'];
			$strdelCnty = $_REQUEST['delCnty'];
			$strdelPstCde = $_REQUEST['delPstCde'];
			$strbillfirstname = $_REQUEST['bilfirstname'];
			$strbillsurname = $_REQUEST['bilsurname'];
			$strbilltitle = $_REQUEST['biltitle'];
			$strbilladd1 = $_REQUEST['biladd1'];
			$strbilladd2 = $_REQUEST['biladd2'];
			$strbilladd3 = $_REQUEST['biladd3'];
			$strbillTown = $_REQUEST['bilTown'];
			$strbillCnty = $_REQUEST['bilCnty'];
			$strbillPstCde = $_REQUEST['bilPstCde'];
			$strpassword = $_REQUEST['password'];

			$intaddtype = intval($_REQUEST['addtype']);
			if (isset($_REQUEST['newsletter'])) $intnewsletter = 1;
			else $intnewsletter = 0;
			
			$checkUsernameQuery = "SELECT COUNT(*) AS num_accounts FROM customer WHERE username = :username";
			$strType = "single";
			$arrdbparam = array( "username" => $strusername );
			$checkUsername = query($conn, $checkUsernameQuery, $strType, $arrdbparam);

			if ($checkUsername['num_accounts'] != 0)
			{
				$strerror = "Sorry - This username is already in use.";
				$strcmd = "";
			}
			else
			{

				if (check_email_address($stremail))
				{
					//create account
					$createAccountQuery = "INSERT INTO customer (groupID, username, password, title, firstname, surname, company, telephone, email, fax) VALUES ('1', :username, :password, :title, :firstname, :surname, :company, :telephone, :email, :fax)";
					$strType = "insert";
					$arrdbparams = array(
										"username" => $strusername,
										"password" => md5($strpassword),
										"title" => $strtitle,
										"firstname" => $strfirstname,
										"surname" => $strsurname,
										"company" => $strcompany,
										"telephone" => $strphone,
										"email" => $stremail,
										"fax" => $strfax
									);
					$customerAccountID = query($conn, $createAccountQuery, $strType, $arrdbparams);
					
					if($customerAccountID)
					{	
						if($intaddtype == 1)
						{
							//create account with two addresses
							$insertAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Delivery Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
							$strType = "insert";
							$arrdbparams = array(
												"customerID" => $customerAccountID,
												"title" => $strdeltitle,
												"firstname" => $strdelfirstname,
												"surname" => $strdelsurname,
												"add1" => $strdeladd1,
												"add2" => $strdeladd2,
												"add3" => $strdeladd3,
												"town" => $strdelTown,
												"county" => $strdelCnty,
												"postcode" => $strdelPstCde
											);
							$delAddID = query($conn, $insertAddressQuery, $strType, $arrdbparams);
							
							$billAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Billing Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
							$strType = "insert";
							$arrdbparams = array(
												"customerID" => $customerAccountID,
												"title" => $strbilltitle,
												"firstname" => $strbillfirstname,
												"surname" => $strbillsurname,
												"add1" => $strbilladd1,
												"add2" => $strbilladd2,
												"add3" => $strbilladd3,
												"town" => $strbillTown,
												"county" => $strbillCnty,
												"postcode" => $strbillPstCde
											);
							$billAddID = query($conn, $billAddressQuery, $strType, $arrdbparams);
						}
						else
						{
							$insertAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Delivery Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
							$strType = "insert";
							$arrdbparams = array(
												"customerID" => $customerAccountID,
												"title" => $strtitle,
												"firstname" => $strfirstname,
												"surname" => $strsurname,
												"add1" => $strdeladd1,
												"add2" => $strdeladd2,
												"add3" => $strdeladd3,
												"town" => $strdelTown,
												"county" => $strdelCnty,
												"postcode" => $strdelPstCde
											);
							$delAddID = query($conn, $insertAddressQuery, $strType, $arrdbparams);
							
							$billAddID = $delAddID;
						}
						
						$values = array(
							"billingadd" => $billAddID,
							"deliveryadd" => $delAddID,
							"custid" => $customerAccountID
						);
						
						$assignCustomerAddresses = updateUserBillDelAddresses($values, "billanddel", $conn);
						
						if($intnewsletter == 1)
						{
							$subscriptionCheck = getNewsletterSubscription($stremail, "byemail", $conn);
							
							if(empty($subscriptionCheck))
							{
								$name = $strtitle." ".$strfirstname." ".$strsurname;
								$addSubscription = insertNewsletterSubscription($customerAccountID, $name, $stremail, $conn);
							}
						}

						//send email confirmation
						$strmessage = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>";
						$strmessage = $strmessage."<html><head><title>Welcome to jhbenson.co.uk</title></head><body><img src='http://jhbenson.co.uk/images/logo.png' /><br/><br/><br/>";
						$strmessage = $strmessage."<p><b>Thank you for Registering for an account with <a href='http://jhbenson.co.uk'>jhbenson.co.uk</a>.</b></p>";
						$strmessage = $strmessage."<p>Your account details are given below. Please keep a record of your username and password as you will require these to log onto the site.</p>";
						$strmessage = $strmessage."<table width='500px'><tr><td width='40%'>User Name</td><td width='60%'>$strusername</td></tr>";
						$strmessage = $strmessage."<tr><td>Password</td><td>$strpassword</td></tr>";
						$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
						$strmessage = $strmessage."<tr><td>First Name</td><td>$strfirstname</td></tr>";
						$strmessage = $strmessage."<tr><td>Last Name</td><td>$strsurname</td></tr>";
						$strmessage = $strmessage."<tr><td>Title</td><td>$strtitle</td></tr>";
						$strmessage = $strmessage."<tr><td>Company</td><td>$strcompany</td></tr>";
						$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
						$strmessage = $strmessage."<tr><td>Phone Number</td><td>$strphone</td></tr>";
						$strmessage = $strmessage."<tr><td>Email</td><td>$stremail</td></tr>";
						$strmessage = $strmessage."<tr><td>Fax</td><td>$strfax</td></tr>";
						$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";

						if ($intaddtype ==1 ) {
							$strmessage = $strmessage."<tr><td>Address</td><td>$strdeladd1</td></tr>";
							if ($strdeladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd2</td></tr>"; }
							if ($strdeladd3 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd3</td></tr>"; }
							if ($strdelTown != "") {$strmessage = $strmessage."<tr><td></td><td>$strdelTown</td></tr>"; }
							$strmessage = $strmessage."<tr><td></td><td>$strdelCnty</td></tr>";
							$strmessage = $strmessage."<tr><td></td><td>$strdelPstCde</td></tr>";
							$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
						} else {
							$strmessage = $strmessage."<tr><td>Delivery Address</td><td>$strdelname</td>";
							$strmessage = $strmessage."<tr><td></td><td>$strdeladd1</td></tr>";
							if ($strdeladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd2</td></tr>"; }
							if ($strdeladd3 != "") {$strmessage = $strmessage."<tr><td></td><td>$strdeladd3</td></tr>"; }
							if ($strdelTown != "") {$strmessage = $strmessage."<tr><td></td><td>$strdelTown</td></tr>"; }
							$strmessage = $strmessage."<tr><td></td><td>$strdelCnty</td></tr>";
							$strmessage = $strmessage."<tr><td></td><td>$strdelPstCde</td></tr>";

							$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
							$strmessage = $strmessage."<tr><td>Billing Address</td><td>$strbillname</td>";
							$strmessage = $strmessage."<tr><td></td><td>$strbilladd1</td></tr>";
							if ($strbilladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strbilladd2</td></tr>"; }
							if ($strbilladd2 != "") {$strmessage = $strmessage."<tr><td></td><td>$strbilladd2</td></tr>"; }
							if ($strbillTown != "") {$strmessage = $strmessage."<tr><td></td><td>$strbillTown</td></tr>"; }
							$strmessage = $strmessage."<tr><td></td><td>$strbillCnty</td></tr>";
							$strmessage = $strmessage."<tr><td></td><td>$strbillPstCde</td></tr>";
							$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
						}

						$strmessage = $strmessage."</table></body></html>";


						mail($stremail, "Welcome to jhbenson.co.uk",$strmessage, "MIME-Version: 1.0".PHP_EOL."X-Mailer: PHP/" . phpversion().PHP_EOL."Sensitivity: Personal".PHP_EOL."Return-Path:jhbenson.co.uk".PHP_EOL."From: Stollers UK <no-reply@jhbenson.co.uk>".PHP_EOL."Content-Type: text/html; charset=iso-8859-1");

						//set logged in cookie
						setcookie ("stoid", $intuserid, time() + 60*60*2, "/", ".jhbenson.co.uk");

						//forward user back to source page or to account options
						header ("Location: ".$strreferer);
						exit;
					} else {
						$strerror = "There was a problem creating your account please try again.";
						$strcmd = "";
					}
				}
				else
				{
					$strerror = "Sorry - Invalid Email Address - please re-enter";
					$strcmd = "";
				}
			}
			
			break;
		case "signout":

			$intuserid = -1;
			setcookie ("stoid", "$intuserid", time() + 60*60*2, "/", ".jhbenson.co.uk");

			break;
	}

	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	print ("<link rel='stylesheet' href='/css/jquery.fancybox.css?v=2.1.5' type='text/css' media='screen' />");
	print ("<link rel='stylesheet' href='/css/jquery.jqzoom.css' type='text/css' />");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
?>
<script type="Text/JavaScript">
<!--
	onload=function(){
		if (document.getElementsByClass == undefined) {
			document.getElementsByClass = function(className)
			{
				var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
				var allElements = document.getElementsByTagName("*");
				var results = [];

				var element;
				for (var i = 0; (element = allElements[i]) != null; i++) {
					var elementClass = element.className;
					if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
						results.push(element);
				}

				return results;
			}
		}
	}

	function jsaddtype(el)
	{
		if (el.value == 1) {
			var elements = document.getElementsByClass('multi-hidden');
			for (var i = 0; i < elements.length; i++) {
				if (elements.hasOwnProperty(i)) {
					elements[i].className = 'multi-show';
				}
			}
			var element = document.getElementById('deltable-hidden');
			if(element != null) element.id = 'deltable-show';
			document.getElementById("addLegend").innerHTML = "Delivery Address";
		} else {
			var elements = document.getElementsByClass('multi-show');
			for (var i = 0; i < elements.length; i++) {
				if (elements.hasOwnProperty(i)) {
					elements[i].className = 'multi-hidden';
				}
			}
			var element = document.getElementById('deltable-show');
			if(element != null) element.id = 'deltable-hidden';
			document.getElementById("addLegend").innerHTML = "Address";
		}
	}

function jscreateaccount(form)
{
		var highlandsnislnds = new Array("GY", "BT", "IM", "HS", "ZE", "JE");
		var highlandsnislnds2 = new Array("IV41", "IV42", "IV43", "IV44", "IV45", "IV46", "IV47", "IV48", "IV49", "IV50", "IV51", "IV52", "IV53", "IV54", "IV55", "IV56", "KA27", "KA28", "PA20", "PA41", "PA42", "PA43", "PA44", "PA45", "PA46", "PA47", "PA48", "PA49", "PA62", "PA63", "PA64", "PA65", "PA66", "PA67", "PA68", "PA69", "PA70", "PA71", "PA72", "PA73", "PA74", "PA75", "PA76", "PH42", "PH43", "PH44", "PO31", "PO32","PO33","PO34","PO35","PO36", "PO37", "PO38", "PO39","PO40", "PO41");

		if (form.username.value == "") {alert("Please enter a Username.");}
		else if (form.password.value == "") {alert("Please enter a Password.");}
		else if (form.password.value != form.cpassword.value) {alert("Your Password does not match the confirmation");}
		else if (form.addtype[1].checked == true) {
			if (form.firstname.value == "") {alert("Please enter your First Name.");}
			else if (form.surname.value == "") {alert("Please enter your Surname.");}
			else if (form.phone.value == "") {alert("Please enter a Phone Number.");}
			else if (form.email.value == "") {alert("Please enter a valid Email Address.");}
			else if (form.deladd1.value == "") {alert("Please enter the Delivery Address.");}
			else if (form.delCnty.value == "") {alert("Please enter the Delivery Address County.");}
			else if (form.delPstCde.value == "") {alert("Please enter the Delivery Address Post Code.");}
			else {
				var postcode = form.delPstCde.value;

				var pcode = postcode.slice(0,2);
				pcode = pcode.toUpperCase();
				for (var i = 0; i <highlandsnislnds.length; i = i+ 1)
				{
					if (pcode == highlandsnislnds[i])
					{
						alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
						return false;
					}
				}

				var pcode2 = postcode.slice(0,4);
				pcode2 = pcode2.toUpperCase();
				for (var i = 0; i <highlandsnislnds2.length; i = i+ 1)
				{
					if (pcode2 == highlandsnislnds2[i])
					{
						alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
						return false;
					}
				}

				postcode = postcode.replace(" ", "");
				postcode = postcode.toUpperCase();
				if (postcode.length < 5 || postcode > 7) {alert("The postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
				else if (postcode.charCodeAt(postcode.length-1) < 65 || postcode.charCodeAt(postcode.length-1) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
				else if (postcode.charCodeAt(postcode.length-2) < 65 || postcode.charCodeAt(postcode.length-2) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
				else if (postcode.charCodeAt(postcode.length-3) < 48 || postcode.charCodeAt(postcode.length-3) > 57) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
				else {
					form.command.value = "createac";
					return true;
				}
			}
		} else if (form.addtype[0].checked == true) {
			if (form.firstname.value == "") {alert("Please enter your account First Name.");}
			else if (form.surname.value == "") {alert("Please enter your account Surname.");}
			else if (form.phone.value == "") {alert("Please enter a Phone Number.");}
			else if (form.email.value == "") {alert("Please enter a valid Email Address.");}
			else if (form.delfirstname.value == "") {alert("Please enter your Delivery AddressFirst Name.");}
			else if (form.delsurname.value == "") {alert("Please enter your Delivery Address Surname.");}
			else if (form.deladd1.value == "") {alert("Please enter the first line of Delivery Address.");}
			else if (form.delCnty.value == "") {alert("Please enter the Delivery Address County.");}
			else if (form.delPstCde.value == "") {alert("Please enter the Delivery Address Post Code.");}
			else if (form.bilfirstname.value == "") {alert("Please enter your Billing Address First Name.");}
			else if (form.bilsurname.value == "") {alert("Please enter your Billing Address Surname.");}
			else if (form.biladd1.value == "") {alert("Please enter the first line of your Billing Address.");}
			else if (form.bilCnty.value == "") {alert("Please enter the Billing Address County.");}
			else if (form.bilPstCde.value == "") {alert("Please enter the Billing Address Post Code.");}
			else {
				var postcode = form.delPstCde.value;
				var postcode2 = form.bilPstCde.value;

				var pcode = postcode.slice(0,2);
				pcode = pcode.toUpperCase();
				var pcode3 = postcode2.slice(0,2);
				pcode3 = pcode3.toUpperCase();
				for (var i = 0; i <highlandsnislnds.length; i = i+ 1)
				{
					if (pcode == highlandsnislnds[i])
					{
						alert ("Your delivery postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
						return false;
					}
					if (pcode3 == highlandsnislnds[i])
					{
						alert ("Your billing postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
						return false;
					}
				}

				var pcode2 = postcode.slice(0,4);
				pcode2 = pcode2.toUpperCase();
				var pcode4 = postcode2.slice(0,4);
				pcode4 = pcode4.toUpperCase();
				for (var i = 0; i <highlandsnislnds2.length; i = i+ 1)
				{
					if (pcode2 == highlandsnislnds2[i])
					{
						alert ("Your delivery postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
						return false;
					}
					if (pcode4 == highlandsnislnds2[i])
					{
						alert ("Your billing postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
						return false;
					}
				}

				postcode = postcode.replace(" ", "");
				postcode = postcode.toUpperCase();
				if (postcode.length < 5 || postcode > 7) {alert("The postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
				else if (postcode.charCodeAt(postcode.length-1) < 65 || postcode.charCodeAt(postcode.length-1) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
				else if (postcode.charCodeAt(postcode.length-2) < 65 || postcode.charCodeAt(postcode.length-2) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
				else if (postcode.charCodeAt(postcode.length-3) < 48 || postcode.charCodeAt(postcode.length-3) > 57) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
				else {
					form.command.value = "createac";
					return true
				}
			}
		}
		return false;
}

function jsfindaddress (type)
{
	var listID = "ajax_address"+type;
	
	document.getElementById(listID).style.display = "block";
    var postcode = document.getElementById(type+"PstCde").value;
 	var request = "/ajax/ajaxaddress.php";
	var vars = "postcode="+postcode;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			if(xml.getElementsByTagName("ErrorNumber")[0].childNodes[0].nodeValue == 0) {
				if(xml.getElementsByTagName("Address2").length != 0)document.getElementById(type+"add2").value= xml.getElementsByTagName("Address2")[0].childNodes[0].nodeValue;
				if(xml.getElementsByTagName("Address3").length != 0)document.getElementById(type+"add3").value= xml.getElementsByTagName("Address3")[0].childNodes[0].nodeValue;
				document.getElementById(type+"Town").value= xml.getElementsByTagName("Town")[0].childNodes[0].nodeValue;
				document.getElementById(type+"Cnty").value= xml.getElementsByTagName("County")[0].childNodes[0].nodeValue;
				var resultlist =  xml.getElementsByTagName("PremiseData")[0].childNodes[0].nodeValue;
				var arrlist = resultlist.split(";");
				var addlist = "<ul >";
				for (var i = 0; i < arrlist.length; i++) {
					if(arrlist[i] != "") {
						arrlist[i] = arrlist[i].replace("||","|");
						arrlist[i] = arrlist[i].replace("|",", ");
						arrlist[i] = arrlist[i].replace("|",", ");
						var straddress =arrlist[i]+" "+xml.getElementsByTagName("Address1")[0].childNodes[0].nodeValue;
						straddress = straddress.replace(/(^,)|(,$)/g, "");
						straddress = straddress.replace(/(^ )|(,$)/g, "");
						addlist += "<li><a href='javascript:jsaddsel(\""+straddress+"\", \""+type+"\")'>"+straddress+"</a></li>";
					}
				}
				addlist += "</ul>";
				document.getElementById(listID).innerHTML = addlist;

			} else {
				document.getElementById(listID).innerHTML = "<span class='notification-warning'>"+xml.getElementsByTagName("ErrorMessage")[0].childNodes[0].nodeValue+"</span>";
			}

		} else if (xmlhttp.readyState==4) {
			document.getElementById(listID).innerHTML = " <span class='notification-warning'>There has been an error updating your address please try again.</span> ";
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
	document.getElementById(listID).innerHTML = " <img src='img/ajax-loader.gif' /> <span> Search...</span> ";	
}
function jsaddsel(address, type)
{
	document.getElementById(type+"add1").value= address;
	document.getElementById("ajax_address"+type).style.display = "none";
}
-->
</script>
    <div id="mainContent">
            <h2>Site Registration</h2>
<?php

				if ($strerror != "")
				{
					print ("<center><font color='red' size='+2'>$strerror</font></center><br/><br/>");
				}

	switch ($strcmd)
	{
		case "createac":

			print ("The New account has been created.");

			print ("<form name='frm_form' action='account.php' method='post'>");
			print ("<input type='hidden' name='command' value='signin'/>");
			if ($strreferer != "") print ("<input type='hidden' name='referer' value='$strreferer'/>");
					print ("<div class='signedOut'><!--page to display if no-one is signed in-->");
					print ("<div id='signin'>");
					print ("<h3>Sign in:</h3><table>");
					print ("<tr><td><label for='username'>User Name : </label></td><td><input name='username' type='text' size='30' tabindex='1'/><br /></td></tr>");
					print ("<tr><td><label for='password'>Password : </label></td><td><input name='password' type='password' size='30' tabindex='2'/><br /></td></tr>");
					print ("<tr><td align='center' colspan='2'><button type='submit' value='Sign In' tabindex='3'>Sign In</button><!--need way of signing in-->");
					print ("<button type='reset' value='Clear Form' tabindex='4'>Clear Form</button></td></tr></table></div>");
					print ("</div>");
			print ("</form>");

			break;

		default:

			if ($intuserid > 0)
			{

				print ("<form name='frm_form' action='register.php' method='post'>");
				print ("<input type='hidden' name='command' value='signout' />");
				if ($strreferer != "") print ("<input type='hidden' name='referer' value='$strreferer'/>");
					print ("<div class='signedIn'><!--page contents to display if user issigned in-->");
						print ("<div class='notice'>");
							print ("<p>You are already sign in as '$strusername'.</p>");
							print ("<p>Do you want to:</p>");
							print ("<ul><li><a href='editAccount.php'>Edit this account</a></li>");
							print ("<li><a href='#' onclick='document.frm_form.submit();'>Sign out</a></li>");
							print ("</ul>");
						print ("</div>");
					print ("</div><!--end of signed in contents-->");
				print ("</form>");
			}
			else
			{
				print ("<form name='frm_form' action='register.php' method='post' onsubmit='return jscreateaccount(this)'>");
				print ("<input type='hidden' name='command' />");
				if ($strreferer != "") print ("<input type='hidden' name='referer' value='$strreferer'/>");



				$strusername = $_REQUEST['username'];
				$strfirstname = $_REQUEST['firstname'];
				$strsurname = $_REQUEST['surname'];
				$strtitle = $_REQUEST['title'];
				$strcompany = $_REQUEST['company'];
				$strphone = $_REQUEST['phone'];
				$stremail = $_REQUEST['email'];
				$strcemail = $_REQUEST['cemail'];
				$strfax = $_REQUEST['fax'];
				$strdelfirstname = $_REQUEST['delfirstname'];
				$strdelsurname = $_REQUEST['delsurname'];
				$strdeltitle = $_REQUEST['deltitle'];
				$strdeladd1 = $_REQUEST['deladd1'];
				$strdeladd2 = $_REQUEST['deladd2'];
				$strdeladd3 = $_REQUEST['deladd3'];
				$strdelTown = $_REQUEST['delTown'];
				$strdelCnty = $_REQUEST['delCnty'];
				$strdelPstCde = $_REQUEST['delPstCde'];
				$strbilfirstname = $_REQUEST['bilfirstname'];
				$strbilsurname = $_REQUEST['bilsurname'];
				$strbiltitle = $_REQUEST['biltitle'];
				$strbiladd1 = $_REQUEST['biladd1'];
				$strbiladd2 = $_REQUEST['biladd2'];
				$strbiladd3 = $_REQUEST['biladd3'];
				$strbilTown = $_REQUEST['bilTown'];
				$strbilCnty = $_REQUEST['bilCnty'];
				$strbilPstCde = $_REQUEST['bilPstCde'];
				$intaddtype = intval($_REQUEST['addtype']);
				if ($_REQUEST['newsletter']) $intnewsletter = 1;
				else $intnewsletter = 0;
				
				print ("<span id='register-left'>");
					print ("<fieldset id='account'><legend><h3>Account Details</h3></legend>");
					print ("<table>");
					print ("<tr><td style='width: 100px;'><label for='username'>User Name : </label></td><td><input name='username' id='username' type='text' size='30' value='$strusername' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='password'>Password : </label></td><td><input name='password' id='password' type='password' size='30' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='cpassword'>Confirm Password : </label></td><td><input name='cpassword' id='cpassword' type='password' size='30' />&nbsp;<font size='1' color='red'>*</font></td></tr></table>");
					print ("<br/><p><a href='#' onclick='document.getElementById(\"why\").style.display = \"block\"; return false;'> Why Register?</a>");
					if ($strreferer != "account.php") print (" | <a href='acount.php?referer=".$strreferer."'>Already got an Account?</a>");
					print ("</p>");
					print ("<div id='why' style='display:none'><h3>Why Do I Need to Register?</h3>");
					print ("<p>In order for you to buy from our store we need your delivery details, we might also need an email address or phone number so we can get in touch the unlikely event that there are problems fulfilling your order.</p><p>Having users create accounts allows us to collect these details and provide customers with basic feedback about their current order and to place future orders quicker and easier.</p><p>Stollers Building Products does not give or sell customer details to any third party. Any details you provide will <u>only</u> be used in processing your orders and providing relavant customer information and support.</p></div>");
					print ("</fieldset>");

					print ("<fieldset id='delivery'><legend><h3>Customers Details</h3></legend>");
					print ("<table>");
					print ("<tr><td style='width: 100px;'><label for='firstname'>First Name : </label></td><td><input id='firstname' name='firstname' type='text' size='30' value='$strfirstname' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='surname'>Surname : </label></td><td><input id='surname' name='surname' type='text' size='30' value='$strsurname' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='title'>Title : </label></td><td><input id='title' name='title' type='text' size='10' value='$strtitle' /></td></tr>");
					print ("<tr><td><label for='company'>Company : </label></td><td><input id='company' type='text' name='company' size='30' value='$strcompany' /></td></tr>");
					print ("<tr><td><label for='phone'>Phone Number : </label></td><td><input type='text' id='phone' name='phone' size='30' value='$strphone' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='email'>Email : </label></td><td><input type='text' id='email' name='email' size='30' value='$stremail' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='cemail'>Confirm Email : </label></td><td><input type='text' id='cemail' name='cemail' size='30' value='$strcemail' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='fax'>Fax Number : </label></td><td><input type='text' id='fax' name='fax' size='30' value='$strfax' /></td></tr>");
					
					print ("</table></fieldset>");
					
					print ("<fieldset><legend><h3>Newsletter</h3></legend>");
					print ("<input type='checkbox' name='newsletter' id='newsletter' value='1' checked='checked' /> <label for='newsletter'>Sign up for our newsletter</label><br/><br/></fieldset><br/>\n");
					
					
				print ("</span>");
				print("<span id='register-right'>");
				
				print ("<fieldset><legend><h3>Address Types</h3></legend>");
					print ("<input type='radio' value='1' name='addtype' id='multiaddress' onclick='jsaddtype(this)' ");
					if($intaddtype == 1) {print("checked='checked'");$strclass='-show';}
					print ("/><label for='multiaddress'>Use separate billing and delivery addresses</label><br/>");
					print ("<input type='radio' value='0' name='addtype' id='singleaddress' onclick='jsaddtype(this)' ");
					if($intaddtype == 0) {print("checked='checked'");$strclass='-hidden';}
					print ("/><label for='singleaddress'>Use a single address for billing and delivery</label><br/><br/>");
				print ("</fieldset><br/>");
				
				print ("<fieldset id='biltable' class='multi".$strclass."'><legend><h3>Billing Address</h3></legend>");
					print ("<table>");
						print ("<tr><td style='width: 100px;'><label for='bilfirstname'>First Name : </label></td><td><input id='bilfirstname' name='bilfirstname' type='text' size='28' value='$strbilfirstname' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='bilsurname'>Surname : </label></td><td><input id='bilsurname' name='bilsurname' type='text' size='28' value='$strbilsurname' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='biltitle'>Title : </label></td><td><input id='biltitle' name='biltitle' type='text' size='10' value='$strbiltitle' /><br/></td></tr>");
					print ("</table><table>");
				
					print ("<tr><td style='width: 100px;'><label for='bilPstCde'>Post Code : </label></td><td><input id='bilPstCde' name='bilPstCde' size='23' value='$strbilPstCde' /><font size='1' color='red'>*</font>");
					print ("<a style='border: none; display: inline-block; padding-left: 5px; vertical-align: top;' href='Javascript:jsfindaddress(\"bil\")'><img style='border: none;' src='/images/search.png' alt='Find'></a></td></tr>\n");
					print ("<tr><td></td><td><div id='ajax_addressbil'></div></td></tr>");
					print ("<tr><td><label for='biladd1'>Address 1 : </label></td><td><input id='biladd1' name='biladd1' size='28' value='$strbiladd1' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("<tr><td><label for='biladd2'>Address 2 : </label></td><td><input id='biladd2' name='biladd2' size='28' value='$strbiladd2' /></td></tr>");
					print ("<tr><td><label for='biladd3'>Address 3 : </label></td><td><input id='biladd3' name='biladd3' size='28' value='$strbiladd3' /></td></tr>");
					print ("<tr><td><label for='bilTown'>Town : </label></td><td><input id='bilTown' name='bilTown' size='28' value='$strbilTown' /></td></tr>");
					print ("<tr><td><label for='bilCnty'>County : </label></td><td><input id='bilCnty' name='bilCnty' size='28' value='$strbilCnty' />&nbsp;<font size='1' color='red'>*</font></td></tr>");

					print("</table>");
					print ("</fieldset>");

				print ("<div id='deltable".$strclass."'>");
					print ("<fieldset><legend><h3 id='addLegend'>Address</h3></legend>");
					print ("<table class='multi".$strclass."'>");
						print ("<tr><td style='width: 100px;'><label for='delfirstname'>First Name : </label></td><td><input id='delfirstname' name='delfirstname' type='text' size='28' value='$strdelfirstname' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='delsurname'>Surname : </label></td><td><input id='delsurname' name='delsurname' type='text' size='28' value='$strdelsurname' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='deltitle'>Title : </label></td><td><input id='deltitle' name='deltitle' type='text' size='10' value='$strdeltitle' /><br/></td></tr>");
						print ("</table><table>");
						print ("<tr><td style='width: 100px;'><label for='delPstCde'>Post Code : </label></td><td><input id='delPstCde' name='delPstCde' size='23' value='$strdelPstCde' /><font size='1' color='red'>*</font>");
						print ("<a style='border: none; display: inline-block; padding-left: 5px; vertical-align: top;' href='Javascript:jsfindaddress(\"del\")'><img style='border: none;' src='/images/search.png' alt='Find'></a></td></tr>\n");
						print ("<tr><td></td><td><div id='ajax_addressdel'></div></td></tr>");
						print ("<tr><td><label for='deladd1'>Address 1 : </label></td><td><input id='deladd1' name='deladd1' size='28' value='$strdeladd1' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
						print ("<tr><td><label for='deladd2'>Address 2 : </label></td><td><input id='deladd2' name='deladd2' size='28' value='$strdeladd2' /></td></tr>");
						print ("<tr><td><label for='deladd3'>Address 3 : </label></td><td><input id='deladd3' name='deladd3' size='28' value='$strdeladd3' /></td></tr>");
						print ("<tr><td><label for='delTown'>Town : </label></td><td><input id='delTown' name='delTown' size='28' value='$strdelTown' /></td></tr>");
						print ("<tr><td><label for='delCnty'>County : </label></td><td><input id='delCnty' name='delCnty' size='28' value='$strdelCnty' />&nbsp;<font size='1' color='red'>*</font></td></tr>");
					print ("</table></fieldset><br/>");
					print("</div>");
					print("</span>");


				print ("<div id='register-buttons'>");
				print ("<button type='submit' id='createAccount' class='short_button' >Create Account</button></div>");

				print ("</form>");
			}
			break;
	}
?>
        </div><!--end of 'pagetext', 'contents' and 'pagemiddle'-->

	<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>
