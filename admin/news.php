<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * news.php						                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	
	$strpage = "news"; //define the current page
	
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	
	$conn = connect(); // Open Connection to Database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if(isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if(isset($_REQUEST['newsCategoryID'])) $newsCategoryID = $_REQUEST['newsCategoryID']; else $newsCategoryID = "";
	if(isset($_REQUEST['newsItemID'])) $newsItemID = $_REQUEST['newsItemID']; else $newsItemID = "";
	$strshowArticle = (isset($_REQUEST['frm_enabled'])) ? 1 : 0;
	$strItemType = 1;
	
	switch($strcmd)
	{
		case "insertNewsCategory":
		case "updateNewsCategory":
			
			$arrdbparams = array("description" => $_POST['frm_description']);
			
			if($strcmd == "insertNewsCategory")
			{	
				$strdbsql = "INSERT INTO site_news_categories (description, type) 
							VALUES (:description, ".$strItemType.")";
				$strType = "insert";
			}
			elseif($strcmd == "updateNewsCategory")
			{
				$strdbsql = "UPDATE site_news_categories SET description = :description, type = ".$strItemType." WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $newsCategoryID;
				$strType = "update";
			}
			
			$updateNewsCategory = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if($strcmd == "insertNewsCategory")
			{
				$newsCategoryID = $updateNewsCategory;
			}
			
			if($strcmd == "insertNewsCategory")
			{
				if($updateNewsCategory > 0)
				{
					$strsuccess = "News category successfully added";
				}
				elseif($updateNewsCategory == 0)
				{
					$strerror = "An error occurred while adding the news category";
				}
			}
			elseif($strcmd == "updateNewsCategory")
			{
				if($updateNewsCategory <= 1)
				{
					$strsuccess = "News category successfully updated";
				}
				elseif($updateNewsCategory > 1)
				{
					$strwarning = "An error may have occurred while updating this news category";
				}
			}
			
			$strcmd = "viewNewsCategory";
			
		break;
		
		case "deleteNewsCategory":
			
			$strdbsql = "DELETE FROM site_news_categories WHERE recordID = :newsCategoryID";
			$deleteNewsItem = query($conn,$strdbsql,"delete",["newsCategoryID"=>$newsCategoryID]);
			
			$strcmd = "";
			
		break;
		
		case "insertNewsItem":
		case "updateNewsItem":
			
			if($_POST['frm_category'] == 0) $_POST['frm_category'] = null;

			$arrdbparams = array(
				"title" => $_POST['frm_title'],
				"content" => $_POST['frm_content'],
				"categoryID" => $_POST['frm_category'],
				"date" => time(),
				"image" => $_POST['frm_image'],
				"link" => $_POST['frm_link']
			);
			
			if($strcmd == "insertNewsItem") {	
				$strdbsql = "INSERT INTO site_news_events (title, content, date, type, categoryID, image, link, enabled) VALUES (:title, :content, :date, ".$strItemType.", :categoryID, :image, :link, :enabled)";
				$arrdbparams['enabled'] = 0;
				$strType = "insert";
			} elseif($strcmd == "updateNewsItem") {
				$strdbsql = "UPDATE site_news_events SET title = :title, content = :content, date = :date, type = ".$strItemType.", categoryID = :categoryID, image = :image, link = :link WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $newsItemID;
				$strType = "update";
			}
			
			$updateNewsItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if($strcmd == "insertNewsItem") $newsItemID = $updateNewsItem;
			
			if($strcmd == "insertNewsItem") {
				if($updateNewsItem > 0) $strsuccess = "Blog post successfully created.";
				elseif($updateNewsItem == 0) $strerror = "An error occurred while creating the blog post.";
			} elseif($strcmd == "updateNewsItem") {
				if($updateNewsItem <= 1) $strsuccess = "Blog post successfully updated.";
				elseif($updateNewsItem > 1) $strwarning = "An error may have occurred while saving the blog post.";
			}
			
			if($strcmd == "updateNewsItem") $strcmd = "";
			else $strcmd = "viewNewsItem";
			
		break;
		
		case "deleteNewsItem":
			
			$strdbsql = "DELETE FROM site_news_events WHERE recordID = :newsItemID";
			$deleteNewsItem = query($conn,$strdbsql,"delete",["newsItemID"=>$newsItemID]);
			
			$strcmd = "";
			
		break;
		
		case "insertImage":
			
			// Get image name
			$newfilename = strtolower($_FILES['frm_image']['name']);
			$strfileextn = getExtension($newfilename);
			
			// Get intended directory
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath;
			
			// Get image name and directory
			$upfile = $struploaddir.$newfilename;
			
			if($_FILES['frm_image']['error'] > 0)
			{
				switch ($_FILES['frm_image']['error']) {
					case 1: $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file."; break;
					case 2: $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file."; break;
					case 3: $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file."; break;
					case 4: $strerror = "Problem: No file selected. Please try again."; break;
				}
			} else {
				// Does the file have the right extension ?
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0) {
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				} else {
					if (is_uploaded_file($_FILES['frm_image']['tmp_name'])) {
						if (!move_uploaded_file($_FILES['frm_image']['tmp_name'], $upfile)) {
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						} else {
							//scale to maximum that will fix in box 1024x768 pixels (smaller files will be enlarged, larger files will be shrunk, aspect ratio of file remains the same)
							$resizedfile = image_scaletomax($upfile, $upfile, 1200);
							$strsuccess = "New image added.";
						}
					} else {
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "") $strcommand = "ERRORMSG";
			else {
				
				// Get new image directory
				$strpagepath = $strimguploadpath."news/";
				$strpagedir = $strrootpath.$strpagepath;
				
				// Get image name and new directory
				$newfile = $strpagedir.$newfilename;
				
				$range = copy($upfile,$newfile);
				
				// Change image permissions to rewrite
				chmod($newfile, 0777);
				
				// Delete file if already exists
				unlink($upfile);
				
				// Get total images assigned to the same property
				$strdbsql = "SELECT COUNT(*) AS max_order FROM site_news_images WHERE newsID = :galleryID";
				$maxOrder = query($conn, $strdbsql, "single", array("galleryID"=>$_REQUEST['galleryID']));
				
				if($maxOrder['max_order'] > 0){ 
					$order = $maxOrder['max_order'] + 1;
					$main = NULL;
				} else {
					$order = 1;
					$main = "yes";
				}
				
				// Insert image into database as last
				$strdbsql = "INSERT INTO site_news_images (newsID, imageRef, imageOrder) VALUES (:id, :imageLocation, :galleryOrder)";
				$updatePageImage = query($conn, $strdbsql, "insert", array("id"=>$_REQUEST['galleryID'], "imageLocation"=>$newfilename, "galleryOrder"=>$order));
			
			}
			
			$strcmd = "viewNewsItem";
			
		break;
		case "deleteImages":
			
			$strdbsql = "DELETE FROM site_news_images WHERE recordID = :deleteImages";
			$removeImages = query($conn, $strdbsql, "delete", array("deleteImages"=>$_POST['deleteImages']));
			
			$strdbsql = "SELECT recordID FROM site_news_images WHERE newsID = :galleryID ORDER BY imageOrder";
			$galleryImages = query($conn,$strdbsql,"multi",array("galleryID"=>$_REQUEST['galleryID']));
			
			$i = 1;
			
			foreach($galleryImages AS $galleryImage)
			{
				$strdbsql = "UPDATE site_news_images SET imageOrder = ".$i." WHERE recordID = :recordID";
				query($conn, $strdbsql, "update", array("recordID"=>$galleryImage['recordID']));
				$i++;
			}
			
			$strsuccess = "Image deleted";
			
			$strcmd = "viewNewsItem";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Blog Posts</h1>");
			
			//Print out debug and error messages
			if($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddNewsCategory() {
					document.form.cmd.value="addNewsCategory";
					document.form.submit();
				}
				function jsviewNewsCategory(newsCategoryID) {
					document.form.cmd.value="viewNewsCategory";
					document.form.newsCategoryID.value=newsCategoryID;
					document.form.submit();
				}
				function jsdeleteNewsCategory(newsCategoryID) {
					if(confirm("Are you sure you want to delete this news category?"))
					{
						document.form.cmd.value="deleteNewsCategory";
						document.form.newsCategoryID.value=newsCategoryID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertNewsCategory() {
					document.form.cmd.value='insertNewsCategory';
					document.form.submit();
				}
				function jsupdateNewsCategory() {
					if($('#form').valid()) {
						document.form.cmd.value='updateNewsCategory';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddNewsItem() {
					document.form.cmd.value="addNewsItem";
					document.form.submit();
				}
				function jsviewNewsItem(newsItemID) {
					document.form.cmd.value="viewNewsItem";
					document.form.newsItemID.value=newsItemID;
					document.form.submit();
				}
				function jsdeleteNewsItem(newsItemID) {
					if(confirm("Are you sure you want to delete this news item?"))
					{
						document.form.cmd.value="deleteNewsItem";
						document.form.newsItemID.value=newsItemID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertNewsItem() {
					document.form.cmd.value='insertNewsItem';
					document.form.submit();
				}
				function jsupdateNewsItem() {
					if($('#form').valid()) {
						document.form.cmd.value='updateNewsItem';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				function jsdeleteImages() {
					if($("#sortableGalleryImages li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this image(s)?"))
						{
							var deleteImages = "";
							$("#sortableGalleryImages li.selected").each(function (index, item) {
								deleteImages += $(item).attr("id")+",";
							});
							deleteImages = deleteImages.replace(/,\s*$/, "");
							document.form.cmd.value="deleteImages";
							document.form.deleteImages.value=deleteImages;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the image(s) you wish to delete");
					}
				}
			</script>
			<?php
		
			print("<form action='news.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='newsCategoryID' id='newsCategoryID' value='".$newsCategoryID."' />");
				print("<input type='hidden' name='newsItemID' id='newsItemID' value='".$newsItemID."' />");
				print("<input type='hidden' name='itemType' id='itemType' value='".$strItemType."' />");
				
				switch($strcmd)
				{
					case "viewNewsCategory":
					case "addNewsCategory":
						
						if($strcmd == "viewNewsCategory")
						{
							$strdbsql = "SELECT * FROM site_news_categories WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $newsCategoryID);
							$newsCategoryDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							/*print("<pre>");
							print_r($newsItemDetails);
							print("</pre>");*/
							
							print("<fieldset class='inlineLabels'> <legend>Change News Category Details</legend>");
						}
						elseif($strcmd == "addNewsCategory")
						{
							$pageDetails = array(
												"description" => ""
											);
							
							print("<fieldset class='inlineLabels'> <legend>Add News Category Details</legend>");
						}
						
						print("<div class='form-group'>
							<label for='frm_description' class='col-sm-2 control-label'>Description</label>
							<div class='col-sm-10'>
							  <input style='width:800px;' type='text' class='form-control' id='frm_description' name='frm_description' value='".$newsCategoryDetails['description']."'>
							</div>
						  </div>");
						print("<div class='form-group'>
								<div class='col-sm-10'>");
								if($strcmd == "addNewsCategory")
								{
								  print("<button onclick='return jsinsertNewsCategory();' type='submit' class='btn btn-success'>Update News Category</button> ");
								}
								elseif($strcmd == "viewNewsCategory")
								{
								  print("<button onclick='return jsupdateNewsCategory();' type='submit' class='btn btn-success'>Update News Category</button> ");
								}
								print  ("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>
								</div>
						  </div>");
						print("</fieldset>");
					
						break;
						
					case "viewNewsItem":
					case "addNewsItem":
						
						print("<div class='row'>");
							print("<div class='col-sm-12 col-md-6'>");
								print("<div class='section'>");
						
									if($strcmd == "viewNewsItem") {
										
										$strdbsql = "SELECT * FROM site_news_events WHERE recordID = :recordID";
										$newsItemDetails = query($conn,$strdbsql,"single",["recordID"=>$newsItemID]);
										
										$newsItemCategory = $newsItemDetails['categoryID'];
										$newsImage = $newsItemDetails['image'];
										
									} else if($strcmd == "addNewsItem") {
										
										$newsItemCategory = 0;
										$newsImage = "";
										
									}
									
									print("<input type='hidden' name='galleryID' id='galleryID' value='".$newsItemDetails['recordID']."' />");
									
									print("<fieldset class='inlineLabels'>");
										print("<legend>Details</legend>");
						
										print("<div class='row'>");
											print("<div class='col-sm-12 form-group'>");
												print("<label for='frm_title' class='control-label'>Title <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='A brief summary of the blog.'></i></label>");
												print("<input type='text' class='form-control' id='frm_title' name='frm_title' value='".htmlspecialchars($newsItemDetails['title'], ENT_QUOTES)."' placeholder='Aa'>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='col-sm-12 col-md-6 form-group even-left'>");
												print("<label for='frm_category' class='control-label'>Category</label>");
												print("<select id='frm_category' name='frm_category' class='form-control'>");
													print("<option value='0'>None</option>");
									
													$strdbsql = "SELECT * FROM site_news_categories";
													$strType = "multi";
													$newsCategories = query($conn, $strdbsql, $strType);
													foreach($newsCategories AS $newsCategory)
													{
														$strSelected = "";
														if($newsItemCategory == $newsCategory['recordID']) $strSelected = " selected";
														
														print("<option value='".$newsCategory['recordID']."'".$strSelected.">".$newsCategory['description']."</option>");
													}
							
												print("</select>");
											print("</div>");
											print("<div class='col-sm-12 col-md-6 form-group even-right'>");
												print("<label for='frm_link' class='control-label'>Link  <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='Must be lowercase and cannot contain any spaces or special characters.'></i></label>");
												print("<input type='text' class='form-control' id='frm_link' name='frm_link' value='".$newsItemDetails['link']."' placeholder='our-new-site'>");
											print("</div>");
										print("</div>");
								
										print("<div class='row'>");
											print("<div class='col-sm-12 form-group'>");
												print("<label for='frm_content' class='control-label'>Content</label>");
												print("<textarea class='form-control tinymce' id='frm_content' name='frm_content' rows='12'>".$newsItemDetails['content']."</textarea>");
											print("</div>");
										print("</div>");
									
									print("</fieldset>");
						
								print("</div>");
							print("</div>");
							
							if($strcmd == "viewNewsItem") {
							
								print("<div class='col-sm-12 col-md-6'>");
								
									$strdbsql = "SELECT * FROM site_news_images WHERE newsID = :article ORDER BY imageOrder";
									$galleryImages = query($conn,$strdbsql,"multi",["article"=>$newsItemDetails['recordID']]);
									
									$unusedImages = array();
									$dh = opendir("../images/news");
									while (false !== ($galleryImg = readdir($dh))) {
										if($galleryImg != "." && $galleryImg != "..") {
											$unusedImages[] = $galleryImg;
										}
									}
									
									$assignedImages = "";
									if(!empty($galleryImages))
									{
										foreach($galleryImages AS $galleryImage)
										{
											if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/news/".$galleryImage['imageRef'])) {
												$assignedImages .= "<li id='".$galleryImage['recordID']."' data-order='".$galleryImage['imageOrder']."'>";
													$assignedImages .= "<img src='/images/news/".$galleryImage['imageRef']."'>";
												$assignedImages .= "</li>";
											}
											$key = array_search($galleryImage['imageRef'], $unusedImages);
											if($key !== false)
											{
												unset($unusedImages[$key]);
											}
										}
									}
								
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
									
											print("<legend>Images</legend>");
									
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_image' class='control-label'>Add an Image</label>");
													print("<div class='row'>");
														print("<div class='col-xs-9' style='padding-right: 7.5px;'>");
															print("<input type='file' class='form-control' id='frm_image' name='frm_image' style='padding:6px 12px;'>");
														print("</div>");
														print("<div class='col-xs-3' style='padding-left: 7.5px;'>");
															print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
															print("<button style='margin-left: 7.5px;' onclick='return jsdeleteImages();' type='button' class='btn btn-danger circle pull-left'><i class='fa fa-trash'></i></button>");
															print("<input type='hidden' name='deleteImages' id='deleteImages' />");
														print("</div>");
													print("</div>");
												print("</div>");
											print("</div>");
								
											if(count($unusedImages) > 0 || $assignedImages != "")
											{
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<ul id='sortableGalleryImages' style='height:495px;'>");
															print($assignedImages);
														print("</ul>");
													print("</div>");
												print("</div>");
											}
									
										print("</fieldset>");
									print("</div>");
								print("</div>");
							
							}
							
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='col-sm-12'>");
								if($strcmd == "addNewsItem") print("<button onclick='return jsinsertNewsItem();' type='submit' class='btn btn-success pull-right'>Add Blog</button>");
								else if($strcmd == "viewNewsItem") print("<button onclick='return jsupdateNewsItem();' type='submit' class='btn btn-success pull-right'>Save Blog</button>");
								print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
							print("</div>");
						print("</div>");
					
						break;
						
					default:
						
					/*	$strdbsql = "SELECT * FROM site_news_categories";
						$newsCategories = query($conn,$strdbsql,"multi");
						
						print("<div class='section'>");
							print("<table id='newscategory-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th>Name</th>");
									print("<th>Edit</th>");
									print("<th>Delete</th>");
								print("</tr></thead><tbody>");
								foreach($newsCategories AS $newsCategory)
								{
									print("<tr>");
										print("<td>".$newsCategory['description']."</td>");
										print("<td><button onclick='return jsviewNewsCategory(\"".$newsCategory['recordID']."\");' type='submit' class='btn btn-primary circle'><i class='fa fa-pencil'></i></button></td>");
										print("<td><button onclick='return jsdeleteNewsCategory(\"".$newsCategory['recordID']."\");' type='submit' class='btn btn-danger circle'><i class='fa fa-trash'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<button onclick='return jsaddNewsCategory();' type='submit' class='btn btn-success'>Add Category</button>");*/
						
						$strdbsql = "SELECT * FROM site_news_events";
						$newsItems = query($conn, $strdbsql, "multi");
						
						print("<div class='section'>");
							print("<table id='news-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th width='35%'>Title</th>");
									print("<th width='20%'>Date</th>");
									print("<th width='15%'>Views</th>");
									print("<th width='10%' style='text-align:center;'>Show/Hide</th>");
									print("<th width='10%' style='text-align:center'>Edit</th>");
									print("<th width='10%' style='text-align:center;'>Delete</th>");
								print("</tr></thead><tbody>");
								foreach($newsItems AS $newsItem)
								{
									print("<tr>");
										print("<td>".$newsItem['title']."</td>");
										print("<td>".date("j/m/y \a\\t H:i", $newsItem['date'])."</td>");
										print("<td>".$newsItem['views']."</td>");
										print("<td style='text-align:center;'>");
											print("<div class='switch' style='display:inline-block;margin:10px 0 0;'>");
												if($newsItem['enabled'] == 0) print("<input id='show-toggle-".$newsItem['recordID']."' class='show-toggle cmn-toggle-round' type='checkbox'>");
												else print("<input id='show-toggle-".$newsItem['recordID']."' class='show-toggle cmn-toggle-round' type='checkbox' checked>");
												print("<label for='show-toggle-".$newsItem['recordID']."'></label>");
											print("</div>");
										print("</td>");
										print("<td style='text-align:center;'><button onclick='return jsviewNewsItem(\"".$newsItem['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");
										print("<td style='text-align:center;'><button onclick='return jsdeleteNewsItem(\"".$newsItem['recordID']."\");' type='submit' class='btn btn-danger circle' style='display:inline-block;'><i class='fa fa-trash'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");	
							
						print("<button onclick='return jsaddNewsItem();' type='submit' class='btn btn-success pull-right'>New Post</button>");
						
						
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#newscategory-table').DataTable();
			$('#news-table').DataTable();
			
			$(".show-toggle").click(function(){
				var idstring = $(this).attr('id');
				if(this.checked) var show = 1;
				else var show = 0;
				var idsplit = idstring.split('-')[2];
				$.ajax({
					type: "GET",
					url: "/admin/includes/ajax_newsvisible.php?newsID="+idsplit+"&enabled="+show
				});
			});
			
			$("#sortableGalleryImages").on('click', 'li', function (e) {
				if(e.ctrlKey || e.metaKey) $(this).toggleClass("selected");
				else $(this).addClass("selected").siblings().removeClass('selected');
			}).sortable({
				delay: 150, //Needed to prevent accidental drag when trying to select
				revert: 0,
				placeholder: "ui-state-highlight",
				helper: function (e, item) {
					//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
					if(!item.hasClass('selected')) item.addClass('selected').siblings().removeClass('selected');
					
					//Clone the selected items into an array
					var elements = item.parent().children('.selected').clone();
					
					//Add a property to `item` called 'multidrag` that contains the selected items, then remove the selected items from the source list
					item.data('multidrag', elements).siblings('.selected').remove();
					
					//Now the selected items exist in memory, attached to the `item`, so we can access them later when we get to the `stop()` callback
					
					//Create the helper
					var helper = $('<li/>');
					return helper.append(elements);
				},
				stop: function( event, ui ) {
					//Now we access those items that we stored in `item`s data!
					var elements = ui.item.data('multidrag');
					ui.item.after(elements).remove();
					
					var dataArray = [];
					var i = 0;
					var sortType = "";
					$(elements).each(function(index, item) {
						
						var oldOrder = $(item).attr("data-order");
						$("ul#sortableGalleryImages").children("li").each(function(index, item) {
							$(item).attr("data-order", $(item).index() + 1);
						});
						
						var newOrder = $(item).attr("data-order");
						
						if(parseInt(newOrder) > parseInt(oldOrder)) sortType = "DESC";
						else if(parseInt(newOrder) < parseInt(oldOrder)) sortType = "ASC";
						
						dataArray[i] = {};
						dataArray[i]["galleryOrder"] = $(item).attr("data-order");
						dataArray[i]["imageLocation"] = /[^/]*$/.exec($(item).children("img").attr("src"))[0];
						dataArray[i]["recordID"] = $(item).attr("id");
						dataArray[i]["galleryID"] = $("#galleryID").attr("value");
						dataArray[i]["cmd"] = "reorder";

						i++;
						
					});
					dataArray.sort(function(a,b) {
						if(sortType == "ASC") return parseInt(a.galleryOrder) - parseInt(b.galleryOrder);
						else if(sortType == "DESC") return parseInt(b.galleryOrder) - parseInt(a.galleryOrder);
					});
					$.ajax({
						type: "POST",
						url: "includes/ajax_newsimagemanagement.php",
						data:{ array : dataArray }
					});
				}
			}).disableSelection();
			
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>