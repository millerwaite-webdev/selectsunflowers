<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * customer-groups.php			                                       * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "invoiceprint"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['orderHeaderID'])) $orderHeaderID = $_REQUEST['orderHeaderID']; else $orderHeaderID = "";
	if (isset($_REQUEST['documentType'])) $documentType = $_REQUEST['documentType']; else $documentType = "";

	$strdbsql = "SELECT order_header.*, customer.firstname, customer.surname, customer.title, customer.company, customer.mobile, customer.fax, customer.notes AS customerNotes FROM order_header LEFT JOIN customer ON order_header.customerID = customer.recordID WHERE order_header.recordID = :recordID";
	$strType = "single";
	$arrdbparams = array("recordID"=>$orderHeaderID);
	$orderHeader = query($conn,$strdbsql,$strType,$arrdbparams);
	
	$strfirstname = $orderHeader['firstname'];
	$strsurname = $orderHeader['surname'];
	$strtitle = $orderHeader['title'];
	$strcompany = $orderHeader['company'];
	$strphone = $orderHeader['telephone'];
	$strmobile = $orderHeader['mobile'];
	$stremail = $orderHeader['email'];
	$strfax = $orderHeader['fax'];
	$strnotes = $orderHeader['notes'];
	$customerNotes = $orderHeader['customerNotes'];
	$giftMessage = $orderHeader['giftMessage'];
	$strdelfirstname = $orderHeader['addDeliveryFirstname'];
	$strdelsurname = $orderHeader['addDeliverySurname'];
	$strdeltitle = $orderHeader['addDeliveryTitle'];
	$strdeladd1 = $orderHeader['addDelivery1'];
	$strdeladd2 = $orderHeader['addDelivery2'];
	$strdeladd3 = $orderHeader['addDelivery3'];
	$strdelTown = $orderHeader['addDelivery4'];
	$strdelCnty = $orderHeader['addDelivery5'];
	$strdelCntry = $orderHeader['addDelivery6'];
	$strdelPstCde = $orderHeader['addDeliveryPostcode'];
	$strbilfirstname = $orderHeader['addBillingFirstname'];
	$strbilsurname = $orderHeader['addBillingSurname'];
	$strbiltitle = $orderHeader['addBillingTitle'];
	$strbiladd1 = $orderHeader['addBilling1'];
	$strbiladd2 = $orderHeader['addBilling2'];
	$strbiladd3 = $orderHeader['addBilling3'];
	$strbilTown = $orderHeader['addBilling4'];
	$strbilCnty = $orderHeader['addBilling5'];
	$strbilCntry = $orderHeader['addBilling6'];
	$strbilPstCde = $orderHeader['addBillingPostcode'];
	$orderDate = $orderHeader['timestampOrder'];
	$dispatchDate = $orderHeader['dispatchDate'];
	$externalOrderID = $orderHeader['externalOrderID'];
	$deliveryDate = $orderHeader['addDeliveryDate'];
	
	$strdbsql = "SELECT * FROM deliveryTimes WHERE enabled = 1 AND recordID = :time";
	$times = query($conn,$strdbsql,"single",["time"=>$orderHeader['addDeliveryTime']]);
	if($times) $deliveryTime = substr($times['startTime'], 0, 5)." - ".substr($times['endTime'], 0, 5);

	switch ($documentType){
		case "giftMessage":
		
			$print = ("<table cellpadding=\"5\" width=\"100%\" cellpadding=\"3\"><tbody><tr><td valign=\"top\">");	
				$print .= ("<table cellpadding=\"3\" border=\"0\" width=\"100%\" class=\"tabTable\">");
				$print .= ("<tbody>");
				$print .= ("<tr>");
					$print .= ("<td>");
						$print .= ("<p>");
							$print .= ("<span style=\"font-size: 20px; font-weight: bold;\">".$strcompanyname."</span>");
							$print .= ("<br/>");
							$print .= ("<span style=\"font-size: 12px;\">");
								$companyAddArray = explode(",", $companyAddress);
								foreach ($companyAddArray AS $value) {
									$print .= ($value."<br/>");
								}
							$print .= ("</span>");
						$print .= ("</p>");
					$print .= ("</td>");
							$print .= ("<td style=\"text-align: center;\">
								<img src=\"../images/company/logo-vertical.png\" />
							</td>
							<td style=\"text-align: right;\">
								<p><span style=\"font-size: 16px; font-weight: bold;\">Gift Message</span>");
								if(!empty($companyRegNumber)){
									$print .= ("<br/>Registration No. ".$companyRegNumber);
								}
								if(!empty($companyVATNumber)){
									$print .= ("<br/>VAT No. ".$companyVATNumber."<br/><br/>");
								}
								if(!empty($companyPhone)){
									$print .= ("t: ".$companyPhone."<br/>");
								}
								if(!empty($companyFax)){
									$print .= ("f: ".$companyFax."<br/>");
								}
								if(!empty($companyEmail)){
									$print .= ("e: ".$companyEmail."<br/>");
								}
								if(!empty($companyWebsite)){
									$print .= ($companyWebsite);
								}
								$print .= ("</p>
							</td>
						</tr>
						<tr>
							<td colspan=\"3\"></td>
						</tr>
						<tr>
							<td colspan=\"3\" style=\"padding: 40px 0;\"><p><span style=\"font-size: 16px; font-weight: bold;\">Gift Message</span></p></td>
						</tr>
						<tr>
							<td colspan=\"3\">
								<p>$giftMessage</p>
							</td>
						</tr>
						<tr>
							<td colspan=\"3\" style=\"padding: 40px 0 0 0;\">
								<br/><br/><p>$customText1</p>
							</td>
						</tr>
					</tbody>
				</table></td>
				</tr>");
			$print .= ("</tbody></table>");
		
		break;		
		case "feedbackRequest":
		$print = ("<table cellpadding=\"5\" width=\"100%\" cellpadding=\"3\"><tbody><tr><td valign=\"top\">");	
			$print .= ("<table cellpadding=\"3\" border=\"0\" width=\"100%\" class=\"tabTable\">");
			$print .= ("<tbody>");
			$print .= ("<tr>");
				$print .= ("<td>");
					$print .= ("<p>");
						$print .= ("<span style=\"font-size: 20px; font-weight: bold;\">".$strcompanyname."</span>");
						$print .= ("<br/>");
						$print .= ("<span style=\"font-size: 12px;\">");
							$companyAddArray = explode(",", $companyAddress);
							foreach ($companyAddArray AS $value) {
								$print .= ($value."<br/>");
							}
						$print .= ("</span>");
					$print .= ("</p>");
				$print .= ("</td>");
						$print .= ("<td style=\"text-align: center;\">
							<img src=\"../images/company/logo-vertical.png\" />
						</td>
						<td style=\"text-align: right;\">
							<p><span style=\"font-size: 16px; font-weight: bold;\">Feedback</span>");
							if(!empty($companyRegNumber)){
								$print .= ("<br/>Registration No. ".$companyRegNumber);
							}
							if(!empty($companyVATNumber)){
								$print .= ("<br/>VAT No. ".$companyVATNumber."<br/><br/>");
							}
							if(!empty($companyPhone)){
								$print .= ("t: ".$companyPhone."<br/>");
							}
							if(!empty($companyFax)){
								$print .= ("f: ".$companyFax."<br/>");
							}
							if(!empty($companyEmail)){
								$print .= ("e: ".$companyEmail."<br/>");
							}
							if(!empty($companyWebsite)){
								$print .= ($companyWebsite);
							}
							$print .= ("</p>
						</td>
					</tr>
					<tr>
						<td colspan=\"3\"></td>
					</tr>
					<tr>
						<td colspan=\"3\" style=\"padding: 40px 0;\"><p><span style=\"font-size: 16px; font-weight: bold;\">Order No. ".$orderHeaderID."</span></p></td>
					</tr>
					<tr>
						<td colspan=\"3\">
							<p>You recently placed on order on our system, if you could take a minute to provide some feedback it would be greatly appreciated. You can do this by emailing $companyEmail, or if you would prefer to speak to someone on the phone you can call $companyPhone.</p>
						</td>
					</tr>
				</tbody>
			</table></td>
			</tr>");

			$print .= ("</tbody></table>");
		break;	
		default:
			
			$print = ("<style>
				table.grid,
				table.grid {
					border-top: solid 1px #E1E1E1;
					border-left: solid 1px #E1E1E1;
				}
				table.grid th,
				table.grid td {
					border-right: solid 1px #E1E1E1;
					border-bottom: solid 1px #E1E1E1;
				}
			</style>");
			
			$print .= ("<table cellpadding=\"5\" width=\"100%\" cellpadding=\"3\"><tbody><tr><td valign=\"top\">");	
			$print .= ("<table cellpadding=\"3\" border=\"0\" width=\"100%\" class=\"tabTable\">");
			$print .= ("<tbody>");
			$print .= ("<tr>");
				$print .= ("<td>");
					$print .= ("<p>");
						$print .= ("<span style=\"font-size: 20px; font-weight: bold;\">".$strcompanyname."</span>");
						$print .= ("<br/>");
						$print .= ("<span style=\"font-size: 12px;\">");
							$companyAddArray = explode(",", $companyAddress);
							foreach ($companyAddArray AS $value) {
								$print .= ($value."<br/>");
							}
						$print .= ("</span>");
					$print .= ("</p>");
				$print .= ("</td>");
						$print .= ("<td style=\"text-align: center;\">
							<img src=\"../images/company/logo-vertical.png\" />
						</td>
						<td style=\"text-align: right;\">
							<p><span style=\"font-size: 16px; font-weight: bold;\">VAT Invoice</span><br/>");
							if(!empty($companyRegNumber)){
								$print .= ("<br/>Registration No. ".$companyRegNumber);
							}
							if(!empty($companyVATNumber)){
								$print .= ("<br/>VAT No. ".$companyVATNumber."<br/><br/>");
							}
							if(!empty($companyPhone)){
								$print .= ("t: ".$companyPhone."<br/>");
							}
							if(!empty($companyFax)){
								$print .= ("f: ".$companyFax."<br/>");
							}
							if(!empty($companyEmail)){
								$print .= ("e: ".$companyEmail."<br/>");
							}
							if(!empty($companyWebsite)){
								$print .= ($companyWebsite);
							}
							$print .= ("</p>
						</td>
					</tr>
					<tr>
						<td colspan=\"3\"></td>
					</tr>
					<tr>
						<td colspan=\"3\">
							<p><span style=\"font-size: 16px; font-weight: bold;\">Order No. ".$orderHeaderID."</span><br/><br/>
							<strong>Order Date: ".($orderDate ? fnconvertunixtime($orderDate) : "Not Set")."</strong><br/>
							<strong>Estimated Delivery: ".($deliveryDate ? fnconvertunixtime($deliveryDate) : "Not Set").($deliveryTime ? " (".$deliveryTime.")" : "")."</strong></p>
						</td>
					</tr>");
					
					$print .= "<tr><td colspan=\"3\"></td></tr>";
					
					// Addresses
					$print .= "<tr>";
						$print .= "<td colspan=\"3\">";
							$print .= "<table class=\"grid\" cellpadding=\"0\" border=\"0\" width=\"100%\">";
								$print .= "<tr>";
					
									// Billing Address
									$print .= "<td width=\"50%\">";
										$print .= "<table cellpadding=\"5\" border=\"0\" width=\"100%\">";
											$print .= "<tr bgcolor=\"#EDEDED\"><td><strong>Billing Address</strong></td></tr>";
											$print .= "<tr><td>".(!empty($strbiltitle) ? ucwords($strbiltitle)." " : "").(!empty($strbilfirstname) ? ucwords($strbilfirstname)." " : "").(!empty($strbilsurname) ? ucwords($strbilsurname) : "")."</td></tr>";
											$print .= "<tr><td>";
												if(!empty($strbiladd1)) $print .= $strbiladd1.", ";
												if(!empty($strbiladd2)) $print .= $strbiladd2.", ";
												if(!empty($strbiladd3)) $print .= $strbiladd3.", ";
												if(!empty($strbilTown)) $print .= $strbilTown.", ";
												if(!empty($strbilCnty)) $print .= $strbilCnty.", ";
												if(!empty($strbilCntry)) $print .= $strbilCntry.", ";
												if(!empty($strbilPstCde)) $print .= $strbilPstCde;
											$print .= "</td></tr>";
										$print .= "</table>";
									$print .= "</td>";
									
									// Delivery Address
									$print .= "<td width=\"50%\">";
										$print .= "<table cellpadding=\"5\" border=\"0\" width=\"100%\">";
											$print .= "<tr bgcolor=\"#EDEDED\"><td><strong>Delivery Address</strong></td></tr>";
											$print .= "<tr><td>".(!empty($strdeltitle) ? ucwords($strdeltitle)." " : "").(!empty($strdelfirstname) ? ucwords($strdelfirstname)." " : "").(!empty($strdelsurname) ? ucwords($strdelsurname) : "")."</td></tr>";
											$print .= "<tr><td>";
												if(!empty($strdeladd1)) $print .= $strdeladd1.", ";
												if(!empty($strdeladd2)) $print .= $strdeladd2.", ";
												if(!empty($strdeladd3)) $print .= $strdeladd3.", ";
												if(!empty($strdelTown)) $print .= $strdelTown.", ";
												if(!empty($strdelCnty)) $print .= $strdelCnty.", ";
												if(!empty($strdelCntry)) $print .= $strdelCntry.", ";
												if(!empty($strdelPstCde)) $print .= $strdelPstCde;
											$print .= "</td></tr>";
										$print .= "</table>";
									$print .= "</td>";
								$print .= "</tr>";
							
							$print .= "</table>";
						$print .= "</td>";
					$print .= "</tr>";
					
					$print .= "<tr><td colspan=\"3\"></td></tr>";
					
					// Contact Information
					$print .= "<tr>";
						$print .= "<td colspan=\"3\">";
							$print .= "<table cellpadding=\"0\" border=\"0\" width=\"100%\">";
								if(!empty($strphone)) $print .= "<tr><td><strong>Phone Number:</strong> ".$strphone."</td></tr>";
								if(!empty($stremail)) $print .= "<tr><td><strong>Email:</strong> ".$stremail."</td></tr>";
								if(!empty($externalOrderID)) $print .= "<tr><td><strong>External Reference:</strong> ".$externalOrderID."</td></tr>";
							$print .= "</table>";
						$print .= "</td>";
					$print .= "</tr>";
					
					$print .= "<tr><td colspan=\"3\"></td></tr>";
					
				$print .= "</tbody>";
			$print .= "</table></td>";
			$print .= "</tr><tr><td valign=\"top\">";

			$print .= ("<table cellpadding=\"5\" border=\"0\" width=\"100%\">
			<thead><tr bgcolor=\"#EDEDED\" style=\"text-align: right;\">
				<th style=\"font-weight: bold; text-align: left; border: 1px solid #E1E1E1; width: 30%;\">Name</th>
				<th style=\"font-weight: bold; text-align: left; border: 1px solid #E1E1E1; width: 10%;\">Qty</th>
				<th style=\"font-weight: bold; text-align: left; border: 1px solid #E1E1E1; width: 30%;\">Description</th>
				<th style=\"font-weight: bold; border: 1px solid #E1E1E1; width: 10%;\">Price</th>
				<th style=\"font-weight: bold; border: 1px solid #E1E1E1; width: 10%;\">VAT</th>
				<th style=\"font-weight: bold; border: 1px solid #E1E1E1; width: 10%;\">Total</th>
			</tr></thead><tbody>");

				$strdbsql = "SELECT * FROM order_header 
				WHERE recordID = :recordID";
				$strType = "single";
				$arrdbparams = array("recordID"=>$orderHeaderID);
				$orderDetails = query($conn,$strdbsql,$strType,$arrdbparams);
				
				$strdbsql = "SELECT order_items.*, site_vat.amount, stock_units.description AS unit, stock_group_information.name FROM order_items 
				INNER JOIN site_vat ON order_items.vatID = site_vat.recordID 
				INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID 
				INNER JOIN stock ON order_items.stockID = stock.recordID 
				INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
				INNER JOIN stock_units ON stock.unit = stock_units.recordID 
				WHERE orderHeaderID = :recordID AND display = 1";
				$strType = "multi";
				$arrdbparams = array("recordID"=>$orderHeaderID);
				$orderItems = query($conn,$strdbsql,$strType,$arrdbparams);
				
				$strdbsql = "SELECT sum(transactionAmount) AS amountPaid FROM order_payment_details INNER JOIN order_payment_status ON order_payment_details.paymentStatus = order_payment_status.recordID WHERE orderHeaderID = :recordID AND order_payment_status.countTowardsTotal = 1";
				$strType = "single";
				$arrdbparams = array("recordID"=>$orderHeaderID);
				$paymentDetails = query($conn,$strdbsql,$strType,$arrdbparams);
				
				//ajax to get items
				if (count($orderItems) > 0) {
					foreach($orderItems AS $orderItem){

						$lineAmount = number_format($orderItem['price'],2);
						$lineTax = "0.00";
						$lineTotal = number_format(($lineAmount * $orderItem['quantity']) + $lineTax,2);
						
						$print .= ("<tr>");
							$print .= ("<td style=\"border-left: 1px solid #E1E1E1; border-right: 1px solid #E1E1E1;\">".$orderItem['name']."</td>");
							$print .= ("<td style=\"border-left: 1px solid #E1E1E1; border-right: 1px solid #E1E1E1;\">".$orderItem['quantity']."</td>");
							$print .= ("<td style=\"border-left: 1px solid #E1E1E1; border-right: 1px solid #E1E1E1;\">".$orderItem['unit']."</td>");
							$print .= ("<td style=\"border-left: 1px solid #E1E1E1; border-right: 1px solid #E1E1E1; text-align: right;\">&pound;".$lineAmount."</td>");
							$print .= ("<td style=\"border-left: 1px solid #E1E1E1; border-right: 1px solid #E1E1E1; text-align: right;\">&pound;".$lineTax."</td>");
							$print .= ("<td style=\"border-left: 1px solid #E1E1E1; border-right: 1px solid #E1E1E1; text-align: right;\">&pound;".$lineTotal."</td>");
						$print .= ("</tr>");
					}
				} else {
					$print .= ("<tr><td style=\"border-left: 1px solid #E1E1E1; border-right: 1px solid #E1E1E1; text-align: center;\" colspan='6'>No stock lines</td></tr>");
				}
				
				$print .= ("<tr bgcolor=\"#EDEDED\">
				<td style=\"border: 1px solid #E1E1E1; border-right: solid 1px #EDEDED;\" colspan=\"5\">Subtotal</td>
				<td style=\"border: 1px solid #E1E1E1; border-left: solid 1px #EDEDED; text-align: right;\">&pound;".number_format($orderDetails['amountStock'], 2)."</td>
				</tr>");
				$print .= ("<tr bgcolor=\"#EDEDED\">
				<td style=\"border: 1px solid #E1E1E1; border-right: 1px solid #EDEDED;\" colspan=\"5\">Delivery</td>
				<td style=\"border: 1px solid #E1E1E1; border-left: 1px solid #EDEDED; text-align: right;\">&pound;".number_format($orderDetails['amountDelivery'], 2)."</td>
				</tr>");
				$print .= ("<tr bgcolor=\"#EDEDED\">
				<td style=\"border: 1px solid #E1E1E1; border-right: 1px solid #EDEDED;\" colspan=\"5\">VAT @ 0%</td>
				<td style=\"border: 1px solid #E1E1E1; border-left: 1px solid #EDEDED; text-align: right;\">&pound;".number_format($orderDetails['amountVat'], 2)."</td>
				</tr>");
				$print .= ("<tr bgcolor=\"#EDEDED\">
				<td style=\"border: 1px solid #E1E1E1; border-right: 1px solid #EDEDED;\" colspan=\"5\">Total</td>
				<td style=\"border: 1px solid #E1E1E1; border-left: 1px solid #EDEDED; text-align: right;\"><strong>&pound;".number_format($orderDetails['amountTotal'], 2)."</strong></td>
				</tr>");
				$print .= ("</tbody></table>");
			$print .= ("</td></tr>");
			$print .= ("<tr><td>");
			$print .= ("<table cellpadding=\"3\" border=\"0\" width=\"100%\">");
			$print .= ("<tr>
							<td width=\"180px\">
								<span style=\"font-size: 10px;\">
								<br/>"
								.date("d/m/Y   H:i", $datnow).
								"</span>
							</td><td>
								<span style=\"font-size: 10px; float:right;\">
								<br/>".(is_numeric($customerNotes) ? $customerNotes : "")."</span>
							</td>
						</tr>
						<tr>
							<td width=\"94%\" align=\"center\">
								<br/><br/>
								$customText1
							</td>
						</tr>
						<tr>
							<td colspan=\"2\" style=\"text-align: center;\" width=\"100%\">
								<span style=\"font-style: italic; font-weight: bold; font-size: 12px;\">Thank you for your order</span>
							</td>
						</tr>");
			$print .= ("</table>");
			$print .= ("</td></tr>");
			$print .= ("</tbody></table>");
		break;
	}

	
	if (isset($_REQUEST['print']) AND $_REQUEST['print'] == "pdf") {
		
		require_once('includes/tcpdf_include.php');
		
		// create new PDF document
		$pdf = new TCPDF("portrait", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor($strcompanyname);
		$pdf->SetTitle('Customer Invoice');
		$pdf->SetSubject('');

		// turn headers and footers off
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(15, 15, 15);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		$pdf->SetFont('helvetica', '', 10);
		//$pdf->SetFont('kozminproregular', '', 9);

		// add a page
		switch ($documentType){
			case "giftMessage":
				$pdf->AddPage('L', 'A5');
			break;
			default:
				$pdf->AddPage();
			break;
		}
		$pdf->writeHTML($print, true, false, true, false, '');
		
		$pdf->lastPage();// reset pointer to the last page
		
		ob_end_clean();
		$pdf->Output("Customer_Invoice_".date("d-m-Y", $datnow).".pdf", 'I');
	} else {
	
?>
		<script Language="JavaScript">
		function jscall_printpdf(orderHeaderID, timestamp)
		{
			window.open("/invoiceprint.php?orderHeaderID="+orderHeaderID+"&timestamp="+timestamp+"&print=pdf"); 
		}
		</script>
<?php
		if ($strerror != '') { print ("<div class='notification-error'><h3>Error</h3><p>$strerror</p></div>"); }
		print $print;
		print ("<div style='display:block; clear:both; padding:10px; overflow:auto;' class='buttonHolder'><button style='float:right;' type='button' class='short_button orange' onclick='jscall_printpdf(\"".$_REQUEST['orderHeaderID']."\", \"".$_REQUEST['timestamp']."\");'>Print Invoice</button></div>");

	}
?>
	<script language='Javascript'>
		$().ready(function() {
		});
	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>
