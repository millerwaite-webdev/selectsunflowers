<?php
/*-------------------------------------------------------------------------*/
/* Image Manipulation functions                                            */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* Current Version: 2.4                                                    */
/* ------- -------- ---                                                    */
/*                                                                         */
/* Version 2.4 25/06/2014												   */
/* - Wrap imagebmp and imasgecreatefrombmp in if not exists for PHP 7+     */
/*   compatability as these functions have been added to core GD library   */
/*                                                                         */
/* Version 2.3 25/06/2014												   */
/* - Moved a make an iptc tag function into library from another page      */
/*                                                                         */
/* Version  2.2  06/11/2013                                                */
/* - Included meta data from original image in resized versions.           */
/*   Included feature in alpha mask and watermark function but not overlay.*/
/*   With masks and marks main image is used as meta data source but       */
/*   with overlay priority source data can't be determined logically.      */
/*                                                                         */
/* Version  2.1  30/04/2013                                                */
/* - Included functions to add bmp support                                 */
/* - File import uses mime type rather than extension                      */
/*                                                                         */
/* Version  2.0  29/04/2013                                                */
/* - Version control added                                                 */
/* - Two new functions to restrict thumb to area selection on original     */
/* - Two new functions for detecting file type and converting between files*/
/*   and php image resources rather than repeating same tests in each      */
/*   function                                                              */
/* - Fixed bug so that file extensions are checked case-insenitively when  */
/*   working with filenames                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Function Manifest                                                       */
/* --------                                                                */
/*                                                                         */
/* RESIZE - Change Shape by pad or crop                                    */
/* image_scaleandpad(currentfile,newname,new_w,new_h,r,g,b)                */
/* image_scaleandcrop(currentfile,newname,new_w,new_h)                     */
/* image_croptobounds(currentfile,newname,x1,y1,x2,y2)                     */
/* image_scalefrombounds(currentfile,newname,new_w,new_h,x1,y1,x2,y2)      */
/*                                                                         */
/* RESIZE - To guide line  maintain aspect ratio                           */
/* image_scaletowidth(currentfile,newname,new_w)                           */
/* image_scaletoheight(currentfile,newname,new_h)                          */
/* image_scaletomax(currentfile,newname,new_w, new_h)                      */
/* image_shrinktomax(currentfile,newname,new_w, new_h)                     */
/*                                                                         */
/* OVERLAY - combine two images as layers                                  */
/* imagealphamask(currentfile, overlay, newname)                           */
/* imagewatermark(currentfile,overlay,alpha,newname)                       */
/* imageoverlay(currentfile,overlay,newname, position)                     */
/*                                                                         */
/* FILE -test type using extension                                         */
/* imagecreatefromfile(filename)                                           */
/* imagefile(filename, resource)                                           */
/* imagecopyiptc(sourcefile,destfile)                                      */
/* imagecreatefrombmp(filename)                                            */
/* imagebmp(image,filename)                                                */
/* iptc_make_tag(rec, data, value)                                         */
/*                                                                         */
/* INTERNAL                                                                */
/* getExtension(currentfile)                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/* Credits                                                                 */
/* -------                                                                 */
/*                                                                         */
/* Most Image functions were loosely based on make_thumb                   */
/* http://davidwalsh.name/create-image-thumbnail-php                       */
/*                                                                         */
/* getExtension function found on-line with no credits                     */
/*                                                                         */
/* imagebmp, dword, word by de77 under MIT Licence                         */
/*                                                                         */
/* imagecreatefrombmp by Mad Rog http://www.groupofdots.com/               */
/* based on ImageCreateFromBMP by DHKold admin@dhkold.com                  */
/*                                                                         */
/* iptc_make_tag() function by Thies C. Arntzen                            */
/*                                                                         */
/* All other functions written and collection compiled by Rhian Singleton  */
/* for user by Miller Waite Limited at its customers					   */
/* rhiansingleton@millerwaite.com                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* image_scaleandpad                                                       */
/*-------------------------------------------------------------------------*/
/* takes in an image from file and creates a new version of new size adding*/
/* a background to pad diffrence in aspect ratio default is white or clear */
/* (if original had alpha channel) also optionly set new filename/extension*/
/*------------------------------------------------------------------------ */
/*  Parameters                                                             */
/*  $img_name = location of image to be transformed                        */
/*  $filename = location to save new image (can be same)                   */
/*  $new_w    = hotizontal width of new image                              */
/*  $new_h    = vertical width of new image                                */
/*  $r        = r value of background colour (default 255)                 */
/*  $g        = g value of background colour (default 255)                 */
/*  $b        = b value of background colour (default 255)                 */
/*-------------------------------------------------------------------------*/


 function image_scaleandpad($img_name,$filename,$new_w,$new_h, $r=null, $g=null, $b=null)
 {
	if ($filename == NULL) $filename = $img_name;

	//create image resource
	$src_img=imagecreatefromfile($img_name);
	


	//gets the dimensions of the image
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	// next we will calculate the new dimmensions for the thumbnail image
	$ratio1=$old_x/$new_w;
	$ratio2=$old_y/$new_h;
	if($ratio1>$ratio2) {
		$thumb_w=$new_w;
		$thumb_h=$old_y/$ratio1;
	} else {
		$thumb_h=$new_h;
		$thumb_w=$old_x/$ratio2;
	}

	// create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($new_w,$new_h);
	
	//work out centering
	$starty = ($new_h - $thumb_h)/2;
	$startx = ($new_w - $thumb_w)/2;

	//set background color for new image
	if($r != null && $g != null && $b != null) {
		//user defined
		imagefill ( $dst_img , 0 , 0 , imagecolorallocate ( $dst_img , $r, $g , $b ) );
	} else {
		//set background to transparency colour if available
		$transcol = imagecolortransparent($src_img);
		if ($transcol != -1) {
			imagefill ( $dst_img , 0 , 0 , $transcol);
			//maintain existing transparency
			imagealphablending($dst_img, false);
			imagesavealpha($dst_img, true);
			imagealphablending($src_img, true);
		} else {
			//default to white
			imagefill ( $dst_img , 0 , 0 , imagecolorallocate ( $dst_img , 255, 255 , 255 ) );
		}

	}

	// resize the big image to the new created one
	imagecopyresampled($dst_img,$src_img,$startx,$starty,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

	// output the created image to the file. Now we will have the thumbnail into the file named by $filename
	imagefile($filename, $dst_img);	
	imagecopyiptc($img_name,$filename);
	
	//destroys source and destination images.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}


/*------------------------------------------------------------*/
/* image_scaleandcrop                                         */
/*------------------------------------------------------------*/
/* takes in an image create a new version with a different    */
/* filename and set size cropping image around centre for     */
/* diffrence in aspect ratio                                  */
/*                                                            */
/*  Parameters                                                 */
/*  $img_name = location of image to be transformed           */
/*  $filename = location to save new image (can be same)      */
/*  $new_w    = hotizontal width of new image                 */
/*  $new_h    = vertical width of new image                   */
/*------------------------------------------------------------*/

 function image_scaleandcrop($img_name,$filename,$new_w,$new_h)
 {
	if ($filename == NULL) $filename = $img_name;

	//create image resource
   	$src_img=imagecreatefromfile($img_name);

	//gets the dimmensions of the image
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	// next we will calculate the new dimmensions for the thumbnail image
	$ratio1=$old_x/$new_w;
	$ratio2=$old_y/$new_h;
	if($ratio1<$ratio2) {
		$thumb_w=$new_w;
		$thumb_h=$old_y/$ratio1;
	} else {
		$thumb_h=$new_h;
		$thumb_w=$old_x/$ratio2;
	}

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($new_w,$new_h);

	//work out centering
	$starty = ($new_h - $thumb_h)/2;
	$startx = ($new_w - $thumb_w)/2;

	// maintain transparency
	imagealphablending($dst_img, false);
	imagesavealpha($dst_img, true);
	imagealphablending($src_img, true);

	// resize the big image to the new created one
	imagecopyresampled($dst_img,$src_img,$startx,$starty,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

	
	// output the created image to the file. Now we will have the thumbnail into the file named by $filename
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);

	//destroys source and destination images.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}
 
/*------------------------------------------------------------*/
/* image_croptobounds                                         */
/*------------------------------------------------------------*/
/* takes in an image create a new version with a different    */
/* filename and crops image to bounds provided using xy coords*/
/*                                                            */
/*  Parameters                                                */
/*  $img_name = location of image to be transformed           */
/*  $filename = location to save new image (can be same)      */
/*  $x1,y1    = top left of bounds box relative to top left   */
/*              of graphic                                    */
/*  $x2,y2    = bottom right of bounds box relative to top    */
/*              left of graphic                               */
/*------------------------------------------------------------*/

 function image_croptobounds($img_name,$filename,$x1,$y1,$x2,$y2)
 {
 	//new file is null overwrite existing
	if ($filename == NULL) $filename = $img_name;

	//create image resource from file
   	$src_img=imagecreatefromfile($img_name);

	//calculate the new dimmensions for the new image
	$thumb_w = $x2-$x1;
	$thumb_h = $y2-$y1;
	$starty = $y1;
	$startx = $x1;

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);

	// maintain transparency
	imagealphablending($dst_img, false);
	imagesavealpha($dst_img, true);
	imagealphablending($src_img, true);

	// resize the big image to the newly created one
	imagecopyresampled($dst_img,$src_img,0,0,$startx,$starty,$thumb_w,$thumb_h,$thumb_w,$thumb_h);

	
	// output the created image to the file.
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);
	
	
	//destroy source and destination images from php memory.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}

/*------------------------------------------------------------*/
/* image_scalefrombounds                                      */
/*------------------------------------------------------------*/
/* takes in an image create a new version with a different    */
/* filename and size, takes crops image from bounds provided  */
/* using xy coords and scales to desired size                 */
/*                                                            */
/*  Parameters                                                */
/*  $img_name = location of image to be transformed           */
/*  $filename = location to save new image (can be same)      */
/*  $new_w    = hotizontal width of new image                 */
/*  $new_h    = vertical width of new image                   */
/*  $x1,y1    = top left of bounds box relative to top left   */
/*              of graphic                                    */
/*  $x2,y2    = bottom right of bounds box relative to top    */
/*              left of graphic                               */
/*------------------------------------------------------------*/

 function image_scalefrombounds($img_name,$filename,$new_w,$new_h,$x1,$y1,$x2,$y2)
 {
	if ($filename == NULL) $filename = $img_name;

	//creates the new image using the appropriate function from gd library
   	$src_img=imagecreatefromfile($img_name);

	// next we will calculate the new dimmensions for the thumbnail image
	$thumb_w = $x2-$x1;
	$thumb_h = $y2-$y1;
	$starty = $y1;
	$startx = $x1;

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($new_w,$new_h);

	// maintain transparency
	imagealphablending($dst_img, false);
	imagesavealpha($dst_img, true);
	imagealphablending($src_img, true);

	// resize the big image to the newly created one
	imagecopyresampled($dst_img,$src_img,0,0,$startx,$starty,$new_w,$new_h,$thumb_w,$thumb_h);

	
	// output the created image to the file.
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);
	
	
	//destroy source and destination images from php memory.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}

/*------------------------------------------------------------*/
/* image_scaletowidth                                         */
/*------------------------------------------------------------*/
/* takes in an image create a new version with a different    */
/* filename and size image is scaled to largest that will fit */
/* in new image width - maintaining aspect ratio.             */
/*                                                            */
/*  Parameters                                                */
/*  $img_name = location of image to be transformed           */
/*  $filename = location to save new image (can be same)      */
/*  $new_w    = hotizontal width of new image                 */
/*------------------------------------------------------------*/

 function image_scaletowidth($img_name,$filename,$new_w)
 {
	if ($filename == NULL) $filename = $img_name;

	//creates the new image using the appropriate function from gd library
   	$src_img=imagecreatefromfile($img_name);

	//gets the dimmensions of the image
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	// next we will calculate the new dimmensions for the thumbnail image
	$ratio1=$old_x/$new_w;
	$thumb_w=$new_w;
	$thumb_h=$old_y/$ratio1;

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);

	// maintain transparency
	imagealphablending($dst_img, false);
	imagesavealpha($dst_img, true);
	imagealphablending($src_img, true);

	// resize the big image to the new created one
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);


	
	// output the created image to the file. Now we will have the thumbnail into the file named by $filename
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);
	
	//destroys source and destination images.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}

/*------------------------------------------------------------*/
/* image_scaletoheight                                       */
/*------------------------------------------------------------*/
/* takes in an image create a new version with a different    */
/* filename and size image is scaled to largest that will fit */
/* in new image height - maintaining aspect ratio.            */
/*                                                            */
/*  Parameters                                                */
/*  $img_name = location of image to be transformed           */
/*  $filename = location to save new image (can be same)      */
/*  $new_h    = vertical width of new image                   */
/*------------------------------------------------------------*/

 function image_scaletoheight($img_name,$filename,$new_h)
 {
	if ($filename == NULL) $filename = $img_name;

	//creates the new image using the appropriate function from gd library
   	$src_img=imagecreatefromfile($img_name);

	//gets the dimmensions of the image
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	// next we will calculate the new dimmensions for the thumbnail image
	$ratio2=$old_y/$new_h;
	$thumb_h=$new_h;
	$thumb_w=$old_x/$ratio2;

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);

	// maintain transparency
	imagealphablending($dst_img, false);
	imagesavealpha($dst_img, true);
	imagealphablending($src_img, true);

	// resize the big image to the new created one
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);


	// output the created image to the file. Now we will have the thumbnail into the file named by $filename
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);

	//destroys source and destination images.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}


/*------------------------------------------------------------*/
/* image_scaletomax                                           */
/*------------------------------------------------------------*/
/* takes in an image create a new version with a different    */
/* filename and size image is scaled to largest that will fit */
/* in new image size - maintaining aspect ratio  will shrink  */
/* or enlarge the image to match new size                     */
/*                                                            */
/*  Parameters                                                */
/*  $img_name = location of image to be transformed           */
/*  $filename = location to save new image (can be same)      */
/*  $new_w    = hotizontal width of new image                 */
/*  $new_h    = vertical width of new image                   */
/*------------------------------------------------------------*/

 function image_scaletomax($img_name,$filename,$new_w, $new_h)
 {
	if ($filename == NULL) $filename = $img_name;

	//creates the new image using the appropriate function from gd library
   	$src_img=imagecreatefromfile($img_name);

	//gets the dimmensions of the image
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	// next we will calculate the new dimmensions for the thumbnail image
	$ratio1=$old_x/$new_w;
	$ratio2=$old_y/$new_h;
	if($ratio1>$ratio2) {
		$thumb_w=$new_w;
		$thumb_h=$old_y/$ratio1;
	} else {
		$thumb_h=$new_h;
		$thumb_w=$old_x/$ratio2;
	}

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
	$starty = 0;
	$startx = 0;

	// maintain transparency
	imagealphablending($dst_img, false);
	imagesavealpha($dst_img, true);
	imagealphablending($src_img, true);

	// resize the big image to the new created one
	imagecopyresampled($dst_img,$src_img,$startx,$starty,0,0,$thumb_w,$thumb_h,$old_x,$old_y);



	// output the created image to the file. Now we will have the thumbnail into the file named by $filename
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);

	//destroys source and destination images.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}

/*------------------------------------------------------------*/
/* image_shrinktomax                                          */
/*------------------------------------------------------------*/
/* takes in an image create a new version with a different    */
/* filename and size image is shrunk to largest that will fit */
/* in new image size - maintaining aspect ratio. will leave   */
/* images smaller than new size as is                         */
/*                                                            */
/*  Parameters                                               */
/*  $img_name = location of image to be transformed           */
/*  $filename = location to save new image (can be same)      */
/*  $new_w    = hotizontal width of new image                 */
/*  $new_h    = vertical width of new image                   */
/*------------------------------------------------------------*/

function image_shrinktomax($img_name,$filename,$new_w, $new_h)
{
	if ($filename == NULL) $filename = $img_name;

	//creates the new image using the appropriate function from gd library
   	$src_img=imagecreatefromfile($img_name);

	//gets the dimmensions of the image
	$old_x=imageSX($src_img);
	$old_y=imageSY($src_img);

	// if image is larger in either direction than target
	if ($new_w < $old_x || $new_h < $old_y)
	{
		//look for change in aspect ratio
		$ratio1=$old_x/$new_w;
		$ratio2=$old_y/$new_h;

		//x is biggest change
		if($ratio1>$ratio2) {
			$thumb_w=$new_w;
			$thumb_h=$old_y/$ratio1;
		//y is biggest change
		} else {
			$thumb_h=$new_h;
			$thumb_w=$old_x/$ratio2;
		}
	//image is smaller in both directions
	} else {
		//no change
		$thumb_w = $old_x;
		$thumb_h = $old_y;
	}

	// we create a new image with the new dimmensions
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
	
	//maintain any transparency
	imagealphablending($dst_img, false);
	imagesavealpha($dst_img, true);
	imagealphablending($src_img, true);

	// resize the big image to the new created one
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

	
	// output the created image to the file. Now we will have the thumbnail into the file named by $filename
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);

	//destroys source and destination images.
	imagedestroy($dst_img);
	imagedestroy($src_img);
}




/*------------------------------------------------------------*/
/* imagealphamask                                             */
/*------------------------------------------------------------*/
/* takes in 2 image filenames uses second file as a           */
/* transparency mask on first. mask should be black and white */
/* avoid jpegs for this file as artifacts may affect result   */
/*                                                            */
/*  Parameters                                                */
/*  $picture           = file name with or without path       */
/*  $mask              = file name with or without path       */
/*  $filename          = file name with or without path       */
/*                                                            */
/*  Return (none)                                             */
/*------------------------------------------------------------*/

function imagealphamask($picture, $mask, $filename=NULL) {
	if ($filename == NULL) $filename = $picture;
	//creates the new image using the appropriate function from gd library
   	$src_img=imagecreatefromfile($picture);
	$msk_img=imagecreatefromfile($mask);


    $xSize = imagesx( $src_img );
    $ySize = imagesy( $src_img );
    $dst_img = imagecreatetruecolor( $xSize, $ySize );
    imagesavealpha( $dst_img, true );
    imagefill( $dst_img, 0, 0, imagecolorallocatealpha( $dst_img, 0, 0, 0, 127 ) );
 

    // Perform pixel-based alpha map application 
    for( $x = 0; $x < $xSize; $x++ ) { 
        for( $y = 0; $y < $ySize; $y++ ) { 
            $alpha = imagecolorsforindex( $msk_img, imagecolorat( $msk_img, $x, $y ) );
            $alpha = 127 - floor( $alpha[ 'red' ] / 2 ); 
            $color = imagecolorsforindex( $src_img, imagecolorat( $src_img, $x, $y ) );
            imagesetpixel( $dst_img, $x, $y, imagecolorallocatealpha( $dst_img, $color[ 'red' ], $color[ 'green' ], $color[ 'blue' ], $alpha ) );
        } 
    }


    imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);

    // clear up files in memory
    imagedestroy( $src_img );
    imagedestroy( $msk_img );
    imagedestroy( $dst_img );
}
/*------------------------------------------------------------*/
/* imagewatermark                                             */
/*------------------------------------------------------------*/
/* this script creates a watermarked version of an image from */
/* two image files one for the image and one for the watermark*/
/* transparence and out put file can be altered               */
/*                                                            */
/*  Parameters                                                */
/*  $picture           = file name with or without path       */
/*  $overlay           = file name with or without path       */
/*  $alpha             = decimal 0-1 transparency of watermark*/
/*  $destination       = file name with or without path       */
/*                                                            */
/*  Return (none)                                             */
/*------------------------------------------------------------*/


function imagewatermark($picture,$overlay,$alpha=0.6,$filename=NULL)
{

	if ($filename == NULL) $filename = $picture;

	//creates the new image using the appropriate function from gd library
	$src_img=imagecreatefromfile($picture);
	$ovl_img=imagecreatefromfile($overlay);


	$imagewidth = imagesx($src_img);
	$imageheight = imagesy($src_img);
     
	$dst_img=ImageCreateTrueColor($imagewidth,$imageheight);


	$watermarkwidth =  imagesx($ovl_img);
	$watermarkheight =  imagesy($ovl_img);

	$startwidth = (($imagewidth - $watermarkwidth)/2);
	$startheight = (($imageheight - $watermarkheight)/2);
	$endheight = $startheight + $watermarkheight - 1;
	$endwidth = $startwidth + $watermarkwidth - 1;
	if ($endwidth > $imagewidth) $endwidth = $imagewidth;
	if ($endheight > $imageheight) $endheight = $imageheight;

	//get colour of background for overlay
	$trans = imagecolorat($ovl_img,30,30);

	//Reads the original colors pixel by pixel
	for ($y=0;$y<$imageheight;$y++) {
		if ($y >=$startheight && $y <= $endheight) {
			$y2 = $y - $startheight;
		}
		for ($x=0;$x<$imagewidth;$x++) {
			//get the colour of the main image
			$rgb1 = imagecolorat($src_img,$x,$y);
			$r1 = ($rgb1 >> 16) & 0xFF;
			$g1 = ($rgb1 >> 8) & 0xFF;
			$b1 = $rgb1 & 0xFF;
			
			if ($y >=$startheight && $y <= $endheight && $x >=$startwidth && $x <= $endwidth) {
				$x2 = $x - $startwidth;

				//get the colour of the watermark image
				$rgb2 = imagecolorat($ovl_img,$x2,$y2);
				$r2 = ($rgb2 >> 16) & 0xFF;
				$g2 = ($rgb2 >> 8) & 0xFF;
				$b2 = $rgb2 & 0xFF;

				//if oerlaycolour is not background
				if ($rgb2 != $trans) {
					//generate combined colour values
					$r = ((1-$alpha)*$r1 + $alpha*$r2);
					$g = ((1-$alpha)*$g1 + $alpha*$g2);
					$b = ((1-$alpha)*$b1 + $alpha*$b2);
					imagesetpixel($dst_img,$x,$y,imagecolorallocate($dst_img, $r, $g, $b));
				}
				else imagesetpixel($dst_img,$x,$y,imagecolorallocate($dst_img, $r1, $g1, $b1));
			}
		}
	}
	
	imagefile($filename, $dst_img);
	imagecopyiptc($img_name,$filename);

	// clear up files in memory
	imagedestroy( $src_img );
	imagedestroy( $ovl_img );
	imagedestroy( $dst_img );
}
/*------------------------------------------------------------*/
/* imageoverlay                                               */
/*------------------------------------------------------------*/
/* this script creates a new version of an image from         */
/* two image files one is overlayed on top of the other       */
/* transparence and out put file can be altered               */
/*                                                            */
/*  Parameters                                               */
/*  $picture           = file name with or without path       */
/*  $overlay           = file name with or without path       */
/*  $filename          = file name with or without path       */
/*  $position          = string describing the position of the*/
/*                       based on corners, middle of edges or */
/*                       center                               */
/*                                                            */
/*  Return (none)                                             */
/*------------------------------------------------------------*/

function imageoverlay ($picture,$overlay,$filename, $position='center') {

	if ($filename == NULL) $filename = $picture;

	//creates the new image using the appropriate function from gd library
	$dst_img=imagecreatefromfile($picture);
	$src_img=imagecreatefromfile($overlay);

	$dst_w = imagesx($dst_img);
	$dst_h = imagesy($dst_img);
	$src_w = imagesx($src_img);
	$src_h = imagesy($src_img);


	imagealphablending($dst_img,true);
	imagealphablending($src_img,true);
	if ($position == 'random') {
		$position = rand(1,8);
	}

    switch ($position) {
        case 'top-right': 
        case 'right-top': 
        case 1:
            imagecopy($dst_img, $src_img, ($dst_w-$src_w), 0, 0, 0, $src_w, $src_h);
         break;
        case 'top-left':
        case 'left-top':
        case 2:
            imagecopy($dst_img, $src_img, 0, 0, 0, 0, $src_w, $src_h);
        break;
        case 'bottom-right':
        case 'right-bottom':
        case 3:
            imagecopy($dst_img, $src_img, ($dst_w-$src_w), ($dst_h-$src_h), 0, 0, $src_w, $src_h);
         break; 
        case 'bottom-left': 
        case 'left-bottom': 
        case 4: 
            imagecopy($dst_img, $src_img, 0 , ($dst_h-$src_h), 0, 0, $src_w, $src_h);
         break;
        case 'center':
        case 5:
            imagecopy($dst_img, $src_img, (($dst_w/2)-($src_w/2)), (($dst_h/2)-($src_h/2)), 0, 0, $src_w, $src_h);
         break; 
        case 'top': 
        case 6: 
            imagecopy($dst_img, $src_img, (($dst_w/2)-($src_w/2)), 0, 0, 0, $src_w, $src_h);
         break;
        case 'bottom':
        case 7:
            imagecopy($dst_img, $src_img, (($dst_w/2)-($src_w/2)), ($dst_h-$src_h), 0, 0, $src_w, $src_h);
         break;
        case 'left': 
        case 8: 
            imagecopy($dst_img, $src_img, 0, (($dst_h/2)-($src_h/2)), 0, 0, $src_w, $src_h);
         break;
        case 'right':
        case 9:
            imagecopy($dst_img, $src_img, ($dst_w-$src_w), (($dst_h/2)-($src_h/2)), 0, 0, $src_w, $src_h);
         break; 
    }
	
	

    imagefile($filename, $dst_img);	

    imagedestroy($src_img);
    imagedestroy($dst_img);
} 


/*------------------------------------------------------------*/
/* getextension                                               */
/*------------------------------------------------------------*/
/* takes in a filename and extracts the file extension        */
/* used in all other forulas                                  */
/*                                                            */
/*  Parameters                                                */
/*  $str           = file name with or without path           */
/*                                                            */
/*  Return                                                    */
/*  $ext (string) = file extension                            */
/*------------------------------------------------------------*/

function getExtension($str) {
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}
/*------------------------------------------------------------*/
/* imagefile                                           */
/*------------------------------------------------------------*/
/* takes in a filename and php image and saves the image to   */
/* the file name provided using the correct file type         */
/*                                                            */
/*  Attributes                                                */
/*  $filename      = file name with or without path           */
/*  $image         = php image resource                       */
/*                                                            */
/*  Return                                                    */
/*  $success(bool) = boolean to declare success or failure    */
/*------------------------------------------------------------*/
function imagefile($filename, $image) {
	$extn=strtolower(getExtension($filename));

	if(!strcmp("png",$extn)) return imagepng($image,$filename);
	else if(!strcmp("jpg",$extn) || !strcmp("jpeg",$extn)) return imagejpeg($image,$filename);
	else if(!strcmp("gif",$extn)) return imagegif($imageg,$filename);
	else return false;
}

/*------------------------------------------------------------*/
/* imagecreatefromfile                                        */
/*------------------------------------------------------------*/
/* takes in a filename creates php image based on mime type   */
/*                                                            */
/*  Attributes                                                */
/*  $filename      = file name with or without path           */
/*  $image         = php image resource                       */
/*                                                            */
/*  Return                                                    */
/*  $success(bool) = boolean to declare success or failure    */
/*------------------------------------------------------------*/
function imagecreatefromfile($filename) {
	$imgdata = getimagesize($filename);
	$mime = $imgdata['mime'];

	if($mime == "image/jpeg" || $mime == "image/pjjpeg") $image=imagecreatefromjpeg($filename);
	else if($mime == "image/png") $image=imagecreatefrompng($filename);
	else if($mime == "image/gif") $image=imagecreatefromgif($filename);
	else if($mime == "image/bmp" || $mime == "image/x-windows-bmp") $image=imagecreatefrombmp($filename);
	else $image = false;
	
	if($image == false) {
		$extn=strtolower(getExtension($filename));
		if(!strcmp("png",$extn)) $image=imagecreatefrompng($filename);
		else if(!strcmp("jpg",$extn) || !strcmp("jpeg",$extn)) $image=imagecreatefromjpeg($filename);
		else if(!strcmp("gif",$extn)) $image=imagecreatefromgif($filename);
		else if(!strcmp("bmp",$extn)) $image=imagecreatefrombmp($filename);
		else $image = false;
	}

	return $image;
}

/*------------------------------------------------------------*/
/* imagecopyiptc                                              */
/*------------------------------------------------------------*/
/* copy iptc data from one image to another. skip if iptc     */
/* set in destination image                                   */
/*                                                            */
/*  Attributes                                                */
/*  $srcimage      = file name of source image                */
/*  $destimage     = file name of destination image           */
/*                                                            */
/*  Return                                                    */
/*  $success(bool) = boolean to declare success or failure    */
/*------------------------------------------------------------*/
function imagecopyiptc($srcimage,$destimage) {
	//get iptc metadata from source image
	$size = getimagesize($srcimage, $srcinfo);
	$size2 = getimagesize($destimage, $destinfo);
	if(isset($srcinfo['APP13'])){
		if(!isset($destinfo['APP13'])) {
			$content = iptcembed($srcinfo['APP13'], $destimage); 
			$fw = fopen($destimage, 'w'); 
			fwrite($fw, $content); 
			fclose($fw); 
			return true;
		} else {
			error_log("IPTC Data can not be added to a file where it already exists.");
			return false;
		}
	} else {
		error_log("No IPTC Data to be copied.");
		return false;
	}
}

/*********************************************/
/* Fonction: ImageCreateFromBMP              */
/* Author:   DHKold                          */
/* Contact:  admin@dhkold.com                */
/* Date:     The 15th of June 2005           */
/* Version:  2.0B                            */
/*********************************************/
if(!function_exists("imagecreatefrombmp")) {
	function imagecreatefrombmp($filename)
	{
		//Ouverture du fichier en mode binaire
		if (! $f1 = fopen($filename,"rb")) return FALSE;
		
		//1 : Chargement des ent?tes FICHIER
		$FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
		if ($FILE['file_type'] != 19778) return FALSE;
		
		//2 : Chargement des ent?tes BMP
		$BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
		'/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
		'/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
		$BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
		if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
		$BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
		$BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
		$BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
		$BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
		$BMP['decal'] = 4-(4*$BMP['decal']);
		if ($BMP['decal'] == 4) $BMP['decal'] = 0;
		
	 //3 : Chargement des couleurs de la palette
	   $PALETTE = array();
	   if ($BMP['colors'] < 16777216)
	   {
		$PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
	   }

	 //4 : Creation de l'image
	   $IMG = fread($f1,$BMP['size_bitmap']);
	   $VIDE = chr(0);

	   $res = imagecreatetruecolor($BMP['width'],$BMP['height']);
	   $P = 0;
	   $Y = $BMP['height']-1;
	   while ($Y >= 0)
	   {
		$X=0;
		while ($X < $BMP['width'])
		{
		 if ($BMP['bits_per_pixel'] == 24)
			 {
			$COLOR = @unpack("V",substr($IMG,$P,3).$VIDE);
			 }
		 elseif ($BMP['bits_per_pixel'] == 16)
		 {
			$COLOR = unpack("n",substr($IMG,$P,2));
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 elseif ($BMP['bits_per_pixel'] == 8)
		 {
			$COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 elseif ($BMP['bits_per_pixel'] == 4)
		 {
			$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
			if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 elseif ($BMP['bits_per_pixel'] == 1)
		 {
			$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
			if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
			elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
			elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
			elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
			elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
			elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
			elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
			elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 else
			return FALSE;

			if ($COLOR === false)
					return false;

		 imagesetpixel($res,$X,$Y,$COLOR[1]);
		 $X++;
		 $P += $BMP['bytes_per_pixel'];
		}
		$Y--;
		$P+=$BMP['decal'];
	   }

	 //Fermeture du fichier
	   fclose($f1);

	 return $res;
	}
}

// Save 24bit BMP files

// Author: de77
// Licence: MIT
// Webpage: de77.com
// Version: 07.02.2010
if(!function_exists("imagebmp")) {
	function imagebmp(&$img, $filename = false) {
		$wid = imagesx($img);
		$hei = imagesy($img);
		$wid_pad = str_pad('', $wid % 4, "\0");

		$size = 54 + ($wid + $wid_pad) * $hei;

		//prepare & save header
		$header['identifier'] = 'BM';
		$header['file_size'] = dword($size);
		$header['reserved'] = dword(0);
		$header['bitmap_data'] = dword(54);
		$header['header_size'] = dword(40);
		$header['width'] = dword($wid);
		$header['height'] = dword($hei);
		$header['planes'] = word(1);
		$header['bits_per_pixel']= word(24);
		$header['compression']= dword(0);
		$header['data_size'] = dword(0);
		$header['h_resolution'] = dword(0);
		$header['v_resolution'] = dword(0);
		$header['colors'] = dword(0);
		$header['important_colors'] = dword(0);

		if ($filename) {
			$f = fopen($filename, "wb");
			foreach ($header AS $h) {
				fwrite($f, $h);
			}

			//save pixels
			for ($y=$hei-1; $y>=0; $y--) {
				for ($x=0; $x<$wid; $x++) {
					$rgb = imagecolorat($img, $x, $y);
					fwrite($f, byte3($rgb));
				}
				fwrite($f, $wid_pad);
			}

			return fclose($f);
		}

		else {
			foreach ($header AS $h) {
				echo $h;
			}

			//save pixels
			for ($y = $hei - 1; $y >= 0; $y--) {
				for ($x=0; $x<$wid; $x++) {
					$rgb = imagecolorat($img, $x, $y);
					echo byte3($rgb);
				}
				echo $wid_pad;
			}

			return true;
		}
	}
}
function byte3($n) {
	return chr($n & 255) . chr(($n >> 8) & 255) . chr(($n >> 16) & 255);    
}

function dword($n) {
	return pack("V", $n);
}

function word($n) {
	return pack("v", $n);
}

// Make an iptc tag for a file
// function by Thies C. Arntzen
// http://stackoverflow.com/questions/5871728/writing-exif-and-itpc-data-in-php
	function iptc_make_tag($rec, $data, $value)
	{
		$length = strlen($value);
		$retval = chr(0x1C) . chr($rec) . chr($data);

		if($length < 0x8000)
		{
			$retval .= chr($length >> 8) .  chr($length & 0xFF);
		}
		else
		{
			$retval .= chr(0x80) . 
					   chr(0x04) . 
					   chr(($length >> 24) & 0xFF) . 
					   chr(($length >> 16) & 0xFF) . 
					   chr(($length >> 8) & 0xFF) . 
					   chr($length & 0xFF);
		}

		return $retval . $value;
	}
?>