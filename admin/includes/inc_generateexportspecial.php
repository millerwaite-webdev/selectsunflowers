<?php


	$intcreatefile = 1;
	error_log("inc_generateexportspecial.php TOP");

	$strwhocalledme = basename($_SERVER['SCRIPT_FILENAME']);
	error_log("Who called: ".$strwhocalledme);	

	$strdbsql = "SELECT * FROM configFileExportFormats WHERE exportID=:exportID";
	$strType = "single";
	$arrdbparams = array("exportID"=>$intexportID);
	$resultdatasingle = query($conn,$strdbsql,$strType,$arrdbparams);

	$strspecialsql = str_replace("","\"",$resultdatasingle['specialSQL']);
	$strmatchvariable = $resultdatasingle['matchVariable'];
	$strmatchvalue = $resultdatasingle['matchValue'];

	$strupdatesql = str_replace("","\"",$resultdatasingle['updateSQL']);
	$strupdatevariable = $resultdatasingle['updateVariable'];
	$strupdatevalue = $resultdatasingle['updateValue'];

	$arrcolumnHeader = $resultdatasingle['columnHeader'];
	$arrcolumnMatch = $resultdatasingle['columnMatch'];
	$strfileformat = $resultdatasingle['fileFormat'];
	$strdocumentdescription = $resultdatasingle['formatName'];
	$strdelimiterField = $resultdatasingle['fileDelimiter'];
	$intshowColumns = $resultdatasingle['showColumns'];
	$emailTo = explode(",",$resultdatasingle['emailTo']);

	$streol = "\r\n";
	$arrdocument = array();		// the array containing the document
	$arrline = array();			// the array containing the currently being created line of the document
	$strfilename = "";
	
	
	error_log($strdocumentdescription);
	switch ($strdocumentdescription) {
	
		case "Stock Sales":
		
			array_push($arrdocument, "");
			array_push($arrdocument, "Stock sales between ".fnconvertunixtime($fromdate)." and ".fnconvertunixtime($todate));
			array_push($arrdocument, "");
			
			$arrline = array();
			$arrline[] = "Stock Code";
			$arrline[] = "Stock Name";
			$arrline[] = "Description";
			$arrline[] = "Total Orders";
			$arrline[] = "Total Quantity";
			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			$strdbsql = "SELECT stock.*, stock_group_information.name, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost, stock_status.description AS statusDesc FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_group_information.statusID > 2 AND productType = 0 ORDER BY stockCode ASC";
			$arrType = "multi";
			$strdbparams = array();
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

			foreach($resultdata AS $row)
			{
				$orderDetailsQuery = "SELECT sum(order_items.quantity) AS qty, count(order_items.recordID) AS count FROM order_items INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID WHERE order_header.timestampOrder >= :fromdate AND order_header.timestampOrder <= :todate AND order_header.orderStatus != 3 AND order_items.stockID = :stockID GROUP BY order_items.stockID";
				$strType = "single";
				$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate,"stockID"=>$row['recordID']);
				$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
				
				
				$arrline = array();
				$arrline[] = str_replace(",","", $row['stockCode']);
				$arrline[] = str_replace(",","", $row['name']);
				$arrline[] = str_replace(",","", $row['description']);
				$arrline[] = str_replace(",","", $ordersDetails['count']);
				$arrline[] = str_replace(",","", $ordersDetails['qty']);
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
			}
			
			array_push($arrdocument, "");
			array_push($arrdocument, "Hampers");
			array_push($arrdocument, "");
			$arrline = array();
			$arrline[] = "Hamper Name";
			$arrline[] = "Description";
			$arrline[] = "Stock Code";
			$arrline[] = "Stock Name";
			$arrline[] = "Total Orders";
			$arrline[] = "Total Quantity";
			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			$strdbsql = "SELECT stock_group_information.*, stock_status.description AS statusDesc, stock.recordID AS stockID FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_group_information.statusID > 2 AND productType = 1 ORDER BY name ASC";
			$arrType = "multi";
			$strdbparams = array();
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
			
			foreach($resultdata AS $row)
			{
				
				$orderDetailsQuery = "SELECT sum(order_items.quantity) AS qty, count(order_items.recordID) AS count FROM order_items INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID WHERE order_header.timestampOrder >= :fromdate AND order_header.timestampOrder <= :todate AND order_header.orderStatus != 3 AND order_items.stockID = :stockID GROUP BY order_items.stockID";
				$strType = "single";
				$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate,"stockID"=>$row['stockID']);
				$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
				
				$arrline = array();
				$arrline[] = str_replace(",","", $row['name']);
				$arrline[] = str_replace(",","", $row['description']);
				$arrline[] = "";
				$arrline[] = "";
				$arrline[] = str_replace(",","", $ordersDetails['count']);
				$arrline[] = str_replace(",","", $ordersDetails['qty']);

				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
			
				$strdbsql = "SELECT stock_group_information.name, stock_group_information.description, stock.*, stock_status.description AS statusDesc FROM stock_hamper INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_hamper.stockGroupID = :hamperID ORDER BY name ASC";
				$arrType = "multi";
				$strdbparams = array("hamperID"=>$row['recordID']);
				$resultdata2 = query($conn, $strdbsql, $arrType, $strdbparams);
				
				foreach($resultdata2 AS $row2){
					$arrline = array();
					$arrline[] = ""; //hamper name
					$arrline[] = ""; //hamper description
					$arrline[] = str_replace(",","", $row2['stockCode']);
					$arrline[] = str_replace(",","", $row2['name']);
					$arrline[] = "";
					$arrline[] = "";
					$strthisline = implode($strdelimiterField, $arrline);
					array_push($arrdocument, $strthisline);
				}
			}
		
			break;
	
	
		case "Stock Listing":
			
			array_push($arrdocument, "");
			array_push($arrdocument, "Active Stock Listing");
			array_push($arrdocument, "");
			
			$arrline = array();
			$arrline[] = "Stock Code";
			$arrline[] = "Stock Name";
			$arrline[] = "Description";
			$arrline[] = "Product Status";
			$arrline[] = "Weight";
			$arrline[] = "Price";
			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			$strdbsql = "SELECT stock.*, stock_group_information.name, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost, stock_status.description AS statusDesc FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_group_information.statusID > 2 AND productType = 0 ORDER BY stockCode ASC";
			$arrType = "multi";
			$strdbparams = array();
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

			foreach($resultdata AS $row)
			{
				$arrline = array();
				$arrline[] = str_replace(",","", $row['stockCode']);
				$arrline[] = str_replace(",","", $row['name']);
				$arrline[] = str_replace(",","", $row['description']);
				$arrline[] = str_replace(",","", $row['statusDesc']);
				$arrline[] = str_replace(",","", $row['weight']);
				$arrline[] = str_replace(",","", number_format($row['price'],2));
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
			}
			
			array_push($arrdocument, "");
			array_push($arrdocument, "Hampers");
			array_push($arrdocument, "");
			$arrline = array();
			$arrline[] = "Hamper Name";
			$arrline[] = "Description";
			$arrline[] = "Stock Code";
			$arrline[] = "Stock Name";
			$arrline[] = "Product Status";
			$arrline[] = "Weight";
			$arrline[] = "Price";
			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			$strdbsql = "SELECT stock_group_information.*, stock_status.description AS statusDesc FROM stock_group_information INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_group_information.statusID > 2 AND productType = 1 ORDER BY name ASC";
			$arrType = "multi";
			$strdbparams = array();
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
			
			foreach($resultdata AS $row)
			{
				$arrline = array();
				$arrline[] = str_replace(",","", $row['name']);
				$arrline[] = str_replace(",","", $row['description']);
				$arrline[] = "";
				$arrline[] = "";
				$arrline[] = str_replace(",","", $row['statusDesc']);
				$arrline[] = "";
				if ($row['productCost'] > 0) {
					$arrline[] = str_replace(",","", number_format($row['productCost'],2));
				} else {
					$arrline[] = "";
				}
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
			
				$strdbsql = "SELECT stock_group_information.name, stock_group_information.description, stock.*, stock_status.description AS statusDesc FROM stock_hamper INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_hamper.stockGroupID = :hamperID ORDER BY name ASC";
				$arrType = "multi";
				$strdbparams = array("hamperID"=>$row['recordID']);
				$resultdata2 = query($conn, $strdbsql, $arrType, $strdbparams);
				
				foreach($resultdata2 AS $row2){
					$arrline = array();
					$arrline[] = ""; //hamper name
					$arrline[] = ""; //hamper description
					$arrline[] = str_replace(",","", $row2['stockCode']);
					$arrline[] = str_replace(",","", $row2['name']);
					$arrline[] = str_replace(",","", $row2['statusDesc']);
					$arrline[] = str_replace(",","", $row2['weight']);
					$arrline[] = str_replace(",","", number_format($row2['price'],2));
					$strthisline = implode($strdelimiterField, $arrline);
					array_push($arrdocument, $strthisline);
				}
			}
			
		break;
		case "Payments":
		
			array_push($arrdocument, "");
			array_push($arrdocument, "Payments Between ".fnconvertunixtime($fromdate)." and ".fnconvertunixtime($todate));
			array_push($arrdocument, "");
			
			$arrline = array();
			$arrline[] = "Order Number";
			$arrline[] = "Payment Date";
			$arrline[] = "Payment Amount";
			$arrline[] = "Order Date";
			$arrline[] = "Order Sub-Total";
			$arrline[] = "Order VAT";
			$arrline[] = "Order Total";
			//$arrline[] = "Total Amount Paid";
			$arrline[] = "Order Status";
			$arrline[] = "Delivery Details";
			$arrline[] = "Billing Details";

			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			$orderDetailsQuery = "SELECT order_payment_details.*, order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_status.description AS orderStatus,order_dispatch.description AS dispatchDesc FROM order_payment_details INNER JOIN order_header ON order_payment_details.orderHeaderID = order_header.recordID INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID LEFT JOIN order_dispatch ON order_header.dispatch = order_dispatch.recordID WHERE order_payment_details.transactionTimestamp >= :fromdate AND order_payment_details.transactionTimestamp <= :todate AND order_header.orderStatus != 3 ORDER BY transactionTimestamp";
			$strType = "multi";
			$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate);
			$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);

			foreach($ordersDetails AS $orderDetails)
			{
				//reset values
				$subTotalAmount = 0;
				$vatAmount = 0;
				$totalAmount = 0;
					
				//get item details
				$strdbsql = "SELECT order_items.*, site_vat.amount, order_dispatch.description FROM order_items INNER JOIN site_vat ON order_items.vatID = site_vat.recordID INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID WHERE orderHeaderID = :recordID";
				$strType = "multi";
				$arrdbparam = array("recordID"=>$orderDetails['recordID']);
				$orderItems = query($conn, $strdbsql, $strType, $arrdbparam);
				
				foreach($orderItems AS $orderItem){
					$lineAmount = number_format($orderItem['price']*$orderItem['quantity'],2);
					if ($orderItem['amount'] == 0) {
						$lineTax = "0.00";
					} else {
						$lineTax = number_format(($lineAmount / 100) * $orderItem['amount'],2);
					}
					$lineTotal = number_format($lineAmount + $lineTax,2);
					$subTotalAmount += $lineAmount;
					$vatAmount += $lineTax;
					$totalAmount += $lineTotal;
				}
				
				$deliveryDetails = $orderDetails['addDeliveryTitle']." ".$orderDetails['addDeliveryFirstname']." ".$orderDetails['addDeliverySurname']." - ".$orderDetails['addDelivery1']." ".$orderDetails['addDeliveryPostcode'];
				$billingDetails = $orderDetails['addBillingTitle']." ".$orderDetails['addBillingFirstname']." ".$orderDetails['addBillingSurname']." - ".$orderDetails['addBilling1']." ".$orderDetails['addBillingPostcode'];
				
				$arrline = array();
				$arrline[] = str_replace(",","", $orderDetails['recordID']);
				$arrline[] = str_replace(",","", fnconvertunixtime($orderDetails['transactionTimestamp']));
				$arrline[] = str_replace(",","", number_format($orderDetails['transactionAmount'],2));
				$arrline[] = str_replace(",","", fnconvertunixtime($orderDetails['timestampOrder']));
				$arrline[] = str_replace(",","", number_format($subTotalAmount,2));
				$arrline[] = str_replace(",","", number_format($vatAmount,2));
				$arrline[] = str_replace(",","", number_format($totalAmount,2));
				//$arrline[] = str_replace(",","", number_format($orderDetails['paymentAmount'],2));
				$arrline[] = str_replace(",","", $orderDetails['orderStatus']);
				$arrline[] = str_replace(",","", $deliveryDetails);
				$arrline[] = str_replace(",","", $billingDetails);
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
			}
		break;
		case "Order Listing":
		
			array_push($arrdocument, "");
			array_push($arrdocument, "Order Listing Between ".fnconvertunixtime($fromdate)." and ".fnconvertunixtime($todate));
			array_push($arrdocument, "");
			
			$arrline = array();
			$arrline[] = "Order Number";
			$arrline[] = "Order Date";
			$arrline[] = "Sub-Total";
			$arrline[] = "VAT";
			$arrline[] = "Total";
			//$arrline[] = "Amount Paid";
			$arrline[] = "Order Status";
			$arrline[] = "Delivery Details";
			$arrline[] = "Billing Details";

			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			$orderDetailsQuery = "SELECT order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_status.description AS orderStatus,order_dispatch.description AS dispatchDesc FROM order_header INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID LEFT JOIN order_dispatch ON order_header.dispatch = order_dispatch.recordID WHERE timestampOrder >= :fromdate AND timestampOrder <= :todate AND order_header.orderStatus != 3 ORDER BY timestampOrder";
			$strType = "multi";
			$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate);
			$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);

			foreach($ordersDetails AS $orderDetails)
			{
				//reset values
				$subTotalAmount = 0;
				$vatAmount = 0;
				$totalAmount = 0;
					
				//get item details
				$strdbsql = "SELECT order_items.*, site_vat.amount, order_dispatch.description FROM order_items INNER JOIN site_vat ON order_items.vatID = site_vat.recordID INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID WHERE orderHeaderID = :recordID";
				$strType = "multi";
				$arrdbparam = array("recordID"=>$orderDetails['recordID']);
				$orderItems = query($conn, $strdbsql, $strType, $arrdbparam);
				
				foreach($orderItems AS $orderItem){
					$lineAmount = number_format($orderItem['price']*$orderItem['quantity'],2);
					if ($orderItem['amount'] == 0) {
						$lineTax = "0.00";
					} else {
						$lineTax = number_format(($lineAmount / 100) * $orderItem['amount'],2);
					}
					$lineTotal = number_format($lineAmount + $lineTax,2);
					$subTotalAmount += $lineAmount;
					$vatAmount += $lineTax;
					$totalAmount += $lineTotal;
				}
				
				$deliveryDetails = $orderDetails['addDeliveryTitle']." ".$orderDetails['addDeliveryFirstname']." ".$orderDetails['addDeliverySurname']." - ".$orderDetails['addDelivery1']." ".$orderDetails['addDeliveryPostcode'];
				$billingDetails = $orderDetails['addBillingTitle']." ".$orderDetails['addBillingFirstname']." ".$orderDetails['addBillingSurname']." - ".$orderDetails['addBilling1']." ".$orderDetails['addBillingPostcode'];
				
				$arrline = array();
				$arrline[] = str_replace(",","", $orderDetails['recordID']);
				$arrline[] = str_replace(",","", fnconvertunixtime($orderDetails['timestampOrder']));
				$arrline[] = str_replace(",","", number_format($subTotalAmount,2));
				$arrline[] = str_replace(",","", number_format($vatAmount,2));
				$arrline[] = str_replace(",","", number_format($totalAmount,2));
				//$arrline[] = str_replace(",","", number_format($orderDetails['paymentAmount'],2));
				$arrline[] = str_replace(",","", $orderDetails['orderStatus']);
				$arrline[] = str_replace(",","", $deliveryDetails);
				$arrline[] = str_replace(",","", $billingDetails);
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
			}
		break;
		case "Order Listing with Items":
		
			array_push($arrdocument, "");
			array_push($arrdocument, "Order Listing Between ".fnconvertunixtime($fromdate)." and ".fnconvertunixtime($todate));
			array_push($arrdocument, "");
			
			$orderDetailsQuery = "SELECT order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_status.description AS orderStatus,order_dispatch.description AS dispatchDesc FROM order_header INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID LEFT JOIN order_dispatch ON order_header.dispatch = order_dispatch.recordID WHERE dispatchDate >= :fromdate AND dispatchDate <= :todate AND order_header.orderStatus != 3 ORDER BY dispatchDate";
			$strType = "multi";
			$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate);
			$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);

			foreach($ordersDetails AS $orderDetails)
			{
				//reset values
				$subTotalAmount = 0;
				$vatAmount = 0;
				$totalAmount = 0;
					
				//get item details
				$strdbsql = "SELECT order_items.*, site_vat.amount, order_dispatch.description FROM order_items INNER JOIN site_vat ON order_items.vatID = site_vat.recordID INNER JOIN order_dispatch ON order_items.dispatch = order_dispatch.recordID WHERE orderHeaderID = :recordID AND display = 1";
				$strType = "multi";
				$arrdbparam = array("recordID"=>$orderDetails['recordID']);
				$orderItems = query($conn, $strdbsql, $strType, $arrdbparam);
				
				foreach($orderItems AS $orderItem){
					$lineAmount = number_format($orderItem['price']*$orderItem['quantity'],2);
					if ($orderItem['amount'] == 0) {
						$lineTax = "0.00";
					} else {
						$lineTax = number_format(($lineAmount / 100) * $orderItem['amount'],2);
					}
					$lineTotal = number_format($lineAmount + $lineTax,2);
					$subTotalAmount += $lineAmount;
					$vatAmount += $lineTax;
					$totalAmount += $lineTotal;
				}
				
				$deliveryDetails = $orderDetails['addDeliveryTitle']." ".$orderDetails['addDeliveryFirstname']." ".$orderDetails['addDeliverySurname']." - ".$orderDetails['addDelivery1']." ".$orderDetails['addDeliveryPostcode'];
				$billingDetails = $orderDetails['addBillingTitle']." ".$orderDetails['addBillingFirstname']." ".$orderDetails['addBillingSurname']." - ".$orderDetails['addBilling1']." ".$orderDetails['addBillingPostcode'];
				
				array_push($arrdocument,"Order Number:,".str_replace(",","", $orderDetails['recordID']));
				array_push($arrdocument,"Order Date:,".str_replace(",","", fnconvertunixtime($orderDetails['timestampOrder'])));
				array_push($arrdocument,"Dispatch Date:,".str_replace(",","", fnconvertunixtime($orderDetails['dispatchDate'])));
				array_push($arrdocument,"Sub-Total:,".str_replace(",","", number_format($subTotalAmount,2)));
				array_push($arrdocument,"VAT:,".str_replace(",","", number_format($vatAmount,2)));
				array_push($arrdocument,"Total:,".str_replace(",","", number_format($totalAmount,2)));
				//array_push($arrdocument,"Amount Paid:,".str_replace(",","", number_format($orderDetails['paymentAmount'],2)));
				array_push($arrdocument,"Order Status:,".str_replace(",","", $orderDetails['orderStatus']));
				array_push($arrdocument,"Delivery Details:,".str_replace(",","", $deliveryDetails));
				array_push($arrdocument,"Billing Details:,".str_replace(",","", $billingDetails));
				
				array_push($arrdocument, "");
				$arrline = array();
				$arrline[] = "Stock Code";
				$arrline[] = "Name";
				$arrline[] = "Qty";
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
				/*
				if (!empty($pickListInfo['recordID'])) {
					$orderDetailsQuery = "SELECT order_items.*, delivery.deliveryDate, delivery.recordID AS deliveryID FROM order_items INNER JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID INNER JOIN delivery ON deliveryOrders.deliveryItemID = delivery.recordID WHERE delivery.deliveryDate >= :fromdate AND delivery.deliveryDate < :todate AND delivery.pickList = :pickList ORDER BY delivery.deliveryDate, order_items.stockDetailsArray";
					$strType = "multi";
					$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate, "pickList"=>$pickListInfo['description'].$pickListInfo['pickdate']);
					$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
				} else {
					$orderDetailsQuery = "SELECT order_items.*, delivery.deliveryDate, delivery.recordID AS deliveryID FROM order_items INNER JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID INNER JOIN delivery ON deliveryOrders.deliveryItemID = delivery.recordID WHERE delivery.deliveryDate >= :fromdate AND delivery.deliveryDate < :todate AND delivery.pickList = '' ORDER BY delivery.deliveryDate, order_items.stockDetailsArray";
					$strType = "multi";
					$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate);
					$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
				}*/

				foreach($orderItems AS $orderItem)
				{
					//this detail is a hamper so we need to ignore this one and get the actual products from the hamper
					if ($orderItem['hamperOrderID'] != 0) {
						$orderDetailsQuery = "SELECT order_items.* FROM order_items WHERE hamperOrderID = :hamperOrderID AND display = 0";
						$strType = "multi";
						$arrdbparams = array("hamperOrderID"=>$orderItem['hamperOrderID']);
						$hamperDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
						foreach($hamperDetails AS $hamperDetail){
							$stockDetails = unserialize($hamperDetail['stockDetailsArray']);
							$arrline = array();
							$arrline[] = str_replace(",","", $stockDetails['stockCode']);
							$arrline[] = str_replace(",","", $stockDetails['name']);
							$arrline[] = str_replace(",","", $hamperDetail['quantity']);
							$strthisline = implode($strdelimiterField, $arrline);
							array_push($arrdocument, $strthisline);
						}
					} else {
						$stockDetails = unserialize($orderItem['stockDetailsArray']);
						$arrline = array();
						$arrline[] = str_replace(",","", $stockDetails['stockCode']);
						$arrline[] = str_replace(",","", $stockDetails['name']);
						$arrline[] = str_replace(",","", $orderItem['quantity']);
						$strthisline = implode($strdelimiterField, $arrline);
						array_push($arrdocument, $strthisline);
					}
				}
				array_push($arrdocument, "");
				array_push($arrdocument, "");
				
			}
		break;
		case "Order Pick List":
			
			$pickList = $_REQUEST['frm_pickList'];
			if (!empty($pickList)) {
				$strdbsql = "SELECT * FROM deliveryPickList WHERE recordID = :recordID";
				$strType = "single";
				$arrdbparams = array("recordID"=>$pickList);
				$pickListInfo = query($conn, $strdbsql, $strType, $arrdbparams);
			}
			
			array_push($arrdocument, "");
			array_push($arrdocument, "Order Pick List - ".fnconvertunixtime($fromdate)." to ".fnconvertunixtime($todate));
			array_push($arrdocument, "Printed: ".fnconvertunixtime($datnow, "h:i d/m/Y"));
			if (!empty($pickListInfo['recordID'])) array_push($arrdocument, "Pick List: ".date("d/m/Y", $pickListInfo['pickdate'])." - ".$pickListInfo['description']);
			array_push($arrdocument, "");
			
			$arrline = array();
			//$arrline[] = "Order Number";
			//$arrline[] = "Delivery Number";
			$arrline[] = "Dispatch Date";
			$arrline[] = "Stock Code";
			$arrline[] = "Name";
			$arrline[] = "Qty";
			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			if (!empty($pickListInfo['recordID'])) {
				$orderDetailsQuery = "SELECT order_items.*, delivery.deliveryDate, delivery.recordID AS deliveryID FROM order_items INNER JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID INNER JOIN delivery ON deliveryOrders.deliveryItemID = delivery.recordID INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID WHERE delivery.deliveryDate >= :fromdate AND delivery.deliveryDate < :todate AND delivery.pickList = :pickList AND order_header.orderStatus != 3 AND order_items.display = 1 ORDER BY delivery.deliveryDate, order_items.stockDetailsArray";
				$strType = "multi";
				$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate, "pickList"=>$pickListInfo['description'].$pickListInfo['pickdate']);
				$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
			} else {
				$orderDetailsQuery = "SELECT order_items.*, delivery.deliveryDate, delivery.recordID AS deliveryID FROM order_items INNER JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID INNER JOIN delivery ON deliveryOrders.deliveryItemID = delivery.recordID INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID WHERE delivery.deliveryDate >= :fromdate AND delivery.deliveryDate < :todate AND delivery.pickList = '' AND order_header.orderStatus != 3 AND order_items.display = 1 ORDER BY delivery.deliveryDate, order_items.stockDetailsArray";
				$strType = "multi";
				$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate);
				$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
			}

			$stockArray = array();
			foreach($ordersDetails AS $orderDetails)
			{
				//this detail is a hamper so we need to ignore this one and get the actual products from the hamper
				if ($orderDetails['hamperOrderID'] != 0) {
				
					$orderDetailsQuery = "SELECT order_items.* FROM order_items WHERE hamperOrderID = :hamperOrderID AND display = 0";
					$strType = "multi";
					$arrdbparams = array("hamperOrderID"=>$orderDetails['hamperOrderID']);
					$hamperDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
				
					foreach($hamperDetails AS $hamperDetail){
						
						$stockDetails = unserialize($hamperDetail['stockDetailsArray']);
						$stockArray[str_replace(",","", fnconvertunixtime($orderDetails['deliveryDate']))][str_replace(",","", $stockDetails['stockCode'])]['name'] = str_replace(",","", $stockDetails['name']);
						$stockArray[str_replace(",","", fnconvertunixtime($orderDetails['deliveryDate']))][str_replace(",","", $stockDetails['stockCode'])]['quantity'] += $hamperDetail['quantity'];
						
						
						/*$arrline = array();
						//$arrline[] = str_replace(",","", $orderDetails['orderHeaderID']);
						//$arrline[] = str_replace(",","", $orderDetails['deliveryID']);
						$arrline[] = str_replace(",","", fnconvertunixtime($orderDetails['deliveryDate']));
						$arrline[] = str_replace(",","", $stockDetails['stockCode']);
						$arrline[] = str_replace(",","", $stockDetails['name']);
						$arrline[] = str_replace(",","", $hamperDetail['quantity']);
						$strthisline = implode($strdelimiterField, $arrline);
						array_push($arrdocument, $strthisline);*/
					}
				
				} else {
					
					$stockDetails = unserialize($orderDetails['stockDetailsArray']);
					$stockArray[str_replace(",","", fnconvertunixtime($orderDetails['deliveryDate']))][str_replace(",","", $stockDetails['stockCode'])]['name'] = str_replace(",","", $stockDetails['name']);
					$stockArray[str_replace(",","", fnconvertunixtime($orderDetails['deliveryDate']))][str_replace(",","", $stockDetails['stockCode'])]['quantity'] += $orderDetails['quantity'];
					
					
					/*$arrline = array();
					//$arrline[] = str_replace(",","", $orderDetails['orderHeaderID']);
					//$arrline[] = str_replace(",","", $orderDetails['deliveryID']);
					$arrline[] = str_replace(",","", fnconvertunixtime($orderDetails['deliveryDate']));
					$arrline[] = str_replace(",","", $stockDetails['stockCode']);
					$arrline[] = str_replace(",","", $stockDetails['name']);
					$arrline[] = str_replace(",","", $orderDetails['quantity']);
					$strthisline = implode($strdelimiterField, $arrline);
					array_push($arrdocument, $strthisline);*/
					
						
				}
			}
			
			foreach($stockArray AS $deliveryDate => $stockDetails) {
				
				foreach ($stockDetails AS $stockCode => $details) {
					
					$arrline = array();
					$arrline[] = str_replace(",","", $deliveryDate);
					$arrline[] = str_replace(",","", $stockCode);
					$arrline[] = str_replace(",","", $details['name']);
					$arrline[] = str_replace(",","", $details['quantity']);
					$strthisline = implode($strdelimiterField, $arrline);
					array_push($arrdocument, $strthisline);
					
				}
			}
		break;
		case "Fedex Label Print":
		
			$query = "SELECT batchNo FROM deliveryBatch ORDER BY recordID DESC LIMIT 1";
			$strType = "single";
			$arrdbparams = array();
			$deliveryBatch = query($conn, $query, $strType, $arrdbparams);
			
			if (!empty($deliveryBatch['batchNo'])){
				
				if ($deliveryBatch['batchNo'] < 999) {
					$batchNo = $deliveryBatch['batchNo'] + 1;
				} else {
					$batchNo = 1;
				}
				
			} else {
				$batchNo = 1;
			}

			$orderDate = $datnow;
			//$orderDetailsQuery = "SELECT order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_status.description AS orderStatus,order_dispatch.description AS dispatchDesc FROM order_header INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID INNER JOIN order_dispatch ON order_header.dispatch = order_dispatch.recordID WHERE timestampOrder >= :fromdate AND timestampOrder <= :todate";
			//$orderDetailsQuery = "SELECT delivery.*, order_header.customerID, order_header.addDeliveryTitle, order_header.addDeliveryFirstname, order_header.addDeliverySurname, order_header.addDelivery1, order_header.addDelivery2, order_header.addDelivery3, order_header.addDelivery4, order_header.addDelivery5, order_header.addDeliveryPostcode, order_header.email, order_header.telephone, order_header.notes AS orderNotes, order_header.giftMessage, order_status.description AS orderStatus FROM delivery INNER JOIN order_header ON delivery.orderHeaderID = order_header.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID WHERE deliveryDate >= :fromdate AND deliveryDate <= :todate AND deliveryBatchID IS NULL AND order_header.orderStatus != 3 ";
			$orderDetailsQuery = "SELECT delivery.*, order_header.customerID, order_header.addDeliveryTitle, order_header.addDeliveryFirstname, order_header.addDeliverySurname, order_header.addDelivery1, order_header.addDelivery2, order_header.addDelivery3, order_header.addDelivery4, order_header.addDelivery5, order_header.addDeliveryPostcode, order_header.email, order_header.telephone, order_header.notes AS orderNotes, order_header.giftMessage, order_status.description AS orderStatus FROM delivery INNER JOIN order_header ON delivery.orderHeaderID = order_header.recordID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID WHERE deliveryDate >= :fromdate AND deliveryDate < :todate AND order_header.orderStatus != 3 ";
			$strType = "multi";
			$arrdbparams = array("fromdate"=>$fromdate,"todate"=>$todate);
			$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparams);
			
			if (count($ordersDetails) > 0) {
				
				$query = "INSERT INTO deliveryBatch (service, batchNo, timestamp) VALUES ('Fedex',:batchNo,UNIX_TIMESTAMP())";
				$strType = "insert";
				$arrdbparams = array("batchNo"=>$batchNo);
				$deliveryBatch = query($conn, $query, $strType, $arrdbparams);

				//first line = 0000,000,00/00/0000, (No. of records, file batch?, date)
				$arrline = array();
				$arrline[] = sprintf("%04d", count($ordersDetails));
				$arrline[] = sprintf("%03d", $batchNo);
				$arrline[] = fnconvertunixtime($orderDate);
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);

				$i = 1;
				foreach($ordersDetails AS $orderDetails)
				{
					$arrline = array();
					$arrline[] = ""; //contract no. - always empty
					$arrline[] = $orderDetails['customerID']; //customer / account code - 5 digits leading 0's
					$arrline[] = $orderDetails['addDeliverySurname']." ".$orderDetails['addDeliveryTitle']." ".$orderDetails['addDeliveryFirstname']; //customer name (surname, title, firstname then customer account code in brackets no leading 0's)
					$arrline[] = $orderDetails['addDelivery1']; //add 1
					$arrline[] = $orderDetails['addDelivery2']; //add 2
					$arrline[] = $orderDetails['addDelivery3']; //add 3
					$arrline[] = ""; //location - blank
					$arrline[] = $orderDetails['addDelivery4']; //town
					$arrline[] = $orderDetails['addDelivery5']; //county
					$arrline[] = $orderDetails['addDeliveryPostcode']; //post code
					$arrline[] = $orderDetails['email']; //email
					$arrline[] = $orderDetails['telephone']; //telephone - first from this list that isnt blank: home, work, fax
					$arrline[] = $orderDetails['addDeliverySurname']." ".$orderDetails['addDeliveryTitle']." ".$orderDetails['addDeliveryFirstname']; //contact - customer name (surname title firstname then customer account code in brackets no leading 0's)
					$arrline[] = $orderDetails['serviceCode']; //service code - A for next day / B for Scotland
					$arrline[] = fnconvertunixtime($orderDetails['deliveryDate']); //date
					$arrline[] = "001"; //no of items - (000) - always set to 1 on examples, possibly delivery boxes rather than order items
					$arrline[] = sprintf("%03d", ceil($orderDetails['email'] / 1000)); //weight - Kg rounded up (001 - 005) need to calculate from grams of items + packaging
					$arrline[] = "1".sprintf("%03d", $batchNo).sprintf("%03d", $i)."/".substr($orderDetails['addDeliverySurname']."_".$orderDetails['addDeliveryTitle'],0,8); //dispatch reference - 1###001/Surname_Title (replace ### with the file batch and increment the last 3 digits - trim last section to 8 characters)
					$deliveryNotesArray = str_split($orderDetails['notes'], 32);
					$arrline[] = $deliveryNotesArray[0]; //delivery notes 1 (size 32)
					$arrline[] = $deliveryNotesArray[1]; //delivery notes 2 (size 32)
					$arrline[] = $deliveryNotesArray[2]; //delivery notes 3 (size 32)
					$arrline[] = "A"; //carrier code (size 1) - A
					$arrline[] = ""; //carrier desc (size 25) - blank
					$arrline[] = ""; //hazard flag (size 10) - blank
					$arrline[] = ""; //packing group (size 10) - blank
					$arrline[] = ""; //hazard class (size 10) - blank
					$arrline[] = "c".$orderDetails['customerID']."/o".$orderDetails['orderHeaderID']."/d".$orderDetails['recordID']."/r".$orderDetails['customerID']; //dispatch ref cCustomerID/oOrderID/dDeliveryID/rRecieverID (total size 32, replace with correct info, recievedID looks to be same as customerID)
					$arrline[] = "FUR003"; //ANC account - FUR003
					$arrline[] = ""; //goods description - blank (size 32)
					$arrline[] = ""; //add 4 - blank
					$arrline[] = ""; //user console - blank
					$orderNotesArray = str_split($orderDetails['orderNotes'], 32);
					$arrline[] = $orderNotesArray[0]; //customer notes 1 (size 32)
					$arrline[] = $orderNotesArray[1]; //customer notes 2 (size 32)
					$arrline[] = "Furness Fish"; //BText2 - Furness Fish
					$arrline[] = ""; //BText1 - blank
					$arrline[] = ""; //systemID - blank
					
					$query = "UPDATE delivery SET deliveryBatchID = :batchNo WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array("batchNo"=>$deliveryBatch,"recordID"=>$orderDetails['recordID']);
					$deliveryUpdate = query($conn, $query, $strType, $arrdbparams);
					
					$query = "UPDATE order_header SET orderStatus = 5 WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array("recordID"=>$orderDetails['orderHeaderID']);
					$orderUpdate = query($conn, $query, $strType, $arrdbparams);
					
					$i++;
					
					$strthisline = implode($strdelimiterField, $arrline);
					array_push($arrdocument, $strthisline);
				}
			}
			$strfilename == "ANC".sprintf("%03d", $batchNo).".TXT";
			
		break;
		case "Customers":
		
			array_push($arrdocument, "");
			array_push($arrdocument, "Customer Export");
			array_push($arrdocument, "Printed: ".fnconvertunixtime($datnow, "h:i d/m/Y"));
			array_push($arrdocument, "");
		
			
			$arrline = array();
			$arrline[] = "Customer ID";
			$arrline[] = "Title";
			$arrline[] = "Firstname";
			$arrline[] = "Surname";
			$arrline[] = "Telephone";
			$arrline[] = "Mobile";
			$arrline[] = "Email";
			$arrline[] = "Notes";
			$arrline[] = "Delivery Title";
			$arrline[] = "Delivery Firstname";
			$arrline[] = "Delivery Surname";
			$arrline[] = "Delivery Add1";
			$arrline[] = "Delivery Add2";
			$arrline[] = "Delivery Add3";
			$arrline[] = "Delivery Town";
			$arrline[] = "Delivery County";
			$arrline[] = "Delivery Country";
			$arrline[] = "Delivery Postcode";
			$arrline[] = "Delivery Notes";
			$arrline[] = "Billing Title";
			$arrline[] = "Billing Firstname";
			$arrline[] = "Billing Surname";
			$arrline[] = "Billing Add1";
			$arrline[] = "Billing Add2";
			$arrline[] = "Billing Add3";
			$arrline[] = "Billing Town";
			$arrline[] = "Billing County";
			$arrline[] = "Billing Country";
			$arrline[] = "Billing Postcode";
			$arrline[] = "Billing Notes";
			$arrline[] = "Last Order Date";
			$strthisline = implode($strdelimiterField, $arrline);
			array_push($arrdocument, $strthisline);
			
			
			$mysqlQuery = "SELECT customer.*, deliveryAdd.title AS deltitle,deliveryAdd.firstname AS delfirstname,deliveryAdd.surname AS delsurname,deliveryAdd.add1 AS deladd1,deliveryAdd.add2 AS deladd2,deliveryAdd.add3 AS deladd3,deliveryAdd.town AS deltown,deliveryAdd.county AS delcounty,deliveryAdd.country AS delcountry,deliveryAdd.postcode AS delpostcode,deliveryAdd.notes AS delnotes,billingAdd.title AS biltitle,billingAdd.firstname AS bilfirstname,billingAdd.surname AS bilsurname,billingAdd.add1 AS biladd1,billingAdd.add2 AS biladd2,billingAdd.add3 AS biladd3,billingAdd.town AS biltown,billingAdd.county AS bilcounty,billingAdd.country AS bilcountry,billingAdd.postcode AS bilpostcode,billingAdd.notes AS bilnotes 
			FROM customer 
			LEFT JOIN customer_address deliveryAdd ON customer.defaultDeliveryAdd = deliveryAdd.recordID 
			LEFT JOIN customer_address billingAdd ON customer.defaultBillingAdd = billingAdd.recordID";
			$strType = "multi";
			$arrdbparams = array();
			$resultdata = query($conn, $mysqlQuery, $strType, $arrdbparams);

			foreach($resultdata AS $row)
			{
				//get latest order date
				$mysqlQuery = "SELECT timestampOrder FROM order_header WHERE customerID = :customerID AND orderStatus != 3 ORDER BY timestampOrder DESC LIMIT 1";
				//$mysqlQuery = "SELECT timestampOrder FROM order_header WHERE (customerID = :customerID OR addDeliveryCustomerID = :customerID2) AND orderStatus != 3 ORDER BY timestampOrder DESC LIMIT 1";
				$strType = "single";
				$arrdbparams = array("customerID"=>$row['recordID']);
				$orderDetails = query($conn, $mysqlQuery, $strType, $arrdbparams);
				$lastOrderDate = $orderDetails['timestampOrder'];
				
				$mysqlQuery = "SELECT timestampOrder FROM order_header WHERE addDeliveryCustomerID = :customerID AND orderStatus != 3 ORDER BY timestampOrder DESC LIMIT 1";
				$strType = "single";
				$arrdbparams = array("customerID"=>$row['recordID']);
				$orderDetails = query($conn, $mysqlQuery, $strType, $arrdbparams);
				$lastGiftOrderDate = $orderDetails['timestampOrder'];
				
				if ($lastGiftOrderDate > $lastOrderDate) {
					$lastOrderDate = $lastGiftOrderDate;
				}
				
				$arrline = array();
				$arrline[] = str_replace(",","", $row['recordID']);
				$arrline[] = str_replace(",","", $row['title']); //Title";
				$arrline[] = str_replace(",","", $row['firstname']); //"Firstname";
				$arrline[] = str_replace(",","", $row['surname']); //"Surname";
				$arrline[] = str_replace(",","", $row['telephone']); //"Telephone";
				$arrline[] = str_replace(",","", $row['mobile']); //"Mobile";
				$arrline[] = str_replace(",","", $row['email']); //"Email";
				$arrline[] = str_replace(",","", $row['notes']); //"Notes";
				$arrline[] = str_replace(",","", $row['deltitle']); //"Delivery Title";
				$arrline[] = str_replace(",","", $row['delfirstname']); //"Delivery Firstname";
				$arrline[] = str_replace(",","", $row['delsurname']); //"Delivery Surname";
				$arrline[] = str_replace(",","", $row['deladd1']); //"Delivery Add1";
				$arrline[] = str_replace(",","", $row['deladd2']); //"Delivery Add2";
				$arrline[] = str_replace(",","", $row['deladd3']); //"Delivery Add3";
				$arrline[] = str_replace(",","", $row['deltown']); //"Delivery Town";
				$arrline[] = str_replace(",","", $row['delcounty']); //"Delivery County";
				$arrline[] = str_replace(",","", $row['delcountry']); //"Delivery Country";
				$arrline[] = str_replace(",","", $row['delpostcode']); //"Delivery Postcode";
				$arrline[] = str_replace(",","", $row['delnotes']); //"Delivery Notes";
				$arrline[] = str_replace(",","", $row['biltitle']); //"Billing Title";
				$arrline[] = str_replace(",","", $row['bilfirstname']); //"Billing Firstname";
				$arrline[] = str_replace(",","", $row['bilsurname']); //"Billing Surname";
				$arrline[] = str_replace(",","", $row['biladd1']); //"Billing Add1";
				$arrline[] = str_replace(",","", $row['biladd2']); //"Billing Add2";
				$arrline[] = str_replace(",","", $row['biladd3']); //"Billing Add3";
				$arrline[] = str_replace(",","", $row['biltown']); //"Billing Town";
				$arrline[] = str_replace(",","", $row['bilcounty']); //"Billing County";
				$arrline[] = str_replace(",","", $row['bilcountry']); //"Billing Country";
				$arrline[] = str_replace(",","", $row['bilpostcode']); //"Billing Postcode";
				$arrline[] = str_replace(",","", $row['bilnotes']); //"Billing Notes";
				$arrline[] = (!empty($lastOrderDate)) ? str_replace(",","", fnconvertunixtime($lastOrderDate)) : "";
				$strthisline = implode($strdelimiterField, $arrline);
				array_push($arrdocument, $strthisline);
			}

		break;
		default:
			print ("That format is not supported at this time");
			die;
			break;
	}


	//name the document
	if ($strfilename == "") {
		$strfilename = $strdocumentdescription." ".date("d-m-y h_i_s").$strfileformat;
	}

	if ($strwhocalledme != "config-exports.php" and $strwhocalledme != "export-history.php" and $strwhocalledme != "datadump.php") {

		if (count($arrdocument) < 300) {
			// store in the database
			$strdbsql = "INSERT INTO fileExport (exportFormatID, dataPassed, fileName, fileExtension, operator, timestamp) VALUES (:exportFormatID, :dataPassed, :fileName, :fileExtension, :operator, UNIX_TIMESTAMP())";
			$strType = "insert";
			$arrdbparams = array("exportFormatID"=>-1,"dataPassed"=>serialize($arrdocument),"fileName"=>$strfilename,"fileExtension"=>$strfileformat,"operator"=>$_SESSION['operatorID']);
			$resultdatainsert = query($conn,$strdbsql,$strType,$arrdbparams);
		} else {
			// write to an actual file on the server and a link in the database
			$stractualfile = "/var/www/html/files/exports/reports/".$strfilename;
			$f = fopen($stractualfile, 'w');
			foreach ($arrdocument as $row => $value) {
				fputs($f, $value.PHP_EOL);
			}
			fclose($f);
			// write link to database...
			print ("$stractualfile<br>");
			$strdbsql = "INSERT INTO fileExport (exportFormatID, dataPassed, fileName, fileExtension, operator, timestamp) VALUES (:exportFormatID, :dataPassed, :fileName, :fileExtension, :operator, UNIX_TIMESTAMP())";
			$strType = "insert";
			$arrdbparams = array("exportFormatID"=>-3,"dataPassed"=>"/files/exports/reports/".$strfilename,"fileName"=>$strfilename,"fileExtension"=>$strfileformat,"operator"=>$_SESSION['operatorID']);
			$resultdatainsert = query($conn,$strdbsql,$strType,$arrdbparams);
		}


		//check for a custom sql update
		if (strlen($strupdatesql) > 1 AND $strupdatesql != "SEE FILE /includes/inc_generateexportspecial.php") {
			// so there is a query to run....
			if (strlen($strmatchvalue) > 0) {
				$strupdatesql = str_replace($strmatchvariable, $strmatchvalue, $strupdatesql);
			} else {
				$strupdatesql = str_replace($strmatchvariable, $strlookupdata, $strupdatesql);
			}
			$strupdatesql = str_replace($strupdatevariable, $strupdatevalue, $strupdatesql);

			$strType = "update";
			$resultdataupdate = query($conn,$strupdatesql,$strType);
		}
	}

	if ($intcreatefile == 1) {
		fndataexportfixed($arrdocument,$strfilename,$streol);
		$strcmd = "filegenerated";
	}
	error_log("inc_generateexportspecial.php BOTTOM");
?>