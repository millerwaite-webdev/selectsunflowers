<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_newsimagemanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	foreach($_POST AS $imagesData)
	{
		//var_dump($imagesData);
		foreach($imagesData AS $imageData)
		{
			$strcmd = $imageData['cmd'];
			switch ($strcmd)
			{
				case "reorder":
					
					// Get total number of images
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM site_news_images WHERE newsID = :galleryID";
					$maxOrder = query($conn, $maxOrderQuery, "single", array("galleryID"=>$imageData['galleryID']));
					
					// Get the current order of the selected image
					$currOrderQuery = "SELECT imageOrder FROM site_news_images WHERE recordID = :recordID";
					$currOrder = query($conn, $currOrderQuery, "single", array("recordID"=>$imageData['recordID']));
					
					
					// If current image order is not the same as the destined order change
					if($currOrder['imageOrder'] != $imageData['galleryOrder'])
					{	
						// If moving order up (+1)
						if(($currOrder['imageOrder'] < $imageData['galleryOrder']))
						{
							// If destined order change isn't higher than total amount of images
							if($imageData['galleryOrder'] <= $maxOrder['max_order']) {
								
								// Swap with image with same order value
								$updateOrderQuery = "UPDATE site_news_images SET imageOrder = imageOrder - 1 WHERE imageOrder <= :galleryOrder AND imageOrder >= :prevOrder AND newsID = :galleryID";
								$updateOrder = query($conn, $updateOrderQuery, "update", array("galleryOrder"=>$imageData['galleryOrder'],"prevOrder"=>$currOrder['imageOrder'],"galleryID"=>$imageData['galleryID']));
								
								// Change order of selected image
								$updateOrdersQuery = "UPDATE site_news_images SET imageOrder = :newOrder WHERE recordID = :recordID";
								$updateOrders = query($conn, $updateOrdersQuery, "update", array("recordID"=>$imageData['recordID'],"newOrder"=>$imageData['galleryOrder']));
							}
						}
						else if($currOrder['imageOrder'] > $imageData['galleryOrder'])
						{
							// If destined order change isn't lower than 0
							if($imageData['galleryOrder'] > 0) {
								
								// Swap with image with same order value
								$updateOrderQuery = "UPDATE site_news_images SET imageOrder = imageOrder + 1 WHERE imageOrder >= :galleryOrder AND imageOrder <= :prevOrder AND newsID = :galleryID";
								$updateOrder = query($conn, $updateOrderQuery, "update", array("galleryOrder"=>$imageData['galleryOrder'],"prevOrder"=>$currOrder['imageOrder'],"galleryID"=>$imageData['galleryID']));
								
								// Change order of selected image
								$updateOrdersQuery = "UPDATE site_news_images SET imageOrder = :newOrder WHERE recordID = :recordID";
								$updateOrders = query($conn, $updateOrdersQuery, "update", array("recordID"=>$imageData['recordID'],"newOrder"=>$imageData['galleryOrder']));
							}
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
