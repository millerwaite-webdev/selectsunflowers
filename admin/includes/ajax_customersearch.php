<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_customersearch"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	if(isset($_REQUEST['q']))
	{
		$strselect = "%".$_REQUEST['q']."%";
		$strdbsql = "SELECT * FROM customer WHERE surname LIKE :search ORDER BY surname ASC";
		$arrType = "multi";
		$strdbparams = array("search" => $strselect);
		$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

		$strdestinationdiv = $_REQUEST['destdiv'];
		if (isset($_REQUEST['c'])) { $strcmd = $_REQUEST['c']; } else { $strcmd = ""; }

		$intCust = count($resultdata);

		if(count($resultdata) > 0 && ($_REQUEST['q'] != ""))
		{
			print "<table style='width: 100%;' id='list' class='tabTable'>";
			print "<tr class='listitem blueHeader'><th class='linehead'>Name</th><th class='linehead'>Address</th><th class='linehead'>Post Code</th><th style='border-right: none;'></th></tr>";
			foreach($resultdata AS $row)
			{
				print "<tr style='cursor:pointer' class='listitem' onclick=\"setitem('".$row['fld_counter']."','".$strdestinationdiv."','".$strcmd."')\"><td>".$row['fld_firstname']." ".$row['fld_surname']."</td><td>".$row['fld_add1']."</td><td>".$row['fld_postcode']."</td><td style='border-right: none;'><span class='short_button orange' style='margin: 0;'>Select</span></td></tr>";
			}
			print "</table>";
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
