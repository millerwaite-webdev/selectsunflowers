<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_stockimagemanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $imagesData)
	{
		//var_dump($imagesData);
		$i = 0;
		$intImages = count($imagesData);
		foreach($imagesData AS $imageData)
		{
			$strcmd = $imageData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_images WHERE stockID = :stockID AND imageTypeID = 1";
					$strType = "single";
					$arrdbparam = array( "stockID" => $imageData['stockID'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT imageOrder FROM stock_images WHERE stockID = :stockID AND imageLink = :imageLink";
						$strType = "single";
						$arrdbparam = array( "stockID" => $imageData['stockID'], "imageLink" => $imageData['imageLink'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['imageOrder'] != $imageData['order'])
						{
							if (($currOrder['imageOrder'] < $imageData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE stock_images SET imageOrder = imageOrder - 1 WHERE stockID = :stockID AND imageOrder <= :newOrder AND imageOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"stockID" => $imageData['stockID'],
												"prevOrder" => $currOrder['imageOrder'],
												"newOrder" => $imageData['order']
											);
								
								if ($imageData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['imageOrder'] > $imageData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE stock_images SET imageOrder = imageOrder + 1 WHERE stockID = :stockID AND imageOrder <= :prevOrder AND imageOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"stockID" => $imageData['stockID'],
												"prevOrder" => $currOrder['imageOrder'],
												"newOrder" => $imageData['order']
											);
								if ($imageData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($imageData['order'] > 0 && $imageData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE stock_images SET imageOrder = :order WHERE stockID = :stockID AND imageLink = :imageLink";
								$strType = "update";
								$arrdbparams = array(
												"order" => $imageData['order'],
												"stockID" => $imageData['stockID'],
												"imageLink" => $imageData['imageLink']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						$currOrder['imageOrder'] = $imageData['order'];
						
						if ($imageData['order'] > 0)
						{
							if ($imageData['order'] <= $maxOrder['max_order'])
							{
								//echo "add\n";
								$updateOrdersQuery = "UPDATE stock_images SET imageOrder = imageOrder + 1 WHERE stockID = :stockID AND imageOrder >= :order";
								$strType = "update";
								$arrdbparam = array(
												"stockID" => $imageData['stockID'],
												"order" => $imageData['order']
											);
								
								//echo "move down before addition\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$addOrderQuery = "INSERT INTO stock_images (stockID, imageOrder, imageLink, imageTypeID) VALUES (:stockID, :order, :imageLink, :imageTypeID)";
							$strType = "insert";
							$arrdbparams = array(
											"stockID" => $imageData['stockID'],
											"order" => $imageData['order'],
											"imageLink" => $imageData['imageLink'],
											"imageTypeID" => 1
										);
							
							//echo "addition\n";
							$addOrder = query($conn, $addOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['menuOrder']." ".$imageData['order']."\n";
					//echo $maxOrder['max_order']." ".$imageData['order']."\n";
					//echo "Order is: ".$imageData['order']."\n";
					
					break;
					
				case "delete":
					
					//var_dump($imageData);
					$removeOrderQuery = "DELETE FROM stock_images WHERE stockID = :stockID AND imageOrder = :order AND imageLink = :imageLink";
					$strType = "delete";
					$arrdbparams = array(
									"stockID" => $imageData['stockID'],
									"order" => $imageData['order'],
									"imageLink" => $imageData['imageLink']
								);
					//echo "deletion\n";
					$removeOrder = query($conn, $removeOrderQuery, $strType, $arrdbparams);
					
					//echo $deleteOrderQuery;
					//var_dump($deleteOrder);
					
					$i++;
					
					if ($i == $intImages)
					{
						
						$strdbsql = "SELECT recordID FROM stock_images WHERE stockID = :stockID ORDER BY imageOrder";
						$strType = "multi";
						$arrdbparams = array(
										"stockID" => $imageData["stockID"]
									);
						//echo "get products\n";
						$associatedImages = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedImages AS $associatedImage)
						{
							$strdbsql = "UPDATE stock_images SET imageOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedImage['recordID']
										);
							//echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
