<?php

	function deliveryLocationCheck($postcode, $country, $conn){
		
		$deliveryAllowed = true;
		$foundLocationRules = array();
		$deliveryLocationDetails = array();
		$deliveryLocationDetails['strerror'] = "";
		$deliveryLocationDetails['strwarning'] = "";
		$deliveryLocationDetails['strsuccess'] = "";
		
		
		//find zone from country
				
		//check postcode for wildcard
		$strdbsql = "SELECT * FROM deliveryLocations WHERE country = :country OR country = ''";
		$arrdbparams = array("country"=>$country);
		$strType = "multi";
		$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

		if (count($resultdata) > 0) {
			foreach ($resultdata as $row) {
				
				if (substr($row['postcode'], -1) == "*") {
					$starPosition = strpos($row['postcode'], "*");
					if (substr(strtolower(str_replace(" ","",$row['postcode'])), 0, $starPosition) == substr(strtolower(str_replace(" ","",$postcode)), 0, $starPosition)) {
						$foundLocationRules[] = $row;
						if ($row['allowDelivery'] == 0) {
							$deliveryAllowed = false;
						}
					}
				} else {
					if (strtolower(str_replace(" ","",$row['postcode'])) == strtolower(str_replace(" ","",$postcode))) {
						$foundLocationRules[] = $row;
						if ($row['allowDelivery'] == 0) {
							$deliveryAllowed = false;
						}
					}
				}								
			}
		}
		
		if (!$deliveryAllowed){
			$deliveryLocationDetails['strerror'] .= "Delivery is not allowed to this location, please review delivery details.<br/>";
		}
		
		return $deliveryLocationDetails;
	}

	function updateUserBillDelAddresses($values, $criteria, $conn){
		switch($criteria)
		{
			case "billanddel":
				$updateBillDellAddressesQuery = "UPDATE customer SET defaultDeliveryAdd = :deladd, defaultBillingAdd = :billadd WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"billadd" => $values['billingadd'],
					"deladd" => $values['deliveryadd'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
			case "bill":
				$updateBillDellAddressesQuery = "UPDATE customer SET defaultBillingAdd = :billadd WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"billadd" => $values['billingadd'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
			case "del":
				$updateBillDellAddressesQuery = "UPDATE customer SET defaultDeliveryAdd = :deladd WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"deladd" => $values['deliveryadd'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
			case "byid":
				$updateBillDellAddressesQuery = "UPDATE customer_address SET firstname = :firstname, surname = :surname, title = :title, add1 = :add1, add2 = :add2, add3 = :add3, town = :town, county = :county, country = :country, postcode = :postcode WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"firstname" => $values['firstname'],
					"surname" => $values['surname'],
					"title" => $values['title'],
					"add1" => $values['add1'],
					"add2" => $values['add2'],
					"add3" => $values['add3'],
					"town" => $values['town'],
					"county" => $values['county'],
					"country" => $values['country'],
					"postcode" => $values['postcode'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
		}
		
		return $updateUserBillDelAddresses;
	}
	function getUserAddresses($id, $view, $conn){
		switch($view)
		{
			case "all":
				$getCustomerAddressesQuery = "SELECT * FROM customer_address WHERE customerID = :custid";
				$returnType = "multi";
				$param = array( "custid" => $id );
				$customerAddresses = query($conn, $getCustomerAddressesQuery, $returnType, $param);
				break;
			case "byid":
				$getCustomerAddressesQuery = "SELECT * FROM customer_address WHERE recordID = :addid";
				$returnType = "single";
				$param = array( "addid" => $id );
				$customerAddresses = query($conn, $getCustomerAddressesQuery, $returnType, $param);
				break;
		}
		return $customerAddresses;
	}
	function deleteUserAddress($addressid, $custid, $conn){
		$deleteCustomerAddressQuery = "DELETE FROM customer_address WHERE recordID = :addressid AND customerID = :custid";
		$queryType = "delete";
		$params = array(
			"addressid" => $addressid,
			"custid" => $custid
		);
		
		return query($conn, $deleteCustomerAddressQuery, $queryType, $params);
	}
	function insertUserAddress($values, $conn){
		$insertUserAddressQuery = "INSERT INTO customer_address (customerID, title, firstname, surname, add1, add2, add3, town, county, postcode, country) VALUES (:custid, :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode, :country)";
		$queryType = "insert";
		$params = array(
			"title" => $values['title'],
			"firstname" => $values['firstname'],
			"surname" => $values['surname'],
			"add1" => $values['add1'],
			"add2" => $values['add2'],
			"add3" => $values['add3'],
			"town" => $values['town'],
			"county" => $values['county'],
			"postcode" => $values['postcode'],
			"country" => $values['country'],
			"custid" => $values['custid']
		);
		
		return query($conn, $insertUserAddressQuery, $queryType, $params);
	}
	function check_email_address($email){
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email))
		{
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++)
		{
			if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i]))
			{
				return false;
			}
		}
		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1]))
		{
			// Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2)
			{
				return false;
				// Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++)
			{
				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i]))
				{
					return false;
				}
			}
		}
		return true;
	}

	function pagePermission($currentpage,$conn) {

		$location = "";
		if ($currentpage == "") {
			$currentpage = curPageName(); //this is a function that is near the bottom of this page. It gets the pagename of the current page
		}

		$strdbsql = "SELECT * FROM admin_pages WHERE link=:pagename";
		$arrType = "single";
		$strdbparams = array("pagename" => $currentpage);
		$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
		

		//if the pagename cannot be found in the database then bounce back to the index or login pages
		if ($resultdata != null) {
			if (isset($resultdata['permissionGroup'])) $permGroup = $resultdata['permissionGroup']; else $permGroup = "";
			$pagePermissionGroup = permissionGroup($permGroup,$conn);
			$operatorPermissionGroup =
				array_key_exists('permissionGroup', $_SESSION)
					? permissionGroup($_SESSION['permissionGroup'],$conn)
					: NULL;

			//checks to see if the user has permission to view this page
			if ($pagePermissionGroup['groupID'] > $operatorPermissionGroup['groupID']) {
				if ($currentpage != 'index' AND $currentpage != 'login') {
					$location = ("Location: /index.php?error=You do not have permission to access this page");
				} else {
					$location = ("Location: /login.php");
				}
			}


		} else {
			if ($currentpage != 'index') {
				$location = ("Location: /index.php?error=1");
			} else {
				$location = ("Location: /login.php?error=1");
			}
		}
		return $location;
	}
	function permissionGroup($permissionGroupID,$conn) {
		$strdbsql = "SELECT * FROM admin_permission_groups WHERE groupID=:groupID";
		$arrType = "single";
		$strdbparams = array("groupID" => $permissionGroupID);
		$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
		return $resultdata;
	}
	function stripCharactersFilename($filename){
		$filename = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $filename);
		return $filename;
	}
	function reArrayFiles( $arr ){foreach( $arr as $key => $all ){foreach( $all as $i => $val ){$new[$i][$key] = $val;}} return $new; }
	function createCsv($xml,$f){foreach ($xml->children() as $item) { $hasChild = (count($item->children()) > 0)?true:false; if( ! $hasChild){$put_arr = array($item->getName(),$item); fputcsv($f, $put_arr ,',','"');} else {createCsv($item, $f);}}}
	function html_encode($var){
		return htmlentities($var, ENT_QUOTES, 'UTF-8') ;
	}
	function getMailMerge(&$wts, $index, $dataarray, $mergerecord){
		$loop = true;
		$startfield = false;
		$setval = false;
		$counter = $index;
		$newcount = 0;
		while( $loop )
		{
			//find any nodes that are named w:fldCharType
			if( $wts->item( $counter )->attributes->item(0)->nodeName == 'w:fldCharType' )
			{
				$nodeName = '';
				$nodeValue = '';

				//switch the found nodes value
				switch( $wts->item( $counter )->attributes->item(0)->nodeValue )
				{
					//the first value will be begin - this tells us what type of merge we are looking at
					case 'begin':
						if( $startfield )
						{
							$counter = getMailMerge( $wts, $counter, $dataarray, $mergerecord);
						}
						$startfield = true;
						//look for the next sibling of our parent node
						if( $wts->item( $counter )->parentNode->nextSibling )
						{
							$nodeName = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeName;
							$nodeValue = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeValue;
						} else {
							// No sibling so check next node
							$nodeName = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeName;
							$nodeValue = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeValue;
						}

						//once found the nodes will then have a value - we need to switch these and replace the values
						if( $nodeValue == 'date \@ "MMMM d, yyyy"' )
						{
							$setval = true;
							$newval = date( "F j, Y" );
						}
						if( substr( $nodeValue, 0, 11 ) == ' MERGEFIELD' )
						{
							$setval = true;
							$newval = $dataarray[strtolower(str_replace('"', '', trim(substr($nodeValue, 12 ))))];
							$array['data'] = strtolower(str_replace('"', '', trim(substr($nodeValue, 12 ))));
						}
						if( substr( $nodeValue, 0, 9 ) == ' MERGEREC' )
						{
							$setval = true;
							$newval = $mergerecord;
						}
						$counter++;

						//not sure why this is in here but it doesnt have a end after it so we need to ignore.
						if($nodeValue == ' IF ' )
						{
							if( $startfield )
							{
								$startfield = false;
							}
							$loop = false;
						}

					break;
					case 'separate':
						if( $wts->item( $counter )->parentNode->nextSibling )
						{
							$nodeName = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeName;
							$nodeValue = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeValue;
						} else {
							// No sibling
							// check next node
							$nodeName = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeName;
							$nodeValue = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeValue;
						}
						if( $setval )
						{
							$wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeValue = $newval;
							$setval = false;
							$newval = '';
						}
						$counter++;
					break;
					case 'end':
						if( $startfield )
						{
							$startfield = false;
						}
						$loop = false;
				}
			}
		}
		$array['counter'] = $counter;
		return $array;
	}
	function printLetter($letterID, $lookupData, $datnow, $conn){
		
		$return = array();
		$return['error'] = "";
		$return['warning'] = "";
		$return['success'] = "";

		//get document details
		$strdbsql = "SELECT * FROM letters WHERE groupID = :letterID ORDER BY version DESC LIMIT 1";
		$arrdbvalues = array("letterID" => $letterID);
		$strType = "single";
		$letterDetails = query($conn, $strdbsql, $strType, $arrdbvalues);
		
		$mergerecord = 1; //this will increment if more than one letter being produced
		$requiredData = unserialize($letterDetails['requiredData']);
		
		foreach ($requiredData['tables'] AS $table) {
			
			$strdbsql = "SELECT * FROM exportTables WHERE databaseTable = :table";
			$strType = "single";
			$arrdbvalues = array("table"=>$table);
			$exportData = query($conn, $strdbsql, $strType, $arrdbvalues);
			
			//loop around each record that we need to produce a letter for
			for($i = 0; $i < count($lookupData); $i++) {

				$datafields[$i][$table] = Array();
				$strsqltmp = "SHOW COLUMNS FROM ".$table;
				$cols = query($conn, $strsqltmp,"columns");
				foreach($cols AS $col) {
					$datafields[$i][$table][$col] = "";
				}
			}
		}


		//replace the merge fields with actual data
		for($i = 0; $i < count($lookupData); $i++) {
				
			//need to re-oreder address fields so that there are no gaps when making a letter, this below makes a new array if the value isnt blank and should be displayed and then rebuilds the fields to be displayed excluding any gaps
			$addressData = array();
			$addressCount = 0;
			if ($datafields[$i]['customerAddress'][0]['add1'] != "" AND in_array ('customer_address.add1', $requiredData['columns'])) $addressData[] = $datafields[$i]['customerAddress'][0]['add1'];
			if ($datafields[$i]['customerAddress'][0]['add2'] != "" AND in_array ('customer_address.add2', $requiredData['columns'])) $addressData[] = $datafields[$i]['customerAddress'][0]['add2'];
			if ($datafields[$i]['customerAddress'][0]['add3'] != "" AND in_array ('customer_address.add3', $requiredData['columns'])) $addressData[] = $datafields[$i]['customerAddress'][0]['add3'];
			if ($datafields[$i]['customerAddress'][0]['town'] != "" AND in_array ('customer_address.town', $requiredData['columns'])) $addressData[] = $datafields[$i]['customerAddress'][0]['town'];
			if ($datafields[$i]['customerAddress'][0]['county'] != "" AND in_array ('customer_address.county', $requiredData['columns'])) $addressData[] = $datafields[$i]['customerAddress'][0]['county'];
			if ($datafields[$i]['customerAddress'][0]['country'] != "" AND in_array ('customer_address.country', $requiredData['columns'])) $addressData[] = $datafields[$i]['customerAddress'][0]['country'];
			if ($datafields[$i]['customerAddress'][0]['postcode'] != "" AND in_array ('customer_address.postcode', $requiredData['columns'])) $addressData[] = $datafields[$i]['customerAddress'][0]['postcode'];
			
			if (in_array ('customer_address.add1', $requiredData['columns'])) {
				$datafields[$i]['customerAddress'][0]['add1'] = ((isset($addressData[$addressCount])) ? $addressData[$addressCount] : ""); 
				$addressCount++;
			}
			if (in_array ('customer_address.add2', $requiredData['columns'])) {
				$datafields[$i]['customerAddress'][0]['add2'] = ((isset($addressData[$addressCount])) ? $addressData[$addressCount] : ""); 
				$addressCount++;
			}
			if (in_array ('customer_address.add3', $requiredData['columns'])) {
				$datafields[$i]['customerAddress'][0]['add3'] = ((isset($addressData[$addressCount])) ? $addressData[$addressCount] : ""); 
				$addressCount++;
			}
			if (in_array ('customer_address.town', $requiredData['columns'])) {
				$datafields[$i]['customerAddress'][0]['town'] = ((isset($addressData[$addressCount])) ? $addressData[$addressCount] : ""); 
				$addressCount++;
			}
			if (in_array ('customer_address.county', $requiredData['columns'])) {
				$datafields[$i]['customerAddress'][0]['county'] = ((isset($addressData[$addressCount])) ? $addressData[$addressCount] : ""); 
				$addressCount++;
			}
			if (in_array ('customer_address.country', $requiredData['columns'])) {
				$datafields[$i]['customerAddress'][0]['country'] = ((isset($addressData[$addressCount])) ? $addressData[$addressCount] : ""); 
				$addressCount++;
			}
			if (in_array ('customer_address.postcode', $requiredData['columns'])) {
				$datafields[$i]['customerAddress'][0]['postcode'] = ((isset($addressData[$addressCount])) ? $addressData[$addressCount] : ""); 
				$addressCount++;
			}
		
			
			$fullName = "";
			if (!empty($datafields[$i]['customer'][0]['salutation'])) $fullName .= $datafields[$i]['customer'][0]['salutation']." ";
			if (!empty($datafields[$i]['customer'][0]['firstname'])) $fullName .= $datafields[$i]['customer'][0]['firstname']." ";
			if (!empty($datafields[$i]['customer'][0]['surname'])) $fullName .= $datafields[$i]['customer'][0]['surname'];
			
			foreach ($requiredData['columns'] AS $key => $columns) {
				
				//text is used as an array to store other $key's text value
				if ($key != "text") {
					$drawdatetemp = $intdrawepoch;
					$split = explode(".", $columns);
					//Start of custom rules
					if ($key == "membershipid") { 
						$letterdata[$i][$key] = $datafields[$i]['customer'][0]['recordID'];
					} else if ($key == "title") { 
						$letterdata[$i][$key] = trim($datafields[$i]['customer'][0]['salutation']);
					} else if ($key == "playerfirstname" || $key == "firstname" || $key == "forename"|| $key == "playerforename") { 
						$letterdata[$i][$key] = trim($datafields[$i]['customer'][0]['firstname']);
					} else if ($key == "playerlastname" || $key == "surname"|| $key == "playersurname" || $key == "lastname") { 
						$letterdata[$i][$key] = trim($datafields[$i]['customer'][0]['surname']);			
					} else if ($key == "firstorlastname") {
						$letterdata[$i][$key] = trim($fullName);
					} else if ($key == "salutation") { //not sure why this is full name 
						$letterdata[$i][$key] = trim($fullName);
					} else if ($key == "name" || $key == "titlename") {
						$letterdata[$i][$key] = trim($fullName);
					} else if ($key == "uppername") {
						$letterdata[$i][$key] = trim(strtoupper($fullName));
					} else if ($key == "playerpostcode" || $key == "postcode") {
						$letterdata[$i][$key] = $datafields[$i]['customerAddress'][0]['postcode'];
					} else if ($key == "playerpostcounty" || $key == "county") {
						$letterdata[$i][$key] = $datafields[$i]['customerAddress'][0]['county'];
					} else if ($key == "playerposttown" || $key == "town") {
						$letterdata[$i][$key] = $datafields[$i]['customerAddress'][0]['town'];
					} else if ($key == "countofletters") {
						$letterCount = $i + 1;
						$letterdata[$i][$key] = "$letterCount of ".count($lookupData);
					
					} else { //no custom rules defined for this key
						
						//if there is a text value use that, other wise use the selected column
						if (!empty($requiredData['columns']['text'][$key])) {
							$letterdata[$i][$key] = $requiredData['columns']['text'][$key];
						} else {
							$letterdata[$i][$key] = $datafields[$i][$split[0]][0][$split[1]];
						}
					}
				}
				$letterdata[$i]['all'] = $datafields[$i];
			}
		}
		

		//create directory if it does not currently exist
		$directory = "/files/exports/letters/";
		$filename = fnconvertunixtime($datnow,"H:i:s-d-m-Y")."_".stripCharactersFilename($letterDetails['description'])."_ver_".$letterDetails['version'].".docx";
		if (!file_exists($directory)){$makedir = recursive_mkdir($directory);}
		
		$oldfile = $_SERVER['DOCUMENT_ROOT'].$letterDetails['fileLocation'];
		$newfile = $_SERVER['DOCUMENT_ROOT'].$directory.$filename;
		$moveResult = copy($oldfile, $newfile);
		
		if ($moveResult != true) {
			$return['error'] = ("Could not move file to the exports directory ($newfile)");
		} else {
	
			error_log("letter");
			
			$zip = new ZipArchive();
			if( $zip->open($newfile, ZIPARCHIVE::CHECKCONS ) !== TRUE ) { echo "failed to open template"; exit; }
			$file = 'word/document.xml';
			$data = $zip->getFromName($file);
			
			$doc = new DOMDocument();
			$doc->loadXML( $data );

			$root = $doc->getElementsByTagName("document")->item(0);
			$node = $doc->getElementsByTagName("body")->item(0);
			$pagebreak1 = $doc->createElement('w:br','');
			$attr = $doc->createAttribute('w:type');
			$attr->value = "page";
			$pagebreak1->appendChild($attr);
			//$pagebreak2 = $doc->createElement('w:lastRenderedPageBreak','');
			//$pagebreak->appendXML('<w:p w:rsidR="00801FEC" w:rsidRDefault="00801FEC"><w:r><w:br w:type="page"/></w:r></w:p><w:p w:rsidR="00284F46" w:rsidRDefault="00801FEC"><w:r><w:lastRenderedPageBreak/></w:r></w:p>');
			
			//loop around each record that we need to produce a letter for
			$letterGroup = "";
			for($i = 0; $i < count($lookupData); $i++) {

				//if it is our first loop then newnode is node (the original) otherwise we clone the original and modify that.
				if ($i != 0) {
					$newnode = $node->cloneNode(true);
				} else {
					$newnode = $node;
				}
				$wts = $newnode->getElementsByTagNameNS('http://schemas.openxmlformats.org/wordprocessingml/2006/main', 'fldChar');
				
				for( $x = 0; $x < $wts->length; $x++ )
				{
					if( $wts->item( $x )->attributes->item(0)->nodeName == 'w:fldCharType' && $wts->item( $x )->attributes->item(0)->nodeValue == 'begin' )
					{
						$newcount = getMailMerge( $wts, $x, $letterdata[$i], $mergerecord);
						$x = $newcount['counter'];
					}
				}

				//if the record isnt the first letter then append another copy
				if (($i+1) == count($lookupData)) {
					try {
						$lastpage = $newnode->getElementsByTagName('w:br')->item(0);
						if ($lastpage !== null) $newnode->removeChild($lastpage);
					} catch (Exception $e) {
					
					}
					$root->appendChild($newnode);
				} else if (count($lookupData) != 1) { //! testing if there is more than one not if not first
					$node->appendChild($pagebreak1);
					//$node->appendChild($pagebreak2);
					$root->appendChild($newnode);
				}

				$mergerecord++;
				$doc->saveXML();
			
				
				
				//insert a record into letterHistory
				$strdbsql = "INSERT INTO letterHistory (letterID, letterGroup, customerID, orderID, dataArray, timestamp, fileLocation, operator, batchID) VALUES (:letterID, :letterGroup, :customerID, :orderID, :dataArray, :timestamp, :fileLocation, :operator, :batchID);";
				$arrdbvalues = array("letterID"=>$letterID, "letterGroup"=>$letterGroup, "customerID"=>$datafields[$i]['customer'][0]['recordID'], "customerID"=>$datafields[$i]['orderHeader'][0]['recordID'], "dataArray"=>serialize($letterdata[$i]), "timestamp"=>$datnow, "fileLocation"=>$directory.$filename, "operator"=>$_SESSION['operatorID'], "batchID"=>$batchID);
				$strType = "insert";
				if ($updateAccounts) $insertresult = query($conn, $strdbsql, $strType, $arrdbvalues);
				//else error_log($strdbsql);

				
				if ($updateAccounts AND $letterGroup == "") {
					$letterGroup = $insertresult;
					$strdbsql = "UPDATE letterHistory SET letterGroup = :letterGroup WHERE recordID = :recordID";
					$arrdbvalues = array("letterGroup"=>$letterGroup,"recordID"=>$letterGroup);
					$strType = "update";
					$updateresult = query($conn, $strdbsql, $strType, $arrdbvalues);
				}
			}
			$zip->deleteName($file);
			$zip->addFromString( $file, $doc->saveXML() );
			$zip->close();
			
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header("Content-Disposition: attachment; filename=\"{$filename}\"");
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($newfile));
			ob_clean();
			flush();
			readfile($newfile);
			$return['success'] = "Letters created: ".$directory.$filename;
			$return['filePath'] = $directory.$filename;
		}
		return $return;
	}
	function fn_findworkingdays($datknowndate,$intdays,$intdirection) {
		// $datknowndate = The date we know
		// $intdays = how many working days to move
		// $intdirection = -1 = $intdays before $datknowndate
		// $intdirection = 1  = $intdays after $datknowndate

		// working variables
		$intseconds = $intdirection * 86400;
		$intcountdays = 0;
		$intworkingday = 0;

		// find hour mins and secs of known date, and use these to generate holidays
		$inthour = date("H",$datknowndate);
		$intmins = date("i",$datknowndate);
		$intsecs = date("s",$datknowndate);

		// generate array of bank holidays
		$arrholidays = array();
			// normal bank holidays
			$datholiday = strtotime("first Monday of May ".date("Y",$datknowndate)) + 7200;
			$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
			array_push($arrholidays, $datholiday);
			$datholiday = strtotime("last Monday of May ".date("Y",$datknowndate)) + 7200;
			$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
			array_push($arrholidays, $datholiday);
			$datholiday = strtotime("last Monday of August ".date("Y",$datknowndate)) + 7200;
			$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
			array_push($arrholidays, $datholiday);

			// easter
			$intplusdays = easter_days(date("Y",$datknowndate));
			$datholiday = gmmktime($inthour,$intmins,$intsecs,3,21+$intplusdays-2,date("Y",$datknowndate));
			array_push($arrholidays, $datholiday);
			$datholiday = gmmktime($inthour,$intmins,$intsecs,3,21+$intplusdays+1,date("Y",$datknowndate));
			array_push($arrholidays, $datholiday);

			// christmas this year and last, just in case
			$datholiday = gmmktime($inthour,$intmins,$intsecs,12,25,date("Y",$datknowndate));
			while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
			array_push($arrholidays, $datholiday);
			$datholiday = gmmktime($inthour,$intmins,$intsecs,12,25,date("Y",$datknowndate)-1);
			while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
			array_push($arrholidays, $datholiday);

			// boxing day this year and last, just in case
			$datholiday = gmmktime($inthour,$intmins,$intsecs,12,26,date("Y",$datknowndate));
			while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
			array_push($arrholidays, $datholiday);
			$datholiday = gmmktime($inthour,$intmins,$intsecs,12,26,date("Y",$datknowndate)-1);
			while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
			array_push($arrholidays, $datholiday);

			// new years day this year and next, just in case
			$datholiday = gmmktime($inthour,$intmins,$intsecs,1,1,date("Y",$datknowndate));
			while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
			array_push($arrholidays, $datholiday);
			$datholiday = gmmktime($inthour,$intmins,$intsecs,1,1,date("Y",$datknowndate)+1);
			while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
			array_push($arrholidays, $datholiday);

		// loop through counting the days
		do {
			if ((date("w",$datknowndate)) > 0 and (date("w",$datknowndate)) < 6 and !in_array($datknowndate, $arrholidays)) { $intcountdays++; }
			$datknowndate = $datknowndate + $intseconds;
		} while ($intcountdays < $intdays);


		// The end date here may be a w/e or holiday - so make sure it is a working day
		while ($intworkingday == 0) {
			if ((date("w",$datknowndate)) > 0 and (date("w",$datknowndate)) < 6 and !in_array($datknowndate, $arrholidays)) { $intworkingday = 1; } else { $datknowndate = $datknowndate + $intseconds;	}
		}

		return $datknowndate;
	}
	function fn_newdate($conn,$datdate,$intrecordID,$intdir=1) {
		$intmodifier = 1;
		switch ($intrecordID) {
			case 1: // weekly
				// add 7 * 24 * 60 * 60 seconds to last draw epoch
				$intdatechange = 604800;
				break;
			case 2: // monthly
				// need to break down $intdrawepoch and increment the month
				$intmodifier = 0;
				$intmonth = date("n",$datdate);
				$intyear = date("Y",$datdate);
				switch ($intdir) {
					case 1:
						// forward
						$intmonth++;
						break;
					case 2:
						//backward
						$intmonth--;
						break;
				}
				if ($intmonth > 12) {
					$intmonth = 1;
					$intyear++;
				}
				if ($intmonth < 1) {
					$intmonth = 12;
					$intyear--;
				}
				$datdate = gmmktime(date("H",$datdate),date("i",$datdate),date("s",$datdate),$intmonth,date("j",$datdate),$intyear);
				break;
			case 3:	// 2 weeks
				$intdatechange = (2 * 604800);
				break;
			case 4:	// 3 weeks
				$intdatechange = (3 * 604800);
				break;
			case 5:	// 4 weeks
				$intdatechange = (4 * 604800);
				break;
			case 6:	// 5 weeks
				$intdatechange = (5 * 604800);
				break;
			case 7:	// 6 weeks
				$intdatechange = (6 * 604800);
				break;
		}

		if ($intmodifier == 1) {
			switch ($intdir) {
				case 1:
					$datdate += $intdatechange;
					break;
				case 2:
					$datdate -= $intdatechange;
					break;
			}
		}


		return $datdate;
	}
	function fn_encrypt($strdata,$iv) {
		$iv = substr("3185296328795229".$iv, -16);
		$strfullkey = $_SESSION['mwldecrypt'];
		$strpartkey = "Gmj*cfd3";

		$strfirstdata = substr($strdata,0,-4);
		$strseconddata = substr($strdata,-4);

		if ($strfullkey == "") {
			$strreturn = "-x-";
		} else {
			$strreturn = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $strfullkey, $strfirstdata, MCRYPT_MODE_CBC, $iv)."####".mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
		}

		return $strreturn;
	}
	function fn_decrypt($strdata,$iv) {
		$iv = substr("3185296328795229".$iv, -16);
		$strfullkey = $_SESSION['mwldecrypt'];
		$strpartkey = "Gmj*cfd3";

		$intlocation = strpos($strdata, "####");
		$strfirstdata = substr($strdata,0,$intlocation);
		$strseconddata = substr($strdata,$intlocation+4);

		if ($strfullkey == "") {
			$strreturn = "xxxx".mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
			$intlocation = strpos($strreturn, chr(0));
			$strreturn = substr($strreturn,0,$intlocation);
		} else {
			$strreturn = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strfullkey, $strfirstdata, MCRYPT_MODE_CBC, $iv);
			$intlocation = strpos($strreturn, chr(0));
			$strreturn = substr($strreturn,0,$intlocation);
			$strreturn .= mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
			$intlocation = strpos($strreturn, chr(0));
			$strreturn = substr($strreturn,0,$intlocation);
		}

		return $strreturn;
	}
	function fn_partdecrypt($strdata,$iv) {
		$iv = substr("3185296328795229".$iv, -16);
		$strpartkey = "Gmj*cfd3";

		$intlocation = strpos($strdata, "####");
		if ($intlocation < 1) {
			$strreturn = "";
		} else {
			$strseconddata = substr($strdata,$intlocation+4);
			$strreturn = "xxxx".mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
		}

		$intlocation = strpos($strreturn, chr(0));
		$strreturn = substr($strreturn,0,$intlocation);

		return $strreturn;
	}
	function fn_fixedlength($strstring,$intlength,$intpaddirection=STR_PAD_RIGHT,$strpadchr=" ",$strquote="") {
		$strstring = str_pad($strstring, $intlength, $strpadchr, $intpaddirection);
		$strstring = substr($strstring, 0, $intlength);
		return $strquote.$strstring.$strquote;
	}
	function fn_delimited($strstring,$intmin,$intmax,$inttype,$strdelimiter,$intnumberFields,$intthisfield,$intpaddirection=STR_PAD_RIGHT,$strpadchr=" ",$strquote="",$strprefixchr="") {
		if ($strprefixchr == "~") { $strprefixchr = "£"; }
		switch ($inttype) {
			case "0":
				// fixed
				$strstring = fn_fixedlength($strprefixchr.$strstring,$intmax, $intpaddirection, $strpadchr, $strquote);
				break;
			case "1":
			case "2":
				// Delimited
				$strstring = str_pad($strprefixchr.$strstring, $intmin, $strpadchr, $intpaddirection);
				$strstring = substr($strstring, 0, $intmax);
				$strstring = $strquote.$strstring.$strquote;
				if ($intthisfield < $intnumberFields) { $strstring .= $strdelimiter; }
				break;
		}
		return $strstring;
	}
	function sendemail($from, $fromemail, $replyto, $EmailTo, $subject, $textmessage, $htmlmessage, $useTemplate, $sendtoself) {

		// Create the mail transport configuration
		$transport = Swift_MailTransport::newInstance();

		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo(array(
		  "$EmailTo" => "$EmailTo"
		));
		$message->setSubject("$subject");
		$message->setBody("$textmessage");
		if ($useTemplate == 1) {
			$message->addPart(email_template($htmlmessage, $subject), 'text/html');
		} else {
			$message->addPart("$htmlmessage", 'text/html');
		}
		$message->setFrom("$fromemail", "$from");

		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		if ($mailer->send($message)){
			if ($sendtoself) mail($fromemail, $Subject, $Body, $headers);
			$strmessage["success"] = 'Email successfully sent.';
			return $strmessage;
		} else {
			$strmessage["error"] = 'Email could not be sent. Please try again';
			return $strmessage;
		}
	}
	function email_template($content, $subject){

		global $companyName, $companyPhone, $companyEmail;

		$template = ("<!doctype html>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>

			<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

			<title>$companyName</title>

			<style type='text/css'>
				.ReadMsgBody {width: 100%; background-color: #ffffff;}
				.ExternalClass {width: 100%; background-color: #ffffff;}
				body	 {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}
				table {border-collapse: collapse;}

				@media only screen and (max-width: 640px)  {
								body[yahoo] .deviceWidth {width:440px!important; padding:0;}
								body[yahoo] .center {text-align: center!important;}
						}

				@media only screen and (max-width: 479px) {
								body[yahoo] .deviceWidth {width:280px!important; padding:0;}
								body[yahoo] .center {text-align: center!important;}
						}

			</style>
			</head>

			<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' yahoo='fix' style='font-family: Georgia, Times, serif'>

			<!-- Wrapper -->
			<table width='640px' border='0' cellpadding='0' cellspacing='0' align='center'>
				<tr>
					<td width='100%' valign='top' bgcolor='#ffffff' style='padding-top:20px'>

						<!-- Start Header-->
						<table width='580' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
							<tr>
								<td width='100%' bgcolor='#ffffff'>

										<!-- Logo -->
										<table border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth'>
											<tr>
												<td style='padding:10px 20px' class='center'>
													<a href='#'><img src='' alt='' border='0' /></a>
												</td>
											</tr>
										</table><!-- End Logo -->

										<!-- Nav -->
										<table border='0' cellpadding='0' cellspacing='0' align='right' class='deviceWidth'>
											<tr>
												<td class='center' style='font-size: 13px; color: #272727; font-weight: light; text-align: right; font-family: Georgia, Times, serif; line-height: 20px; vertical-align: middle; padding:10px 20px; font-style:italic'>
													<p href='#' style='text-decoration: none; color: #3b3b3b;'></p>
												</td>
											</tr>
										</table><!-- End Nav -->

								</td>
							</tr>
						</table><!-- End Header -->

						<!-- One Column -->
						<table width='580'  class='deviceWidth' border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='#eeeeed'>
							<tr>
								<td style='font-size: 13px; color: #959595; font-weight: normal; text-align: left; font-family: Georgia, Times, serif; line-height: 24px; vertical-align: top; padding:20px' bgcolor='#eeeeed'>

									<table>
										<tr>
											<td valign='middle' style='padding:0 10px 10px 0'><a href='#' style='text-decoration: none; color: #272727; font-size: 16px; color: #272727; font-weight: bold; font-family:Arial, sans-serif '>$subject</a>
											</td>
										</tr>
									</table>

									$content
								</td>
							</tr>
						</table><!-- End One Column -->

						<!-- 4 Columns -->
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<tr>
								<td bgcolor='#363636' style='padding:20px;'>
									<table width='540' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
										<tr>
											<td>
												<table width='45%' cellpadding='0' cellspacing='0'  border='0' align='left' class='deviceWidth'>
													<tr>
														<td valign='top' style='font-size: 11px; color: #f1f1f1; color:#999; font-family: Arial, sans-serif; padding-bottom:20px' class='center'>
															<span style='color: #999999; font-size: 24px; '></span><br/>
															<b style='color: #5b5b5b;'></b><br/><br/>
														</td>
													</tr>
												</table>

												<table width='40%' cellpadding='0' cellspacing='0'  border='0' align='right' class='deviceWidth'>
													<tr>
														<td valign='top' style='font-size: 11px; color: #f1f1f1; font-weight: normal; font-family: Georgia, Times, serif; line-height: 26px; vertical-align: top; text-align:right' class='center'>


															<a href='#' style='text-decoration: none; color: #848484; font-weight: normal;'>$companyPhone</a><br/>
															<a href='#' style='text-decoration: none; color: #848484; font-weight: normal;'>$companyEmail</a>
														</td>
													</tr>
												</table>
											</td>
										</tr>

									</table>
								</td>
							</tr>
						</table><!-- End 4 Columns -->

					</td>
				</tr>
			</table> <!-- End Wrapper -->
			</body>
			</html>");
		return $template;
	}
	function getRandChars($numPositions = 1, $string = 'xxxxxxxx') {
		$positionArray = array();
		for ($i=0; $i < $numPositions; $i++) {
			$position = rand(1, strlen($string));
			if (in_array($position, $positionArray)) {
				$i--;
			} else {
				$positionArray[$i] = $position;
			}
		}
		return $positionArray;
	}
	function text_number($n){
		$teen_array = array(11, 12, 13, 14, 15, 16, 17, 18, 19);
		$single_array = array(1 => 'st', 2 => 'nd', 3 => 'rd', 4 => 'th', 5 => 'th', 6 => 'th', 7 => 'th', 8 => 'th', 9 => 'th', 0 => 'th');
		$if_teen = substr($n, -2, 2);
		$single = substr($n, -1, 1);
		if ( in_array($if_teen, $teen_array) ){
			$new_n = $n . 'th';
		} else if ( $single_array[$single] ){
			$new_n = $n . $single_array[$single];
		}
		return $new_n;
	}
	function remote_file_exists($path){
		return (@fopen($path,"r")==true);
	}

	function imageupload($location, $name, $inputname) {

		//upload image to images folder
		$strerror = "";
		$arrallowlist = array("jpg", "jpeg", "png", "gif");
		$newfilename = strtolower($_FILES[$inputname]['name']);
		$strfileextn = trim(strtolower(substr($newfilename,-4)),".");

		$filename = "/files/images/".$location;
		//create directory if it does not currently exist
		if (!file_exists($filename)){
			$makedir = recursive_mkdir($filename);
		}
		if ($makedir) {
			$filename = $_SERVER['DOCUMENT_ROOT'].$filename.$name.".".$strfileextn;

			if ($_FILES[$inputname]['error'] > 0)
			{
				switch ($_FILES[$inputname]['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				// Does the file have the right extension
				if (!in_array($strfileextn,$arrallowlist))
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br>You may only upload image files in jpeg, gif or png format.<br><br>The file you are trying to upload is a ".$_FILES[$inputname]['type']." file.<br><br>Please convert the picture to the required format and try again.";
				}
				else
				{
					if (is_uploaded_file($_FILES[$inputname]['tmp_name']))
					{
						if (!move_uploaded_file($_FILES[$inputname]['tmp_name'], $filename))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES[$inputname]['tmp_name']." and could not move file to destination directory ($filename). Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES[$inputname]['name'].". Please try again.<br>";
					}
				}
			}
		} else {
			$strerror = "Problem: Could not find or create the directory";
		}

		$imagedetails['location'] = $filename;
		$imagedetails['extension'] = $strfileextn;
		$imagedetails['error'] = $strerror;
		return $imagedetails;
	}
	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
	function getSubCategories($parentID, $level, $maxLevel, $conn) {

		$level++;

		$style = "style='padding-left: ".(($level + 1) * 10)."px; display: none; border-left: 1px dotted; min-height: 17px;'";

		$getsubcatsQuery = "SELECT category.* FROM category INNER JOIN category_relations ON category.recordID = category_relations.childID WHERE parentID = :parentID AND level = ".$level." ORDER BY category_relations.menuOrder";
		$strType = "multi";
		$arrdbparam = array("parentID" => $parentID);
		$subcats = query($conn, $getsubcatsQuery, $strType, $arrdbparam);

		$i = 1;

		foreach ($subcats as $subcat) {
			if ($level <= $maxLevel)
			{
				$numNestedQuery = "SELECT COUNT(*) AS num_nested FROM category_relations WHERE parentID = :categoryID AND level = ".($level + 1);
				$strType = "single";
				$numnesteddbparam = array( "categoryID" => $subcat['recordID'] );
				$numNested = query($conn, $numNestedQuery, $strType, $numnesteddbparam);
				//echo $numNested;
				//var_dump($numnesteddbparam);
			}


			print ("<li id='".$subcat['recordID']."' data-level='".$level."' data-order='".$i."' data-parentid='".$parentID."'>");
				print ("<span><span class='expand_collapse'>+</span> ".$subcat['contentName']." (".$subcat['metaPageLink'].") <a href='#' onclick='jsviewCategory(".$subcat['recordID']."); return false;'>Edit</a></span>");
				if ($level <= $maxLevel)
				{
					print ("<ul class='sortableCategoryList sortableSubcategoryList' ".$style.">");
					if ($numNested['num_nested'] > 0)
					{
						getSubCategories($subcat['recordID'], $level, $maxLevel, $conn);
					}
					print ("</ul>");
					$i++;
				}
			print ("</li>");
		}

	}
	function pdfprint($html,$strpdforientation) {

		// Include the main TCPDF library (search for installation path).
		require_once('tcpdf_include.php');

		// create new PDF document
		$pdf = new TCPDF($strpdforientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Author');
		$pdf->SetTitle('Test');
		$pdf->SetSubject('Testing');

		// turn headers and footers off
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(5, 15, 5);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();

		$css = ("<!-- EXAMPLE OF CSS STYLE -->
		<style>
		table {
			border: 0 none;
			border-collapse: collapse;
			padding: 0;
		}
		th {
			border-bottom: 1px solid #E1E1E1;
			font-size: 10px;
			font-weight: normal;
			padding: 5px;
		}
		table.tabTable {
			background: none repeat scroll 0 0 #FFFFFF;
			border-left: 1px solid #E1E1E1;
			border-top: 1px solid #E1E1E1;
			margin: 14px 0 5px;
		}
		table.tabTable th {
			background: none repeat scroll 0 0 #FCFCFC;
			border-bottom: 1px solid #E1E1E1;
			border-right: 1px solid #E1E1E1;
			font-size: 10px;
			font-weight: normal;
			padding: 5px;
		}
		table.tabTable th a {
			font-weight: bold;
			line-height: 20px;
			text-decoration: none;
		}
		table.tabTable td {
			border-bottom: 1px solid #E1E1E1;
			border-right: 1px solid #E1E1E1;
			padding: 5px;
		}
		table.tabTable td .listingDescription {
			font-size: 12px;
			line-height: 17px;
			padding: 5px 0;
		}
		table.tabTable td a {
			line-height: 20px;
			text-decoration: none;
		}</style>");

		$html = $css.$html;
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		// reset pointer to the last page
		$pdf->lastPage();

		// ---------------------------------------------------------
		ob_clean();
		//Close and output PDF document
		$pdf->Output($documentname.".pdf", 'I');
	}
	function logout() {
		session_destroy();
		header ("Location: /admin/login.php");
	}
	function FormatName($name=NULL){
		if (empty($name))
		return false;

		$name = strtolower($name);
		$name = preg_replace("[\-]", " - ",$name); // Surround hyphens with our delimiter so our strncmp is accurate

		//if (preg_match("/^[a-z]{2,}$/i",$name))  // Simple preg_match if statement
		//{
			$names_array = explode(' ',$name);  // Set the delimiter as a space.

			for ($i = 0; $i < count($names_array); $i++)
			{
				if (strncmp($names_array[$i],'mc',2) == 0 || ereg('^[oO]\'[a-zA-Z]',$names_array[$i]))
				{
				   $names_array[$i][2] = strtoupper($names_array[$i][2]);
				}
				$names_array[$i] = ucfirst($names_array[$i]);
			}
			$name = implode(' ',$names_array);

			$name = preg_replace("[ \- ]", "-",$name); //  Remove the extra instances of our delimiter
			return ucwords($name);
		//}
	}
	function recursive_mkdir($path, $mode = 0777) {
		$dirs = explode(DIRECTORY_SEPARATOR , $path);
		$count = count($dirs);
		$path = '.';
		for ($i = 0; $i < $count; ++$i) {
			$path .= DIRECTORY_SEPARATOR . $dirs[$i];
			if (!is_dir($path) && !mkdir($path, $mode)) {
				return false;
			}
			//chmod($path, 0777);
		}
		return true;
	}
	function fntimeconvert($strdate,$intflag) {
		if (strlen($strdate) == 0) {
			$intepoch = 0;
		} else {
			if ($intflag == 0) {
				// Use time
				$intepoch = gmmktime(substr($strdate,11,2),substr($strdate,14,2),substr($strdate,17,2),substr($strdate,5,2),substr($strdate,8,2),substr($strdate,0,4));
			} else {
				// use 12:00:00 for time
				$intepoch = gmmktime(12,0,0,substr($strdate,5,2),substr($strdate,8,2),substr($strdate,0,4));
			}
		}

		return $intepoch;
	}

	function fngetRandomString($length) {
		$validCharacters = "0123456789";
		$validCharNumber = strlen($validCharacters);
		$result = "";
		for ($i = 0; $i < $length; $i++) {
			$index = mt_rand(0, $validCharNumber - 1);
			$result .= $validCharacters[$index];
		}
		return $result;
	}
	function fngetRandomStringChar($length) {
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}
	function fndataexportfixed($dataarray, $filename="export.txt", $streol=PHP_EOL) {
		ob_end_clean();
		$f = fopen('php://memory', 'w');
		foreach ($dataarray as $line) {
			fputs($f,$line.$streol);
		}
		// rewrite the "file" with the txt lines
		fseek($f, 0);
		// tell the browser it's going to be a txt file
		header('Content-Type: application/txt');
		// tell the browser we want to save it instead of displaying it
		header('Content-Disposition: attachement; filename="'.$filename.'"');
		// make php send the generated txt lines to the browser
		fpassthru($f);
		exit();
	}
	function importDataValidation($rules, $data, $importDataRowID, $conn) {
		$importRowErrors = array();

		$i = 0;
		foreach($rules AS $rule)
		{
			switch ($rule) {
				case "stockGroupMetaLink":

					if ($data['stockGroupMetaLink'] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Meta Link Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Group Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "stockGroupMetaLinkExists":

					$checkGroupExistsQuery = "SELECT * FROM stock_group_information WHERE metaLink = :stockGroupMetaLink";
					$strType = "single";
					$arrdbparam = array("stockGroupMetaLink" => $data['stockGroupMetaLink']);
					$checkGroupExists = query($conn, $checkGroupExistsQuery, $strType, $arrdbparam);

					if ($data['stockGroupMetaLink'] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Meta Link Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Group Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if ($data['stockGroupMetaLink'] != "" AND empty($checkGroupExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Meta Link Not Valid</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Stock Group Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['stockGroupMetaLink']."' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "stockCode":

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockCode'] = $data['stockCode'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stockcode Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stockcode</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "stockPrice":

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockCode'] = $data['stockCode'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Price Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Price</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "stockPriceControl":

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockCode'] = $data['stockCode'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Price Control Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Control Price</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "assocCatMetaPageLink":

					$checkCatExistsQuery = "SELECT * FROM category WHERE metaPageLink = :assocCatMetaPageLink";
					$strType = "single";
					$arrdbparam = array("assocCatMetaPageLink" => $data['assocGroupMetaLink']);
					$checkCatExists = query($conn, $checkCatExistsQuery, $strType, $arrdbparam);

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if ($data[$rule] != "" AND empty($checkCatExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Not Valid</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocGroupMetaLink']."' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "assocCatMetaRangeLink":

					$checkRangeExistsQuery = "SELECT * FROM stock_ranges WHERE metaPageLink = :assocRangeMetaPageLink";
					$strType = "single";
					$arrdbparam = array("assocRangeMetaPageLink" => $data['assocGroupMetaLink']);
					$checkCatExists = query($conn, $checkRangeExistsQuery, $strType, $arrdbparam);

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if ($data[$rule] != "" AND empty($checkCatExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Not Valid</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocGroupMetaLink']."' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "assocGroupMetaLink":

					$checkCatExistsQuery = "SELECT * FROM category WHERE metaPageLink = :assocCatMetaPageLink";
					$strType = "single";
					$arrdbparam = array("assocCatMetaPageLink" => $data['assocGroupMetaLink']);
					$checkCatExists = query($conn, $checkCatExistsQuery, $strType, $arrdbparam);

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if ($data[$rule] != "" AND empty($checkCatExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Not Valid</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocGroupMetaLink']."' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "assocCatType":

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['assocCatType'] = $data['assocCatType'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category type ('category' or 'range') not stated</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Type</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if ($data[$rule] != "category" AND $data[$rule] != "range") { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['assocCatType'] = $data['assocCatType'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Type Not Valid, must be 'category' or 'range'</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Type</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocCatType']."' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "groupName":

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Name Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Group Name</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "groupDescription":

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Description Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Group Description</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "groupRetailPrice":

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Retail Price Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Group Retail Price</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "groupStatus":

					$checkStatusExistsQuery = "SELECT * FROM stock_status WHERE description = :groupStatus";
					$strType = "single";
					$arrdbparam = array("groupStatus" => $data['groupStatus']);
					$checkStatusExists = query($conn, $checkStatusExistsQuery, $strType, $arrdbparam);

					if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Status Missing</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Group Status</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

					} else if ($data[$rule] != "" AND empty($checkStatusExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Status Not Valid</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Stock Group Status (must be 'Deleted', 'Do not show on site', 'Standard', 'Web Exclusive' or 'Catalogue Only')</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['groupStatus']."' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;

				case "groupBrandMetaLink":

					if ($data[$rule] != "") { //if there is an error this is how to show it in each row

						$checkBrandExistsQuery = "SELECT * FROM stock_brands WHERE metaPageLink = :groupBrandMetaLink";
						$strType = "single";
						$arrdbparam = array("groupBrandMetaLink" => $data['groupBrandMetaLink']);
						$checkBrandExists = query($conn, $checkBrandExistsQuery, $strType, $arrdbparam);

						if(empty($checkBrandExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) {

							$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
							$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Brand Meta Link Not Valid</label>";
							$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Brand Meta Link</label>";
							$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['groupBrandMetaLink']."' />";

						} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
							$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
							unset($data['error-'.$rule.'-'.$importDataRowID]);
						}
					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				break;
			}
			$i++;
		}
		return $importRowErrors;
	}
	function dataexportcsv($dataarray, $filename = "export.csv", $delimiter=",") {

		ob_end_clean();
		$f = fopen('php://memory', 'w');
		// loop over the input array
		foreach ($dataarray as $line) {
			// generate csv lines from the inner arrays
			fputcsv($f, $line, $delimiter);
		}
		// rewrite the "file" with the csv lines
		fseek($f, 0);
		// tell the browser it's going to be a csv file
		header('Content-Type: application/csv;charset=UTF-8');
		// tell the browser we want to save it instead of displaying it
		header('Content-Disposition: attachement; filename="'.$filename.'"');
		// make php send the generated csv lines to the browser
		fpassthru($f);
		exit();
	}
	class fixed2CSV extends SplFileObject{
		public function __construct( $filename ){parent::__construct( $filename );}

		/*
		* Settor, is called when trying to assign a value to non-existing property
		*
		* @access    public
		* @param    string    $name    The name of the property to set
		* @param    mixed    $value    The value of the property
		* @throw    Excption if property is not able to be set
		*
		*/
		public function __set( $name, $value )
		{
			switch( $name )
			{
				case 'eol':
				case 'fields':
				case 'separator':
				$this->$name = $value;
				break;

				default:
				throw new Exception("Unable to set $name");
			}
		}

		/**
		*
		* Gettor This is called when trying to access a non-existing property
		*
		* @access    public
		* @param    string    $name    The name of the property
		* @throw    Exception if proplerty cannot be set
		* @return    string
		*
		*/
		public function __get( $name )
		{
			switch( $name )
			{
				case 'eol':
				return "\n";

				case 'fields':
				return array();

				case 'separator':
				return ',';

				default:
				throw new Exception("$name cannot be set");
			}
		}

		/**
		*
		* Over ride the parent current method and convert the lines
		*
		* @access    public
		* @return    string    The line as a CSV representation of the fixed width line, false otherwise
		*
		*/
		public function current()
		{
			if( parent::current() )
			{
				$csv = '';
				$fields = new cachingIterator( new ArrayIterator( $this->fields ) );
				foreach( $fields as $f )
				{
					$csv .= trim( substr( parent::current(), $fields->key(), $fields->current()  ) );
					$csv .= $fields->hasNext() ? $this->separator : $this->eol;
				}
				return $csv;
			}
			return false;
		}
	}

	
	
function getValueUsingKey($key, $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode) {
				
		$value = "";
		$columnID = $columnDataMatches[$key]['option'];
		
		if ($columnID == "") { //just set to a value set in the config, not from imported file
			
			$value = $columnDataMatches[$key]['value'];
		
		} else if (strpos($columnID, 'explode') !== FALSE){ //part of another exploded column, the key contains the word explode so we search for that
		
			//format is: explode0-0 first value is explode id, second is explode column
			$explodeValues = explode("-", substr($columnID,7));
			$explodeID = $explodeValues[0];
			$explodeColumn = $explodeValues[1];
			
			//need to get details of explode from array
			$explodeColumnID = $columnExplode[$explodeID]['column'];
			$explodeCharacter = $columnExplode[$explodeID]['explodeChar'];
			
			//need to explode the actual column
			$explodeArray = explode($explodeCharacter, $dataRow[$explodeColumnID-1]);
			
			//set the value to the exploded column value
			$value = trim($explodeArray[$explodeColumn]);
			//error_log("set to explode value $explodeID - $value - $explodeColumn - $explodeCharacter");
		
		} else if (strpos($columnID, 'concat') !== FALSE) { //concatenated column
			$concatID = substr($columnID,6);
			foreach ($arrconcat[$concatID] as $myrow) {
				if ($myrow['column'] > 0) {
					$strthispart = $dataRow[$myrow['column']-1];
					if ($strthispart != "") {$value .= $strthispart . $myrow['append']; }
				}
			}
		} else {
			$value = $dataRow[$columnID-1];
		}
		
		/*if ($columnID ==31) error_log($value);
		show($dataRow);
		die;*/

		//column conversion if we need to convert the imported data to a database friendly value
		if (!empty($columnConversion)) {
			foreach ($columnConversion AS $conversion) {
				if ($conversion['column'] == $columnID) {
					if ($conversion['found'] == 1) {
					
						//error_log("Value: ".$value);
						//error_log("Search: ".$conversion['searchString']);
					
						if (strpos($value, $conversion['searchString']) !== FALSE) {
							$value = $conversion['foundvalue'];
							//error_log("set to found value");
						} else if ($conversion['notfound'] == 1 AND strpos($value, $conversion['searchString']) === FALSE) {
							$value = $conversion['notfoundvalue'];
							//error_log("set to not found value");
						}
						
					} else if ($conversion['notfound'] == 1) {
						if (strpos($value, $conversion['searchString']) === FALSE) {
							$value = $conversion['notfoundvalue'];
							//error_log("set to not found value");
						} else if ($conversion['found'] == 1 AND strpos($value, $conversion['searchString']) !== FALSE) {
							$value = $conversion['foundvalue'];
							//error_log("set to found value");
						}
					}
					//error_log(serialize($conversion));
				}
			}
		}
		return $value;
	}
	function checkRequiredDataColumns($dataRow,$key,$column,$columnDataMatches, $arrconcat, $columnConversion, $columnExplode){
	
		$error = "";
		//check to make sure the required data fields are available
		if ($column['option'] != "") {
			$columnID = $column['option']-1;
			//$columnValue = $column['value'];
			$columnValue = getValueUsingKey ($key, $columnDataMatches, $dataRow, $arrconcat, $columnConversion, $columnExplode);
			
			//check to see this column isnt blank
			if ($dataRow[$columnID] == "" AND $columnValue == "") {
				$error = "Required column ($key) value is empty";
			}
			//error_log($key.$dataRow[$columnID]);
		}
		return $error;
	}
	function fnstrtflt($number) {
		$il = strlen($number);
		$flt = "";
		$cstr = "";

		for($i=0;$i<$il;$i++) {
			$cstr = substr($number, $i, 1);
			if(is_numeric($cstr) || $cstr == ".") { $flt = $flt.$cstr; }
		}
		return $flt; 
	}
	
?>