<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_pagevisible"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	$strdbsql = "UPDATE stock_group_information SET enabled = :showValue WHERE recordID = :recordID";
	$queryResult = query($conn,$strdbsql,"update",["recordID"=>$_REQUEST['pageID'],"showValue"=>$_REQUEST['visible']]);
	
	$conn = null; // close the Database connection after all processing
?>
