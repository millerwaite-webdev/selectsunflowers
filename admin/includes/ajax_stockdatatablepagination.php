<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_stockdtattablepagination"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	
	// DB table to use
	$table = 'stock_group_information INNER JOIN stock ON stock_group_information.recordID = stock.groupID';
	 
	// Table's primary key
	$primaryKey = 'stock_group_information.recordID';
	 
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array( 'db' => 'stock_group_information.recordID', 'dt' => 0),
		array( 'db' => 'stock_group_information.name', 'dt' => 1),
		array( 'db' => 'stock.stockCode', 'dt' => 2),
		array( 'db' => 'stock.price', 'dt' => 3),
		array( 'db' => 'stock_group_information.statusID', 'dt' => 4)
	);
	 
	// SQL server connection information
	$sql_details = array(
		'user' => USER,
		'pass' => PASS,
		'db'   => DB,
		'host' => HOST
	);
	 
	 
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( 'ssp.class.php' );
	 
	/*echo json_encode(
		SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
	);*/
	
	$ajaxDataItems = SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns );
	$intAjaxDataItems = count($ajaxDataItems);
	
	$i = 1;
	print ("{");
		foreach($ajaxDataItems AS $ajaxDataIndex => $ajaxDataItem)
		{
			print ("\"".$ajaxDataIndex."\": ");
			if(!is_array($ajaxDataItem))
			{
				print ("\"".$ajaxDataItem."\"");
			}
			else
			{
				print ("[");
						$intAjaxDataItem = count($ajaxDataItem);
						$y = 1;
						foreach($ajaxDataItem AS $ajaxNestedDataItem)
						{
							print ("[");
							//	print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[0])."\",");
								print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[1])."\",");
								print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[2])."\",");
								print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[3])."\",");
								print ("\"<button onclick='jsviewStockGroup(\\\"".$ajaxNestedDataItem[0]."\\\"); return false;' class='center btn btn-primary circle' style='display:inline-block;' type='submit'><i class='fa fa-pencil'></i></button>\",");
								if($ajaxNestedDataItem[4] != 4) print ("\"<button onclick='jstoggleStockGroup(\\\"".$ajaxNestedDataItem[0]."\\\"); return false;' class='center btn btn-danger circle' style='display:inline-block;' type='submit'><i class='fa fa-ban'></i></button>\"");
								else print ("\"<button onclick='jstoggleStockGroup(\\\"".$ajaxNestedDataItem[0]."\\\"); return false;' class='center btn btn-success circle' style='display:inline-block;' type='submit'><i class='fa fa-check'></i></button>\"");
							print ("]");
							if($y < $intAjaxDataItem)
							{
								print (",");
							}
							$y++;
						}
				print ("]");
			}
			if($i < $intAjaxDataItems)
			{
				print (",");
			}
			$i++;
		}
	print ("}");
	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null; // close the Database connection after all processing
?>
