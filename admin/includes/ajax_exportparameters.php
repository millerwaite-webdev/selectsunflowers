<?php

	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_exportparameters"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to lottery Database

	function fnShowFromDate($REQUEST){
		print ("<div class='form-group'>");
			print ("<label for='frm_fromdate' class='control-label'>Date From: </label>");
			print ("<input tabindex='1' type='text' name='frm_fromdate' id='frm_fromdate' maxlength='80' style='width:150px;' size='35' maxlength='50' class='form-control' />");
		print ("</div>");
	}
	function fnShowToDate($REQUEST){
		print ("<div class='form-group'>");
			print ("<label for='frm_todate' class='control-label'>Date To: </label>");
			print ("<input tabindex='1' type='text' name='frm_todate' id='frm_todate' maxlength='80' style='width:150px;' size='35' maxlength='50' class='form-control' />");
		print ("</div>");
	}
	function fnShowPickLists($REQUEST,$conn){
		
		print("<div class='form-group'>");
			print("<label for='frm_pickList' class='control-label'>Pick List:</label>");
			print("<select name='frm_pickList' id='frm_pickList' class='form-control'>");
				
				$strdbsql = "SELECT * FROM deliveryPickList WHERE closed = 0";
				$strType = "multi";
				$arrdbparams = array();
				$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
				
				print("<option value='' >Not Required</option>"); //default
				foreach ($resultdata AS $row)
				{
					$strSelected = "";
					print("<option value='".$row['recordID']."'".$strSelected.">".date("d/m/Y", $row['pickdate'])." - ".$row['description']."</option>");
				}
			print("</select>");
		print("</div>");
	}

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	$strdbsql = "SELECT * FROM configFileExportFormats WHERE exportID = :exportID";
	$strType = "single";
	$arrdbparams = array("exportID"=>$_REQUEST['exportID']);
	$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
		
	/*print ("<div class='form-group'>");
		print ("<label for='' class='control-label'>Description: </label><div class='col-sm-5'>");
			print ("<p>".$resultdata['description']."</p>");
		print ("</div>");
	print ("</div>");*/
	
	if ($resultdata['formSearch'] == "") { //show all
		
		fnShowFromDate($_REQUEST);
		fnShowToDate($_REQUEST);
		fnShowPickLists($_REQUEST,$conn);
		
	} else {
		$formSearchArray = explode(",", $resultdata['formSearch']);
		if (in_array("fromDate", $formSearchArray)) fnShowFromDate($_REQUEST);
		if (in_array("toDate", $formSearchArray)) fnShowToDate($_REQUEST);
		if (in_array("picklist", $formSearchArray)) fnShowPickLists($_REQUEST,$conn);
	}
	
	/*fromDate
	toDate*/
	
	print ("<div class='form-group'>");
			print ("<button class='btn btn-primary ' onclick='fngeneratereport(\"".$resultdata['matchVariable']."\"); return false;'>Generate Export Document</button>");
	print ("</div>");

	$resultdata = null;
	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null; // close the lottery Database connection after all processing
	session_write_close();
?>

