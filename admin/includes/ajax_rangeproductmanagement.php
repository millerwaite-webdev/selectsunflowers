<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_rangeproductmanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $rangesData)
	{
		//var_dump($rangesData);
		$i = 0;
		$intRanges = count($rangesData);
		foreach($rangesData AS $rangeData)
		{
			$strcmd = $rangeData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_range_relations WHERE rangeID = :rangeID";
					$strType = "single";
					$arrdbparam = array( "rangeID" => $rangeData['rangeID'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT stockOrder FROM stock_range_relations WHERE rangeID = :rangeID AND stockID = :stockID";
						$strType = "single";
						$arrdbparam = array( "rangeID" => $rangeData['rangeID'], "stockID" => $rangeData['stockID'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['stockOrder'] != $rangeData['order'])
						{
							if (($currOrder['stockOrder'] < $rangeData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE stock_range_relations SET stockOrder = stockOrder - 1 WHERE rangeID = :rangeID AND stockOrder <= :newOrder AND stockOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"rangeID" => $rangeData['rangeID'],
												"prevOrder" => $currOrder['stockOrder'],
												"newOrder" => $rangeData['order']
											);
								
								if ($rangeData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['stockOrder'] > $rangeData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE stock_range_relations SET stockOrder = stockOrder + 1 WHERE rangeID = :rangeID AND stockOrder <= :prevOrder AND stockOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"rangeID" => $rangeData['rangeID'],
												"prevOrder" => $currOrder['stockOrder'],
												"newOrder" => $rangeData['order']
											);
								if ($rangeData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($rangeData['order'] > 0 && $rangeData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE stock_range_relations SET stockOrder = :order WHERE rangeID = :rangeID AND stockID = :stockID";
								$strType = "update";
								$arrdbparams = array(
												"order" => $rangeData['order'],
												"rangeID" => $rangeData['rangeID'],
												"stockID" => $rangeData['stockID']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						$currOrder['stockOrder'] = $rangeData['order'];
						
						if ($rangeData['order'] > 0)
						{
							if ($rangeData['order'] <= $maxOrder['max_order'])
							{
								//echo "add\n";
								$updateOrdersQuery = "UPDATE stock_range_relations SET stockOrder = stockOrder + 1 WHERE rangeID = :rangeID AND stockOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"rangeID" => $rangeData['rangeID'],
												"newOrder" => $rangeData['order']
											);
								
								//echo "move down before addition\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$insertOrderQuery = "INSERT INTO stock_range_relations (rangeID, stockID, stockOrder) VALUES (:rangeID, :stockID, :order)";
							$strType = "insert";
							$arrdbparams = array(
											"rangeID" => $rangeData['rangeID'],
											"stockID" => $rangeData['stockID'],
											"order" => $rangeData['order']
										);
										
							//echo "addition\n";
							$insertOrder = query($conn, $insertOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['menuOrder']." ".$rangeData['order']."\n";
					//echo $maxOrder['max_order']." ".$rangeData['order']."\n";
					//echo "Order is: ".$rangeData['order']."\n";
					
					break;
					
				case "delete":
					
					//var_dump($rangeData);
					$deleteRelationQuery = "DELETE FROM stock_range_relations WHERE rangeID = :rangeID AND stockID = :stockID AND stockOrder = :order";
					$strType = "delete";
					$arrdbparams = array(
									"rangeID" => $rangeData['rangeID'],
									"stockID" => $rangeData['stockID'],
									"order" => $rangeData['order']
								);
					//echo "deletion\n";
					$deleteOrder = query($conn, $deleteRelationQuery, $strType, $arrdbparams);
					
					//echo $deleteOrderQuery;
					//var_dump($deleteOrder);
					
					$i++;
					
					if ($i == $intRanges)
					{
						
						$strdbsql = "SELECT recordID FROM stock_range_relations WHERE rangeID = :rangeID ORDER BY stockOrder";
						$strType = "multi";
						$arrdbparams = array(
										"rangeID" => $rangeData["rangeID"]
									);
						//echo "get brands\n";
						$associatedProducts = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedProducts AS $associatedProduct)
						{
							$strdbsql = "UPDATE stock_range_relations SET stockOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedProduct['recordID']
										);
							//echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
