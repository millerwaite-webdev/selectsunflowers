<?php
$strrootpath = $_SERVER['DOCUMENT_ROOT']."/";

//include ("../lib/swift_required.php");
include($strrootpath."includes/Encoding.php");
include ($strrootpath."includes/corefunctions.php");
include ($strrootpath."includes/config.php");
include ("adminfunctions.php");

// Site Variables
// ================
date_default_timezone_set('UTC');
$dattod = gmmktime(0, 0, 0, date("m", time()), date("d", time()), date("Y", time()));
$datnow = gmmktime(date("H", time()), date("i", time()), date("s", time()), date("m", time()), date("d", time()), date("Y", time()));
$dattom = gmmktime(22, 59, 59, date("m", time()), date("d", time())+1, date("Y", time()));
$strwarning = "";
$strsuccess = "";
$strerror = "";
$booldebug = false;
$strwebtitle = "Site Administration";


$strsoftwareversion = "V0.1";

$supportemail = "carlwilkes@millerwaite.co.uk";
$strcookiedom = "localhost";
$customText1 = ""; //displayed at the bottom of the invoice and gift message (invoiceprint.php)

$strAFDServer = "http://pce.afd.co.uk";
$strAFDSerial = "811995";
$strAFDPassword = "PS92k2mM";
$strAFDUserID = "FURNE01";


class CompanyDetails {
	public $companyAddress;
	public $companyPhone;
	public $companyFax;
	public $companyEmail;
	public $companyName;
	public $strcookiedom;
}

function getCompanyDetails($companyAddress, $companyPhone, $companyFax, $companyEmail, $companyName)
{
	$company = new CompanyDetails();
	$company->companyAddress = $companyAddress;
	$company->companyPhone = $companyPhone;
	$company->companyFax = $companyFax;
	$company->companyEmail = $companyEmail;
	$company->companyName = $companyName;
	return $company;
}
#endregion Company

#region Database
class DatabaseSettings
{
	public $driver;
	public $user;
	public $db;
	public $pass;
	public $host;
}

function getDatabaseDetails()
{
	$databasesettings = new DatabaseSettings();
	$databasesettings->driver = constant("DRIVER");
	$databasesettings->user = constant("USER");
	$databasesettings->db = constant("DB");
	$databasesettings->pass = constant("PASS");
	$databasesettings->host = constant("HOST");
	return $databasesettings;
}


#endregion

// Common Requests
// ================
if (isset($_REQUEST['error']) AND $_REQUEST['error'] == 1) { $strerror = 'Page not found';}
if (isset($_REQUEST['logout']) AND $strpage != 'login') {logout();}
if (isset($_SESSION['enabled'])) { if ($_SESSION['enabled'] == 0) { $location = ("Location: /login.php?error=Your Account has been disabled"); }}
if (isset($_SESSION['blankpwd']) AND $_SESSION['blankpwd'] == 1 AND $strpage != "operator") { header("Location: /operator.php");}
else if (isset($_SESSION['blankpwd']) AND $_SESSION['blankpwd'] == 1) { $strwarning = "Please update your password before using the system";}

// Common Checks
// ================
$conn = connect(); // Open Connection to database
$pagePermission = pagePermission($strpage, $conn);
if (isset($pagePermission) AND $strpage != 'login') { header ($pagePermission); }

if (!isset($_SESSION['operatorID']) AND $strpage != 'login'){
	if (isset($_COOKIE['ADMINConnection'])) {

		//get the connection details for this cookie
		$strdbsql = "SELECT * FROM admin_operator_connections WHERE recordID=:recordID AND timestamp >= unix_timestamp(now() - INTERVAL 8 HOUR);";
		$arrType = "single";
		$strdbparams = array("recordID" => $_COOKIE['ADMINConnection']);
		$connection = query($conn, $strdbsql, $arrType, $strdbparams);

		if (!empty($connection)) {

			//get the local account details
			$strdbsql = "SELECT * FROM admin_operator WHERE operatorID=:operatorID";
			$arrType = "single";
			$strdbparams = array("operatorID" => $connection['operatorID']);
			$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

			if (!empty($resultdata)) {
				$_SESSION['operatorID'] = $resultdata['operatorID'];
				$_SESSION['username'] = $resultdata['username'];
				$_SESSION['permissionGroup'] = $resultdata['permissionGroup'];
				$_SESSION['enabled'] = $resultdata['enabled'];
				$restore = true;
			} else {
				unset($_COOKIE['ADMINConnection']);
				header ("Location: login.php");
			}
		} else {
			unset($_COOKIE['ADMINConnection']);
			header ("Location: login.php");
		}
	} else {
		header ("Location: login.php");
	}
}
$conn = null; // close the database connection after all processing


?>