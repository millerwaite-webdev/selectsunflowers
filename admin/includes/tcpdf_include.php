<?php
//============================================================+
// File name   : tcpdf_include.php
// Begin       : 2008-05-14
// Last Update : 2013-05-14
//
// Description : Search and include the TCPDF library.
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Search and include the TCPDF library.
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Include the main class.
 * @author Nicola Asuni
 * @since 2013-05-14
 */

// always load alternative config file for examples
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/lib/tcpdf/config/tcpdf_config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/lib/tcpdf/tcpdf.php');


//============================================================+
// END OF FILE
//============================================================+
