<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_search"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	
	//details submitted
	if(isset($_REQUEST['q']) AND isset($_REQUEST['searchType']))
	{
	
		if(isset($_REQUEST['searchType']))
		{
			switch ($_REQUEST['searchType']) {
				
				case "customer":
				
					$strselect = "%".$_REQUEST['q']."%";
					//$strdbsql = "SELECT customer.*, customer_address.add1, customer_address.postcode FROM customer LEFT JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE CONCAT_WS(' ', customer.title, customer.firstname, customer.surname, company, telephone, mobile, email, add1, add2, add3, town, county, postcode) LIKE :search ORDER BY surname ASC";
					$strdbsql = "SELECT customer.*, customer_address.add1, customer_address.postcode FROM customer LEFT JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE postcode LIKE :search ORDER BY postcode ASC";
					$arrType = "multi";
					$strdbparams = array("search" => $strselect);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

					if (isset($_REQUEST['c'])) { $strcmd = $_REQUEST['c']; } else { $strcmd = ""; }

					$intCust = count($resultdata);

					if(count($resultdata) > 0 && ($_REQUEST['q'] != ""))
					{
						print("<div class='section customerSearchResults'>");
							print("<div style='height:330px;overflow-y:scroll;'>");
								print("<table class='member-list table table-striped table-bordered table-hover table-condensed' style='margin-bottom:0;'>");
									print("<thead>");
										print("<tr>");
											print("<th>Title</th>");
											print("<th>Firstname</th>");
											print("<th>Surname</th>");
											//print("<th>Company</th>");
											print("<th>Telephone</th>");
											print("<th>Mobile</th>");
											print("<th>Email</th>");
											print("<th>Add 1</th>");
											print("<th>Postcode</th>");
											print("<th>Select</th>");
										print("</tr>");
									print("</thead>");
									print("<tbody>");
								
								if (count($resultdata) > 0){
									foreach($resultdata AS $row)
									{
										print("<tr style='cursor:pointer' class='listitem'>");
										print("<td>".$row['title']."</td>");
										print("<td>".$row['firstname']."</td>");
										print("<td>".$row['surname']."</td>");
										//print("<td>".$row['company']."</td>");
										print("<td>".$row['telephone']."</td>");
										print("<td>".$row['mobile']."</td>");
										print("<td>".$row['email']."</td>");
										print("<td>".$row['add1']."</td>");
										print("<td>".$row['postcode']."</td>");
										print("<td><button type='button' class='btn btn-primary circle' onclick=\"setitem('".$row['recordID']."','".$_REQUEST['searchType']."');\"><i class='fa fa-eye'></i></button></td>");
										print("</tr>");
									}
								}
									print("</tbody>");
								print("</table>");
							print("</div>");
						print("</div>");
					}
				
				break;
				case "customerReturn":
					$strselect = $_REQUEST['q'];
					$strdbsql = "SELECT customer.*, d.title AS dtitle, d.firstname AS dfirstname, d.surname AS dsurname, d.add1 AS dadd1, d.add2 AS dadd2, d.add3 AS dadd3, d.town AS dtown, d.county AS dcounty, d.country AS dcountry, d.postcode AS dpostcode, b.title AS btitle, b.firstname AS bfirstname, b.surname AS bsurname, b.add1 AS badd1, b.add2 AS badd2, b.add3 AS badd3, b.town AS btown, b.county AS bcounty, b.country AS bcountry, b.postcode AS bpostcode FROM customer LEFT JOIN customer_address d ON customer.defaultDeliveryAdd = d.recordID LEFT JOIN customer_address b ON customer.defaultBillingAdd = b.recordID WHERE customer.recordID = :recordID";
					$arrType = "single";
					$strdbparams = array("recordID"=>$strselect);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
					
					print (json_encode($resultdata));
				break;
				case "customerDeliveryAdd":
				
					$strselect = "%".$_REQUEST['q']."%";
					//$strdbsql = "SELECT customer.*, customer_address.add1, customer_address.postcode FROM customer LEFT JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE CONCAT_WS(' ', customer.title, customer.firstname, customer.surname, company, telephone, mobile, email, add1, add2, add3, town, county, postcode) LIKE :search ORDER BY surname ASC";
					$strdbsql = "SELECT customer.*, customer_address.add1, customer_address.postcode FROM customer LEFT JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE postcode LIKE :search ORDER BY postcode ASC";
					$arrType = "multi";
					$strdbparams = array("search" => $strselect);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

					if (isset($_REQUEST['c'])) { $strcmd = $_REQUEST['c']; } else { $strcmd = ""; }

					$intCust = count($resultdata);

					if(count($resultdata) > 0 && ($_REQUEST['q'] != ""))
					{
						print("<div class='section customerSearchResults'>");
							print("<div style='height:330px;overflow-y:scroll;'>");
								print("<table class='member-list table table-striped table-bordered table-hover table-condensed' style='margin-bottom:0;'>");
									print("<thead>");
										print("<tr>");
											print("<th>Title</th>");
											print("<th>Firstname</th>");
											print("<th>Surname</th>");
											//print("<th>Company</th>");
											//print("<th>Telephone</th>");
											//print("<th>Mobile</th>");
											//print("<th>Email</th>");
											print("<th>Add 1</th>");
											print("<th>Postcode</th>");
											print("<th>Select</th>");
										print("</tr>");
									print("</thead>");
									print("<tbody>");
								
								if (count($resultdata) > 0){
									foreach($resultdata AS $row)
									{
										print("<tr style='cursor:pointer' class='listitem'>");
										print("<td>".$row['title']."</td>");
										print("<td>".$row['firstname']."</td>");
										print("<td>".$row['surname']."</td>");
										//print("<td>".$row['company']."</td>");
										//print("<td>".$row['telephone']."</td>");
										//print("<td>".$row['mobile']."</td>");
										//print("<td>".$row['email']."</td>");
										print("<td>".$row['add1']."</td>");
										print("<td>".$row['postcode']."</td>");
										print("<td><button type='button' class='btn btn-primary circle' onclick=\"setitem('".$row['recordID']."','".$_REQUEST['searchType']."');\"><i class='fa fa-eye'></i></button></td>");
										print("</tr>");
									}
								}
									print("</tbody>");
								print("</table>");
							print("</div>");
						print("</div>");
					}
				
				break;
				case "stock":
				
					$strselect = "%".$_REQUEST['q']."%";
					$strdbsql = "SELECT stock.*, stock_group_information.name, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE CONCAT_WS(' ', stock_group_information.name, stock_group_information.description, stock.stockCode, stock.weight, stock.price) LIKE :search AND stock_group_information.statusID > 2 ORDER BY stockCode ASC";
					$arrType = "multi";
					$strdbparams = array("search" => $strselect);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);

					if (isset($_REQUEST['c'])) { $strcmd = $_REQUEST['c']; } else { $strcmd = ""; }

					$intCust = count($resultdata);

					if(count($resultdata) > 0 && ($_REQUEST['q'] != ""))
					{
						print("<table class='member-list table table-striped table-bordered table-hover table-condensed' style='border:none; border-top: 1px solid #ddd; margin: 10px 0;'>");
						print("<thead><tr>");
							print("<th>Stock Code</th>");
							print("<th>Name</th>");
							print("<th>Description</th>");
							print("<th>Price</th>");
							print("<th>Weight</th>");
							print("<th></th>");
						print("</tr></thead><tbody>");
						
						if (count($resultdata) > 0){
							foreach($resultdata AS $row)
							{
								$price = $row['price'];
								
								//check to see if our product is a hamper 0 = no, 1 = yes
								if ($row['productType'] == 1) {
									//check to see if we have a hamper price
									if ($row['productCost'] > 0) { 
										$price = $row['productCost'];
									} else { //no hamper price so set the price to the total from all products
										$strdbsql = "SELECT sum(stock.price) AS totalPrice FROM stock_hamper INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID WHERE stock_hamper.stockGroupID = :stockGroup";
										$arrType = "single";
										$strdbparams = array("stockGroup"=>$row['groupID']);
										$hamperCost = query($conn, $strdbsql, $arrType, $strdbparams);
										$price = $hamperCost['totalPrice'];
									}
								}
							
								print("<tr style='cursor:pointer' class='listitem'>");
								print("<td>".$row['stockCode']."</td>");
								print("<td>".$row['name']."</td>");
								print("<td>".$row['description']."</td>");
								print("<td>&pound;".number_format($price,2)."</td>");
								print("<td>".$row['weight']."</td>");
								print("<td><button type='button' class='btn btn-primary right' onclick=\"setitem('".$row['recordID']."','".$_REQUEST['searchType']."');\">Select</button></td>");
								print("</tr>");
							}
						}
						print("</tbody>");
						print("</table>");
					}
				
				break;
				case "stockReturn":
					$strselect = $_REQUEST['q'];
					$strdbsql = "SELECT stock.*, stock_group_information.name, stock_group_information.description, stock_group_information.productType, stock_group_information.productCost FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock.recordID = :recordID";
					$arrType = "single";
					$strdbparams = array("recordID"=>$strselect);
					$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
					
					//check to see if our product is a hamper 0 = no, 1 = yes
					if ($resultdata['productType'] == 1) {
						//check to see if we have a hamper price
						if ($resultdata['productCost'] > 0) { 
							$resultdata['price'] = $resultdata['productCost'];
						} else { //no hamper price so set the price to the total from all products
							$strdbsql = "SELECT sum(stock.price) AS totalPrice FROM stock_hamper INNER JOIN stock ON stock_hamper.stockItemID = stock.recordID WHERE stock_hamper.stockGroupID = :stockGroup";
							$arrType = "single";
							$strdbparams = array("stockGroup"=>$resultdata['groupID']);
							$hamperCost = query($conn, $strdbsql, $arrType, $strdbparams);
							$resultdata['price'] = $hamperCost['totalPrice'];
						}
					}
					
					print (json_encode($resultdata));
				break;
			}
		}
	}
	$resultdata = null;

	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null;
?>

