<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	$boolCloseConn = false;
	if(!isset($_SESSION['operatorID']))
	{
		session_start();
		
		$strpage = "ajax_updatedeliveryorder"; //define the current page
		require_once("inc_sitecommon.php");
		
		if(empty($conn))
		{
			$conn = connect();
		}
	
		$customerID = $_REQUEST['customerID'];
		$deliveryID = $_REQUEST['deliveryID'];
		$boolCloseConn = true;
	}
	else
	{
		$customerID = $delivery['customerID'];
	}
	
	if(!empty($customerID) && $customerID != "null")
	{
		$strdbsql = "SELECT order_items.* FROM order_items LEFT JOIN deliveryOrders ON order_items.recordID = deliveryOrders.deliveryItemID INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID WHERE (deliveryOrders.deliveryItemID IS NULL";
		$arrParam = array("customerID" => $customerID);
		
		if($deliveryID != "")
		{
			$strdbsql .= " OR deliveryOrders.deliveryItemID = :deliveryID";
			$arrParam['deliveryID'] = $deliveryID;
		}
		
		$strdbsql .= ") AND order_header.customerID = :customerID AND order_items.display = 1";
		$strType = "multi";
		$resultdata = query($conn, $strdbsql, $strType, $arrParam);
		
		if(!empty($resultdata))
		{
			print ("<div class='col-sm-6' style='padding-left: 0px;'>");
				
				print ("<h4>Unassigned Order Items</h4>");
				
				print ("<ul id='unassignedOrderItems' class='ui-sortable'>");
				
				$strdbsql = "SELECT order_items.* FROM order_items LEFT JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID WHERE deliveryOrders.deliveryItemID IS NULL AND order_header.customerID = :customerID AND order_items.display = 1";
				$strType = "multi";
				$arrParam = array("customerID" => $customerID);
				$resultdata = query($conn, $strdbsql, $strType, $arrParam);
				foreach($resultdata AS $unassignedOrderItem)
				{
					print ("<li id='".$unassignedOrderItem['recordID']."'>");
						
						$stockDetails = unserialize($unassignedOrderItem['stockDetailsArray']);
						print ("<span>".$stockDetails['stockCode']." ".$stockDetails['name']." ".$stockDetails['description']."</span>");
						
					print ("</li>");
				}
				
				print ("</ul>");
				
				print ("<span onclick='selectAllOrderItems(\"unassigned\")' class='selectAll'>Select All</span>");
			
			print ("</div>");
			
			print ("<div class='col-sm-6'>");
				
				print ("<h4>Delivery Order Items</h4>");
				
				print ("<ul id='deliveryOrderItems' class='ui-sortable'>");
				
				$strdbsql = "SELECT order_items.* FROM order_items INNER JOIN deliveryOrders ON order_items.recordID = deliveryOrders.orderItemID INNER JOIN order_header ON order_items.orderHeaderID = order_header.recordID WHERE deliveryOrders.deliveryItemID = :deliveryItemID AND order_header.customerID = :customerID";
				$strType = "multi";
				$arrParam = array("deliveryItemID" => $deliveryID, "customerID" => $customerID);
				$resultdata = query($conn, $strdbsql, $strType, $arrParam);
				
				foreach($resultdata AS $deliveryOrderItem)
				{
					print ("<li id='".$deliveryOrderItem['recordID']."'>");
						
						$stockDetails = unserialize($deliveryOrderItem['stockDetailsArray']);
						print ("<span>".$stockDetails['stockCode']." ".$stockDetails['name']." ".$stockDetails['description']."</span>");
						
					print ("</li>");
				}
				
				print ("</ul>");
				
				print ("<span onclick='selectAllOrderItems(\"assigned\")' class='selectAll'>Select All</span>");
			
			print ("</div>");
		}
		else
		{
			print ("The selected order does not contain any items");
		}
	}
	else
	{
		print ("Please select an order to assign items");
	}
	
	
	if($boolCloseConn)
	{
		$conn = null; // close the Database connection after all processing
	}
?>
