<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_customerdatatablepagination"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	
	// DB table to use
	$table = 'customer LEFT JOIN customer_address ON customer.defaultDeliveryAdd = customer_address.recordID';
	 
	// Table's primary key
	$primaryKey = 'customer.recordID';
	 
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array( 'db' => 'customer.title', 'dt' => 0 ),
		array( 'db' => 'customer.firstname', 'dt' => 1),
		array( 'db' => 'customer.surname', 'dt' => 2),
		array( 'db' => 'customer.telephone', 'dt' => 3),
		array( 'db' => 'customer.mobile', 'dt' => 4),
		array( 'db' => 'customer.email', 'dt' => 5),
		array( 'db' => 'customer_address.postcode', 'dt' => 6),
		array( 'db' => 'customer.recordID', 'dt' => 7)
	);
	 
	// SQL server connection information
	$sql_details = array(
		'user' => USER,
		'pass' => PASS,
		'db'   => DB,
		'host' => HOST
	);
	 
	 
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	require( 'ssp.class.php' );
	 
	/*echo json_encode(
		SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
	);*/
	
	$ajaxDataItems = SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns );
	$intAjaxDataItems = count($ajaxDataItems);
	
	$i = 1;
	print ("{");
		foreach($ajaxDataItems AS $ajaxDataIndex => $ajaxDataItem)
		{
			print ("\"".$ajaxDataIndex."\": ");
			if(!is_array($ajaxDataItem))
			{
				print ("\"".$ajaxDataItem."\"");
			}
			else
			{
				print ("[");
						$intAjaxDataItem = count($ajaxDataItem);
						$y = 1;
						foreach($ajaxDataItem AS $ajaxNestedDataItem)
						{
							print ("[");				
							print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[0])."\","); //title
							print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[1])."\","); //firstname
							print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[2])."\","); //surname
							print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[3])."\","); //phone number
						//	print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[4])."\","); //mobile number
							print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[5])."\","); //email
							print ("\"".str_replace(array("\"", "/", "\n", ","), array("\\\"", "\/", "", ""), $ajaxNestedDataItem[6])."\","); //postcode
							//print ("\"".str_replace(array("\"", "/", "\n"), array("\\\"", "\/", ""), $ajaxNestedDataItem[6])."\","); //billing
							//print ("\"".str_replace(array("\"", "/", "\n"), array("\\\"", "\/", ""), $ajaxNestedDataItem[7])."\","); //delivery
							print ("\"<button onclick='jsorderHistory(\\\"".$ajaxNestedDataItem[7]."\\\"); return false;' class='center btn btn-primary circle' type='submit'><i class='fa fa-history'></i></button>\",");
							print ("\"<button onclick='jsfullDetails(\\\"".$ajaxNestedDataItem[7]."\\\"); return false;' class='center btn btn-primary circle' type='submit'><i class='fa fa-phone'></i></button>\",");
							print ("\"<button onclick='jsDeleteMember(\\\"".$ajaxNestedDataItem[7]."\\\"); return false;' class='center btn btn-danger circle' type='submit'><i class='fa fa-remove'></i></button>\"");
							print ("]");
							if($y < $intAjaxDataItem)
							{
								print (",");
							}
							$y++;
						}
				print ("]");
			}
			if($i < $intAjaxDataItems)
			{
				print (",");
			}
			$i++;
		}
	print ("}");
	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null; // close the Database connection after all processing
?>
