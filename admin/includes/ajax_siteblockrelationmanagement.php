<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_siteblockrelationmanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $relationsData)
	{
		//var_dump($imagesData);
		foreach($relationsData AS $relationData)
		{
			$strcmd = $relationData['cmd'];
			$type = $relationData['type']."ID";
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM site_block_relations WHERE ".$type." = :typeID AND positionID = :positionID";
					$strType = "single";
					$arrdbparam = array(
										"typeID" => $relationData['sitePageID'],
										"positionID" => $relationData['positionID']
									);
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					$currOrderQuery = "SELECT pageOrder FROM site_block_relations WHERE recordID = :recordID";
					$strType = "single";
					$arrdbparam = array( "recordID" => $relationData['recordID'] );
					$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
					
					if ($currOrder['pageOrder'] != $relationData['pageOrder'])
					{
						if (($currOrder['pageOrder'] < $relationData['pageOrder']))
						{
							//echo "move down\n";
							$updateOrdersQuery = "UPDATE site_block_relations SET pageOrder = pageOrder - 1 WHERE ".$type." = :typeID AND positionID = :positionID AND pageOrder <= :newOrder AND pageOrder >= :prevOrder";
							$strType = "update";
							$arrdbparam = array(
											"typeID" => $relationData['sitePageID'],
											"positionID" => $relationData['positionID'],
											"prevOrder" => $currOrder['pageOrder'],
											"newOrder" => $relationData['pageOrder']
										);
							
							if ($relationData['pageOrder'] <= $maxOrder['max_order'])
							{
								//echo "move down before reorder\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
						}
						elseif ($currOrder['pageOrder'] > $relationData['pageOrder'])
						{
							
							//echo "move up\n";
							$updateOrdersQuery = "UPDATE site_block_relations SET pageOrder = pageOrder + 1 WHERE ".$type." = :typeID AND positionID = :positionID AND pageOrder <= :prevOrder AND pageOrder >= :newOrder";
							$strType = "update";
							$arrdbparam = array(
											"typeID" => $relationData['sitePageID'],
											"positionID" => $relationData['positionID'],
											"prevOrder" => $currOrder['pageOrder'],
											"newOrder" => $relationData['pageOrder']
										);
							if ($relationData['pageOrder'] > 0)
							{
								//echo "move up before reorder\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
						}
						
						if ($relationData['pageOrder'] > 0 && $relationData['pageOrder'] <= $maxOrder['max_order'])
						{
							//echo "move level\n";
							$updateOrderQuery = "UPDATE site_block_relations SET pageOrder = :pageOrder WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparams = array(
											"pageOrder" => $relationData['pageOrder'],
											"recordID" => $relationData['recordID']
										);
							
							//echo "reorder\n";
							$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['menuOrder']." ".$relationData['galleryOrder']."\n";
					//echo $maxOrder['max_order']." ".$relationData['galleryOrder']."\n";
					//echo "Order is: ".$relationData['galleryOrder']."\n";
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
