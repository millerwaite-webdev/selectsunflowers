<?php

	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_postcode"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	if (isset($_REQUEST['postcode'])) $postcode = $_REQUEST['postcode']; 
	else $postcode = ""; 

	
	# Build up the XML query string
	$xmlLocation = $strAFDServer . "/afddata.pce?";
	$xmlLocation = $xmlLocation . "Serial=" . $strAFDSerial . "&";
	$xmlLocation = $xmlLocation . "Password=" . $strAFDPassword . "&";
	$xmlLocation = $xmlLocation . "UserID=" . $strAFDUserID . "&";
	
	//check to see if we have a postcode key and if so return all results otherwise return a list
	if (!empty($_REQUEST['lstResults'])) {
		
		$xmlLocation = $xmlLocation . "Data=Address&Task=Retrieve&Fields=Standard";

		# Set XML parameter to retrieve the selected record
		$xmlLocation = $xmlLocation . "&Key=" . urlencode($_REQUEST['lstResults']);
	
	} else {
	
		$xmlLocation = $xmlLocation . "Data=Address&Task=FastFind&Fields=List";
		
		# Set the maximum number of records to return
		$xmlLocation = $xmlLocation . "&MaxQuantity=100";

		# Set the Country Name or ISO code for International operations
		$xmlLocation = $xmlLocation . "&Country=UK";

		# Set the lookup string
		$xmlLocation = $xmlLocation . "&Lookup=" . urlencode($postcode);
	}
	
	
	if((isset($_REQUEST["postcode"]) && $_REQUEST["postcode"] != '') || isset($_REQUEST["lstResults"]) && $_REQUEST["lstResults"] != '') {
		$xml = file_get_contents(str_replace(' ','', $xmlLocation)); // Removes unnecessary spaces
	} else {
		$xml = "<?xml version='1.0' encoding='UTF-8' ?>";
		$xml .= "<Address xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://postcodesoftware.co.uk/\">";
		$xml .= "<result>-2</result>";
		$xml .= "<ErrorText>No Postcode Entered</ErrorText>";
		$xml .= "</Address>";
	}
	
	header ("Content-type: text/xml; charset=utf-8");
	print(utf8_encode($xml));

	
	$resultdata = null;

	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null;
?>

