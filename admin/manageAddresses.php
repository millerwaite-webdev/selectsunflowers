<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * manageAddresses.php                                                * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	
	// Ensure user is logged on
	// --------------------
	if (empty($_SESSION['userID'])) { header ("Location: account.php"); }

	
	$strpage = "addressbook"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	$strerror = "";

	// ************* Requests processing ****************** //
	//=====================================================//
	
	if(isset($_REQUEST['command'])) $strcmd = $_REQUEST['command'];
	else $strcmd = "";

	switch ($strcmd)
	{

		case "changedefaults":

			$intdeladd = $_REQUEST['deladd'];
			$intbilladd = $_REQUEST['billadd'];
			
			$values = array(
				"deliveryadd" => $intdeladd,
				"billingadd" => $intbilladd,
				"custid" => $_SESSION['userID']
			);
			updateUserBillDelAddresses($values, "billanddel", $conn);

			break;

		case "deleteaddress":

			$intaddress = $_REQUEST['address'];

			deleteUserAddress($intaddress, $_SESSION['userID'], $conn);

			break;
	}
	
	// get current defaults from account settings
	$userDetails = getUserDetails($_SESSION['userID'], "byid", $conn);

	$intdeladd = $userDetails['defaultDeliveryAdd'];
	$intbilladd = $userDetails['defaultBillingAdd'];
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	print("<div id='mainContent'>");
	
		if($strerror != "") {
			print("<div class='notification-error'>");
				print("<h3>Error</h3>");
				print("<p>".$strerror."</p>");
			print("</div>");
		}
		if($strinfo != "") {
			print("<div class='notification-info'>");
				print("<h3>Info</h3>");
				print("<p>".$strinfo."</p>");
			print("</div>");
		}
?>
		<script type='text/javascript'>
		function jsnewaddress() {

			document.frm_form.action = "/editAddress.php";
			document.frm_form.command.value = "new";
			document.frm_form.address.value = 0;
			document.frm_form.submit();

		}

		function jsback() {
			window.location = "account.php";
		}

		function jssavedefaults() {
			document.frm_form.command.value = 'changedefaults';
			document.frm_form.submit();
			
		}

		function jseditaddress(addressid) {
			document.frm_form.action = "/editAddress.php";
			document.frm_form.address.value = addressid;
			document.frm_form.submit();

		}
		function jsdeladdress(addressid) {
			var answer = confirm ("Are you sure you wish to delete this address? ");
			if(answer) {
			document.frm_form.address.value = addressid;
				document.frm_form.command.value = 'deleteaddress';
				document.frm_form.submit();
			}
		}

		</script>

<?php

		print ("<form name='frm_form' action='manageAddresses.php' method='post' >");
			print ("<input type='hidden' name='command' />");
			print ("<input type='hidden' name='address' />");
			print ("<h2>Manage Addresses</h2>");
			if ($intuserid > 0)
			{
				print ("<div id='delivery' class='bodytext' style='clear:both; padding: 5px;'>");
					
					$userAddresses = getUserAddresses($intuserid, "all", $conn);
					
					if(count($userAddresses) > 0) {
						print("<table style='width: 100%;'>");
							print("<tr><th>Name</th><th>Address</th><th>Postcode</th><th style='text-align: center;'>Delivery Address</th><th style='text-align: center;'>Billing address</th><th>Options</th></tr>");
							print("<tr><td colspan='7'><hr/></td></tr>");
							foreach($userAddresses AS $userAddress) {
								$strname = trim(str_replace ("  ", " ", $userAddress['title']." ".$userAddress['firstname']." ".$userAddress['surname']));
								$straddress = $userAddress['add1']."<br/>\n".$userAddress['add2']."<br/>\n".$userAddress['add3']."<br/>\n".$userAddress['town']."<br/>\n".$userAddress['county']."<br/>\n";
								$straddress = str_replace("<br/>\n<br/>\n","<br/>\n", $straddress);
								$strcountry = $userAddress['country'];
								$strpostcode = $userAddress['postcode']."<br/>\n";
								$intaddress = $userAddress['recordID'];

								print("<tr>");
									print("<td>".$strname."</td>");
									print("<td>".$straddress."</td>");
									print("<td>".$strpostcode."</td>");
									if ($intaddress == $intdeladd) $strchecked = " checked='checked' ";
									else $strchecked = "";
									if($intdeladd == 0) {
										// if there is an address but your delivery address is nil set your delivery address
										$values = array(
											"deliveryadd" => $intaddress,
											"custid" => $intuserid
										);
										updateUserBillDelAddresses($values, "del", $conn);
										$intbilladd = $intaddress;
										$intdeladd = $intaddress;
										$strchecked = " checked='checked' ";
									}
									print("<td style='text-align: center;'><input type='radio' value='$intaddress' name='deladd' $strchecked /></td>");
									if ($intaddress == $intbilladd) $strchecked = " checked='checked' ";
									else $strchecked = "";
									if($intbilladd == 0) {
										// if there is an address but your billing address is nil set your billing address
										$values = array(
											"billingadd" => $intaddress,
											"custid" => $intuserid
										);
										updateUserBillDelAddresses($values, "bill", $conn);
										$intbilladd = $intaddress;
										$strchecked = " checked='checked' ";
									}
									print("<td style='text-align: center;'><input type='radio' value='$intaddress' name='billadd' $strchecked /></td>");

									if ($intaddress != $intdeladd && $intaddress != $intbilladd) $stroptions = "<input type='button' class='short_button' value='Delete address' onclick='jsdeladdress($intaddress)' />";
									else $stroptions = "";
									print("<td><input type='button' class='short_button' value='Edit Address' onclick='jseditaddress($intaddress)' />$stroptions</td>");

								print("</tr>");
								print("<tr><td colspan='7'><hr/></td></tr>");
						}
						print("</table>");
					}
				
				print ("</div>");
				print ("<div class='buttons' style='text-align:center'>");
					print ("<input type='button' class='short_button' onclick='jsnewaddress()' value='Add New Address' /> ");
					print ("<input type='button' class='short_button' onclick='jssavedefaults()' value='Update defaults' /> ");
					print ("<input type='button' onclick='jsback()' class='short_button' value='Return to Account' />");
				print ("</div>");
			}
			else
			{
				redirect("account.php");
			}

		print ("</form>");
		
	print("</div>");
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>