<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * customer-groups.php			                                       * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "paymentSenseCallback"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	require_once ("PaymentFormHelper.php");
	include ("PaymentSenseConfig.php");

	
	$Width = 800;
    $FormAction = '';
	
	// what we do here depends on the ResultDeliveryMethod
	$DuplicateTransaction = false;
	$PreviousTransactionMessage = '';

	switch ($ResultDeliveryMethod)
	{
		case 'POST':
			// the results will be delivered via POST variables to this
			// page	
			$boResultValidationSuccessful = PaymentFormHelper::validateTransactionResult_POST($MerchantID, 
																   							  $Password, 
																							  $PreSharedKey, 
																							  $HashMethod,
																							  $_POST,
																							  $trTransactionResult,
																							  $szValidateErrorMessage);
			// the results need to be stored here as this is the first time
			// they will have touched this system
			if ($boResultValidationSuccessful)
			{
				if (!PaymentFormHelper::reportTransactionResults($trTransactionResult, $szOutputMessage))
				{
					// handle the case where the results aren't stored correctly
				}
			}
			break;
		case 'SERVER':
			// the results have already been delivered via a server-to-server
			// call from the payment form to the ServerResultURL
			// need to query these transaction results to display
			$boResultValidationSuccessful = PaymentFormHelper::validateTransactionResult_SERVER($MerchantID, 
																								$Password, 
																								$PreSharedKey, 
																								$HashMethod,
																								$_GET,
																							    $trTransactionResult,
																								$szValidateErrorMessage);
			break;
		case 'SERVER_PULL':
			// need to query the results from the payment form using the passed
			// cross reference
			$szPaymentFormResultHandler = 'https://mms.'.$PaymentProcessorDomain.'/Pages/PublicPages/PaymentFormResultHandler.ashx';

			$boResultValidationSuccessful = PaymentFormHelper::validateTransactionResult_SERVER_PULL($MerchantID, 
																									 $Password, 
																									 $PreSharedKey, 
																									 $HashMethod,
																									 $_GET,
																									 $szPaymentFormResultHandler,
																							   		 $trTransactionResult,
																									 $szValidateErrorMessage);
			// the results need to be stored here as this is the first time
			// they will have touched this system
			if ($boResultValidationSuccessful)
			{
				if (!PaymentFormHelper::reportTransactionResults($trTransactionResult, $szOutputMessage))
				{
					// handle the case where the results aren't stored correctly
				}
			}
			break;
	}

	// display an error message if the transaction result couldn't be validated
	if (!$boResultValidationSuccessful)
	{
		$MessageClass = "ErrorMessage";
		$Message = $szValidateErrorMessage;
	}
	else
	{
		switch ($trTransactionResult->getStatusCode())
		{
			case 0:
				$MessageClass = 'SuccessMessage';
				break;
			case 4:
				$MessageClass = 'ErrorMessage';
				break;
			case 5:
				$MessageClass = 'ErrorMessage';
				break;
			case 20:
				$DuplicateTransaction = true;
				if ($trTransactionResult->getPreviousStatusCode() == 0)
				{
					$MessageClass = 'SuccessMessage';
				}
				else
				{
					$MessageClass = 'ErrorMessage';
				}
				$PreviousTransactionMessage = $trTransactionResult->getPreviousMessage();
				break;
			case 30:
				$MessageClass = 'ErrorMessage';
				break;
			default:
				$MessageClass = 'ErrorMessage';
				break;
		}

		$Message = $trTransactionResult->getMessage();
		$StatusCode = $trTransactionResult->getStatusCode();
		$PreviousStatusCode = $trTransactionResult->getPreviousStatusCode();
		$PreviousMessage = $trTransactionResult->getPreviousMessage();
		$CrossReference = $trTransactionResult->getCrossReference();
		$CardType = $trTransactionResult->getCardType();
		$CardClass = $trTransactionResult->getCardClass();
		$CardIssuer = $trTransactionResult->getCardIssuer();
		$CardIssuerCountryCode = $trTransactionResult->getCardIssuerCountryCode();
		$Amount = $trTransactionResult->getAmount();
		$CurrencyCode = $trTransactionResult->getCurrencyCode();
		$OrderID = $trTransactionResult->getOrderID();
		$TransactionType = $trTransactionResult->getTransactionType();
		$TransactionDateTime = $trTransactionResult->getTransactionDateTime();
		$OrderDescription = $trTransactionResult->getOrderDescription();
		$CustomerName = $trTransactionResult->getCustomerName();
		$Address1 = $trTransactionResult->getAddress1();
		$Address2 = $trTransactionResult->getAddress2();
		$Address3 = $trTransactionResult->getAddress3();
		$Address4 = $trTransactionResult->getAddress4();
		$City = $trTransactionResult->getCity();
		$State = $trTransactionResult->getState();
		$PostCode = $trTransactionResult->getPostCode();
		$CountryCode = $trTransactionResult->getCountryCode();
		
		if ($ResultDeliveryMethod == 'POST')
		{
			$EmailAddress = $trTransactionResult->getEmailAddress();
			$PhoneNumber = $trTransactionResult->getPhoneNumber();
		}
		else
		{
			session_start();
			$EmailAddress = $_SESSION['email'];
			$PhoneNumber = $_SESSION['phone_number'];
		}
		
	}


	$customerAddress = "";	
	if (!empty($Address1)) $customerAddress .= $Address1.", ";
	if (!empty($Address2)) $customerAddress .= $Address2.", ";
	if (!empty($Address3)) $customerAddress .= $Address3.", ";
	if (!empty($Address4)) $customerAddress .= $Address4.", ";
	if (!empty($City)) $customerAddress .= $City.", ";
	if (!empty($State)) $customerAddress .= $State.", ";
	if (!empty($PostCode)) $customerAddress .= $PostCode.", ";
	$customerAddress = rtrim($customerAddress, ', ');
	$Amount = $Amount/100;
	
	if ($StatusCode == '0'){
		$paymentStatus = 2;
		$print = '<strong>The Payment has been successful</strong><br/>Please make a note of the authorisation number below<br/>'.$Message.'<br/><a href="/sales-orders.php">Process Another</a>';
	} else if($StatusCode == '5'){
		$paymentStatus = 3;
		$print = '<strong>The Payment has been Declined</strong><br/>Reason: '.$Message.'<br/>Please check the details and try again<br/><a href="/Process.php?orderHeaderID='.$OrderID.'&amount='.number_format($Amount, 2, '.', ',').'">Try Again</a>';
	} else if ($StatusCode == '20'){
		$print = 'A duplicate transaction means that a transaction with these details has already been processed by the payment provider. The details of the original transaction are given below:';											
		if ($PreviousStatusCode == '0'){
			$paymentStatus = 2;
			$print .= '<strong>The transaction was successful</strong><br/>'.$PreviousTransactionMessage.'<br/><a href="/sales-orders.php">Process Another</a>';
		} else if ($PreviousStatusCode == '5') {
			$paymentStatus = 3;
			$print .= '<strong>The transaction was declined</strong><br/>Reason: '.$PreviousTransactionMessage.'<br/><a href="/Process.php?orderHeaderID='.$OrderID.'&amount='.number_format($Amount, 2, '.', ',').'">Try Again</a>';
		}
	} else {
		$paymentStatus = 5;
		$print = '<strong>An Unknown Error has Occurred</strong><br/><a href="/Process.php?orderHeaderID='.$OrderID.'&amount='.number_format($Amount, 2, '.', ',').'">Try Again</a>';
	}


	//replace on unique key (crossReference)
	$strdbsql = "REPLACE INTO `order_payment_details` (`orderHeaderID`, `transactionSource`, `transactionTimestamp`, `transactionAmount`, `paymentStatus`, `transactionCurrency`, `cardType`, `message`, `statusCode`, `previousStatusCode`, `previousMessage`, `crossReference`, `cardClass`, `cardIssuer`, `cardIssuerCountryCode`, `transactionType`, `orderDescription`, `customerName`, `customerAddress`, `countryCode`) VALUES (:orderHeaderID, :transactionSource, :transactionTimestamp, :transactionAmount, :paymentStatus, :transactionCurrency, :cardType, :message, :statusCode, :previousStatusCode, :previousMessage, :crossReference, :cardClass, :cardIssuer, :cardIssuerCountryCode, :transactionType, :orderDescription, :customerName, :customerAddress, :countryCode);";
	$strType = "insert";
	$arrdbparams = array(
		"orderHeaderID"=>$OrderID,
		"transactionSource"=>"Payment Sense",
		"transactionTimestamp"=>strtotime($TransactionDateTime),
		"transactionAmount"=>$Amount,
		"paymentStatus"=>$paymentStatus,
		"transactionCurrency"=>$CurrencyCode,
		"cardType"=>$CardType,
		"message"=>$Message,
		"statusCode"=>$StatusCode,
		"previousStatusCode"=>$PreviousStatusCode,
		"previousMessage"=>$PreviousMessage,
		"crossReference"=>$CrossReference,
		"cardClass"=>$CardClass,
		"cardIssuer"=>$CardIssuer,
		"cardIssuerCountryCode"=>$CardIssuerCountryCode,
		"transactionType"=>$TransactionType,
		"orderDescription"=>$OrderDescription,
		"customerName"=>$CustomerName,
		"customerAddress"=>$customerAddress,
		"countryCode"=>$CountryCode
	);
	$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
	
	//just keep a constant record as the above will update
	$strdbsql = "INSERT INTO `order_payment_details_history` (`orderHeaderID`, `transactionSource`, `transactionTimestamp`, `transactionAmount`, `paymentStatus`, `transactionCurrency`, `cardType`, `message`, `statusCode`, `previousStatusCode`, `previousMessage`, `crossReference`, `cardClass`, `cardIssuer`, `cardIssuerCountryCode`, `transactionType`, `orderDescription`, `customerName`, `customerAddress`, `countryCode`) VALUES (:orderHeaderID, :transactionSource, :transactionTimestamp, :transactionAmount, :paymentStatus, :transactionCurrency, :cardType, :message, :statusCode, :previousStatusCode, :previousMessage, :crossReference, :cardClass, :cardIssuer, :cardIssuerCountryCode, :transactionType, :orderDescription, :customerName, :customerAddress, :countryCode);";
	$strType = "insert";
	$arrdbparams = array(
		"orderHeaderID"=>$OrderID,
		"transactionSource"=>"Payment Sense",
		"transactionTimestamp"=>strtotime($TransactionDateTime),
		"transactionAmount"=>$Amount,
		"paymentStatus"=>$paymentStatus,
		"transactionCurrency"=>$CurrencyCode,
		"cardType"=>$CardType,
		"message"=>$Message,
		"statusCode"=>$StatusCode,
		"previousStatusCode"=>$PreviousStatusCode,
		"previousMessage"=>$PreviousMessage,
		"crossReference"=>$CrossReference,
		"cardClass"=>$CardClass,
		"cardIssuer"=>$CardIssuer,
		"cardIssuerCountryCode"=>$CardIssuerCountryCode,
		"transactionType"=>$TransactionType,
		"orderDescription"=>$OrderDescription,
		"customerName"=>$CustomerName,
		"customerAddress"=>$customerAddress,
		"countryCode"=>$CountryCode
	);
	$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
	
	
		// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Sales Order Processing</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

			print("<div class='section'>");

				print $print;

			print ("</div>");
	
		print("</div>");
	print("</div>");
	
	
	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>