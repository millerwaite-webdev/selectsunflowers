<?php

//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * info.php                                                           * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	if(isset($_GET['frame'])) $boolframe = true;
	else $boolframe = false;

	$strpage = $_REQUEST['page'];
	$getPageQuery = "SELECT * FROM site_pages WHERE pageName = :pageName";
	$strType = "single";
	$arrdbparam = array ( "pageName" => $strpage );
	$pageData = query($conn, $getPageQuery, $strType, $arrdbparam);
	$meta = array();
	
	if (!empty($pageData))
	{
		$meta['metaTitle'] = $pageData['metaTitle']." - Stollers Furniture & Interiors";
		$meta['description'] = $pageData['metaDescription'];
		$meta['keywords'] = $pageData['metaKeywords'];
		$meta['metaPageLink'] = $pageData['metaPageLink'].".html";
		$strpgtitle = $pageData['pageTitle'];
		$strpgcontent = $pageData['pageContent'];
		//breadcrumb links
		$strbreadcrumbs = "<div class='pro_breadcrumbs-indent'><ul class='pro_breadcrumbs-one'>";
		$strbreadcrumbs .= "<li><a href='index.php'>Home</a></li>";
		$strbreadcrumbs .= "<li><a href='#' class='current'>".$strpgtitle."</a></li></ul></div>";
	}
	else
	{
		header("HTTP/1.0 404 Not Found");
		$strpgtitle = "Page not found";
		$meta['metaTitle'] = "Page not found - Stollers Furniture & Interiors";
		$strpgcontent = "<br/><br/>Sorry, the page you're looking for could not be found on our site.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

		$strbreadcrumbs = "<div class='pro_breadcrumbs-indent'><ul class='pro_breadcrumbs-one'>";
		$strbreadcrumbs .= "<li><a href='index.php'>Home</a></li>";
		$strbreadcrumbs .= "<li><a href='#' class='current'>Page not Found</a></li></ul></div>";
	}

	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//	
?>
            <!--Image map and inaccessible content goes here so users can skip over it -->
           <div id="content">
				<?php echo $strbreadcrumbs; ?>
				<h2><?php echo $strpgtitle; ?></h2>
                <?php echo $strpgcontent; ?>
            <?php
			
				if ($strpage == 'sitemap') {
				
					print ("<table style='cell-spacing: 0; cellpadding: 10;' id='sitemap'><tr><td style='vertical-align: top;'>Site Pages: <br/>");
					
						print ("<a href='$strsiteurl' >Home</a><br/>");
						print ("<a href='$strsiteurl/news.php' >News</a><br/>");
						print ("<a href='$strsiteurl/jobs.php' >Jobs</a><br/>");
						
						$oldpagearr = array ("help", "sitemap", "offers", "downloads");

						$getPagesQuery = "SELECT * FROM site_pages ORDER BY recordID";
						$strType = "multi";
						$pagesData = query($conn, $getPagesQuery, $strType);

						if (empty($pagesData))
						{
							$strmessage = "Error";
						}
						else
						{
							foreach($pagesData AS $pageData)
							{
								if (!in_array($pageData['pageName'], $oldpagearr)) print ("<a href='$strsiteurl/".$pageData['pageName'].".html' >".$pageData['pageTitle']."</a><br/>");
							}
						}

						print ("</td><td style='vertical-align: top;'>Main Categories: <br/>");
						$getCategoriesQuery = "SELECT category.* FROM category INNER JOIN category_relations ON category.recordID = category_relations.parentID WHERE category_relations.parentID = category_relations.childID AND level = 1 ORDER BY category.contentName";
						$strType = "multi";
						$categories = query($conn, $getCategoriesQuery, $strType);

						if (empty($categories))
						{
							$strmessage = "Error";
						}
						else
						{
							foreach($categories AS $category)
							{
								print ("<a href='$strsiteurl/category_".$category['metaPageLink']."' >".$category['contentName']."</a><br/>");
							}
						}
						
						print ("</td><td style='vertical-align: top;'>Sub Categories: <br/>");
						$getSubcategoriesQuery = "SELECT DISTINCT category.* FROM category INNER JOIN category_relations ON category.recordID = category_relations.childID WHERE category_relations.parentID != category_relations.childID AND level > 1 ORDER BY category.contentName";
						$strType = "multi";
						$subcategories = query($conn, $getSubcategoriesQuery, $strType);

						if (empty($subcategories))
						{
							$strmessage = "Error";
						}
						else
						{
							foreach($subcategories AS $subcategory)
							{
								print ("<a href='$strsiteurl/category_".$subcategory['metaPageLink']."' >".$subcategory['contentName']."</a><br/>");
							}
						}
						
						print ("</td><td style='vertical-align: top;'>Brands: <br/>");
						$getBrandsQuery = "SELECT * FROM stock_brands ORDER BY brandName";
						$strType = "multi";
						$brands = query($conn, $getBrandsQuery, $strType);

						if (empty($brands))
						{
							$strmessage = "Error";
						}
						else
						{
							foreach($brands AS $brand)
							{
								print ("<a href='$strsiteurl/brand_".$brand['metaPageLink']."' >".$brand['brandName']."</a><br/>");
							}
						}
					print ("</td></tr></table>");
				}
				//breadcrumb links paste from above
				print("<hr class='clearing' /><br/>".$strbreadcrumbs);
			?>
			</div><!-- END OF PAGETEXT-->            

<script type="text/javascript">
/*<![CDATA[*/
OAS_rn = new String (Math.random());
OAS_rns = OAS_rn.substring (2, 11);
var tfsm_protocol = window.location.protocol;
if (tfsm_protocol == "https:") {
 DataColl="https://";
} else { DataColl="http://"; 
 } 
document.write('<img alt=""  SRC="'+DataColl+'oas.newsquestdigital.co.uk/RealMedia/ads/adstream.track/1'+OAS_rns+'?XE&epmAccountKey=2609&epmXTransKey=1381&epmXtransStep=0&ProductCategory=&ItemDescription=&XE" style="width:0px;height:0px;border:none" />');
/*]]>*/
</script>
	<?php
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>