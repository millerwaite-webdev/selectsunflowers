<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * customer-manage.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "customer-manage"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['userID'])) $userID = $_REQUEST['userID']; else $userID = "";
	
	switch($strcmd)
	{
		case "userSearch":
		
			$customersQuery = "SELECT * FROM customer WHERE ";
			$strType = "multi";
			$arrdbparams = array();
			$boolCriteria = false;
			
			if ($_POST['frm_fields'] == "any") $strwherejoin = " OR ";
			if ($_POST['frm_fields'] == "all") $strwherejoin = " AND ";
			
			if ($_REQUEST['frm_match'] == "partial") $strwhereoperator = "LIKE";
			if ($_REQUEST['frm_match'] == "exact") $strwhereoperator = "=";
			
			$firstname = $_POST['frm_firstname'];
			if(!empty($_POST['frm_firstname']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_firstname'] = "%".$_POST['frm_firstname']."%";
				}
				$customersQuery .= "firstname ".$strwhereoperator." :firstname";
				$arrdbparams['firstname'] = $_POST['frm_firstname'];
				$boolCriteria = true;
			}
			$surname = $_POST['frm_surname'];
			if(!empty($_POST['frm_surname']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_surname'] = "%".$_POST['frm_surname']."%";
				}
				$customersQuery .= "surname ".$strwhereoperator." :surname";
				$arrdbparams['surname'] = $_POST['frm_surname'];
				$boolCriteria = true;
			}
			$companyname = $_POST['frm_companyname'];
			if(!empty($_POST['frm_companyname']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_companyname'] = "%".$_POST['frm_companyname']."%";
				}
				$customersQuery .= "company ".$strwhereoperator." :company";
				$arrdbparams['company'] = $_POST['frm_companyname'];
				$boolCriteria = true;
			}
			$email = $_POST['frm_email'];
			if(!empty($_POST['frm_email']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_email'] = "%".$_POST['frm_email']."%";
				}
				$customersQuery .= "email ".$strwhereoperator." :email";
				$arrdbparams['email'] = $_POST['frm_email'];
				$boolCriteria = true;
			}
			
			if($boolCriteria)
			{
				$customersDetails = query($conn, $customersQuery, $strType, $arrdbparams);
			}
			else
			{
				$strwarning = "Please specify search criteria";
			}
			
		break;
		case "viewDetails":
			
			$customerDetailsQuery = "SELECT * FROM customer WHERE recordID = :userID";
			$strType = "single";
			$arrdbparam = array( "userID" => $userID );
			$customerDetails = query($conn, $customerDetailsQuery, $strType, $arrdbparam);
			
			if (count($customerDetails) < 1)
			{
				$strerror = "Matching customer not found";
			}
		break;
		case "createCustomer":
			
			$strfirstname = $_REQUEST['frm_firstname'];
			$strsurname = $_REQUEST['frm_surname'];
			$strtitle = $_REQUEST['frm_title'];
			$strcompany = $_REQUEST['frm_company'];
			$strphone = $_REQUEST['frm_phone'];
			$strmobile = $_REQUEST['frm_mobile'];
			$stremail = (!empty($_REQUEST['frm_email'])) ? $_REQUEST['frm_email'] : "";
			$strfax = $_REQUEST['frm_fax'];
			$strnotes = $_REQUEST['frm_notes'];
			$strdelfirstname = (!empty($_REQUEST['delfirstname'])) ? $_REQUEST['delfirstname'] : $_REQUEST['frm_firstname'];
			$strdelsurname = (!empty($_REQUEST['delsurname'])) ? $_REQUEST['delsurname'] : $_REQUEST['frm_surname'];
			$strdeltitle = (!empty($_REQUEST['deltitle'])) ? $_REQUEST['deltitle'] : $_REQUEST['frm_title'];
			$strdeladd1 = $_REQUEST['deladd1'];
			$strdeladd2 = $_REQUEST['deladd2'];
			$strdeladd3 = $_REQUEST['deladd3'];
			$strdelTown = $_REQUEST['delTown'];
			$strdelCnty = $_REQUEST['delCnty'];
			$strdelPstCde = $_REQUEST['delPstCde'];
			$strbillfirstname = (!empty($_REQUEST['bilfirstname'])) ? $_REQUEST['bilfirstname'] : $_REQUEST['frm_firstname'];
			$strbillsurname = (!empty($_REQUEST['bilsurname'])) ? $_REQUEST['bilsurname'] : $_REQUEST['frm_surname'];
			$strbilltitle = (!empty($_REQUEST['biltitle'])) ? $_REQUEST['biltitle'] : $_REQUEST['frm_title'];
			$strbilladd1 = $_REQUEST['biladd1'];
			$strbilladd2 = $_REQUEST['biladd2'];
			$strbilladd3 = $_REQUEST['biladd3'];
			$strbillTown = $_REQUEST['bilTown'];
			$strbillCnty = $_REQUEST['bilCnty'];
			$strbillPstCde = $_REQUEST['bilPstCde'];
			$customerGroupID = $_REQUEST['frm_customerGroupID'];
			
			$intaddtype = intval($_REQUEST['addtype']);
			if (isset($_REQUEST['newsletter'])) $intnewsletter = 1;
			else $intnewsletter = 0;

			if (check_email_address($stremail) || $stremail == "")
			{
				//create account
				$createAccountQuery = "INSERT INTO customer (title, firstname, surname, company, telephone, mobile, email, fax, notes, groupID) VALUES (:title, :firstname, :surname, :company, :telephone, :mobile, :email, :fax, :notes, :groupID)";
				$strType = "insert";
				$arrdbparams = array(
									"title" => $strtitle,
									"firstname" => $strfirstname,
									"surname" => $strsurname,
									"company" => $strcompany,
									"telephone" => $strphone,
									"mobile" => $strmobile,
									"email" => $stremail,
									"fax" => $strfax,
									"notes" => $strnotes,
									"groupID" => $customerGroupID
								);
				$customerAccountID = query($conn, $createAccountQuery, $strType, $arrdbparams);
				
				if($customerAccountID)
				{	
					if($intaddtype == 1)
					{
						//create account with two addresses
						$insertAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Delivery Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
						$strType = "insert";
						$arrdbparams = array(
											"customerID" => $customerAccountID,
											"title" => $strdeltitle,
											"firstname" => $strdelfirstname,
											"surname" => $strdelsurname,
											"add1" => $strdeladd1,
											"add2" => $strdeladd2,
											"add3" => $strdeladd3,
											"town" => $strdelTown,
											"county" => $strdelCnty,
											"postcode" => $strdelPstCde
										);
						$delAddID = query($conn, $insertAddressQuery, $strType, $arrdbparams);
						
						$billAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Billing Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
						$strType = "insert";
						$arrdbparams = array(
											"customerID" => $customerAccountID,
											"title" => $strbilltitle,
											"firstname" => $strbillfirstname,
											"surname" => $strbillsurname,
											"add1" => $strbilladd1,
											"add2" => $strbilladd2,
											"add3" => $strbilladd3,
											"town" => $strbillTown,
											"county" => $strbillCnty,
											"postcode" => $strbillPstCde
										);
						$billAddID = query($conn, $billAddressQuery, $strType, $arrdbparams);
					}
					else
					{
						$insertAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Delivery Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
						$strType = "insert";
						$arrdbparams = array(
											"customerID" => $customerAccountID,
											"title" => $strdeltitle,
											"firstname" => $strdelfirstname,
											"surname" => $strdelsurname,
											"add1" => $strdeladd1,
											"add2" => $strdeladd2,
											"add3" => $strdeladd3,
											"town" => $strdelTown,
											"county" => $strdelCnty,
											"postcode" => $strdelPstCde
										);
						$delAddID = query($conn, $insertAddressQuery, $strType, $arrdbparams);
						
						$billAddID = $delAddID;
					}
					
					$values = array(
						"billingadd" => $billAddID,
						"deliveryadd" => $delAddID,
						"custid" => $customerAccountID
					);
					
					$assignCustomerAddresses = updateUserBillDelAddresses($values, "billanddel", $conn);
					
					if($intnewsletter == 1)
					{
						$subscriptionCheck = getNewsletterSubscription($stremail, "byemail", $conn);
						
						if(empty($subscriptionCheck))
						{
							$name = $strtitle." ".$strfirstname." ".$strsurname;
							$addSubscription = insertNewsletterSubscription($customerAccountID, $name, $stremail, $conn);
						}
					}
					$userID = $customerAccountID;
					$strsuccess = "Successfully created a new customer.";
					$strcmd = "viewDetails";

				} else {
					$strerror = "There was a problem creating the customer please try again.";
					$strcmd = "customerAdd";
				}
			}
			else
			{
				$strerror = "Sorry - Invalid Email Address - please re-enter";
				$strcmd = "customerAdd";
			}
		break;
		case "updateCustomer":
			
			$customerAccountID = $_REQUEST['userID'];
			$strfirstname = $_REQUEST['frm_firstname'];
			$strsurname = $_REQUEST['frm_surname'];
			$strtitle = $_REQUEST['frm_title'];
			$strcompany = $_REQUEST['frm_company'];
			$strphone = $_REQUEST['frm_phone'];
			$strmobile = $_REQUEST['frm_mobile'];
			$stremail = (!empty($_REQUEST['frm_email'])) ? $_REQUEST['frm_email'] : "";
			$strfax = $_REQUEST['frm_fax'];
			$strnotes = $_REQUEST['frm_notes'];
			$strdelfirstname = (!empty($_REQUEST['delfirstname'])) ? $_REQUEST['delfirstname'] : $_REQUEST['frm_firstname'];
			$strdelsurname = (!empty($_REQUEST['delsurname'])) ? $_REQUEST['delsurname'] : $_REQUEST['frm_surname'];
			$strdeltitle = (!empty($_REQUEST['deltitle'])) ? $_REQUEST['deltitle'] : $_REQUEST['frm_title'];
			$strdeladd1 = $_REQUEST['deladd1'];
			$strdeladd2 = $_REQUEST['deladd2'];
			$strdeladd3 = $_REQUEST['deladd3'];
			$strdelTown = $_REQUEST['delTown'];
			$strdelCnty = $_REQUEST['delCnty'];
			$strdelPstCde = $_REQUEST['delPstCde'];
			$strbillfirstname = (!empty($_REQUEST['bilfirstname'])) ? $_REQUEST['bilfirstname'] : $_REQUEST['frm_firstname'];
			$strbillsurname = (!empty($_REQUEST['bilsurname'])) ? $_REQUEST['bilsurname'] : $_REQUEST['frm_surname'];
			$strbilltitle = (!empty($_REQUEST['biltitle'])) ? $_REQUEST['biltitle'] : $_REQUEST['frm_title'];
			$strbilladd1 = $_REQUEST['biladd1'];
			$strbilladd2 = $_REQUEST['biladd2'];
			$strbilladd3 = $_REQUEST['biladd3'];
			$strbillTown = $_REQUEST['bilTown'];
			$strbillCnty = $_REQUEST['bilCnty'];
			$strbillPstCde = $_REQUEST['bilPstCde'];
			$customerGroupID = $_REQUEST['frm_customerGroupID'];
			
			$intaddtype = intval($_REQUEST['addtype']);
			if (isset($_REQUEST['newsletter'])) $intnewsletter = 1;
			else $intnewsletter = 0;

			if (check_email_address($stremail) || $stremail == "")
			{
				//get current customer details
				$createAccountQuery = "SELECT * FROM customer WHERE recordID = :recordID";
				$strType = "single";
				$arrdbparams = array("recordID"=>$customerAccountID);
				$customerDetails = query($conn, $createAccountQuery, $strType, $arrdbparams);
			
				//update customer
				if(!empty($customerDetails['recordID'])){
					$createAccountQuery = "UPDATE customer SET title = :title, firstname = :firstname, surname = :surname, company = :company, telephone = :telephone, mobile = :mobile, email = :email, fax = :fax, notes = :notes, groupID = :groupID WHERE recordID = :recordID";
					$strType = "insert";
					$arrdbparams = array("title" => $strtitle, "firstname" => $strfirstname, "surname" => $strsurname, "company" => $strcompany, "telephone" => $strphone, "mobile" => $strmobile, "email" => $stremail, "fax" => $strfax, "notes" => $strnotes, "recordID"=>$customerAccountID, "groupID"=>$customerGroupID);
					$updateResult = query($conn, $createAccountQuery, $strType, $arrdbparams);
				}
				
				//update address details - 
				if(!empty($customerDetails['defaultDeliveryAdd']) AND !empty($customerDetails['defaultBillingAdd']))
				{	
					if($intaddtype == 1) //update account with two addresses - may need to insert another
					{
						
						//update the delivery address
						$insertAddressQuery = "UPDATE customer_address SET title = :title, firstname = :firstname, surname = :surname, add1 = :add1, add2 = :add2, add3 = :add3, town = :town, county = :county, postcode = :postcode WHERE recordID = :recordID";
						$strType = "update";
						$arrdbparams = array("recordID" => $customerDetails['defaultDeliveryAdd'], "title" => $strdeltitle, "firstname" => $strdelfirstname, "surname" => $strdelsurname, "add1" => $strdeladd1, "add2" => $strdeladd2, "add3" => $strdeladd3, "town" => $strdelTown, "county" => $strdelCnty, "postcode" => $strdelPstCde);
						$updateDetails = query($conn, $insertAddressQuery, $strType, $arrdbparams);
						$delAddID = $customerDetails['defaultDeliveryAdd'];
						
						//insert or update the billing address
						if ($customerDetails['defaultDeliveryAdd'] == $customerDetails['defaultBillingAdd']) { //current address the same for each so insert new billing
							
							$billAddressQuery = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, postcode) VALUES (:customerID, 'Billing Address', :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode)";
							$strType = "insert";
							$arrdbparams = array( "customerID" => $customerAccountID, "title" => $strbilltitle, "firstname" => $strbillfirstname, "surname" => $strbillsurname, "add1" => $strbilladd1, "add2" => $strbilladd2, "add3" => $strbilladd3, "town" => $strbillTown, "county" => $strbillCnty, "postcode" => $strbillPstCde);
							$billAddID = query($conn, $billAddressQuery, $strType, $arrdbparams);
						} else {
							
							$insertAddressQuery = "UPDATE customer_address SET title = :title, firstname = :firstname, surname = :surname, add1 = :add1, add2 = :add2, add3 = :add3, town = :town, county = :county, postcode = :postcode WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparams = array("recordID" => $customerDetails['defaultBillingAdd'], "title" => $strbilltitle, "firstname" => $strbillfirstname, "surname" => $strbillsurname, "add1" => $strbilladd1, "add2" => $strbilladd2, "add3" => $strbilladd3, "town" => $strbillTown, "county" => $strbillCnty, "postcode" => $strbillPstCde);
							$updateDetails = query($conn, $insertAddressQuery, $strType, $arrdbparams);
							$billAddID = $customerDetails['defaultBillingAdd'];
						}
					}
					else //1 address
					{
						//update the delivery address and set it to be the default for both
						$insertAddressQuery = "UPDATE customer_address SET title = :title, firstname = :firstname, surname = :surname, add1 = :add1, add2 = :add2, add3 = :add3, town = :town, county = :county, postcode = :postcode WHERE recordID = :recordID";
						$strType = "update";
						$arrdbparams = array("recordID" => $customerDetails['defaultDeliveryAdd'], "title" => $strdeltitle, "firstname" => $strdelfirstname, "surname" => $strdelsurname, "add1" => $strdeladd1, "add2" => $strdeladd2, "add3" => $strdeladd3, "town" => $strdelTown, "county" => $strdelCnty, "postcode" => $strdelPstCde);
						$updateDetails = query($conn, $insertAddressQuery, $strType, $arrdbparams);
						$delAddID = $customerDetails['defaultDeliveryAdd'];
						$billAddID = $customerDetails['defaultDeliveryAdd'];
						
						//delete previous billing address
						if ($customerDetails['defaultDeliveryAdd'] != $customerDetails['defaultBillingAdd']) {
							$insertAddressQuery = "DELETE FROM customer_address WHERE recordID = :recordID";
							$strType = "delete";
							$arrdbparams = array("recordID" => $customerDetails['defaultBillingAdd']);
							$deleteQuery = query($conn, $insertAddressQuery, $strType, $arrdbparams);
						}
						
						$delAddID = $customerDetails['defaultDeliveryAdd'];
						$billAddID = $customerDetails['defaultDeliveryAdd'];
					}
					
					$values = array("billingadd" => $billAddID,"deliveryadd" => $delAddID,"custid" => $customerAccountID);
					$assignCustomerAddresses = updateUserBillDelAddresses($values, "billanddel", $conn);
					$strsuccess = "Successfully updated customer.";
					$strcmd = "viewDetails";

				} else {
					$strerror = "There was a problem updating the customer please try again.";
					$strcmd = "viewDetails";
				}
				
			} else {
				$strerror = "Sorry - Invalid Email Address - please re-enter";
				$strcmd = "viewDetails";
			}
		break;
		case "customerDelete":
			$customerAccountID = $_REQUEST['userID'];
			
			$createAccountQuery = "SELECT * FROM customer WHERE recordID = :recordID";
			$strType = "single";
			$arrdbparams = array("recordID"=>$customerAccountID);
			$customerDetails = query($conn, $createAccountQuery, $strType, $arrdbparams);
			
			if (!empty($customerDetails['recordID'])) {
			
				$insertAddressQuery = "DELETE FROM customer_address WHERE recordID = :recordID";
				$strType = "delete";
				$arrdbparams = array("recordID" => $customerDetails['defaultDeliveryAdd']);
				$deleteQuery = query($conn, $insertAddressQuery, $strType, $arrdbparams);
								
				$insertAddressQuery = "DELETE FROM customer_address WHERE recordID = :recordID";
				$strType = "delete";
				$arrdbparams = array("recordID"=>$customerDetails['defaultBillingAdd']);
				$deleteQuery = query($conn, $insertAddressQuery, $strType, $arrdbparams);
				
				$insertAddressQuery = "DELETE FROM order_header WHERE customerID = :customerID";
				$strType = "delete";
				$arrdbparams = array("customerID"=>$customerAccountID);
				$deleteQuery = query($conn, $insertAddressQuery, $strType, $arrdbparams);
				
				$insertAddressQuery = "DELETE FROM customer WHERE recordID = :customerID";
				$strType = "delete";
				$arrdbparams = array("customerID"=>$customerAccountID);
				$deleteQuery = query($conn, $insertAddressQuery, $strType, $arrdbparams);
			}
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Customer Manage</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsuserSearch() {
					document.form.cmd.value='userSearch';
					document.form.submit();
				}
				function jsCustomerAdd() {
					document.form.cmd.value='customerAdd';
					document.form.submit();
				}
				function jsCustomerUpdate() {
					document.form.cmd.value='updateCustomer';
					document.form.submit();
				}
				function jsorderHistory(userID) {
					document.form.userID.value = userID;
					document.form.action="sales-orders.php";
					document.form.cmd.value = "";
					document.form.submit();
				}
				function jsModifyCustomer(userID) {
					document.form.userID.value = userID;
					document.form.cmd.value = "customerEdit";
					document.form.submit();
				}
				function jsfullDetails(userID) {
					document.form.userID.value = userID;
					document.form.cmd.value='viewDetails';
					document.form.submit();
				}
				function jsupdateList() {
					var e = document.getElementById("user");
					var selected = e.options[e.selectedIndex].value;
					document.form.userID.value = e.options[e.selectedIndex].value;
					if (selected != "all" && selected != "recent")
					{
						document.form.cmd.value='viewDetails';
					}
					else
					{
						document.form.userID.value='';
						document.form.cmd.value='';
					}
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.userID.value='';
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				onload=function(){
					if (document.getElementsByClass == undefined) {
						document.getElementsByClass = function(className)
						{
							var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
							var allElements = document.getElementsByTagName("*");
							var results = [];

							var element;
							for (var i = 0; (element = allElements[i]) != null; i++) {
								var elementClass = element.className;
								if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
									results.push(element);
							}

							return results;
						}
					}
				}
				function jsaddtype(el){
					if (el.value == 1) {
						document.getElementById("biltable").style.display = 'block';
						var element = document.getElementById('deltable-hidden');
						if(element != null) element.id = 'deltable-show';
						document.getElementById("addLegend").innerHTML = "Delivery Address";
					} else {
						document.getElementById("biltable").style.display = 'none';
						var element = document.getElementById('deltable-show');
						if(element != null) element.id = 'deltable-hidden';
						document.getElementById("addLegend").innerHTML = "Address";
					}
				}
				function jscreateaccount(){
						
						var form = document.getElementById('form');
						
						var highlandsnislnds = new Array("GY", "BT", "IM", "HS", "ZE", "JE");
						var highlandsnislnds2 = new Array("IV41", "IV42", "IV43", "IV44", "IV45", "IV46", "IV47", "IV48", "IV49", "IV50", "IV51", "IV52", "IV53", "IV54", "IV55", "IV56", "KA27", "KA28", "PA20", "PA41", "PA42", "PA43", "PA44", "PA45", "PA46", "PA47", "PA48", "PA49", "PA62", "PA63", "PA64", "PA65", "PA66", "PA67", "PA68", "PA69", "PA70", "PA71", "PA72", "PA73", "PA74", "PA75", "PA76", "PH42", "PH43", "PH44", "PO31", "PO32","PO33","PO34","PO35","PO36", "PO37", "PO38", "PO39","PO40", "PO41");

						if (form.addtype[0].checked == true) {
							if (form.frm_firstname.value == "") {alert("Please enter your First Name.");}
							else if (form.frm_surname.value == "") {alert("Please enter your Surname.");}
							//else if (form.frm_phone.value == "") {alert("Please enter a Phone Number.");}
							//else if (form.frm_email.value == "") {alert("Please enter a valid Email Address.");}
							else if (form.deladd1.value == "") {alert("Please enter the Delivery Address.");}
							else if (form.delCnty.value == "") {alert("Please enter the Delivery Address County.");}
							else if (form.delPstCde.value == "") {alert("Please enter the Delivery Address Post Code.");}
							else {
								var postcode = form.delPstCde.value;

								/*var pcode = postcode.slice(0,2);
								pcode = pcode.toUpperCase();
								for (var i = 0; i <highlandsnislnds.length; i = i+ 1)
								{
									if (pcode == highlandsnislnds[i])
									{
										alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
								}

								var pcode2 = postcode.slice(0,4);
								pcode2 = pcode2.toUpperCase();
								for (var i = 0; i <highlandsnislnds2.length; i = i+ 1)
								{
									if (pcode2 == highlandsnislnds2[i])
									{
										alert ("Your postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
								}*/

								postcode = postcode.replace(" ", "");
								postcode = postcode.toUpperCase();
								if (postcode.length < 5 || postcode > 7) {alert("The postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
								else if (postcode.charCodeAt(postcode.length-1) < 65 || postcode.charCodeAt(postcode.length-1) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
								else if (postcode.charCodeAt(postcode.length-2) < 65 || postcode.charCodeAt(postcode.length-2) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
								else if (postcode.charCodeAt(postcode.length-3) < 48 || postcode.charCodeAt(postcode.length-3) > 57) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
								else {
									form.cmd.value = "createCustomer";
									return true;
								}
							}
						} else if (form.addtype[1].checked == true) {
							if (form.frm_firstname.value == "") {alert("Please enter your account First Name.");}
							else if (form.frm_surname.value == "") {alert("Please enter your account Surname.");}
							//else if (form.frm_phone.value == "") {alert("Please enter a Phone Number.");}
							else if (form.frm_email.value != "" && form.frm_cemail == "") {alert("Please confirm the Email Address.");}
							//else if (form.delfirstname.value == "") {alert("Please enter your Delivery AddressFirst Name.");}
							//else if (form.delsurname.value == "") {alert("Please enter your Delivery Address Surname.");}
							else if (form.deladd1.value == "") {alert("Please enter the first line of Delivery Address.");}
							else if (form.delCnty.value == "") {alert("Please enter the Delivery Address County.");}
							else if (form.delPstCde.value == "") {alert("Please enter the Delivery Address Post Code.");}
							//else if (form.bilfirstname.value == "") {alert("Please enter your Billing Address First Name.");}
							//else if (form.bilsurname.value == "") {alert("Please enter your Billing Address Surname.");}
							else if (form.biladd1.value == "") {alert("Please enter the first line of your Billing Address.");}
							else if (form.bilCnty.value == "") {alert("Please enter the Billing Address County.");}
							else if (form.bilPstCde.value == "") {alert("Please enter the Billing Address Post Code.");}
							else {
								var postcode = form.delPstCde.value;
								var postcode2 = form.bilPstCde.value;

								var pcode = postcode.slice(0,2);
								pcode = pcode.toUpperCase();
								var pcode3 = postcode2.slice(0,2);
								pcode3 = pcode3.toUpperCase();
								for (var i = 0; i <highlandsnislnds.length; i = i+ 1)
								{
									if (pcode == highlandsnislnds[i])
									{
										alert ("Your delivery postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
									if (pcode3 == highlandsnislnds[i])
									{
										alert ("Your billing postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
								}

								var pcode2 = postcode.slice(0,4);
								pcode2 = pcode2.toUpperCase();
								var pcode4 = postcode2.slice(0,4);
								pcode4 = pcode4.toUpperCase();
								for (var i = 0; i <highlandsnislnds2.length; i = i+ 1)
								{
									if (pcode2 == highlandsnislnds2[i])
									{
										alert ("Your delivery postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
									if (pcode4 == highlandsnislnds2[i])
									{
										alert ("Your billing postcode is in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.");
										return false;
									}
								}

								postcode = postcode.replace(" ", "");
								postcode = postcode.toUpperCase();
								if (postcode.length < 5 || postcode > 7) {alert("The postcode you have entered does not appear to be valid, it should be between 5 and 7 characters long.");}
								else if (postcode.charCodeAt(postcode.length-1) < 65 || postcode.charCodeAt(postcode.length-1) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
								else if (postcode.charCodeAt(postcode.length-2) < 65 || postcode.charCodeAt(postcode.length-2) > 90) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
								else if (postcode.charCodeAt(postcode.length-3) < 48 || postcode.charCodeAt(postcode.length-3) > 57) {alert ("The postcode you have entered does not appear to be valid. Your postcode should contain at least two numbers and three letters with an optional space before the last three characters.");}
								else {
									form.cmd.value = "createCustomer";
									return true
								}
							}
						}
						return false;
				}
				
				/*function jsfindaddress (type){
					var listID = "ajax_address"+type;
					
					document.getElementById(listID).style.display = "block";
					var postcode = document.getElementById(type+"PstCde").value;
					var request = "/ajax/ajaxaddress.php";
					var vars = "postcode="+postcode;
					var xmlhttp;
					if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp=new XMLHttpRequest();
					} else {// code for IE6, IE5
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					}

					if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

					xmlhttp.open("POST",request,true);
					xmlhttp.onreadystatechange=function() {
						if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							var xml = xmlhttp.responseXML;
							if(xml.getElementsByTagName("ErrorNumber")[0].childNodes[0].nodeValue == 0) {
								if(xml.getElementsByTagName("Address2").length != 0)document.getElementById(type+"add2").value= xml.getElementsByTagName("Address2")[0].childNodes[0].nodeValue;
								if(xml.getElementsByTagName("Address3").length != 0)document.getElementById(type+"add3").value= xml.getElementsByTagName("Address3")[0].childNodes[0].nodeValue;
								document.getElementById(type+"Town").value= xml.getElementsByTagName("Town")[0].childNodes[0].nodeValue;
								document.getElementById(type+"Cnty").value= xml.getElementsByTagName("County")[0].childNodes[0].nodeValue;
								var resultlist =  xml.getElementsByTagName("PremiseData")[0].childNodes[0].nodeValue;
								var arrlist = resultlist.split(";");
								var addlist = "<ul >";
								for (var i = 0; i < arrlist.length; i++) {
									if(arrlist[i] != "") {
										arrlist[i] = arrlist[i].replace("||","|");
										arrlist[i] = arrlist[i].replace("|",", ");
										arrlist[i] = arrlist[i].replace("|",", ");
										var straddress =arrlist[i]+" "+xml.getElementsByTagName("Address1")[0].childNodes[0].nodeValue;
										straddress = straddress.replace(/(^,)|(,$)/g, "");
										straddress = straddress.replace(/(^ )|(,$)/g, "");
										addlist += "<li><a href='javascript:jsaddsel(\""+straddress+"\", \""+type+"\")'>"+straddress+"</a></li>";
									}
								}
								addlist += "</ul>";
								document.getElementById(listID).innerHTML = addlist;

							} else {
								document.getElementById(listID).innerHTML = "<span class='notification-warning'>"+xml.getElementsByTagName("ErrorMessage")[0].childNodes[0].nodeValue+"</span>";
							}

						} else if (xmlhttp.readyState==4) {
							document.getElementById(listID).innerHTML = " <span class='notification-warning'>There has been an error updating your address please try again.</span> ";
						}
					}
					xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
					xmlhttp.send(vars);
					document.getElementById(listID).innerHTML = " <img src='img/ajax-loader.gif' /> <span> Search...</span> ";	
				}*/
				/*function jsaddsel(address, type){
					document.getElementById(type+"add1").value= address;
					document.getElementById("ajax_address"+type).style.display = "none";
				}*/
			
			
				function jsPostcodeLookup(postcodeType) {
					switch (postcodeType){
						case "bil":
							postcode = document.getElementById("bilPstCde").value;
							returnLocation = "ajax_addressbil";
						break;
						case "del":
							postcode = document.getElementById("delPstCde").value;
							returnLocation = "ajax_addressdel";
						break;
						case "delivery":
							postcode = document.getElementById("delPstCde").value;
							returnLocation = "ajax_addressDel";
						break;
						case "billing":
							postcode = document.getElementById("bilPstCde").value;
							returnLocation = "ajax_addressBill";
						break;
					}
					jsfindaddress(postcode,returnLocation);
				}
				
				function jsDeleteMember(userID){
					var r = confirm("Are you sure you want to delete this customer?");
					if (r == true) {
						document.form.userID.value = userID;
						document.form.cmd.value = "customerDelete";
						document.form.submit();
					}
				}
			
			</script>
			<?php
		
			print("<form action='customer-manage.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='userID' id='userID' value='".$userID."'/>");
				
				switch($strcmd)
				{
					case "customerAdd":
											
							$strfirstname = $_REQUEST['frm_firstname'];
							$strsurname = $_REQUEST['frm_surname'];
							$strtitle = $_REQUEST['frm_title'];
							$strcompany = $_REQUEST['frm_company'];
							$strphone = $_REQUEST['frm_phone'];
							$strmobile = $_REQUEST['frm_mobile'];
							$stremail = $_REQUEST['frm_email'];
							$strfax = $_REQUEST['frm_fax'];
							$strnotes = $_REQUEST['frm_notes'];
							$strdelfirstname = (!empty($_REQUEST['delfirstname'])) ? $_REQUEST['delfirstname'] : $_REQUEST['frm_firstname'];
							$strdelsurname = (!empty($_REQUEST['delsurname'])) ? $_REQUEST['delsurname'] : $_REQUEST['frm_surname'];
							$strdeltitle = (!empty($_REQUEST['deltitle'])) ? $_REQUEST['deltitle'] : $_REQUEST['frm_title'];
							$strdeladd1 = $_REQUEST['deladd1'];
							$strdeladd2 = $_REQUEST['deladd2'];
							$strdeladd3 = $_REQUEST['deladd3'];
							$strdelTown = $_REQUEST['delTown'];
							$strdelCnty = $_REQUEST['delCnty'];
							$strdelPstCde = $_REQUEST['delPstCde'];
							$strbillfirstname = (!empty($_REQUEST['bilfirstname'])) ? $_REQUEST['bilfirstname'] : $_REQUEST['frm_firstname'];
							$strbillsurname = (!empty($_REQUEST['bilsurname'])) ? $_REQUEST['bilsurname'] : $_REQUEST['frm_surname'];
							$strbilltitle = (!empty($_REQUEST['biltitle'])) ? $_REQUEST['biltitle'] : $_REQUEST['frm_title'];
							$strbilladd1 = $_REQUEST['biladd1'];
							$strbilladd2 = $_REQUEST['biladd2'];
							$strbilladd3 = $_REQUEST['biladd3'];
							$strbillTown = $_REQUEST['bilTown'];
							$strbillCnty = $_REQUEST['bilCnty'];
							$strbillPstCde = $_REQUEST['bilPstCde'];
							$intaddtype = intval($_REQUEST['addtype']);
							$customerGroupID = $_REQUEST['frm_customerGroupID'];
							if ($_REQUEST['newsletter']) $intnewsletter = 1;
							else $intnewsletter = 0;
							
							print("<div class='row'>");
								print("<div class='col-md-6 split even-left'>");
									print("<div class='section'>");
							
										print("<div class='box-light' id='delivery'>");
											print("<fieldset class='inlineLabels'>");
												print("<legend>Customer Details</legend>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='frm_title' name='frm_title' type='text' placeholder='Title' value='$strtitle' />");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='frm_firstname' name='frm_firstname' type='text' placeholder='First Name' value='$strfirstname' />");
														print("<font size='1' color='red'>*</font>");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='frm_surname' name='frm_surname' type='text' placeholder='Surname' value='$strsurname' />");
														print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");

														print("<input class='form-control' id='frm_company' type='hidden' name='frm_company' placeholder='Company' value='$strcompany' />");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' type='text' id='frm_phone' name='frm_phone' placeholder='Phone Number' value='$strphone' />");
														//print("<font size='1' color='red'>*</font>");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' type='text' id='frm_mobile' name='frm_mobile' placeholder='Mobile' value='$strmobile' />");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' type='text' id='frm_email' name='frm_email' placeholder='Email' value='$stremail' />");
														//print("<font size='1' color='red'>*</font>");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' type='text' id='frm_cemail' name='frm_cemail' placeholder='Confirm Email' value='$strcemail' />");
														//print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														
														$getCustomerGroupsQuery = "SELECT * FROM customer_group";
														$strType = "multi";
														$customerGroups = query($conn, $getCustomerGroupsQuery, $strType);
														
														print ("<select id='frm_customerGroupID' name='frm_customerGroupID' class='form-control' >");
														foreach ($customerGroups AS $row)
														{	
															if ($customerGroupID == $row['recordID']) $selected = "selected"; else $selected = "";
															print("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
														}
														print("</select>");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' type='hidden' id='frm_fax' name='frm_fax' placeholder='Fax Number' value='$strfax' />");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<textarea style='max-width:100%;max-height:500px;' class='form-control' id='frm_notes' name='frm_notes' rows='8' columns='30' placeholder='Notes' >$strnotes</textarea>");
													print("</div>");
												print("</div>");
												
											print("</fieldset>");
										print("</div>");
							
										print("<div class='box-light' id='address-types'>");
											print("<table style='width:100%;'>");
												print("<tr><td>");
													print("<input type='radio' value='0' name='addtype' id='singleaddress' onclick='jsaddtype(this)' ");
														if($intaddtype == 0) {print("checked='checked'");$strclass='-hidden';}
													print("/><label for='singleaddress' style='font-family:arial;font-size:12px;vertical-align:top;margin-left:15px;width:210px;'>Use a single address for billing and delivery</label>");
													
													print("<input type='radio' value='1' name='addtype' id='multiaddress' onclick='jsaddtype(this)' style='width:auto;'");
														if($intaddtype == 1) {print("checked='checked'");$strclass='-show';}
													print("/><label for='multiaddress' style='font-family:arial;font-size:12px;vertical-align:top;margin-left:15px;width:210px;'>Use separate billing and delivery addresses</label>");
												print("</td></tr>");
											print("</table>");
										print("</div>");
										
									print("</div>");
								print("</div>");
								
								print("<div class='col-md-6 split even-right'>");
									print("<div class='section'>");
					
										print("<div class='box-light' id='deltable".$strclass."'>");
											print("<fieldset class='inlineLabels'>");
												print("<legend id='addLegend'>Address</legend>");
									
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='deltitle' name='deltitle' type='text' placeholder='Title' value='$strdeltitle' />");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='delfirstname' name='delfirstname' type='text' placeholder='First Name' value='$strdelfirstname' />");
														//print("<font size='1' color='red'>*</font>");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='delsurname' name='delsurname' type='text' placeholder='Surname' value='$strdelsurname' />");
														//print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='delPstCde' class='postCode' name='delPstCde' type='text' placeholder='Post Code' value='$strdelPstCde' />");
														print("<font size='1' color='red'>*</font>");
														print("<a class='postCode-icon' href='Javascript:jsPostcodeLookup(\"delivery\")'><i class='fa fa-search'></i></a>");
														print("<div id='ajax_addressDel'></div>");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='deladd1' placeholder='Address 1' name='deladd1' type='text' value='$strdeladd1' />");
														print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='deladd2' placeholder='Address 2' name='deladd2' type='text' value='$strdeladd2' />");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='deladd3' placeholder='Address 3' name='deladd3' type='text' value='$strdeladd3' />");
													print("</div>");
												print("</div>");
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='delTown' placeholder='Town' name='delTown' type='text' value='$strdelTown' />");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='delCnty' placeholder='County' name='delCnty' type='text' value='$strdelCnty' />");
														print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
											
											print("</fieldset>");
										print("</div>");
										
									print("</div>");
									print("<div class='box-light multi".$strclass."' id='biltable' style='display:none;'>");
										print("<div class='section'>");
											print("<fieldset class='inlineLabels'>");
												print("<legend>Billing Address</legend>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='biltitle' name='biltitle' type='text' placeholder='Title' value='$strbiltitle' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='bilfirstname' name='bilfirstname' type='text' placeholder='First Name' value='$strbilfirstname' />");
														//print("<font size='1' color='red'>*</font>");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='bilsurname' name='bilsurname' type='text' placeholder='Surname' value='$strbilsurname' />");
														//print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='bilPstCde' class='postCode' name='bilPstCde' type='text' placeholder='Post Code' value='$strbilPstCde' />");
														print("<font size='1' color='red'>*</font>");
														print("<a class='postCode-icon' href='Javascript:jsPostcodeLookup(\"billing\")'><i class='fa fa-search'></i></a>");
														print("<div id='ajax_addressBill'></div>");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='biladd1' placeholder='Address 1' name='biladd1' type='text' placeholder='Address 1' value='$strbiladd1' />");
														print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='biladd2' placeholder='Address 2' name='biladd2' type='text' placeholder='Address 2' value='$strbiladd2' />");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='biladd3' placeholder='Address 3' name='biladd3' type='text' placeholder='Address 3' value='$strbiladd3' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='bilTown' placeholder='Town' name='bilTown' type='text' placeholder='Town' value='$strbilTown' />");
													print("</div>");
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='bilCnty' placeholder='Country' name='bilCnty' type='text' placeholder='County' value='$strbilCnty' />");
														print("<font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");

											print("</fieldset>");
										print("</div>");
									print("</div>");
									
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-6' style='text-align:left;'>");
									print("<button onclick='return jsCancel();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-6' style='text-align:right;'>");
									print("<button onclick='return jscreateaccount(this);' type='submit' class='btn btn-success' style='display:inline-block;'>Create</button> ");
									//print("<button onclick='return jscreateaccount(this);' type='submit' class='btn btn-success' style='display:inline-block;'>Create</button> ");
								print("</div>");
							print("</div>");
						
					break;
				
					case "customerEdit":
										
							$customerDetailsQuery = "SELECT * FROM customer WHERE recordID = :userID";
							$strType = "single";
							$arrdbparam = array( "userID" => $userID );
							$customerDetails = query($conn, $customerDetailsQuery, $strType, $arrdbparam);
							
							if (count($customerDetails) < 1)
							{
								$strerror = "Matching customer not found";
							}
							else
							{
								$strfirstname = $customerDetails['firstname'];
								$strsurname = $customerDetails['surname'];
								$strtitle = $customerDetails['title'];
								$strcompany = $customerDetails['company'];
								$strphone = $customerDetails['telephone'];
								$strmobile = $customerDetails['mobile'];
								$stremail = $customerDetails['email'];
								$strcemail = $customerDetails['email'];
								$strfax = $customerDetails['fax'];
								$strnotes = $customerDetails['notes'];
								$customerGroupID = $customerDetails['groupID'];
								
								$customerDetailsQuery = "SELECT * FROM customer_address WHERE recordID = :recordID";
								$strType = "single";
								$arrdbparam = array("recordID"=>$customerDetails['defaultDeliveryAdd']);
								$addressDetails = query($conn, $customerDetailsQuery, $strType, $arrdbparam);
								
								$strdelfirstname = $addressDetails['firstname'];
								$strdelsurname = $addressDetails['surname'];
								$strdeltitle = $addressDetails['title'];
								$strdeladd1 = $addressDetails['add1'];
								$strdeladd2 = $addressDetails['add2'];
								$strdeladd3 = $addressDetails['add3'];
								$strdelTown = $addressDetails['town'];
								$strdelCnty = $addressDetails['county'];
								$strdelPstCde = $addressDetails['postcode'];
								
								$customerDetailsQuery = "SELECT * FROM customer_address WHERE recordID = :recordID";
								$strType = "single";
								$arrdbparam = array("recordID"=>$customerDetails['defaultBillingAdd']);
								$addressDetails = query($conn, $customerDetailsQuery, $strType, $arrdbparam);
								
								$strbilfirstname = $addressDetails['firstname'];
								$strbilsurname = $addressDetails['surname'];
								$strbiltitle = $addressDetails['title'];
								$strbiladd1 = $addressDetails['add1'];
								$strbiladd2 = $addressDetails['add2'];
								$strbiladd3 = $addressDetails['add3'];
								$strbilTown = $addressDetails['town'];
								$strbilCnty = $addressDetails['county'];
								$strbilPstCde = $addressDetails['postcode'];
								
								if ($customerDetails['defaultDeliveryAdd'] == $customerDetails['defaultBillingAdd']) $intaddtype = 0;
								else $intaddtype = 1;
							}
							
							print("<div class='row'>");
								print("<div class='col-md-6 split even-left'>");
									print("<div class='section'>");
							
										print("<div class='box-light' id='delivery'>");
											print("<fieldset class='inlineLabels'>");
												print("<legend>Customer Details</legend>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='frm_title' name='frm_title' type='text' placeholder='Title' value='$strtitle' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control required' id='frm_firstname' name='frm_firstname' type='text' placeholder='First Name' value='$strfirstname' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='frm_surname' name='frm_surname' type='text' placeholder='Surname' value='$strsurname' /><font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='frm_company' type='hidden' name='frm_company' placeholder='Company' value='$strcompany' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' type='text' id='frm_phone' name='frm_phone' placeholder='Phone Number' value='$strphone' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' type='text' id='frm_mobile' name='frm_mobile' placeholder='Mobile' value='$strmobile' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' type='text' id='frm_email' name='frm_email' placeholder='Email' value='$stremail' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' type='text' id='frm_cemail' name='frm_cemail' placeholder='Confirm Email' value='$strcemail' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														
														$getCustomerGroupsQuery = "SELECT * FROM customer_group";
														$strType = "multi";
														$customerGroups = query($conn, $getCustomerGroupsQuery, $strType);
														
														print ("<select id='frm_customerGroupID' name='frm_customerGroupID' class='form-control' >");
														foreach ($customerGroups AS $row)
														{	
															if ($customerGroupID == $row['recordID']) $selected = "selected"; else $selected = "";
															print("<option $selected value='".$row['recordID']."'>".$row['description']."</option>");
														}
														print("</select>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' type='hidden' id='frm_fax' name='frm_fax' placeholder='Fax Number' value='$strfax' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<textarea class='form-control' id='frm_notes' name='frm_notes' rows='8' columns='30' style='resize:none;' placeholder='Notes' >$strnotes</textarea>");
													print("</div>");
												print("</div>");
												
											print("</fieldset>");
										print("</div>");
							
										print("<div class='box-light' id='address-types'>");
											print("<table style='width:100%;'>");
												print("<tr><td>");
													print("<input type='radio' value='0' name='addtype' id='singleaddress' onclick='jsaddtype(this)' ");
														if($intaddtype == 0) {print("checked='checked'");$strclass='-hidden';}
													print("/><label for='singleaddress' style='font-family:arial;font-size:12px;vertical-align:top;margin-left:15px;width:210px;'>Use a single address for billing and delivery</label>");
													
													print("<input type='radio' value='1' name='addtype' id='multiaddress' onclick='jsaddtype(this)' style='width:auto;'");
														if($intaddtype == 1) {print("checked='checked'");$strclass='-show';}
													print("/><label for='multiaddress' style='font-family:arial;font-size:12px;vertical-align:top;margin-left:15px;width:210px;'>Use separate billing and delivery addresses</label>");
												print("</td></tr>");
											print("</table>");
										print("</div>");
										
									print("</div>");
								print("</div>");
								print("<div class='col-md-6 split even-left'>");

									print("<div class='box-light' id='deltable".$strclass."'>");
										print("<div class='section'>");
											print("<fieldset>");
												print("<legend id='addLegend'>Address</legend>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='deltitle' name='deltitle' type='text' placeholder='Title' value='$strdeltitle' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='delfirstname' name='delfirstname' type='text' placeholder='First Name' value='$strdelfirstname' /><font size='1' color='red'>*</font>");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='delsurname' name='delsurname' type='text' placeholder='Surname' value='$strdelsurname' /><font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='delPstCde' class='postCode' name='delPstCde' type='text' size='23' placeholder='Post Code' value='$strdelPstCde' /><font size='1' color='red'>*</font><a class='postCode-icon' href='Javascript:jsPostcodeLookup(\"delivery\")'><i class='fa fa-search'></i></a>");
														print("<div id='ajax_addressDel'></div>");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='deladd1' placeholder='Address 1' name='deladd1' type='text' value='$strdeladd1' /><font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='deladd2' placeholder='Address 2' name='deladd2' type='text' value='$strdeladd2' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='deladd3' placeholder='Address 3' name='deladd3' type='text' value='$strdeladd3' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='delTown' placeholder='Town' name='delTown' type='text' value='$strdelTown' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='delCnty' placeholder='County' name='delCnty' type='text' value='$strdelCnty' /><font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
											print("</fieldset>");
										print("</div>");
									print("</div>");
										
									if($intaddtype == 0) {$strdisplay='none';} else {$strdisplay='block';}
												
									print("<div class='box-light multi".$strclass."' id='biltable' style='display:$strdisplay;'>");
										print("<div class='section'>");
											print("<fieldset>");
												print("<legend>Billing Address</legend>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='biltitle' name='biltitle' type='text' placeholder='Title' value='$strbiltitle' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='bilfirstname' name='bilfirstname' type='text' placeholder='First Name' value='$strbilfirstname' /><font size='1' color='red'>*</font>");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='bilsurname' name='bilsurname' type='text' placeholder='Surname' value='$strbilsurname' /><font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6 even-left'>");
														print("<input class='form-control' id='bilPstCde' class='postCode' name='bilPstCde' type='text' placeholder='Post Code' value='$strbilPstCde' /><font size='1' color='red'>*</font><a style='border:none;display:inline-block;padding:7px 0px;width:15%;font-size:13px;text-align:center;' href='Javascript:jsPostcodeLookup(\"billing\")'><i class='fa fa-search'></i></a>");
														print("<div id='ajax_addressBill'></div>");
													print("</div>");
													
													print("<div class='form-group col-sm-6 even-right'>");
														print("<input class='form-control' id='biladd1' placeholder='Address 1' name='biladd1' type='text' placeholder='Address 1' value='$strbiladd1' /><font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6' style='padding-right:15px;'>");
														print("<input class='form-control' id='biladd2' placeholder='Address 2' name='biladd2' type='text' placeholder='Address 2' value='$strbiladd2' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6' style='padding-left:15px;'>");
														print("<input class='form-control' id='biladd3' placeholder='Address 3' name='biladd3' type='text' placeholder='Address 3' value='$strbiladd3' />");
													print("</div>");
												print("</div>");
												
												print("<div class='row'>");
													print("<div class='form-group col-sm-6' style='padding-right:15px;'>");
														print("<input class='form-control' id='bilTown' placeholder='Town' name='bilTown' type='text' placeholder='Town' value='$strbilTown' />");
													print("</div>");
													
													print("<div class='form-group col-sm-6' style='padding-left:15px;'>");
														print("<input class='form-control' id='bilCnty' placeholder='Country' name='bilCnty' type='text' placeholder='County' value='$strbilCnty' /><font size='1' color='red'>*</font>");
													print("</div>");
												print("</div>");

											print("</fieldset>");
										print("</div>");
									print("</div>");
									
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-6' style='text-align:left;'>");
									print("<button onclick='return jsCancel();' class='btn btn-danger left' style='display:inline-block;'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-6 style='text-align:right;'>");
									print("<button onclick='return jsCustomerUpdate(this);' type='submit' class='btn btn-success right' style='display:inline-block;'>Save</button> ");
								print("</div>");
							print("</div>");
						break;
				
					case "viewDetails":
						
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
								print("<div class='section'>");
						
									$customerDetailsQuery = "SELECT * FROM customer WHERE recordID = :userID";
									$strType = "single";
									$arrdbparam = array( "userID" => $userID );
									$customerDetails = query($conn, $customerDetailsQuery, $strType, $arrdbparam);
					
									if (count($customerDetails) > 0)
									{
										if(!empty($customerDetails['firstname']) && !empty($customerDetails['surname']))
										{
											$strName = $customerDetails['firstname']." ".$customerDetails['surname'];
											if(!empty($customerDetails['title']))
											{
												$strName = $customerDetails['title']." ".$strName;
											}
										}
									
										print("<fieldset class='inlineLabels'>");
											print("<legend>Customer Details ".$customerDetails['email']." - ".date("d M Y", time())."</legend>");
											
											print("<div class='row'>");
												print("<div class='form-group col-md-4'>");
													print("<label class='control-label'>Full Name</label>");
														print($strName);
												print("</div>");
											
												print("<div class='form-group col-md-4'>");
													print("<label class='control-label'>Phone</label>");
													print($customerDetails['telephone']);
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group col-md-4'>");
													print("<label class='control-label'>Mobile</label>");
													print($customerDetails['mobile']);
												print("</div>");

												print("<div class='form-group col-md-4'>");
													print("<label class='control-label'>Email</label>");
														print($customerDetails['email']);
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group col-md-4'>");
													print("<label class='control-label'>Notes</label>");
													print($customerDetails['notes']);
												print("</div>");
											print("</div>");
												
										print("</fieldset>");
								
								print("</div>");
								print("<div class='section'>");
									
									$getCustomerAddressesQuery = "SELECT * FROM customer_address WHERE customerID = :customerID";
									$strType = "multi";
									$arrdbparam = array( "customerID" => $customerDetails['recordID'] );
									$customerAddresses = query($conn, $getCustomerAddressesQuery, $strType, $arrdbparam);
									
									if(count($customerAddresses) > 0)
									{
										print("<table class='table table-striped table-bordered table-hover table-condensed' >");
											print("<tbody>");
												foreach($customerAddresses AS $customerAddress)
												{
													$boolDescAmended = false;
													if($customerDetails['defaultDeliveryAdd'] == $customerAddress['recordID'])
													{
														$customerAddress['addDescription'] = "Default Delivery";
														$boolDescAmended = true;
													}
													if($customerDetails['defaultBillingAdd'] == $customerAddress['recordID'])
													{
														if($boolDescAmended)
														{
															$customerAddress['addDescription'] .= ", Default Billing";
														}
														else
														{
															$customerAddress['addDescription'] = "Default Billing";
														}
													}
													if($boolDescAmended)
													{
														$customerAddress['addDescription'] .= " Address";
													}
													print("<tr>");
														print("<th>".$customerAddress['addDescription']."</th>");
													print("</tr>");
													print("<tr>");
														print("<td>");
															if (!empty($customerAddress['firstname']) && !empty($customerAddress['surname']))
															{
																$strAddressName = $customerAddress['firstname']." ".$customerAddress['surname'];
																if (!empty($customerAddress['title']))
																{
																	$strAddressName = $customerAddress['title']." ".$strAddressName;
																}
															}
															else
															{
																$strAddressName = $strName." - Derived from account name as none entered for this address";
															}
															print ($strAddressName."<br/>");
															print ($customerAddress['add1']."<br/>");
															if (!empty($customerAddress['add2']))
															{
																print ($customerAddress['add2']."<br/>");
															}
															if (!empty($customerAddress['add3']))
															{
																print ($customerAddress['add3']."<br/>");
															}
															print ($customerAddress['town']."<br/>");
															print ($customerAddress['county']."<br/>");
															print ($customerAddress['country']."<br/>");
															print ($customerAddress['postcode']."<br/>");
														print("</td>");
													print("</tr>");
												}
											print("</tbody>");
										print("</table>");
									}
									else
									{
										print("<p>No address information could be found</p>");
									}
									
								print("</div>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='col-md-6 split even-left'>");
						
								print("<div class='row'>");
									print("<div class='form-group col-xs-4' style='text-align:left;padding-left:15px;padding-right:15px;'>");
									
										print("<button onclick='return jscancel(\"\")' class='btn btn-danger left' type='submit' style='display:inline-block;'>Return</button>");
										
									print("</div>");
									print("<div class='form-group col-xs-8' style='text-align:right;padding-right:15px;padding-left:15px;'>");
									
										print("<button onclick='return jsorderHistory(\"".$customerDetails['recordID']."\")' class='btn btn-primary ' type='submit' style='display:inline-block;margin-right:15px;'>Orders</button>");
										print("<button onclick='return jsModifyCustomer(\"".$customerDetails['recordID']."\")' class='btn btn-primary ' type='submit' style='display:inline-block;margin-right:15px;'>Edit</button>");
										print("<a href='/sales-order-create.php?customerIDPassed=".$customerDetails['recordID']."' class='btn btn-success ' type='submit' style='display:inline-block;'>Create New Order</a>");
										
									print("</div>");
								print("</div>");
								
							print("</div>");
						print("</div>");
													
									}
					break;
						
					default:
					case "userSearch":
				
						print("<button style='margin-bottom: 15px;' onclick='return jsCustomerAdd();' type='submit' class='btn btn-success'>Add New</button>");
						print("<div class='section'>");
							print("<table id='sortableCustomerTable' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th>Title</th>");
									print("<th>Firstname</th>");
									print("<th>Surname</th>");
									print("<th>Phone Number</th>");
								//	print("<th>Mobile Number</th>");
									print("<th>Email Address</th>");
									print("<th>Postcode</th>");
									//print("<th>Billing Address</th>");
									//print("<th>Delivery Address</th>");
									print("<th style=''>Orders</th>");
									print("<th style=''>Details</th>");
									print("<th style=''>Delete</th>");
								print("</tr></thead>");
							print("</table>");
						print("</div>");
						print("<button onclick='return jsCustomerAdd();' type='submit' class='btn btn-success'>Add New</button>");
					break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#sortableCustomerTable').dataTable({
				"processing": true,
				"serverSide": true,
				"bStateSave": true,
				"ajax": {
					"type": "POST",
					"url": "includes/ajax_customerdatatablepagination.php"
				}
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>