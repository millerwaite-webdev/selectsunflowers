<?php

//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * rss_news.php                                                       * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '
//	RSS Feed for news to be listed on Google Base.


	// ************* Common page setup ******************** //
	//=====================================================//
	
	$strpage = "search"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

    header ("Content-type: text/xml");
	print("<?xml version='1.0' encoding='US-ASCII' ?>\n");
	print("<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>\n");
	print("<channel>\n");
	print("<title>".$meta['title'].": Site News</title>\n");
	print("<link>".$strsiteurl."/news.php</link>\n");
	print("<description>Information about changes to the web site, new products and other updates relevant to our customers.</description>\n");
	print("<atom:link href='".$strsiteurl."/rss_news.php' rel='self' type='application/rss+xml' />\n\n\n");

	$getNewsArticlesQuery = "SELECT * FROM site_news_events WHERE title != 'New news item' ORDER BY date DESC";
	$strType = "multi";
	$newsArticles = query($conn, $getNewsArticlesQuery, $strType);

	if (empty($newsArticles))
	{
		$strmessage = "Error";
	}
	else
	{
		foreach($newsArticles AS $newsArticle)
		{
			print("<item>\n");
			print("<title>".str_replace("& ", "&amp; ", $newsArticle['title'])."</title>\n");
			print("<description>".strip_tags(str_replace("�", "GBP", str_replace("&pound;", "GBP", str_replace("&","&amp;",str_replace("?", "\"",str_replace("?", "\"",str_replace("?", "'",str_replace("?","-",$newsArticle['content']))))))))."</description>\n");
			print("<link>".$strsiteurl."/news.php?story=".$newsArticle['recordID']."</link>\n");
			print("<pubDate>".date("D, d M Y H:i:s ",$newsArticle['date'])."GMT</pubDate>\n");
			print("<guid>".$strsiteurl."news.php?story=".$newsArticle['recordID']."</guid>\n");
			print("</item>\n");
			print("\n\n\n");

			
		}
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null; // close the database connection after all processing

?>

</channel>
</rss>