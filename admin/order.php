<?php

//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * order.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "search"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	$intorder = $_REQUEST['id'];
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
?>
<div id="pagemiddle">
<?php
	include("includes/inc_sidebar.php");
?>
    <div id="content">
		<div id='pagetext'>
			<h2>Order Information</h2>
			<?php
				if ($intuserid > 0)
				{
					//$orderHeaderQuery = "SELECT * FROM order_header".$strtable." WHERE recordID = :orderHeaderID AND customerID = :customerID";
					$orderHeaderQuery = "SELECT order_header.*, order_status.description AS orderStatus FROM order_header INNER JOIN order_status ON order_header.orderStatus = order_status.recordID WHERE order_header.recordID = :orderHeaderID AND order_header.customerID = :customerID";
					$strType = "single";
					$arrdbparams = array(
						"orderHeaderID" => $intorder,
						"customerID" => $intuserid
					);
					$orderHeader = query($conn, $orderHeaderQuery, $strType, $arrdbparams);

					if (empty($orderHeader))
					{
						print ("<h2>Order Not Found</h2>");
					}
					else
					{
						$datorderdate = $orderHeader['timestampOrder'];
						$strstatus = $orderHeader['orderStatus'];
						$intmemberid = $orderHeader['customerID'];
						$strselectedpostage = $orderHeader['postageID'];

						$strname = $orderHeader['addDeliveryName'];
						$stradd1 = $orderHeader['addDelivery1'];
						$stradd2 = $orderHeader['addDelivery2'];
						$stradd3 = $orderHeader['addDelivery3'];
						$stradd4 = $orderHeader['addDelivery4'];
						$stradd5 = $orderHeader['addDelivery5'];
						$strpc = $orderHeader['addDeliveryPostcode'];

						$strbname = $orderHeader['addBillingName'];
						$strbadd1 = $orderHeader['addBilling1'];
						$strbadd2 = $orderHeader['addBilling2'];
						$strbadd3 = $orderHeader['addBilling3'];
						$strbadd4 = $orderHeader['addBilling4'];
						$strbadd5 = $orderHeader['addBilling5'];
						$strbpc = $orderHeader['addBillingPostcode'];
						

						$strcode = $orderHeader['offerCode'];
						$dectotalcost = $orderHeader['amountStock'];
						$decvatamt = $orderHeader['amountVat'];
						$decvatrate = $orderHeader['vatRate'];
						$decpostageamt = $orderHeader['amountDelivery'];
						$decdiscount = $orderHeader['amountOffer'];
						$dectotalorder = $orderHeader['amountTotal'];

						print ("<div class='signedIn'><!--content to display if a user is logged in-->");
							print ("<div id='order'>");
								print ("<h3><span style='float:right'>Order Number : $intorder</span>Order Date : ".date("D jS M Y",$orderHeader['timestampOrder'])."</h3>");
								print ("<h3>Order Status : ".$orderHeader['orderStatus']."</h3>");
								print ("<div id='orderAddress'>");
									print ("<h5>Billing Address : </h5>");
									print ("<p>$strbname</p>");
									print ("<address>$strbadd1,<br />");
									if ($strbadd2 != "") print ("$strbadd2<br />");
									if ($strbadd3 != "") print ("$strbadd3<br />");
									if ($strbadd4 != "") print ("$strbadd4<br />");
									if ($strbadd5 != "") print ("$strbadd5<br />");
									print ("$strbpc</address>");
								print ("</div>");

								print ("<div id='orderAddress'>");
								print ("<h5>Delivery Address : </h5>");
								print ("<p>$strname</p>");
								print ("<address>$stradd1,<br />");
								if ($stradd2 != "") print ("$stradd2<br />");
								if ($stradd3 != "") print ("$stradd3<br />");
								if ($stradd4 != "") print ("$stradd4<br />");
								if ($stradd5 != "") print ("$stradd5<br />");
								print ("$strpc</address>");
								print ("</div>");

								print ("<h5>Order Summary : </h5>");
								print ("<table id='orderTable'>");
									print ("<thead>");
										print ("<tr>");
											print ("<th style='width: 50%;'>Name</th>");
											print ("<th style='width: 15%;'>Size</th>");
											print ("<th style='width: 15%;'>Colour</th>");
											print ("<th style='width: 15%;'>Quantity</th>");
											print ("<th style='width: 15%;'>Price</th>");
										print ("</tr>");
									print ("</thead>");
									print ("<tbody>");
									
										$blnoddeven = false;
										$intitems = 0;
										
										//$orderItemsQuery = "SELECT recordID, orderHeaderID, stockID, stockDetailsArray, quantity, price FROM order_items".$strtable." WHERE orderHeaderID = :orderHeaderID ORDER BY recordID";
										$orderItemsQuery = "SELECT recordID, orderHeaderID, stockID, stockDetailsArray, quantity, price FROM order_items WHERE orderHeaderID = :orderHeaderID ORDER BY recordID";
										$strType = "multi";
										$arrdbparam = array("orderHeaderID" => $intorder);
										$orderItems = query($conn, $orderItemsQuery, $strType, $arrdbparam);

										foreach($orderItems AS $orderItem)
										{
											if ($blnoddeven)
											{
												$strtdclass = "class='evenrow'";
											}
											else
											{
												$strtdclass = "";
											}
											$blnoddeven = !$blnoddeven;
											
											$orderItemDetails = unserialize($orderItem['stockDetailsArray']);

											print ("<tr>");
												print ("<td $strtdclass>".$orderItemDetails['name']."</td>");
												print ("<td $strtdclass>".$orderItemDetails['size']."</td>");
												print ("<td $strtdclass>".$orderItemDetails['colour']."</td>");
												print ("<td $strtdclass>".$orderItem['quantity']."</td>");
												print ("<td align='right' $strtdclass>&pound;&nbsp;".number_format(($orderItem['quantity'] * $orderItem['price']),2)."</td>");
											print ("</tr>");

											$intitems = $intitems + $orderItem['quantity'];
										}
									
									print ("</tbody>");
									print ("<tfoot>");
										print ("<tr>");
											print ("<th>Sub Total:</th>");
											print ("<th> &nbsp; </th>");
											print ("<th> &nbsp; </th>");
											print ("<th>$intitems Items</th>");
											print ("<th align='right'>&pound;&nbsp;".number_format($dectotalcost,2)."</th>");
										print ("</tr>");
									print ("</tfoot>");
								print ("</table>");
								print ("<br/><br/><br/>");

								switch ($strselectedpostage)
								{
									case "3day":
										$strpostdesc = "3 Day Postage";
										break;
									case "3daysmall":
										$strpostdesc = "3 Day Postage";
										break;

									case "1day":
										$strpostdesc = "Next Day Postage";
										break;
									case "free":
										$strpostdesc = "Free 3 Day Postage";
										break;
								}

								print ("<table id='totalTable'>");
									print ("<thead><tr><th style='width:75%'>Item</th><th>Cost</th></tr></thead>");
									print ("<tbody>");
										print ("<tr><td>Goods</td><td align='right'>&pound;&nbsp;".number_format($dectotalcost,2)."</td></tr>");
										if ($strcode != "") print ("<tr><td class='evenrow'>Discount ($strcode)</td><td align='right' class='evenrow'> - &pound;&nbsp;".number_format($decdiscount,2)."</td></tr>");
										print ("<tr><td>$strpostdesc</td><td align='right'>&pound;&nbsp;".number_format($decpostageamt,2)."</td></tr>");
										print ("<tr><td class='evenrow'>VAT @ ".number_format($decvatrate,2)."%</td><td align='right' class='evenrow'>&pound;&nbsp;".number_format($decvatamt,2)."</td></tr>");
									print ("</tbody>");
									print ("<tfoot><tr><th style='width:25%'>Total</th><th align='right'>&pound;&nbsp;".number_format($dectotalorder,2)."</th></tr></tfoot>");
								print ("</table>");
							print ("</div><!--end of order-->");
						print ("</div>");
					}

				}
				else
				{
					redirect("account.php");
				}
			?>
		</div><!-- END OF PAGETEXT-->
	</div><!--End of page contents-->
</div><!--End of 'pagemiddle'-->

<?php	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>
