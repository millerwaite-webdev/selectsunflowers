<?php
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "import-history"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	function fnViewFileDetails($importID, $fileType, $conn){
		
		if ($importID != "") {
						
			//file information
			$strdbsql = "SELECT fileImport.*, configPaymentMethods.description AS payDescription, configFileImportTypes.description AS importDescription, admin_operator.username FROM fileImport 
			INNER JOIN admin_operator ON fileImport.operator = admin_operator.operatorID
			INNER JOIN configFileImportTypes ON fileImport.importType = configFileImportTypes.recordID
			INNER JOIN configPaymentMethods ON fileImport.paymentType = configPaymentMethods.paymentMethodID WHERE fileImport.importID = :importID";

			$arrdbparams = array("importID"=>$importID);
			$strType = "single";
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			$filePath = $resultdata['filePath'];
			
			//file imformation
			print ("<div class='col-sm-8'>");
			print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
			print ("<thead><tr>");
				print ("<th colspan='2'>File Details</th>");
			print ("</tr></thead><tbody>");
				print ("<tr>");
					print ("<td>Import Reference:</td>");
					print ("<td>".$resultdata['reference']."</td>");
				print ("</tr>");
				
				print ("<tr>");
					print ("<td>File Path:</td>");
					print ("<td><a href='".$resultdata['filePath']."' >".$resultdata['filePath']."</a></td>");
				print ("</tr>");
				
				print ("<tr>");
					print ("<td>Import Type:</td>");
					print ("<td>".$resultdata['importDescription']."</td>");
				print ("</tr>");
				
				print ("<tr>");
					print ("<td>Payment Type:</td>");
					print ("<td>".$resultdata['payDescription']."</td>");
				print ("</tr>");
			print ("</tbody>");
			print ("</table>");
			print ("</div>");

			//import format information
			$strdbsql = "SELECT configFileImportFormats.description AS formatDescription, configFileImportFormats.fileFormat, configFileImportWorksheets.*,  configFileImportMatchTypes.description AS accountMatchTypeName, configPaymentMethods.description AS paymentTypeName
			FROM configFileImportWorksheets
			INNER JOIN configFileImportFormats ON configFileImportWorksheets.importID = configFileImportFormats.importID
			INNER JOIN configFileImportMatchTypes ON configFileImportWorksheets.accountMatchType = configFileImportMatchTypes.recordID
			INNER JOIN configPaymentMethods ON configFileImportFormats.filePaymentType = configPaymentMethods.paymentMethodID
			WHERE configFileImportWorksheets.importID = :importID AND configFileImportWorksheets.importIndex = :importIndex";
			
			$arrdbparams = array("importID"=>$resultdata['configID'],"importIndex"=>$resultdata['worksheetID']);
			$strType = "single";
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);

			//file information
			print ("<div class='col-sm-4'>");
			print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
			print ("<thead><tr>");
				print ("<th colspan='2'>Format Details</th>");
			print ("</tr></thead><tbody>");
				print ("<tr>");
					print ("<td>Format Name:</td>");
					print ("<td>".$resultdata['formatDescription']."</td>");
				print ("</tr>");
				print ("<tr>");
					print ("<td>File Format:</td>");
					print ("<td>".$resultdata['fileFormat']."</td>");
				print ("</tr>");
				print ("<tr>");
					print ("<td>Payment Type:</td>");
					print ("<td>".$resultdata['paymentTypeName']."</td>");
				print ("</tr>");
				print ("<tr>");
					print ("<td>Account Match Type:</td>");
					print ("<td>".$resultdata['accountMatchTypeName']."</td>");
				print ("</tr>");
			print ("</tbody>");
			print ("</table>");
			print ("</div>");
			
			
		
			function getFileImportData($importID,$resultdata,$conn) {
								
				$strdbsql = "SELECT * FROM fileImportData WHERE importID = :importID ORDER BY rowID";
				$arrdbparams = array("importID"=>$importID);
				$strType = "multi";
				$datarows = query($conn, $strdbsql, $strType, $arrdbparams);
				
				//setup table
				$return['content'] .= ("<div class='col-sm-12'>");
				$return['content'] .= ("<table class='table table-striped table-bordered table-hover table-condensed' >");
				$return['content'] .= ("<thead><tr>");
				$return['content'] .= ("<th>Row</th>");
				$return['content'] .= ("<th>Matched Value</th>");
				$return['content'] .= ("<th>Matched Members</th>");
				$return['content'] .= ("<th>Payment Amount</th>");
				$return['content'] .= ("<th>Payment Per Account</th>");
				$columnNames = explode(",", $resultdata['columnNames']);
				for ($i = 0; $i < $resultdata['noColumns']; $i++) {
					$return['content'] .= ("<th>".$columnNames[$i]."</th>");
				}
				$return['content'] .= ("</tr></thead><tbody>");
				
				//get highest row of data and then loop around all displaying each data row
				$noofdatarows = count($datarows);
				$strerror = "";
				for ($j = 0; $j < $noofdatarows; $j++) {
					
					if (($resultdata['noIgnoreTop'] <= $j) AND (($noofdatarows - $resultdata['noIgnoreBottom']) > $j)) {
					
						$return['content'] .= ("<tr>");
						$rowData = unserialize($datarows[$j]['dataArray']);
						switch($resultdata['paymentTypeName']){
							case "Direct Debit":

								switch($resultdata['directDebitFileTypeID']){
									case 6: //clean - weekly, no restrictions
										$strdbsql = "SELECT * FROM fileImportPayments WHERE importID = :importID AND dataID = :dataID";
									break;
									case 7: //rejection - weekly, no restrictions
										$strdbsql = "SELECT * FROM fileImportRejection WHERE importID = :importID AND dataID = :dataID";
									break;
									case 16: //amendments - weekly, no restrictions
										$strdbsql = "SELECT * FROM fileImportAmendments WHERE importID = :importID AND dataID = :dataID";
									break;
									case 17: //claim file
										$strdbsql = "SELECT * FROM fileImportClaimData WHERE importID = :importID AND dataID = :dataID";
									break;
									case 18: //unpaid comments
										$strdbsql = "SELECT * FROM fileImportRejection WHERE importID = :importID AND dataID = :dataID";
									break;
									default:
										$strdbsql = "SELECT * FROM fileImportDirectDebits WHERE importID = :importID AND dataID = :dataID";
									break;
								}
								
							break;
							default:
								$strdbsql = "SELECT * FROM fileImportPaymentReport WHERE importID = :importID AND dataID = :dataID";
							break;
						
						}
						$arrdbparams = array("importID"=>$importID, "dataID"=>$datarows[$j]['recordID']);
						$strType = "single";
						$paymentReportInfo = query($conn, $strdbsql, $strType, $arrdbparams);
						
						$return['content'] .= ("<td class='info'>".$datarows[$j]['rowID']."</td>");
						//$return['content'] .= ("<td class='info'>".$paymentReportInfo['matchedValue']."</td>");
						
						$unserialized = @unserialize($paymentReportInfo['matchedValue']);

						if ( FALSE === $unserialized){
							$return['content'] .= ("<td class='info'>".$paymentReportInfo['matchedValue']."</td>");
						} else {
							$matchedValue = unserialize($paymentReportInfo['matchedValue']);
							$return['content'] .= ("<td class='info'>");
							foreach ($matchedValue AS $match){
								$return['content'] .= $match['matchType']." ".$match['matchValue']."<br/>";
							}
							$return['content'] .= ("</td>");
						}
						
						$matchedAccounts = explode(",",$paymentReportInfo['matchedAccounts']);
						$matchedMembers = "";
						foreach ($matchedAccounts AS $accountID) {
							$strdbsql = "SELECT membershipID FROM account WHERE accountID = :accountID";
							$arrdbparams = array("accountID"=>$accountID);
							$strType = "single";
							$memberLookup = query($conn, $strdbsql, $strType, $arrdbparams);
							$matchedMembers .= "<a href='/membership-edit.php?id=".$memberLookup['membershipID']."' >".$memberLookup['membershipID']."</a>,";
						}
						$return['content'] .= ("<td class='info'>".rtrim($matchedMembers,",")."</td>");
						$return['content'] .= ("<td class='info'>".number_format($paymentReportInfo['paymentAmount'],2)."</td>");
						$return['content'] .= ("<td class='info'>".number_format($paymentReportInfo['paymentPerAccount'],2)."</td>");
						
						if (count($rowData) != $resultdata['noColumns']) { //sometimes the files can have an extra space on the end or a space missing from the last column
							
							if (count($rowData) == $resultdata['noColumns']+1) { //extra column
								
								//check to see if the last column is blank
								if ($rowData[count($rowData)-1] == "") {
									$strwarning = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'].".<br/><b> The extra column looks to only contain spaces, please check the import process carefully.</b>";
								} else {
									$strwarning = "";
									$strerror = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'].".<br/><b> The last column has at least one value, please review the file.</b>";
								}
								
							} else if (count($rowData) == $resultdata['noColumns']-1) { //missing 1 column
								
								$strwarning = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'].".<br/><b>If the last column is not used you can ignore this warning, however if you are unsure please check the file.</b>";

							} else {
								$strerror = "Number of columns does not match the number defined in the import format. Columns in this import: ".count($rowData).", columns expected: ".$resultdata['noColumns'];
							}
						}
						for ($i = 0; $i < $resultdata['noColumns']; $i++) {
							$return['content'] .= ("<td>".$rowData[$i]."</td>");
						}
						$return['content'] .= ("</tr>");
					}
				}
				
				$return['content'] .= ("</tbody>");
				$return['content'] .= ("</table>");
				$return['content'] .= ("</div>");
				$return['error'] = $strerror;
				$return['warning'] = $strwarning;
				return $return;
			}
			function getButtons($cmd, $strerror,$strwarning){
				print ("<div class='col-sm-12'>");
				if ($strerror == "") {
					if ($strwarning != "") print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>");
					print ("<button style='margin-bottom: 10px;' onclick='fnCancel(); return false;' class='btn btn-default left'>Cancel</button>");
					//print ("<button style='margin-bottom: 10px;' onclick='fnProcess(\"$cmd\"); return false;' class='btn btn-success right'>Continue</button>");
				} else {
					print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>");
					print ("</div>");
					print ("<div class='col-sm-12'>");
					print ("<button style='margin-bottom: 10px;' onclick='fnCancel(); return false;' class='btn btn-default left'>Cancel</button>");
				}
				print ("</div>");
			}
			
			$fileImportData = getFileImportData($importID,$resultdata,$conn);
			$cmd = "processFile";
			getButtons($cmd, $fileImportData['error'], $fileImportData['warning']);
			print ("<div class='col-sm-12'>");
			print($fileImportData['content']);
			print ("</div>");
			getButtons($cmd, $fileImportData['error'], $fileImportData['warning']);

		} else {
			print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>No file uploaded</p></div>");
		}
	}
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['importID'])) $importID = $_REQUEST['importID']; else $importID = "";
	switch($strcmd)
	{
		case "removeImport": // complete

			$strdbsql = "DELETE FROM fileImportAmendments WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "DELETE FROM fileImportCancellation WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "DELETE FROM fileImportClaimData WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "DELETE FROM fileImportDirectDebits WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "DELETE FROM fileImportMembers WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "DELETE FROM fileImportPaymentReport WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "DELETE FROM fileImportPayments WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strdbsql = "DELETE FROM fileImportRejection WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
		
			$strdbsql = "DELETE FROM fileImport WHERE importID=:importID";
			$strType = "delete";
			$arrdbparams = array("importID"=>$_REQUEST['importID']);
			$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
			$strcmd = "";
		break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>".$configPageDetails['pageTitle']."</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

	?>
	<script language='Javascript' >
	function fnRemoveImport(importID) {
		document.getElementById("cmd").value = 'removeImport';
		document.getElementById("importID").value = importID;
		document.getElementById("form").submit();
	}
	function fnLoadImportRows(importID) {
		document.getElementById("cmd").value = 'showImportRows';
		document.getElementById("importID").value = importID;
		document.getElementById("form").submit();
	}
	function fnCancel(){
		document.getElementById("cmd").value = '';
		document.getElementById("importID").value = '';
		document.getElementById("form").submit();
	}
	</script>
<?php
	
			
			print ("<div class='section' >");
			print ("<form action='import-history.php' class='uniForm' method='post' name='form' id='form'>");
				print ("<input type='hidden' class='nodisable' name='cmd' id='cmd' value=''/>");
				print ("<input type='hidden' class='nodisable' name='importID' id='importID' value='$importID'/>");

				print ("<h1>Import History</h1>");
				
				//Print out debug and error messages
				if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
				if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
				if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

				switch ($strcmd){
				
					case "showImportRows":
						
						fnViewFileDetails($importID, "", $conn);
						
					break;
					default:
			
						print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
								print ("<th>Reference</th>");
								print ("<th>File Path</th>");
								print ("<th>Import Type</th>");
								print ("<th>Status</th>");
								print ("<th>Operator</th>");
								print ("<th>Date</th>");
								print ("<th></th>");
							print ("</tr></thead><tbody>");
							
							$strdbsql = "SELECT fileImport.*, fileImportStatus.description AS statusDescription, configFileImportTypes.description AS importDescription, admin_operator.username FROM fileImport 
							INNER JOIN admin_operator ON fileImport.operator = admin_operator.operatorID
							INNER JOIN configFileImportTypes ON fileImport.importType = configFileImportTypes.recordID
							INNER JOIN fileImportStatus ON fileImport.status = fileImportStatus.recordID
							ORDER BY fileImport.timestamp DESC";
							$strType = "multi";
							$resultdata = query($conn, $strdbsql, $strType);

							if (count($resultdata) != 0) {
								foreach ($resultdata as $row) {
									$tablecolour = "";
									
									switch ($row['status']) {
										case 1:
											$tablecolour = "warning";
										break;
										case 2:
											$tablecolour = "info";
										break;
										case 3:
											$tablecolour = "success";
										break;
									}
									

									print ("<tr>");
										print ("<td class='$tablecolour'>".$row['reference']."</td>");
										print ("<td class='$tablecolour'><a href='".$row['filePath']."' >".$row['filePath']."</a></td>");
										if ($row['importDescription'] == "Payment") print ("<td class='$tablecolour'><a href='' onclick='fnLoadImportRows(".$row['importID']."); return false;' >".$row['importDescription']."</a></td>");
										else print ("<td class='$tablecolour'>".$row['importDescription']."</td>");
										print ("<td class='$tablecolour'>".$row['statusDescription']."</td>");
										print ("<td class='$tablecolour'>".$row['username']."</td>");
										print ("<td class='$tablecolour'>".date("H:i d-m-Y", $row['timestamp'])."</td>");
										if ($row['status'] == 1) {
											print ("<td class='$tablecolour'><button onclick='fnRemoveImport(".$row['importID']."); return false;' class='center btn btn-danger circle' type='submit'>X</button></td>");
										} else {
											print ("<td class='$tablecolour'></td>");
										}
									print ("</tr>");
								}
							} else {
								print ("<tr><td colspan='7'>No files have been uploaded</td></tr>");
							}
							print ("</tbody>");
						print ("</table>");
					break;
				}
			print ("</form>");
			print("</div>");
		print("</div>");
	print("</div>");

// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>