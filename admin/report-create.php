<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * config-letters.php                                      * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "report-create"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['frm_fromdate']) AND $_REQUEST['frm_fromdate'] != "") {$fromdate = strtotime(str_replace("/", "-", $_REQUEST['frm_fromdate']));} else {$fromdate = strtotime('-1 year', $datnow);}
	if (isset($_REQUEST['frm_todate']) AND $_REQUEST['frm_todate'] != "") { $todate = strtotime('+1 day', strtotime(str_replace("/", "-", $_REQUEST['frm_todate'])));} else {$todate = $datnow;}
	
	switch ($strcmd) {
		case "generatereport":
			$intexportID = $_REQUEST['frm_exportConfigID'];

			// check export file format
			$strdbsql = "SELECT * FROM configFileExportFormats WHERE exportID=:exportID";
			$strType = "single";
			$arrdbparams = array("exportID"=>$intexportID);
			$resultdatasingle = query($conn,$strdbsql,$strType,$arrdbparams);

			include("includes/inc_generateexportspecial.php"); // generates a special export file

			break;
	}
	$strcmd = "";

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//


?>
<script language='Javascript' >

	function jsExportSelected(selected){
		var exportID = selected.value;
		if (exportID !== "") {
			jQuery.ajax({
				type: "POST",
				url: "includes/ajax_exportparameters.php",
				data: {exportID: exportID},
				async: true
			}).done(function( data ) {
				$("#exportresults").html(data);
				$('#frm_fromdate').datepicker({
					dateFormat: 'dd/mm/yy',
					changeMonth: true, 
					changeYear: true
				});
				$('#frm_todate').datepicker({
					dateFormat: 'dd/mm/yy',
					changeMonth: true, 
					changeYear: true
				});
				$('#frm_fromdate').change(function(){
					$(this).attr('value', $('#frm_fromdate').val());
				});
				$('#frm_todate').change(function(){
					$(this).attr('value', $('#frm_todate').val());
				});
			});
		}
	}

	function fngeneratereport(reportType) {
		$('select[name="frm_exportConfigID"]').rules('add', { required: true });
		if ($('#form').valid()) {
			switch (reportType) {
				default: document.getElementById("cmd").value = "generatereport"; break;
			}
			document.getElementById("form").submit();		
		}
	}

</script>
<?php
	

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<form action='report-create.php' class='uniForm' method='post' name='form' id='form'>");
				print("<input type='hidden' class='nodisable' name='cmd' id='cmd' value=''/>");
				print("<input type='hidden' class='nodisable' name='exportID' id='exportID' value=''/>");

				print("<h1>Create Report</h1>");
				
				//Print out debug and error messages
				if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
				if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
				if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

				print("<div class='row'>");
					print("<div class='col-md-6 even-left'>");
						print("<div class='section'>");
							print("<fieldset>");
				
								// select an export document
								$strdbsql = "SELECT * FROM configFileExportFormats WHERE status=1 ORDER BY formatName";
								$strType = "multi";
								$resultdataExportFormats = query($conn, $strdbsql, $strType);

								if (count($resultdataExportFormats) > 0) {
									
									print("<legend>Select Export Document: </legend>");
								
									print("<div class='row'>");
										print("<div class='form-group col-sm-6 even-left'>");
											print("<select name='frm_exportConfigID' id='frm_exportConfigID' class='form-control' onchange='jsExportSelected(this);'>");
												print("<option value=''>-- Please Select --</option>");
												foreach ($resultdataExportFormats as $row) {
													print("<option value='".$row['exportID']."'>".$row['formatName']."</option>");
												}
											print("</select>");
										print("</div>");
									print("</div>");
									
									print("<div id='exportresults'></div>");
									
								}

			print("</form>");
		print("</div>");
	print("</div>");

?>
<script language='Javascript'>

	// validate signup form on keyup and submit
	$().ready(function(){
		$("#form").validate({
			errorPlacement: function(error,element) {
				error.insertAfter(element);
			},
			rules: {		
			}
		});
	});

</script>
<?php

	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null;
?>