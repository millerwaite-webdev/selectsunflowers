<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * subscribe.php                                                      * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details

	
	$strpage = "subscribe"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database
	
	if(isset($_REQUEST['name']))$strname = $_REQUEST['name']; else $strname = "";
	if(isset($_REQUEST['email']))$stremail = $_REQUEST['email']; else $stremail = "";
	
	// ************* Requests processing ****************** //
	//=====================================================//	
	if(isset($_REQUEST['cmd'])) {
		$strcmd = $_REQUEST['cmd'];
	
		switch ($strcmd) {
			case "subscribe":
				if ($stremail != "")
				{
					if(!empty($_SESSION['userID']))
					{
						$newsletterSub = getNewsletterSubscription($_SESSION['userID'], "bycustid", $conn);
					}
					else
					{
						$newsletterSub = getNewsletterSubscription($stremail, "byemail", $conn);
					}
					
					if (empty($_SESSION['userID']) && empty($newsletterSub))
					{
						if (insertNewsletterSubscription(0, $strname, $stremail, $conn)) $strinfo = "Your email address (".$stremail.") has been added to our mailing list.";
						else $strerror = "An unknown error has occured. Please try again. If you still have problems please contact ".$companyName." directly to have your newsletter subscription started.";
					}
					elseif (!empty($_SESSION['userID']) && empty($newsletterSub))
					{
						if (insertNewsletterSubscription($_SESSION['userID'], $strname, $stremail, $conn)) $strinfo = "Your email address (".$stremail.") has been added to our mailing list.";
						else $strerror = "An unknown error has occured. Please try again. If you still have problems please contact ".$companyName." directly to have your newsletter subscription started.";
					}
					elseif(!empty($_SESSION['userID']) && !empty($newsletterSub))
					{
						if (updateNewsletterSubscription($stremail, $strname, $_SESSION['userID'], $conn)) $strinfo = "Your email address (".$stremail.") has been re-added to our mailing list.";
						else $strerror = "An unknown error has occured. Please try again. If you still have problems please contact ".$companyName." directly to have your newsletter subscription started.";
					}
					elseif(empty($_SESSION['userID']) && !empty($newsletterSub))
					{
						$strerror = "The entered email address is already on our mailing list.";
					}
				}
				else
				{
					$strerror = "Please enter an Email Address";
				}
				break;
		}
	}

	// ************* Common page setup ******************** //
	//=====================================================//
	$meta['metaTitle'] = "Newsletter Subscription";
	$meta['description'] = "Newsletter";
	$meta['keywords'] = "newsletter,subscription";
	$meta['metaPageLink'] = "subscribe.php";

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");
	
	print("<div id='mainContent'>");
	
	if($strerror != "") {
		print("<div class='notification-error'>");
			print("<h3>Error</h3>");
			print("<p>".$strerror."</p>");
		print("</div>");
	}
	if($strinfo != "") {
		print("<div class='notification-info'>");
			print("<h3>Info</h3>");
			print("<p>".$strinfo."</p>");
		print("</div>");
	}

?>
	<h2>Newsletter Subscriptions</h2>
	<p>Thank you for your interest.</p>
	<p>Our free newsletter will normally be issued every month throughout the year and will contain a host of information we think will be useful to you as a customer. Such as information on any seeds we have recently added to our catalogue, special offers and catalogue releases.</p>
	<form name='frm_newsletter' id='frm_newsletter' action='subscribe.php' method='post'>
		<input type='hidden' name='cmd' value='subscribe' />
		<p><label for='name'>Name:</label> <input type='text' id='name' name='name' value='<?php print($strname); ?>' /></p>
		<p><label for='email'>Email:</label> <input type='text' id='email' name='email' value='<?php print($stremail); ?>' /></p>
		<input type='submit' class='short_button' value='Subscribe'/>  <br/>		
	</form>
	<p style='text-align:center;'> Do you want to Unsubscribe? <a href='/unsubscribe.php' >Click Here</a>
<?php
	if($intuserid > 1) print("<br/><br/><input type='button' class='short_button' onclick='window.location = \"account.php\"' value='Return to Account' />");
?>
	</p>
	</div>
<?php 	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>

