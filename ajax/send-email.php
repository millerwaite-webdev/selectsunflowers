<?php

	include("incsitecommon.php");
 
	$type = "Website Enquiry";
 
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	$phone = $_REQUEST['phone'];
	$message = $_REQUEST['message'];
	
	$secret = $captchaSecret;
	$response = $_REQUEST["captcha"];
	$verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$response);
	$captcha_success = json_decode($verify);
	
	if($captcha_success->success == false) {

	} else if($captcha_success->success == true) {
	
		$strdbsql = "INSERT INTO site_contact_history (name, telephone, email, subject, message, date) VALUES (:name, :telephone, :email, :type, :message, UNIX_TIMESTAMP())";
		$strType = "insert";
		$dataParams = array('name' => $name, 'telephone' => $phone, 'email' => $email, 'type' => $type, 'message' => $message);
		$addEnquiry = query($conn, $strdbsql, $strType, $dataParams);
		
	//	$to = $compEmail;
		$to = "leewatts@millerwaite.co.uk";
		
		$subject = $type;
		
		$headers = "From: " .str_replace(" ", "", strtolower("enquiry@".$compName.".co.uk"))."\r\n";
		$headers .= "Reply-To: ".strip_tags($email)."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$html = "<html>";
			$html .= "<head>";			
				$html .= "<style>";
					$html .= "body { background-color: #F1F1F1; margin: 0; font-family: arial; }";
					$html .= "a { color: #887777; }";
					$html .= "p { font-size: 14px; }";
					$html .= "strong.primary { color: #887777; }";
				$html .= "</style>";
			$html .= "</head>";
			$html .= "<body>";
				$html .= "<table width='100%' cellpadding='0' cellspacing='0'>";
					$html .= "<tr>";
						$html .= "<td>";
							$html .= "<table width='600px' align='center' cellpadding='0' cellspacing='0'>";
								$html .= "<thead>";
									$html .= "<tr>";
										$html .= "<td align='right'>";
											$html .= "<img width='100%' src='".$strsiteurl."images/emails/header.jpg'/>";
										$html .= "</td>";
									$html .= "</tr>";
								$html .= "</thead>";
								$html .= "<tbody>";
									$html .= "<tr>";
										$html .= "<td>";
											$html .= "<table width='100%' align='left' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>";
												$html .= "<tr>";
													$html .= "<td>";
														$html .= "<table width='510' align='left' cellpadding='45' cellspacing='0'>";
															$html .= "<tr>";
																$html .= "<td>";
																	$html .= "<p>".$compName.",</p>";
																	$html .= "<br/>";
																	$html .= "<p>You have <strong>1</strong> new &quot;".$type."&quot; from a visitor on your website. The details are shown below:</p>";
																	$html .= "<br/>";
																	$html .= "<p><strong>&quot; ".$message." &quot;</strong></p>";
																	$html .= "<br/>";
																	$html .= "<p>You received this enquiry from <strong class='primary'>".$name." (".$email.")</strong> at ".date('g:ia')." on ".date('l jS F').". To contact the sender, you should reply to this email".($phone != '' ? ' or contact them on <strong>'.$phone.'</strong>.' : '.')."</p>";
																$html .= "</td>";
															$html .= "</tr>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</table>";
										$html .= "</td>";
									$html .= "</tr>";
								$html .= "</tbody>";
								$html .= "<tfoot>";
									$html .= "<tr>";
										$html .= "<td align='right'>";
											$html .= "<img width='100%' src='".$strsiteurl."images/emails/footer.jpg'/>";
										$html .= "</td>";
									$html .= "</tr>";
								$html .= "</tfoot>";
							$html .= "</table>";
						$html .= "</td>";
					$html .= "</tr>";
				$html .= "</table>";
			$html .= "</body>";
		$html .= "</html>";
		
		mail($to, $subject, $html, $headers);
	
		print("Success");
	
	}
	
?>