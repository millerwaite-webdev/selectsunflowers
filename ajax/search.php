<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	$keywords = $_REQUEST['keywords'];
	
	$strdbsql = "SELECT stock.recordID, stock.stockCode, stock_group_information.name AS stockGroup, stock_group_information.description AS stockDescription, stock_group_information.productType, stock_group_information.productCost FROM stock 
	LEFT JOIN stock_group_information ON stock.groupID = stock_group_information.recordID
	WHERE (stock_group_information.name LIKE :keywords OR stock.stockCode LIKE :keywords) AND stock_group_information.enabled = 1
	LIMIT 10";
	$result = query($conn,$strdbsql,"multi",["keywords"=>"%".$keywords."%"]);
	
	$products = [];
	
	if(count($result) > 0) {
		foreach($result AS $row) {
			$product = [
				"id" => $row['recordID'],
				"name" => $row['stockGroup'],
				"link" => "/".($row['productType'] > 0 ? "boxes" : "wholesale")."/".str_replace(" ", "-", strtolower($row['stockCode'])),
			];
			$products[] = $product;
		}
	} else {
		$product = [
			"id" => 0,
			"name" => "Sorry, we can't find &quot;".$keywords."&quot; in our shop. Try searching again, or browse our selections through the <a href='/wholesale'>shop</a>.",
			"link" => "",
		];
		$products[] = $product;
	}
	
	print(json_encode($products));

?>