<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	switch($_REQUEST['method']) {
		
		case "edit":
	
			$strdbsql = "SELECT order_header.*, order_saves.title, order_saves.description FROM order_header 
			INNER JOIN order_saves ON order_header.basketHeaderID = order_saves.basketHeaderID 
			WHERE order_header.basketHeaderID = :order";
			$result1 = query($conn,$strdbsql,"single",["order"=>$_REQUEST['order']]);
			
			if($result1) {
				
				$strdbsql = "SELECT order_items.*, stock_images.imageLink AS image, stock_images.description AS alt FROM order_items 
				INNER JOIN stock_images ON order_items.stockID = stock_images.stockID 
				WHERE order_items.orderHeaderID = :order 
				GROUP BY stockID";
				$result2 = query($conn,$strdbsql,"multi",["order"=>$result1['recordID']]);
				
				if(count($result2) > 0) {
					foreach($result2 AS $row2) {
						$items = [
							"image" => $row2['image']
						];
					}
				}
			}
			
			$order = [
				"method" => $_REQUEST['method'],
				"title" => $result1['title'],
				"description" => $result1['description'],
				"price" => $result1['amountTotal'],
				"items" => $items
			];
			
			print(json_encode($order));
			
			break;
		case "save":
		
			$strdbsql = "UPDATE order_saves SET title = :title, description = :description WHERE basketHeaderID = :order";
			$result = query($conn,$strdbsql,"update",["order"=>$_REQUEST['order'],"title"=>$_REQUEST['title'],"description"=>$_REQUEST['description']]);
			
			print(json_encode(["title"=>$_REQUEST['title'],"description"=>$_REQUEST['description']]));
		
			break;
	
	}

?>