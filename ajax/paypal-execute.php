<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");
	
	require_once(dirname(__file__)."/paypal.php");

	if($debug == false) $mode = "live";
	else $mode = "dev";
	
	$paypal = new PayPal($mode,$conn);
	
	$data = array();
	
	$data['payer_id'] = $_POST['payerID'];
	
	$customerTel = $_POST['customerTel'];
	
	$deliveryDate = strtotime($_POST['deliveryDate']);
	$deliveryTime = $_POST['deliveryTime'];
	$deliveryNote = $_POST['deliveryNote'];
	
	$hear = $_POST['hear'];
	$newsletter = $_POST['newsletter'];
	
	$executeurl = $paypal->url."/v1/payments/payment/".$_POST['paymentID']."/execute/";
	
	$ch = curl_init($executeurl);
	
	//Encode the array into JSON.
	$jsonDataEncoded = json_encode($data);
	
	//Tell cURL that we want to send a POST request.
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	 
	//Attach our encoded JSON string to the POST fields.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
	 
	//Set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: ".$paypal->tokenType." ".$paypal->token)); 
	 
	//Execute the request
	$result = json_decode(curl_exec($ch));
	
	if(isset($result->intent) && $result->intent == "sale") {
	
		// Get data from PayPal
		$basketid = $result->transactions[0]->invoice_number;
		$payPalPaymentRef = $result->transactions[0]->related_resources[0]->sale->id;
		$payPalPaymentStatus = $result->transactions[0]->related_resources[0]->sale->state;
		
		$ordertotal = $result->transactions[0]->amount->total;
		$orderCurrency = $result->transactions[0]->amount->currency;
		
		$contents = $result->transactions[0]->item_list->items;
		$recipient = $result->transactions[0]->item_list->shipping_address;
		$transtime = $result->transactions[0]->related_resources[0]->sale->created_time;
		
		// Customer details
		$recipientTitle = "";
		$recipientFirstname = $result->payer->payer_info->first_name;
		$recipientSurname = $result->payer->payer_info->last_name;
		$recipientEmail = $result->payer->payer_info->email;
		
		// Address details
		$strname = $recipient->recipient_name;
		$stradd1 = $recipient->line1;
		$stradd2 = ((isset($recipient->line2)) ? $recipient->line2 : "");
		$stradd3 = "";
		$stradd4 = $recipient->city;
		$stradd5 = ((isset($recipient->state)) ? $recipient->state : "");
		$stradd6 = $recipient->country_code;
		$strpostcode = $recipient->postal_code;
		
		$customerid = checkcustomer($conn, $accountid, $recipientTitle, $recipientFirstname, $recipientSurname, $customerTel, $stradd1, $stradd2, $stradd3, $stradd4, $stradd5, $stradd6, $strpostcode, $recipientEmail);
		
		// Add customer marketing
		if(!empty($hear)) {
			$strdbsql = "INSERT INTO customer_marketing (customerID, value) VALUES (:customer, :value)";
			$marketing = query($conn,$strdbsql,"insert",["customer"=>$customerid,"value"=>$hear]);
		}
		
		// Add customer to newsletter
		if($newsletter == 1) {
			// Check if already on newsletter
			$strdbsql = "SELECT * FROM customer_newsletter WHERE emailAddress = :email";
			$newsletter = query($conn,$strdbsql,"single",["email"=>$recipientEmail]);
			// Add to newsletter
			if(!$newsletter) {
				$strdbsql = "INSERT INTO customer_newsletter (customerID, name, emailAddress) VALUES (:customer, :name, :email)";
				$newsletter = query($conn,$strdbsql,"insert",["customer"=>$customerid,"name"=>$recipientTitle." ".$recipientFirstname." ".$recipientSurname,"email"=>$recipientEmail]);
			}
		}
		
		$basket = basketCheckContents($conn,$basketid);
		
		$orderid = createorder($conn, $customerid, $basketid, $basket, $recipientFirstname, $recipientSurname, $stradd1, $stradd2, $stradd3, $stradd4, $stradd5, $stradd6, $strpostcode, $deliveryDate, $deliveryTime, $customerTel, $recipientEmail, $deliveryNote, $strdomain, 2, $ordertotal);

		deletebasket($conn, $basketid);

		confirmationemail($conn, $basketid);

		$result = ["st"=>"success","id"=>$orderid];

	} else {
		$result = ["st" => "error"];
	}
	
	header("Content-type:Application/json");
	
	echo json_encode($result);

?>