<?php

	/**
	* PayPal API Class
	*
	* @property string $mode        "live" or "dev" indicates if sandbox should be used
	* @property string $token       Current auth token to be used with requests
	* @property string $tokenType   The auth type associated witht he current token
	* @property string $url         The URL of the PayPal API endpoint
	*/
	class PayPal {
		private $__conn;
		private $__authUser;
		private $__authSecret;
		protected $__mode;
		protected $__token;
		protected $__tokenType;
		protected $__url;
		
		public function __construct($mode, $conn = null) {
			if($conn == null) {
				$this->__conn = connect();
			} else {
				$this->__conn = $conn;
			}
			$this->__mode = $mode;
			
			$strdbsql = "SELECT * FROM payPalSettings WHERE mode LIKE :mode";
			$ppSetup = query($this->__conn,$strdbsql,"single",["mode"=>$this->__mode]);
			$this->__authUser = $ppSetup['authUser'];
			$this->__authSecret = $ppSetup['authSecret'];
			$this->__url = $ppSetup['paypalurl'];

			if($ppSetup['tokenExpireTime'] < time()+(60*60)) {
				$this->refreshToken();
			} else {
				$this->__token = $ppSetup['currentToken'];
				$this->__tokenType = $ppSetup['tokenType'];
			}
		}
		
		public function __get($var)
		{
			$var = "__".$var;
			return ((isset($this->$var)) ? $this->$var : null);
		}
		
		public function refreshToken() {
			
			$tokenurl = $this->__url."/v1/oauth2/token";
			
			//Initiate cURL.
			$ch = curl_init($tokenurl);
			
			
			//Tell cURL that we want to send a POST request.
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//Attach our encoded JSON string to the POST fields.
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');

			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $this->__authUser . ":" . $this->__authSecret);
			
			//Set the content type to application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded')); 
			
			
			//Execute the request
			$result = json_decode(curl_exec($ch));

			$strdbsql = "UPDATE payPalSettings SET currentToken  = :authToken, tokenType = :tokenType, tokenExpireTime = :expire WHERE mode LIKE :mode";

			$update = query($this->__conn,$strdbsql,"update",[
				"tokenType" => $result->token_type,
				"authToken" => $result->access_token,
				"expire" => strtotime(substr($result->nonce,0,19)) + intval($result->expires_in),
				"mode" => $this->__mode
			]);
			$this->__token = $result->access_token;
			$this->__tokenType = $result->token_type;
		}
	}
