<?php
	
	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");
	
	if(isset($_POST["postcode"]) && $_POST["postcode"] != "") {
		
		$account = "3215";
		$password = "exrf95ev";
		$url = "http://ws1.postcodesoftware.co.uk/lookup.asmx/getAddress?account=".$account."&password=".$password."&postcode=".$_POST["postcode"];
		
		$xml = simplexml_load_file(str_replace(' ','', $url)); // Removes unnecessary spaces

		if($xml->ErrorNumber == 0) {
			
			if($xml->PremiseData != "") {
				$chunks = explode (";", $xml->PremiseData); // Splits up premise data
				print("<div class='col s12'>");
					print("<div class='input-field' id='address1'>");
						print("<select class='browser-default depend' id='frm_address' name='frm_address'>");
							foreach($chunks AS $v) {
								if($v != "") {
									list($organisation, $building , $number) = explode('|', $v); // Splits premises into organisation, building and number
									print("<option>");
										if($organisation != "") print($organisation.", ");
										if($building != "") print(str_replace("/", ", ", $building).", ");
										if($number != "") print($number." ");
										print($xml->Address1);
									print("</option>");
								}
							}
						print("</select>");
					print("</div>");
				print("</div>");
			} else {
				print("<div class='col s12'>");
					print("<div class='input-field'>");
						print("<input type='text' class='depend' id='frm_address1' name='frm_address1' value='".$xml->Address1."' />");
					print("</div>");
				print("</div>");
			}

			if($xml->Address2 != "") {
				print("<div class='col s12'>");
					print("<div class='input-field'>");
						print("<input type='text' class='depend' id='frm_address2' name='frm_address2' value='".$xml->Address2."' />");
					print("</div>");
				print("</div>");
			}

			if($xml->Address3 != "") {
				print("<div class='col s12'>");
					print("<div class='input-field'>");
						print("<input type='text' class='depend' id='frm_address3' name='frm_address3' value='".$xml->Address3."' />");
					print("</div>");
				print("</div>");
			}
			
			if($xml->Address4 != "") {
				print("<input type='text' class='depend' id='frm_address4' name='frm_address4' value='".$xml->Address4."' />");
			}

			print("<div class='col s12 m12 l6'>");
				print("<div class='input-field'>");
					print("<input type='text' class='depend' id='frm_town' name='frm_town' value='".$xml->Town."' />");
				print("</div>");
			print("</div>");
			
			print("<div class='col s12 m12 l6'>");
				print("<div class='input-field'>");
					print("<input type='text' class='depend' id='frm_county' name='frm_county' value='".$xml->County."' />");
				print("</div>");
			print("</div>");
			
		} else {
			
			print("<div class='col s12'>");
				print("<div class='label-note error'>Your postcode is invalid. Please try again or <a href='/contact-us'>contact us</a> if this issue continues.</div>");
			print("</div>");
			
		}
		
	} else {
		
		print("<div class='col s12'>");
			print("<div class='label-note error'>Your postcode is invalid. Please try again or <a href='/contact-us'>contact us</a> if this issue continues.</div>");
		print("</div>");
		
	}
	
?>
