<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	$method = $_REQUEST['method'];
	
	switch($method) {
		
		case "add":
			
			$stock = $_REQUEST['stock'];
			$name = $_REQUEST['name'];
			$email = $_REQUEST['email'];
			$rating = $_REQUEST['rating'];
			$title = $_REQUEST['title'];
			$description = $_REQUEST['description'];
			
			if($accountid > 0) $member = $accountid;
			else {
			
				// Check if customer has an account
				$strdbsql = "SELECT * FROM customer WHERE email = :email";
				$customer = query($conn,$strdbsql,"single",["email"=>$email]);
			
				if($customer) $member = $customer['recordID'];
				else $member = NULL;
				
			}

			$strdbsql = "INSERT INTO stock_reviews (stockGroupID, memberID, reviewerName, reviewerEmail, numStars, description, title, dateReview) VALUES (:stock, :member, :name, :email, :rating, :description, :title, UNIX_TIMESTAMP())";
			$result = query($conn,$strdbsql,"insert",["stock"=>$stock,"member"=>$member,"name"=>$name,"email"=>$email,"rating"=>$rating,"description"=>$description,"title"=>$title]);
			
			if($result) {
				$data = ["type"=>1,"response"=>"Your reviews was submitted successfully."];
			} else {
				$data = ["type"=>0,"response"=>"There was an issue submitting your review. Please <a href='#'>refresh</a> the page and try again."];
			}
			
			break;
			
	}
	
	print(json_encode($data));
	
?>