<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	$product = $_REQUEST['product'];
	
	// If signed in
	if($accountid > 0) {
		
		// Get product details
		$strdbsql = "SELECT recordID, name AS stockGroup FROM stock_group_information 
		WHERE recordID = :product";
		$result = query($conn,$strdbsql,"single",array("product"=>$product));
		
		// Check if product is already a favourites
		$strdbsql = "SELECT * FROM stock_favourites WHERE stockCode = :product AND customerID = :customer";
		$result2 = query($conn,$strdbsql,"single",array("product"=>$result['recordID'],"customer"=>$accountid));
		
		if($result2 == FALSE) {
		
			// Add product to favourites
			$strdbsql = "INSERT INTO stock_favourites (stockCode, customerID) VALUES (:product, :customer)";
			$result3 = query($conn,$strdbsql,"insert",array("product"=>$product,"customer"=>$accountid));
			
			if($result3) {
				$method = "add";
				$message = "<span><strong>".$result['stockGroup']."</strong> has been added to your favourites.</span><a href='/account/favourites' class='right'>View Favourites</a>";
			} else {
				// Error
			}
			
		} else {
			
			// Remove product from favourites
			$strdbsql = "DELETE FROM stock_favourites WHERE stockCode = :product AND customerID = :customer";
			$result3 = query($conn,$strdbsql,"delete",array("product"=>$product,"customer"=>$accountid));
			
			if($result3) {
				$method = "remove";
				$message = "<span><strong>".$result['stockGroup']."</strong> has been removed from your favourites.</span>";
			} else {
				// Error
			}
			
		}
		
	} else {
		
		$method = "login";
		$message = "<span>Login</span>";
		
	}
	
	print(json_encode(array("id"=>$product,"method"=>$method,"message"=>$message)));
	
?>