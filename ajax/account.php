<?php
	if(session_status() != PHP_SESSION_ACTIVE) {
		session_start();
	}
	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	switch($_REQUEST['method']) {
		
		case "login":
	
			$strdbsql = "SELECT * FROM customer WHERE username = :username";
			$result = query($conn,$strdbsql,"single",["username"=>$_REQUEST['username']]);
			
			if($result && password_verify($_REQUEST['password'], $result['password'])) {
				//setcookie("account", $result['recordID'], time() + 60 * 60 * 2, "/");
				$_SESSION['account'] = $result['recordID'];
				$method = "correct";
				$message = "";
			} else {
				$method = "incorrect";
				$message = "Sorry, those details have not been recognised. Please try again.";
			}
			
			print(json_encode(["method"=>$method,"message"=>$message]));
			
			break;
			
		case "register":
		
			// Check if customer has an account
			$strdbsql = "SELECT * FROM customer WHERE email = :email";
			$customer = query($conn,$strdbsql,"single",["email"=>$REQUEST['email']]);
			
			if(!$customer) {
			
				// Create customer account
				$strdbsql = "INSERT INTO customer (groupID, username, password, title, firstname, surname, telephone, email, notes) VALUES (:group, :username, :password, :title, :firstname, :lastname, :telephone, :email, :notes)";
				$addCustomer = query($conn,$strdbsql,"insert",["group"=>1,"username"=>$_REQUEST['username'],"password"=>password_hash($_REQUEST['password'], PASSWORD_BCRYPT),"title"=>$_REQUEST['title'],"firstname"=>$_REQUEST['firstname'],"lastname"=>$_REQUEST['lastname'],"telephone"=>$_REQUEST['telephone'],"email"=>$_REQUEST['email'],"notes"=>$_REQUEST['note']]);
			
				if($addCustomer) {
			
					$customerid = $addCustomer;
				
					$address = explode(", ", $_REQUEST['address']);
					
					// Create an address for customer
					$strdbsql = "INSERT INTO customer_address (customerID, title, firstname, surname, add1, add2, add3, town, county, postcode,country) VALUES (:customer, :title, :firstname, :lastname, :address1, :address2, :address3, :town, :county, :postcode, :country)";
					$addAddress = query($conn,$strdbsql,"insert",["customer"=>$customerid,"title"=>$_REQUEST['title'],"firstname"=>$_REQUEST['firstname'],"lastname"=>$_REQUEST['lastname'],"address1"=>$address[0],"address2"=>$address[1],"address3"=>$address[2],"town"=>$_REQUEST['town'],"county"=>$_REQUEST['county'],"postcode"=>$_REQUEST['postcode'],"country"=>$_REQUEST['country']]);
				
					if($addAddress) {
				
						// Assign address to customer
						$strdbsql = "UPDATE customer SET defaultDeliveryAdd = :delivery, defaultBillingAdd = :delivery WHERE recordID = :customer";
						$assignAddress = query($conn,$strdbsql,"update",["customer"=>$customerid,"delivery"=>$addAddress]);
						
						if($assignAddress) {
							
							// Add customer marketing
							if(!empty($_REQUEST['hear'])) {
								$strdbsql = "INSERT INTO customer_marketing (customerID, value) VALUES (:customer, :value)";
								$marketing = query($conn,$strdbsql,"insert",["customer"=>$customerid,"value"=>$_REQUEST['hear']]);
							}
							
							//setcookie("account", $customerid, time() + 60 * 60 * 2, "/");
							$_SESSION["account"] = $customerid;
							
						} else {
							
							$message = "Sorry, there was an issue creating your account. Please try again.";
							
						}
						
					} else {
					
						$message = "Sorry, there was an issue creating your account. Please try again.";
						
					}
					
				} else {
					
					$message = "Sorry, there was an issue creating your account. Please try again.";
					
				}
				
			} else {
				
				$message = "This email address is already assigned to a Salad Bowl account. Have you <a href='/forgot-password'>forgotten your password?</a>";
				
			}
		
			print(json_encode(["method"=>$method,"message"=>$message]));
		
			break;
			
		case "edit":
			
			// Save customer details
			$strdbsql = "UPDATE customer SET title = :title, firstname = :firstname, surname = :lastname, telephone = :telephone, email = :email, notes = :notes WHERE recordID = :customer";
			$saveCustomer = query($conn,$strdbsql,"update",["customer"=>$accountid,"title"=>$_REQUEST['title'],"firstname"=>$_REQUEST['firstname'],"lastname"=>$_REQUEST['lastname'],"telephone"=>$_REQUEST['telephone'],"email"=>$_REQUEST['email'],"notes"=>$_REQUEST['notes']]);
		
			// Create an address for customer
			$strdbsql = "UPDATE customer_address SET title = :title, firstname = :firstname, surname = :lastname, add1 = :address1, add2 = :address2, town = :town, county = :county, country = :country, postcode = :postcode WHERE customerID = :customer";
			$saveAddress = query($conn,$strdbsql,"update",["customer"=>$accountid,"title"=>$_REQUEST['title'],"firstname"=>$_REQUEST['firstname'],"lastname"=>$_REQUEST['lastname'],"address1"=>$_REQUEST['address'],"address2"=>$_REQUEST['address2'],"town"=>$_REQUEST['town'],"county"=>$_REQUEST['county'],"country"=>$_REQUEST['country'],"postcode"=>$_REQUEST['postcode']]);
		
			if($saveAddress) {
		
				$method = 1;
				$message = "Your account details have been saved.";
				
			} else {
			
				$method = 0;
				$message = "Sorry, there was an issue saving your account details. Please try again.";
				
			}
		
			print(json_encode(["method"=>$method,"message"=>$message]));
			
			break;
			
		case "logout":
		
			//setcookie("account", 0, time() + 60 * 60 * 2, "/");
			$_SESSION['account'] = 0;
			break;
			
		case "forgot-password":
		
			$strdbsql = "SELECT recordID FROM customer WHERE email = :email AND password <> ''";
			$result = query($conn,$strdbsql,"single",["email"=>$_REQUEST['email']]);
			
			if($result) {

				$strdbsql = "UPDATE customer SET requestStamp = UNIX_TIMESTAMP(), requestHash = MD5(CONCAT(UNIX_TIMESTAMP(), `username`))  WHERE email LIKE :email";
				$result2 = query($conn,$strdbsql,"update",["email"=>$_REQUEST['email']]);
			
				if($result2 > 0) {
					passwordemail($conn, $result['recordID']);
					$response = 1;
					$message = "We have sent you an email with instructions on how to reset your password. If you have not received this email, please <a href='/contact-us'>contact us</a>.";
					
				} else {
					$response = 0;
					$message = "We were unable to reset your password. Please try again!";
				}
			} else {
				$response = 0;
				$message = "The email you have entered is not assigned to any account. Please try again or register an account.";
			}
			
			print(json_encode(["response"=>$response,"message"=>$message]));
			
			break;
		case "reset-password":
		
			$strdbsql = "SELECT * FROM customer WHERE recordID = :customer";
			$result = query($conn,$strdbsql,"single",["customer"=>$_REQUEST['customer']]);
			
			if($result) {
				
				$strdbsql = "UPDATE customer SET password = :password, requestStamp = 0, requestHash = '' WHERE recordID = :customer";
				$result2 = query($conn,$strdbsql,"update",["password"=>password_hash($_REQUEST['password'], PASSWORD_BCRYPT),"customer"=>$_REQUEST['customer']]);
				
				if($result2) {
					//setcookie("account", $result['recordID'], time() + 60 * 60 * 2, "/");
					$_SESSION["account"] = $result['recordID'];
					$response = 1;
					$message = "Your password has been reset.";
				} else {
					$response = 0;
					$message = "We were unable to reset your password. Please try again!";
				}
				
			} else {
				$response = 0;
				$message = "We were unable to reset your password. Please try again!";
			}
		
			print(json_encode(["response"=>$response,"message"=>$message]));
		
			break;
	
	}

?>