<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");
	
	require_once(dirname(__file__)."/paypal.php");
	
	$paypal = new PayPal($paypalMode,$conn);
	
	//API Url
	$paymenturl = $paypal->url."/v1/payments/payment";
	
	$itemList = [];
	$subtotal = 0.00;
	
	$strdbsql = "SELECT 
	(basket_items.quantity * basket_items.price) AS itemPrice, 
	basket_items.quantity AS itemQuantity, 
	stock.recordID, 
	stock.stockCode, 
	stock_group_information.name AS groupName, 
	stock_group_information.description AS groupDescription 
	FROM basket_items 
	INNER JOIN stock ON basket_items.stockID = stock.recordID 
	INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
	WHERE basket_items.basketHeaderID = :basket";
	$result = query($conn, $strdbsql,"multi",["basket"=>$basketid]);
	
	// Items
	foreach($result As $row) {
		
		$itemDetails = [
			"quantity" => "".round($row['itemQuantity'])."",
			"name" => $row['groupName'],
			"sku" => $row['stockCode'],
			"price" => number_format($row['itemPrice'] / $row['itemQuantity'], 2, '.', ''),
			"currency" => "GBP",
			"description" => ""
		];
		
		if($row['productType'] > 0) $basketboxes += $row['quantity'];
		$subtotal = $subtotal + $row['itemPrice'];
		$itemList[] = $itemDetails;
	
	}

	// Get basket offers
	//$discount = getDiscountAmount($conn,$basketid,$baskettotal);
	$strdbsql = "SELECT basket_header.discountAmount AS discountTotal, site_offer_codes.*, basket_header.postageID, basket_header.postageAmount FROM basket_header 
	LEFT JOIN site_offer_codes ON basket_header.offerCodeID = site_offer_codes.recordID 
	WHERE basket_header.recordID = :basket";
	$result2 = query($conn,$strdbsql,"single",["basket"=>$basketid]);
	
	if($discount->amount > 0) {
		$itemDetails = [
			"quantity" => "".(1)."",
			"name" => $result2['description'],
			"sku" => "special-offer",
			"price" => number_format((-1*$result2['discountTotal']), 2, '.', ''),
			"currency" => "GBP",
			"description" => $result2['description']
		];
		$subtotal = $subtotal - $result2['discountTotal'];
		$itemList[] = $itemDetails;
	}
	
	
	$deliveryDescription = "Standard Delivery";
	
	//$deliveryCost = checkPostage($conn,1,null,$country,$postcode,$baskettotal);
	
	error_log("Basket: ".$basketid);
	
	$itemDetails = [
		"quantity" => "".(1)."",
		"name" => $deliveryDescription ,
		"sku" => "delivery-".$result2['postageID'],
		"price" => number_format($result2['postageAmount'], 2, '.', ''),
		"currency" => "GBP",
		"description" => $deliveryDescription
	];
	
	$subtotal = $subtotal + $result2['postageAmount'];
	$itemList[] = $itemDetails;
	
	//The JSON data.
	$data = [
		'intent' => "sale", // Payment Type	
		'redirect_urls' => [ // Redirect URLS
			'return_url' => "https://www.kingfisheruk.com/pplanding.php?st=QUICK-PPERROR",
			'cancel_url' => "https://www.kingfisheruk.com/pplanding.php?st=QUICK-PPERROR"
		],
		'payer' => [
			'payment_method' => "paypal" // Payment Method
		],
		"transactions" => [ // Transactions
			[
				"amount" => [
					"total" => number_format($subtotal, 2, '.', ''),
					"currency" => "GBP",
					"details" => [
						"subtotal" => number_format($subtotal, 2, '.', '')
					]
				],
				"item_list" => ["items" => $itemList],
			//	"description" => "The payment transaction description.", // Custom Messages
				"invoice_number" => $basketid,
			]
		]
	];
	
	//Initiate cURL.
	$ch = curl_init($paymenturl);
	
	//Encode the array into JSON.
	$jsonDataEncoded = json_encode($data);
	
	//Tell cURL that we want to send a POST request.
	curl_setopt($ch, CURLOPT_POST, 1);
	 
	//Attach our encoded JSON string to the POST fields.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
	 
	//Set the content type to application/json
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: ".$paypal->tokenType." ".$paypal->token)); 
	 
	//Execute the request
	$result = curl_exec($ch);

?>	
