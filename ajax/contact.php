<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	switch($_REQUEST['method']) {
		
		case "enquiry":
			
			$strdbsql = "INSERT INTO site_contact_history (name, telephone, email, subject, message, date) VALUES (:name, :telephone, :email, :subject, :message, UNIX_TIMESTAMP())";
			$result = query($conn,$strdbsql,"insert",["name"=>$_REQUEST['firstname']." ".$_REQUEST['lastname'],"telephone"=>$_REQUEST['phone'],"email"=>$_REQUEST['email'],"subject"=>"Website Enquiry","message"=>$_REQUEST['message']]);
			
			$method = 0;
			$message = "There was an issue submitting your enquiry. Please try again or call <a href='tel:".$compPhone."'>".$compPhone."</a> if this issue continues.";
			
			if($result) {
				if(enquiryemail($conn,$result)) {
					$method = 1;
					$message = "Your enquiry has been submitted. Well will respond as soon as possible.";
				}
			}
			
			print(json_encode(["method"=>$method,"message"=>$result,"email"=>$compEmail]));
			
			break;
		case "wholesale":
			
			$address = explode(", ", $_REQUEST['busaddress']);
			
			$strdbsql = "INSERT INTO customer_applications (type, date, busName, busWebsite, busBrief, busAddress1, busAddress2, busAddress3, busAddress4, busAddress5, busAddress6, busPostcode, busAim, repTitle, repFirstname, repLastname, repEmail, repPhone) 
			VALUES ('Wholesale Application', UNIX_TIMESTAMP(), :busname, :buswebsite, :busbrief, :busaddress1, :busaddress2, :busaddress3, :bustown, :buscounty, 'GB', :buspostcode, :buspurpose, :reptitle, :repfirstname, :replastname, :repemail, :repphone)";
			$result = query($conn,$strdbsql,"insert",["busname"=>$_REQUEST['busname'],"buswebsite"=>$_REQUEST['buswebsite'],"busbrief"=>$_REQUEST['busbrief'],"busaddress1"=>$address[0],"busaddress2"=>$address[1],"busaddress3"=>$address[2],"bustown"=>$_REQUEST['bustown'],"buscounty"=>$_REQUEST['buscounty'],"buspostcode"=>$_REQUEST['buspostcode'],"buspurpose"=>$_REQUEST['buspurpose'],"reptitle"=>$_REQUEST['reptitle'],"repfirstname"=>$_REQUEST['repfirstname'],"replastname"=>$_REQUEST['replastname'],"repemail"=>$_REQUEST['repemail'],"repphone"=>$_REQUEST['repphone']]);
			
			$method = 0;
			$message = "There was an issue submitting your application. Please try again or call <a href='tel:".$compPhone."'>".$compPhone."</a> if this issue continues.";
			
			if($result) {
				if(applicationemail($conn,$result)) {
					$method = 1;
					$message = "Your application has been submitted. Well will respond as soon as possible.";
				}
			}
			
			print(json_encode(["method"=>$method,"message"=>$result,"email"=>$compEmail]));
			
			break;
		case "newsletter":
		
			$address = explode(", ", $_REQUEST['address']);
			
			if($accountid > 0) {
				$strdbsql = "INSERT INTO customer_newsletter (customerID, name, emailAddress) VALUES (:customer, :name, :email)";
				$result = query($conn,$strdbsql,"insert",["customer"=>$accountid,"name"=>$customertitle." ".$customerfirstname." ".$customersurname,"email"=>$_REQUEST['email']]);
			} else {
				$strdbsql = "INSERT INTO customer_newsletter (emailAddress) VALUES (:email)";
				$result = query($conn,$strdbsql,"insert",["email"=>$_REQUEST['email']]);
			}
			
			$method = 0;
			$message = "Oops! Please try again!";
			
			if($result) {
			//	if(applicationemail($conn,$result)) {
					$method = 1;
					$message = "Thanks for subscribing!";
			//	}
			}
			
			print(json_encode(["method"=>$method,"message"=>$message]));
		
			break;
	
	}

?>