<?php
	
	if(session_status() != PHP_SESSION_ACTIVE) {
		session_start();
	}

	require_once($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	$method = $_REQUEST['method'];
	$product = $_REQUEST['product'];
	$quantity = $_REQUEST['quantity'];
	$country = $_REQUEST['country'];
	$postcode = $_REQUEST['postcode'];
	$postageType = 1;

	error_log("Country: ".$country);
	
	// Get product details
	$strdbsql = "SELECT stock.*, stock_group_information.name AS groupName, stock_group_information.productType, stock_group_information.productCost AS groupPrice FROM stock 
	INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID 
	WHERE stock.recordID = :product";
	$result = query($conn,$strdbsql,"single",["product"=>$product]);
	
	switch($method) {
		
		case "add":
		case "all":
			
			// Check if basket exists
			if($basketid > 0) {
				
				if($result['productType'] > 0) $price = $result['groupPrice'];
				else $price = $result['price'];
			
				$strdbsql = "UPDATE basket_header SET modifiedTimestamp = UNIX_TIMESTAMP() 
				WHERE recordID = :basket";
				$result2 = query($conn,$strdbsql,"update",["basket"=>$basketid]);
			
				// Check if item already exists
				$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basket AND stockID = :product";
				$result3 = query($conn,$strdbsql,"single",["basket"=>$basketid,"product"=>$product]);
				
				// Add/Update to basket_items
				if($result3) {
					$strdbsql = "UPDATE basket_items SET quantity = quantity + :quantity WHERE basketHeaderID = :basket AND stockID = :product";
					$result4 = query($conn,$strdbsql,"update",["basket"=>$basketid,"product"=>$product,"quantity"=>$quantity]);
				} else {
					$strdbsql = "INSERT INTO basket_items (basketHeaderID, stockID, quantity, price) VALUES (:basket, :product, :quantity, :price)";
					$result4 = query($conn,$strdbsql,"insert",["basket"=>$basketid,"product"=>$product,"quantity"=>$quantity,"price"=>$price]);
				}
			
			} else {
			
				// Add to basket_header
				$strdbsql = "INSERT INTO basket_header (createdTimestamp, customerID) VALUES (UNIX_TIMESTAMP(), :user)";
				$result2 = query($conn,$strdbsql,"insert",["user"=>$accountid]);
				
				$basketid = $result2['insert_id'];
				
				// Set basket cookie
				//setcookie("basket", $basketid, time() + 60 * 60 * 2, "/");
				$_SESSION['basket'] = $basketid;
				
				// Add to basket_items
				$strdbsql = "INSERT INTO basket_items (basketHeaderID, stockID, quantity, price) VALUES (:basket, :product, :quantity, :price)";
				$result2 = query($conn,$strdbsql,"insert",["basket"=>$basketid,"product"=>$product,"quantity"=>$quantity,"price"=>$result['price']]);
				
			}

			$basket = basketCheckContents($conn,$basketid);
			$discount = getDiscountAmount($conn,$basketid,$basket->total);
			$baskettotal = round($basket->total - $discount->amount, 2);
			$deliveryamount = checkPostage($conn,$postageType,null,$country,$postcode,$baskettotal);

			print(
				json_encode([
					"id"=>$product,
					"message"=>"<span><strong>".$result['groupName']."</strong> has been added to your basket.</span>",
					"basketdiscount"=>$discount->amount,
					"vatamount"=>$basket->vat,
					"baskettotal"=>$baskettotal+$deliveryamount,
					"deliveryamount"=>$deliveryamount,
					"packSizeWarning"=>$basket->packSizeWarning,
					"qtyWarning"=>$basket->qtyError
				])
			);
			
			break;
			
		case "edit":
			
			$strdbsql = "UPDATE basket_header SET modifiedTimestamp = UNIX_TIMESTAMP() 
			WHERE recordID = :basket";
			$result2 = query($conn,$strdbsql,"update",["basket"=>$basketid]);
			
			$strdbsql = "UPDATE basket_items SET quantity = quantity + :quantity WHERE stockID = :product AND basketHeaderID = :basket";
			$result3 = query($conn,$strdbsql,"update",["product"=>$product,"quantity"=>$quantity,"basket"=>$basketid]);

			//get new quantity
			$strdbsql = "SELECT basket_items.quantity FROM basket_items WHERE stockID = :product AND basketHeaderID = :basket";
			$item = query($conn,$strdbsql,"single",["product"=>$product,"basket"=>$basketid]);
			
			$basket = basketCheckContents($conn,$basketid);
			$discount = getDiscountAmount($conn,$basketid,$basket->total);
			$baskettotal = round($basket->total - $discount->amount, 2);
			$deliveryamount = checkPostage($conn,$postageType,null,$country,$postcode,$baskettotal);
			
			print(
				json_encode([
					"id"=>$product,
					"quantity"=>floatVal($item['quantity']),
					"price"=>round($result['price'] * $item['quantity'], 2),
					"basketdiscount"=>$discount->amount,
					"vatamount"=>$basket->vat,
					"baskettotal"=>$baskettotal+$deliveryamount,
					"deliveryamount"=>$deliveryamount,
					"packSizeWarning"=>$basket->packSizeWarning,
					"qtyWarning"=>$basket->qtyError
				])
			);
			
			break;
			
		case "remove":
		
			$strdbsql = "UPDATE basket_header SET modifiedTimestamp = UNIX_TIMESTAMP() 
			WHERE recordID = :basket";
			$result2 = query($conn,$strdbsql,"update",["basket"=>$basketid]);
		
			$strdbsql = "DELETE FROM basket_items WHERE basketHeaderID = :basket AND stockID = :product";
			$result3 = query($conn,$strdbsql,"delete",["basket"=>$basketid,"product"=>$product]);
			
			$basket = basketCheckContents($conn,$basketid);
			$discount = getDiscountAmount($conn,$basketid,$basket->total);
			$baskettotal = round($basket->total - $discount->amount, 2);
			$deliveryamount = checkPostage($conn,$postageType,null,$country,$postcode,$baskettotal);
			
			if($result2) {
				print(
					json_encode([
						"id"=>$product,
						"message"=>"<span><strong>".$result['groupName']."</strong> has been removed from your basket.</span>",
						"basketdiscount"=>$discount->amount,
						"vatamount"=>$basket->vat,
						"baskettotal"=>$baskettotal+$deliveryamount,
						"deliveryamount"=>$deliveryamount,
						"packSizeWarning"=>$basket->packSizeWarning,
						"qtyWarning"=>$basket->qtyError
					])
				);
			}
		
			break;
			
		case "discount":
			
			//TODO min/max amounts , qualifying/excluded items etc.
			$strdbsql = "SELECT * FROM site_offer_codes WHERE code = :code";
			$result2 = query($conn,$strdbsql,"single",["code"=>$_REQUEST['code']]);
			
			if($basketdiscount == 0) {
				if($result2) {
					if($result2['totalUses'] >= $result2['allowedUses']) {
						if($result2['startDate'] < time()) {
							if($result2['expiryDate'] > time() || $result2['expiryDate'] == 0) {
								if($baskettotal >= $result2['minimumSpend']) {
									
									if($result2['discountType'] == "percent") $basketdiscount = round($baskettotal * (100 - $result2['discountAmount']) / 100, 2);
									else $basketdiscount = round($result2['discountAmount'], 2);
									
									$baskettotal = round($baskettotal - $basketdiscount, 2);
									
									$deliveryamount = checkPostage($conn,$postageType,null,$country,$postcode,$baskettotal);
									
									$strdbsql = "UPDATE basket_header SET offerCodeID = :discountid, discountAmount = :discounttotal, postageAmount = :postage WHERE recordID = :basket";
									$result3 = query($conn,$strdbsql,"update",["discountid"=>$result2['recordID'],"discounttotal"=>$basketdiscount,"basket"=>$basketid, "postage" => $deliveryamount]);
									
									if($result3) {
										$message = $result2['description'];
									} else {
										$message = "There was an issue with this promo code.";
									}
								} else {
									// The minimum spend has not been met
									$message = "Spend a minimum of £".number_format($result2['minimumSpend'], 2)." to use this promo code.";
								}
							} else {
								// The discount code has expired
								$message = "This promo code has expired.";
							}
						} else {
							// The discount code is yet to be activated
							$message = "You have entered an invalid promo code.";
						}
					} else {
						// The discount code has reached its max uses
						$message = "This promo code is no longer valid.";
					}				
				} else {
					$message = "You have entered an invalid promo code.";
				}
			} else {
				$message = "You can only apply 1 discount per order.";
			}
			
			$deliveryamount = checkPostage($conn,$postageType,null,$country,$postcode,$baskettotal);
			
			print(
				json_encode([
					"message"=>$message,
					"discounttype"=>$result2['discountType'],
					"discountlabel"=>$result2['description'],
					"discountvalue"=>$basketdiscount,
					"deliveryamount"=>$deliveryamount,
					"discounttoggle"=>$result2['removeable'],
					"baskettotal"=>$baskettotal
				])
			);
		
			break;
			
		case "discountremove":
		
			if($basketdiscount > 0) {

				$strdbsql = "SELECT basket_header.offerCodeID AS offerID, basket_header.discountAmount AS discountTotal, basket_header.postageID, site_offer_codes.* FROM basket_header 
				INNER JOIN site_offer_codes ON basket_header.offerCodeID = site_offer_codes.recordID 
				WHERE basket_header.recordID = :basket";
				$result2 = query($conn,$strdbsql,"single",["basket"=>$basketid]);
				
				if($result2) {
			
					$basketdiscount = 0;
					$baskettotal = round($baskettotal + $result2['discountTotal'], 2);
					
					$deliveryamount = checkPostage($conn,$postageType,null,$country,$postcode,$baskettotal);
					
					$strdbsql = "UPDATE basket_header SET offerCodeID = NULL, discountAmount = 0, postageAmount = :postage WHERE recordID = :basket";
					$result3 = query($conn,$strdbsql,"update",["basket"=>$basketid, "postage" => $deliveryamount]);
					if($result3) {
						$message = "Discount has been removed.";
					} else {
						$message = "There was an error removing the discount.";
					}
				} else {
					$message = "There was an error removing the discount.";
				}
			} else {
				$message = "There was an error removing the discount.";
			}
		
			print(
				json_encode([
					"message"=>$message,
					"deliveryamount"=>$deliveryamount,
					"baskettotal"=>$baskettotal
				])
			);
		
			break;
			
		case "changePostage":
			
			$basket = basketCheckContents($conn,$basketid);
			$discount = getDiscountAmount($conn,$basketid,$basket->total);
			$baskettotal = round($basket->total - $discount->amount, 2);
			$deliveryamount = checkPostage($conn,$postageType,null,$country,$postcode,$baskettotal);
			
			$q = "UPDATE basket_header SET postageAmount = :postage, postageID = :postageID, modifiedTimestamp = UNIX_TIMESTAMP() WHERE basket_header.recordID = :basketid ";
			$update = query($conn,$q,"update",[
				"postage" => $deliveryamount,
				"postageID" => $postageType,
				"basketid"=>$basketid
			]);
			
			if($update) {
				print(
					json_encode([
						"basketdiscount"=>$discount->amount,
						"vatamount"=>$basket->vat,
						"baskettotal"=>$baskettotal+$deliveryamount,
						"deliveryamount"=>$deliveryamount,
						"packSizeWarning"=>$basket->packSizeWarning,
						"qtyWarning"=>$basket->qtyError
					])
				);
			}
			
			break;
	}
	

?>