<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");
	
	// Save customer information
	$title = $_REQUEST['title'];
	$firstname = $_REQUEST['firstname'];
	$lastname = $_REQUEST['lastname'];
	$email = $_REQUEST['email'];
	$postcode = $_REQUEST['postcode'];
	$address = explode(", ", $_REQUEST['address']);
	$town = $_REQUEST['town'];
	$county = $_REQUEST['county'];
	$country = $_REQUEST['country'];
	$notes = $_REQUEST['note'];
	$telephone = $_REQUEST['telephone'];
	$date = strtotime($_REQUEST['date']);
	$time = $_REQUEST['time'];
	$hear = $_REQUEST['hear'];
	$newsletter = $_REQUEST['newsletter'];
	$pageageType = 1;
	
	// Get basket information
	$basket = basketCheckContents($conn,$basketid);

	$discount = getDiscountAmount($conn,$basketid,$basket->total);
	$baskettotal = round($basket->total - $basketdiscount, 2);
	
	$deliveryamount = checkPostage($conn,1,null,$country,$postcode,$baskettotal);
			
	// Get Stripe API library
	require_once($_SERVER['DOCUMENT_ROOT']."/stripe/init.php");

	// Set your secret key: remember to change this to your live secret key in production
	// See your keys here: https://dashboard.stripe.com/account/apikeys
	\Stripe\Stripe::setApiKey($stripeKey);

	// Token is created using Checkout or Elements!
	// Get the payment token ID submitted by the form:
	$token = $_REQUEST['token'];
	
	$customer = \Stripe\Customer::create([
		'email' => $email,
		'source'  => $token
	]);
	
	$charge = \Stripe\Charge::create([
		'customer' => $customer->id,
		'amount' => number_format(($baskettotal+$deliveryamount) * 100, 0, ".", ""),
		'currency' => 'GBP'
	]);
	
	$strdbsql = "SELECT countryCode FROM configCountries WHERE countryName LIKE :country";
	$countryDetails = query($conn,$strdbsql,"single",["country"=>$country]);
	if(!empty($countryDetails)) {
		$country = $countryDetails['countryCode'];
	}
	
	if($charge) {
		
		$customerid = checkcustomer($conn, $accountid, $title, $firstname, $lastname, $telephone, $address[0], $address[1], $address[2], $town, $county, $country, $postcode, $email);
	
		// Add customer marketing
		if(!empty($hear)) {
			$strdbsql = "INSERT INTO customer_marketing (customerID, value) VALUES (:customer, :value)";
			$marketing = query($conn,$strdbsql,"insert",["customer"=>$customerid,"value"=>$hear]);
		}
		
		// Add customer to newsletter
		if($newsletter == 1) {
			// Check if already on newsletter
			$strdbsql = "SELECT * FROM customer_newsletter WHERE emailAddress = :email";
			$newsletter = query($conn,$strdbsql,"single",["email"=>$email]);
			// Add to newsletter
			if(!$newsletter) {
				$strdbsql = "INSERT INTO customer_newsletter (customerID, name, emailAddress) VALUES (:customer, :name, :email)";
				$newsletter = query($conn,$strdbsql,"insert",["customer"=>$customerid,"name"=>$title." ".$firstname." ".$lastname,"email"=>$email]);
			}
		}
		
		$orderid = createorder($conn, $customerid, $basketid, $basket, $firstname, $lastname, $address[0], $address[1], $address[2], $town, $county, $country, $postcode, $date, $time, $telephone, $email, $notes, $strdomain, 1, round($baskettotal+$deliveryamount,2));
	
		deletebasket($conn, $basketid);
		
		confirmationemail($conn, $basketid);
		
		$order = [
			'id' => $orderid
		];
		
	}
	
	print(json_encode($order));

?>