<?php
	
	if(session_status() != PHP_SESSION_ACTIVE) {
		session_start();
	}
	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");

	$method = $_REQUEST['method'];
	$product = $_REQUEST['product'];
	
	switch($method) {
		
		case "size":
		
			// Get product sizes
			$strdbsql = "SELECT stock_units.standard AS amount FROM stock 
			INNER JOIN stock_units ON stock.unit = stock_units.recordID 
			WHERE stock.recordID = :product";
			$result = query($conn,$strdbsql,"single",["product"=>$product]);
			
			$status = 0;
			$maximum = 10000;
			
			if($result['amount'] >= $maximum) $status = 1;
			
			print(
				json_encode([
					"id"=>$product,
					"amount"=>$result['amount']
				])
			);
			
			break;

	}
	
?>